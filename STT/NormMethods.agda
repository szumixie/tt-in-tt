{-# OPTIONS --no-eta --rewriting #-}

module STT.NormMethods where

open import Level

open import lib

open import STT.Syntax
open import STT.Elim
open import STT.Nf

infixl 5 _,ᴹ_
infixl 4 _⇒ᴹ_
infixl 5 _,ₛᴹ_
infix  6 _∘ᴹ_
infixl 8 _[_]ᴹ

Tyᴹ : Ty → Set₁
Tyᴹ A = ∀ {Γ} → Tm Γ A → Set

Conᴹ : Con → Set₁
Conᴹ Δ = ∀ {Γ} → Tms Γ Δ → Set

Tmsᴹ : ∀ {Γ} → Conᴹ Γ → ∀{Δ} → Conᴹ Δ → Tms Γ Δ → Set
Tmsᴹ {θ} θᴹ {Δ} Δᴹ σ = ∀ Γ (δ : Tms Γ θ) → θᴹ δ → Δᴹ (σ ∘ δ)

Tmᴹ : ∀ {Γ} → Conᴹ Γ → ∀{A} → Tyᴹ A → Tm Γ A → Set
Tmᴹ {θ} θᴹ {A} Aᴹ t = ∀ Γ (σ : Tms Γ θ) → θᴹ σ → Aᴹ (t [ σ ])

ιᴹ : Tyᴹ ι
ιᴹ {Γ} t = Σ (Nf Γ ι) λ n → ⌜ n ⌝ ≡ t

_⇒ᴹ_ : ∀ {A} → Tyᴹ A → ∀ {B} → Tyᴹ B → Tyᴹ (A ⇒ B)
_⇒ᴹ_ {A} Aᴹ {B} Bᴹ {Γ} t = (u : Tm Γ A) → Aᴹ u → Bᴹ (t $ u)

∙ᴹ : Conᴹ ∙
∙ᴹ _ = ⊤

_,ᴹ_ : ∀ {Γ} → Conᴹ Γ → ∀ {A} → Tyᴹ A → Conᴹ (Γ , A)
_,ᴹ_ Δᴹ Aᴹ σ = Δᴹ (π₁ σ) × Aᴹ (π₂ σ)

idᴹ : ∀ {Γ}{Γᴹ : Conᴹ Γ} → Tmsᴹ Γᴹ Γᴹ (id {Γ})
idᴹ {Δ}{Δᴹ} σ _ = coe (ap Δᴹ (idl ⁻¹))

_∘ᴹ_ :
  ∀ {Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}
  → {σ : Tms Δ Σ} → Tmsᴹ Δᴹ Σᴹ σ → {ν : Tms Γ Δ} → Tmsᴹ Γᴹ Δᴹ ν → Tmsᴹ Γᴹ Σᴹ (σ ∘ ν)
_∘ᴹ_ {Σᴹ = Σᴹ} {σ} σᴹ {ν} νᴹ _ δ δᴹ = coe (ap Σᴹ (ass ⁻¹)) (σᴹ _ (ν ∘ δ) (νᴹ _ δ δᴹ))

εᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ} → Tmsᴹ Γᴹ ∙ᴹ ε
εᴹ _ _ _ = tt

_,ₛᴹ_ :
  ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ} → Tmsᴹ Γᴹ Δᴹ δ
  → ∀{A}{Aᴹ : Tyᴹ A}{t : Tm Γ A} → Tmᴹ Γᴹ Aᴹ t → Tmsᴹ Γᴹ (Δᴹ ,ᴹ Aᴹ) (δ ,ₛ t)
_,ₛᴹ_ {Δᴹ = Δᴹ} δᴹ {Aᴹ = Aᴹ} tᴹ _ σ σᴹ =
  coe (ap Δᴹ ((ap π₁ ,∘ ◾ ,β₁) ⁻¹)) (δᴹ _ σ σᴹ) ,Σ
  coe (ap Aᴹ ((ap π₂ ,∘ ◾ ,β₂) ⁻¹)) (tᴹ _ σ σᴹ)

π₁ᴹ :
  ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A}{Aᴹ : Tyᴹ A}{δ : Tms Γ (Δ , A)}
  → Tmsᴹ Γᴹ (Δᴹ ,ᴹ Aᴹ) δ → Tmsᴹ Γᴹ Δᴹ (π₁ δ)
π₁ᴹ {Δᴹ = Δᴹ}{δ = δ} δᴹ Σ σ σᴹ =
  coe (ap Δᴹ (
    π₁ (δ ∘ σ)                  ≡⟨ ap (λ x → π₁ (x ∘ σ)) (,η ⁻¹) ⟩
    π₁ ((π₁ δ ,ₛ π₂ δ) ∘ σ)     ≡⟨ ap π₁ ,∘ ⟩
    π₁ (π₁ δ ∘ σ ,ₛ π₂ δ [ σ ]) ≡⟨ ,β₁ ⟩
    π₁ δ ∘ σ ∎
    ))
  (proj₁ (δᴹ _ σ σᴹ))

_[_]ᴹ  :
  ∀ {Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A}{Aᴹ : Tyᴹ A}{t : Tm Δ A}
  → Tmᴹ Δᴹ Aᴹ t → {δ : Tms Γ Δ} → Tmsᴹ Γᴹ Δᴹ δ → Tmᴹ Γᴹ Aᴹ (t [ δ ])
_[_]ᴹ {Aᴹ = Aᴹ} tᴹ {δ} δᴹ _ σ σᴹ = coe (ap Aᴹ ([][] ⁻¹)) (tᴹ _ (δ ∘ σ) (δᴹ _ σ σᴹ))

π₂ᴹ :
  ∀ {Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A}{Aᴹ : Tyᴹ A}{δ : Tms Γ (Δ , A)}
  → Tmsᴹ Γᴹ (Δᴹ ,ᴹ Aᴹ) δ → Tmᴹ Γᴹ Aᴹ (π₂ δ)
π₂ᴹ {Aᴹ = Aᴹ}{δ = δ} δᴹ _ σ σᴹ =
  coe (ap Aᴹ (
    π₂ (δ ∘ σ)                  ≡⟨ ap (λ x → π₂ (x ∘ σ)) (,η ⁻¹) ⟩
    π₂ ((π₁ δ ,ₛ π₂ δ) ∘ σ)     ≡⟨ ap π₂ ,∘ ⟩
    π₂ (π₁ δ ∘ σ ,ₛ π₂ δ [ σ ]) ≡⟨ ,β₂ ⟩
    π₂ δ [ σ ] ∎
    ))
  (proj₂ (δᴹ _ σ σᴹ))


-- [id]ᴹ
--------------------------------------------------------------------------------

-- todo : factor out funext+coe-apply into two functions,
--        one for Tmᴹ and one for Tmsᴹ

uncoe2 : -- uses K
  ∀{α β γ}{A : Set α}{B : A → Set β}{C : ∀ a → B a → Set γ}
  → (f : (a : A) → (b : B a) → C a b)
  → {a a' : A}
  → (b : B a)
  → (p : a ≡ a')
  → (q : C a' (coe (ap B p) b) ≡ C a b)
  → f a b ≡ coe q (f a' (coe (ap B p) b))
uncoe2 f b refl refl = refl

ap-◾ :
  ∀ {α}{A B C : Set α}
  → (p : B ≡ C)
  → (q : A ≡ B)
  → (a : A)
  → coe p (coe q a) ≡ coe (q ◾ p) a
ap-◾ refl refl a = refl

[id]ᴹ :
  ∀{Γ}{Γᴹ : Conᴹ Γ}{A}{Aᴹ : Tyᴹ A}{t : Tm Γ A}{tᴹ : Tmᴹ Γᴹ Aᴹ t}
  -- tᴹ [ idᴹ ]ᴹ ≡[ ap (Tmᴹ Γᴹ Aᴹ) [id] ]≡ tᴹ
  → _[_]ᴹ {Aᴹ = Aᴹ} tᴹ idᴹ ≡[ ap (Tmᴹ Γᴹ Aᴹ) [id] ]≡ tᴹ
[id]ᴹ {Γ}{Γᴹ}{A}{Aᴹ}{t}{tᴹ} = funext λ Δ → funext λ σ → funext λ σᴹ →
  ap (λ x → x σ σᴹ) (coe-apply
    (λ Γ' t → (σ : Tms Γ' Γ) → Γᴹ σ → Aᴹ (t [ σ ])) [id]
    (λ _ σ₁ σᴹ₁ → coe (ap Aᴹ ([][] ⁻¹)) (tᴹ _ (id ∘ σ₁) (coe (ap Γᴹ (idl ⁻¹)) σᴹ₁)))
    Δ)
  ◾
  (ap (λ x → x σᴹ) (coe-apply
    (λ σ t → Γᴹ σ → Aᴹ (t [ σ ])) [id]
    (λ σ₁ σᴹ₁ → coe (ap Aᴹ ([][] ⁻¹)) (tᴹ _ (id ∘ σ₁) (coe (ap Γᴹ (idl ⁻¹)) σᴹ₁)))
    σ))
  ◾
  coe-apply
    (λ σᴹ t → Aᴹ (t [ σ ]))
    [id]
    (λ σᴹ → coe (ap Aᴹ ([][] ⁻¹)) (tᴹ _ (id ∘ σ) (coe (ap Γᴹ (idl ⁻¹)) σᴹ)))
    σᴹ
  ◾
  ap-◾
    ((ap (λ t₁ → Aᴹ (t₁ [ σ ])) [id]))
    (ap Aᴹ ([][] ⁻¹))
    (tᴹ _ (id ∘ σ) (coe (ap Γᴹ (idl ⁻¹)) σᴹ))
  ◾
  uncoe2 (tᴹ _) σᴹ (idl ⁻¹) (ap Aᴹ ([][] ⁻¹) ◾ ap (λ t₁ → Aᴹ (t₁ [ σ ])) [id]) ⁻¹

-- [][]ᴹ
--------------------------------------------------------------------------------

-- [][]ᴹ :
--   ∀ {Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{A}{Aᴹ : Tyᴹ A}
--    {t : Tm Σ A}{tᴹ : Tmᴹ Σᴹ Aᴹ t}{σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}{δ}{δᴹ : Tmsᴹ Δᴹ Σᴹ δ}
   
--    -- tᴹ [ δᴹ ]ᴹ [ σᴹ ]ᴹ ≡[ ap (Tmᴹ Γᴹ Aᴹ) [][] ]≡ tᴹ [ δᴹ ∘ᴹ σᴹ ]ᴹ
   
--    → _[_]ᴹ {Aᴹ = Aᴹ} (_[_]ᴹ {Aᴹ = Aᴹ} tᴹ δᴹ) σᴹ
--             ≡[ ap (Tmᴹ Γᴹ Aᴹ) [][] ]≡
--      _[_]ᴹ {Aᴹ = Aᴹ} tᴹ (_∘ᴹ_ {Σᴹ = Σᴹ} δᴹ σᴹ)
   
-- [][]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ}{A}{Aᴹ}{t}{tᴹ}{σ}{σᴹ}{δ}{δᴹ} = funext λ Ξ → funext λ γ → funext λ γᴹ → 
--   {!!}   


