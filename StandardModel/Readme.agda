module StandardModel.Readme where

-- this folder contains the standard model (set model) of type theory

-- modelling different parts of the syntax:

import StandardModel.Decl              -- declaration of the model
import StandardModel.Core              -- substitution calculus
import StandardModel.Base              -- base type and family over it
import StandardModel.Func              -- function space
import StandardModel.Extensions        -- 0, 1, 2, ℕ, Id, Σ types
import StandardModel.FuncU             -- a universe closed under Π
import StandardModel.FuncSP            -- a strictly positive function space
import StandardModel.FuncPar           -- function space with a metatheoretic domain
import StandardModel.ExtIden           -- extensional identity type
import StandardModel.Restriction       -- a restriction type

-- putting this together into an interpretation from the syntax to the metatheory:

import StandardModel.StandardModel     -- interpretation into the standard model
import StandardModel.Cons              -- consistency proof from the standard model

-- an inductive-recursive variant:

import StandardModel.IR.U0             -- a small inductive-recursive universe
import StandardModel.IR.U1             -- a large inductive-recursive universe containing U0
import StandardModel.IR.StandardModel  -- modelling Π, Σ and a universe closed under Π

-- model of a type theory with two universe levels:

import StandardModel.TwoLevels.Decl
import StandardModel.TwoLevels.Core
import StandardModel.TwoLevels.Univ
