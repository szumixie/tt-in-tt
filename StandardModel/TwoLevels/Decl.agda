{-# OPTIONS --without-K --no-eta #-}

module StandardModel.TwoLevels.Decl where

open import Agda.Primitive
open import lib
open import TT.TwoLevels.Decl

d : Decl
d = record
  { Con = Set₁
  ; Ty₀ = λ ⟦Γ⟧     → ⟦Γ⟧ → Set₀
  ; Ty₁ = λ ⟦Γ⟧     → ⟦Γ⟧ → Set₁
  ; Tms = λ ⟦Γ⟧ ⟦Δ⟧ → ⟦Γ⟧ → ⟦Δ⟧
  ; Tm₀ = λ ⟦Γ⟧ ⟦A⟧ → (γ : ⟦Γ⟧) → (⟦A⟧ γ)
  ; Tm₁ = λ ⟦Γ⟧ ⟦A⟧ → (γ : ⟦Γ⟧) → (⟦A⟧ γ)
  }
