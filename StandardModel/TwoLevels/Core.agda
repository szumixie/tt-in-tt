{-# OPTIONS --without-K --no-eta #-}

module StandardModel.TwoLevels.Core where

open import StandardModel.TwoLevels.Decl

open import lib
open import TT.TwoLevels.Core

c : Core d
c = record
  { •      = Lift ⊤
  ; _,₀_   = Σ
  ; _,₁_   = Σ
  ; _[_]T₀ = λ ⟦A⟧ ⟦δ⟧ γ → ⟦A⟧ (⟦δ⟧ γ)
  ; _[_]T₁ = λ ⟦A⟧ ⟦δ⟧ γ → ⟦A⟧ (⟦δ⟧ γ)
  ; id     = λ         γ → γ
  ; _∘_    = λ ⟦δ⟧ ⟦σ⟧ γ → ⟦δ⟧ (⟦σ⟧ γ)
  ; ε      = λ         _ → lift tt
  ; _,s₀_  = λ ⟦δ⟧ ⟦t⟧ γ → ⟦δ⟧ γ ,Σ ⟦t⟧ γ
  ; _,s₁_  = λ ⟦δ⟧ ⟦t⟧ γ → ⟦δ⟧ γ ,Σ ⟦t⟧ γ
  ; π₁₀    = λ ⟦δ⟧     γ → proj₁ (⟦δ⟧ γ)
  ; π₁₁    = λ ⟦δ⟧     γ → proj₁ (⟦δ⟧ γ)
  ; _[_]t₀ = λ ⟦t⟧ ⟦δ⟧ γ → ⟦t⟧ (⟦δ⟧ γ)
  ; _[_]t₁ = λ ⟦t⟧ ⟦δ⟧ γ → ⟦t⟧ (⟦δ⟧ γ)
  ; π₂₀    = λ ⟦δ⟧     γ → proj₂ (⟦δ⟧ γ)
  ; π₂₁    = λ ⟦δ⟧     γ → proj₂ (⟦δ⟧ γ)
  ; [id]T₀ = refl
  ; [id]T₁ = refl
  ; [][]T₀ = refl
  ; [][]T₁ = refl
  ; idl    = refl
  ; idr    = refl
  ; ass    = refl
  ; ,∘₀    = refl
  ; ,∘₁    = refl
  ; π₁β₀   = refl
  ; π₁β₁   = refl
  ; πη₀    = refl
  ; πη₁    = refl
  ; εη     = refl
  ; π₂β₀   = refl
  ; π₂β₁   = refl
  }
