{-# OPTIONS --without-K --no-eta #-}

module StandardModel.TwoLevels.Univ where

open import StandardModel.TwoLevels.Decl
open import StandardModel.TwoLevels.Core

open import lib
open import TT.TwoLevels.Univ

u : Univ c
u = record
  { U = λ _ → Set
  ; U[] = refl
  ; El = λ a γ → a γ
  ; El[] = refl
  }
