{-# OPTIONS --without-K --no-eta #-}

module StandardModel.Core {i} where

open import StandardModel.Decl {i}

open import lib
open import TT.Core

c : Core d
c = record
  { c1 = record
  { •     =             Lift ⊤
  ; _,_   = λ ⟦Γ⟧ ⟦A⟧ → Σ ⟦Γ⟧ ⟦A⟧
  ; _[_]T = λ ⟦A⟧ ⟦δ⟧ γ → ⟦A⟧ (⟦δ⟧ γ)
  ; id    = λ         γ → γ
  ; _∘_   = λ ⟦δ⟧ ⟦σ⟧ γ → ⟦δ⟧ (⟦σ⟧ γ)
  ; ε     = λ         _ → lift tt
  ; _,s_  = λ ⟦δ⟧ ⟦t⟧ γ → ⟦δ⟧ γ ,Σ ⟦t⟧ γ
  ; π₁    = λ ⟦δ⟧     γ → proj₁ (⟦δ⟧ γ)
  ; _[_]t = λ ⟦t⟧ ⟦δ⟧ γ → ⟦t⟧ (⟦δ⟧ γ)
  ; π₂    = λ ⟦δ⟧     γ → proj₂ (⟦δ⟧ γ)
  } ; c2 = record
  { [id]T = refl
  ; [][]T = refl
  ; idl   = refl
  ; idr   = refl
  ; ass   = refl
  ; ,∘    = refl
  ; π₁β   = refl
  ; πη    = refl
  ; εη    = refl
  ; π₂β   = refl
  } }
