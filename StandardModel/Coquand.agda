{-# OPTIONS --rewriting --without-K #-}

module StandardModel.Coquand where

open import Agda.Primitive
open import lib

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
-- infix 4 <_>
infixl 5 _^_

record Con (i : Level) : Set (lsuc i) where
  field
    ∣_∣C : Set i
open Con public

record Ty {j : Level}(Γ : Con j)(i : Level) : Set (lsuc i ⊔ j) where
  field
    ∣_∣T  : ∣ Γ ∣C → Set i
open Ty public

• : {i : Level} → Con i -- \bub
• = record { ∣_∣C = Lift ⊤ }

_▷_ : {i : Level}(Γ : Con i){j : Level}(A : Ty Γ j) → Con (i ⊔ j)
Γ ▷ A = record { ∣_∣C = Σ ∣ Γ ∣C ∣ A ∣T }

record Sub {i : Level}(Γ : Con i){j : Level}(Δ : Con j) : Set (i ⊔ j) where
  field
    ∣_∣s : ∣ Γ ∣C → ∣ Δ ∣C
open Sub public    

record Tm {i : Level}(Γ : Con i){j : Level}(A : Ty Γ j) : Set (i ⊔ j) where
  field
    ∣_∣t : (γ : ∣ Γ ∣C) → ∣ A ∣T γ
open Tm public

_[_]T : {i : Level}{Δ : Con i}{j : Level}(A : Ty Δ j){k : Level}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
A [ σ ]T = record { ∣_∣T = λ γ → ∣ A ∣T (∣ σ ∣s γ) }

id : {i : Level}{Γ : Con i} → Sub Γ Γ
id = record { ∣_∣s = λ γ → γ }

_∘_ : {i : Level}{Θ : Con i}{j : Level}{Δ : Con j}(σ : Sub Θ Δ){k : Level}{Γ : Con k}(δ : Sub Γ Θ) → Sub Γ Δ
σ ∘ δ = record { ∣_∣s = λ γ → ∣ σ ∣s (∣ δ ∣s γ) }

ε : {i : Level}{Γ : Con i}{j : Level} → Sub Γ {j} •
ε = record { ∣_∣s = λ _ → lift tt }

•η : {i : Level}{Γ : Con i}{j : Level}{σ : Sub Γ {j} •} → σ ≡ ε
•η = refl

_,_ : {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}(σ : Sub Γ Δ){k : Level}{A : Ty Δ k}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▷ A)
σ , t = record { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ }

π₁ : {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}{k : Level}{A : Ty Δ k}(σ : Sub Γ (Δ ▷ A)) → Sub Γ Δ
π₁ σ = record { ∣_∣s = λ γ → proj₁ (∣ σ ∣s γ) }

π₂ : {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}{k : Level}{A : Ty Δ k}(σ : Sub Γ (Δ ▷ A)) → Tm Γ (A [ π₁ σ ]T)
π₂ σ = record { ∣_∣t = λ γ → proj₂ (∣ σ ∣s γ) }

_[_]t : {i : Level}{Δ : Con i}{j : Level}{A : Ty Δ j}(t : Tm Δ A){k : Level}{Γ : Con k}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = record { ∣_∣t = λ γ → ∣ t ∣t (∣ σ ∣s γ) }

[id]T : {i : Level}{Γ : Con i}{j : Level}{A : Ty Γ j} → A [ id ]T ≡ A
[id]T = refl

[∘]T : {i : Level}{Θ : Con i}{j : Level}{Δ : Con j}{σ : Sub Θ Δ}{k : Level}{Γ : Con k}{δ : Sub Γ Θ}
  {l : Level}{A : Ty Δ l} → A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T
[∘]T = refl

idl : {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ ≡ σ
idl = refl

idr : {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id ≡ σ
idr = refl

ass : {i : Level}{Θ : Con i}{j : Level}{Δ : Con j}{σ : Sub Θ Δ}{k : Level}{Ξ : Con k}{δ : Sub Ξ Θ}{l : Level}{Γ : Con l}
  {ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = refl

▷β₁ : {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}{σ : Sub Γ Δ}{k : Level}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} →
  π₁ {A = A}(σ , t) ≡ σ
▷β₁ = refl

▷β₂ : {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}{σ : Sub Γ Δ}{k : Level}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} →
  π₂ {A = A}(σ , t) ≡ t
▷β₂ = refl

▷η : {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}{k : Level}{A : Ty Δ k}{σ : Sub Γ (Δ ▷ A)} →
  (π₁ σ , π₂ σ) ≡ σ
▷η = refl

,∘ : {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}{σ : Sub Γ Δ}{k : Level}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)}
  {l : Level}{Θ : Con l}{δ : Sub Θ Γ} → (_,_ σ {A = A} t) ∘ δ ≡ (σ ∘ δ , t [ δ ]t)
,∘ = refl

-- abbreviations

_^_ : {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}(σ : Sub Γ Δ){k : Level}(A : Ty Δ k) →
  Sub (Γ ▷ A [ σ ]T) (Δ ▷ A)
σ ^ A = σ ∘ π₁ id , π₂ id

-- Π

Π : {i : Level}{Γ : Con i}{j : Level}(A : Ty Γ j){k : Level}(B : Ty (Γ ▷ A) k) → Ty Γ (j ⊔ k)
Π A B = record { ∣_∣T = λ γ → (α : ∣ A ∣T γ) → ∣ B ∣T (γ ,Σ α) }

Π[] : {i : Level}{Γ : Con i}{j : Level}{A : Ty Γ j}{k : Level}{B : Ty (Γ ▷ A) k}{l : Level}
  {Θ : Con l}{σ : Sub Θ Γ} → Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ^ A ]T)
Π[] = refl

lam : {i : Level}{Γ : Con i}{j : Level}{A : Ty Γ j}{k : Level}{B : Ty (Γ ▷ A) k}(t : Tm (Γ ▷ A) B) → Tm Γ (Π A B)
lam t = record { ∣_∣t = λ γ α → ∣ t ∣t (γ ,Σ α) }

app : {i : Level}{Γ : Con i}{j : Level}{A : Ty Γ j}{k : Level}{B : Ty (Γ ▷ A) k}(t : Tm Γ (Π A B)) → Tm (Γ ▷ A) B
app t = record { ∣_∣t = λ { (γ ,Σ α) → ∣ t ∣t γ α } }

Πβ :  {i : Level}{Γ : Con i}{j : Level}{A : Ty Γ j}{k : Level}{B : Ty (Γ ▷ A) k}{t : Tm (Γ ▷ A) B} →
  app (lam t) ≡ t
Πβ = refl

Πη :  {i : Level}{Γ : Con i}{j : Level}{A : Ty Γ j}{k : Level}{B : Ty (Γ ▷ A) k}{t : Tm Γ (Π A B)} →
  lam (app t) ≡ t
Πη = refl

-- Coquand universe

U : {i : Level}{Γ : Con i}(j : Level) → Ty Γ (lsuc j)
U j = record { ∣_∣T = λ _ → Set j }

El : {i : Level}{Γ : Con i}{j : Level}(a : Tm Γ (U j)) → Ty Γ j
El a = record { ∣_∣T = λ γ → ∣ a ∣t γ }

c :  {i : Level}{Γ : Con i}{j : Level}(A : Ty Γ j) → Tm Γ (U j)
c A = record { ∣_∣t = λ γ → ∣ A ∣T γ }

Elc : {i : Level}{Γ : Con i}{j : Level}{A : Ty Γ j} → El (c A) ≡ A
Elc = refl

cEl : {i : Level}{Γ : Con i}{j : Level}{a : Tm Γ (U j)} → c (El a) ≡ a
cEl = refl
