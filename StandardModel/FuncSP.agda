{-# OPTIONS --without-K --no-eta #-}

module StandardModel.FuncSP where

open import StandardModel.Decl 
open import StandardModel.Core

open import lib

open import StandardModel.Base Set Lift

open import TT.FuncSP

fsp : FuncSP b
fsp = record
  { Π     = λ a B γ → (x : a γ) → B (γ ,Σ lift x)
  ; Π[]   = refl
  ; lam   = λ t γ → λ x → t (γ ,Σ lift x)
  ; app   = λ { f (γ ,Σ lift x) → f γ x }
  ; lam[] = refl
  ; Πβ    = refl
  ; Πη    = refl
  }
