module StandardModel.IRomega.Decl where

open import lib

Lvl : Set₁
Lvl = Σ ℕ (λ i → Set)

data UU (l : Lvl) : Set
EL : (l : Lvl) → UU l → Set

data UU l where
  ππ  : (a : UU l)(b : (x : EL l a) → UU l) → UU l
  σσ  : (a : UU l)(b : (x : EL l a) → UU l) → UU l
  uu : UU l

EL l (ππ a b) = (x : EL l a) → EL l (b x)
EL l (σσ a b) = Σ (EL l a) λ x → EL l (b x)
EL l uu = proj₂ l

U : (i : ℕ) → Set
U 0 = ⊥
U (suc i) = UU (i ,Σ U i)

El : {i : ℕ} → U i → Set
El {zero} ()
El {suc i} a = EL (i ,Σ U i) a

π : {i : ℕ}(a : U i)(b : El a → U i) → U i
π {zero} ()
π {suc i} a b = ππ a b

dup : {i : ℕ}(a : U i) → UU (i ,Σ U i)
dup {zero} ()
dup {suc i} a = {!a!}

-- Decl

Con : Set₁
Con = Set

Ty : (Γ : Con)(i : ℕ) → Set
Ty Γ i = Γ → U i

Tm : (Γ : Con){i : ℕ}(A : Ty Γ i) → Set
Tm Γ A = (γ : Γ) → El (A γ)

Tms : (Γ Δ : Con) → Set
Tms Γ Δ = Γ → Δ
