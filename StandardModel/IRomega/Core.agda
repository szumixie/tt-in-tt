module StandardModel.IRomega.Core where

open import lib

open import StandardModel.IRomega.Decl
  
infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t

• : Con  -- \bub
• = ⊤

_▷_ : (Γ : Con){i : ℕ}(A : Ty Γ i) → Con
Γ ▷ A = Σ Γ λ γ → El (A γ)

_[_]T : ∀{Γ Δ i} → Ty Δ i → Tms Γ Δ → Ty Γ i
A [ δ ]T = λ γ → A (δ γ)

id : ∀{Γ} → Tms Γ Γ
id γ = γ

_∘_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
δ ∘ ν = λ γ → δ (ν γ)

ε : ∀{Γ} → Tms Γ •
ε _ = tt

_,_  : ∀{Γ Δ i}(δ : Tms Γ Δ){A : Ty Δ i}(t : Tm Γ (A [ δ ]T)) → Tms Γ (Δ ▷ A)
δ , t = λ γ → δ γ ,Σ t γ

π₁ : ∀{Γ Δ i}{A : Ty Δ i}(δ : Tms Γ (Δ ▷ A)) → Tms Γ Δ
π₁ δ = λ γ → proj₁ (δ γ)

π₂ : ∀{Γ Δ i}{A : Ty Δ i}(δ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {i = i} δ ]T)
π₂ δ = λ γ → proj₂ (δ γ)

_[_]t : ∀{Γ Δ i}{A : Ty Δ i}(t : Tm Δ A)(δ : Tms Γ Δ) → Tm Γ (A [ δ ]T)
t [ δ ]t = λ γ → t (δ γ)

[id]T : ∀{Γ i}{A : Ty Γ i} → A [ id ]T ≡ A
[id]T = refl

[][]T : ∀{Γ Δ Σ i}{A : Ty Σ i}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)
[][]T = refl

idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ
idl = refl

idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ 
idr = refl

ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ} → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
ass = refl

,∘ : ∀{Γ Δ Σ i}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ i}{t : Tm Γ (A [ δ ]T)} →
  ((_,_ δ {A} t) ∘ σ) ≡ (_,_ (δ ∘ σ) {A} (_[_]t {i = i} t σ))
,∘ = refl

π₁β : ∀{Γ Δ i}{A : Ty Δ i}{δ : Tms Γ Δ}{t : Tm Γ (A [ δ ]T)} → π₁ {i = i}(_,_ δ {A} t) ≡ δ
π₁β = refl

π₂β : ∀{Γ Δ i}{A : Ty Δ i}{δ : Tms Γ Δ}{t : Tm Γ (A [ δ ]T)} → π₂ {i = i}(_,_ δ {A} t) ≡ t
π₂β = refl

πη : ∀{Γ Δ i}{A : Ty Δ i}{δ : Tms Γ (Δ ▷ A)} → _,_ (π₁ {i = i} δ) {A} (π₂ {i = i} δ) ≡ δ
πη = refl

εη : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε
εη = refl
