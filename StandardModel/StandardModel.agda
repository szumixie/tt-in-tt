{-# OPTIONS --no-eta --rewriting #-}

-- note : we rely on eta for Σ (in library.agda, eta is not turned on)

module StandardModel.StandardModel (⟦U⟧ : Set)(⟦El⟧ : ⟦U⟧ → Set) where

open import lib

-- the set model

open import StandardModel.Decl public
open import StandardModel.Core public
open import StandardModel.Func public
open import StandardModel.Base ⟦U⟧ ⟦El⟧ public

open import TT.Syntax

open import TT.Rec b f

-- interpretation into the set model

⟦_⟧C : Con → Set
⟦_⟧T : ∀{Γ} → Ty Γ → ⟦ Γ ⟧C → Set
⟦_⟧s : ∀{Γ Δ} → Tms Γ Δ → ⟦ Γ ⟧C → ⟦ Δ ⟧C
⟦_⟧t : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → (γ : ⟦ Γ ⟧C) → ⟦ A ⟧T γ

⟦_⟧C = RecCon
⟦_⟧T = RecTy
⟦_⟧s = RecTms
⟦_⟧t = RecTm

-- examples

open import TT.Examples

ex : Ty •
ex = U ⇒ U ⇒ U ⇒ U

⟦ex⟧ : ⟦ ex ⟧T ≡ λ _ → ⟦U⟧ → ⟦U⟧ → ⟦U⟧ → ⟦U⟧
⟦ex⟧  = refl

