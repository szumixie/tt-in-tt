{-# OPTIONS --without-K --no-eta #-}

module StandardModel.Restriction {i} where

open import StandardModel.Decl {i}
open import StandardModel.Core {i}

open import lib
open import TT.Restriction

r : Restriction c
r = record
  { Restr = λ {Γ} A {B} t u γ → Σ (A γ) λ α → t (γ ,Σ α) ≡ u (γ ,Σ α)
  ; mkRestr = λ t e γ → (t γ) ,Σ ap (λ z → z γ) e
  ; unRestr = λ w γ → proj₁ (w γ)
  ; eqRestr = λ w → funext λ γ → proj₂ (w γ)
  ; Restrβun = refl
  ; Restrβeq = fun-η _
  ; Restrη = λ {_}{_}{_}{_}{_}{w} → funext λ γ → ,Σ= refl (ap (λ z → z γ) (fun-β (λ γ → proj₂ (w γ))))
  ; Restr[] = refl
  }
