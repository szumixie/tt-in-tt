module StandardModel.UnitBad where

open import lib

open import StandardModel.Decl
open import StandardModel.Core

open import TT.One 

one : One c
one = record { Unit = λ {Γ} γ → Γ ; Unit[] = {!!} ; * = λ γ → γ ; *[] = {!refl!} }
