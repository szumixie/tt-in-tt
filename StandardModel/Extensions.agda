module StandardModel.Extensions (⟦U⟧ : Set)(⟦El⟧ : ⟦U⟧ → Set) where

open import lib

open import StandardModel.Decl
open import StandardModel.Core
open import StandardModel.Func
open import StandardModel.Base ⟦U⟧ ⟦El⟧

open import TT.Empty
open import TT.One 
open import TT.Sigma
open import TT.Boolean
open import TT.Natural
open import TT.Iden

open import TT.Decl
open import TT.Core

empty : Empty c
empty = record { Void = λ _ → ⊥ ; Void[] = refl ; abort = λ bot γ → ⊥-elim (bot γ) ; abort[] = refl }

one : One c
one = record { Unit = λ _ → ⊤ ; Unit[] = refl ; * = λ _ → tt ; *[] = refl }

bool : Boolean c
bool = record
  { Bool' = λ _ → Bool
  ; Bool[] = refl
  ; true' = λ _ → true
  ; false' = λ _ → false
  ; true[] = refl
  ; false[] = refl
  ; if'_then_else_ = λ {Γ}{C} b c d γ → if_then_else_ {C = λ b → C (γ ,Σ b)}(b γ)(c γ)(d γ)
  ; ifthenelse[] = refl
  ; trueβ = refl
  ; falseβ = refl
  }

natural : Natural c
natural = record
  { Nat = λ _ → ℕ
  ; Nat[] = refl
  ; zero' = λ _ → zero
  ; suc' = λ n γ → suc (n γ)
  ; ind' = λ P pz ps n γ → ind (λ n → P (γ ,Σ n)) (pz γ) (λ {n} pn → ps ((γ ,Σ n) ,Σ pn)) (n γ)
  ; zero[] = refl
  ; suc[] = refl
  ; ind[] = refl
  ; zeroβ = refl
  ; sucβ = refl
  }

open import Agda.Primitive

iden : Iden {lsuc lzero} c
iden = record
  { i1 = record
  { Id = λ a b γ → a γ ≡ b γ
  ; Id[] = refl
  ; ref = λ u γ → refl
  ; ref[] = refl
  } ; i2 = record
  { IdElim = λ {Γ}{A} a P pref {u} p γ → J {A = A γ}{a γ}(λ {u} p → P ((γ ,Σ u) ,Σ p)) (pref γ) (p γ) 
  ; IdElim[] = refl
  ; IdElimβ = refl
  } }

sigma : Sigma {lsuc lzero} c
sigma = record
  { Σ' = λ {Γ} A B γ → Σ (A γ) λ a → B (γ ,Σ a)
  ; Σ[] = refl
  ; _,Σ'_ = λ {Γ}{A}{B} a b γ → a γ ,Σ b γ
  ; proj₁' = λ {Γ}{A}{B} w γ → proj₁ (w γ)
  ; proj₂' = λ {Γ}{A}{B} w γ → proj₂ (w γ)
  ; Σβ₁ = refl
  ; Σβ₂ = refl
  ; Ση = refl
  ; ,Σ[] = refl
  }
