{-# OPTIONS --without-K --no-eta #-}

module StandardModel.Base {i}(⟦U⟧ : Set i)(⟦El⟧ : ⟦U⟧ → Set i) where

open import StandardModel.Decl {i}
open import StandardModel.Core {i}

open import lib
open import TT.Base

b : Base c
b = record
  { U     = λ _ → ⟦U⟧
  ; U[]   = refl
  ; El    = λ ⟦Â⟧ γ → ⟦El⟧ (⟦Â⟧ γ)
  ; El[]  = refl
  }
