{-# OPTIONS --without-K --no-eta #-}

module StandardModel.ExtIden {i} where

open import StandardModel.Decl {i}
open import StandardModel.Core {i}

open import lib
open import TT.ExtIden

iden : ExtIden c
iden = record
  { Id      = λ ⟦t⟧ ⟦u⟧ γ → ⟦t⟧ γ ≡ ⟦u⟧ γ
  ; Id[]    = refl
  ; ref     = λ ⟦u⟧ γ → refl
  ; ref[]   = refl
  ; reflect = funext
  }
