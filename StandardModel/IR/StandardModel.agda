{-# OPTIONS --without-K --no-eta #-}

module StandardModel.IR.StandardModel where

open import lib

open import StandardModel.IR.U0
open import StandardModel.IR.U1

open import TT.Decl

d : Decl
d = record
  { Con =             U1
  ; Ty  = λ ⟦Γ⟧     → El1 ⟦Γ⟧ → U1
  ; Tms = λ ⟦Γ⟧ ⟦Δ⟧ → El1 ⟦Γ⟧ → El1 ⟦Δ⟧
  ; Tm  = λ ⟦Γ⟧ ⟦A⟧ → (γ : El1 ⟦Γ⟧) → El1 (⟦A⟧ γ)
  }

open import TT.IsSet

set : IsSet d
set = record
  { setC = setU1
  ; setT = setCodomain setU1
  ; sets = λ {_}{Δ} → setCodomain (setEl1 {Δ})
  ; sett = λ {_}{A} → setCodomain (setEl1 {A _})
  }

open import TT.Core

c : Core d
c = record
  { c1 = record
  { •     =             ⊤1
  ; _,_   = λ ⟦Γ⟧ ⟦A⟧ → Σ1 ⟦Γ⟧ ⟦A⟧
  ; _[_]T = λ ⟦A⟧ ⟦δ⟧ γ → ⟦A⟧ (⟦δ⟧ γ)
  ; id    = λ         γ → γ
  ; _∘_   = λ ⟦δ⟧ ⟦σ⟧ γ → ⟦δ⟧ (⟦σ⟧ γ)
  ; ε     = λ         _ → tt
  ; _,s_  = λ ⟦δ⟧ ⟦t⟧ γ → ⟦δ⟧ γ ,Σ ⟦t⟧ γ
  ; π₁    = λ ⟦δ⟧     γ → (proj₁ (⟦δ⟧ γ))
  ; _[_]t = λ ⟦t⟧ ⟦δ⟧ γ → ⟦t⟧ (⟦δ⟧ γ)
  ; π₂    = λ ⟦δ⟧     γ → (proj₂ (⟦δ⟧ γ))
  } ; c2 = record
  { [id]T = refl
  ; [][]T = refl
  ; idl   = refl
  ; idr   = refl
  ; ass   = refl
  ; ,∘    = refl
  ; π₁β   = refl
  ; πη    = refl
  ; εη    = refl
  ; π₂β   = refl
  } }

open import TT.Base

b : Base c
b = record
  { U    = λ _ → u0
  ; U[]  = refl
  ; El   = λ ⟦Â⟧ γ → el0 (⟦Â⟧ γ)
  ; El[] = refl
  }

open import TT.FuncU

fU : FuncU b
fU = record
  { Π     = λ ⟦A⟧ ⟦B⟧ γ → Π0 (⟦A⟧ γ) (λ x → ⟦B⟧ (γ ,Σ x))
  ; Π[]   = refl
  ; lam   = λ ⟦t⟧ γ → λ a → ⟦t⟧ (γ ,Σ a)
  ; app   = λ ⟦t⟧ γ → ⟦t⟧ (proj₁ γ) (proj₂ γ)
  ; lam[] = refl
  ; Πβ    = refl
  ; Πη    = refl
  }

open import TT.Func

f : Func c
f = record
  { Π     = λ ⟦A⟧ ⟦B⟧ γ → Π1 (⟦A⟧ γ) (λ x → ⟦B⟧ (γ ,Σ x))
  ; Π[]   = refl
  ; lam   = λ ⟦t⟧ γ → λ a → ⟦t⟧ (γ ,Σ a)
  ; app   = λ ⟦t⟧ γ → ⟦t⟧ (proj₁ γ) (proj₂ γ)
  ; lam[] = refl
  ; Πβ    = refl
  ; Πη    = refl
  }

-- Sigma is defined with K
{-
open import TT.Sigma

sig : Sigma c
sig = record
  { Σ'     = λ ⟦A⟧ ⟦B⟧ γ → Σ1 (⟦A⟧ γ) (λ x → ⟦B⟧ (γ ,Σ x))
  ; Σ[]    = refl
  ; _,Σ'_  = λ ⟦t⟧ ⟦u⟧ γ → ⟦t⟧ γ ,Σ ⟦u⟧ γ
  ; proj₁' = λ ⟦t⟧ γ → proj₁ (⟦t⟧ γ)
  ; proj₂' = λ ⟦t⟧ γ → proj₂ (⟦t⟧ γ)
  ; Σβ₁    = refl
  ; Σβ₂    = refl
  ; Ση     = refl
  ; ,Σ[]   = refl
  }
-}
