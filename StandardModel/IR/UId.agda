{-# OPTIONS --without-K --no-eta #-}

module StandardModel.IR.UId where

open import lib

------------------------------------------------------------------------------
-- an inductive-recursive universe
------------------------------------------------------------------------------

data U : Set
El : U → Set

data U where
  Πc  : (a : U)(b : El a → U) → U -- \'
  Σc  : (a : U)(b : El a → U) → U
  ⊤c  : U
  =c  : (a : U) → El a → El a → U

El (Πc a b) = (x : El a) → El (b x)
El (Σc a b) = Σ (El a) λ x → El (b x)
El ⊤c = ⊤
El (=c a t u) = t ≡ u

------------------------------------------------------------------------------
-- equality for Πc
------------------------------------------------------------------------------

Πc= : {A A' : U}{B : El A → U}{B' : El A' → U}
    → (w : A ≡ A') → B ≡[ ap (λ A → El A → U) w ]≡ B' → Πc A B ≡ Πc A' B'
Πc= refl refl = refl

Πc=0 : {A A' : U}{B : El A → U}{B' : El A' → U} → Πc A B ≡ Πc A' B' → A ≡ A'
Πc=0 refl = refl

Πc=1 : {A A' : U}{B : El A → U}{B' : El A' → U}(w : Πc A B ≡ Πc A' B')
    → B ≡[ ap (λ A → El A → U) (Πc=0 w) ]≡ B'
Πc=1 refl = refl

Πc=η : {A A' : U}{B : El A → U}{B' : El A' → U}(α : Πc A B ≡ Πc A' B')
     → α ≡ Πc= (Πc=0 α) (Πc=1 α)
Πc=η refl = refl

Πc=β0 : {A A' : U}{B : El A → U}{B' : El A' → U}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El X → U) α ]≡ B')
      → Πc=0 (Πc= α β) ≡ α
Πc=β0 refl refl = refl

Πc=β1 : {A A' : U}{B : El A → U}{B' : El A' → U}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El X → U) α ]≡ B')
      → Πc=1 (Πc= α β) ≡[ ap (λ γ → B ≡[ ap (λ X → El X → U) γ ]≡ B') (Πc=β0 α β) ]≡ β
Πc=β1 refl refl = refl

Πc=' : {A : U}{B : El A → U}{α : A ≡ A}{β : B ≡[ ap (λ X → El X → U) α ]≡ B}
     → (w : α ≡ refl) → β ≡[ ap (λ γ → B ≡[ ap (λ X → El X → U) γ ]≡ B) w ]≡ refl
     → Πc= {A}{A}{B}{B} α β ≡ refl
Πc=' refl refl = refl

------------------------------------------------------------------------------
-- equality for Σc
------------------------------------------------------------------------------

Σc= : {A A' : U}{B : El A → U}{B' : El A' → U}
    → (w : A ≡ A') → B ≡[ ap (λ A → El A → U) w ]≡ B' → Σc A B ≡ Σc A' B'
Σc= refl refl = refl

Σc=0 : {A A' : U}{B : El A → U}{B' : El A' → U} → Σc A B ≡ Σc A' B' → A ≡ A'
Σc=0 refl = refl

Σc=1 : {A A' : U}{B : El A → U}{B' : El A' → U}(w : Σc A B ≡ Σc A' B')
     → B ≡[ ap (λ A → El A → U) (Σc=0 w) ]≡ B'
Σc=1 refl = refl

Σc=η : {A A' : U}{B : El A → U}{B' : El A' → U}(α : Σc A B ≡ Σc A' B')
     → α ≡ Σc= (Σc=0 α) (Σc=1 α)
Σc=η refl = refl

Σc=β0 : {A A' : U}{B : El A → U}{B' : El A' → U}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El X → U) α ]≡ B')
      → Σc=0 (Σc= α β) ≡ α
Σc=β0 refl refl = refl

Σc=β1 : {A A' : U}{B : El A → U}{B' : El A' → U}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El X → U) α ]≡ B')
      → Σc=1 (Σc= α β) ≡[ ap (λ γ → B ≡[ ap (λ X → El X → U) γ ]≡ B') (Σc=β0 α β) ]≡ β
Σc=β1 refl refl = refl

Σc=' : {A : U}{B : El A → U}{α : A ≡ A}{β : B ≡[ ap (λ X → El X → U) α ]≡ B}
     → (w : α ≡ refl) → β ≡[ ap (λ γ → B ≡[ ap (λ X → El X → U) γ ]≡ B) w ]≡ refl
     → Σc= {A}{A}{B}{B} α β ≡ refl
Σc=' refl refl = refl

------------------------------------------------------------------------------
-- equality for =c
------------------------------------------------------------------------------

=c= : {a a' : U}{x y : El a}{x' y' : El a'}
  (a= : a ≡ a')(x= : x ≡[ ap El a= ]≡ x')(y= : y ≡[ ap El a= ]≡ y') →
  =c a x y ≡ =c a' x' y'
=c= refl refl refl = refl

=c=0 : {a a' : U}{x y : El a}{x' y' : El a'} → =c a x y ≡ =c a' x' y' → a ≡ a'
=c=0 refl = refl

=c=1 : {a a' : U}{x y : El a}{x' y' : El a'}(p : =c a x y ≡ =c a' x' y') → x ≡[ ap El (=c=0 p) ]≡ x'
=c=1 refl = refl

=c=2 : {a a' : U}{x y : El a}{x' y' : El a'}(p : =c a x y ≡ =c a' x' y') → y ≡[ ap El (=c=0 p) ]≡ y'
=c=2 refl = refl

=c=η : {a a' : U}{x y : El a}{x' y' : El a'}(p : =c a x y ≡ =c a' x' y') → p ≡ =c= (=c=0 p) (=c=1 p) (=c=2 p)
=c=η refl = refl

=c=' : {a : U}{x y : El a}{a= : a ≡ a}{x= : x ≡[ ap El a= ]≡ x}{y= : y ≡[ ap El a= ]≡ y}
  (a== : a= ≡ refl)
  (x== : x= ≡[ ap (λ z → x ≡[ ap El z ]≡ x) a== ]≡ refl)
  (y== : y= ≡[ ap (λ z → y ≡[ ap El z ]≡ y) a== ]≡ refl) →
  =c= a= x= y= ≡ refl
=c=' refl refl refl = refl

------------------------------------------------------------------------------
-- El a is a set
------------------------------------------------------------------------------

loop→line : {A : Set} → ({a : A}{p : a ≡ a} → p ≡ refl) → {a a' : A}(p q : a ≡ a') → p ≡ q
loop→line {A} w p refl = w {p = p}

setEl' : {A : U}{a : El A}{α : a ≡ a} → α ≡ refl
setEl' {Πc A B}{f}{α} = fun-2 (λ x p → setEl' {B x}{f x}{p}) α
setEl' {Σc A B}{(a ,Σ b)}{α}
  = (,Σ=η α ⁻¹) ◾
    (,Σ=2 (setEl' {A}{a}{,Σ=0 α})
          (setEl' {B a}
                   {b}
                   {coe (ap (λ r → b ≡[ ap (λ x → El (B x)) r ]≡ b)
                            (setEl' {A}{a}))
                        (,Σ=1 α)}))
setEl' {⊤c} {tt} {refl} = refl
setEl' {=c a x y}{p}{α} = set2h1 (loop→line setEl') α refl

setEl : {A : U}{a a' : El A}{α β : a ≡ a'} → α ≡ β
setEl {α = α}{β} = loop→line setEl' α β

------------------------------------------------------------------------------
-- U is a set
------------------------------------------------------------------------------

setU' : {A : U}{α : A ≡ A} → α ≡ refl
setU' {Πc A B}{α} = Πc=η α ◾
                      (Πc=' (setU' {A} {Πc=0 α})
                            (fun-2 (λ x p → setU' {B x} {p}) _))
setU' {Σc A B}{α} = Σc=η α ◾
                      (Σc=' (setU' {A} {Σc=0 α})
                            (fun-2 (λ x p → setU' {B x} {p}) _))
setU' {⊤c} {refl} = refl
setU' {=c a x y}{α} = =c=η α ◾ =c=' (setU' {a}{=c=0 α}) setEl' setEl'

setU : {A B : U}{α β : A ≡ B} → α ≡ β
setU {α = α}{β = refl} = setU' {α = α}
