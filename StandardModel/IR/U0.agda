{-# OPTIONS --without-K --no-eta #-}

module StandardModel.IR.U0 where

open import lib

------------------------------------------------------------------------------
-- an inductive-recursive universe
------------------------------------------------------------------------------

data U0 : Set
El0 : U0 → Set

data U0 where
  Π0  : (a : U0)(b : El0 a → U0) → U0 -- \'
  Σ0  : (a : U0)(b : El0 a → U0) → U0
  ⊤0  : U0

El0 (Π0 a b) = (x : El0 a) → El0 (b x)
El0 (Σ0 a b) = Σ (El0 a) λ x → El0 (b x)
El0 ⊤0 = ⊤

------------------------------------------------------------------------------
-- equality for Π0
------------------------------------------------------------------------------

Π0= : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0}
    → (w : A ≡ A') → B ≡[ ap (λ A → El0 A → U0) w ]≡ B' → Π0 A B ≡ Π0 A' B'
Π0= refl refl = refl

Π0=0 : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0} → Π0 A B ≡ Π0 A' B' → A ≡ A'
Π0=0 refl = refl

Π0=1 : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0}(w : Π0 A B ≡ Π0 A' B')
    → B ≡[ ap (λ A → El0 A → U0) (Π0=0 w) ]≡ B'
Π0=1 refl = refl

Π0=η : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0}(α : Π0 A B ≡ Π0 A' B')
     → α ≡ Π0= (Π0=0 α) (Π0=1 α)
Π0=η refl = refl

Π0=β0 : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El0 X → U0) α ]≡ B')
      → Π0=0 (Π0= α β) ≡ α
Π0=β0 refl refl = refl

Π0=β1 : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El0 X → U0) α ]≡ B')
      → Π0=1 (Π0= α β) ≡[ ap (λ γ → B ≡[ ap (λ X → El0 X → U0) γ ]≡ B') (Π0=β0 α β) ]≡ β
Π0=β1 refl refl = refl

Π0=2 : {A : U0}{B : El0 A → U0}{α : A ≡ A}{β : B ≡[ ap (λ X → El0 X → U0) α ]≡ B}
     → (w : α ≡ refl) → β ≡[ ap (λ γ → B ≡[ ap (λ X → El0 X → U0) γ ]≡ B) w ]≡ refl
     → Π0= {A}{A}{B}{B} α β ≡ refl
Π0=2 refl refl = refl

------------------------------------------------------------------------------
-- equality for Σ0
------------------------------------------------------------------------------

Σ0= : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0}
    → (w : A ≡ A') → B ≡[ ap (λ A → El0 A → U0) w ]≡ B' → Σ0 A B ≡ Σ0 A' B'
Σ0= refl refl = refl

Σ0=0 : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0} → Σ0 A B ≡ Σ0 A' B' → A ≡ A'
Σ0=0 refl = refl

Σ0=1 : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0}(w : Σ0 A B ≡ Σ0 A' B')
     → B ≡[ ap (λ A → El0 A → U0) (Σ0=0 w) ]≡ B'
Σ0=1 refl = refl

Σ0=η : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0}(α : Σ0 A B ≡ Σ0 A' B')
     → α ≡ Σ0= (Σ0=0 α) (Σ0=1 α)
Σ0=η refl = refl

Σ0=β0 : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El0 X → U0) α ]≡ B')
      → Σ0=0 (Σ0= α β) ≡ α
Σ0=β0 refl refl = refl

Σ0=β1 : {A A' : U0}{B : El0 A → U0}{B' : El0 A' → U0}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El0 X → U0) α ]≡ B')
      → Σ0=1 (Σ0= α β) ≡[ ap (λ γ → B ≡[ ap (λ X → El0 X → U0) γ ]≡ B') (Σ0=β0 α β) ]≡ β
Σ0=β1 refl refl = refl

Σ0=2 : {A : U0}{B : El0 A → U0}{α : A ≡ A}{β : B ≡[ ap (λ X → El0 X → U0) α ]≡ B}
     → (w : α ≡ refl) → β ≡[ ap (λ γ → B ≡[ ap (λ X → El0 X → U0) γ ]≡ B) w ]≡ refl
     → Σ0= {A}{A}{B}{B} α β ≡ refl
Σ0=2 refl refl = refl

------------------------------------------------------------------------------
-- U0 is a set
------------------------------------------------------------------------------

setU0' : {A : U0}{α : A ≡ A} → α ≡ refl
setU0' {Π0 A B}{α} = Π0=η α ◾
                      (Π0=2 (setU0' {A} {Π0=0 α})
                             (fun-2 (λ x p → setU0' {B x} {p}) _))
setU0' {Σ0 A B}{α} = Σ0=η α ◾
                      (Σ0=2 (setU0' {A} {Σ0=0 α})
                             (fun-2 (λ x p → setU0' {B x} {p}) _))
setU0' {⊤0} {refl} = refl

setU0 : {A B : U0}{α β : A ≡ B} → α ≡ β
setU0 {α = α}{β = refl} = setU0' {α = α}

------------------------------------------------------------------------------
-- El0 a is a set
------------------------------------------------------------------------------

setEl0' : {A : U0}{a : El0 A}{α : a ≡ a} → α ≡ refl
setEl0' {Π0 A B}{f}{α} = fun-2 (λ x p → setEl0' {B x}{f x}{p}) α
setEl0' {Σ0 A B}{(a ,Σ b)}{α}
  = (,Σ=η α ⁻¹) ◾
    (,Σ=2 (setEl0' {A}{a}{,Σ=0 α})
          (setEl0' {B a}
                   {b}
                   {coe (ap (λ r → b ≡[ ap (λ x → El0 (B x)) r ]≡ b)
                            (setEl0' {A}{a}))
                        (,Σ=1 α)}))
setEl0' {⊤0} {tt} {refl} = refl

setEl0 : {A : U0}{a a' : El0 A}{α β : a ≡ a'} → α ≡ β
setEl0 {α = α}{β = refl} = setEl0' {α = α}
