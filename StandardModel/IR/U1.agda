{-# OPTIONS --without-K --no-eta #-}

module StandardModel.IR.U1 where

open import lib

open import StandardModel.IR.U0

------------------------------------------------------------------------------
-- a big inductive-recursive universe
------------------------------------------------------------------------------

data U1 : Set
El1 : U1 → Set

data U1 where
  Π1  : (a : U1)(b : El1 a → U1) → U1 -- \'
  Σ1  : (a : U1)(b : El1 a → U1) → U1
  ⊤1  : U1
  u0  : U1
  el0 : El1 u0 → U1

El1 (Π1 a b) =   (x : El1 a) → El1 (b x)
El1 (Σ1 a b) = Σ (El1 a) λ x → El1 (b x)
El1 ⊤1       = ⊤
El1 u0       = U0
El1 (el0 a)  = El0 a

------------------------------------------------------------------------------
-- equality for Π1
------------------------------------------------------------------------------

Π1= : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1}
    → (w : A ≡ A') → B ≡[ ap (λ A → El1 A → U1) w ]≡ B' → Π1 A B ≡ Π1 A' B'
Π1= refl refl = refl

Π1=0 : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1} → Π1 A B ≡ Π1 A' B' → A ≡ A'
Π1=0 refl = refl

Π1=1 : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1}(w : Π1 A B ≡ Π1 A' B')
    → B ≡[ ap (λ A → El1 A → U1) (Π1=0 w) ]≡ B'
Π1=1 refl = refl

Π1=η : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1}(α : Π1 A B ≡ Π1 A' B')
     → α ≡ Π1= (Π1=0 α) (Π1=1 α)
Π1=η refl = refl

Π1=β0 : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El1 X → U1) α ]≡ B')
      → Π1=0 (Π1= α β) ≡ α
Π1=β0 refl refl = refl

Π1=β1 : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El1 X → U1) α ]≡ B')
      → Π1=1 (Π1= α β) ≡[ ap (λ γ → B ≡[ ap (λ X → El1 X → U1) γ ]≡ B') (Π1=β0 α β) ]≡ β
Π1=β1 refl refl = refl

Π1=2 : {A : U1}{B : El1 A → U1}{α : A ≡ A}{β : B ≡[ ap (λ X → El1 X → U1) α ]≡ B}
     → (w : α ≡ refl) → β ≡[ ap (λ γ → B ≡[ ap (λ X → El1 X → U1) γ ]≡ B) w ]≡ refl
     → Π1= {A}{A}{B}{B} α β ≡ refl
Π1=2 refl refl = refl

------------------------------------------------------------------------------
-- equality for Σ1
------------------------------------------------------------------------------

Σ1= : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1}
    → (w : A ≡ A') → B ≡[ ap (λ A → El1 A → U1) w ]≡ B' → Σ1 A B ≡ Σ1 A' B'
Σ1= refl refl = refl

Σ1=0 : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1} → Σ1 A B ≡ Σ1 A' B' → A ≡ A'
Σ1=0 refl = refl

Σ1=1 : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1}(w : Σ1 A B ≡ Σ1 A' B')
     → B ≡[ ap (λ A → El1 A → U1) (Σ1=0 w) ]≡ B'
Σ1=1 refl = refl

Σ1=η : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1}(α : Σ1 A B ≡ Σ1 A' B')
     → α ≡ Σ1= (Σ1=0 α) (Σ1=1 α)
Σ1=η refl = refl

Σ1=β0 : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El1 X → U1) α ]≡ B')
      → Σ1=0 (Σ1= α β) ≡ α
Σ1=β0 refl refl = refl

Σ1=β1 : {A A' : U1}{B : El1 A → U1}{B' : El1 A' → U1}
        (α : A ≡ A')(β : B ≡[ ap (λ X → El1 X → U1) α ]≡ B')
      → Σ1=1 (Σ1= α β) ≡[ ap (λ γ → B ≡[ ap (λ X → El1 X → U1) γ ]≡ B') (Σ1=β0 α β) ]≡ β
Σ1=β1 refl refl = refl

Σ1=2 : {A : U1}{B : El1 A → U1}{α : A ≡ A}{β : B ≡[ ap (λ X → El1 X → U1) α ]≡ B}
     → (w : α ≡ refl) → β ≡[ ap (λ γ → B ≡[ ap (λ X → El1 X → U1) γ ]≡ B) w ]≡ refl
     → Σ1= {A}{A}{B}{B} α β ≡ refl
Σ1=2 refl refl = refl

------------------------------------------------------------------------------
-- equality for el0
------------------------------------------------------------------------------

el0= : {a a' : U0}(α : a ≡ a') → el0 a ≡ el0 a'
el0= refl = refl

el0=0 : {a a' : U0} → el0 a ≡ el0 a' → a ≡ a'
el0=0 refl = refl

el0=η : {a a' : U0}(α : el0 a ≡ el0 a') → α ≡ el0= (el0=0 α)
el0=η refl = refl

el0=β : {a a' : U0}(α : a ≡ a') → el0=0 (el0= α) ≡ α
el0=β refl = refl

------------------------------------------------------------------------------
-- U1 is a set
------------------------------------------------------------------------------

setU1' : {A : U1}{α : A ≡ A} → α ≡ refl
setU1' {Π1 A B}{α} = Π1=η α ◾
                      (Π1=2 (setU1' {A} {Π1=0 α})
                            (fun-2 (λ x p → setU1' {B x} {p}) _))
setU1' {Σ1 A B}{α} = Σ1=η α ◾
                      (Σ1=2 (setU1' {A} {Σ1=0 α})
                            (fun-2 (λ x p → setU1' {B x} {p}) _))
setU1' {⊤1} {refl} = refl
setU1' {u0} {refl} = refl
setU1' {el0 x} {α} = el0=η α ◾ ap el0= (setU0' {_}{el0=0 α})

setU1 : {A B : U1}{α β : A ≡ B} → α ≡ β
setU1 {α = α}{β = refl} = setU1' {α = α}

------------------------------------------------------------------------------
-- El1 a is a set
------------------------------------------------------------------------------

setEl1' : {A : U1}{a : El1 A}{α : a ≡ a} → α ≡ refl
setEl1' {Π1 A B}{f}{α} = fun-2 (λ x p → setEl1' {B x}{f x}{p}) α
setEl1' {Σ1 A B}{(a ,Σ b)}{α}
  = (,Σ=η α ⁻¹) ◾
    (,Σ=2 (setEl1' {A}{a}{,Σ=0 α})
          (setEl1' {B a}
                   {b}
                   {coe (ap (λ r → b ≡[ ap (λ x → El1 (B x)) r ]≡ b)
                            (setEl1' {A}{a}))
                        (,Σ=1 α)}))
setEl1' {⊤1}{tt}{refl} = refl
setEl1' {u0}{a}{α} = setU0' {a}{α}
setEl1' {el0 a}{x}{α} = setEl0' {a}{x}{α}

setEl1 : {A : U1}{a a' : El1 A}{α β : a ≡ a'} → α ≡ β
setEl1 {A}{α = α}{β = refl} = setEl1' {A}{α = α}
