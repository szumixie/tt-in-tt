{-# OPTIONS --without-K --no-eta #-}

module StandardModel.IR.ExtIden where

open import lib

open import StandardModel.IR.UId

open import TT.Decl

d : Decl
d = record
  { Con =             U
  ; Ty  = λ ⟦Γ⟧     → El ⟦Γ⟧ → U
  ; Tms = λ ⟦Γ⟧ ⟦Δ⟧ → El ⟦Γ⟧ → El ⟦Δ⟧
  ; Tm  = λ ⟦Γ⟧ ⟦A⟧ → (γ : El ⟦Γ⟧) → El (⟦A⟧ γ)
  }

open import TT.IsSet

set : IsSet d
set = record
  { setC = setU
  ; setT = setCodomain setU
  ; sets = λ {_}{Δ} → setCodomain (setEl {Δ})
  ; sett = λ {_}{A} → setCodomain (setEl {A _})
  }

open import TT.Core

c : Core d
c = record
  { c1 = record
  { •     =             ⊤c
  ; _,_   = λ ⟦Γ⟧ ⟦A⟧ → Σc ⟦Γ⟧ ⟦A⟧
  ; _[_]T = λ ⟦A⟧ ⟦δ⟧ γ → ⟦A⟧ (⟦δ⟧ γ)
  ; id    = λ         γ → γ
  ; _∘_   = λ ⟦δ⟧ ⟦σ⟧ γ → ⟦δ⟧ (⟦σ⟧ γ)
  ; ε     = λ         _ → tt
  ; _,s_  = λ ⟦δ⟧ ⟦t⟧ γ → ⟦δ⟧ γ ,Σ ⟦t⟧ γ
  ; π₁    = λ ⟦δ⟧     γ → (proj₁ (⟦δ⟧ γ))
  ; _[_]t = λ ⟦t⟧ ⟦δ⟧ γ → ⟦t⟧ (⟦δ⟧ γ)
  ; π₂    = λ ⟦δ⟧     γ → (proj₂ (⟦δ⟧ γ))
  } ; c2 = record
  { [id]T = refl
  ; [][]T = refl
  ; idl   = refl
  ; idr   = refl
  ; ass   = refl
  ; ,∘    = refl
  ; π₁β   = refl
  ; πη    = refl
  ; εη    = refl
  ; π₂β   = refl
  } }

open import TT.Func

f : Func c
f = record
  { Π     = λ ⟦A⟧ ⟦B⟧ γ → Πc (⟦A⟧ γ) (λ x → ⟦B⟧ (γ ,Σ x))
  ; Π[]   = refl
  ; lam   = λ ⟦t⟧ γ → λ a → ⟦t⟧ (γ ,Σ a)
  ; app   = λ ⟦t⟧ γ → ⟦t⟧ (proj₁ γ) (proj₂ γ)
  ; lam[] = refl
  ; Πβ    = refl
  ; Πη    = refl
  }

open import TT.Sigma

sig : Sigma c
sig = record
  { Σ'     = λ ⟦A⟧ ⟦B⟧ γ → Σc (⟦A⟧ γ) (λ x → ⟦B⟧ (γ ,Σ x))
  ; Σ[]    = refl
  ; _,Σ'_  = λ ⟦t⟧ ⟦u⟧ γ → ⟦t⟧ γ ,Σ ⟦u⟧ γ
  ; proj₁' = λ ⟦t⟧ γ → proj₁ (⟦t⟧ γ)
  ; proj₂' = λ ⟦t⟧ γ → proj₂ (⟦t⟧ γ)
  ; Σβ₁    = refl
  ; Σβ₂    = refl
  ; Ση     = refl
  ; ,Σ[]   = refl
  }

open import TT.ExtIden

iden : ExtIden c
iden = record
  { Id      = λ {_}{⟦a⟧} ⟦t⟧ ⟦u⟧ γ → =c (⟦a⟧ γ) (⟦t⟧ γ) (⟦u⟧ γ)
  ; Id[]    = refl
  ; ref     = λ u γ → refl
  ; ref[]   = refl
  ; reflect = funext
  }
