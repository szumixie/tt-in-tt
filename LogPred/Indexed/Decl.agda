{-# OPTIONS --rewriting #-}

module LogPred.Indexed.Decl where

open import lib
open import TT.Syntax
open import TT.DepModel

{-# REWRITE [][]T #-}

record Conᴾ (Γ : Con) : Set where
  field
    _C  : {Ω : Con}(ρ : Tms Ω Γ) → Ty Ω
    _[]C : {Ω : Con}{ρ : Tms Ω Γ}{Ψ : Con}{τ : Tms Ψ Ω} → _C ρ [ τ ]T ≡ _C (ρ ∘ τ)
open Conᴾ public

record Tyᴾ {Γ : Con}(Γᴾ : Conᴾ Γ)(A : Ty Γ) : Set where
  field
    _T   : {Ω : Con}{ρ : Tms Ω Γ}(ρ̂ : Tm Ω ((Γᴾ C) ρ))(t : Tm Ω (A [ ρ ]T)) → Ty Ω
    _[]T : {Ω : Con}{ρ : Tms Ω Γ}{ρ̂ : Tm Ω ((Γᴾ C) ρ)}{t : Tm Ω (A [ ρ ]T)}{Ψ : Con}{τ : Tms Ψ Ω} →
      _T ρ̂ t [ τ ]T ≡ _T {Ψ}{ρ ∘ τ} (transport (Tm Ψ) (Γᴾ []C) (ρ̂ [ τ ]t)) (t [ τ ]t)

record Tmsᴾ {Γ Δ : Con}(Γᴾ : Conᴾ Γ)(Δᴾ : Conᴾ Δ)(ρ : Tms Γ Δ) : Set where
  field
    ∣_∣s    : {!!}

  infix 7 ∣_∣s

open Tmsᴾ public

Tmᴾ : {Γ : Con}(Γᴾ : Conᴾ Γ){A : Ty Γ}(Aᴾ : Tyᴾ Γᴾ A) → Tm Γ A → Set
Tmᴾ {Γ} Γᴾ {A} Aᴾ a = {!!}

d : Declᴹ
d = record
  { Conᴹ = Conᴾ
  ; Tyᴹ  = Tyᴾ
  ; Tmsᴹ = Tmsᴾ
  ; Tmᴹ  = Tmᴾ
  }
