{-# OPTIONS --no-eta #-}

module LogPred.SP.U where

open import lib
open import TT.Decl.Syntax
open import TT.Decl.Congr syntaxDecl
open import TT.Core.Syntax
open import TT.Core.Congr syntaxCore
open import TT.Base.Syntax
open import TT.Base.Congr syntaxBase
open import TT.FuncSP.Syntax

open import JM
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

Uᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ} → Ty (∣ Γᴹ ∣C , U [ Pr Γᴹ ]T)
Uᴹ {Γ}{Γᴹ} = Π (coe (TmΓ= ([][]T ◾ U[])) vz) U

module Elᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ} {a : Tm Γ U}
  (aᴹ : Tm ∣ Γᴹ ∣C (Π (coe (TmΓ= ([][]T ◾ U[])) vz) U [ < a [ Pr Γᴹ ]t > ]T))
  where

    abstract
      p : El (coe (TmΓ= U[]) (a [ Pr Γᴹ ]t)) [ wk ]T
        ≡[ Ty= (Γ,C= (El[] {σ = Pr Γᴹ}{Â = a} ⁻¹)) ]≡
          El (coe (TmΓ= U[])
                  (coe (TmΓ= ([][]T ◾ U[])) vz [ < a [ Pr Γᴹ ]t > ∘ wk ]t))
      p = from≃ ( uncoe (Ty= (Γ,C= (El[] ⁻¹))) ⁻¹̃
                ◾̃ to≃ El[]
                ◾̃ El≃ (Γ,C= (El[] ⁻¹))
                      ( uncoe (TmΓ= U[]) ⁻¹̃
                      ◾̃ coe[]t' U[]
                      ◾̃ []t≃ (Γ,C= (El[] ⁻¹)) (π₁id≃ refl (to≃ (El[] ⁻¹)))
                      ◾̃ coe[]t' ([id]T ⁻¹) ⁻¹̃
                      ◾̃ uncoe (TmΓ= [][]T)
                      ◾̃ π₂idβ ⁻¹̃
                      ◾̃ []t≃ refl (to≃ ,∘) ⁻¹̃
                      ◾̃ coe[]t' ([][]T ◾ U[]) ⁻¹̃
                      ◾̃ uncoe (TmΓ= U[])))

    abstract
      q : coe (Ty= (Γ,C= El[]))
              (U [ < a [ Pr Γᴹ ]t > ∘ wk ^ El (coe (TmΓ= ([][]T ◾ U[])) vz) ]T) [ < coe (Tm= (Γ,C= (El[] ⁻¹)) p) vz > ]T
        ≡ U
      q = coe[]T (Γ,C= El[]) ◾ [][]T ◾ U[]

    ret : Ty (∣ Γᴹ ∣C , El a [ Pr Γᴹ ]T)
    ret = El (coe (TmΓ= q)
             ( coe (TmΓ= ([][]T ◾ Π[])) (aᴹ [ wk ]t)
             $ coe (Tm= (Γ,C= (El[] ⁻¹)) p) vz))

b : Baseᴹ c
b = record
  { Uᴹ    = λ {Γ}{Γᴹ} → Uᴹ {Γ}{Γᴹ}
  ; Elᴹ   = λ {Γ}{Γᴹ}{a} aᴹ → Elᴹ.ret {Γ}{Γᴹ}{a} aᴹ
  ; U[]ᴹ  = {!!}
  ; El[]ᴹ = {!!}
  }
