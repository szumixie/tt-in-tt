{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func.Proofs1 where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

abstract
  pΠᴹ : ∀{Γ}(Γᴹ : Conᴹ Γ){A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}
      → B [ Pr Γᴹ ^ A ]T [ π₁ {A = Π A B [ Pr Γᴹ ]T} id ^ A [ Pr Γᴹ ]T ]T [ π₁ id ]T
      ≡ B [ Pr (Γᴹ ,Cᴹ Aᴹ) ]T [ π₁ id ^ A [ Pr Γᴹ ]T ^ Aᴹ ]T
  pΠᴹ {Γ} Γᴹ {A}{Aᴹ}{B}

    = [][]T
    ◾ [][]T
    ◾ ap (_[_]T B)
         (ap (_∘_ (Pr Γᴹ ^ A)) ∘π₁id ◾ ass ⁻¹)
    ◾ [][]T ⁻¹

abstract
  pΠᴹ' : ∀{Γ}(Γᴹ : Conᴹ Γ){A : Ty Γ}{B : Ty (Γ , A)}
       → Π A B [ Pr Γᴹ ]T [ π₁ {A = Π A B [ Pr Γᴹ ]T} id ]T
       ≡ Π (A [ Pr Γᴹ ]T [ π₁ id ]T)
           (B [ Pr Γᴹ ^ A ]T [ π₁ id ^ A [ Pr Γᴹ ]T ]T)
  pΠᴹ' Γᴹ = ap (λ z → z [ π₁ id ]T) Π[] ◾ Π[]

module eqΓABᴹ {Γ : Con}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}
              {Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
              {t : Tm Γ (Π A B)} where

  abstract
    eqAPr : A [ Pr Γᴹ ]T [ π₁ id ]T [ < t [ Pr Γᴹ ]t > ]T
          ≃ A [ Pr Γᴹ ]T
    eqAPr = to≃ ( [][]T ◾ ap (_[_]T _) π₁idβ ◾ [id]T)

  abstract
    eqΓAPr : _≡_ {A = Con}
                 (∣ Γᴹ ∣C , A [ Pr Γᴹ ]T [ π₁ id ]T [ < t [ Pr Γᴹ ]t > ]T)
                 (∣ Γᴹ ∣C , A [ Pr Γᴹ ]T)
    eqΓAPr = ,C≃ refl eqAPr

  abstract
    eqAᴹ : Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ]T
         ≃ Aᴹ
    eqAᴹ = to≃ [][]T
         ◾̃ []T≃ eqΓAPr (∘^ ⁻¹̃ ◾̃ (^≃ π₁idβ ◾̃ id^))
         ◾̃ to≃ [id]T

  abstract
    eqΓAPrAᴹ : ∣ Γᴹ ∣C
             , A [ Pr Γᴹ ]T [ π₁ id ]T [ < t [ Pr Γᴹ ]t > ]T
             , Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ]T
             ≡ ∣ Γᴹ ,Cᴹ Aᴹ ∣C
    eqΓAPrAᴹ = ,C≃ eqΓAPr eqAᴹ

  abstract
    eqBPr : B [ Pr Γᴹ ^ A ]T [ π₁ id ^ A [ Pr Γᴹ ]T ]T
              [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ]T
          ≃ B [ Pr Γᴹ ^ A ]T
    eqBPr = to≃ [][]T
          ◾̃ []T≃ eqΓAPr (∘^ ⁻¹̃ ◾̃ ^≃ π₁idβ ◾̃ id^)
          ◾̃ to≃ [id]T

  abstract
    eqBᴹ : Bᴹ [  _,s_  (π₁ id ^ A [ Pr Γᴹ ]T ^ Aᴹ)
                       {B [ Pr (Γᴹ ,Cᴹ Aᴹ) ]T}
                       (coe (TmΓ= (pΠᴹ Γᴹ)) (app (coe (TmΓ= (pΠᴹ' Γᴹ)) (π₂ id)) [ π₁ id ]t))  ]T
              [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ^ Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T ]T
         ≃ Bᴹ [ < app t [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t > ]T
    eqBᴹ = to≃ [][]T
         ◾̃ []T≃ eqΓAPrAᴹ
                ( to≃ ,∘
                ◾̃ ,s≃ eqΓAPrAᴹ
                      refl
                      ( ∘^^ ⁻¹̃
                      ◾̃ ^≃' (,C≃ refl ([]T≃ refl (to≃ π₁idβ)))
                            (^≃ π₁idβ)
                      ◾̃ (^≃' (,C≃ refl (to≃ [id]T)) id^ ◾̃ id^))
                      r̃
                      ( coecoe[]t (pΠᴹ Γᴹ) [][]T
                      ◾̃ [][]t'
                      ◾̃ []t≃'' (∘π₁id ⁻¹)
                      ◾̃ [][]t' ⁻¹̃
                      ◾̃ []t≃' eqΓAPrAᴹ
                              eqΓAPr
                              eqBPr
                              ( to≃ (app[] ⁻¹)
                              ◾̃ app≃ refl
                                     eqAPr
                                     eqBPr
                                     ( coecoe[]t (pΠᴹ' Γᴹ) Π[]
                                     ◾̃ π₂idβ
                                     ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= Π[]))
                              ◾̃ to≃ app[])
                              (π₁id≃ eqΓAPr eqAᴹ)
                      ◾̃ [][]t'
                      ◾̃ uncoe (TmΓ= ([id]T ⁻¹))))
