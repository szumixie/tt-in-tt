{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func.Ty where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

abstract
  pΠᴹ : ∀{Γ}(Γᴹ : Conᴹ Γ){A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}
      → B [ Pr Γᴹ ^ A ]T [ π₁ {A = Π A B [ Pr Γᴹ ]T} id ^ A [ Pr Γᴹ ]T ]T [ π₁ id ]T
      ≡ B [ Pr (Γᴹ ,Cᴹ Aᴹ) ]T [ π₁ id ^ A [ Pr Γᴹ ]T ^ Aᴹ ]T
  pΠᴹ {Γ} Γᴹ {A}{Aᴹ}{B}

    = [][]T
    ◾ [][]T
    ◾ ap (_[_]T B)
         (ap (_∘_ (Pr Γᴹ ^ A)) ∘π₁id ◾ ass ⁻¹)
    ◾ [][]T ⁻¹

abstract
  pΠᴹ' : ∀{Γ}(Γᴹ : Conᴹ Γ){A : Ty Γ}{B : Ty (Γ , A)}
       → Π A B [ Pr Γᴹ ]T [ π₁ {A = Π A B [ Pr Γᴹ ]T} id ]T
       ≡ Π (A [ Pr Γᴹ ]T [ π₁ id ]T)
           (B [ Pr Γᴹ ^ A ]T [ π₁ id ^ A [ Pr Γᴹ ]T ]T)
  pΠᴹ' Γᴹ = ap (λ z → z [ π₁ id ]T) Π[] ◾ Π[]

Πᴹ : {Γ : Con}{Γᴹ : Declᴹ.Conᴹ d Γ}
     {A : Ty Γ}(Aᴹ : Declᴹ.Tyᴹ d Γᴹ A)
     {B : Ty (Γ , A)}(Bᴹ : Declᴹ.Tyᴹ d ((c Coreᴹ.,Cᴹ Γᴹ) Aᴹ) B)
   → Declᴹ.Tyᴹ d Γᴹ (Π A B)
Πᴹ {Γ}{Γᴹ}{A} Aᴹ {B} Bᴹ
  = Π (A [ Pr Γᴹ ]T [ π₁ id ]T)
      (Π (Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T)
         (Bᴹ [ (π₁ id ^ A [ Pr Γᴹ ]T ^ Aᴹ)
             ,s coe (TmΓ= (pΠᴹ Γᴹ))
                    (app (coe (TmΓ= (pΠᴹ' Γᴹ)) (π₂ id)) [ π₁ id ]t) ]T))
