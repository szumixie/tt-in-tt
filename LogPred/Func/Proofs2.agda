{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func.Proofs2 where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

open import LogPred.Func.Proofs1


pappᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
        {t : Tm Γ (Π A B)}
      → Tm ( ∣ Γᴹ ∣C
           , A [ Pr Γᴹ ]T [ π₁ id ]T [ < t [ Pr Γᴹ ]t > ]T
           , Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ]T)
           (Bᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ^ Aᴹ ,s coe (TmΓ= (pΠᴹ Γᴹ)) (app (coe (TmΓ= (pΠᴹ' Γᴹ)) (π₂ id)) [ π₁ id ]t) ]T
               [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ^ Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T ]T)
      ≡ Tm ∣ Γᴹ ,Cᴹ Aᴹ ∣C (Bᴹ [ < app t [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t > ]T)
pappᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}

  = let
      open eqΓABᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}
    in
      Tm≃ eqΓAPrAᴹ eqBᴹ
{-
abstract
  plamᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
          {t : Tm (Γ , A) B}
        → Π (A [ Pr Γᴹ ]T) (Π Aᴹ (Bᴹ [ < t [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t > ]T))
        ≡ Πᴹ {Γᴹ = Γᴹ} Aᴹ Bᴹ [ < lam t [ Pr Γᴹ ]t > ]T
  plamᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}

    = let
        open eqΓABᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{lam t}
      in
          Π≃' (from≃ (eqAPr ⁻¹̃))
              ( Π≃ (eqΓAPr ⁻¹)
                   (eqAᴹ ⁻¹̃)
                   ( to≃ (ap (λ z → Bᴹ [ < z [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t > ]T) (Πβ ⁻¹))
                   ◾̃ eqBᴹ ⁻¹̃)
              ◾̃ to≃ (Π[] ⁻¹))
        ◾ Π[] ⁻¹
-}
