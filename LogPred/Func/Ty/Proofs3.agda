{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func.Ty.Proofs3 where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

open import LogPred.Func.Ty
import LogPred.Func.Ty.Proofs1
import LogPred.Func.Ty.Proofs2

module Π[]ᴹ
  {Γ : Con} {Γᴹ : Conᴹ Γ} {Δ : Con} {Δᴹ : Conᴹ Δ} {δ : Tms Γ Δ}
  {δᴹ : Tmsᴹ Γᴹ Δᴹ δ} {A : Ty Δ} {Aᴹ : Tyᴹ Δᴹ A} {B : Ty (Δ , A)}
  where

  open LogPred.Func.Ty.Proofs1.Π[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}
  open LogPred.Func.Ty.Proofs2.Π[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}

  abstract
    p8 : coe (TmΓ= [][]T)
          (coe (TmΓ= (pΠᴹ Δᴹ)) (app (coe (TmΓ= (pΠᴹ' Δᴹ)) (π₂ id)) [ π₁ id ]t) [
           coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (∣ δᴹ ∣s ^ Π A B [ Pr Δᴹ ]T)
           ^ (A [ Pr Δᴹ ]T) [ π₁ id ]T
           ^ Aᴹ [ π₁ id ^ A [ Pr Δᴹ ]T ]T
           ]t)
          ≃
          coe (TmΓ= [][]T)
          (coe (TmΓ= [][]T) (π₂ id) [
           coe (TmsΓ-= (,C≃ refl (to≃ (TyNat {A = B}(δᴹ ^ᴹ Aᴹ))) ⁻¹))
           (π₁ id ^ (A [ δ ]T) [ Pr Γᴹ ]T ^ Aᴹ [ δᴹ ]Tᴹ ,s
            coe (TmΓ= (pΠᴹ Γᴹ)) (app (coe (TmΓ= (pΠᴹ' Γᴹ)) (π₂ id)) [ π₁ id ]t))
           ]t)

    p8 = coecoe[]t (pΠᴹ Δᴹ) [][]T
       ◾̃ [][]t'
       ◾̃ []t≃'' (∘π₁id ⁻¹)
       ◾̃ [][]t' ⁻¹̃
       ◾̃ []t≃' (,C≃ (,C≃ p6 p7) pAᴹ)
               (,C≃ p6 p7)
               s'
               ( to≃ (app[] ⁻¹)
               ◾̃ app≃ p6
                      p7
                      s'
                      ( coecoe[]t (pΠᴹ' Δᴹ) Π[]
                      ◾̃ π₂coe (,C≃ refl (to≃ (TyNat δᴹ)))
                      ◾̃ π₂β'
                      ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                      ◾̃ π₂id≃ refl (to≃ (TyNat δᴹ ◾ ap (λ z → z [ Pr Γᴹ ]T) Π[]))
                      ◾̃ uncoe (TmΓ= (pΠᴹ' Γᴹ))))
               (π₁id≃ (,C≃ p6 p7) pAᴹ)
       ◾̃ uncoe (TmΓ= (pΠᴹ Γᴹ))
       ◾̃ π₂β' ⁻¹̃
       ◾̃ π₂coe⁻¹ (TyNat (δᴹ ^ᴹ Aᴹ)) ⁻¹̃
       ◾̃ coecoe[]t [][]T [][]T ⁻¹̃
