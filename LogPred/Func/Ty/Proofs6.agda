{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func.Ty.Proofs6 where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

open import LogPred.Func.Ty
import LogPred.Func.Ty.Proofs1
import LogPred.Func.Ty.Proofs5

module Π[]ᴹ
  {Γ : Con} {Γᴹ : Conᴹ Γ} {Δ : Con} {Δᴹ : Conᴹ Δ} {δ : Tms Γ Δ}
  {δᴹ : Tmsᴹ Γᴹ Δᴹ δ} {A : Ty Δ} {Aᴹ : Tyᴹ Δᴹ A} {B : Ty (Δ , A)}
  {Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
  where

    open LogPred.Func.Ty.Proofs1.Π[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}
    open LogPred.Func.Ty.Proofs5.Π[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}

    abstract
      ret : Πᴹ {Γᴹ = Δᴹ} Aᴹ Bᴹ [ δᴹ ]Tᴹ
          ≡[ Declᴹ.TyΓᴹ= d {Γᴹ = Γᴹ} Π[] ]≡
            Πᴹ {Γᴹ = Γᴹ}
               (Aᴹ [ δᴹ ]Tᴹ)
               (Bᴹ [ ((δᴹ ∘ᴹ π₁ᴹ idᴹ) ,sᴹ coe (Declᴹ.TmΓᴹ= d [][]T [][]Tᴹ refl) (π₂ᴹ idᴹ)) ]Tᴹ)
      ret            
        = from≃ ( uncoe (Declᴹ.TyΓᴹ= d Π[]) ⁻¹̃
                ◾̃ to≃ Π[]
                ◾̃ Π≃ p6
                     p7
                     ( to≃ Π[]
                     ◾̃ Π≃ (,C≃ p6 p7)
                          pAᴹ
                          ( to≃ [][]T
                          ◾̃ []T≃ (,C≃ (,C≃ p6 p7) pAᴹ)
                                 Goal
                          ◾̃ to≃ ([][]T ⁻¹))))
