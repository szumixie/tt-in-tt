{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func.Tm.Proofs2 where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

open import LogPred.Func.Ty
open import LogPred.Func.Ty.Proofs6
open import LogPred.Func.Tm
import LogPred.Func.Tm.Proofs1

module lam[]ᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
  {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
  {t : Tm (Δ , A) B}{tᴹ : Tmᴹ (Δᴹ ,Cᴹ Aᴹ) Bᴹ t}
  where

  eq : Tm ∣ Γᴹ ∣C (Πᴹ {Γᴹ = Δᴹ} Aᴹ Bᴹ [ δᴹ ]Tᴹ [ < lam t [ δ ]t [ Pr Γᴹ ]t > ]T)
     ≡ Tm ∣ Γᴹ ∣C (Πᴹ {Γᴹ = Γᴹ} (Aᴹ [ δᴹ ]Tᴹ) (Bᴹ [ Coreᴹ._^ᴹ_ c δᴹ Aᴹ ]Tᴹ) [ < lam (t [ δ ^ A ]t) [ Pr Γᴹ ]t > ]T)
  eq = Declᴹ.TmΓᴹ= d
         (Π[] {Γ}{Δ}{δ}{A}{B})
         {Πᴹ {Γᴹ = Δᴹ} Aᴹ Bᴹ [ δᴹ ]Tᴹ}
         {Πᴹ {Γᴹ = Γᴹ} (Aᴹ [ δᴹ ]Tᴹ) (Bᴹ [ Coreᴹ._^ᴹ_ c δᴹ Aᴹ ]Tᴹ)}
         (Π[]ᴹ.ret {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ})
         {lam t [ δ ]t}
         {lam (t [ δ ^ A ]t)}
         (lam[] {Γ}{Δ}{δ}{A}{B}{t})

  open LogPred.Func.Tm.Proofs1.lam[]ᴹ' {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}{tᴹ}
  
  abstract
    ret1 : lamᴹ {Γᴹ = Δᴹ} tᴹ [ δᴹ ]tᴹ
           ≡[ eq ]≡
             lamᴹ {Γᴹ = Γᴹ} (tᴹ [ Coreᴹ._^ᴹ_ c δᴹ Aᴹ ]tᴹ)
    ret1 = from≃ (uncoe eq ⁻¹̃ ◾̃ ret)
