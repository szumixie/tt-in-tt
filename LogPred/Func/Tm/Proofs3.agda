{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func.Tm.Proofs3 where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import NormTy.Inj
open import TT.ConElim

open import LogPred.Decl
open import LogPred.Core

open import LogPred.Func.Ty
open import LogPred.Func.Tm

abstract
  applam : ∀{Γ}{A₀ A₁ : Ty Γ}
           {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}
           {C₀₀ C₀₁ : Ty (Γ , A₀ , B₀)}{C₁ : Ty (Γ , A₁ , B₁)}
           {t : Tm (Γ , A₀ , B₀) C₀₀}
           {ΠABC₁ : Ty Γ}
           (ΠABC₂ : Π A₀ (Π B₀ C₀₀) ≡ ΠABC₁)
           {ΠBC₁ : Ty (Γ , A₁)}
           (AΠBC₂ : ΠABC₁ ≡ Π A₁ ΠBC₁)
           (BC₂ : ΠBC₁ ≡ Π B₁ C₁)
           (C₀₂ : C₀₀ ≡ C₀₁)
           (p : Tm (Γ , A₁ , B₁) C₁ ≡ Tm (Γ , A₀ , B₀) C₀₁)
         → coe p (app (coe (TmΓ= BC₂) (app (coe (TmΓ= AΠBC₂) ((coe (TmΓ= ΠABC₂) (lam (lam t))))))))
         ≃ t
  applam refl p refl refl q
    = uncoe q ⁻¹̃
    ◾̃ app≃ (,C= refl (injΠ₁ refl (p ⁻¹)))
           (injΠ₁' (,C= refl (injΠ₁ refl p)) (injΠ₂' refl (to≃ p)) ⁻¹̃)
           (injΠ₂' (,C= refl (injΠ₁ refl p)) (injΠ₂' refl (to≃ p)) ⁻¹̃)
           ( app≃ refl
                  (to≃ (injΠ₁ refl (p ⁻¹)))
                  (injΠ₂' refl (to≃ p) ⁻¹̃)
                  (uncoe (TmΓ= p) ⁻¹̃)
           ◾̃ to≃ Πβ)
    ◾̃ to≃ Πβ

abstract
  lamapp : ∀{Γ}{A₀ A₁ : Ty Γ}
           {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}
           {C₀ : Ty (Γ , A₀ , B₀)}{C₁ : Ty (Γ , A₁ , B₁)}
           {ΠABC₀ ΠABC₁ : Ty Γ}
           {t : Tm Γ ΠABC₀}
           {ΠBC₀ : Ty (Γ , A₀)}
           (AΠBC₀ : ΠABC₀ ≡ Π A₀ ΠBC₀)
           (BΠC₀ : ΠBC₀ ≡ Π B₀ C₀)
           (ΓAB₂ : (Γ , A₀ , B₀) ≡ (Γ , A₁ , B₁))
           (C₂ : C₀ ≃ C₁)
           (p : Π A₁ (Π B₁ C₁) ≡ ΠABC₁)
         → coe (TmΓ= p) (lam (lam (coe (Tm≃ ΓAB₂ C₂) (app (coe (TmΓ= BΠC₀) (app (coe (TmΓ= AΠBC₀) t)))))))
         ≃ t
  lamapp refl refl p q refl
    = lam≃ refl
           (from≡ (Ty= (inj,₀ (inj,₀ p))) (inj,₁ (inj,₀ p)) ⁻¹̃)
           (Π≃ (inj,₀ p ⁻¹)
               (from≡ (Ty= (inj,₀ p)) (inj,₁ p) ⁻¹̃)
               (q ⁻¹̃))
           ( lam≃ (inj,₀ p ⁻¹)
                  (from≡ (Ty= (inj,₀ p)) (inj,₁ p) ⁻¹̃)
                  (q ⁻¹̃)
                  (uncoe (Tm≃ p q) ⁻¹̃)
           ◾̃ to≃ Πη)
    ◾̃ to≃ Πη
