{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func.Tm.Proofs1 where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

open import LogPred.Func.Ty
import LogPred.Func.Ty.Proofs2
open import LogPred.Func.Tm

module lam[]ᴹ'
  {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
  {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
  {t : Tm (Δ , A) B}{tᴹ : Tmᴹ (Δᴹ ,Cᴹ Aᴹ) Bᴹ t}
  where

  open LogPred.Func.Ty.Proofs2.Π[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ} hiding (_^ᴹ_)

  abstract
    pΓA : _≡_ {A = Con}
              (∣ Γᴹ ∣C , A [ Pr Δᴹ ]T [ ∣ δᴹ ∣s ]T)
              (∣ Γᴹ ∣C , A [ δ ]T [ Pr Γᴹ ]T)
    pΓA = ,C≃ refl (to≃ (TyNat δᴹ))

  abstract
    pAᴹ : Aᴹ [ ∣ δᴹ ∣s ^ A [ Pr Δᴹ ]T ]T ≃ Aᴹ [ δᴹ ]Tᴹ
    pAᴹ = []T≃ pΓA (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))))

  abstract
    pBᴹ : (Bᴹ [ < t [ Pr (Δᴹ ,Cᴹ Aᴹ) ]t > ]T [ ∣ δᴹ ∣s ^ A [ Pr Δᴹ ]T ^ Aᴹ ]T)
        ≃ (Bᴹ [ Coreᴹ._^ᴹ_ c δᴹ Aᴹ ]Tᴹ [ < t [ δ ^ A ]t [ Pr (Γᴹ ,Cᴹ (Aᴹ [ δᴹ ]Tᴹ)) ]t > ]T)
    pBᴹ = to≃ [][]T
        ◾̃ []T≃ (,C≃ pΓA pAᴹ)
               ( to≃ ,∘
               ◾̃ ,s≃ (,C≃ pΓA pAᴹ)
                     refl
                     (to≃ idl ◾̃ =s^ ⁻¹̃)
                     r̃
                     ( coecoe[]t ([id]T ⁻¹) [][]T
                     ◾̃ []t≃ (,C≃ pΓA pAᴹ) (=s^ ⁻¹̃)
                     ◾̃ TmNat (Coreᴹ._^ᴹ_ c δᴹ Aᴹ)
                     ◾̃ uncoe (TmΓ= (TyNat (Coreᴹ._^ᴹ_ c δᴹ Aᴹ) ⁻¹)))
               ◾̃ to≃ (^∘<> (TyNat (Coreᴹ._^ᴹ_ c δᴹ Aᴹ)) ⁻¹))
        ◾̃ to≃ [][]T ⁻¹̃

  abstract
    ret : lamᴹ {Γᴹ = Δᴹ} tᴹ [ δᴹ ]tᴹ
        ≃ lamᴹ {Γᴹ = Γᴹ} (tᴹ [ Coreᴹ._^ᴹ_ c δᴹ Aᴹ ]tᴹ)
    ret
      = uncoe (TmΓ= (p[]tᴹ _)) ⁻¹̃
      ◾̃ coe[]t' (plamᴹ {Γᴹ = Δᴹ})
      ◾̃ from≡ (TmΓ= Π[]) lam[]
      ◾̃ lam≃ refl
             r̃
             (to≃ Π[])
             (from≡ (TmΓ= Π[]) lam[])
      ◾̃ lam≃ refl
             (to≃ (TyNat δᴹ))
             (Π≃ pΓA pAᴹ pBᴹ)
             (lam≃ pΓA
                   ([]T≃ pΓA (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ))))))
                   pBᴹ
                   ([]t≃ (,C≃ pΓA pAᴹ) (=s^ ⁻¹̃) ◾̃ uncoe (TmΓ= (p[]tᴹ _))))
      ◾̃ uncoe (TmΓ= (plamᴹ {Γᴹ = Γᴹ}))
