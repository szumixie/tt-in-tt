
Require Import TT.Lib.

Axiom Con : Set.
Axiom Ty  : Con → Set.
Axiom Sub : Con → Con → Set.
Axiom Tm  : ∀ (Γ : Con), Ty Γ → Set.

Axiom nil  : Con.
Axiom cons : ∀ (Γ : Con), Ty Γ → Con.
Notation "∙" := nil.
Infix    "▷" := cons (at level 41, left associativity).

Axiom U  : ∀ {Γ}, Ty Γ.
Axiom El : ∀ {Γ}, Tm Γ U → Ty Γ.
Axiom Π  : ∀ {Γ}(A : Ty Γ), Ty (Γ ▷ A) → Ty Γ.
Axiom TyS : ∀ {Γ Δ}, Sub Γ Δ → Ty Δ → Ty Γ.

Axiom ε    : ∀ {Γ}, Sub Γ ∙.
Axiom id  : ∀ {Γ}, Sub Γ Γ.
Axiom comp : ∀ {Γ Δ Σ}, Sub Δ Σ → Sub Γ Δ → Sub Γ Σ.
Axiom ext  : ∀ {Γ Δ A}(σ : Sub Γ Δ), Tm Γ (TyS σ A) → Sub Γ (Δ ▷ A).
Axiom π₁   : ∀ {Γ Δ A}, Sub Γ (Δ ▷ A) → Sub Γ Δ.

Infix "," := ext (at level 43, left associativity).
Infix "∘" := comp (at level 42, right associativity).

Axiom π₂  : ∀ {Γ Δ A}(σ : Sub Γ (Δ ▷ A)), Tm Γ (TyS (π₁ σ) A).
Axiom lam : ∀ {Γ A B}, Tm (Γ ▷ A) B → Tm Γ (Π A B).
Axiom app : ∀ {Γ A B}, Tm Γ (Π A B) → Tm (Γ ▷ A) B.
Axiom TmS : ∀ {Γ Δ A}(σ : Sub Γ Δ), Tm Δ A → Tm Γ (TyS σ A).

Axiom TyId    : ∀ {Γ}{A : Ty Γ}, TyS id A ≡ A.
Axiom TyComp  : ∀ {Γ Δ Σ}{A : Ty Σ}{σ : Sub Δ Σ}{δ : Sub Γ Δ}, TyS δ (TyS σ A) ≡ TyS (σ ∘ δ) A.
Axiom idl     : ∀ {Γ Δ}{σ : Sub Γ Δ}, id ∘ σ ≡ σ.
Axiom idr     : ∀ {Γ Δ}{σ : Sub Γ Δ}, σ ∘ id ≡ σ.
Axiom ass     : ∀ {Γ Δ Σ Ξ}{σ : Sub Σ Ξ}{δ : Sub Δ Σ}{ν : Sub Γ Δ}, σ ∘ δ ∘ ν ≡ (σ ∘ δ) ∘ ν.
Axiom εη      : ∀ {Γ}{σ : Sub Γ ∙}, σ ≡ ε.
Axiom π₁β     : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (TyS σ A)}, π₁ (σ , t) ≡ σ.
Axiom π₂β     : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (TyS σ A)}, coe ((λ x, Tm Γ (TyS x A)) & π₁β) (π₂ (σ , t)) ≡ t.
Axiom extη    : ∀ {Γ Δ A}{σ : Sub Γ (Δ ▷ A)}, (π₁ σ , π₂ σ) ≡ σ.

Axiom extComp :
  ∀ {Γ Δ Σ}{δ : Sub Γ Δ}{σ : Sub Σ Γ}{A : Ty Δ}{a : Tm Γ (TyS δ A)},
    (δ , a) ∘ σ ≡ (δ ∘ σ , coe (Tm Σ & TyComp) (TmS σ a)).

Axiom US  : ∀ {Γ Δ}{σ : Sub Γ Δ}, TyS σ U ≡ U.
Axiom ElS : ∀ {Γ Δ}{σ : Sub Γ Δ}{A : Tm Δ U}, TyS σ (El A) ≡ El (coe (Tm Γ & US) (TmS σ A)).

Definition wk {Γ}{A : Ty Γ} : Sub (Γ ▷ A) Γ := π₁ id.
Definition vz {Γ}{A : Ty Γ} : Tm (Γ ▷ A) (TyS wk A) := π₂ id.
Definition vs {Γ}{A B : Ty Γ}(t : Tm Γ A) : Tm (Γ ▷ B) (TyS wk A) := TmS wk t.
Definition drop {Γ Δ}{A : Ty Γ}(σ : Sub Γ Δ) : Sub (Γ ▷ A) Δ := σ ∘ wk.
Definition keep {Γ Δ}{A : Ty Δ}(σ : Sub Γ Δ) : Sub (Γ ▷ TyS σ A) (Δ ▷ A) := (drop σ, coe (Tm _ & TyComp) vz).
                                                   
Axiom ΠS :
  ∀ {Γ Δ}{σ : Sub Γ Δ}{A : Ty Δ}{B : Ty (Δ ▷ A)}, TyS σ (Π A B) ≡ Π (TyS σ A) (TyS (keep σ) B).

Axiom lamS :
  ∀{Γ Δ}{δ : Sub Γ Δ}{A : Ty Δ}{B : Ty (Δ ▷ A)}{t : Tm (Δ ▷ A) B},
    coe (Tm Γ & ΠS) (TmS δ (lam t)) ≡ lam (TmS (keep δ) t).

Axiom Πβ : ∀ {Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{t : Tm (Γ ▷ A) B}, app (lam t) ≡ t.
Axiom Πη : ∀ {Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{t : Tm Γ (Π A B)}, lam (app t) ≡ t.

