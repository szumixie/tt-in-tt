
How to edit/compile:

  1. Install Coq 8.6 (https://coq.inria.fr/download)
  2. Install Proof General (https://proofgeneral.github.io/)
     - Recommended: install company-coq (https://github.com/cpitclaudel/company-coq)
  3. Make sure "Coq->Auto Compilation->Compile Before Require" is enabled.
    If not, enable, then save settings in "Coq->Settings->Save Settings"
    (or via standard Emacs customize-group interface)
            
