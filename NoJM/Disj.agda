{-# OPTIONS --without-K --no-eta --rewriting #-}

module NoJM.Disj where

open import lib using (⊥; ⊤; tt; refl; coe; ap; Lift; lift)

open import NoJM.TT as M using ()
open import NoJM.Syntax

module m,• where

  d : M.Decl
  d = record
    { Con = Set
    ; Ty  = λ _ → Lift ⊤
    ; Tms = λ _ _ → ⊤
    ; Tm  = λ _ _ → ⊤
    }

  c : M.Core d
  c = record
    { • = ⊥
    ; _,_ = λ _ _ → ⊤
    ; _[_]T = λ _ _ → lift tt
    ; id = tt
    ; _∘_ = λ _ _ → tt
    ; ε = tt
    ; _,s_ = λ _ _ → tt
    ; π₁ = λ _ → tt
    ; _[_]t = λ _ _ → tt
    ; π₂ = λ _ → tt
    ; [id]T = λ { {A = lift tt} → refl }
    ; [][]T = refl
    ; idl = refl
    ; idr = refl
    ; ass = refl
    ; ,∘ = refl
    ; π₁β = refl
    ; πη = refl
    ; εη = refl
    ; π₂β = refl
    }

  b : M.Base d c
  b = record { U = lift tt ; U[] = refl ; El = λ _ → lift tt ; El[] = refl }

  f : M.Func d c
  f = record
    { Π = λ _ _ → lift tt
    ; Π[] = refl
    ; lam = λ _ → tt
    ; app = λ _ → tt
    ; lam[] = refl
    ; Πβ = refl
    ; Πη = refl
    }

disj,• : ∀{Γ A} → Con= (Γ , A) • → ⊥
disj,• p = coe (ap (recCon d c b f) p) tt
  where
    open m,•

module mΠU where

  d : M.Decl
  d = record
    { Con = Lift ⊤
    ; Ty  = λ _ → Set
    ; Tms = λ _ _ → ⊤
    ; Tm  = λ _ _ → ⊤
    }

  c : M.Core d
  c = record
    { • = lift tt
    ; _,_ = λ _ _ → lift tt
    ; _[_]T = λ A _ → A
    ; id = tt
    ; _∘_ = λ _ _ → tt
    ; ε = tt
    ; _,s_ = λ _ _ → tt
    ; π₁ = λ _ → tt
    ; _[_]t = λ _ _ → tt
    ; π₂ = λ _ → tt
    ; [id]T = refl
    ; [][]T = refl
    ; idl = refl
    ; idr = refl
    ; ass = refl
    ; ,∘ = refl
    ; π₁β = refl
    ; πη = refl
    ; εη = refl
    ; π₂β = refl
    }

  b : M.Base d c
  b = record
    { U = ⊥
    ; U[] = refl
    ; El = λ _ → ⊤
    ; El[] = refl
    }

  f : M.Func d c
  f = record
    { Π = λ _ _ → ⊤
    ; Π[] = refl
    ; lam = λ _ → tt
    ; app = λ _ → tt
    ; lam[] = refl
    ; Πβ = refl
    ; Πη = refl
    }

disjΠU : ∀{Γ A B} → Ty= refl (Π {Γ} A B) U → ⊥
disjΠU p = coe (ap (recTy d c b f) p) tt
  where
    open mΠU
