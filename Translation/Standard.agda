{-# OPTIONS --rewriting #-}

module Translation.Standard where

open import lib using (_≡_; _⁻¹; ap; refl)
open import TTR as S using ()

-- the standard model as a syntactic translation

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixl 5 _$_
infixl 7 _‚_

Con : Set
Con = S.Ty S.•

Ty : Con → Set
Ty Γ = S.Ty (S.• S.▷ Γ)

Tms : Con → Con → Set
Tms Γ Δ = S.Tm S.• (Γ S.⇒ Δ)

Tm : (Γ : Con) → Ty Γ → Set
Tm Γ A = S.Tm S.• (S.Π Γ A)

• : Con  -- \bub
• = S.⊤

_▷_ : (Γ : Con) → Ty Γ → Con
Γ ▷ A = S.Σ Γ A

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
A [ σ ]T = A S.[ S.ε S., S.app σ ]T

id : ∀{Γ} → Tms Γ Γ
id = S.lam S.vz

_∘_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
σ ∘ ν = S.lam (σ S.[ S.wk ]t S.$ (ν S.[ S.wk ]t S.$ S.vz))

ε : ∀{Γ} → Tms Γ •
ε = S.lam S.tt

_,_ : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
σ , t = S.lam ((σ S.[ S.wk ]t S.$ S.vz) S.‚ {!t S.[ S.wk ]t S.$ S.vz!})

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▷ A) → Tms Γ Δ
π₁ σ = S.lam (S.proj₁ (σ S.[ S.wk ]t S.$ S.vz))

_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = S.lam {!t S.[ S.wk ]t S.$ (σ S.[ S.wk ]t S.$ S.vz)!}

π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ {Γ}{Δ}{A} σ = S.lam {! S.proj₂ (σ S.[ S.wk ]t S.$ S.vz) !}

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T {Γ}{A} = refl

[][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ} →
  (A [ δ ]T [ σ ]T) ≡ (A [ _∘_ {Γ}{Δ}{Σ} δ σ ]T)
[][]T = {!!}

idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Δ}{Δ} id δ) ≡ δ
idl = {!!}

idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Γ}{Δ} δ id) ≡ δ
idr = {!!}
