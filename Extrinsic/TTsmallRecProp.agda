{-# OPTIONS --rewriting --no-eta #-}

module Extrinsic.TTsmallRecProp where

open import Agda.Primitive
open import lib
open import JM

-- this is a version of Extrinsic.TTsmallRec where the well-typedness
-- relations are propositional (not proof-relevant)

------------------------------------------------------------------------------
-- intrinsic syntax
------------------------------------------------------------------------------

-- model

record ConTy {ℓ} : Set (lsuc ℓ) where
  field
    Con  : Set ℓ
    Ty   : Con → Set ℓ
    •    : Con  -- \bub
    _,_  : (Γ : Con) → Ty Γ → Con
    U    : (Γ : Con) → Ty Γ
    Π    : (Γ : Con)(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ

  infixl 6 _,_

-- morphism

record ConTyᴹ {ℓ₁}{ℓ₂}(M : ConTy {ℓ₁})(N : ConTy {ℓ₂}) : Set (ℓ₁ ⊔ ℓ₂) where
  private module M = ConTy M
  private module N = ConTy N
  field
    Con : M.Con → N.Con
    Ty  : ∀{Γ} → M.Ty Γ → N.Ty (Con Γ)
    •   : Con M.• ≡ N.•
    ,   : ∀{Γ A} → Con (Γ M., A) ≡ Con Γ N., Ty A
    U   : ∀{Γ} → Ty (M.U Γ) ≡ N.U (Con Γ)
    Π   : ∀{Γ A B} → Ty (M.Π Γ A B) ≡ N.Π (Con Γ) (Ty A) (coe (ap N.Ty ,) (Ty B))

=ConTyᴹ
  : ∀{ℓ₁}{ℓ₂}{M : ConTy {ℓ₁}}{N : ConTy {ℓ₂}}{f₀ f₁ : ConTyᴹ M N}
  → let private module M = ConTy M in
    let private module N = ConTy N in
    let private module f₀ = ConTyᴹ f₀ in
    let private module f₁ = ConTyᴹ f₁ in
    (=Con : f₀.Con ≡ f₁.Con)
    (=Ty : (λ {Γ} → f₀.Ty {Γ}) ≡[ ap (λ Con → ∀{Γ} → M.Ty Γ → N.Ty (Con Γ)) =Con ]≡ (λ {Γ} → f₁.Ty {Γ}))
  → f₀ ≡ f₁
=ConTyᴹ
  {M = M}{N}
  {record { Con =  Con ; Ty =  Ty ; • = •₀ ; , = ,₀ ; U = U₀ ; Π = Π₀ }}
  {record { Con = .Con ; Ty = .Ty ; • = •₁ ; , = ,₁ ; U = U₁ ; Π = Π₁ }}
  refl refl
  = ap4' {A = Con M.• ≡ N.•}{∀{Γ A} → Con (Γ M., A) ≡ Con Γ N., Ty A}{∀{Γ} → Ty (M.U Γ) ≡ N.U (Con Γ)}{λ , → ∀{Γ A B} → Ty (M.Π Γ A B) ≡ N.Π (Con Γ) (Ty A) (coe (ap N.Ty ,) (Ty B))}
         {ConTyᴹ M N}
         (λ • , U Π → record { Con =  Con ; Ty =  Ty ; • = • ; , = , ; U = U ; Π = Π } )
         (UIP _ _)
         (funexti λ Γ → funexti λ A → UIP _ _)
         (funexti λ Γ → UIP _ _)
         (funexti λ Γ → funexti λ A → funexti λ B → UIP _ _)
  where
    private module M = ConTy M
    private module N = ConTy N

------------------------------------------------------------------------------
-- presyntax
------------------------------------------------------------------------------

-- model

record ConTy' {ℓ} : Set (lsuc ℓ) where
  field
    Con : Set ℓ
    Ty  : Set ℓ
    •   : Con
    _,_ : Con → Ty → Con
    U   : Con → Ty
    Π   : Con → Ty → Ty → Ty

  infixl 6 _,_

-- morphism

record ConTy'ᴹ {ℓ₁ ℓ₂}(M' : ConTy' {ℓ₁})(N' : ConTy' {ℓ₂}) : Set (lsuc (ℓ₁ ⊔ ℓ₂)) where
  private module M' = ConTy' M'
  private module N' = ConTy' N'
  field
    Con : M'.Con → N'.Con
    Ty  : M'.Ty  → N'.Ty
    •   : Con M'.• ≡ N'.•
    ,   : {Γ' : M'.Con}{A' : M'.Ty} → Con (Γ' M'., A') ≡ Con Γ' N'., Ty A'
    U   : {Γ' : M'.Con} → Ty (M'.U Γ') ≡ N'.U (Con Γ')
    Π   : {Γ' : M'.Con}{A' B' : M'.Ty} → Ty (M'.Π Γ' A' B') ≡ N'.Π (Con Γ') (Ty A') (Ty B')

-- identity morphism

id' : ∀{ℓ}{M' : ConTy' {ℓ}} → ConTy'ᴹ M' M'
id' = record { Con = λ Γ' → Γ' ; Ty = λ A' → A' ; • = refl ; , = refl ; U = refl ; Π = refl }

-- syntax

postulate
  S' : ConTy' {lzero}

module S' = ConTy' S'

-- recursor

module _ {ℓ}(M' : ConTy' {ℓ}) where

  postulate
    r' : ConTy'ᴹ S' M'

  module r' = ConTy'ᴹ r'

  {-# REWRITE r'.• #-}
  {-# REWRITE r'., #-}
  {-# REWRITE r'.U #-}
  {-# REWRITE r'.Π #-}

-- uniqueness

module _ {ℓ}{M' : ConTy' {ℓ}}(f f' : ConTy'ᴹ S' M') where

  postulate
    u' : f ≡ f'

------------------------------------------------------------------------------
-- well typedness predicate defined over a model of the presyntax
------------------------------------------------------------------------------

module _ {ℓ}(M' : ConTy' {ℓ}) where

  private module M' = ConTy' M'

  -- model

  record ConTyw {ℓ'} : Set (ℓ ⊔ lsuc ℓ') where
    field
      Con : M'.Con → Set ℓ'
      Ty  : M'.Con → M'.Ty → Set ℓ'
      •   : Con M'.•
      _,_ : {Γ' : M'.Con}(Γw : Con Γ'){A' : M'.Ty}(Aw : Ty Γ' A') → Con (Γ' M'., A')
      U   : {Γ' : M'.Con}(Γw : Con Γ') → Ty Γ' (M'.U Γ')
      Π   : {Γ' : M'.Con}(Γw : Con Γ'){A' : M'.Ty}(Aw : Ty Γ' A'){B' : M'.Ty}(Bw : Ty (Γ' M'., A') B') → Ty Γ' (M'.Π Γ' A' B')
      pCon : {Γ' : M'.Con}{Γw₀ Γw₁ : Con Γ'} → Γw₀ ≡ Γw₁
      pTy  : {Γ' : M'.Con}{A' : M'.Ty}{Aw₀ Aw₁ : Ty Γ' A'} → Aw₀ ≡ Aw₁

    infixl 6 _,_

  -- syntax

  postulate
    Sw : ConTyw {lzero}

  module Sw = ConTyw Sw

module _ {ℓ}{M' : ConTy' {ℓ}} where -- M' is implicit

  private module M' = ConTy' M'

  -- morphism

  -- (if we make this a special case of the heterogeneous morphism
  -- below, the rewrite rules cannot be stated anymore)

  record ConTywᴹ {ℓ'₁ ℓ'₂}(Mw : ConTyw M' {ℓ'₁})(Nw : ConTyw M' {ℓ'₂}) : Set (ℓ ⊔ lsuc ℓ'₁ ⊔ lsuc ℓ'₂) where
    private module Mw = ConTyw Mw
    private module Nw = ConTyw Nw
    field
      Con : {Γ' : M'.Con} → Mw.Con Γ' → Nw.Con Γ'
      Ty  : {Γ' : M'.Con}{A' : M'.Ty} → Mw.Ty Γ' A' → Nw.Ty Γ' A'

  -- recursor

  module _ {ℓw}(Mw : ConTyw M' {ℓw}) where

    postulate
      rw : ConTywᴹ (Sw M') Mw

    module rw = ConTywᴹ rw

------------------------------------------------------------------------------
-- heterogeneous morphism between well typedness predicates
------------------------------------------------------------------------------

record ConTywᴹh
  {ℓ'₁}{M' : ConTy' {ℓ'₁}}
  {ℓ'₂}{N' : ConTy' {ℓ'₂}}
  (f' : ConTy'ᴹ M' N')
  {ℓw₁}(Mw : ConTyw M' {ℓw₁})
  {ℓw₂}(Nw : ConTyw N' {ℓw₂})
    : Set (lsuc ℓ'₁ ⊔ lsuc ℓ'₂ ⊔ ℓw₁ ⊔ ℓw₂)
  where
    private module M' = ConTy' M'
    private module N' = ConTy' N'
    private module f' = ConTy'ᴹ f'
    private module Mw = ConTyw Mw
    private module Nw = ConTyw Nw
    field
      Con : {Γ' : M'.Con} → Mw.Con Γ' → Nw.Con (f'.Con Γ')
      Ty  : {Γ' : M'.Con}{A' : M'.Ty} → Mw.Ty Γ' A'
          → Nw.Ty (f'.Con Γ') (f'.Ty A')

-- lifting a premodel-morphism to a (heterogeneous) well-typedness
-- morphism between the well-typedness syntaxes

module _
  {ℓ'₁}{M' : ConTy' {ℓ'₁}}
  {ℓ'₂}{N' : ConTy' {ℓ'₂}}
  (f' : ConTy'ᴹ M' N') -- the premodel morphism
  where

  private module f' = ConTy'ᴹ f'
  private module SwN' = Sw N'

  liftSw : ConTywᴹh f' (Sw M') (Sw N')
  liftSw = record
    { Con = rwMw.Con
    ; Ty  = rwMw.Ty
    }
    where
      Mw : ConTyw M' -- a well-typedness relation over M' which uses Sw N'
      Mw = record
        { Con = λ Γ' → SwN'.Con (f'.Con Γ')
        ; Ty  = λ Γ' A' → SwN'.Ty (f'.Con Γ') (f'.Ty A')
        ; •   = coe (ap SwN'.Con (f'.• ⁻¹)) SwN'.•
        ; _,_ = λ Γw Aw → coe (ap SwN'.Con (f'., ⁻¹)) (Γw SwN'., Aw)
        ; U   = λ Γw → coe (ap (SwN'.Ty _) (f'.U ⁻¹)) (SwN'.U Γw)
        ; Π   = λ Γw Aw Bw → coe (ap (SwN'.Ty _) (f'.Π ⁻¹)) (SwN'.Π Γw Aw (coe (ap (λ z → SwN'.Ty z _) f'.,) Bw))
        ; pCon = SwN'.pCon
        ; pTy  = SwN'.pTy
        }

      private module rwMw = ConTywᴹ (rw Mw) -- the initial morphism from (Sw M') to Mw

-- composition of a heterogeneous morphism and a homogeneous morphism

_∘h_ : ∀{ℓ₁ ℓ₂ ℓ₁w ℓ₂w ℓ₃w}{M' : ConTy' {ℓ₁}}{N' : ConTy' {ℓ₂}}
       {f' : ConTy'ᴹ M' N'}{Mw : ConTyw M' {ℓ₁w}}{Nw : ConTyw N' {ℓ₂w}}{Ow : ConTyw N' {ℓ₃w}}
     → ConTywᴹ Nw Ow → ConTywᴹh f' Mw Nw → ConTywᴹh f' Mw Ow
_∘h_ {M' = M'}{N'}{f'}{Mw}{Nw}{Ow} gw fw = record
  { Con = λ Γw → gw.Con (fw.Con Γw)
  ; Ty  = λ Aw → gw.Ty (fw.Ty Aw)
  }
  where
    module M' = ConTy' M'
    module N' = ConTy' N'
    module Mw = ConTyw Mw
    module Nw = ConTyw Nw
    module Ow = ConTyw Ow
    module f' = ConTy'ᴹ f'
    module gw = ConTywᴹ gw
    module fw = ConTywᴹh fw

------------------------------------------------------------------------------
-- constructing an intrinsic model from an extrinsic model
------------------------------------------------------------------------------

-- there is no need for a model of the presyntax ConTy'

EI : ∀{ℓ ℓ'} → Σ (ConTy' {ℓ}) (λ M' → ConTyw M' {ℓ'}) → ConTy
EI (M' ,Σ Mw) = record
  { Con = Σ M'.Con Mw.Con
  ; Ty  = λ { (Γ' ,Σ Γw) → Σ M'.Ty λ A' → Mw.Ty Γ' A' }
  ; •   = M'.• ,Σ Mw.•
  ; _,_ = λ { (Γ' ,Σ Γw)(A' ,Σ Aw) → (Γ' M'., A') ,Σ (Γw Mw., Aw) }
  ; U   = λ { (Γ' ,Σ Γw) → M'.U Γ' ,Σ Mw.U Γw }
  ; Π   = λ { (Γ' ,Σ Γw)(A' ,Σ Aw)(B' ,Σ Bw) → M'.Π Γ' A' B' ,Σ Mw.Π Γw Aw Bw }
  }
  where
    private module M' = ConTy' M'
    private module Mw = ConTyw Mw

-- this is what we will use for initial ConTy-algebra

S : ConTy
S = EI (S' ,Σ (Sw S'))

------------------------------------------------------------------------------
-- uniqueness
------------------------------------------------------------------------------

uniq : ∀{ℓ}{M : ConTy {ℓ}}(f f' : ConTyᴹ S M) → f ≡ f'
uniq {ℓ}{M} f f'
  = =ConTyᴹ (funext (λ { (Γ' ,Σ Γw) → {!!} })) {!!} -- TODO: induction on preterms and the typing relation
  where
    private module M  = ConTy M
    private module f  = ConTyᴹ f
    private module f' = ConTyᴹ f'

------------------------------------------------------------------------------
-- constructing an extrinsic model from an intrinsic model
------------------------------------------------------------------------------

module _ {ℓ}(M : ConTy {ℓ}) where

  private module M = ConTy M

  -- a quotient inductive type with its recursor: this is a version of
  -- the presyntax where

  record ConTyη {ℓ'} : Set (ℓ ⊔ lsuc ℓ') where
    field
      Con  : Set ℓ'
      Ty   : Set ℓ'
      •    : Con
      _,_  : Con → Ty → Con
      U    : Con → Ty
      Π    : Con → Ty → Ty → Ty
      Conη : M.Con → Con
      Tyη  : Σ M.Con M.Ty → Ty
      •η   : Conη M.• ≡ •
      ,η   : ∀{Γ A} → Conη (Γ M., A) ≡ Conη Γ , Tyη (Γ ,Σ A)
      Uη   : ∀{Γ} → Tyη (Γ ,Σ M.U Γ) ≡ U (Conη Γ)
      Πη   : ∀{Γ A B} → Tyη (Γ ,Σ M.Π Γ A B)
                      ≡ Π (Conη Γ) (Tyη (Γ ,Σ A)) (Tyη (Γ M., A ,Σ B))

  -- syntax

  postulate
    Sη : ConTyη {lzero}

  module Sη = ConTyη Sη

  -- morphism

  record ConTyηᴹ {ℓ₁ ℓ₂}(Mη : ConTyη {ℓ₁})(Nη : ConTyη {ℓ₂}) : Set (ℓ ⊔ ℓ₁ ⊔ ℓ₂) where
    private module Mη = ConTyη Mη
    private module Nη = ConTyη Nη

    field
      Con  : Mη.Con → Nη.Con
      Ty   : Mη.Ty → Nη.Ty
      •    : Con Mη.• ≡ Nη.•
      ,    : {Γη : Mη.Con}{Aη : Mη.Ty} → Con (Γη Mη., Aη) ≡ Con Γη Nη., Ty Aη
      U    : {Γη : Mη.Con} → Ty (Mη.U Γη) ≡ Nη.U (Con Γη)
      Π    : {Γη : Mη.Con}{Aη Bη : Mη.Ty} → Ty (Mη.Π Γη Aη Bη) ≡ Nη.Π (Con Γη) (Ty Aη) (Ty Bη)
      Conη : {Γη : M.Con} → Con (Mη.Conη Γη) ≡ Nη.Conη Γη
      Tyη  : {ΓA : Σ M.Con M.Ty} → Ty (Mη.Tyη ΓA) ≡ Nη.Tyη ΓA

  -- recursor

  module _ {ℓη}(Mη : ConTyη {ℓη}) where

    postulate
      rη : ConTyηᴹ Sη Mη

    module rη = ConTyηᴹ rη

    -- propositional computation rules are enough for the proof below,
    -- so I uncommented these
{-
    {-# REWRITE rη.• #-}
    {-# REWRITE rη., #-}
    {-# REWRITE rη.U #-}
    {-# REWRITE rη.Π #-}
    {-# REWRITE rη.Conη #-}
    {-# REWRITE rη.Tyη #-}
-}

  -- injectivity of the constructors Conη and Tyη

  open import Extrinsic.propext

  private
    mInj : ConTyη
    mInj = record
            { Con = Σ (Set ℓ) λ P → P → M.Con
            ; Ty = Σ (Set ℓ) λ P → P → Σ M.Con M.Ty
            ; • = Lift ⊤ ,Σ λ _ → M.•
            ; _,_ = λ { (P ,Σ pΓ)(P' ,Σ pΓA)
                       →    (Σ P λ p → Σ P' λ p' → proj₁ (pΓA p') ≡ pΓ p)
                         ,Σ (λ { (p ,Σ (p' ,Σ eq)) → pΓ p M., coe (ap M.Ty eq) (proj₂ (pΓA p')) })
                       }
            ; U = λ { (P ,Σ pΓ) → P ,Σ (λ p → pΓ p ,Σ M.U (pΓ p)) }
            ; Π = λ { (P ,Σ pΓ)(P' ,Σ pΓA)(P'' ,Σ pΓAB)
                     →    (Σ P λ p → Σ P' λ p' → Σ P'' λ p'' → (proj₁ (pΓA p') ≡ pΓ p) × (proj₁ (pΓAB p'') ≡ (proj₁ (pΓA p') M., proj₂ (pΓA p'))))
                       ,Σ (λ { (p ,Σ (p' ,Σ (p'' ,Σ (eq ,Σ eq')))) → pΓ p ,Σ coe (ap M.Ty eq) (M.Π (proj₁ (pΓA p')) (proj₂ (pΓA p')) (coe (ap M.Ty eq') (proj₂ (pΓAB p'')))) })
                     }
            ; Conη = λ Γ → Lift ⊤ ,Σ λ _ → Γ
            ; Tyη = λ ΓA → Lift ⊤ ,Σ λ _ → ΓA
            ; •η = refl
            ; ,η = λ {Γ}{A} → let p = propext pLift⊤
                                              (pΣ pLift⊤ (λ _ → pΣ pLift⊤ λ _ → p≡))
                                              (  (λ _ → lift tt ,Σ (lift tt ,Σ refl))
                                              ,Σ λ _ → lift tt)
                              in
                              ,Σ= p ( from≃ (uncoe (ap (λ P → P → M.Con) p) ⁻¹̃
                                    ◾̃ funext≃ p λ {x₀}{x₁} x₂ → to≃ (ap (λ z → Γ M., coe z A)
                                                                        (UIP refl (ap M.Ty (proj₂ (proj₂ x₁)))))))
            ; Uη = refl
            ; Πη = λ {Γ}{A}{B} → let p = propext pLift⊤
                                                 (pΣ pLift⊤ λ _ → pΣ pLift⊤ λ _ → pΣ pLift⊤ λ _ → pΣ p≡ λ _ → p≡)
                                                 (  (λ _ → lift tt ,Σ (lift tt ,Σ (lift tt ,Σ (refl ,Σ refl))))
                                                 ,Σ (λ _ → lift tt))
                                 in
                                 ,Σ= p (from≃ ( uncoe (ap (λ P → P → Σ M.Con M.Ty) p) ⁻¹̃
                                              ◾̃ funext≃ p (λ {x₀}{x₁} x₂ → to≃ ( ap (λ z → Γ ,Σ M.Π Γ A (coe z B)) (UIP refl (ap M.Ty (proj₂ (proj₂ (proj₂ (proj₂ x₁))))))
                                                                               ◾ ap (λ z → Γ ,Σ coe z (M.Π Γ A (coe (ap M.Ty (proj₂ (proj₂ (proj₂ (proj₂ x₁))))) B)))
                                                                                    (UIP refl (ap M.Ty (proj₁ (proj₂ (proj₂ (proj₂ x₁))))))))))
            }

  injConη : {Γ₀ Γ₁ : M.Con} → Sη.Conη Γ₀ ≡ Sη.Conη Γ₁ → Γ₀ ≡ Γ₁
  injConη {Γ₀}{Γ₁} p = ap (λ z → z (lift tt)) (loopcoe (ap (λ P → P → M.Con) (,Σ=0 q)) ⁻¹ ◾ ,Σ=1 q)
    where
      q = rη.Conη mInj {Γ₀} ⁻¹ ◾ ap (rη.Con mInj) p ◾ rη.Conη mInj {Γ₁}

  injTyη : {Γ₀ Γ₁ : M.Con}{A₀ : M.Ty Γ₀}{A₁ : M.Ty Γ₁} → Sη.Tyη (Γ₀ ,Σ A₀) ≡ Sη.Tyη (Γ₁ ,Σ A₁) → Γ₀ ,Σ A₀ ≡ Γ₁ ,Σ A₁
  injTyη {Γ₀}{Γ₁}{A₀}{A₁} p = ap (λ z → z (lift tt)) ( loopcoe (ap (λ P → P → Σ M.Con M.Ty) (,Σ=0 q)) ⁻¹ ◾ ,Σ=1 q)
    where
      q = rη.Tyη mInj {Γ₀ ,Σ A₀} ⁻¹ ◾ ap (rη.Ty mInj) p ◾ rη.Tyη mInj {Γ₁ ,Σ A₁}

  injTyηC : {Γ₀ Γ₁ : M.Con}{A₀ : M.Ty Γ₀}{A₁ : M.Ty Γ₁}(p : Sη.Tyη (Γ₀ ,Σ A₀) ≡ Sη.Tyη (Γ₁ ,Σ A₁))
          → Γ₀ ≡ Γ₁
  injTyηC p = ,Σ=0 (injTyη p)

  injTyηT : {Γ₀ Γ₁ : M.Con}{A₀ : M.Ty Γ₀}{A₁ : M.Ty Γ₁}(p : Sη.Tyη (Γ₀ ,Σ A₀) ≡ Sη.Tyη (Γ₁ ,Σ A₁))
          → A₀ ≡[ ap M.Ty (injTyηC p) ]≡ A₁
  injTyηT p = ,Σ=1 (injTyη p)

  -- mapping from intrinsic to extrinsic syntax

  IE' : ConTy'
  IE' = record
          { Con = Sη.Con
          ; Ty  = Sη.Ty
          ; •   = Sη.•
          ; _,_ = Sη._,_
          ; U   = Sη.U
          ; Π   = Sη.Π
          }

  module IEw where
    record Con (Γη : Sη.Con) : Set ℓ where
      field
        C  : M.Con
        Cη : Sη.Conη C ≡ Γη

    =Con : {Γη : Sη.Con}
           {C₀ C₁ : M.Con}(C₂ : C₀ ≡ C₁)
           {Cη₀ : Sη.Conη C₀ ≡ Γη}{Cη₁ : Sη.Conη C₁ ≡ Γη}
         → _≡_ {A = Con Γη}
               (record { C = C₀ ; Cη = Cη₀ })
               (record { C = C₁ ; Cη = Cη₁ })
    =Con {C₀ = C} refl = ap (λ Cη → record { C = C ; Cη = Cη }) (UIP _ _)

    record Ty (Γη : Sη.Con)(Aη : Sη.Ty) : Set ℓ where
      field
        C  : M.Con
        Cη : Sη.Conη C ≡ Γη
        T  : M.Ty C
        Tη : Sη.Tyη (C ,Σ T) ≡ Aη

    =Ty : {Γη : Sη.Con}{Aη : Sη.Ty}
          {C₀ C₁ : M.Con}(C₂ : C₀ ≡ C₁)
          {Cη₀ : Sη.Conη C₀ ≡ Γη}{Cη₁ : Sη.Conη C₁ ≡ Γη}
          {T₀ : M.Ty C₀}{T₁ : M.Ty C₁}(T₂ : T₀ ≡[ ap M.Ty C₂ ]≡ T₁)
          {Tη₀ : Sη.Tyη (C₀ ,Σ T₀) ≡ Aη}{Tη₁ : Sη.Tyη (C₁ ,Σ T₁) ≡ Aη}
         → _≡_ {A = Ty Γη Aη}
               (record { C = C₀ ; Cη = Cη₀ ; T = T₀ ; Tη = Tη₀ })
               (record { C = C₁ ; Cη = Cη₁ ; T = T₁ ; Tη = Tη₁ })
    =Ty {C₀ = C} refl {T₀ = T} refl = ap2 (λ Cη Tη → record { C = C ; Cη = Cη ; T = T ; Tη = Tη }) (UIP _ _) (UIP _ _)

    eqTcoeT : {Γη : Sη.Con}{Aη₀ Aη₁ : Sη.Ty}(p : Aη₀ ≡ Aη₁)(w : Ty Γη Aη₀)
           → M.Ty (Ty.C w) ≡ M.Ty (Ty.C (coe (ap (Ty Γη) p) w))
    eqTcoeT p w = ap M.Ty (ap (λ z → coe z (Ty.C w)) (apconst p ⁻¹) ◾ coeap p Ty.C)

    TcoeT : {Γη : Sη.Con}{Aη₀ Aη₁ : Sη.Ty}(p : Aη₀ ≡ Aη₁){w : Ty Γη Aη₀}
          → Ty.T (coe (ap (Ty Γη) p) w)
          ≡ coe (eqTcoeT p w) (Ty.T w)
    TcoeT refl = refl
  
    eqTcoeC : {Γη₀ Γη₁ : Sη.Con}(p : Γη₀ ≡ Γη₁){Aη : Sη.Ty}(w : Ty Γη₀ Aη)
           → M.Ty (Ty.C w) ≡ M.Ty (Ty.C (coe (ap (λ z → Ty z Aη) p) w))
    eqTcoeC p w = ap M.Ty (ap (λ z → coe z (Ty.C w)) (apconst p ⁻¹) ◾ coeap p Ty.C)

    TcoeC : {Γη₀ Γη₁ : Sη.Con}(p : Γη₀ ≡ Γη₁){Aη : Sη.Ty}{w : Ty Γη₀ Aη}
         → Ty.T (coe (ap (λ z → Ty z Aη) p) w)
         ≡ coe (eqTcoeC p w) (Ty.T w)
    TcoeC refl = refl
  
    abstract
      eqCT : {Γ' : Sη.Con}(Γw : Con Γ'){A' : Sη.Ty}(Aw : Ty Γ' A') → Ty.C Aw ≡ Con.C Γw
      eqCT Γw Aw = injConη (Ty.Cη Aw ◾ Con.Cη Γw ⁻¹)

    _,_ : {Γ' : Sη.Con}(Γw : Con Γ'){A' : Sη.Ty}(Aw : Ty Γ' A') → Con (Γ' Sη., A')
    Γw , Aw = record
      { C  = Γ.C M., coe (ap M.Ty (eqCT Γw Aw)) A.T
      ; Cη = Sη.,η
           ◾ ap2 Sη._,_
                 Γ.Cη
                 ( ap Sη.Tyη (,Σ= (eqCT Γw Aw ⁻¹) (coeap2' (eqCT Γw Aw)))
                 ◾ A.Tη)
      }
      where
        private module Γ = Con Γw
        private module A = Ty Aw

    U : {Γ' : Sη.Con} → Con Γ' → Ty Γ' (Sη.U Γ')
    U Γw = record
      { C  = Γ.C
      ; Cη = Γ.Cη
      ; T  = M.U Γ.C
      ; Tη = Sη.Uη ◾ ap Sη.U Γ.Cη
      }
      where
        private module Γ = Con Γw

    abstract
      eqC,T' : {Γ' : Sη.Con}(Γw : Con Γ'){A' : Sη.Ty}(Aw : Ty Γ' A')
             → A' ≡ Sη.Tyη (Con.C Γw ,Σ coe (ap M.Ty (eqCT Γw Aw)) (Ty.T Aw))
      eqC,T' Γw Aw = A.Tη ⁻¹ ◾ ap Sη.Tyη (,Σ= (eqCT Γw Aw) refl)
        where
          private module Γ = Con Γw
          private module A = Ty Aw

    abstract
      eqC,T : {Γ' : Sη.Con}(Γw : Con Γ'){A' : Sη.Ty}(Aw : Ty Γ' A'){B' : Sη.Ty}(Bw : Ty (Γ' Sη., A') B')
            → Ty.C Bw ≡ Con.C Γw M., coe (ap M.Ty (eqCT Γw Aw)) (Ty.T Aw)
      eqC,T Γw Aw Bw = injConη ( B.Cη
                               ◾ ap2 Sη._,_
                                     (Γ.Cη ⁻¹)
                                     (eqC,T' Γw Aw)
                               ◾ Sη.,η ⁻¹)
        where
          private module Γ = Con Γw
          private module A = Ty Aw
          private module B = Ty Bw

    Π : {Γ' : Sη.Con}(Γw : Con Γ'){A' : Sη.Ty}(Aw : Ty Γ' A'){B' : Sη.Ty}(Bw : Ty (Γ' Sη., A') B')
      → Ty Γ' (Sη.Π Γ' A' B')
    Π Γw Aw Bw = record
      { C  = Γ.C
      ; Cη = Γ.Cη
      ; T  = M.Π Γ.C
                 (coe (ap M.Ty (eqCT Γw Aw))     A.T)
                 (coe (ap M.Ty (eqC,T Γw Aw Bw)) B.T)
      ; Tη = Sη.Πη
           ◾ ap3 Sη.Π
                 Γ.Cη
                 (eqC,T' Γw Aw ⁻¹)
                 (ap Sη.Tyη (,Σ= (eqC,T Γw Aw Bw ⁻¹) (coeap2' (eqC,T Γw Aw Bw))) ◾ B.Tη)
      }
      where
        private module Γ = Con Γw
        private module A = Ty Aw
        private module B = Ty Bw

    ret : ConTyw IE' {ℓ}
    ret = record
      { Con = Con
      ; Ty  = Ty
      ; •   = record { C = M.• ; Cη = Sη.•η }
      ; _,_ = _,_
      ; U   = U
      ; Π   = Π
      ; pCon = λ { {Γ'}{record { C = Γ₀ ; Cη = p₀}}{record { C = Γ₁ ; Cη = p₁}}
                 → =Con (injConη (p₀ ◾ p₁ ⁻¹))
                 }
      ; pTy  = λ { {Γ'}{A'}{record { C = Γ₀ ; Cη = pC₀ ; T = A₀ ; Tη = pT₀ }}{record { C = Γ₁ ; Cη = pC₁ ; T = A₁ ; Tη = pT₁ }}
                 → =Ty (injConη (pC₀ ◾ pC₁ ⁻¹))
                       ( ap (λ z → coe z A₀)
                            (UIP (ap M.Ty (injConη (pC₀ ◾ pC₁ ⁻¹)))
                                 (ap M.Ty (,Σ=0 (injTyη (pT₀ ◾ pT₁ ⁻¹)))))
                       ◾ ,Σ=1 (injTyη (pT₀ ◾ pT₁ ⁻¹)))
                 }
      }

------------------------------------------------------------------------------
-- constructing the intrinsic recursor from the extrinsic one
------------------------------------------------------------------------------

module _ {ℓ}(M : ConTy {ℓ}) where

  SwS' : ConTyw S'
  SwS' = Sw S'
  M' = IE' M
  Mw = IEw.ret M
  f' : ConTy'ᴹ S' M'
  f' = r' M'
  fw : ConTywᴹh f' SwS' Mw
  fw = rw Mw ∘h liftSw (r' M')

  private module M    = ConTy M
  private module S    = ConTy S
  private module SwS' = ConTyw SwS'
  private module M'   = ConTy' M'
  private module Mw   = ConTyw Mw
  private module f'   = ConTy'ᴹ f'
  private module fw   = ConTywᴹh fw

  Con : S.Con → M.Con
  Con (Γ' ,Σ Γw) = Γ.C
    where
      private module Γ = IEw.Con (fw.Con Γw)

  Ty : {Γ : S.Con} → S.Ty Γ → M.Ty (Con Γ)
  Ty {Γ' ,Σ Γw}(A' ,Σ Aw) = coe (ap M.Ty (IEw.eqCT M Γ A)) A.T
    where
      Γ = fw.Con Γw
      A = fw.Ty  Aw
      private module Γ = IEw.Con Γ
      private module A = IEw.Ty  A
{-
  abstract
    • : Con S.• ≡ M.•
    • = ap (λ z → coe z (IEw.Con.C (fw.Con SwS'.•))) (apconst f'.• ⁻¹)
      ◾ coeap f'.• IEw.Con.C
      ◾ ap IEw.Con.C fw.•
      where
        private module Γ = IEw.Con (fw.Con SwS'.•)
        
  abstract
    , : {Γ : S.Con}{A : S.Ty Γ} → Con (Γ S., A) ≡ (Con Γ M., Ty {Γ} A)
    , {Γ' ,Σ Γw}{A' ,Σ Aw} = ap (λ z → coe z (IEw.Con.C (fw.Con (Γw SwS'., Aw)))) (apconst f'., ⁻¹)
                           ◾ coeap f'., IEw.Con.C
                           ◾ ap IEw.Con.C (fw., {Γ'}{Γw}{A'}{Aw})

  abstract
    U : {Γ : S.Con} → Ty {Γ} (S.U Γ) ≡ M.U (Con Γ)
    U {Γ' ,Σ Γw} = ap (λ z → coe z (IEw.Ty.T (fw.Ty (SwS'.U Γw))))
                       (UIP (ap M.Ty (IEw.eqCT M (fw.Con Γw) (fw.Ty (SwS'.U Γw))))
                            ((IEw.eqTcoeT M f'.U (fw.Ty (SwS'.U Γw)) ◾ ap (λ z → M.Ty (IEw.Ty.C z)) (fw.U {Γ'}{Γw}))))
                 ◾ coecoe (IEw.eqTcoeT M (f'.U {Γ'}) (fw.Ty (SwS'.U Γw)))
                          (ap (λ z → M.Ty (IEw.Ty.C z)) (fw.U {Γ'}{Γw}))
                          {IEw.Ty.T (fw.Ty (SwS'.U Γw))} ⁻¹
                 ◾ ap (coe (ap (λ z → M.Ty (IEw.Ty.C z)) fw.U)) (IEw.TcoeT M f'.U ⁻¹)
                 ◾ apd IEw.Ty.T (fw.U {Γ'}{Γw})

  abstract
    Π : {Γ : S.Con}{A : S.Ty Γ}{B : S.Ty (Γ S., A)}
      → Ty {Γ} (S.Π Γ A B) ≡ M.Π (Con Γ) (Ty {Γ} A) (coe (ap M.Ty (, {Γ}{A})) (Ty {Γ S., A} B))
    Π {Γ' ,Σ Γw}{A' ,Σ Aw}{B' ,Σ Bw}
      = ap (λ z → coe z (IEw.Ty.T (fw.Ty SwS'Π)))
           (UIP (ap M.Ty (IEw.eqCT M (fw.Con Γw) (fw.Ty SwS'Π)))
                (IEw.eqTcoeT M f'.Π (fw.Ty SwS'Π) ◾ ap (λ z → M.Ty (IEw.Ty.C z)) fwΠ))
      ◾ coecoe (IEw.eqTcoeT M f'Π (fw.Ty SwS'Π))
               (ap (λ z → M.Ty (IEw.Ty.C z)) fwΠ)
               {IEw.Ty.T (fw.Ty SwS'Π)} ⁻¹
      ◾ ap (coe (ap (λ z → M.Ty (IEw.Ty.C z)) fwΠ))
           (IEw.TcoeT M f'Π {fw.Ty SwS'Π} ⁻¹)
      ◾ apd IEw.Ty.T fwΠ
      ◾ ap (M.Π (Con Γ) (Ty {Γ} A))
           ( ap (coe (ap M.Ty (IEw.eqC,T M (fw.Con Γw) (fw.Ty Aw) (coe (ap (λ z → Mw.Ty z (f'.Ty B')) f',) (fw.Ty Bw)))))
                (IEw.TcoeC M f', {_}{fw.Ty Bw})
           ◾ coecoe (IEw.eqTcoeC M f', (fw.Ty Bw))
                     (ap M.Ty (IEw.eqC,T M (fw.Con Γw) (fw.Ty Aw) (coe (ap (λ z → Mw.Ty z (f'.Ty B')) f',) (fw.Ty Bw))))
           ◾ ap (λ z → coe z (IEw.Ty.T (fw.Ty Bw)))
                 (UIP (IEw.eqTcoeC M f', (fw.Ty Bw) ◾ ap M.Ty (IEw.eqC,T M (fw.Con Γw) (fw.Ty Aw) (coe (ap (λ z → Mw.Ty z (f'.Ty B')) f',) (fw.Ty Bw))))
                      (ap M.Ty (IEw.eqCT M (fw.Con (Γw SwS'., Aw)) (fw.Ty Bw)) ◾ ap M.Ty (, {Γ}{A})))
           ◾ coecoe (ap M.Ty (IEw.eqCT M (fw.Con (Γw SwS'., Aw)) (fw.Ty Bw)))
                    (ap M.Ty (, {Γ}{A}))
                    {IEw.Ty.T (fw.Ty Bw)} ⁻¹)
      where
        Γ : S.Con
        Γ = Γ' ,Σ Γw
        A : S.Ty Γ
        A = A' ,Σ Aw
        B : S.Ty (Γ S., A)
        B = B' ,Σ Bw
        f'Π = f'.Π {Γ'}{A'}{B'}
        fwΠ = fw.Π {Γ'}{Γw}{A'}{Aw}{B'}{Bw}
        SwS'Π = SwS'.Π Γw Aw Bw
        f', = f'., {Γ'}{A'}
        fw, = fw., {Γ'}{Γw}{A'}{Aw}

  r : ConTyᴹ S M
  r = record
    { Con = Con
    ; Ty  = λ {Γ} → Ty {Γ}
    ; •   = •
    ; ,   = λ {Γ}{A} → , {Γ}{A}
    ; U   = λ {Γ} → U {Γ}
    ; Π   = λ {Γ}{A}{B} → Π {Γ}{A}{B}
    }
-}
