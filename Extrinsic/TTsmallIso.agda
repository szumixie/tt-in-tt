module Extrinsic.TTsmallIso where

open import lib
open import JM

-- a small part of the syntax which is already inductive inductive
--
-- here we show that the extrinsic and the intrinsic definitions are
-- isomorphic

------------------------------------------------------------------------------
-- intrinsic syntax
------------------------------------------------------------------------------

data Con : Set
data Ty : Con → Set

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con) → Ty Γ → Con

infixl 6 _,_

data Ty where
  U : (Γ : Con) → Ty Γ
  Π : (Γ : Con)(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ

-- congruence rules

Ty= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → Ty Γ₀ ≡ Ty Γ₁
Ty= refl = refl

,= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
     {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
   → Γ₀ , A₀ ≡ Γ₁ , A₁
,= refl refl = refl

U= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → U Γ₀ ≡[ Ty= Γ₂ ]≡ U Γ₁
U= refl = refl

Π= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
     {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
     {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≡[ Ty= (,= Γ₂ A₂) ]≡ B₁)
   → Π Γ₀ A₀ B₀ ≡[ Ty= Γ₂ ]≡ Π Γ₁ A₁ B₁
Π= refl refl refl = refl

------------------------------------------------------------------------------
-- extrinsic syntax
------------------------------------------------------------------------------

-- precontexts and pretypes

data Con' : Set
data Ty' : Set

data Con' where
  •' : Con'
  _,'_ : Con' → Ty' → Con'

infixl 6 _,'_

data Ty' where
  U' : Con' → Ty'
  Π' : Con' → Ty' → Ty' → Ty'

-- congruence rules

,'= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
    → Γ₀ ,' A₀ ≡ Γ₁ ,' A₁
,'= refl refl = refl

U'= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁) → U' Γ₀ ≡ U' Γ₁
U'= refl = refl

Π'= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
      {B₀ B₁ : Ty'}(B₂ : B₀ ≡ B₁)
    → Π' Γ₀ A₀ B₀ ≡ Π' Γ₁ A₁ B₁
Π'= refl refl refl = refl

-- injectivity rules

,'inj₁ : {Γ₀ Γ₁ : Con'}{A₀ A₁ : Ty'} → Γ₀ ,' A₀ ≡ Γ₁ ,' A₁ → Γ₀ ≡ Γ₁
,'inj₁ refl = refl

,'inj₂ : {Γ₀ Γ₁ : Con'}{A₀ A₁ : Ty'} → Γ₀ ,' A₀ ≡ Γ₁ ,' A₁ → A₀ ≡ A₁
,'inj₂ refl = refl

Π'inj₁ : {Γ₀ Γ₁ : Con'}{A₀ A₁ : Ty'}{B₀ B₁ : Ty'} → Π' Γ₀ A₀ B₀ ≡ Π' Γ₁ A₁ B₁ → Γ₀ ≡ Γ₁
Π'inj₁ refl = refl

Π'inj₂ : {Γ₀ Γ₁ : Con'}{A₀ A₁ : Ty'}{B₀ B₁ : Ty'} → Π' Γ₀ A₀ B₀ ≡ Π' Γ₁ A₁ B₁ → A₀ ≡ A₁
Π'inj₂ refl = refl

Π'inj₃ : {Γ₀ Γ₁ : Con'}{A₀ A₁ : Ty'}{B₀ B₁ : Ty'} → Π' Γ₀ A₀ B₀ ≡ Π' Γ₁ A₁ B₁ → B₀ ≡ B₁
Π'inj₃ refl = refl

-- typing relations

data ⊢C_ : Con' → Set
data _⊢T_ : Con' → Ty' → Set

infix 5 ⊢C_
infix 5 _⊢T_

data ⊢C_ where
  •w   : ⊢C •'
  _,w_ : {Γ : Con'}(Γw : ⊢C Γ){A : Ty'}(Aw : Γ ⊢T A) → ⊢C (Γ ,' A)

infixl 6 _,w_

data _⊢T_ where
  Uw : {Γ : Con'}(Γw : ⊢C Γ) → Γ ⊢T U' Γ
  Πw : {Γ : Con'}(Γw : ⊢C Γ){A : Ty'}(Aw : Γ ⊢T A){B : Ty'}(Bw : Γ ,' A ⊢T B) → Γ ⊢T (Π' Γ A B)

-- eliminator

record M⊢ : Set₁ where
  field
    ⊢Cᴹ : {Γ : Con'} → ⊢C Γ → Set
    ⊢Tᴹ : {Γ : Con'}{A : Ty'} → Γ ⊢T A → Set
    •wᴹ : ⊢Cᴹ •w
    ,wᴹ : {Γ : Con'}{Γw : ⊢C Γ}(Γwᴹ : ⊢Cᴹ Γw){A : Ty'}{Aw : Γ ⊢T A}(Awᴹ : ⊢Tᴹ Aw) → ⊢Cᴹ (Γw ,w Aw)
    Uwᴹ : {Γ : Con'}{Γw : ⊢C Γ}(Γwᴹ : ⊢Cᴹ Γw) → ⊢Tᴹ (Uw Γw)
    Πwᴹ : {Γ : Con'}{Γw : ⊢C Γ}(Γwᴹ : ⊢Cᴹ Γw){A : Ty'}{Aw : Γ ⊢T A}(Awᴹ : ⊢Tᴹ Aw){B : Ty'}{Bw : Γ ,' A ⊢T B}(Bwᴹ : ⊢Tᴹ Bw) → ⊢Tᴹ (Πw Γw Aw Bw)

module Elim⊢ (m : M⊢) where
  open M⊢ m

  Elim⊢C : {Γ : Con'}(Γw : ⊢C Γ) → ⊢Cᴹ Γw
  Elim⊢T : {Γ : Con'}{A : Ty'}(Aw : Γ ⊢T A) → ⊢Tᴹ Aw

  Elim⊢C •w = •wᴹ
  Elim⊢C (Γw ,w Aw) = ,wᴹ (Elim⊢C Γw) (Elim⊢T Aw)
  Elim⊢T (Uw Γw) = Uwᴹ (Elim⊢C Γw)
  Elim⊢T (Πw Γw Aw Bw) = Πwᴹ (Elim⊢C Γw) (Elim⊢T Aw) (Elim⊢T Bw)

-- congruence rules

⊢C= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
    → ⊢C Γ₀ ≡ ⊢C Γ₁
⊢C= refl = refl

⊢T= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
    → Γ₀ ⊢T A₀ ≡ Γ₁ ⊢T A₁
⊢T= refl refl = refl

,w= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
      {Γw₀ : ⊢C Γ₀}{Γw₁ : ⊢C Γ₁}(Γw₂ : Γw₀ ≡[ ⊢C= Γ₂ ]≡ Γw₁)
      {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
      {Aw₀ : Γ₀ ⊢T A₀}{Aw₁ : Γ₁ ⊢T A₁}(Aw₂ : Aw₀ ≡[ ⊢T= Γ₂ A₂ ]≡ Aw₁)
    → Γw₀ ,w Aw₀ ≡[ ⊢C= (,'= Γ₂ A₂) ]≡ Γw₁ ,w Aw₁
,w= refl refl refl refl = refl

Uw=
  : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
    {Γw₀ : ⊢C Γ₀}{Γw₁ : ⊢C Γ₁}(Γw₂ : Γw₀ ≡[ ⊢C= Γ₂ ]≡ Γw₁)
  → Uw Γw₀ ≡[ ⊢T= Γ₂ (U'= Γ₂) ]≡ Uw Γw₁
Uw= refl refl = refl

Πw=
  : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
    {Γw₀ : ⊢C Γ₀}{Γw₁ : ⊢C Γ₁}(Γw₂ : Γw₀ ≡[ ⊢C= Γ₂ ]≡ Γw₁)
    {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
    {Aw₀ : Γ₀ ⊢T A₀}{Aw₁ : Γ₁ ⊢T A₁}(Aw₂ : Aw₀ ≡[ ⊢T= Γ₂ A₂ ]≡ Aw₁)
    {B₀ B₁ : Ty'}(B₂ : B₀ ≡ B₁)
    {Bw₀ : Γ₀ ,' A₀ ⊢T B₀}{Bw₁ : Γ₁ ,' A₁ ⊢T B₁}(Bw₂ : Bw₀ ≡[ ⊢T= (,'= Γ₂ A₂) B₂ ]≡ Bw₁)
  → Πw Γw₀ Aw₀ Bw₀ ≡[ ⊢T= Γ₂ (Π'= Γ₂ A₂ B₂) ]≡ Πw Γw₁ Aw₁ Bw₁
Πw= refl refl refl refl refl refl = refl

------------------------------------------------------------------------------
-- uniqueness of the typing relations
------------------------------------------------------------------------------

uniqC' : {Γ : Con'}(Γw₀ Γw₁ : ⊢C Γ) → Γw₀ ≡ Γw₁
uniqT' : {Γ : Con'}{A : Ty'}(Aw₀ Aw₁ : Γ ⊢T A) → Aw₀ ≡ Aw₁

uniqC' {•'} •w •w = refl
uniqC' {Γ ,' A} (Γw₀ ,w Aw₀) (Γw₁ ,w Aw₁)
  = ,w= refl (uniqC' {Γ} Γw₀ Γw₁) refl (uniqT' Aw₀ Aw₁)

uniqT' {A = U' Γ} (Uw Γw₀) (Uw Γw₁) = Uw= refl (uniqC' Γw₀ Γw₁) -- usage of K
uniqT' {A = Π' Γ A B} (Πw Γw₀ Aw₀ Bw₀) (Πw Γw₁ Aw₁ Bw₁)
  = Πw= refl (uniqC' Γw₀ Γw₁) refl (uniqT' Aw₀ Aw₁) refl (uniqT' Bw₀ Bw₁)

------------------------------------------------------------------------------
-- intrinsic to extrinsic
------------------------------------------------------------------------------

IEC' : Con → Con'
IET' : {Γ : Con}(A : Ty Γ) → Ty'

IEC' • = •'
IEC' (Γ , A) = IEC' Γ ,' IET' A
IET' (U Γ) = U' (IEC' Γ)
IET' (Π Γ A B) = Π' (IEC' Γ) (IET' A) (IET' B)

IECw : (Γ : Con) → ⊢C IEC' Γ
IETw : {Γ : Con}(A : Ty Γ) → IEC' Γ ⊢T IET' A

IECw • = •w
IECw (Γ , A) = (IECw Γ) ,w IETw A
IETw (U Γ) = Uw (IECw Γ)
IETw (Π Γ A B) = Πw (IECw Γ) (IETw A) (IETw B)

IET'coe : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){A : Ty Γ₀} → IET' (coe (Ty= Γ₂) A) ≡ IET' A
IET'coe refl = refl

IET'= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
      → IET' A₀ ≡ IET' A₁
IET'= refl refl = refl

------------------------------------------------------------------------------
-- uniqueness of Con and Ty wrt EIC' and IET'
------------------------------------------------------------------------------

uniqC : (Γ₀ Γ₁ : Con) → IEC' Γ₀ ≡ IEC' Γ₁ → Γ₀ ≡ Γ₁
uniqT : {Γ₀ : Con}{Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)(A₀ : Ty Γ₀)(A₁ : Ty Γ₁) → IET' A₀ ≡ IET' A₁ → A₀ ≡[ Ty= Γ₂ ]≡ A₁

uniqC • • refl = refl
uniqC • (Γ , A) ()
uniqC (Γ , A) • ()
uniqC (Γ₀ , A₀) (Γ₁ , A₁) p = ,= (uniqC _ _ (,'inj₁ p)) (uniqT (uniqC Γ₀ Γ₁ (,'inj₁ p)) _ _ (,'inj₂ p))

uniqT refl (U .Γ) (U Γ) refl = refl
uniqT Γ₂ (U Γ₁) (Π Γ A₁ A₂) ()
uniqT Γ₂ (Π Γ₁ A₀ A₁) (U Γ) ()
uniqT {Γ₀}{Γ₁} Γ₂ (Π Γ₀' A₀ B₀) (Π Γ₁' A₁ B₁) p = Π= Γ₂ (uniqT Γ₂ _ _ (Π'inj₂ p)) (uniqT (,= Γ₂ (uniqT Γ₂ A₀ A₁ (Π'inj₂ p))) _ _ (Π'inj₃ p))

------------------------------------------------------------------------------
-- extrinsic to intrinsic
------------------------------------------------------------------------------

mEI : M⊢
mEI = record
  { ⊢Cᴹ = λ {Γ'} Γw → Σ Con λ Γ → IEC' Γ ≡ Γ'
  ; ⊢Tᴹ = λ {Γ'}{A'} Aw → Σ Con λ Γ → Σ (Ty Γ) λ A → (IEC' Γ ≡ Γ') × (IET' A ≡ A')
  ; •wᴹ = • ,Σ refl
  ; ,wᴹ = λ { {Γ'}{Γw}(Γ ,Σ pΓ){A'}{Aw}(Γ₁ ,Σ (A ,Σ (pΓ₁ ,Σ pA))) → Γ₁ , A ,Σ ,'= pΓ₁ pA }
  ; Uwᴹ = λ { {Γ'}{Γw}(Γ ,Σ pΓ) → Γ ,Σ (U Γ ,Σ (pΓ ,Σ (U'= pΓ))) }
  ; Πwᴹ = λ { {Γ'}{Γw}(Γ ,Σ pΓ){A'}{Aw}(Γ₁ ,Σ (A ,Σ (pΓ₁ ,Σ pA))){B'}{Bw}(ΓA ,Σ (B ,Σ (pΓA ,Σ pB)))
          → Γ₁ ,Σ (Π Γ₁ A (coe (Ty= (uniqC _ _ (pΓA ◾ ,'= pΓ₁ pA ⁻¹))) B) ,Σ (pΓ₁ ,Σ (Π'= pΓ₁ pA (IET'coe (uniqC ΓA (Γ₁ , A) (pΓA ◾ ,'= pΓ₁ pA ⁻¹)) ◾ pB)))) }
  }

EIC : {Γ : Con'}(Γw : ⊢C Γ) → Con
EIT : {Γ : Con'}(Γw : ⊢C Γ){A : Ty'}(Aw : Γ ⊢T A) → Σ Con Ty

EIC Γw = proj₁ (Elim⊢.Elim⊢C mEI Γw)
EIT Γw Aw = proj₁ (Elim⊢.Elim⊢T mEI Aw) ,Σ proj₁ (proj₂ (Elim⊢.Elim⊢T mEI Aw))

------------------------------------------------------------------------------
-- intrinsic ↦ extrinsic ↦ intrinsic
------------------------------------------------------------------------------

,ΣConTy=
  : {Γ Γ' : Con}{A : Ty Γ}{A' : Ty Γ'}(p : Γ ≡ Γ')(q : A ≡[ Ty= p ]≡ A') → _≡_ {A = Σ Con Ty} (Γ ,Σ A) (Γ' ,Σ A')
,ΣConTy= refl refl = refl

,ΣConTy=0
  : {Γ Γ' : Con}{A : Ty Γ}{A' : Ty Γ'}
  → _≡_ {A = Σ Con Ty} (Γ ,Σ A) (Γ' ,Σ A') → Γ ≡ Γ'
,ΣConTy=0 refl = refl

,ΣConTy=1
  : {Γ Γ' : Con}{A : Ty Γ}{A' : Ty Γ'}(r : _≡_ {A = Σ Con Ty} (Γ ,Σ A) (Γ' ,Σ A'))
  → A ≡[ Ty= (,ΣConTy=0 r) ]≡ A'
,ΣConTy=1 refl = refl

IIC : (Γ : Con) → EIC (IECw Γ) ≡ Γ
IIT : {Γ : Con}(A : Ty Γ) → EIT (IECw Γ) (IETw A) ≡ Γ ,Σ A

IIC • = refl
IIC (Γ , A) = ,= (uniqC _ _ (proj₁ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI (IETw A)))) ◾ proj₂ (Elim⊢.Elim⊢C mEI (IECw Γ)) ⁻¹) ◾ IIC Γ)
                 (uniqT
                    (uniqC _ _ (proj₁ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI (IETw A)))) ◾ proj₂ (Elim⊢.Elim⊢C mEI (IECw Γ)) ⁻¹) ◾ IIC Γ)
                     _ _
                    (proj₂ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI (IETw A))))))

IIT (U Γ) = ,ΣConTy= (IIC Γ)
                     (uniqT (IIC Γ) _ _
                            (U'= (proj₂ (Elim⊢.Elim⊢C mEI (IECw Γ)))))
IIT (Π Γ A B)
  = ,ΣConTy=
      (,ΣConTy=0 (IIT A))
      (Π= (,ΣConTy=0 (IIT A))
          (,ΣConTy=1 (IIT A))
          (from≃ ( uncoe (Ty= (,= (,ΣConTy=0 (IIT A)) (,ΣConTy=1 (IIT A)))) ⁻¹̃
                 ◾̃ uncoe ((Ty= (uniqC (proj₁ (Elim⊢.Elim⊢T mEI (IETw B))) (proj₁ (Elim⊢.Elim⊢T mEI (IETw A)) , proj₁ (proj₂ (Elim⊢.Elim⊢T mEI (IETw A)))) (proj₁ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI (IETw B)))) ◾ ,'= (proj₁ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI (IETw A))))) (proj₂ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI (IETw A))))) ⁻¹)))) ⁻¹̃
                 ◾̃ from≡ (Ty= (,ΣConTy=0 (IIT B))) (,ΣConTy=1 (IIT B)))))

------------------------------------------------------------------------------
-- extrinsic ↦ intrinsic ↦ extrinsic
------------------------------------------------------------------------------

EEC' : {Γ' : Con'}(Γw : ⊢C Γ') → IEC' (EIC Γw) ≡ Γ'
EET' : {Γ' : Con'}(Γw : ⊢C Γ'){A' : Ty'}(Aw : Γ' ⊢T A') → IET' (proj₂ (EIT Γw Aw)) ≡ A'

EEC' •w = refl
EEC' (Γw ,w Aw)
  = ,'= (ap IEC' (uniqC _ _ (proj₁ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI Aw))) ◾ proj₂ (Elim⊢.Elim⊢C mEI Γw) ⁻¹)) ◾ EEC' Γw)
        (EET' Γw Aw)

EET' Γw (Uw Γw₁) = U'= (EEC' Γw₁)
EET' Γw (Πw Γw₁ {A'} Aw {B'} Bw)
  = Π'= (ap IEC' (uniqC _ _ (proj₁ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI Aw))) ◾ proj₂ (Elim⊢.Elim⊢C mEI Γw) ⁻¹)) ◾ EEC' Γw)
        (EET' Γw Aw)
        ( IET'coe (uniqC (proj₁ (Elim⊢.Elim⊢T mEI Bw)) (proj₁ (Elim⊢.Elim⊢T mEI Aw) , proj₁ (proj₂ (Elim⊢.Elim⊢T mEI Aw))) (proj₁ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI Bw))) ◾ ,'= (proj₁ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI Aw)))) (proj₂ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI Aw)))) ⁻¹))
        ◾ EET' (Γw ,w Aw) Bw)

EECw : {Γ' : Con'}(Γw : ⊢C Γ') → IECw (EIC Γw) ≡[ ⊢C= (EEC' Γw) ]≡ Γw
EECw Γw = uniqC' _ _

EETw : {Γ' : Con'}(Γw : ⊢C Γ'){A' : Ty'}(Aw : Γ' ⊢T A')
     → IETw (proj₂ (EIT Γw Aw))
     ≡[ ⊢T= (ap IEC' (uniqC _ _ (proj₁ (proj₂ (proj₂ (Elim⊢.Elim⊢T mEI Aw))) ◾ proj₂ (Elim⊢.Elim⊢C mEI Γw) ⁻¹)) ◾ EEC' Γw) (EET' Γw Aw) ]≡
       Aw
EETw Γw Aw = uniqT' _ _

------------------------------------------------------------------------------
-- relation of ⊢C and ⊢T
------------------------------------------------------------------------------

⊢CT : {Γ' : Con'}{A' : Ty'} → Γ' ⊢T A' → ⊢C Γ'
⊢CT (Uw Γw) = Γw
⊢CT (Πw Γw Aw Bw) = Γw
