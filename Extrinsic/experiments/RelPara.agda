module Extrinsic.RelPara where

open import lib

open import Extrinsic.Presyntax

------------------------------------------------------------------------------
-- typing relations
------------------------------------------------------------------------------

-- this is not paranoid

data ⊢C_    : Con' → Set
data _⊢T_   : Con' → Ty' → Set
data _⊢s_∶_ : Con' → Tms' → Con' → Set
data _⊢t_∶_ : Con' → Tm' → Ty' → Set

-- conversion

data ⊢C_~_  : Con' → Con' → Set
data _⊢T_~_ : Con' → Ty' → Ty' → Set
data _⊢s_~_∶_ : Con' → Tms' → Tms' → Con' → Set
data _⊢t_~_∶_ : Con' → Tm' → Tm' → Ty' → Set

infix 4 ⊢C_~_ _⊢T_~_ _⊢s_~_∶_ _⊢t_~_∶_ ⊢C_ _⊢T_ _⊢s_∶_ _⊢t_∶_
infixl 4 _◾C_ _◾T_ _◾s_ _◾t_
infix 5 _⁻¹C _⁻¹T _⁻¹s _⁻¹t
infixl 6 _,w_

data ⊢C_ where
  •w   : ⊢C •'
  _,w_ : {Γ : Con'}(Γw : ⊢C Γ){A : Ty'}(Aw : Γ ⊢T A) → ⊢C (Γ ,' A)

data _⊢T_ where
  []Tw
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {A : Ty'}(Aw : Δ ⊢T A){σ : Tms'}(σw : Γ ⊢s σ ∶ Δ) → Γ ⊢T (A [ σ ]T')

  -- coercion
  coeT : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁)
         {A : Ty'}(Aw : Γ₀ ⊢T A) → Γ₁ ⊢T A

data _⊢s_∶_ where
  idw
    : {Γ : Con'}(Γw : ⊢C Γ) → Γ ⊢s id' Γ ∶ Γ
  ∘w
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ){Σ : Con'}(Σw : ⊢C Σ)
      {σ ν : Tms'}(σw : Δ ⊢s σ ∶ Σ)(νw : Γ ⊢s ν ∶ Δ)
    → Γ ⊢s σ ∘' ν ∶ Σ
  εw
    : {Γ : Con'}(Γw : ⊢C Γ) → Γ ⊢s ε' Γ ∶ •'
  ,sw
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {σ : Tms'}(σw : Γ ⊢s σ ∶ Δ)
      {A : Ty'}(Aw : Δ ⊢T A)
      {t : Tm'}(tw : Γ ⊢t t ∶ A [ σ ]T')
    → Γ ⊢s (σ ,s' t) {A} ∶ Δ ,' A
  π₁w
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {A : Ty'}(Aw : Δ ⊢T A){σ : Tms'}(σw : Γ ⊢s σ ∶ Δ ,' A)
    → Γ ⊢s π₁' {Δ} σ ∶ Δ

  -- coercion
  coes : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δ~ : ⊢C Δ₀ ~ Δ₁)
         {σ : Tms'}(σw : Γ₀ ⊢s σ ∶ Δ₀) → Γ₁ ⊢s σ ∶ Δ₁

data _⊢t_∶_ where
  []tw
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {A : Ty'}(Aw : Δ ⊢T A)
      {t : Tm'}(tw : Δ ⊢t t ∶ A){σ : Tms'}(σw : Γ ⊢s σ ∶ Δ)
    → Γ ⊢t t [ σ ]t' ∶ A [ σ ]T'
  π₂w
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {A : Ty'}(Aw : Δ ⊢T A)
      {σ : Tms'}(σw : Γ ⊢s σ ∶ Δ ,' A)
    → Γ ⊢t π₂' {A} σ ∶ A [ π₁' {Δ} σ ]T'

  -- coercion
  coet : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){A₀ A₁ : Ty'}(A~ : Γ₀ ⊢T A₀ ~ A₁)
         {t : Tm'}(tw : Γ₀ ⊢t t ∶ A₀) → Γ₁ ⊢t t ∶ A₁

-- conversion relations

data ⊢C_~_ where
  -- PER
  _◾C_ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){Γ₂ : Con'}(Γ~' : ⊢C Γ₁ ~ Γ₂) → ⊢C Γ₀ ~ Γ₂
  _⁻¹C : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁) → ⊢C Γ₁ ~ Γ₀

  -- congruence
  •c   : ⊢C •' ~ •' -- this might not be needed
  _,c_ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁)
         {A₀ A₁ : Ty'}(A~ : Γ₀ ⊢T A₀ ~ A₁) -- an assymetry (later many such occurrences)
       → ⊢C Γ₀ ,' A₀ ~ Γ₁ ,' A₁

data _⊢T_~_ where
  -- PER
  _◾T_ : {Γ : Con'}{A₀ A₁ : Ty'}(A~ : Γ ⊢T A₀ ~ A₁){A₂ : Ty'}(A~' : Γ ⊢T A₁ ~ A₂) → Γ ⊢T A₀ ~ A₂
  _⁻¹T : {Γ : Con'}{A₀ A₁ : Ty'}(A~ : Γ ⊢T A₀ ~ A₁) → Γ ⊢T A₁ ~ A₀

  -- coercion
  coeT~ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){A₀ A₁ : Ty'}(A~ : Γ₀ ⊢T A₀ ~ A₁) → Γ₁ ⊢T A₀ ~ A₁

  -- congruence
  []Tc
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {A₀ A₁ : Ty'}(A~ : Δ ⊢T A₀ ~ A₁)
      {σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ)
    → Γ ⊢T A₀ [ σ₀ ]T' ~ A₁ [ σ₁ ]T'
  -- the paranoid version of _[_]Tc would also require the following
  -- arguments: (Γ~ : ⊢C Γ₀ ~ Γ₁) and (Δ~ : ⊢C Δ₀ ~ Δ₁)

  -- conversion
  [id]T
    : {Γ : Con'}(Γw : ⊢C Γ)
      {A : Ty'}(Aw : Γ ⊢T A)
    → Γ ⊢T A [ id' Γ ]T' ~ A
  [][]T
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ){Σ : Con'}(Σw : ⊢C Σ)
      {A : Ty'}(Aw : Σ ⊢T A)
      {σ : Tms'}(σw : Γ ⊢s σ ∶ Δ){δ : Tms'}(δw : Δ ⊢s δ ∶ Σ)
    → Γ ⊢T (A [ δ ]T' [ σ ]T') ~ (A [ δ ∘' σ ]T')

data _⊢s_~_∶_ where
  -- PER
  _◾s_ : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ){σ₂ : Tms'}(σ~' : Γ ⊢s σ₁ ~ σ₂ ∶ Δ) → Γ ⊢s σ₀ ~ σ₂ ∶ Δ
  _⁻¹s : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → Γ ⊢s σ₁ ~ σ₀ ∶ Δ

  -- coercion
  coes~ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δ~ : ⊢C Δ₀ ~ Δ₁)
          {σ₀ σ₁ : Tms'}(σ~ : Γ₀ ⊢s σ₀ ~ σ₁ ∶ Δ₀) → Γ₁ ⊢s σ₀ ~ σ₁ ∶ Δ₁

  -- congruence
  idc
    : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁) → Γ₀ ⊢s id' Γ₀ ~ id' Γ₁ ∶ Γ₀
  ∘c
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ){Σ : Con'}(Σw : ⊢C Σ)
      {σ₀ σ₁ : Tms'}(σ~ : Δ ⊢s σ₀ ~ σ₁ ∶ Σ)
      {ν₀ ν₁ : Tms'}(ν~ : Γ ⊢s ν₀ ~ ν₁ ∶ Δ)
    → Γ ⊢s σ₀ ∘' ν₀ ~ σ₁ ∘' ν₁ ∶ Σ
  εc
    : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁)
    → Γ₀ ⊢s ε' Γ₀ ~ ε' Γ₁ ∶ •'
  ,sc
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ)
    → {A₀ A₁ : Ty'}(A~ : Δ ⊢T A₀ ~ A₁)
      {t₀ t₁ : Tm'}(t~ : Γ ⊢t t₀ ~ t₁ ∶ A₀ [ σ₀ ]T')
    → Γ ⊢s (σ₀ ,s' t₀) {A₀} ~ (σ₁ ,s' t₁) {A₁} ∶ Δ ,' A₀
  π₁c
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {A₀ A₁ : Ty'}(A~ : Δ ⊢T A₀ ~ A₁)
      {σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ ,' A₀)
    → Γ ⊢s π₁' {Δ} σ₀ ~ π₁' {Δ} σ₁ ∶ Δ

  -- conversion
  idl
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {δ : Tms'}(δw : Γ ⊢s δ ∶ Δ) → Γ ⊢s id' Δ ∘' δ ~ δ ∶ Δ
  idr
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {δ : Tms'}(δw : Γ ⊢s δ ∶ Δ) → Γ ⊢s δ ∘' id' Γ ~ δ ∶ Δ
  ass
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ){Σ : Con'}(Σw : ⊢C Σ){Ω : Con'}(Ωw : ⊢C Ω)
      {σ : Tms'}(σw : Σ ⊢s σ ∶ Ω)
      {δ : Tms'}(δw : Γ ⊢s δ ∶ Σ)
      {ν : Tms'}(ν' : Δ ⊢s ν ∶ Γ)
    → Δ ⊢s ((σ ∘' δ) ∘' ν) ~ (σ ∘' (δ ∘' ν)) ∶ Ω
  ,∘
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ){Σ : Con'}(Σw : ⊢C Σ)
      {δ : Tms'}(δw : Γ ⊢s δ ∶ Δ)
      {σ : Tms'}(σw : Σ ⊢s σ ∶ Γ)
      {A : Ty'}(Aw : Δ ⊢T A)
      {a : Tm'}(aw : Γ ⊢t a ∶ (A [ δ ]T'))
    → Σ ⊢s ((δ ,s' a) {A} ∘' σ) ~ ((δ ∘' σ) ,s' (a [ σ ]t')) {A} ∶ Δ ,' A
  π₁β
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {A : Ty'}(Aw : Δ ⊢T A)
      {δ : Tms'}(δw : Γ ⊢s δ ∶ Δ)
      {a : Tm'}(aw : Γ ⊢t a ∶ (A [ δ ]T'))
    → Γ ⊢s π₁' {Δ} ((δ ,s' a) {A}) ~ δ ∶ Δ
  πη
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {A : Ty'}(Aw : Δ ⊢T A)
      {δ : Tms'}(δw : Γ ⊢s δ ∶ (Δ ,' A))
    → Γ ⊢s (π₁' {Δ} δ ,s' π₂' {A} δ) {A} ~ δ ∶ Δ ,' A
  εη
    : {Γ : Con'}(Γw : ⊢C Γ)
      {σ : Tms'}(σw : Γ ⊢s σ ∶ •') → Γ ⊢s σ ~ ε' Γ ∶ •'

data _⊢t_~_∶_ where
  -- PER
  _◾t_ : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(t~ : Γ ⊢t t₀ ~ t₁ ∶ A){t₂ : Tm'}(t~' : Γ ⊢t t₁ ~ t₂ ∶ A) → Γ ⊢t t₀ ~ t₂ ∶ A
  _⁻¹t : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(t~ : Γ ⊢t t₀ ~ t₁ ∶ A) → Γ ⊢t t₁ ~ t₀ ∶ A

  -- coercion
  coet~ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){A₀ A₁ : Ty'}(A~ : Γ₀ ⊢T A₀ ~ A₁)
          {t₀ t₁ : Tm'}(t~ : Γ₀ ⊢t t₀ ~ t₁ ∶ A₀) → Γ₁ ⊢t t₀ ~ t₁ ∶ A₁
         
  -- congruence
  []tc
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {A : Ty'}(Aw : Δ ⊢T A)
      {t₀ t₁ : Tm'}(t~ : Δ ⊢t t₀ ~ t₁ ∶ A)
      {σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ)
    → Γ ⊢t t₀ [ σ₀ ]t' ~ t₁ [ σ₁ ]t' ∶ A [ σ₀ ]T'
  π₂c
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {A₀ A₁ : Ty'}(A~ : Δ ⊢T A₀ ~ A₁)
      {σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ ,' A₀)
    → Γ ⊢t π₂' {A₀} σ₀ ~ π₂' {A₁} σ₁ ∶ A₀ [ π₁' {Δ} σ₀ ]T'

  -- conversion
  π₂β
    : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
      {A : Ty'}(Aw : Δ ⊢T A)
      {δ : Tms'}(δw : Γ ⊢s δ ∶ Δ)
      {a : Tm'}(aw : Γ ⊢t a ∶ (A [ δ ]T'))
    → Γ ⊢t (π₂' {A} ((δ ,s' a) {A})) ~ a ∶ A [ δ ]T'

------------------------------------------------------------------------------
-- reflexivity of conversion relations
------------------------------------------------------------------------------

reflC : {Γ : Con'} → ⊢C Γ → ⊢C Γ ~ Γ
reflT : {Γ : Con'}{A : Ty'} → Γ ⊢T A → Γ ⊢T A ~ A
refls : {Γ Δ : Con'}{σ : Tms'} → Γ ⊢s σ ∶ Δ → Γ ⊢s σ ~ σ ∶ Δ
reflt : {Γ : Con'}{A : Ty'}{t : Tm'} → Γ ⊢t t ∶ A → Γ ⊢t t ~ t ∶ A

reflC •w = •c
reflC (Γw ,w Aw) = reflC Γw ,c reflT Aw

reflT ([]Tw Γw Δw Aw σw) = []Tc Γw Δw (reflT Aw) (refls σw)
reflT (coeT Γ~ Aw) = coeT~ Γ~ (reflT Aw)

refls (idw Γw) = idc (reflC Γw)
refls (∘w Γw Δw Σw σw νw) = ∘c Γw Δw Σw (refls σw) (refls νw)
refls (εw Γw) = εc (reflC Γw)
refls (,sw Γw Δw σw Aw tw) =  ,sc Γw Δw (refls σw) (reflT Aw) (reflt tw) 
refls (π₁w Γw Δw Aw σw) = π₁c Γw Δw (reflT Aw) (refls σw)
refls (coes Γ~ Δ~ σw) = coes~ Γ~ Δ~ (refls σw)

reflt ([]tw Γw Δw Aw tw σw) = []tc Γw Δw Aw (reflt tw) (refls σw)
reflt (π₂w Γw Δw Aw σw) = π₂c Γw Δw (reflT Aw) (refls σw)
reflt (coet Γ~ A~ tw) = coet~ Γ~ A~ (reflt tw)

------------------------------------------------------------------------------
-- relation of different well-typedness relations
------------------------------------------------------------------------------

⊢C₀ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁) → ⊢C Γ₀
⊢C₁ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁) → ⊢C Γ₁
⊢T₀ : {Γ : Con'}{A₀ A₁ : Ty'}(A~ : Γ ⊢T A₀ ~ A₁) → Γ ⊢T A₀
⊢T₁ : {Γ : Con'}{A₀ A₁ : Ty'}(A~ : Γ ⊢T A₀ ~ A₁) → Γ ⊢T A₁
⊢s₀ : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → Γ ⊢s σ₀ ∶ Δ
⊢s₁ : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → Γ ⊢s σ₁ ∶ Δ
⊢t₀ : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(t~ : Γ ⊢t t₀ ~ t₁ ∶ A) → Γ ⊢t t₀ ∶ A
⊢t₁ : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(t~ : Γ ⊢t t₀ ~ t₁ ∶ A) → Γ ⊢t t₁ ∶ A

⊢C₀ (Γ~ ◾C Γ~₁) = ⊢C₀ Γ~
⊢C₀ (Γ~ ⁻¹C) = ⊢C₁ Γ~
⊢C₀ •c = •w
⊢C₀ (Γ~ ,c A~) = ⊢C₀ Γ~ ,w ⊢T₀ A~
⊢C₁ (Γ~ ◾C Γ~₁) = ⊢C₁ Γ~₁
⊢C₁ (Γ~ ⁻¹C) = ⊢C₀ Γ~
⊢C₁ •c = •w
⊢C₁ (Γ~ ,c A~) = ⊢C₁ Γ~ ,w coeT Γ~ (⊢T₁ A~)
⊢T₀ (A~ ◾T A~₁) = ⊢T₀ A~
⊢T₀ (A~ ⁻¹T) = ⊢T₁ A~
⊢T₀ (coeT~ Γ~ A~) = coeT Γ~ (⊢T₀ A~)
⊢T₀ ([]Tc Γw Δw A~ σ~) = []Tw Γw Δw (⊢T₀ A~) (⊢s₀ σ~)
⊢T₀ ([id]T Γw Aw) = []Tw Γw Γw Aw (idw Γw)
⊢T₀ ([][]T Γw Δw Σw Aw σw δw) = []Tw Γw Δw ([]Tw Δw Σw Aw δw) σw
⊢T₁ (A~ ◾T A~₁) = ⊢T₁ A~₁
⊢T₁ (A~ ⁻¹T) = ⊢T₀ A~
⊢T₁ (coeT~ Γ~ A~) = coeT Γ~ (⊢T₁ A~)
⊢T₁ ([]Tc Γw Δw A~ σ~) = []Tw Γw Δw (⊢T₁ A~) (⊢s₁ σ~)
⊢T₁ ([id]T Γw Aw) = Aw
⊢T₁ ([][]T Γw Δw Σw Aw σw δw) = []Tw Γw Σw Aw (∘w Γw Δw Σw δw σw)
⊢s₀ (σ~ ◾s σ~₁) = ⊢s₀ σ~
⊢s₀ (σ~ ⁻¹s) = ⊢s₁ σ~
⊢s₀ (coes~ Γ~ Δ~ σ~) = {!!}
⊢s₀ (idc Γ~) = idw (⊢C₀ Γ~)
⊢s₀ (∘c Γw Δw Σw σ~ ν~) = ∘w Γw Δw Σw (⊢s₀ σ~) (⊢s₀ ν~)
⊢s₀ (εc Γ~) = εw (⊢C₀ Γ~)
⊢s₀ (,sc Γw Δw σ~ A~ t~) = {!!}
⊢s₀ (π₁c Γw Δw A~ σ~) = {!!}
⊢s₀ (idl Γw Δw δw) = {!!}
⊢s₀ (idr Γw Δw δw) = {!!}
⊢s₀ (ass Γw Δw Σw Ωw σw δw ν') = {!!}
⊢s₀ (,∘ Γw Δw Σw δw σw Aw aw) = {!!}
⊢s₀ (π₁β Γw Δw Aw δw aw) = {!!}
⊢s₀ (πη Γw Δw Aw δw) = {!!}
⊢s₀ (εη Γw σw) = {!!}
⊢s₁ σ~ = {!!}
⊢t₀ t~ = {!!}
⊢t₁ t~ = {!!}

⊢TC     : {Γ : Con'}{A : Ty'} → Γ ⊢T A → ⊢C Γ
⊢sC-dom : {Γ Δ : Con'}{σ : Tms'} → Γ ⊢s σ ∶ Δ → ⊢C Γ
⊢sC-cod : {Γ Δ : Con'}{σ : Tms'} → Γ ⊢s σ ∶ Δ → ⊢C Δ
⊢tC     : {Γ : Con'}{A : Ty'}{t : Tm'} → Γ ⊢t t ∶ A → ⊢C Γ
⊢tT     : {Γ : Con'}{A : Ty'}{t : Tm'} → Γ ⊢t t ∶ A → Γ ⊢T A

⊢TC ([]Tw Γw Δw Aw σw) = Γw
⊢TC (coeT Γ~ Aw) = {!!}
⊢sC-dom (idw Γw) = Γw
⊢sC-dom (∘w Γw Δw Σw σw σw₁) = Γw
⊢sC-dom (εw Γw) = Γw
⊢sC-dom (,sw Γw Δw σw Aw tw) = Γw
⊢sC-dom (π₁w Γw Δw Aw σw) = Γw
⊢sC-dom (coes Γ~ Δ~ σw) = {!⊢C₁ Γ~!}
⊢sC-cod (idw Γw) = Γw
⊢sC-cod (∘w Γw Δw Σw σw σw₁) = Σw
⊢sC-cod (εw Γw) = •w
⊢sC-cod (,sw Γw Δw σw Aw tw) = Δw ,w Aw
⊢sC-cod (π₁w Γw Δw Aw σw) = Δw
⊢sC-cod (coes Γ~ Δ~ σw) = {!!}
⊢tC ([]tw Γw Δw Aw tw σw) = Γw
⊢tC (π₂w Γw Δw Aw σw) = Γw
⊢tC (coet Γ~ A~ tw) = {!!}
⊢tT ([]tw Γw Δw Aw tw σw) = []Tw Γw Δw Aw σw
⊢tT (π₂w Γw Δw Aw σw) = []Tw Γw Δw Aw (π₁w Γw Δw Aw σw)
⊢tT (coet Γ~ A~ tw) = {!!}







{-
~C₀ : {Γ₀ Γ₁ : Con'}(Γ₀w : ⊢C Γ₀)(Γ~ : ⊢C Γ₀ ~ Γ₁) → ⊢C Γ₁
~C₁ : {Γ₀ Γ₁ : Con'}(Γ₁w : ⊢C Γ₁)(Γ~ : ⊢C Γ₀ ~ Γ₁) → ⊢C Γ₀

~T₀ : {Γ : Con'}{A₀ A₁ : Ty'}(A₀w : Γ ⊢T A₀)(A~ : Γ ⊢T A₀ ~ A₁) → Γ ⊢T A₁
~T₁ : {Γ : Con'}{A₀ A₁ : Ty'}(A₁w : Γ ⊢T A₁)(A~ : Γ ⊢T A₀ ~ A₁) → Γ ⊢T A₀

~C₀ Γ₀w (Γ~ ◾C Γ~₁) = ~C₀ (~C₀ Γ₀w Γ~) Γ~₁
~C₀ Γ₀w (Γ~ ⁻¹C) = ~C₁ Γ₀w Γ~
~C₀ Γ₀w •c = •w
~C₀ (Γ₀w ,w A₀w) (Γ~ ,c A~) = ~C₀ Γ₀w Γ~ ,w coeT Γ~ (~T₀ A₀w A~)
~C₁ Γ₁w (Γ~ ◾C Γ~₁) = ~C₁ (~C₁ Γ₁w Γ~₁) Γ~
~C₁ Γ₁w (Γ~ ⁻¹C) = ~C₀ Γ₁w Γ~
~C₁ Γ₁w •c = •w
~C₁ (Γ₁w ,w A₁w) (Γ~ ,c A~) = ~C₁ Γ₁w Γ~ ,w coeT (Γ~ ◾C Γ~ ⁻¹C) (~T₁ (coeT (Γ~ ⁻¹C) A₁w) A~)

~T₀ A₀w (A~ ◾T A~₁) = ~T₀ (~T₀ A₀w A~) A~₁
~T₀ A₀w (A~ ⁻¹T) = ~T₁ A₀w A~
~T₀ A₀w (coeT~ Γ~ A~) = coeT Γ~ (~T₀ (coeT (Γ~ ⁻¹C) A₀w) A~)
~T₀ A₀w (A~ [ σ~ ]Tc) = ~T₀ {!!} A~ [ {!!} ]Tw
~T₀ A₀w ([id]T Aw) = Aw
~T₀ A₀w ([][]T Aw σw δw) = Aw [ δw ∘w σw ]Tw
~T₁ A₁w (A~ ◾T A~₁) = ~T₁ (~T₁ A₁w A~₁) A~
~T₁ A₁w (A~ ⁻¹T) = ~T₀ A₁w A~
~T₁ A₁w (coeT~ Γ~ A~) = coeT Γ~ (~T₁ (coeT (Γ~ ⁻¹C) A₁w) A~)
~T₁ A₁w (A~ [ σ~ ]Tc) = {!!}
~T₁ A₁w ([id]T Aw) = Aw [ {!!} ]Tw
~T₁ A₁w ([][]T Aw σw δw) = Aw [ δw ]Tw [ σw ]Tw
-}
-- 
{-
⊢TC : {Γ : Con'}{A : Ty'} → Γ ⊢T A → ⊢C Γ
⊢sC-dom : {Γ Δ : Con'}{σ : Tms'} → Γ ⊢s σ ∶ Δ → ⊢C Γ
⊢sC-cod : {Γ Δ : Con'}{σ : Tms'} → Γ ⊢s σ ∶ Δ → ⊢C Δ
⊢tC : {Γ : Con'}{A : Ty'}{t : Tm'} → Γ ⊢t t ∶ A → ⊢C Γ
⊢tT : {Γ : Con'}{A : Ty'}{t : Tm'} → Γ ⊢t t ∶ A → Γ ⊢T A

-- Γ ⊢T A → ⊢C Γ

⊢TC (Aw [ σw ]Tw) = ⊢sC-dom σw
⊢TC (coeT Γ~ Aw) = {!(⊢TC Aw)!} -- ⊢C₁ Γ~

-- Γ ⊢s σ ∶ Δ → ⊢C Γ

⊢sC-dom (idw Γw) = Γw
⊢sC-dom (σw ∘w νw) = ⊢sC-dom νw
⊢sC-dom (εw Γw) = Γw
⊢sC-dom (,sw σw tw Aw) = ⊢sC-dom σw
⊢sC-dom (π₁w Aw σw) = ⊢sC-dom σw
⊢sC-dom (coes Γ~ Δ~ σw) = {!!} -- ⊢C₁ Γ~

-- Γ ⊢s σ ∶ Δ → ⊢C Δ

⊢sC-cod (idw Γw) = Γw
⊢sC-cod (σw ∘w νw) = ⊢sC-cod σw
⊢sC-cod (εw Γw) = •w
⊢sC-cod (,sw σw tw Aw) = ⊢sC-cod σw ,w Aw
⊢sC-cod (π₁w Aw σw) = ⊢TC Aw
⊢sC-cod (coes Γ~ Δ~ σw) = {!!} -- ⊢C₁ Δ~

⊢tC (tw [ σw ]tw) = ⊢sC-dom σw
⊢tC (π₂w Aw σw) = ⊢sC-dom σw
⊢tC (coet Γ~ A~ tw) = {!!} -- ⊢C₁ Γ~

⊢tT (tw [ σw ]tw) = ⊢tT tw [ σw ]Tw
⊢tT (π₂w Aw σw) = Aw [ π₁w Aw σw ]Tw
⊢tT (coet Γ~ A~ tw) = coeT Γ~ {!!} -- (⊢T₁ A~)
-}
------------------------------------------------------------------------------
-- relation of different well-typedness relations
------------------------------------------------------------------------------
{-
⊢TC : {Γ : Con'}{A₀ A₁ : Ty'}(A~ : Γ ⊢T A₀ ~ A₁) → ⊢C Γ
⊢sC-dom : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → ⊢C Γ
⊢sC-cod : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → ⊢C Δ

-- only well-typed things are convertible

⊢C₀ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁) → ⊢C Γ₀
⊢C₁ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁) → ⊢C Γ₁
⊢T₀ : {Γ : Con'}{A₀ A₁ : Ty'}(A~ : Γ ⊢T A₀ ~ A₁) → Γ ⊢T A₀
⊢T₁ : {Γ : Con'}{A₀ A₁ : Ty'}(A~ : Γ ⊢T A₀ ~ A₁) → Γ ⊢T A₁
⊢s₀ : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → Γ ⊢s σ₀ ∶ Δ
⊢s₁ : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → Γ ⊢s σ₁ ∶ Δ
⊢t₀ : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(t~ : Γ ⊢t t₀ ~ t₁ ∶ A) → Γ ⊢t t₀ ∶ A
⊢t₁ : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(t~ : Γ ⊢t t₀ ~ t₁ ∶ A) → Γ ⊢t t₁ ∶ A

-- Γ ⊢T A₀ ~ A₁ → ⊢C Γ

⊢TC (Aw ◾T Aw₁) = ⊢TC Aw
⊢TC (Aw ⁻¹T) = ⊢TC Aw
⊢TC (coeT~ Γ~ Aw) = ⊢C₁ Γ~
⊢TC (Aw [ σ~ ]Tc) = ⊢sC-dom σ~
⊢TC ([id]T Aw) = {!!}
⊢TC ([][]T Aw σw δw) = {!!}

-- Γ ⊢s σ₀ ~ σ₁ ∶ Δ → ⊢C Γ

⊢sC-dom = {!!}

-- Γ ⊢s σ₀ ~ σ₁ ∶ Δ → ⊢C Δ

⊢sC-cod = {!!}

-- ⊢C Γ₀ ~ Γ₁ → ⊢C Γ₀ and ⊢C Γ₁

⊢C₀ (Γ~ ◾C Γ~₁) = ⊢C₀ Γ~
⊢C₀ (Γ~ ⁻¹C) = ⊢C₁ Γ~
⊢C₀ •c = •w
⊢C₀ (Γ~ ,c A~) = ⊢C₀ Γ~ ,w ⊢T₀ A~
⊢C₁ (Γ~ ◾C Γ~₁) = ⊢C₁ Γ~₁
⊢C₁ (Γ~ ⁻¹C) = ⊢C₀ Γ~
⊢C₁ •c = •w
⊢C₁ (Γ~ ,c A~) = (⊢C₁ Γ~) ,w coeT Γ~ (⊢T₁ A~)

-- Γ ⊢T A₀ ~ A₁ → Γ ⊢T A₀ and Γ ⊢T A₁

⊢T₀ (A~ ◾T A~₁) = ⊢T₀ A~
⊢T₀ (A~ ⁻¹T) = ⊢T₁ A~
⊢T₀ (coeT~ Γ~ A~) = coeT Γ~ (⊢T₀ A~)
⊢T₀ (A~ [ σ~ ]Tc) = ⊢T₀ A~ [ ⊢s₀ σ~ ]Tw
⊢T₀ ([id]T Aw) = Aw [ idw {!!} ]Tw -- (⊢TC Aw)
⊢T₀ ([][]T Aw σw δw) = Aw [ δw ]Tw [ σw ]Tw
⊢T₁ (A~ ◾T A~₁) = ⊢T₁ A~₁
⊢T₁ (A~ ⁻¹T) = ⊢T₀ A~
⊢T₁ (coeT~ Γ~ A~) = coeT Γ~ (⊢T₁ A~)
⊢T₁ (A~ [ σ~ ]Tc) = ⊢T₁ A~ [ ⊢s₁ σ~ ]Tw
⊢T₁ ([id]T Aw) = Aw
⊢T₁ ([][]T Aw σw δw) = Aw [ δw ∘w σw ]Tw

-- Γ ⊢s σ₀ ~ σ₁ ∶ Δ → Γ ⊢s σ₀ ∶ Δ and Γ ⊢s σ₁ ∶ Δ

⊢s₀ (σ~ ◾s σ~₁) = ⊢s₀ σ~
⊢s₀ (σ~ ⁻¹s) = ⊢s₁ σ~
⊢s₀ (coes~ Γ~ Δ~ σ~) = coes Γ~ Δ~ (⊢s₀ σ~)
⊢s₀ (idc Γ~) = idw (⊢C₀ Γ~)
⊢s₀ (σ~ ∘c ν~) = ⊢s₀ σ~ ∘w ⊢s₀ ν~
⊢s₀ (εc Γ~) = εw (⊢C₀ Γ~)
⊢s₀ (,sc σ~ A~ t~) = ,sw (⊢s₀ σ~) (⊢t₀ t~) (⊢T₀ A~)
⊢s₀ (π₁c A~ σ~) = π₁w (⊢T₀ A~) (⊢s₀ σ~)
⊢s₀ (idl δw) = idw {!!} ∘w δw -- (⊢sC-cod δw)
⊢s₀ (idr δw) = δw ∘w idw {!!} -- (⊢sC-dom δw)
⊢s₀ (ass σw δw νw) = (σw ∘w δw) ∘w νw
⊢s₀ (,∘ δw σw Aw aw) = (,sw δw aw Aw) ∘w σw
⊢s₀ (π₁β Aw δw aw) = π₁w Aw (,sw δw aw Aw)
⊢s₀ (πη Aw δw) = ,sw (π₁w Aw δw) (π₂w Aw δw) Aw
⊢s₀ (εη σw) = σw
⊢s₁ (σ~ ◾s σ~₁) = ⊢s₁ σ~₁
⊢s₁ (σ~ ⁻¹s) = ⊢s₀ σ~
⊢s₁ (coes~ Γ~ Δ~ σ~) = coes Γ~ Δ~ (⊢s₁ σ~)
⊢s₁ (idc Γ~) = coes (Γ~ ⁻¹C) (Γ~ ⁻¹C) (idw (⊢C₁ Γ~))
⊢s₁ (σ~ ∘c σ~₁) = ⊢s₁ σ~ ∘w ⊢s₁ σ~₁
⊢s₁ (εc Γ~) = coes (Γ~ ⁻¹C) •c (εw (⊢C₁ Γ~))
⊢s₁ (,sc σ~ A~ t~) = coes (reflC {!!}){- (⊢sC-dom (⊢s₀ σ~)) -} (reflC {!!} {- (⊢sC-cod (⊢s₀ σ~)) -},c (A~ ⁻¹T)) (,sw (⊢s₁ σ~) (coet (reflC {!!}) (A~ [ σ~ ]Tc) (⊢t₁ t~)) (⊢T₁ A~)) -- (⊢sC-dom (⊢s₀ σ~))
⊢s₁ (π₁c A~ σ~) = π₁w (⊢T₁ A~) (coes (reflC {!!}){- (⊢sC-dom (⊢s₁ σ~)) -} (reflC {!!} ,c A~) (⊢s₁ σ~)) -- (⊢TC (⊢T₁ A~))
⊢s₁ (idl δw) = δw
⊢s₁ (idr δw) = δw
⊢s₁ (ass σw δw νw) = σw ∘w (δw ∘w νw)
⊢s₁ (,∘ δw σw Aw aw) = ,sw (δw ∘w σw) (coet (reflC {!!}) ([][]T Aw σw δw) (aw [ σw ]tw)) Aw -- (⊢sC-dom σw)
⊢s₁ (π₁β Aw δw aw) = δw
⊢s₁ (πη Aw δw) = δw
⊢s₁ (εη σw) = εw {!!} -- (⊢sC-dom σw)

-- Γ ⊢t t₀ ~ t₁ ∶ A → Γ ⊢t t₀ ∶ A and Γ ⊢t t₁ ∶ A

⊢t₀ (t~ ◾t t~₁) = ⊢t₀ t~
⊢t₀ (t~ ⁻¹t) = ⊢t₁ t~
⊢t₀ (coet~ Γ~ A~ t~) = coet Γ~ A~ (⊢t₀ t~)
⊢t₀ (t~ [ σ~ ]tc) = ⊢t₀ t~ [ ⊢s₀ σ~ ]tw
⊢t₀ (π₂c A~ σ~) = π₂w (⊢T₀ A~) (⊢s₀ σ~)
⊢t₀ (π₂β Aw δw aw) = coet (reflC {!!}){- (⊢sC-dom δw) -} (reflT Aw [ π₁β Aw δw aw ]Tc) (π₂w Aw (,sw δw aw Aw)) -- ⊢tC instead of ⊢sC-dom?
⊢t₁ (t~ ◾t t~₁) = ⊢t₁ t~₁
⊢t₁ (t~ ⁻¹t) = ⊢t₀ t~
⊢t₁ (coet~ Γ~ A~ t~) = coet Γ~ A~ (⊢t₁ t~)
⊢t₁ (t~ [ σ~ ]tc) = coet (reflC {!!}){- (⊢sC-dom (⊢s₁ σ~)) -} (reflT {!!} [ σ~ ⁻¹s ]Tc) (⊢t₁ t~ [ ⊢s₁ σ~ ]tw) -- (⊢tT (⊢t₁ t~))
⊢t₁ (π₂c A~ σ~) = coet (reflC {!!}) {- (⊢sC-dom (⊢s₁ σ~)) -} ((A~ ⁻¹T) [ π₁c A~ (σ~ ⁻¹s) ]Tc) (π₂w (⊢T₁ A~) (coes (reflC {!!}){- (⊢sC-dom (⊢s₁ σ~)) -} (reflC {!!} ,c A~) (⊢s₁ σ~))) -- (⊢TC (⊢T₁ A~))
⊢t₁ (π₂β Aw δw aw) = aw

-}
------------------------------------------------------------------------------
-- compatibility
------------------------------------------------------------------------------
{-
⊢ctx : {Γ : Con'}{A : Ty'} → Γ ⊢T A → ctx A ≡ Γ
⊢dom : {Γ Δ : Con'}{σ : Tms'} → Γ ⊢s σ ∶ Δ → dom σ ≡ Γ
⊢cod : {Γ Δ : Con'}{σ : Tms'} → Γ ⊢s σ ∶ Δ → cod σ ≡ Δ

⊢ctx (Aw [ σw ]Tw) = ⊢dom σw
⊢ctx _ = {!!}

⊢dom (idw Γw) = refl
⊢dom (σw ∘w νw) = ⊢dom νw
⊢dom (εw Γw) = refl
⊢dom (,sw σw tw Aw) = ⊢dom σw
⊢dom (π₁w Aw σw) = ⊢dom σw
⊢dom _ = {!!}

⊢cod (idw Γw) = refl
⊢cod (σw ∘w νw) = ⊢cod σw
⊢cod (εw Γw) = refl
⊢cod (,sw σw tw Aw) = ,'= (⊢cod σw) refl
⊢cod (π₁w Aw σw) = refl
⊢cod _ = {!!}

⊢ty : {Γ : Con'}{A : Ty'}{t : Tm'} → Γ ⊢t t ∶ A → ty t ≡ A
⊢ty (tw [ σw ]tw) = []T'= (⊢ty tw) refl
⊢ty (π₂w Aw σw) = []T'= refl (π₁'= (⊢ctx Aw) refl)
⊢ty _ = {!!}

{-
-- congruence rules

⊢C= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
    → ⊢C Γ₀ ≡ ⊢C Γ₁
⊢C= refl = refl

⊢T= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
    → Γ₀ ⊢T A₀ ≡ Γ₁ ⊢T A₁
⊢T= refl refl = refl

,w= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
      {Γw₀ : ⊢C Γ₀}{Γw₁ : ⊢C Γ₁}(Γw₂ : Γw₀ ≡[ ⊢C= Γ₂ ]≡ Γw₁)
      {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
      {Aw₀ : Γ₀ ⊢T A₀}{Aw₁ : Γ₁ ⊢T A₁}(Aw₂ : Aw₀ ≡[ ⊢T= Γ₂ A₂ ]≡ Aw₁)
    → Γw₀ ,w Aw₀ ≡[ ⊢C= (,'= Γ₂ A₂) ]≡ Γw₁ ,w Aw₁
,w= refl refl refl refl = refl

Uw=
  : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
    {Γw₀ : ⊢C Γ₀}{Γw₁ : ⊢C Γ₁}(Γw₂ : Γw₀ ≡[ ⊢C= Γ₂ ]≡ Γw₁)
  → Uw Γw₀ ≡[ ⊢T= Γ₂ (U'= Γ₂) ]≡ Uw Γw₁
Uw= refl refl = refl

Πw=
  : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
    {Γw₀ : ⊢C Γ₀}{Γw₁ : ⊢C Γ₁}(Γw₂ : Γw₀ ≡[ ⊢C= Γ₂ ]≡ Γw₁)
    {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
    {Aw₀ : Γ₀ ⊢T A₀}{Aw₁ : Γ₁ ⊢T A₁}(Aw₂ : Aw₀ ≡[ ⊢T= Γ₂ A₂ ]≡ Aw₁)
    {B₀ B₁ : Ty'}(B₂ : B₀ ≡ B₁)
    {Bw₀ : Γ₀ ,' A₀ ⊢T B₀}{Bw₁ : Γ₁ ,' A₁ ⊢T B₁}(Bw₂ : Bw₀ ≡[ ⊢T= (,'= Γ₂ A₂) B₂ ]≡ Bw₁)
  → Πw Γw₀ Aw₀ Bw₀ ≡[ ⊢T= Γ₂ (Π'= Γ₂ A₂ B₂) ]≡ Πw Γw₁ Aw₁ Bw₁
Πw= refl refl refl refl refl refl = refl

------------------------------------------------------------------------------
-- uniqueness of the typing relations
------------------------------------------------------------------------------

uniqC : {Γ : Con'}(Γw₀ Γw₁ : ⊢C Γ) → Γw₀ ≡ Γw₁
uniqT : {Γ : Con'}{A : Ty'}(Aw₀ Aw₁ : Γ ⊢T A) → Aw₀ ≡ Aw₁

uniqC {•'} •w •w = refl
uniqC {Γ ,' A} (Γw₀ ,w Aw₀) (Γw₁ ,w Aw₁)
  = ,w= refl (uniqC {Γ} Γw₀ Γw₁) refl (uniqT Aw₀ Aw₁)

-- the next line is the only usage of K
uniqT {A = U' Γ} (Uw Γw₀) (Uw Γw₁) = Uw= refl (uniqC Γw₀ Γw₁)
uniqT {A = Π' Γ A B} (Πw Γw₀ Aw₀ Bw₀) (Πw Γw₁ Aw₁ Bw₁)
  = Πw= refl (uniqC Γw₀ Γw₁) refl (uniqT Aw₀ Aw₁) refl (uniqT Bw₀ Bw₁)

------------------------------------------------------------------------------
-- intrinsic to extrinsic
------------------------------------------------------------------------------

IEC' : Con → Con'
IET' : {Γ : Con}(A : Ty Γ) → Ty'

IEC' • = •'
IEC' (Γ , A) = IEC' Γ ,' IET' A
IET' (U Γ) = U' (IEC' Γ)
IET' (Π Γ A B) = Π' (IEC' Γ) (IET' A) (IET' B)

IECw : (Γ : Con) → ⊢C IEC' Γ
IETw : {Γ : Con}(A : Ty Γ) → IEC' Γ ⊢T IET' A

IECw • = •w
IECw (Γ , A) = (IECw Γ) ,w IETw A
IETw (U Γ) = Uw (IECw Γ)
IETw (Π Γ A B) = Πw (IECw Γ) (IETw A) (IETw B)

------------------------------------------------------------------------------
-- extrinsic to intrinsic
------------------------------------------------------------------------------

EIC : {Γ : Con'}(Γw : ⊢C Γ) → Con
EIT : {Γ : Con'}(Γw : ⊢C Γ){A : Ty'}(Aw : Γ ⊢T A) → Ty (EIC Γw)

EIC •w = •
EIC (Γw ,w Aw) = EIC Γw , EIT Γw Aw
EIT Γw (Uw _) = U (EIC Γw)
EIT Γw (Πw _ Aw Bw) = Π (EIC Γw) (EIT Γw Aw) (EIT (Γw ,w Aw) Bw)

------------------------------------------------------------------------------
-- intrinsic ↦ extrinsic ↦ intrinsic
------------------------------------------------------------------------------

IIC : (Γ : Con) → EIC (IECw Γ) ≡ Γ
IIT : {Γ : Con}(A : Ty Γ) → EIT (IECw Γ) (IETw A) ≡[ Ty= (IIC Γ) ]≡ A

IIC • = refl
IIC (Γ , A) = ,= (IIC Γ) (IIT A)

IIT (U Γ) = U= (IIC Γ)
IIT (Π Γ A B) = Π= (IIC Γ) (IIT A) (IIT B)

------------------------------------------------------------------------------
-- extrinsic ↦ intrinsic ↦ extrinsic
------------------------------------------------------------------------------

EEC' : {Γ' : Con'}(Γw : ⊢C Γ') → IEC' (EIC Γw) ≡ Γ'
EET' : {Γ' : Con'}(Γw : ⊢C Γ'){A' : Ty'}(Aw : Γ' ⊢T A') → IET' (EIT Γw Aw) ≡ A'

EEC' •w = refl
EEC' (Γw ,w Aw) = ,'= (EEC' Γw) (EET' Γw Aw)

EET' Γw (Uw _) = U'= (EEC' Γw)
EET' Γw (Πw _ Aw Bw) = Π'= (EEC' Γw) (EET' Γw Aw) (EET' (Γw ,w Aw) Bw)

EECw : {Γ' : Con'}(Γw : ⊢C Γ') → IECw (EIC Γw) ≡[ ⊢C= (EEC' Γw) ]≡ Γw
EECw Γw = uniqC _ _

EETw : {Γ' : Con'}(Γw : ⊢C Γ'){A' : Ty'}(Aw : Γ' ⊢T A') → IETw (EIT Γw Aw) ≡[ ⊢T= (EEC' Γw) (EET' Γw Aw) ]≡ Aw
EETw Γw Aw = uniqT _ _
-}

-}
