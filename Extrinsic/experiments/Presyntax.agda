module Extrinsic.experiments.Presyntax where

open import lib

data Con' : Set
data Ty'  : Set
data Tms' : Set
data Tm'  : Set

data Con' where
  •' : Con'
  _,'_ : Con' → Ty' → Con'

infixl 6 _,'_

data Ty' where
  _[_]T' : Ty' → Tms' → Ty'

data Tms' where
  id'   : (Γ : Con') → Tms' -- Γ is the (co)domain of id'
  _∘'_  : Tms' → Tms' → Tms'
  ε'    : (Γ : Con') → Tms' -- Γ is the domain of ε'
  _,s'_ : (σ : Tms')(t : Tm'){A : Ty'} → Tms' -- t has type A [ σ ]T'
  π₁'   : {Γ : Con'}(σ : Tms') → Tms' -- Γ is the codomain of (π₁' σ)

data Tm' where
  _[_]t' : Tm' → Tms' → Tm'
  π₂'    : {A : Ty'} → Tms' → Tm' -- π₂' σ has type A [ π₁' σ ]T'

-- congruence rules

,'=
  : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
    {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
  → Γ₀ ,' A₀ ≡ Γ₁ ,' A₁
,'= refl refl = refl

[]T'=
  : {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
    {σ₀ σ₁ : Tms'}(σ₂ : σ₀ ≡ σ₁)
  → A₀ [ σ₀ ]T' ≡ A₁ [ σ₁ ]T'
[]T'= refl refl = refl

π₁'=
  : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
    {σ₀ σ₁ : Tms'}(σ₂ : σ₀ ≡ σ₁)
  → π₁' {Γ₀} σ₀ ≡ π₁' {Γ₁} σ₁
π₁'= refl refl = refl

-- following Clairambault

ctx : Ty' → Con'
ty : Tm' → Ty'
dom : Tms' → Con'

ctx (A [ σ ]T') = dom σ

ty (t [ σ ]t') = ty t [ σ ]T'
ty (π₂' {A} σ) = A [ π₁' {ctx A} σ ]T'

dom (id' Γ) = Γ
dom (σ ∘' ν) = dom ν
dom (ε' Γ) = Γ
dom ((σ ,s' t) {A}) = dom σ
dom (π₁' σ) = dom σ

cod : Tms' → Con'
cod (id' Γ) = Γ
cod (σ ∘' ν) = cod σ
cod (ε' Γ) = •'
cod ((σ ,s' t) {A}) = cod σ ,' A
cod (π₁' {Γ} σ) = Γ
