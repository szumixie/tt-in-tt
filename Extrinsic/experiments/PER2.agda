module Extrinsic.experiments.PER2 where

open import lib
open import TT.Decl.Syntax
open import TT.Decl.Congr syntaxDecl
open import TT.Core.Syntax
open import TT.Core.Congr syntaxCore
open import Extrinsic.experiments.Presyntax
open import Extrinsic.experiments.PER

data Maybe {i}(A : Set i) : Set i where
  nothing : Maybe A
  just : A → Maybe A

MaybeSet : ∀{i} → Maybe (Set i) → Set i
MaybeSet nothing = Lift ⊤
MaybeSet (just A) = A

_<$>'_ : ∀{i j}{A : Set i}{B : Set j}
       → (A → B)
       → Maybe A → Maybe B
f <$>' nothing = nothing
f <$>' just x = just (f x)

-- \o*
_⊛'_ : ∀{i j}{A : Set i}{B : Set j}
     → Maybe (A → B)
     → Maybe A → Maybe B
nothing ⊛' a = nothing
just f ⊛' a = f <$>' a

_<$>_ : ∀{i j}{A : Set i}{B : A → Set j}
      → ((x : A) → B x)
      → (a : Maybe A) → MaybeSet (B <$>' a)
f <$> nothing = lift tt
f <$> just x = f x

_⊛_ : ∀{i j}{A : Set i}{B : A → Set j}(f : Maybe ((x : A) → B x))
      (a : Maybe A) → {!!}
_⊛_ = {!!}

infixl 4 _<$>'_ _⊛'_ _<$>_ _⊛_
{-


⟦_⟧C : Con' → Maybe Con
⟦_⟧T : (A : Ty') → MaybeSet (Ty <$> ⟦ ctx A ⟧C)

⟦ •' ⟧C = just •
⟦ Γ ,' A ⟧C = {!_,_ <$> ⟦ Γ ⟧C ⊛ ?!}

⟦_⟧T = {!!}
-}
