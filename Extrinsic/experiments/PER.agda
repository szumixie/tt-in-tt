module Extrinsic.experiments.PER where

open import lib

open import Extrinsic.experiments.Presyntax

------------------------------------------------------------------------------
-- typing relations using PERs
------------------------------------------------------------------------------

-- the PER is nice because typing rules and congruence rules are the
-- same

-- this is not paranoid

data ⊢C_~_  : Con' → Con' → Set
data _⊢T_~_ : Con' → Ty' → Ty' → Set
data _⊢s_~_∶_ : Con' → Tms' → Tms' → Con' → Set
data _⊢t_~_∶_ : Con' → Tm' → Tm' → Ty' → Set

-- the usual typing relations

⊢C_    : Con' → Set
_⊢T_   : Con' → Ty' → Set
_⊢s_∶_ : Con' → Tms' → Con' → Set
_⊢t_∶_ : Con' → Tm' → Ty' → Set

⊢C Γ = ⊢C Γ ~ Γ
Γ ⊢T A = Γ ⊢T A ~ A
Γ ⊢s σ ∶ Δ = Γ ⊢s σ ~ σ ∶ Δ
Γ ⊢t t ∶ A = Γ ⊢t t ~ t ∶ A

infix 4 ⊢C_~_ _⊢T_~_ _⊢s_~_∶_ _⊢t_~_∶_ ⊢C_ _⊢T_ _⊢s_∶_ _⊢t_∶_

infixl 4 _◾C_ _◾T_ _◾s_ _◾t_
infix 5 _⁻¹C _⁻¹T _⁻¹s _⁻¹t

data ⊢C_~_ where
  -- PER
  _◾C_ : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁){Γ₂ : Con'}(Γw' : ⊢C Γ₁ ~ Γ₂) → ⊢C Γ₀ ~ Γ₂
  _⁻¹C : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁) → ⊢C Γ₁ ~ Γ₀

  -- congruence
  •w   : ⊢C •' ~ •'
  _,w_ : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁)
         {A₀ A₁ : Ty'}(Aw : Γ₀ ⊢T A₀ ~ A₁) -- an assymetry (later many such occurrences)
       → ⊢C Γ₀ ,' A₀ ~ Γ₁ ,' A₁

infixl 6 _,w_

data _⊢T_~_ where
  -- PER
  _◾T_ : {Γ : Con'}{A₀ A₁ : Ty'}(Aw : Γ ⊢T A₀ ~ A₁){A₂ : Ty'}(Aw' : Γ ⊢T A₁ ~ A₂) → Γ ⊢T A₀ ~ A₂
  _⁻¹T : {Γ : Con'}{A₀ A₁ : Ty'}(Aw : Γ ⊢T A₀ ~ A₁) → Γ ⊢T A₁ ~ A₀

  -- coercion
  coeT : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁){A₀ A₁ : Ty'}(Aw : Γ₀ ⊢T A₀ ~ A₁) → Γ₁ ⊢T A₀ ~ A₁

  -- congruence
  _[_]Tw
    : {Γ : Con'}{Δ : Con'}
      {A₀ A₁ : Ty'}(Aw : Δ ⊢T A₀ ~ A₁)
      {σ₀ σ₁ : Tms'}(σw : Γ ⊢s σ₀ ~ σ₁ ∶ Δ)
    → Γ ⊢T A₀ [ σ₀ ]T' ~ A₁ [ σ₁ ]T'

  -- the paranoid version of _[_]Tw would also require the following
  -- arguments: (Γw : ⊢C Γ₀ ~ Γ₁) and (Δw : ⊢C Δ₀ ~ Δ₁)

  -- conversion
  [id]Tw
    : {Γ : Con'}{A : Ty'}(Aw : Γ ⊢T A) → Γ ⊢T A [ id' Γ ]T' ~ A
  [][]Tw
    : ∀{Γ Δ Σ : Con'}{A : Ty'}(Aw : Γ ⊢T A)
      {σ : Tms'}(σw : Γ ⊢s σ ∶ Δ){δ : Tms'}(δw : Δ ⊢s δ ∶ Σ)
    → Γ ⊢T (A [ δ ]T' [ σ ]T') ~ (A [ δ ∘' σ ]T')

data _⊢s_~_∶_ where
  -- PER
  _◾s_ : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σw : Γ ⊢s σ₀ ~ σ₁ ∶ Δ){σ₂ : Tms'}(σw' : Γ ⊢s σ₁ ~ σ₂ ∶ Δ) → Γ ⊢s σ₀ ~ σ₂ ∶ Δ
  _⁻¹s : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σw : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → Γ ⊢s σ₁ ~ σ₀ ∶ Δ

  -- coercion
  coes : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δw : ⊢C Δ₀ ~ Δ₁)
         {σ₀ σ₁ : Tms'}(σw : Γ₀ ⊢s σ₀ ~ σ₁ ∶ Δ₀) → Γ₁ ⊢s σ₀ ~ σ₁ ∶ Δ₁

  -- congruence
  idw
    : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁) → Γ₀ ⊢s id' Γ₀ ~ id' Γ₁ ∶ Γ₀
  _∘w_
    : {Γ Δ Σ : Con'}{σ₀ σ₁ : Tms'}(σw : Δ ⊢s σ₀ ~ σ₁ ∶ Σ)
      {ν₀ ν₁ : Tms'}(νw : Γ ⊢s ν₀ ~ ν₁ ∶ Δ)
    → Γ ⊢s σ₀ ∘' ν₀ ~ σ₁ ∘' ν₁ ∶ Σ
  εw
    : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁)
    → Γ₀ ⊢s ε' Γ₀ ~ ε' Γ₁ ∶ •'
  ,sw
    : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σw : Γ ⊢s σ₀ ~ σ₁ ∶ Δ)
    → {A₀ A₁ : Ty'}{Aw : Δ ⊢T A₀ ~ A₁}
      {t₀ t₁ : Tm'}(tw : Γ ⊢t t₀ ~ t₁ ∶ A₀ [ σ₀ ]T')
    → Γ ⊢s (σ₀ ,s' t₀) {A₀} ~ (σ₁ ,s' t₁) {A₁} ∶ Δ ,' A₀
  π₁w
    : {Γ Δ : Con'}{A₀ A₁ : Ty'}(Aw : Δ ⊢T A₀ ~ A₁)
      {σ₀ σ₁ : Tms'}(σw : Γ ⊢s σ₀ ~ σ₁ ∶ Δ ,' A₀)
    → Γ ⊢s π₁' {Δ} σ₀ ~ π₁' {Δ} σ₁ ∶ Δ

  -- conversion
  idlw
    : {Γ Δ : Con'}{δ : Tms'}(δw : Γ ⊢s δ ∶ Δ) → Γ ⊢s id' Δ ∘' δ ~ δ ∶ Δ
  idrw
    : {Γ Δ : Con'}{δ : Tms'}(δw : Γ ⊢s δ ∶ Δ) → Γ ⊢s δ ∘' id' Γ ~ δ ∶ Δ
  assw
    : {Δ Γ Σ Ω : Con'}{σ : Tms'}(σw : Σ ⊢s σ ∶ Ω)
      {δ : Tms'}(δw : Γ ⊢s δ ∶ Σ){ν : Tms'}(ν' : Δ ⊢s ν ∶ Γ)
    → Δ ⊢s ((σ ∘' δ) ∘' ν) ~ (σ ∘' (δ ∘' ν)) ∶ Ω
  ,∘w
    : {Γ Δ Σ : Con'}{δ : Tms'}(δw : Γ ⊢s δ ∶ Δ)
      {σ : Tms'}(σw : Σ ⊢s σ ∶ Γ)
      {A : Ty'}(Aw : Δ ⊢T A)
      {a : Tm'}(aw : Γ ⊢t a ∶ (A [ δ ]T'))
    → Σ ⊢s ((δ ,s' a) {A} ∘' σ) ~ ((δ ∘' σ) ,s' (a [ σ ]t')) {A} ∶ Δ ,' A
  π₁βw
    : {Γ Δ : Con'}{A : Ty'}(Aw : Δ ⊢T A)
      {δ : Tms'}(δw : Γ ⊢s δ ∶ Δ)
      {a : Tm'}(aw : Γ ⊢t a ∶ (A [ δ ]T'))
    → Γ ⊢s π₁' {Δ} ((δ ,s' a) {A}) ~ δ ∶ Δ
  πηw
    : {Γ Δ : Con'}{A : Ty'}(Aw : Δ ⊢T A)
      {δ : Tms'}(δw : Γ ⊢s δ ∶ (Δ ,' A))
    → Γ ⊢s (π₁' {Δ} δ ,s' π₂' {A} δ) {A} ~ δ ∶ Δ ,' A
  εηw
    : {Γ : Con'}{σ : Tms'}(σw : Γ ⊢s σ ∶ •') → Γ ⊢s σ ~ ε' Γ ∶ •'

data _⊢t_~_∶_ where
  -- PER
  _◾t_ : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(tw : Γ ⊢t t₀ ~ t₁ ∶ A){t₂ : Tm'}(tw' : Γ ⊢t t₁ ~ t₂ ∶ A) → Γ ⊢t t₀ ~ t₂ ∶ A
  _⁻¹t : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(tw : Γ ⊢t t₀ ~ t₁ ∶ A) → Γ ⊢t t₁ ~ t₀ ∶ A

  -- coercion
  coet : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁){A₀ A₁ : Ty'}(Aw : Γ₀ ⊢T A₀ ~ A₁)
         {t₀ t₁ : Tm'}(tw : Γ₀ ⊢t t₀ ~ t₁ ∶ A₀) → Γ₁ ⊢t t₀ ~ t₁ ∶ A₁

  -- congruence
  _[_]tw
    : {Γ Δ : Con'}{A : Ty'}
      {t₀ t₁ : Tm'}(tw : Δ ⊢t t₀ ~ t₁ ∶ A)
      {σ₀ σ₁ : Tms'}(σw : Γ ⊢s σ₀ ~ σ₁ ∶ Δ)
    → Γ ⊢t t₀ [ σ₀ ]t' ~ t₁ [ σ₁ ]t' ∶ A [ σ₀ ]T'
  π₂w
    : {Γ Δ : Con'}
      {A₀ A₁ : Ty'}(Aw : Δ ⊢T A₀ ~ A₁)
      {σ₀ σ₁ : Tms'}(σw : Γ ⊢s σ₀ ~ σ₁ ∶ Δ ,' A₀)
    → Γ ⊢t π₂' {A₀} σ₀ ~ π₂' {A₁} σ₁ ∶ A₀ [ π₁' {Δ} σ₀ ]T'

  -- conversion
  π₂βw
    : {Γ Δ : Con'}{A : Ty'}(Aw : Δ ⊢T A)
      {δ : Tms'}(δw : Γ ⊢s δ ∶ Δ)
      {a : Tm'}(aw : Γ ⊢t a ∶ (A [ δ ]T'))
    → Γ ⊢t (π₂' {A} ((δ ,s' a) {A})) ~ a ∶ A [ δ ]T'

-- since we don't have the paranoid versions, we need to prove the
-- following (but it is not hard)

⊢TC : {Γ : Con'}{A₀ A₁ : Ty'} → Γ ⊢T A₀ ~ A₁ → ⊢C Γ
⊢sC-dom : {Γ Δ : Con'}{σ₀ σ₁ : Tms'} → Γ ⊢s σ₀ ~ σ₁ ∶ Δ → ⊢C Γ
⊢sC-cod : {Γ Δ : Con'}{σ₀ σ₁ : Tms'} → Γ ⊢s σ₀ ~ σ₁ ∶ Δ → ⊢C Δ

⊢TC (Aw ◾T Aw₁) = ⊢TC Aw
⊢TC (Aw ⁻¹T) = ⊢TC Aw
⊢TC (coeT Γw Aw) = Γw ⁻¹C ◾C Γw
⊢TC (Aw [ σw ]Tw) = ⊢sC-dom σw
⊢TC ([id]Tw Aw) = ⊢TC Aw
⊢TC ([][]Tw Aw σw δw) = ⊢TC Aw

⊢sC-dom (σw ◾s σw₁) = ⊢sC-dom σw
⊢sC-dom (σw ⁻¹s) = ⊢sC-dom σw
⊢sC-dom (coes Γw Δw σw) = Γw ⁻¹C ◾C Γw
⊢sC-dom (idw Γw) = Γw ◾C Γw ⁻¹C
⊢sC-dom (σw ∘w σw₁) = ⊢sC-dom σw₁
⊢sC-dom (εw Γw) = Γw ◾C Γw ⁻¹C
⊢sC-dom (,sw σw tw) = ⊢sC-dom σw
⊢sC-dom (π₁w Aw σw) = ⊢sC-dom σw
⊢sC-dom (idlw δw) = ⊢sC-dom δw
⊢sC-dom (idrw δw) = ⊢sC-dom δw
⊢sC-dom (assw σw δw νw) = ⊢sC-dom νw
⊢sC-dom (,∘w δw σw Aw aw) = ⊢sC-dom σw
⊢sC-dom (π₁βw Aw δw aw) = ⊢sC-dom δw
⊢sC-dom (πηw Aw δw) = ⊢sC-dom δw
⊢sC-dom (εηw σw) = ⊢sC-dom σw

⊢sC-cod (σw ◾s σw₁) = ⊢sC-cod σw₁
⊢sC-cod (σw ⁻¹s) = ⊢sC-cod σw
⊢sC-cod (coes Γw Δw σw) = Δw ⁻¹C ◾C Δw
⊢sC-cod (idw Γw) = Γw ◾C Γw ⁻¹C
⊢sC-cod (σw ∘w σw₁) = ⊢sC-cod σw
⊢sC-cod (εw Γw) = •w
⊢sC-cod (,sw σw {Aw = Aw} tw) = ⊢TC Aw ,w (Aw ◾T Aw ⁻¹T)
⊢sC-cod (π₁w Aw σw) = ⊢TC Aw
⊢sC-cod (idlw δw) = ⊢sC-cod δw
⊢sC-cod (idrw δw) = ⊢sC-cod δw
⊢sC-cod (assw σw δw ν') = ⊢sC-cod σw
⊢sC-cod (,∘w δw σw Aw aw) = ⊢TC Aw ,w Aw
⊢sC-cod (π₁βw Aw δw aw) = ⊢TC Aw
⊢sC-cod (πηw Aw δw) = ⊢TC Aw ,w Aw
⊢sC-cod (εηw σw) = •w

⊢tC : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'} → Γ ⊢t t₀ ~ t₁ ∶ A → ⊢C Γ
⊢tT : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'} → Γ ⊢t t₀ ~ t₁ ∶ A → Γ ⊢T A

⊢tC (tw ◾t tw₁) = ⊢tC tw
⊢tC (tw ⁻¹t) = ⊢tC tw
⊢tC (coet Γw Aw tw) = Γw ⁻¹C ◾C Γw
⊢tC (tw [ σw ]tw) = ⊢sC-dom σw
⊢tC (π₂w Aw σw) = ⊢sC-dom σw
⊢tC (π₂βw Aw δw aw) = ⊢sC-dom δw

⊢tT (tw ◾t tw₁) = ⊢tT tw
⊢tT (tw ⁻¹t) = ⊢tT tw
⊢tT (coet Γw Aw tw) = coeT Γw (Aw ⁻¹T ◾T Aw)
⊢tT (tw [ σw ]tw) = ⊢tT tw [ σw ◾s σw ⁻¹s ]Tw
⊢tT (π₂w Aw σw) = (Aw ◾T Aw ⁻¹T) [ π₁w Aw (σw ◾s σw ⁻¹s) ]Tw
⊢tT (π₂βw Aw δw aw) = ⊢tT aw

{-
-- congruence rules

⊢C= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
    → ⊢C Γ₀ ≡ ⊢C Γ₁
⊢C= refl = refl

⊢T= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
    → Γ₀ ⊢T A₀ ≡ Γ₁ ⊢T A₁
⊢T= refl refl = refl

,w= : {Γ₀ Γ₁ : Con'}(Γ₂ : Γ₀ ≡ Γ₁)
      {Γw₀ : ⊢C Γ₀}{Γw₁ : ⊢C Γ₁}(Γw₂ : Γw₀ ≡[ ⊢C= Γ₂ ]≡ Γw₁)
      {A₀ A₁ : Ty'}(A₂ : A₀ ≡ A₁)
      {Aw₀ : Γ₀ ⊢T A₀}{Aw₁ : Γ₁ ⊢T A₁}(Aw₂ : Aw₀ ≡[ ⊢T= Γ₂ A₂ ]≡ Aw₁)
    → Γw₀ ,w Aw₀ ≡[ ⊢C= (,'= Γ₂ A₂) ]≡ Γw₁ ,w Aw₁
,w= refl refl refl refl = refl
-}

postulate
  propC : {Γ₀ Γ₁ : Con'}(p q : ⊢C Γ₀ ~ Γ₁) → p ≡ q
  propT : {A₀ A₁ : Ty'}{Γ : Con'}(p q : Γ ⊢T A₀ ~ A₁) → p ≡ q
  props : {σ₀ σ₁ : Tms'}{Γ Δ : Con'}(p q : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → p ≡ q
  propt : {t₀ t₁ : Tm'}{Γ : Con'}{A : Ty'}(p q : Γ ⊢t t₀ ~ t₁ ∶ A) → p ≡ q

-- TODO: eliminator
