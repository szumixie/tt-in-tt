module Extrinsic.experiments.PER1 where

open import lib
open import TT.Decl.Syntax
open import TT.Decl.Congr syntaxDecl
open import TT.Core.Syntax
open import TT.Core.Congr syntaxCore
open import Extrinsic.experiments.Presyntax
open import Extrinsic.experiments.PER

EIC' : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁) → Con
EIT' : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁){A₀ A₁ : Ty'}(Aw : Γ₀ ⊢T A₀ ~ A₁)
     → Ty (EIC' Γw)
EIs' : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δw : ⊢C Δ₀ ~ Δ₁)
       {σ₀ σ₁ : Tms'}(σw : Γ₀ ⊢s σ₀ ~ σ₁ ∶ Δ₀)
     → Tms (EIC' Γw) (EIC' Δw)
EIt' : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁){A₀ A₁ : Ty'}(Aw : Γ₀ ⊢T A₀ ~ A₁)
       {t₀ t₁ : Tm'}(tw : Γ₀ ⊢t t₀ ~ t₁ ∶ A₀)
     → Tm (EIC' Γw) (EIT' Γw Aw)

EIC' (Γw ◾C Γw₁) = EIC' Γw
EIC' (Γw ⁻¹C) = EIC' Γw
EIC' •w = •
EIC' (Γw ,w Aw) = EIC' Γw , EIT' Γw Aw

EIC''
  : {Γ₀₀ Γ₀₁ : Con'}(Γ₀w : ⊢C Γ₀₀ ~ Γ₀₁)
    {Γ₁₀ Γ₁₁ : Con'}(Γ₁w : ⊢C Γ₁₀ ~ Γ₁₁)
    (Γw₀ : ⊢C Γ₀₀ ~ Γ₁₀)(Γw₁ : ⊢C Γ₀₁ ~ Γ₁₁)
  → EIC' Γ₀w ≡ EIC' Γ₁w
EIC'' (Γ₀w ◾C Γ₀w₁) (Γ₁w ◾C Γ₁w₁) Γw₀ Γw₁ = {!!}
EIC'' (Γ₀w ◾C Γ₀w₁) (Γ₁w ⁻¹C) Γw₀ Γw₁ = {!!}
EIC'' (Γ₀w ◾C Γ₀w₁) •w Γw₀ Γw₁ = {!!}
EIC'' (Γ₀w ◾C Γ₀w₁) (Γ₁w ,w Aw) Γw₀ Γw₁ = {!!}
EIC'' (Γ₀w ⁻¹C) (Γ₁w ◾C Γ₁w₁) Γw₀ Γw₁ = {!!}
EIC'' (Γ₀w ⁻¹C) (Γ₁w ⁻¹C) Γw₀ Γw₁ = {!!}
EIC'' (Γ₀w ⁻¹C) •w Γw₀ Γw₁ = {!!}
EIC'' (Γ₀w ⁻¹C) (Γ₁w ,w Aw) Γw₀ Γw₁ = {!!}
EIC'' •w (Γ₁w ◾C Γ₁w₁) Γw₀ Γw₁ = {!!}
EIC'' •w (Γ₁w ⁻¹C) Γw₀ Γw₁ = {!!}
EIC'' •w •w Γw₀ Γw₁ = refl
EIC'' •w (Γ₁w ,w Aw) Γw₀ Γw₁ = {!!}
EIC'' (Γ₀w ,w Aw) (Γ₁w ◾C Γ₁w₁) Γw₀ Γw₁ = {!!}
EIC'' (Γ₀w ,w Aw) (Γ₁w ⁻¹C) Γw₀ Γw₁ = {!!}
EIC'' (Γ₀w ,w Aw) •w Γw₀ Γw₁ = {!!}
EIC'' (Γ₀w ,w Aw) (Γ₁w ,w Aw₁) Γw₀ Γw₁ = {!!}

EIC'''
  : {Γ₀ Γ₁ Γ₂ : Con'}(Γw₀₁ : ⊢C Γ₀ ~ Γ₁)(Γw₁₂ : ⊢C Γ₁ ~ Γ₂)
  → EIC' Γw₀₁ ≡ EIC' Γw₁₂
EIC''' (Γw₀₁ ◾C Γw₂) (Γw₁₂ ◾C Γw₁₃) = {!!}
EIC''' (Γw₀₁ ◾C Γw₂) (Γw₁₂ ⁻¹C) = {!!}
EIC''' (Γw₀₁ ◾C Γw₂) •w = {!!}
EIC''' (Γw₀₁ ◾C Γw₂) (Γw₁₂ ,w Aw) = {!!}
EIC''' (Γw₀₁ ⁻¹C) (Γw₁₂ ◾C Γw₁₃) = {!!}
EIC''' (Γw₀₁ ⁻¹C) (Γw₁₂ ⁻¹C) = {!!}
EIC''' (Γw₀₁ ⁻¹C) •w = {!Γw₀₁!}
EIC''' (Γw₀₁ ⁻¹C) (Γw₁₂ ,w Aw) = {!!}
EIC''' •w (Γw₁₂ ◾C Γw₁₃) = {!!}
EIC''' •w (Γw₁₂ ⁻¹C) = {!!}
EIC''' •w •w = refl
EIC''' (Γw₀₁ ,w Aw) (Γw₁₂ ◾C Γw₁₃) = {!!}
EIC''' (Γw₀₁ ,w Aw) (Γw₁₂ ⁻¹C) = {!!}
EIC''' (Γw₀₁ ,w Aw) (Γw₁₂ ,w Aw₁) = {!!}

{-# TERMINATING #-}
EIT' Γw (Aw ◾T Aw₁) = EIT' Γw Aw
EIT' Γw (Aw ⁻¹T) = EIT' Γw Aw
EIT' Γw (coeT Γw₁ Aw) = coe (Ty= {!!}) (EIT' Γw₁ Aw) 
-- EIT' Γw (coeT Γw₁ Aw) = coe (Ty= (EIC'' Γw₁ Γw Γw₁ Γw)) (EIT' Γw₁ Aw) 
EIT' Γw (Aw [ σw ]Tw) = EIT' (⊢TC Aw) Aw [ EIs' Γw (⊢TC Aw) σw ]T
EIT' Γw ([id]Tw Aw) = EIT' Γw Aw
EIT' Γw ([][]Tw Aw σw δw) = EIT' Γw Aw

EIT''
  : {Γ₀₀ Γ₀₁ : Con'}(Γ₀w : ⊢C Γ₀₀ ~ Γ₀₁)
    {Γ₁₀ Γ₁₁ : Con'}(Γ₁w : ⊢C Γ₁₀ ~ Γ₁₁)
    (Γw₀ : ⊢C Γ₀₀ ~ Γ₁₀)(Γw₁ : ⊢C Γ₀₁ ~ Γ₁₁)
    {A₀₀ A₀₁ : Ty'}(A₀w : Γ₀₀ ⊢T A₀₀ ~ A₀₁)
    {A₁₀ A₁₁ : Ty'}(A₁w : Γ₁₀ ⊢T A₁₀ ~ A₁₁)
    (Aw₀ : Γ₀₀ ⊢T A₀₀ ~ A₁₀)(Aw₁ : Γ₀₁ ⊢T A₀₁ ~ A₁₁)
  → EIT' Γ₀w A₀w ≡[ Ty= (EIC'' Γ₀w Γ₁w Γw₀ Γw₁) ]≡ EIT' Γ₁w A₁w
EIT'' = {!!}

EIs' Γw Δw (σw ◾s σw₁) = EIs' Γw Δw σw
EIs' Γw Δw (σw ⁻¹s) = EIs' Γw Δw σw
EIs' Γw Δw (coes Γw₁ Δw₁ σw)
  = coe (Tms= (EIC'' Γw₁ Γw Γw₁ Γw) (EIC'' Δw₁ Δw Δw₁ Δw)) (EIs' Γw₁ Δw₁ σw)
EIs' Γw Δw (idw Γw₁) = coe (TmsΓ-= (EIC'' Γw Δw (Γw ◾C Γw ⁻¹C) (Γw ⁻¹C ◾C Δw))) (id {EIC' Γw})
EIs' Γw Δw (σw ∘w νw) = EIs' (⊢sC-cod νw) Δw σw ∘ EIs' Γw (⊢sC-cod νw) νw
EIs' Γw Δw (εw Γw₁) = coe (TmsΓ-= (EIC'' •w Δw •w Δw)) (ε {EIC' Γw})
EIs' Γw Δw (,sw σw tw) = {!!}
EIs' Γw Δw (π₁w Aw σw) = {!!}
EIs' Γw Δw (idlw δw) = EIs' Γw Δw δw
EIs' Γw Δw (idrw δw) = EIs' Γw Δw δw
EIs' Γw Δw (assw σw δw νw) = {!EIs' Γw Δw !}
EIs' Γw Δw (,∘w δw σw Aw aw) = {!EIs' Γw Δw!}
EIs' Γw Δw (π₁βw Aw δw aw) = EIs' Γw Δw δw
EIs' Γw Δw (πηw Aw δw) = EIs' Γw Δw δw
EIs' Γw Δw (εηw σw) = EIs' Γw Δw σw

EIt' Γw Aw (tw ◾t tw₁) = EIt' Γw Aw tw
EIt' Γw Aw (tw ⁻¹t) = EIt' Γw Aw tw
EIt' Γw Aw (coet Γw₁ Aw₁ tw)
  = coe (Tm= (EIC'' Γw₁ Γw Γw₁ Γw) (EIT'' Γw₁ Γw Γw₁ Γw Aw₁ Aw Aw₁ Aw))
        (EIt' Γw₁ Aw₁ tw)
EIt' Γw Aw (tw [ σw ]tw) = {!!}
EIt' Γw Aw (π₂w Aw₁ σw) = coe (TmΓ= {!EIT'' !}) (π₂ (EIs' Γw (⊢TC Aw₁ ,w Aw₁) σw))
EIt' Γw Aw (π₂βw Aw₁ δw aw) = EIt' Γw Aw aw

ConΣ : Set
ConΣ = Σ Con' λ Γ → ⊢C Γ

_~C_ : ConΣ → ConΣ → Set
(Γ₀ ,Σ _) ~C (Γ₁ ,Σ _) = ⊢C Γ₀ ~ Γ₁

ConQ = ConΣ / _~C_

TyΣ : Con' → Set
TyΣ Γ = Σ Ty' λ A → Γ ⊢T A

_~T_ : {Γ : Con'} → TyΣ Γ → TyΣ Γ → Set
_~T_ {Γ} (A₀ ,Σ A₀w) (A₁ ,Σ A₁w) = Γ ⊢T A₀ ~ A₁

TyQ : {Γ : Con'} → Set
TyQ {Γ} = TyΣ Γ / _~T_

EIC : ConQ → Con
EIC
  = Elim/
      (λ { (Γ ,Σ Γw) → EIC' Γw })
      {!!}

EIT : {Γ : Con'} → TyQ {Γ} → Ty {!EIC Γ!}
EIT = {!!}

{-
------------------------------------------------------------------------------
-- intrinsic to extrinsic
------------------------------------------------------------------------------

IEC' : Con → Con'
IET' : {Γ : Con}(A : Ty Γ) → Ty'

IEC' • = •'
IEC' (Γ , A) = IEC' Γ ,' IET' A
IET' (U Γ) = U' (IEC' Γ)
IET' (Π Γ A B) = Π' (IEC' Γ) (IET' A) (IET' B)

IECw : (Γ : Con) → ⊢C IEC' Γ
IETw : {Γ : Con}(A : Ty Γ) → IEC' Γ ⊢T IET' A

IECw • = •w
IECw (Γ , A) = (IECw Γ) ,w IETw A
IETw (U Γ) = Uw (IECw Γ)
IETw (Π Γ A B) = Πw (IECw Γ) (IETw A) (IETw B)

------------------------------------------------------------------------------
-- extrinsic to intrinsic
------------------------------------------------------------------------------

EIC : {Γ : Con'}(Γw : ⊢C Γ) → Con
EIT : {Γ : Con'}(Γw : ⊢C Γ){A : Ty'}(Aw : Γ ⊢T A) → Ty (EIC Γw)

EIC •w = •
EIC (Γw ,w Aw) = EIC Γw , EIT Γw Aw
EIT Γw (Uw _) = U (EIC Γw)
EIT Γw (Πw _ Aw Bw) = Π (EIC Γw) (EIT Γw Aw) (EIT (Γw ,w Aw) Bw)

------------------------------------------------------------------------------
-- intrinsic ↦ extrinsic ↦ intrinsic
------------------------------------------------------------------------------

IIC : (Γ : Con) → EIC (IECw Γ) ≡ Γ
IIT : {Γ : Con}(A : Ty Γ) → EIT (IECw Γ) (IETw A) ≡[ Ty= (IIC Γ) ]≡ A

IIC • = refl
IIC (Γ , A) = ,= (IIC Γ) (IIT A)

IIT (U Γ) = U= (IIC Γ)
IIT (Π Γ A B) = Π= (IIC Γ) (IIT A) (IIT B)

------------------------------------------------------------------------------
-- extrinsic ↦ intrinsic ↦ extrinsic
------------------------------------------------------------------------------

EEC' : {Γ' : Con'}(Γw : ⊢C Γ') → IEC' (EIC Γw) ≡ Γ'
EET' : {Γ' : Con'}(Γw : ⊢C Γ'){A' : Ty'}(Aw : Γ' ⊢T A') → IET' (EIT Γw Aw) ≡ A'

EEC' •w = refl
EEC' (Γw ,w Aw) = ,'= (EEC' Γw) (EET' Γw Aw)

EET' Γw (Uw _) = U'= (EEC' Γw)
EET' Γw (Πw _ Aw Bw) = Π'= (EEC' Γw) (EET' Γw Aw) (EET' (Γw ,w Aw) Bw)

EECw : {Γ' : Con'}(Γw : ⊢C Γ') → IECw (EIC Γw) ≡[ ⊢C= (EEC' Γw) ]≡ Γw
EECw Γw = uniqC _ _

EETw : {Γ' : Con'}(Γw : ⊢C Γ'){A' : Ty'}(Aw : Γ' ⊢T A') → IETw (EIT Γw Aw) ≡[ ⊢T= (EEC' Γw) (EET' Γw Aw) ]≡ Aw
EETw Γw Aw = uniqT _ _
-}

