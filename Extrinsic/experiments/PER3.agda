module Extrinsic.experiments.PER3 where

open import lib
open import TT.Decl.Syntax
open import TT.Decl.Congr syntaxDecl
open import TT.Core.Syntax
open import TT.Core.Congr syntaxCore
open import Extrinsic.experiments.Presyntax
open import Extrinsic.experiments.PER

open import NBE.Cheat

inv,1 : {Γ₀ Γ₁ : Con'}{A₀ A₁ : Ty'} → ⊢C (Γ₀ ,' A₀) ~ (Γ₁ ,' A₁) → ⊢C Γ₀ ~ Γ₁
inv,1 (ΓAw ◾C ΓAw₁) = {!inv,1 ΓAw!}
inv,1 (ΓAw ⁻¹C) = inv,1 ΓAw ⁻¹C
inv,1 (ΓAw ,w Aw) = ΓAw

EIC' : (Γ : Con')(Γw : ⊢C Γ ~ Γ) → Con
EIT' : (Γ : Con')(Γw : ⊢C Γ ~ Γ)(A : Ty')(Aw : Γ ⊢T A ~ A)
     → Ty (EIC' Γ Γw)
{-
EIs' : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δw : ⊢C Δ₀ ~ Δ₁)
       {σ₀ σ₁ : Tms'}(σw : Γ₀ ⊢s σ₀ ~ σ₁ ∶ Δ₀)
     → Tms (EIC' Γw) (EIC' Δw)
EIt' : {Γ₀ Γ₁ : Con'}(Γw : ⊢C Γ₀ ~ Γ₁){A₀ A₁ : Ty'}(Aw : Γ₀ ⊢T A₀ ~ A₁)
       {t₀ t₁ : Tm'}(tw : Γ₀ ⊢t t₀ ~ t₁ ∶ A₀)
     → Tm (EIC' Γw) (EIT' Γw Aw)
-}
EIC' •' Γw = •
EIC' (Γ ,' A) Γw = EIC' Γ {!!} , EIT' Γ {!!} A {!!}

EIT' Γ Γw (A [ σ ]T') Aw = {!!}
