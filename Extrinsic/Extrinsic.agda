module Extrinsic.Extrinsic where

-- systematic translation of the intrinsic syntax to extrinsic

infix 4 ⊢C_~_ _⊢T_~_ _⊢s_~_∶_ _⊢t_~_∶_ ⊢C_ _⊢T_ _⊢s_∶_ _⊢t_∶_
infixl 4 _◾C_ _◾T_ _◾s_ _◾t_
infix 5 _⁻¹C _⁻¹T _⁻¹s _⁻¹t

------------------------------------------------------------------------------
-- presyntax
------------------------------------------------------------------------------

data Con' : Set
data Ty'  : Set
data Tms' : Set
data Tm'  : Set

data Con' where
  •'   : Con'  -- \bub
  _,'_ : (Γ : Con')(A : Ty') → Con'

data Ty' where
  []T' : {Γ Δ : Con'}(A : Ty')(σ : Tms') → Ty'

data Tms' where
  id' : {Γ : Con'} → Tms'
  ∘'  : {Γ Δ Σ : Con'}(σ : Tms')(ν : Tms') → Tms'
  ε'  : {Γ : Con'} → Tms'
  ,s' : {Γ Δ : Con'}(σ : Tms'){A : Ty'}(t : Tm') → Tms'
  π₁' : {Γ Δ : Con'}{A : Ty'}(σ : Tms') →  Tms'

data Tm' where
  []t' : {Γ Δ : Con'}{A : Ty'}(t : Tm')(σ : Tms') → Tm'
  π₂'  : {Γ Δ : Con'}{A : Ty'}(σ : Tms') → Tm'

------------------------------------------------------------------------------
-- typing relations and conversion
------------------------------------------------------------------------------

data ⊢C_    : Con' → Set
data _⊢T_   : Con' → Ty' → Set
data _⊢s_∶_ : Con' → Tms' → Con' → Set
data _⊢t_∶_ : Con' → Tm' → Ty' → Set

data ⊢C_~_  : Con' → Con' → Set
data _⊢T_~_ : Con' → Ty' → Ty' → Set
data _⊢s_~_∶_ : Con' → Tms' → Tms' → Con' → Set
data _⊢t_~_∶_ : Con' → Tm' → Tm' → Ty' → Set

data ⊢C_ where
  •w : ⊢C •'
  ,w : {Γ : Con'}(Γw : ⊢C Γ){A : Ty'}(Aw : Γ ⊢T A) → ⊢C (Γ ,' A)

data _⊢T_ where
  []Tw : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
         {A : Ty'}(Aw : Δ ⊢T A){σ : Tms'}(σw : Γ ⊢s σ ∶ Δ)
       → Γ ⊢T []T' {Γ}{Δ} A σ

  -- coercion
  coeT : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){A : Ty'}(Aw : Γ₀ ⊢T A) → Γ₁ ⊢T A

data _⊢s_∶_ where
  idw  : {Γ : Con'}(Γw : ⊢C Γ) → Γ ⊢s id' {Γ} ∶ Γ
  ∘w   : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ){Σ : Con'}(Σw : ⊢C Σ)
         {σ : Tms'}(σw : Δ ⊢s σ ∶ Σ){ν : Tms'}(νw : Γ ⊢s ν ∶ Δ)
       → Γ ⊢s ∘' {Γ}{Δ}{Σ} σ ν ∶ Σ
  εw   : {Γ : Con'}(Γw : ⊢C Γ) → Γ ⊢s ε' {Γ} ∶ •'
  ,sw  : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
         {σ : Tms'}(σw : Γ ⊢s σ ∶ Δ){A : Ty'}(Aw : Δ ⊢T A)
         {t : Tm'}(tw : Γ ⊢t t ∶ []T' {Γ}{Δ} A σ)
       → Γ ⊢s ,s' {Γ}{Δ} σ {A} t ∶ (Δ ,' A)
  π₁w  : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
         {A : Ty'}(Aw : Δ ⊢T A){σ : Tms'}(σw : Γ ⊢s σ ∶ (Δ ,' A))
       → Γ ⊢s π₁' {Γ}{Δ}{A} σ ∶ Δ

  -- coercion
  coes : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δ~ : ⊢C Δ₀ ~ Δ₁)
         {σ : Tms'}(σw : Γ₀ ⊢s σ ∶ Δ₀) → Γ₁ ⊢s σ ∶ Δ₁

data _⊢t_∶_ where
  []tw : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
         {A : Ty'}(Aw : Δ ⊢T A){t : Tm'}(tw : Δ ⊢t t ∶ A)
         {σ : Tms'}(σw : Γ ⊢s σ ∶ Δ) → Γ ⊢t []t' {Γ}{Δ}{A} t σ ∶ ([]T' {Γ}{Δ} A σ) 
  π₂w  : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
         {A : Ty'}(Aw : Δ ⊢T A){σ : Tms'}(σw : Γ ⊢s σ ∶ Δ ,' A)
       → Γ ⊢t π₂' {Γ}{Δ}{A} σ ∶ []T' {Γ}{Δ} A (π₁' {Γ}{Δ}{A} σ)

  -- coercion
  coet : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){A₀ A₁ : Ty'}(A~ : Γ₀ ⊢T A₀ ~ A₁)
         {t : Tm'}(tw : Γ₀ ⊢t t ∶ A₀) → Γ₁ ⊢t t ∶ A₁

data ⊢C_~_ where
  -- PER
  _◾C_ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){Γ₂ : Con'}(Γ~' : ⊢C Γ₁ ~ Γ₂) → ⊢C Γ₀ ~ Γ₂
  _⁻¹C : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁) → ⊢C Γ₁ ~ Γ₀

  -- congruence
  •c : ⊢C •' ~ •' -- this might not be needed
  ,c : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁)
       {A₀ A₁ : Ty'}(A~ : Γ₀ ⊢T A₀ ~ A₁) -- an assymetry (later many such occurrences)
     → ⊢C Γ₀ ,' A₀ ~ Γ₁ ,' A₁

data _⊢T_~_ where
  -- PER
  _◾T_ : {Γ : Con'}{A₀ A₁ : Ty'}(A~ : Γ ⊢T A₀ ~ A₁){A₂ : Ty'}(A~' : Γ ⊢T A₁ ~ A₂) → Γ ⊢T A₀ ~ A₂
  _⁻¹T : {Γ : Con'}{A₀ A₁ : Ty'}(A~ : Γ ⊢T A₀ ~ A₁) → Γ ⊢T A₁ ~ A₀

  -- coercion
  coeT~ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){A₀ A₁ : Ty'}(A~ : Γ₀ ⊢T A₀ ~ A₁) → Γ₁ ⊢T A₀ ~ A₁

  []Tc : {Γ₀ Γ₁ : Con'}(Γc : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δc : ⊢C Δ₀ ~ Δ₁)
         {A₀ A₁ : Ty'}(Ac : Δ₀ ⊢T A₀ ~ A₁){σ₀ σ₁ : Tms'}(σc : Γ₀ ⊢s σ₀ ~ σ₁ ∶ Δ₀)
       → Γ₀ ⊢T []T' {Γ₀}{Δ₀} A₀ σ₀ ~ []T' {Γ₁}{Δ₁} A₁ σ₁

  -- conversion
  [id]Tw : {Γ : Con'}(Γw : ⊢C Γ){A : Ty'}(Aw : Γ ⊢T A) → Γ ⊢T []T' {Γ}{Γ} A (id' {Γ}) ~ A
  [][]Tw : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ){Σ : Con'}(Σw : ⊢C Σ)
          {A : Ty'}(Aw : Σ ⊢T A){σ : Tms'}(σw : Γ ⊢s σ ∶ Δ){δ : Tms'}(δw : Δ ⊢s δ ∶ Σ)
         → Γ ⊢T ([]T' {Γ}{Δ} ([]T' {Δ}{Σ} A δ) σ) ~ []T' {Γ}{Σ} A (∘' {Γ}{Δ}{Σ} δ σ)

data _⊢s_~_∶_ where

  -- PER
  _◾s_ : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ){σ₂ : Tms'}(σ~' : Γ ⊢s σ₁ ~ σ₂ ∶ Δ) → Γ ⊢s σ₀ ~ σ₂ ∶ Δ
  _⁻¹s : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → Γ ⊢s σ₁ ~ σ₀ ∶ Δ

  -- coercion
  coes~ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δ~ : ⊢C Δ₀ ~ Δ₁)
          {σ₀ σ₁ : Tms'}(σ~ : Γ₀ ⊢s σ₀ ~ σ₁ ∶ Δ₀) → Γ₁ ⊢s σ₀ ~ σ₁ ∶ Δ₁

  -- congruence
  idc  : {Γ₀ Γ₁ : Con'}(Γc : ⊢C Γ₀ ~ Γ₁) → Γ₀ ⊢s id' {Γ₀} ~ id' {Γ₁} ∶ Γ₀
  ∘c   : {Γ₀ Γ₁ : Con'}(Γc : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δc : ⊢C Δ₀ ~ Δ₁)
         {Σ₀ Σ₁ : Con'}(Σc : ⊢C Σ₀ ~ Σ₁)
         {σ₀ σ₁ : Tms'}(σc : Δ₀ ⊢s σ₀ ~ σ₁ ∶ Σ₀){ν₀ ν₁ : Tms'}(νc : Γ₀ ⊢s ν₀ ~ ν₁ ∶ Δ₀)
       → Γ₀ ⊢s ∘' {Γ₀}{Δ₀}{Σ₀} σ₀ ν₀ ~ ∘' {Γ₁}{Δ₁}{Σ₁} σ₁ ν₁ ∶ Σ₀
  εc   : {Γ₀ Γ₁ : Con'}(Γc : ⊢C Γ₀ ~ Γ₁) → Γ₀ ⊢s ε' {Γ₀} ~ ε' {Γ₁} ∶ •'
  ,sc  : {Γ₀ Γ₁ : Con'}(Γc : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δc : ⊢C Δ₀ ~ Δ₁)
         {σ₀ σ₁ : Tms'}(σc : Γ₀ ⊢s σ₀ ~ σ₁ ∶ Δ₀){A₀ A₁ : Ty'}(Ac : Δ₀ ⊢T A₀ ~ A₁)
         {t₀ t₁ : Tm'}(tc : Γ₀ ⊢t t₀ ~ t₁ ∶ []T' {Γ₀}{Δ₀} A₀ σ₀)
       → Γ₀ ⊢s ,s' {Γ₀}{Δ₀} σ₀ {A₀} t₀ ~ ,s' {Γ₁}{Δ₁} σ₁ {A₁} t₁ ∶ (Δ₀ ,' A₀)
  π₁c  : {Γ₀ Γ₁ : Con'}(Γc : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δc : ⊢C Δ₀ ~ Δ₁)
         {A₀ A₁ : Ty'}(Ac : Δ₀ ⊢T A₀ ~ A₁){σ₀ σ₁ : Tms'}(σc : Γ₀ ⊢s σ₀ ~ σ₁ ∶ (Δ₀ ,' A₀))
       → Γ₀ ⊢s π₁' {Γ₀}{Δ₀}{A₀} σ₀ ~ π₁' {Γ₁}{Δ₁}{A₁} σ₁ ∶ Δ₀
       
  -- conversion
  idlw : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
         {δ : Tms'}(δw : Γ ⊢s δ ∶ Δ) → Γ ⊢s ∘' {Γ}{Δ}{Δ} (id' {Δ}) δ ~ δ ∶ Δ
  idrw : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
         {δ : Tms'}(δw : Γ ⊢s δ ∶ Δ) → Γ ⊢s ∘' {Γ}{Γ}{Δ} δ (id' {Γ}) ~ δ ∶ Δ
  assw : {Γ : Con'}(Γw : ⊢C Γ)
         {Δ : Con'}(Δw : ⊢C Δ)
         {Σ : Con'}(Σw : ⊢C Σ)
         {Ω : Con'}(Ωw : ⊢C Ω)
         {σ : Tms'}(σw : Σ ⊢s σ ∶ Ω)
         {δ : Tms'}(δw : Δ ⊢s δ ∶ Σ)
         {ν : Tms'}(ν' : Γ ⊢s ν ∶ Δ)
       → Γ ⊢s ∘' {Γ}{Δ}{Ω} (∘' {Δ}{Σ}{Ω} σ δ) ν ~ ∘' {Γ}{Σ}{Ω} σ (∘' {Γ}{Δ}{Σ} δ ν) ∶ Ω
  ,∘w  : {Γ : Con'}(Γw : ⊢C Γ)
         {Δ : Con'}(Δw : ⊢C Δ)
         {Σ : Con'}(Σw : ⊢C Σ)
         {δ : Tms'}(δw : Γ ⊢s δ ∶ Δ)
         {σ : Tms'}(σw : Σ ⊢s σ ∶ Γ)
         {A : Ty'}(Aw : Δ ⊢T A)
         {a : Tm'}(aw : Γ ⊢t a ∶ ([]T' {Γ}{Δ} A δ))
       → Σ ⊢s ∘' {Σ}{Γ}{Δ ,' A} (,s' {Γ}{Δ} δ {A} a) σ ~ ,s' {Σ}{Δ} (∘' {Σ}{Γ}{Δ} δ σ) {A} ([]t' {Σ}{Γ}{[]T' {Γ}{Δ} A δ} a σ) ∶ Δ ,' A
  π₁βw : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
         {A : Ty'}(Aw : Δ ⊢T A){δ : Tms'}(δw : Γ ⊢s δ ∶ Δ)
         {a : Tm'}(aw : Γ ⊢t a ∶ ([]T' {Γ}{Δ} A δ))
       → Γ ⊢s π₁' {Γ}{Δ}{A} (,s' {Γ}{Δ} δ {A} a) ~ δ ∶ Δ
  πηw  : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
         {A : Ty'}(Aw : Δ ⊢T A){δ : Tms'}(δw : Γ ⊢s δ ∶ (Δ ,' A))
       → Γ ⊢s ,s' {Γ}{Δ} (π₁' {Γ}{Δ}{A} δ) {A} (π₂' {Γ}{Δ}{A} δ) ~ δ ∶ Δ ,' A
  εηw  : {Γ : Con'}(Γw : ⊢C Γ){σ : Tms'}(σw : Γ ⊢s σ ∶ •') → Γ ⊢s σ ~ ε' {Γ} ∶ •'

data _⊢t_~_∶_ where

  -- PER
  _◾t_ : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(t~ : Γ ⊢t t₀ ~ t₁ ∶ A){t₂ : Tm'}(t~' : Γ ⊢t t₁ ~ t₂ ∶ A) → Γ ⊢t t₀ ~ t₂ ∶ A
  _⁻¹t : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(t~ : Γ ⊢t t₀ ~ t₁ ∶ A) → Γ ⊢t t₁ ~ t₀ ∶ A

  -- coercion
  coet~ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁){A₀ A₁ : Ty'}(A~ : Γ₀ ⊢T A₀ ~ A₁)
          {t₀ t₁ : Tm'}(t~ : Γ₀ ⊢t t₀ ~ t₁ ∶ A₀) → Γ₁ ⊢t t₀ ~ t₁ ∶ A₁
          
  -- congruence
  []tc : {Γ₀ Γ₁ : Con'}(Γc : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δc : ⊢C Δ₀ ~ Δ₁)
         {A₀ A₁ : Ty'}(Ac : Δ₀ ⊢T A₀ ~ A₁){t₀ t₁ : Tm'}(tc : Δ₀ ⊢t t₀ ~ t₁ ∶ A₀)
         {σ₀ σ₁ : Tms'}(σc : Γ₀ ⊢s σ₀ ~ σ₁ ∶ Δ₀) → Γ₀ ⊢t []t' {Γ₀}{Δ₀}{A₀} t₀ σ₀ ~ []t' {Γ₁}{Δ₁}{A₁} t₁ σ₁ ∶ ([]T' {Γ₀}{Δ₀} A₀ σ₀) 
  π₂c  : {Γ₀ Γ₁ : Con'}(Γc : ⊢C Γ₀ ~ Γ₁){Δ₀ Δ₁ : Con'}(Δc : ⊢C Δ₀ ~ Δ₁)
         {A₀ A₁ : Ty'}(Ac : Δ₀ ⊢T A₀ ~ A₁){σ₀ σ₁ : Tms'}(σc : Γ₀ ⊢s σ₀ ~ σ₁ ∶ Δ₀ ,' A₀)
       → Γ₀ ⊢t π₂' {Γ₀}{Δ₀}{A₀} σ₀ ~ π₂' {Γ₁}{Δ₁}{A₁} σ₁ ∶ []T' {Γ₀}{Δ₀} A₀ (π₁' {Γ₀}{Δ₀}{A₀} σ₀)

  -- conversion
  π₂βw : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ)
         {A : Ty'}(Aw : Δ ⊢T A)
         {δ : Tms'}(δw : Γ ⊢s δ ∶ Δ)
         {a : Tm'}(aw : Γ ⊢t a ∶ ([]T' {Γ}{Δ} A δ))
       → Γ ⊢t (π₂' {Γ}{Δ}{A} (,s' {Γ}{Δ} δ {A} a)) ~ a ∶ []T' {Γ}{Δ} A δ

open import lib

------------------------------------------------------------------------------
-- these are all propositions
------------------------------------------------------------------------------

postulate
  ⊢C-prop : {Γ : Con'}(Γw₀ Γw₁ : ⊢C Γ) → Γw₀ ≡ Γw₁
  ⊢T-prop : {Γ : Con'}{A : Ty'}(Aw₀ Aw₁ : Γ ⊢T A) → Aw₀ ≡ Aw₁
  ⊢s-prop : {Γ : Con'}{Δ : Con'}{σ : Tms'}(σw₀ σw₁ : Γ ⊢s σ ∶ Δ) → σw₀ ≡ σw₁
  ⊢t-prop : {Γ : Con'}{A : Ty'}{t : Tm'}(tw₀ tw₁ : Γ ⊢t t ∶ A) → tw₀ ≡ tw₁

------------------------------------------------------------------------------
-- reflexivity of conversion relations
------------------------------------------------------------------------------

reflC : {Γ : Con'} → ⊢C Γ → ⊢C Γ ~ Γ
reflT : {Γ : Con'}{A : Ty'} → Γ ⊢T A → Γ ⊢T A ~ A
refls : {Γ Δ : Con'}{σ : Tms'} → Γ ⊢s σ ∶ Δ → Γ ⊢s σ ~ σ ∶ Δ
reflt : {Γ : Con'}{A : Ty'}{t : Tm'} → Γ ⊢t t ∶ A → Γ ⊢t t ~ t ∶ A

reflC •w = •c
reflC (,w Γw Aw) = ,c (reflC Γw) (reflT Aw)

reflT ([]Tw Γw Δw Aw σw) = []Tc (reflC Γw) (reflC Δw) (reflT Aw) (refls σw)
reflT (coeT Γ~ Aw) = coeT~ Γ~ (reflT Aw)

refls (idw Γw) = idc (reflC Γw)
refls (∘w Γw Δw Σw σw νw) = ∘c (reflC Γw) (reflC Δw) (reflC Σw) (refls σw) (refls νw)
refls (εw Γw) = εc (reflC Γw)
refls (,sw Γw Δw σw Aw tw) = ,sc (reflC Γw) (reflC Δw) (refls σw) (reflT Aw) (reflt tw)
refls (π₁w Γw Δw Aw σw) = π₁c (reflC Γw) (reflC Δw) (reflT Aw) (refls σw)
refls (coes Γ~ Δ~ σw) = coes~ Γ~ Δ~ (refls σw)

reflt ([]tw Γw Δw Aw tw σw) = []tc (reflC Γw) (reflC Δw) (reflT Aw) (reflt tw) (refls σw)
reflt (π₂w Γw Δw Aw σw) = π₂c (reflC Γw) (reflC Δw) (reflT Aw) (refls σw)
reflt (coet Γ~ A~ tw) = coet~ Γ~ A~ (reflt tw)

------------------------------------------------------------------------------
-- typing from conversion
------------------------------------------------------------------------------

~C₀ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁) → ⊢C Γ₀
~C₁ : {Γ₀ Γ₁ : Con'}(Γ~ : ⊢C Γ₀ ~ Γ₁) → ⊢C Γ₁
~T₀ : {Γ : Con'}{A₀ A₁ : Ty'}(A~ : Γ ⊢T A₀ ~ A₁) → Γ ⊢T A₀
~T₁ : {Γ : Con'}{A₀ A₁ : Ty'}(A~ : Γ ⊢T A₀ ~ A₁) → Γ ⊢T A₁
~s₀ : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → Γ ⊢s σ₀ ∶ Δ
~s₁ : {Γ Δ : Con'}{σ₀ σ₁ : Tms'}(σ~ : Γ ⊢s σ₀ ~ σ₁ ∶ Δ) → Γ ⊢s σ₁ ∶ Δ
~t₀ : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(t~ : Γ ⊢t t₀ ~ t₁ ∶ A) → Γ ⊢t t₀ ∶ A
~t₁ : {Γ : Con'}{A : Ty'}{t₀ t₁ : Tm'}(t~ : Γ ⊢t t₀ ~ t₁ ∶ A) → Γ ⊢t t₁ ∶ A

~C₀ (Γ~ ◾C Γ~₁) = ~C₀ Γ~
~C₀ (Γ~ ⁻¹C) = ~C₁ Γ~
~C₀ •c = •w
~C₀ (,c Γ~ A~) = ,w (~C₀ Γ~) (~T₀ A~)
~C₁ (Γ~ ◾C Γ~₁) = ~C₁ Γ~₁
~C₁ (Γ~ ⁻¹C) = ~C₀ Γ~
~C₁ •c = •w
~C₁ (,c Γ~ A~) = ,w (~C₁ Γ~) (coeT Γ~ (~T₁ A~))
   
~T₀ (Aw ◾T Aw₁) = ~T₀ Aw
~T₀ (Aw ⁻¹T) = ~T₁ Aw
~T₀ (coeT~ Γ~ Aw) = coeT Γ~ (~T₀ Aw)
~T₀ ([]Tc Γc Δc Ac σc) = []Tw (~C₀ Γc) (~C₀ Δc) (~T₀ Ac) (~s₀ σc)
~T₀ ([id]Tw Γw Aw) = []Tw Γw Γw Aw (idw Γw)
~T₀ ([][]Tw Γw Δw Σw Aw σw δw) = []Tw Γw Δw ([]Tw Δw Σw Aw δw) σw
~T₁ (Aw ◾T Aw₁) = ~T₁ Aw₁
~T₁ (Aw ⁻¹T) = ~T₀ Aw
~T₁ (coeT~ Γ~ Aw) = coeT Γ~ (~T₁ Aw)
~T₁ ([]Tc Γc Δc Ac σc) = coeT (Γc ⁻¹C) ([]Tw (~C₁ Γc) (~C₁ Δc) (coeT Δc (~T₁ Ac)) (coes Γc Δc (~s₁ σc)) )
~T₁ ([id]Tw Γw Aw) = Aw
~T₁ ([][]Tw Γw Δw Σw Aw σw δw) = []Tw Γw Σw Aw (∘w Γw Δw Σw δw σw)

~s₀ (σw ◾s σw₁) = ~s₀ σw
~s₀ (σw ⁻¹s) = ~s₁ σw
~s₀ (coes~ Γ~ Δ~ σw) = coes Γ~ Δ~ (~s₀ σw)
~s₀ (idc Γc) = idw (~C₀ Γc)
~s₀ (∘c Γc Δc Σc σw νw) = ∘w (~C₀ Γc) (~C₀ Δc) (~C₀ Σc) (~s₀ σw) (~s₀ νw)
~s₀ (εc Γc) = εw (~C₀ Γc)
~s₀ (,sc Γc Δc σc Ac tc) = ,sw (~C₀ Γc) (~C₀ Δc) (~s₀ σc) (~T₀ Ac) (~t₀ tc)
~s₀ (π₁c Γc Δc Ac σc) = π₁w (~C₀ Γc) (~C₀ Δc) (~T₀ Ac) (~s₀ σc)
~s₀ (idlw Γw Δw δw) = ∘w Γw Δw Δw (idw Δw) δw
~s₀ (idrw Γw Δw δw) = ∘w Γw Γw Δw δw (idw Γw)
~s₀ (assw Γw Δw Σw Ωw σw δw νw) = ∘w Γw Δw Ωw (∘w Δw Σw Ωw σw δw) νw
~s₀ (,∘w Γw Δw Σw δw σw Aw aw) = ∘w Σw Γw (,w Δw Aw) (,sw Γw Δw δw Aw aw) σw 
~s₀ (π₁βw Γw Δw Aw δw aw) = π₁w Γw Δw Aw (,sw Γw Δw δw Aw aw)
~s₀ (πηw Γw Δw Aw δw) = ,sw Γw Δw (π₁w Γw Δw Aw δw) Aw (π₂w Γw Δw Aw δw)
~s₀ (εηw Γw σw) = σw
~s₁ (σw ◾s σw₁) = ~s₁ σw₁
~s₁ (σw ⁻¹s) = ~s₀ σw
~s₁ (coes~ Γ~ Δ~ σw) = coes Γ~ Δ~ (~s₁ σw)
~s₁ (idc Γc) = coes (Γc ⁻¹C) (Γc ⁻¹C) (idw (~C₁ Γc))
~s₁ (∘c Γc Δc Σc σw νw) = coes (Γc ⁻¹C) (Σc ⁻¹C)
                               (∘w (~C₁ Γc) (~C₁ Δc) (~C₁ Σc) (coes Δc Σc (~s₁ σw)) (coes Γc Δc (~s₁ νw)))
~s₁ (εc Γc) = coes (Γc ⁻¹C) •c (εw (~C₁ Γc))
~s₁ (,sc Γc Δc σc Ac tc) = coes (Γc ⁻¹C) (,c Δc Ac ⁻¹C)
                                (,sw (~C₁ Γc) (~C₁ Δc) (coes Γc Δc (~s₁ σc)) (coeT Δc (~T₁ Ac)) (coet Γc ([]Tc Γc Δc Ac σc) (~t₁ tc)))
~s₁ (π₁c Γc Δc Ac σc) = coes (Γc ⁻¹C) (Δc ⁻¹C)
                             (π₁w (~C₁ Γc) (~C₁ Δc) (coeT Δc (~T₁ Ac)) (coes Γc (,c Δc Ac) (~s₁ σc)))
~s₁ (idlw Γw Δw δw) = δw
~s₁ (idrw Γw Δw δw) = δw
~s₁ (assw Γw Δw Σw Ωw σw δw νw) = ∘w Γw Σw Ωw σw (∘w Γw Δw Σw δw νw)
~s₁ (,∘w Γw Δw Σw δw σw Aw aw) = ,sw Σw Δw (∘w Σw Γw Δw δw σw) Aw (coet (reflC Σw) ([][]Tw Σw Γw Δw Aw σw δw) ([]tw Σw Γw ([]Tw Γw Δw Aw δw) aw σw))
~s₁ (π₁βw Γw Δw Aw δw aw) = δw
~s₁ (πηw Γw Δw Aw δw) = δw
~s₁ (εηw Γw σw) = εw Γw

~t₀ (tw ◾t tw₁) = ~t₀ tw
~t₀ (tw ⁻¹t) = ~t₁ tw
~t₀ (coet~ Γ~ A~ tw) = coet Γ~ A~ (~t₀ tw)
~t₀ ([]tc Γc Δc Ac tw σc) = []tw (~C₀ Γc) (~C₀ Δc) (~T₀ Ac) (~t₀ tw) (~s₀ σc)
~t₀ (π₂c Γc Δc Ac σc) = π₂w (~C₀ Γc) (~C₀ Δc) (~T₀ Ac) (~s₀ σc)
~t₀ (π₂βw Γw Δw Aw δw aw) = coet (reflC Γw) ([]Tc (reflC Γw) (reflC Δw) (reflT Aw) (π₁βw Γw Δw Aw δw aw))
                                (π₂w Γw Δw Aw (,sw Γw Δw δw Aw aw))
~t₁ (tw ◾t tw₁) = ~t₁ tw₁
~t₁ (tw ⁻¹t) = ~t₀ tw
~t₁ (coet~ Γ~ A~ tw) = coet Γ~ A~ (~t₁ tw)
~t₁ ([]tc Γc Δc Ac tw σc) = coet (Γc ⁻¹C) (coeT~ Γc ([]Tc Γc Δc Ac σc ⁻¹T))
                                 ([]tw (~C₁ Γc) (~C₁ Δc) (coeT Δc (~T₁ Ac))
                                       (coet Δc Ac (~t₁ tw))
                                       (coes Γc Δc (~s₁ σc)))
~t₁ (π₂c Γc Δc Ac σc) = coet (Γc ⁻¹C) (coeT~ Γc ([]Tc Γc Δc Ac (π₁c Γc Δc Ac σc) ⁻¹T))
                             (π₂w (~C₁ Γc) (~C₁ Δc) (coeT Δc (~T₁ Ac)) (coes Γc (,c Δc Ac) (~s₁ σc)))
~t₁ (π₂βw Γw Δw Aw δw aw) = aw
