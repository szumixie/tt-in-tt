{-# OPTIONS --without-K --prop #-}

module Setoid.SPropPredicative.Func where

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core

∣Π∣ : {Γ : Con}{b : Bool}(A : Ty Γ b)(B : Ty (Γ , A) b)(γ : ∣ Γ ∣C) → Set ⌜ b ⌝
∣Π∣ {Γ}{true}  A B γ = Σsp ((α : ∣ A ∣T γ) → ∣ B ∣T (γ ,Σ α)) λ ∣_∣Π → {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ,p α~ ⊢ ∣_∣Π α ~ ∣_∣Π α'
∣Π∣ {Γ}{false} A B γ = Σsp ((α : ∣ A ∣T γ) → ∣ B ∣T (γ ,Σ α)) λ ∣_∣Π → {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ,p α~ ⊢ ∣_∣Π α ~ ∣_∣Π α'

infix 4 ∣_∣Π

∣_∣Π : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Prop ℓ'} → Σsp A B → A
∣_∣Π = proj₁sp

~Π : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Prop ℓ'}(w : Σsp A B) → B (proj₁sp w)
~Π = proj₂sp

Π : {Γ : Con}{b : Bool}(A : Ty Γ b) → Ty (Γ , A) b → Ty Γ b
Π {Γ}{true} A B = record
  { ∣_∣T_ = ∣Π∣ A B
  ; _T_⊢_~_ = λ {γ}{γ'} γ~ f f' → {α : ∣ A ∣T γ}{α' : ∣ A ∣T γ'}(α~ : A T γ~ ⊢ α ~ α') → B T γ~ ,p α~ ⊢ ∣ f ∣Π α ~ ∣ f' ∣Π α'
  ; refT = ~Π
  ; symT = λ f~ α~ → symT B (f~ (symT A α~))
  ; transT = λ {_}{_}{_}{γ~} f~ f~' {α}{α''} α~ → transT B (f~ (cohT A γ~ α)) (f~' (transT A (symT A (cohT A γ~ α)) α~))
  ; coeT = λ {γ}{γ'} γ~ f → (λ α' → coeT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α'))) ,sp (λ {α}{α'} α~ → transT B (symT B (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α)) (∣ f ∣Π (coeT A (symC Γ γ~) α)))) (transT B (~Π f (transT A (symT A (cohT A (symC Γ γ~) α)) (transT A α~ (cohT A (symC Γ γ~) α')))) (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α')))))
  ; cohT = λ {γ}{γ'} γ~ f {α}{α'} α~ → transT B (~Π f (transT A α~ (cohT A (symC Γ γ~) α'))) (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α')))
  }
Π {Γ}{false} A B = record
  { ∣_∣T_ = ∣Π∣ A B
  ; _T_⊢_~_ = λ {γ}{γ'} γ~ f f' → {α : ∣ A ∣T γ}{α' : ∣ A ∣T γ'}(α~ : A T γ~ ⊢ α ~ α') → B T γ~ ,p α~ ⊢ ∣ f ∣Π α ~ ∣ f' ∣Π α'
  ; refT = ~Π
  ; symT = λ f~ α~ → symT B (f~ (symT A α~))
  ; transT = λ {_}{_}{_}{γ~} f~ f~' {α}{α''} α~ → transT B (f~ (cohT A γ~ α)) (f~' (transT A (symT A (cohT A γ~ α)) α~))
  ; coeT = λ {γ}{γ'} γ~ f → (λ α' → coeT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α'))) ,sp (λ {α}{α'} α~ → transT B (symT B (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α)) (∣ f ∣Π (coeT A (symC Γ γ~) α)))) (transT B (~Π f (transT A (symT A (cohT A (symC Γ γ~) α)) (transT A α~ (cohT A (symC Γ γ~) α')))) (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α')))))
  ; cohT = λ {γ}{γ'} γ~ f {α}{α'} α~ → transT B (~Π f (transT A α~ (cohT A (symC Γ γ~) α'))) (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α')))
  }

Π[] : {Γ Δ : Con} {σ : Tms Γ Δ}{b : Bool}{A : Ty Δ b} {B : Ty (Δ , A) b} →
  Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ _,s_ (σ ∘ π₁ {A = A [ σ ]T} id) {A = A} (π₂ {A = A [ σ ]T} id) ]T)
Π[] {b = true}  = refl
Π[] {b = false} = refl

lam : {Γ : Con} {b : Bool} {A : Ty Γ b} {B : Ty (Γ , A) b} → Tm (Γ , A) B → Tm Γ (Π A B)
lam {Γ}{true} {A}{B} t = record { ∣_∣t = λ γ → (λ α → ∣ t ∣t (γ ,Σ α)) ,sp λ α~ → ~t t (refC Γ γ ,p α~) ; ~t = λ γ~ α~ → ~t t (γ~ ,p α~) }
lam {Γ}{false}{A}{B} t = record { ∣_∣t = λ γ → (λ α → ∣ t ∣t (γ ,Σ α)) ,sp λ α~ → ~t t (refC Γ γ ,p α~) ; ~t = λ γ~ α~ → ~t t (γ~ ,p α~) }

app : {Γ : Con} {b : Bool} {A : Ty Γ b} {B : Ty (Γ , A) b} → Tm Γ (Π A B) → Tm (Γ , A) B
app {Γ}{true} {A}{B} t = record { ∣_∣t = λ { (γ ,Σ α) → ∣ ∣ t ∣t γ ∣Π α } ; ~t = λ { (γ~ ,p α~) → ~t t γ~ α~ } }
app {Γ}{false}{A}{B} t = record { ∣_∣t = λ { (γ ,Σ α) → ∣ ∣ t ∣t γ ∣Π α } ; ~t = λ { (γ~ ,p α~) → ~t t γ~ α~ } }

lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ true}{B : Ty (Δ , A) true}{t : Tm (Δ , A) B} →
  _≡_ {A = Tm Γ (Π A B [ δ ]T)}
      (_[_]t {Γ}{Δ}{true}{Π A B}(lam {Δ}{true}{A}{B} t) δ)
      (lam {Γ}{true}{A [ δ ]T}{B [ δ ^ A ]T} (t [ δ ^ A ]t))
lam[] = refl

Πβ : {Γ : Con}{b : Bool} {A : Ty Γ b} {B : Ty (Γ , A) b}{t : Tm (Γ , A) B} → app {Γ}(lam t) ≡ t
Πβ {Γ} {true} = refl
Πβ {Γ} {false} = refl

Πη : {Γ : Con}{b : Bool} {A : Ty Γ b} {B : Ty (Γ , A) b}{t : Tm Γ (Π A B)} → lam (app t) ≡ t
Πη {Γ} {true} = refl
Πη {Γ} {false} = refl

-- functional extensionality

open import Setoid.SPropPredicative.Iden

extt : {Γ : Con}{A : Ty Γ true} {B : Ty (Γ , A) true} {f g : Tm Γ (Π A B)} → Tm (Γ , A) (Id {A = B} (app {A = A} f) (app {A = A} g)) → Tm Γ (Id {A = Π A B} f g)
extf : {Γ : Con}{A : Ty Γ false}{B : Ty (Γ , A) false}{f g : Tm Γ (Π A B)} → Tm (Γ , A) (Id {A = B} (app {A = A} f) (app {A = A} g)) → Tm Γ (Id {A = Π A B} f g)
extt {Γ}{A}{B}{f}{g} t = record { ∣_∣t = λ γ → liftp λ {α}{α'} α~ → transT B (unliftp (∣ t ∣t (γ ,Σ α))) (~Π (∣ g ∣t γ) α~) }
extf {Γ}{A}{B}{f}{g} t = record { ∣_∣t = λ γ → liftp λ {α}{α'} α~ → transT B (unliftp (∣ t ∣t (γ ,Σ α))) (~Π (∣ g ∣t γ) α~) }
