{-# OPTIONS --without-K  #-}

module Setoid.SPropPredicative.experiments.IdenProp where

open import lib
open import Setoid.SProp.lib

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core
open import Setoid.SPropPredicative.Props0

Id : {Γ : Con}{A : Ty Γ true} → Tm Γ A → Tm Γ A → Tm Γ ℙ₀
Id {Γ}{A} t u = record { ∣_∣t = λ γ → A T refC Γ γ ⊢ ∣ t ∣t γ ~ ∣ u ∣t γ
                       ; ~t = λ γ~ → liftP ((λ a~ → transT A (symT A (~t t γ~)) (transT A a~ (~t u γ~)))
                                           ,p λ a~ → transT A (~t t γ~) (transT A a~ (symT A (~t u γ~)))) }

Id[] : ∀{Γ Θ}{A : Ty Γ true}{u v : Tm Γ A}{σ : Tms Θ Γ}
     → Id u v [ σ ]t ≡ Id (u [ σ ]t) (v [ σ ]t)
Id[] = refl

ref : ∀{Γ}{A : Ty Γ true}(u : Tm Γ A) → Tm Γ (Elℙ₀ (Id u u))
ref {Γ}{A} u = record { ∣_∣t = λ γ → liftp (~t u (refC Γ γ)) }

ref[] : ∀{Γ Θ}{A : Ty Γ true} {u : Tm Γ A}{σ : Tms Θ Γ} → ref u [ σ ]t ≡ ref (u [ σ ]t)
ref[] = refl

-- dependent eliminator

line : {Γ : Con}{A : Ty Γ true}(a u : Tm Γ A)(p : Tm Γ (Elℙ₀ (Id a u))) → Tms Γ (Γ , A , Elℙ₀ (Id (a [ wk' A ]t) (vz' A)))
line {Γ}{A}  a u p = _,s_ (_,s_ id {_}{A} u) {_}{Elℙ₀ (Id (a [ wk' A ]t) (vz' A))} p


IdJ : {Γ : Con}{b : Bool}{A : Ty Γ true}(u : Tm Γ A)(P : Ty (Γ , A , Elℙ₀ (Id (u [ wk' A ]t) (vz' A))) b)
      (t : Tm Γ (P [ line u u (ref u) ]T)) {v : Tm Γ A}(p : Tm Γ (Elℙ₀ (Id u v)))
    → Tm Γ (P [ line u v p ]T)
IdJ {Γ}{b} u P t {v} p = record { ∣_∣t = λ γ → coeT P ((refC Γ _ ,p unliftp (∣ p ∣t γ)) ,p ttp) (∣ t ∣t γ)
                            ; ~t = λ {γ}{γ'} γ~ → transT P (symT P (cohT P _ _)) (transT P (~t t γ~) (cohT P _ _)) }

IdJβ : {Γ : Con}{A : Ty Γ true}{u : Tm Γ A}{P : Ty (Γ , A , Elℙ₀ (Id (u [ wk' A ]t) (vz' A))) true}
       {t : Tm Γ (P [ line u u (ref u) ]T)}
       → Tm Γ (Elℙ₀ (Id (IdJ u P t {u} (ref u)) t))
IdJβ {u = u}{P}{t} = record { ∣_∣t = λ γ → liftp (symT P (cohT P _ (∣ t ∣t γ))) }

IdJ[] : ∀{Γ Θ}{σ : Tms Θ Γ}{b : Bool}{A : Ty Γ true}{u : Tm Γ A}{P : Ty (Γ , A , Elℙ₀ (Id (u [ wk' A ]t) (vz' A))) b}
        {t : Tm Γ (P [ line u u (ref u) ]T)}{v : Tm Γ A}{p : Tm Γ (Elℙ₀ (Id u v))}
        → (IdJ u P t {v} p) [ σ ]t ≡ IdJ {A = A [ σ ]T}(u [ σ ]t)(P [ σ ^ A ^ Elℙ₀ (Id (u [ wk' A ]t) (vz' A)) ]T) (t [ σ ]t) {v [ σ ]t} (p [ σ ]t)
IdJ[] = refl
