{-# OPTIONS --type-in-type --without-K #-}

module Setoid.SPropPredicative.experiments.Props1 where

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core

-- a universe of propositions

ℙ₁ : {Γ : Con} → Ty Γ false
ℙ₁ {Γ} = record
  { ∣_∣T_ = λ γ → Prop₁
  ; _T_⊢_~_ = λ _ a b → LiftP ((a → b) ×p (b → a))
  ; refT = λ _ → liftP ((λ x → x) ,p (λ x → x))
  ; symT = λ { (liftP (f ,p g)) → liftP (g ,p f) }
  ; transT = λ { (liftP (f ,p g)) (liftP (f' ,p g')) → liftP ((λ x → f' (f x)) ,p (λ y → g (g' y))) }
  ; coeT = λ _ a → a
  ; cohT = λ _ _ → liftP ((λ x → x) ,p (λ x → x))
  }

Elℙ₁ : {Γ : Con} → Tm Γ ℙ₁ → Ty Γ true
Elℙ₁ {Γ} t = record
  { ∣_∣T_ = λ γ → Liftp (∣ t ∣t γ)
  ; _T_⊢_~_ = λ _ _ _ → ⊤p
  ; coeT = λ { γ~ (liftp α) → liftp (proj₁p (unliftP (~t t γ~)) α) }
  }

ℙ₁[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ℙ₁ [ σ ]T ≡ ℙ₁
ℙ₁[] = refl
Elℙ₁[] : {Γ Δ : Con}{σ : Tms Γ Δ}{t : Tm Δ ℙ₁} → Elℙ₁ t [ σ ]T ≡ Elℙ₁ (t [ σ ]t)
Elℙ₁[] = refl

-- strict Prop

irr : {Γ : Con}{a : Tm Γ ℙ₁}(u v : Tm Γ (Elℙ₁ a)) → u ≡ v
irr u v = refl

-- closed under unit

⊤ℙ₁ : {Γ : Con} → Tm Γ ℙ₁
⊤ℙ₁ {Γ} = record { ∣_∣t = λ _ → ⊤p ; ~t = λ _ → liftP ((λ x → x) ,p (λ x → x)) }

ttℙ₁ : {Γ : Con} → Tm Γ (Elℙ₁ ⊤ℙ₁)
ttℙ₁ {Γ} = record {}

⊤ℙ₁[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ⊤ℙ₁ [ σ ]t ≡ ⊤ℙ₁
⊤ℙ₁[] = refl
ttℙ₁[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ttℙ₁ [ σ ]t ≡ ttℙ₁
ttℙ₁[] = refl

-- closed under empty

⊥ℙ₁ : {Γ : Con} → Tm Γ ℙ₁
⊥ℙ₁ {Γ} = record { ∣_∣t = λ γ → ⊥p ; ~t = λ _ → liftP ((λ x → x) ,p (λ x → x)) }

abortℙ₁ : {Γ : Con}{b : Bool}{A : Ty Γ b} → Tm Γ (Elℙ₁ ⊥ℙ₁) → Tm Γ A
abortℙ₁ {Γ}{A} t = record
  { ∣_∣t = λ γ → abort⊥p (unliftp (∣ t ∣t γ))
  ; ~t = λ {γ} _ → abort⊥pp (unliftp (∣ t ∣t γ))
  }

⊥ℙ₁[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ⊥ℙ₁ [ σ ]t ≡ ⊥ℙ₁
⊥ℙ₁[] = refl
abortℙ₁[] : {Γ Δ : Con}{σ : Tms Γ Δ}{b : Bool}{A : Ty Δ b}{t : Tm Δ (Elℙ₁ ⊥ℙ₁)} → abortℙ₁ {A = A} t [ σ ]t ≡ abortℙ₁ (t [ σ ]t)
abortℙ₁[] = refl

-- closed under Pi

Πℙ₁ : {Γ : Con}(A : Ty Γ true)(b : Tm (Γ , A) ℙ₁) → Tm Γ ℙ₁
Πℙ₁ {Γ} A b = record
  { ∣_∣t = λ γ → (α : ∣ A ∣T γ) → ∣ b ∣t (γ ,Σ α)
    ; ~t = λ {γ}{γ'} γ~ → liftP ((λ f α' → proj₁p (unliftP (~t b (γ~ ,p symT A (cohT A (symC Γ γ~) α')))) (f (coeT A (symC Γ γ~) α'))) ,p
                                 λ f' α → proj₂p (unliftP (~t b (γ~ ,p cohT A γ~ α))) (f' (coeT A γ~ α))) }

lamℙ₁ : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ , A) ℙ₁} → Tm (Γ , A) (Elℙ₁ b) → Tm Γ (Elℙ₁ (Πℙ₁ A b))
lamℙ₁ {Γ}{A}{b} t = record { ∣_∣t = λ γ → liftp λ α → unliftp (∣ t ∣t (γ ,Σ α)) }

appℙ₁ : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ , A) ℙ₁} → Tm Γ (Elℙ₁ (Πℙ₁ A b)) → Tm (Γ , A) (Elℙ₁ b)
appℙ₁ {Γ}{A}{b} t = record { ∣_∣t = λ { (γ ,Σ α) → liftp (unliftp (∣ t ∣t γ) α) } }

Πℙ₁β : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ , A) ℙ₁}{t : Tm (Γ , A) (Elℙ₁ b)} → appℙ₁ {A = A}{b} (lamℙ₁ {A = A}{b} t) ≡ t
Πℙ₁β = refl

Πℙ₁η : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ , A) ℙ₁}{t : Tm Γ (Elℙ₁ (Πℙ₁ A b))} → lamℙ₁ {A = A}{b} (appℙ₁ {A = A}{b} t) ≡ t
Πℙ₁η = refl

Πℙ₁[] : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ , A) ℙ₁}{Θ : Con}{σ : Tms Θ Γ} → Πℙ₁ A b [ σ ]t ≡ Πℙ₁ (A [ σ ]T) (b [ σ ^ A ]t)
Πℙ₁[] = refl

lamℙ₁[] : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ , A) ℙ₁}{t : Tm (Γ , A) (Elℙ₁ b)}{Θ : Con}{σ : Tms Θ Γ} →
  lamℙ₁ {A = A}{b} t [ σ ]t ≡ lamℙ₁ {A = A [ σ ]T}{b [ σ ^ A ]t}(t [ σ ^ A ]t)
lamℙ₁[] = refl

-- closed under Sigma

Σℙ₁ : {Γ : Con}(a : Tm Γ ℙ₁)(b : Tm (Γ , Elℙ₁ a) ℙ₁) → Tm Γ ℙ₁
Σℙ₁ {Γ} a b = record
  { ∣_∣t = λ γ → Σp (∣ a ∣t γ) λ α → ∣ b ∣t (γ ,Σ liftp α)
  ; ~t = λ {γ}{γ'} γ~ → liftP ((λ { (α ,p β) → proj₁p (unliftP (~t a γ~)) α ,p proj₁p (unliftP (~t b (γ~ ,p _))) β }) ,p
                               (λ { (α ,p β) → proj₂p (unliftP (~t a γ~)) α ,p proj₂p (unliftP (~t b (γ~ ,p _))) β }))
  }

_,ℙ₁_ : {Γ : Con}{a : Tm Γ ℙ₁}{b : Tm (Γ , Elℙ₁ a) ℙ₁}(t : Tm Γ (Elℙ₁ a))(u : Tm Γ (Elℙ₁ b [ < t > ]T)) →
  Tm Γ (Elℙ₁ (Σℙ₁ a b))
t ,ℙ₁ u = record { ∣_∣t = λ γ → liftp (unliftp (∣ t ∣t γ) ,p unliftp (∣ u ∣t γ)) }

proj₁ℙ₁ : {Γ : Con}{a : Tm Γ ℙ₁}{b : Tm (Γ , Elℙ₁ a) ℙ₁} → Tm Γ (Elℙ₁ (Σℙ₁ a b)) → Tm Γ (Elℙ₁ a)
proj₁ℙ₁ w = record { ∣_∣t = λ γ → liftp (proj₁p (unliftp (∣ w ∣t γ))) }

proj₂ℙ₁ : {Γ : Con}{a : Tm Γ ℙ₁}{b : Tm (Γ , Elℙ₁ a) ℙ₁}(w : Tm Γ (Elℙ₁ (Σℙ₁ a b))) →
  Tm Γ (Elℙ₁ b [ < proj₁ℙ₁ {a = a}{b} w > ]T)
proj₂ℙ₁ w = record { ∣_∣t = λ γ → liftp (proj₂p (unliftp (∣ w ∣t γ))) }

Σℙ₁β₁ : {Γ : Con}{a : Tm Γ ℙ₁}{b : Tm (Γ , Elℙ₁ a) ℙ₁}{t : Tm Γ (Elℙ₁ a)}{u : Tm Γ (Elℙ₁ b [ < t > ]T)} →
  proj₁ℙ₁ {a = a}{b} (_,ℙ₁_ {a = a}{b} t u) ≡ t
Σℙ₁β₁ = refl

Σℙ₁β₂ : {Γ : Con}{a : Tm Γ ℙ₁}{b : Tm (Γ , Elℙ₁ a) ℙ₁}{t : Tm Γ (Elℙ₁ a)}{u : Tm Γ (Elℙ₁ b [ < t > ]T)}
  → proj₂ℙ₁ {a = a}{b} (_,ℙ₁_ {a = a}{b} t u) ≡ u
Σℙ₁β₂ = refl

Σℙ₁[] : {Γ : Con}{a : Tm Γ ℙ₁}{b : Tm (Γ , Elℙ₁ a) ℙ₁}{Θ : Con}{σ : Tms Θ Γ} →
  Σℙ₁ a b [ σ ]t ≡ Σℙ₁ (a [ σ ]t) (b [ σ ^ Elℙ₁ a ]t)
Σℙ₁[] = refl

,ℙ₁[] : {Γ : Con}{a : Tm Γ ℙ₁}{b : Tm (Γ , Elℙ₁ a) ℙ₁}{t : Tm Γ (Elℙ₁ a)}{u : Tm Γ (Elℙ₁ b [ < t > ]T)}{Θ : Con}{σ : Tms Θ Γ} →
  (_,ℙ₁_ {a = a}{b} t u) [ σ ]t ≡ _,ℙ₁_ {a = a [ σ ]t}{b = b [ σ ^ Elℙ₁ a ]t} (t [ σ ]t) (u [ σ ]t)
,ℙ₁[] = refl
