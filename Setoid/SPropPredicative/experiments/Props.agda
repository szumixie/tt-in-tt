{-# OPTIONS --without-K  #-}

-- define propositions inductive-recursively, we have to refer to Sets
-- because of ΠP

module Setoid.SPropPredicative.Props where

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core
open import Setoid.SPropPredicative.Sets

data UP : Set
⌜_⌝U  : UP → U
_~UP_ : UP → UP → Prop
refUP : (a : UP) → a ~UP a
-- symUP : {a a' : UP}(a~ : a ~UP a') → a' ~UP a
-- transUP : {a a' a'' : UP}(a~ : a ~UP a')(a~' : a' ~UP a'') → a ~UP a''

ElP : UP → Prop
_⊢_~ElP_ : {a a' : U}(a~ : a ~U a') → El a → El a' → Prop
-- refElP defined separately below
-- symElP : {a a' : UP}{a~ : a ~UP a'}{x : ElP a}{x' : ElP a'}(x~ : a~ ⊢ x ~ElP x') → symUP a~ ⊢ x' ~ElP x
-- transElP : {a a' a'' : UP}{a~ : a ~UP a'}{a~' : a' ~UP a''}{x : ElP a}{x' : ElP a'}{x'' : ElP a''}
--   (x~ : a~ ⊢ x ~ElP x')(x~' : a~' ⊢ x' ~ElP x'') → transUP a~ a~' ⊢ x ~ElP x''
-- coeElP : {a a' : UP}(a~ : a ~UP a') → ElP a → ElP a'
-- cohElP : {a a' : UP}(a~ : a ~UP a')(x : ElP a) → a~ ⊢ x ~ElP coeElP a~ x

data UP where
  ⊤P : UP
  ⊥P : UP
  ΣP : (a : UP)(b : ElP a → UP)(refb : {x x' : ElP a}(x~ : refUP a ⊢ x ~ElP x') → b x ~UP b x') → UP
  ΠP : (a : U)(b : El a → UP)(refb : {x x' : El a}(x~ : refU a ⊢ x ~ElP x') → b x ~UP b x') → UP

⌜_⌝U = ?

_~UP_ = {!!}

refUP = {!!}

ElP = {!!}

_⊢_~ElP_ = {!!}


{-
-- a universe of propositions

data _≡p_ {ℓ}{A : Set ℓ} (x : A) : A → Prop ℓ where
  refl : x ≡p x

_◾p_ : ∀{ℓ}{A : Set ℓ}{x y z : A} → x ≡p y → y ≡p z → x ≡p z
refl ◾p refl = refl

infixl 4 _◾_

_⁻¹p : ∀{ℓ}{A : Set ℓ}{x y : A} → x ≡p y → y ≡p x
refl ⁻¹p = refl

transportp : ∀{ℓ ℓ'}{A : Set ℓ}(P : A → Prop ℓ'){x y : A}(p : x ≡p y) → P x → P y
transportp P refl a = a

JP : ∀{ℓ ℓ'}{A : Set ℓ} {x : A} (P : {y : A} → x ≡p y → Prop ℓ') → P refl → {y : A} → (w : x ≡p y) → P w
JP P pr refl = pr

Props : {Γ : Con} → Ty Γ false
Props {Γ} = record
  { ∣_∣T_ = λ γ → Prop
  ; _T_⊢_~_ = λ _ a b → _≡p_ a b -- a ≡p b -- ? LiftP ((a → b) ×p (b → a))
  ; refT = λ _ → refl -- liftP ((λ x → x) ,p (λ x → x))
  ; symT = _⁻¹p  -- λ { (liftP (f ,p g)) → liftP (g ,p f) }
  ; transT = _◾p_ -- λ { (liftP (f ,p g)) (liftP (f' ,p g')) → liftP ((λ x → f' (f x)) ,p (λ y → g (g' y))) }
  ; coeT = λ _ a → a
  ; cohT = λ _ _ → refl -- liftP ((λ x → x) ,p (λ x → x))
  }

El : {Γ : Con} → Tm Γ Props → Ty Γ true
El {Γ} t = record
  { ∣_∣T_ = λ γ → Liftp (∣ t ∣t γ)
  ; _T_⊢_~_ = λ _ _ _ → LiftP ⊤p
  ; coeT = λ { γ~ (liftp α) → liftp (transportp (λ x → x) (~t t γ~) α) }  -- (proj₁p (unliftP (~t t γ~)) α)
  }

Props[] : {Γ Δ : Con}{σ : Tms Γ Δ} → Props [ σ ]T ≡ Props
Props[] = refl
El[] : {Γ Δ : Con}{σ : Tms Γ Δ}{t : Tm Δ Props} → El t [ σ ]T ≡ El (t [ σ ]t)
El[] = refl

-- closed under unit

⊤P : {Γ : Con} → Tm Γ Props
⊤P {Γ} = record { ∣_∣t = λ _ → ⊤p ; ~t = λ _ → refl } -- record { ∣_∣t = λ γ → ⊤p }

ttP : {Γ : Con} → Tm Γ (El ⊤P)
ttP {Γ} = record {}

⊤P[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ⊤P [ σ ]t ≡ ⊤P
⊤P[] = refl
ttP[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ttP [ σ ]t ≡ ttP
ttP[] = refl

-- closed under empty

⊥P : {Γ : Con} → Tm Γ Props
⊥P {Γ} = record { ∣_∣t = λ γ → ⊥p ; ~t = λ _ → refl }

abortP : {Γ : Con}{b : Bool}{A : Ty Γ b} → Tm Γ (El ⊥P) → Tm Γ A
abortP {Γ}{A} t = record
  { ∣_∣t = λ γ → abort⊥p (unliftp (∣ t ∣t γ))
  ; ~t = λ {γ} _ → abort⊥pp (unliftp (∣ t ∣t γ))
  }

⊥P[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ⊥P [ σ ]t ≡ ⊥P
⊥P[] = refl
abortP[] : {Γ Δ : Con}{σ : Tms Γ Δ}{b : Bool}{A : Ty Δ b}{t : Tm Δ (El ⊥P)} → abortP {A = A} t [ σ ]t ≡ abortP (t [ σ ]t)
abortP[] = refl

-- closed under Pi

ΠP : {Γ : Con}(A : Ty Γ true)(b : Tm (Γ ▷ A) Props) → Tm Γ Props
ΠP {Γ} A b = record
  { ∣_∣t = λ γ → (α : ∣ A ∣T γ) → ∣ b ∣t (γ ,Σ α)
    ; ~t = λ {γ}{γ'} γ~ → {!~t b (γ~ ,p _) !}
    -- liftP ((λ f α' → proj₁p (unliftP (~t b (γ~ ,p symT A (cohT A (symC Γ γ~) α')))) (f (coeT A (symC Γ γ~) α'))) ,p
    --       λ f' α → proj₂p (unliftP (~t b (γ~ ,p cohT A γ~ α))) (f' (coeT A γ~ α)))

-- J (λ {x} z → ((α : ∣ A ∣T γ) → ∣ b ∣t (γ ,Σ α)) ≡p ((α : ∣ A ∣T γ') → ∣ b ∣t (γ' ,Σ α)))
  }
{-
lam : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props} → Tm (Γ ▷ A) (El b) → Tm Γ (El (ΠP A b))
lam {Γ}{A}{b} t = record { ∣_∣t = λ γ → liftp λ α → unliftp (∣ t ∣t (γ ,Σ α)) }

app : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props} → Tm Γ (El (ΠP A b)) → Tm (Γ ▷ A) (El b)
app {Γ}{A}{b} t = record { ∣_∣t = λ { (γ ,Σ α) → liftp (unliftp (∣ t ∣t γ) α) } }

ΠPβ : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props}{t : Tm (Γ ▷ A) (El b)} → app {A = A}{b} (lam {A = A}{b} t) ≡ t
ΠPβ = refl

ΠPη : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props}{t : Tm Γ (El (ΠP A b))} → lam {A = A}{b} (app {A = A}{b} t) ≡ t
ΠPη = refl

ΠP[] : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props}{Θ : Con}{σ : Tms Θ Γ} → ΠP A b [ σ ]t ≡ ΠP (A [ σ ]T) (b [ σ ^ A ]t)
ΠP[] = refl

lam[] : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props}{t : Tm (Γ ▷ A) (El b)}{Θ : Con}{σ : Tms Θ Γ} →
  lam {A = A}{b} t [ σ ]t ≡ lam {A = A [ σ ]T}{b [ σ ^ A ]t}(t [ σ ^ A ]t)
lam[] = refl

-- closed under Sigma

ΣP : {Γ : Con}(a : Tm Γ Props)(b : Tm (Γ ▷ El a) Props) → Tm Γ Props
ΣP {Γ} a b = record
  { ∣_∣t = λ γ → Σp (∣ a ∣t γ) λ α → ∣ b ∣t (γ ,Σ liftp α)
  ; ~t = λ {γ}{γ'} γ~ → liftP ((λ { (α ,p β) → proj₁p (unliftP (~t a γ~)) α ,p proj₁p (unliftP (~t b (γ~ ,p _))) β }) ,p
                               (λ { (α ,p β) → proj₂p (unliftP (~t a γ~)) α ,p proj₂p (unliftP (~t b (γ~ ,p _))) β }))
  }

_,P_ : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}(t : Tm Γ (El a))(u : Tm Γ (El b [ < t > ]T)) →
  Tm Γ (El (ΣP a b))
t ,P u = record { ∣_∣t = λ γ → liftp (unliftp (∣ t ∣t γ) ,p unliftp (∣ u ∣t γ)) }

proj₁P : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props} → Tm Γ (El (ΣP a b)) → Tm Γ (El a)
proj₁P w = record { ∣_∣t = λ γ → liftp (proj₁p (unliftp (∣ w ∣t γ))) }

proj₂P : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}(w : Tm Γ (El (ΣP a b))) →
  Tm Γ (El b [ < proj₁P {a = a}{b} w > ]T)
proj₂P w = record { ∣_∣t = λ γ → liftp (proj₂p (unliftp (∣ w ∣t γ))) }

ΣPβ₁ : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}{t : Tm Γ (El a)}{u : Tm Γ (El b [ < t > ]T)} →
  proj₁P {a = a}{b} (_,P_ {a = a}{b} t u) ≡ t
ΣPβ₁ = refl

ΣPβ₂ : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}{t : Tm Γ (El a)}{u : Tm Γ (El b [ < t > ]T)}
  → proj₂P {a = a}{b} (_,P_ {a = a}{b} t u) ≡ u
ΣPβ₂ = refl

ΣP[] : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}{Θ : Con}{σ : Tms Θ Γ} →
  ΣP a b [ σ ]t ≡ ΣP (a [ σ ]t) (b [ σ ^ El a ]t)
ΣP[] = refl

,P[] : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}{t : Tm Γ (El a)}{u : Tm Γ (El b [ < t > ]T)}{Θ : Con}{σ : Tms Θ Γ} →
  (_,P_ {a = a}{b} t u) [ σ ]t ≡ _,P_ {a = a [ σ ]t}{b = b [ σ ^ El a ]t} (t [ σ ]t) (u [ σ ]t)
,P[] = refl

-- propositional extensionality

open import Setoid.SPropPredicative.Iden

ext : {Γ : Con}{a b : Tm Γ Props} →
  Tm (Γ ▷ El a) (El b [ wk {A = El a} ]T) → 
  Tm (Γ ▷ El b) (El a [ wk {A = El b} ]T) →
  Tm Γ (Id a b)
ext f g = record { ∣_∣t = λ γ → liftp (liftP ((λ α → unliftp (∣ f ∣t (γ ,Σ liftp α))) ,p
                                               λ β → unliftp (∣ g ∣t (γ ,Σ liftp β)))) }
-}
-}
