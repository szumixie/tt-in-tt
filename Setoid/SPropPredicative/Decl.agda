{-# OPTIONS --without-K --prop #-}

module Setoid.SPropPredicative.Decl where

open import lib
open import Agda.Primitive

l0 = lzero
l1 = lsuc l0
l2 = lsuc l1

⌜_⌝ : Bool → Level
⌜ true ⌝ = l0
⌜ false ⌝ = l1

record Con : Set₂ where
  field
    ∣_∣C : Set₁
    _C_~_ : ∣_∣C → ∣_∣C → Prop₁
    refC : ∀ γ → _C_~_ γ γ
    symC : ∀{γ γ'} → _C_~_ γ γ' → _C_~_ γ' γ
    transC : ∀{γ γ' γ''} → _C_~_ γ γ' → _C_~_ γ' γ'' → _C_~_ γ γ''
  infix 4 ∣_∣C
  infix 5 _C_~_
open Con public

record Tms (Γ Δ : Con) : Set₂ where
  field
    ∣_∣s : ∣ Γ ∣C → ∣ Δ ∣C
    ~s   : {γ γ' : ∣ Γ ∣C} → Γ C γ ~ γ' → Δ C (∣_∣s γ) ~ (∣_∣s γ')
  infix 4 ∣_∣s
open Tms public

record Ty (Γ : Con)(b : Bool) : Set (lsuc ⌜ b ⌝) where
  field
    ∣_∣T_   : ∣ Γ ∣C → Set ⌜ b ⌝
    _T_⊢_~_ : ∀{γ γ'}(p : Γ C γ ~ γ') → ∣_∣T_ γ → ∣_∣T_ γ' → Prop ⌜ b ⌝
    refT    : ∀{γ} α → _T_⊢_~_ (refC Γ γ) α α
    symT    : ∀{γ γ'}{p : Γ C γ ~ γ'}{α : ∣_∣T_ γ}{α' : ∣_∣T_ γ'}
            → _T_⊢_~_ p α α' → _T_⊢_~_ (symC Γ p) α' α
    transT  : ∀{γ γ' γ''}{p : Γ C γ ~ γ'}{q : Γ C γ' ~ γ''}
              {α : ∣_∣T_ γ}{α' : ∣_∣T_ γ'}{α'' : ∣_∣T_ γ''}
            → _T_⊢_~_ p α α' → _T_⊢_~_ q α' α'' → _T_⊢_~_ (transC Γ p q) α α''
    coeT    : {γ γ' : ∣ Γ ∣C} → Γ C γ ~ γ' → ∣_∣T_ γ → ∣_∣T_ γ'
    cohT    : {γ γ' : ∣ Γ ∣C}(p : Γ C γ ~ γ')(α : ∣_∣T_ γ) → _T_⊢_~_ p α (coeT p α)
  infix 4 ∣_∣T_
  infix 5 _T_⊢_~_
open Ty public

record Tm (Γ : Con){b : Bool}(A : Ty Γ b) : Set (lsuc ⌜ b ⌝) where
  field
    ∣_∣t : (γ : ∣ Γ ∣C) → ∣ A ∣T γ
    ~t   : {γ γ' : ∣ Γ ∣C}(p : Γ C γ ~ γ') → A T p ⊢ (∣_∣t γ) ~ (∣_∣t γ')
  infix 4 ∣_∣t
open Tm public

open import TT.Decl

d₀ : Decl {l2} {l2}
d₀ = decl Con (λ Γ → Lift (Ty Γ true)) Tms (λ {Γ (lift A) → Lift (Tm Γ A)})

d₁ : Decl {l2} {l2}
d₁ = decl Con (λ Γ → Ty Γ false) Tms λ Γ → Tm Γ {false}
