module Setoid.SPropPredicative.Translation where

open import lib
open import JM
open import Agda.Primitive
open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.Props
import TT.Decl.Congr.NoJM
import TT.Core.Laws.NoJM

module _ {i j}{d : Decl}{c : Core {i}{j} d}(b : Base c)(p : Props b) where
  open Decl d
  open Core c
  open Base b renaming (U to ℙ)
  open Props p
  open TT.Decl.Congr.NoJM d
  open TT.Core.Laws.NoJM c

  record Con' : Set (i ⊔ j) where
    field
      ∣_∣C  : Con
      _C_~_ : {Ω : Con}(ρ₀ ρ₁ : Tms Ω ∣_∣C) → Tm Ω ℙ
      C~[]  : {Ω Ω' : Con}{σ : Tms Ω' Ω}{ρ₀ ρ₁ : Tms Ω ∣_∣C}  → (_C_~_ ρ₀ ρ₁) [ σ ]t ≡[ TmΓ= U[] ]≡ _C_~_ (ρ₀ ∘ σ) (ρ₁ ∘ σ)
      refC  : {Ω : Con}(ρ : Tms Ω ∣_∣C) → Tm Ω (El (_C_~_ ρ ρ))
      symC  : {Ω : Con}{ρ₀ ρ₁ : Tms Ω ∣_∣C} → Tm Ω (El (_C_~_ ρ₀ ρ₁)) → Tm Ω (El (_C_~_ ρ₁ ρ₀))
      transC : {Ω : Con}{ρ₀ ρ₁ ρ₂ : Tms Ω ∣_∣C} → Tm Ω (El (_C_~_ ρ₀ ρ₁)) → Tm Ω (El (_C_~_ ρ₁ ρ₂)) → Tm Ω (El (_C_~_ ρ₀ ρ₂))
    infix 4 ∣_∣C
    infix 5 _C_~_
  open Con' public

  _C_~'_ : (Γ : Con'){Ω : Con}(ρ₀ ρ₁ : Tms Ω ∣ Γ ∣C) → Ty Ω
  _C_~'_ Γ ρ₀ ρ₁ = El (_C_~_ Γ ρ₀ ρ₁)

  _~C_ : {Γ : Con'}{Ω : Con}(ρ₀ ρ₁ : Tms Ω ∣ Γ ∣C) → Ty Ω
  _~C_ {Γ} ρ₀ ρ₁ = _C_~'_ Γ ρ₀ ρ₁

  record Tms' (Γ Δ : Con') : Set (i ⊔ j) where
    field
      ∣_∣s : Tms ∣ Γ ∣C ∣ Δ ∣C
      ~s   : {Ω : Con}{ρ₀ ρ₁ : Tms Ω ∣ Γ ∣C} → Tm Ω (Γ C ρ₀ ~' ρ₁) → Tm Ω (Δ C (∣_∣s ∘ ρ₀) ~' (∣_∣s ∘ ρ₁))
    infix 4 ∣_∣s
  open Tms' public

  record Ty' (Γ : Con') : Set (i ⊔ j) where
    field
      ∣_∣T    : Ty ∣ Γ ∣C
      _T_⊢_~_ : {Ω : Con}{ρ₀ ρ₁ : Tms Ω ∣ Γ ∣C} → Tm Ω (Γ C ρ₀ ~' ρ₁) → Tm Ω (∣_∣T [ ρ₀ ]T) → Tm Ω (∣_∣T [ ρ₁ ]T) → Tm Ω ℙ
      T~[]    : {Ω Ω' : Con}{σ : Tms Ω' Ω}{ρ₀ ρ₁ : Tms Ω ∣ Γ ∣C}{ρ₀₁ : Tm Ω (Γ C ρ₀ ~' ρ₁)}{t₀ : Tm Ω (∣_∣T [ ρ₀ ]T)}{t₁ : Tm Ω (∣_∣T [ ρ₁ ]T)} → (_T_⊢_~_ ρ₀₁ t₀ t₁) [ σ ]t ≡[ TmΓ= U[] ]≡ _T_⊢_~_ (coe (TmΓ= (El[] ◾ ap El (C~[] Γ))) (ρ₀₁ [ σ ]t)) (coe (TmΓ= [][]T) (t₀ [ σ ]t)) (coe (TmΓ= [][]T) (t₁ [ σ ]t))
      refT    : {Ω : Con}{ρ : Tms Ω ∣ Γ ∣C}(t : Tm Ω (∣_∣T [ ρ ]T)) → Tm Ω (El (_T_⊢_~_ (refC Γ ρ) t t))
      symT    : {Ω : Con}{ρ₀ ρ₁ : Tms Ω ∣ Γ ∣C}{ρ₀₁ : Tm Ω (Γ C ρ₀ ~' ρ₁)}{t₀ : Tm Ω (∣_∣T [ ρ₀ ]T)}{t₁ : Tm Ω (∣_∣T [ ρ₁ ]T)} → Tm Ω (El (_T_⊢_~_ ρ₀₁ t₀ t₁)) → Tm Ω (El (_T_⊢_~_ (symC Γ ρ₀₁) t₁ t₀))
      transT  : {Ω : Con}{ρ₀ ρ₁ ρ₂ : Tms Ω ∣ Γ ∣C}{ρ₀₁ : Tm Ω (Γ C ρ₀ ~' ρ₁)}{ρ₁₂ : Tm Ω (Γ C ρ₁ ~' ρ₂)}{t₀ : Tm Ω (∣_∣T [ ρ₀ ]T)}{t₁ : Tm Ω (∣_∣T [ ρ₁ ]T)}{t₂ : Tm Ω (∣_∣T [ ρ₂ ]T)} → Tm Ω (El (_T_⊢_~_ ρ₀₁ t₀ t₁)) → Tm Ω (El (_T_⊢_~_ ρ₁₂ t₁ t₂)) → Tm Ω (El (_T_⊢_~_ (transC Γ ρ₀₁ ρ₁₂) t₀ t₂))
      coeT    : {Ω : Con}{ρ₀ ρ₁ : Tms Ω ∣ Γ ∣C}(ρ₀₁ : Tm Ω (Γ C ρ₀ ~' ρ₁)) → Tm Ω (∣_∣T [ ρ₀ ]T) → Tm Ω (∣_∣T [ ρ₁ ]T)
      cohT    : {Ω : Con}{ρ₀ ρ₁ : Tms Ω ∣ Γ ∣C}(ρ₀₁ : Tm Ω (Γ C ρ₀ ~' ρ₁))(t₀ : Tm Ω (∣_∣T [ ρ₀ ]T)) → Tm Ω (El (_T_⊢_~_ ρ₀₁ t₀ (coeT ρ₀₁ t₀)))
    infix 4 ∣_∣T
    infix 5 _T_⊢_~_

    T⊢~= : {Ω : Con}{ρ₀ ρ₁ ρ₀' ρ₁' : Tms Ω ∣ Γ ∣C}{ρ₀₁ : Tm Ω (Γ C ρ₀ ~' ρ₁)}{ρ₀₁' : Tm Ω (Γ C ρ₀' ~' ρ₁')}{t₀ : Tm Ω (∣_∣T [ ρ₀ ]T)}{t₁ : Tm Ω (∣_∣T [ ρ₁ ]T)}{t₀' : Tm Ω (∣_∣T [ ρ₀' ]T)}{t₁' : Tm Ω (∣_∣T [ ρ₁' ]T)}
      → ρ₀ ≡ ρ₀' → ρ₁ ≡ ρ₁' → ρ₀₁ ≃ ρ₀₁' → t₀ ≃ t₀' → t₁ ≃ t₁'
      → (_T_⊢_~_ ρ₀₁ t₀ t₁) ≡ (_T_⊢_~_ ρ₀₁' t₀' t₁')
    T⊢~= refl refl (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl
  open Ty' public

  record Tm' (Γ : Con')(A : Ty' Γ) : Set (i ⊔ j) where
    field
      ∣_∣t : Tm ∣ Γ ∣C ∣ A ∣T
      ~t   : {Ω : Con}{ρ₀ ρ₁ : Tms Ω ∣ Γ ∣C}(ρ₀₁ : Tm Ω (Γ C ρ₀ ~' ρ₁)) → Tm Ω (El (A T ρ₀₁ ⊢ (∣_∣t [ ρ₀ ]t) ~ (∣_∣t [ ρ₁ ]t)))
    infix 4 ∣_∣t
  open Tm' public




  •' : Con'
  •' = record
         { ∣_∣C = •
         ; _C_~_ = λ ρ₀ ρ₁ → ⊤'
         ; C~[] = ⊤'[]
         ; refC = λ ρ → tt'
         ; symC = λ p → tt'
         ; transC = λ p q → tt'
         }
  _,'_ : (Γ : Con') → Ty' Γ → Con'
  _,'_ Γ A = record
    { ∣_∣C   = ∣ Γ ∣C , ∣ A ∣T
    ; _C_~_  = λ {Ω} ρ₀ ρ₁ → Σ' (Γ C π₁ ρ₀ ~ π₁ ρ₁)
                                (_T_⊢_~_ A (coe (TmΓ= (El[] ◾ ap El (C~[] Γ))) vz) (coe (TmΓ= [][]T) ((π₂ ρ₀) [ wk ]t)) (coe (TmΓ= [][]T) ((π₂ ρ₁) [ wk ]t)))
    ; C~[]   = let X = C~[] Γ in {!Σ'!} --λ ρ₀ ρ₁ σ → ⊤'[]
    ; refC   = λ ρ → (refC Γ (π₁ ρ)) ,Σ' (coe (TmΓ= (ap El (T⊢~= A (idr ⁻¹ ◾ ap (λ x → π₁ ρ ∘ x) (π₁idβ ⁻¹) ◾ ass ⁻¹) ((idr ⁻¹ ◾ ap (λ x → π₁ ρ ∘ x) (π₁idβ ⁻¹) ◾ ass ⁻¹)) {!!} {!!} {!!} ◾  T~[] A ⁻¹) ◾ El[] ⁻¹)) (refT A {_}{π₁ ρ}(π₂ ρ)))
    ; symC   = {!!} --λ { (p ,p r) → symC Γ p ,p symT A r }
    ; transC = {!!} --λ { (p ,p r)(p' ,p r') → transC Γ p p' ,p transT A r r' }
    }

  _[_]T' : ∀{Γ Δ} → Ty' Δ → Tms' Γ Δ → Ty' Γ
  A [ σ ]T' = record
    { ∣_∣T    = ∣ A ∣T [ ∣ σ ∣s ]T
    ; _T_⊢_~_ = λ {_} {ρ₀} {ρ₁} ρ₀₁ t₀ t₁ → _T_⊢_~_ A (~s σ ρ₀₁) (coe (TmΓ= [][]T) t₀) (coe (TmΓ= [][]T) t₁)
    ; T~[]    = T~[] A ◾ T⊢~= A ass ass {!!} {!!} {!!}
    ; refT    = λ t → coe (TmΓ= (ap El (ap (λ x → A T x ⊢ _ ~ _) (irr _ _)))) (refT A (coe (TmΓ= [][]T) t))
    ; symT    = λ p → coe (TmΓ= (ap El (ap (λ x → A T x ⊢ _ ~ _) (irr _ _)))) (symT A p)
    ; transT  = λ p q → coe (TmΓ= (ap El (ap (λ x → A T x ⊢ _ ~ _) (irr _ _)))) (transT A p q)
    ; coeT    = λ ρ₀₁ t₀ → coe (TmΓ= ([][]T ⁻¹)) (coeT A (~s σ ρ₀₁) (coe (TmΓ= [][]T) t₀))
    ; cohT    = λ ρ₀₁ t₀ → coe (TmΓ= (ap El (ap (λ x → A T _ ⊢ _ ~ x) (coecoe (TmΓ= ([][]T ⁻¹)) (TmΓ= [][]T) ◾ loopcoe (TmΓ= ([][]T ⁻¹) ◾ TmΓ= [][]T)) ⁻¹))) (cohT A (~s σ ρ₀₁) (coe (TmΓ= [][]T) t₀))
    }

  id' : ∀{Γ} → Tms' Γ Γ
  id' {Γ} = record
    { ∣_∣s = id
    ; ~s   = λ p → coe (TmΓ= (ap El (ap2 (_C_~_ Γ) (idl ⁻¹) (idl ⁻¹)))) p
    }

  _∘'_ : ∀{Γ Θ Δ} → Tms' Θ Δ → Tms' Γ Θ → Tms' Γ Δ
  _∘'_ {Γ} {Θ} {Δ} σ ν = record
    { ∣_∣s = ∣ σ ∣s ∘ ∣ ν ∣s
    ; ~s   = λ p → coe (TmΓ= (ap El (ap2 (_C_~_ Δ) (ass ⁻¹) (ass ⁻¹)))) (~s σ (~s ν p))
    }

  ε' : ∀{Γ} → Tms' Γ •'
  ε' {Γ} = record
    { ∣_∣s = ε
    ; ~s   = λ p → tt'
    }

  _,s'_  : ∀{Γ Δ}(σ : Tms' Γ Δ){A : Ty' Δ} → Tm' Γ (A [ σ ]T') → Tms' Γ (Δ ,' A)
  _,s'_ = ?
  π₁'    : ∀{Γ Δ}{A : Ty' Δ} → Tms' Γ (Δ ,' A) →  Tms' Γ Δ
  π₁' = ?

  _[_]t' : ∀{Γ Δ}{A : Ty' Δ} → Tm' Δ A → (σ : Tms' Γ Δ) → Tm' Γ (A [ σ ]T')
  _[_]t' = ?
  π₂'    : ∀{Γ Δ}{A : Ty' Δ}(σ : Tms' Γ (Δ ,' A)) → Tm' Γ (A [ π₁' σ ]T')
  π₂' = ?


  d' : Decl
  d' = decl Con' Ty' Tms' Tm'

  c' : Core d'
  c' = record { c1 = record
              { • = •'
              ; _,_ = _,'_
              ; _[_]T = _[_]T'
              ; id = id'
              ; _∘_ = _∘'_
              ; ε = ε'
              ; _,s_ = _,s'_
              ; π₁ = π₁'
              ; _[_]t = _[_]t'
              ; π₂ = π₂'
              } ; c2 = record
              { [id]T = {!!}
              ; [][]T = {!!}
              ; idl = {!!}
              ; idr = {!!}
              ; ass = {!!}
              ; ,∘ = {!!}
              ; π₁β = {!!}
              ; πη = {!!}
              ; εη = {!!}
              ; π₂β = {!!}
              } }
