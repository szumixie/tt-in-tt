{-# OPTIONS --without-K --prop #-}

module Setoid.SPropPredicative.Core where

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropPredicative.Decl

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

-- definitions

• : Con
• = record
  { ∣_∣C   = Lift ⊤
  ; _C_~_  = λ _ _ → LiftP ⊤p
  }

_,_ : (Γ : Con){b : Bool} → Ty Γ b → Con
_,_ Γ {true} A = record
  { ∣_∣C   = Σ ∣ Γ ∣C ∣ A ∣T_
  ; _C_~_  = λ { (γ ,Σ α)(γ' ,Σ α') → Σp (Γ C γ ~ γ') (A T_⊢ α ~ α') }
  ; refC   = λ { (γ ,Σ α) → refC Γ γ ,p refT A α }
  ; symC   = λ { (p ,p r) → symC Γ p ,p symT A r }
  ; transC = λ { (p ,p r)(p' ,p r') → transC Γ p p' ,p transT A r r' }
  }
_,_ Γ {false} A = record
  { ∣_∣C   = Σ ∣ Γ ∣C ∣ A ∣T_
  ; _C_~_  = λ { (γ ,Σ α)(γ' ,Σ α') → Σp (Γ C γ ~ γ') (A T_⊢ α ~ α') }
  ; refC   = λ { (γ ,Σ α) → refC Γ γ ,p refT A α }
  ; symC   = λ { (p ,p r) → symC Γ p ,p symT A r }
  ; transC = λ { (p ,p r)(p' ,p r') → transC Γ p p' ,p transT A r r' }
  }

_[_]T : ∀{Γ Δ b} → Ty Δ b → Tms Γ Δ → Ty Γ b
A [ σ ]T = record
  { ∣_∣T_   = λ γ → ∣ A ∣T (∣ σ ∣s γ)
  ; _T_⊢_~_ = λ p α α' → A T ~s σ p ⊢ α ~ α'
  ; refT    = refT A
  ; symT    = symT A
  ; transT  = transT A
  ; coeT    = λ p → coeT A (~s σ p)
  ; cohT    = λ p → cohT A (~s σ p)
  }

id : ∀{Γ} → Tms Γ Γ
id = record
  { ∣_∣s = λ γ → γ
  ; ~s   = λ p → p
  }

_∘_ : ∀{Γ Θ Δ} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
σ ∘ ν = record
  { ∣_∣s = λ γ → ∣ σ ∣s (∣ ν ∣s γ)
  ; ~s   = λ p → ~s σ (~s ν p)
  }

ε : ∀{Γ} → Tms Γ •
ε = record
  { ∣_∣s = λ _ → lift tt
  }

_,s_ : ∀{Γ Δ}(σ : Tms Γ Δ){b}{A : Ty Δ b} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
_,s_ σ {true}  t = record { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ ; ~s = λ p → ~s σ p ,p ~t t p }
_,s_ σ {false} t = record { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ ; ~s = λ p → ~s σ p ,p ~t t p }

π₁ : ∀{Γ Δ b}{A : Ty Δ b} → Tms Γ (Δ , A) →  Tms Γ Δ
π₁ {b = true}  σ = record { ∣_∣s = λ γ → proj₁ (∣ σ ∣s γ) ; ~s = λ p → proj₁p (~s σ p) }
π₁ {b = false} σ = record { ∣_∣s = λ γ → proj₁ (∣ σ ∣s γ) ; ~s = λ p → proj₁p (~s σ p) }

_[_]t : ∀{Γ Δ b}{A : Ty Δ b} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = record { ∣_∣t = λ γ → ∣ t ∣t (∣ σ ∣s γ) ; ~t = λ p → ~t t (~s σ p) }

π₂ : ∀{Γ Δ b}{A : Ty Δ b}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ {b = true}  σ = record { ∣_∣t = λ γ → proj₂ (∣ σ ∣s γ) ; ~t = λ p → proj₂p (~s σ p) }
π₂ {b = false} σ = record { ∣_∣t = λ γ → proj₂ (∣ σ ∣s γ) ; ~t = λ p → proj₂p (~s σ p) }

[id]T : ∀{Γ b}{A : Ty Γ b}  → A [ id ]T ≡ A
[][]T : ∀{Γ Δ Σ b}{A : Ty Σ b}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)
idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ
idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ
ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ} → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
,∘t   : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ true} {a : Tm Γ (A [ δ ]T)} → ((_,s_ δ {A = A} a) ∘ σ) ≡ (_,s_ (δ ∘ σ) {A = A} (a [ σ ]t))
,∘f   : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ false}{a : Tm Γ (A [ δ ]T)} → ((_,s_ δ {A = A} a) ∘ σ) ≡ (_,s_ (δ ∘ σ) {A = A} (a [ σ ]t))
π₁βt  : ∀{Γ Δ}{A : Ty Δ true} {δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → (π₁ {A = A}(_,s_ δ {A = A} a)) ≡ δ
π₁βf  : ∀{Γ Δ}{A : Ty Δ false}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → (π₁ {A = A}(_,s_ δ {A = A} a)) ≡ δ
πηt   : ∀{Γ Δ}{A : Ty Δ true} {δ : Tms Γ (Δ , A)} → (_,s_ (π₁ {A = A} δ) {A = A} (π₂ {A = A} δ)) ≡ δ
πηf   : ∀{Γ Δ}{A : Ty Δ false}{δ : Tms Γ (Δ , A)} → (_,s_ (π₁ {A = A} δ) {A = A} (π₂ {A = A} δ)) ≡ δ
εη    : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε
π₂βt  : ∀{Γ Δ}{A : Ty Δ true} {δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₂ {A = A}(_,s_ δ {A = A} a) ≡ a
π₂βf  : ∀{Γ Δ}{A : Ty Δ false}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₂ {A = A}(_,s_ δ {A = A} a) ≡ a

[id]T = refl
[][]T = refl
idl   = refl
idr   = refl
ass   = refl
,∘t   = refl
,∘f   = refl
π₁βt  = refl
π₁βf  = refl
πηt   = refl
πηf   = refl
εη    = refl
π₂βt  = refl
π₂βf  = refl

wk : ∀{Γ b}{A : Ty Γ b} → Tms (Γ , A) Γ
wk {A = A} = π₁ {A = A} id

vz : ∀{Γ b}{A : Ty Γ b} → Tm (Γ , A) (A [ wk {A = A} ]T)
vz {A = A} = π₂ {A = A} id

vs : ∀{Γ b c}{A : Ty Γ b}{B : Ty Γ c} → Tm Γ A → Tm (Γ , B) (A [ wk {A = B} ]T) 
vs {B = B} x = x [ wk {A = B} ]t

<_> : ∀{Γ b}{A : Ty Γ b} → Tm Γ A → Tms Γ (Γ , A)
<_> {A = A} = _,s_ id {A = A}

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ){b : Bool}(A : Ty Δ b) → Tms (Γ , A [ σ ]T) (Δ , A)
σ ^ A = _,s_ (σ ∘ wk {A = A [ σ ]T}) {A = A} (vz {A = A [ σ ]T})

infixl 5 _^_


open import TT.Core

-- c₀ : Core d₀
-- c₀ = record { c1 = record
--              { • = •
--              ; _,_ = λ {Γ (lift A) → _,_ Γ A}
--              ; _[_]T = λ { {Γ} {Δ} (lift A) σ → lift (_[_]T {Γ}{Δ}A σ)}
--              ; id = id
--              ; _∘_ = _∘_
--              ; ε = ε
--              ; _,s_ = λ { {Γ} {Δ} σ {lift A} → _,s_ {Γ}{Δ}σ{true}{A}}
--              ; π₁ = λ {Γ}{Δ}{A} → π₁ {Γ}{Δ}{true}{A}
--              ; _[_]t = λ {Γ}{Δ}{A} → _[_]t {Γ}{Δ}{true}{A}
--              ; π₂ = λ {Γ}{Δ}{A} → π₂ {Γ}{Δ}{true}{A}
--              } ; c2 = record
--              { [id]T = λ {Γ}{A} → [id]T {Γ}{true}{A}
--              ; [][]T = λ {Γ}{Δ}{Σ}{A}{σ}{δ} → [][]T {Γ}{Δ}{Σ}{true}{A}{σ}{δ}
--              ; idl = idl
--              ; idr = idr
--              ; ass = λ {Γ}{Δ}{Σ}{A}{σ}{δ}{v} → ass {Γ}{Δ}{Σ}{A}{σ}{δ}{v}
--              ; ,∘ = λ {Γ}{Δ}{Σ}{σ}{δ}{A}{a} → ,∘f {Γ}{Δ}{Σ}{σ}{δ}{A}{a}
--              ; π₁β = λ {Γ}{Δ}{A}{δ}{a} → π₁βf {Γ}{Δ}{A}{δ}{a}
--              ; πη = λ {Γ}{Δ}{A}{δ} → πηf {Γ}{Δ}{A}{δ}
--              ; εη = εη
--              ; π₂β = λ {Γ}{Δ}{A}{δ}{a} → π₂βf {Γ}{Δ}{A}{δ}{a}
--              } }

c₁ : Core d₁
c₁ = record { c1 = record
             { • = •
             ; _,_ = λ Γ → _,_ Γ {false}
             ; _[_]T = λ {Γ} {Δ} → _[_]T {Γ}{Δ}{false}
             ; id = id
             ; _∘_ = _∘_
             ; ε = ε
             ; _,s_ = λ {Γ} {Δ} σ {A} → _,s_ {Γ}{Δ}σ{false}{A}
             ; π₁ = λ {Γ}{Δ}{A} → π₁ {Γ}{Δ}{false}{A}
             ; _[_]t = λ {Γ}{Δ}{A} → _[_]t {Γ}{Δ}{false}{A}
             ; π₂ = λ {Γ}{Δ}{A} → π₂ {Γ}{Δ}{false}{A}
             } ; c2 = record
             { [id]T = λ {Γ}{A} → [id]T {Γ}{false}{A}
             ; [][]T = λ {Γ}{Δ}{Σ}{A}{σ}{δ} → [][]T {Γ}{Δ}{Σ}{false}{A}{σ}{δ}
             ; idl = idl
             ; idr = idr
             ; ass = λ {Γ}{Δ}{Σ}{A}{σ}{δ}{v} → ass {Γ}{Δ}{Σ}{A}{σ}{δ}{v}
             ; ,∘ = λ {Γ}{Δ}{Σ}{σ}{δ}{A}{a} → ,∘f {Γ}{Δ}{Σ}{σ}{δ}{A}{a}
             ; π₁β = λ {Γ}{Δ}{A}{δ}{a} → π₁βf {Γ}{Δ}{A}{δ}{a}
             ; πη = λ {Γ}{Δ}{A}{δ} → πηf {Γ}{Δ}{A}{δ}
             ; εη = εη
             ; π₂β = λ {Γ}{Δ}{A}{δ}{a} → π₂βf {Γ}{Δ}{A}{δ}{a}
             } }


TmΓ= : {Γ : Con}{b : Bool}{A₀ : Ty Γ b}{A₁ : Ty Γ b}(A₂ : A₀ ≡ A₁)
     → Tm Γ A₀ ≡ Tm Γ A₁
TmΓ= refl = refl


wk' : ∀{Γ b}(A : Ty Γ b) → Tms (Γ , A) Γ
wk' A = wk {A = A}

wkT : ∀{Γ b b'}(A : Ty Γ b) → Ty Γ b' → Ty (Γ , A) b'
wkT A B = B [ wk' A ]T

vz' : ∀{Γ b}(A : Ty Γ b) → Tm (Γ , A) (wkT A A)
vz' A = vz {A = A}

vs' : ∀{Γ b c}(A : Ty Γ b)(B : Ty Γ c) → Tm Γ A → Tm (Γ , B) (wkT B A)
vs' A B x = vs {A = A}{B} x

<_∣_> : ∀{Γ b}(A : Ty Γ b) → Tm Γ A → Tms Γ (Γ , A)
<_∣_> A t = <_> {A = A} t
