{-# OPTIONS --without-K  #-}

module Setoid.SPropPredicative.Trunc where

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core

Tr : {Γ : Con}{b : Bool} → Ty Γ b → Ty Γ b
Tr {Γ}{b} A = record
  { ∣_∣T_ = ∣ A ∣T_
  ; _T_⊢_~_ = λ _ _ _ → LiftP ⊤p
  ; coeT = coeT A
  }

embed : {Γ : Con}{b : Bool}{A : Ty Γ b} → Tm Γ A → Tm Γ (Tr A)
embed t = record { ∣_∣t = ∣ t ∣t }

open import Setoid.SPropPredicative.Iden

treq : {Γ : Con}{b : Bool}{A : Ty Γ b}(t u : Tm Γ A) → Tm Γ (Id (embed t) (embed u))
treq t u = record {}

recTrtt : {Γ : Con}{A : Ty Γ true}(C : Ty Γ true)
  (membed : Tm (Γ , A) (C [ wk {A = A} ]T))
  (mtreq  : Tm (Γ , C , C [ wk {A = C} ]T)
               (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T})))
  (u : Tm Γ (Tr A)) →
  Tm Γ C
recTrtt {Γ}{A} C membed mtreq u = record
  { ∣_∣t = λ γ → ∣ membed ∣t (γ ,Σ ∣ u ∣t γ)
  ; ~t = λ {γ}{γ'} γ~ → transT C (unliftp (∣ mtreq ∣t (γ ,Σ ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ,Σ coeT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ')))))
                                 (symT C (cohT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ'))))
  }

recTrtf : {Γ : Con}{A : Ty Γ true} (C : Ty Γ false)(membed : Tm (Γ , A) (C [ wk {A = A} ]T))(mtreq : Tm (Γ , C , C [ wk {A = C} ]T) (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))) (u : Tm Γ (Tr A)) → Tm Γ C
recTrft : {Γ : Con}{A : Ty Γ false}(C : Ty Γ true) (membed : Tm (Γ , A) (C [ wk {A = A} ]T))(mtreq : Tm (Γ , C , C [ wk {A = C} ]T) (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))) (u : Tm Γ (Tr A)) → Tm Γ C
recTrff : {Γ : Con}{A : Ty Γ false}(C : Ty Γ false)(membed : Tm (Γ , A) (C [ wk {A = A} ]T))(mtreq : Tm (Γ , C , C [ wk {A = C} ]T) (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))) (u : Tm Γ (Tr A)) → Tm Γ C
recTrtf {Γ}{A} C membed mtreq u = record { ∣_∣t = λ γ → ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ; ~t = λ {γ}{γ'} γ~ → transT C (unliftp (∣ mtreq ∣t (γ ,Σ ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ,Σ coeT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ'))))) (symT C (cohT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ')))) }
recTrft {Γ}{A} C membed mtreq u = record { ∣_∣t = λ γ → ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ; ~t = λ {γ}{γ'} γ~ → transT C (unliftp (∣ mtreq ∣t (γ ,Σ ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ,Σ coeT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ'))))) (symT C (cohT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ')))) }
recTrff {Γ}{A} C membed mtreq u = record { ∣_∣t = λ γ → ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ; ~t = λ {γ}{γ'} γ~ → transT C (unliftp (∣ mtreq ∣t (γ ,Σ ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ,Σ coeT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ'))))) (symT C (cohT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ')))) }

recTrttβ : {Γ : Con}{A : Ty Γ true}{C : Ty Γ true}
  {membed : Tm (Γ , A) (C [ wk {A = A} ]T)}
  {mtreq  : Tm (Γ , C , C [ wk {A = C} ]T)
               (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))}
  {t : Tm Γ A} →
  recTrtt {Γ}{A} C membed mtreq (embed t) ≡ membed [ < t > ]t
recTrttβ = refl

recTrtfβ : {Γ : Con}{A : Ty Γ true} {C : Ty Γ false}{membed : Tm (Γ , A) (C [ wk {A = A} ]T)}{mtreq : Tm (Γ , C , C [ wk {A = C} ]T)(Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))} {t : Tm Γ A} → recTrtf {Γ}{A} C membed mtreq (embed t) ≡ membed [ < t > ]t
recTrftβ : {Γ : Con}{A : Ty Γ false}{C : Ty Γ true} {membed : Tm (Γ , A) (C [ wk {A = A} ]T)}{mtreq : Tm (Γ , C , C [ wk {A = C} ]T)(Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))} {t : Tm Γ A} → recTrft {Γ}{A} C membed mtreq (embed t) ≡ membed [ < t > ]t
recTrffβ : {Γ : Con}{A : Ty Γ false}{C : Ty Γ false}{membed : Tm (Γ , A) (C [ wk {A = A} ]T)}{mtreq : Tm (Γ , C , C [ wk {A = C} ]T)(Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))} {t : Tm Γ A} → recTrff {Γ}{A} C membed mtreq (embed t) ≡ membed [ < t > ]t
recTrtfβ = refl
recTrftβ = refl
recTrffβ = refl

-- substitution laws

Tr[] : {Γ : Con}{b : Bool}{A : Ty Γ b}{Θ : Con}{σ : Tms Θ Γ} → Tr A [ σ ]T ≡ Tr (A [ σ ]T)
Tr[] = refl

embed[] : {Γ : Con}{b : Bool}{A : Ty Γ b}{t : Tm Γ A}{Θ : Con}{σ : Tms Θ Γ} →
  embed t [ σ ]t ≡ embed (t [ σ ]t)
embed[] = refl

treq[] : {Γ : Con}{b : Bool}{A : Ty Γ b}{t u : Tm Γ A}{Θ : Con}{σ : Tms Θ Γ} →
  treq t u [ σ ]t ≡ treq (t [ σ ]t) (u [ σ ]t)
treq[] = refl

recTrtt[] : {Γ : Con}{A : Ty Γ true}{C : Ty Γ true}{membed : Tm (Γ , A) (C [ wk {A = A} ]T)}{mtreq : Tm (Γ , C , C [ wk {A = C} ]T) (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))}{u : Tm Γ (Tr A)}{Θ : Con}{σ : Tms Θ Γ} →
  recTrtt {Γ}{A} C membed mtreq u [ σ ]t ≡ recTrtt {Θ}{A [ σ ]T}(C [ σ ]T) (membed [ σ ^ A ]t) (mtreq [ σ ^ C ^ C [ wk {A = C} ]T ]t) (u [ σ ]t)
recTrtt[] = refl
