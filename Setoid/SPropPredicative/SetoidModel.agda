module Setoid.SPropPredicative.SetoidModel where

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core
open import Setoid.SPropPredicative.Iden
open import Setoid.SPropPredicative.Props0
open import Setoid.SPropPredicative.Sets0
open import Setoid.SPropPredicative.ComputId
open import Setoid.SPropPredicative.Quotient
