{-# OPTIONS --without-K --prop #-}

module Setoid.SProp.Core {ℓ} where

open import lib
open import Setoid.SProp.lib

open import Setoid.SProp.Decl {ℓ}

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t

-- definitions

• : Con
• = record
  { ∣_∣C   = Lift ⊤
  ; _C_~_  = λ _ _ → LiftP ⊤p
  }

_▷_ : (Γ : Con) → Ty Γ → Con
Γ ▷ A = record
  { ∣_∣C   = Σ ∣ Γ ∣C ∣ A ∣T_
  ; _C_~_  = λ { (γ ,Σ α)(γ' ,Σ α') → Σp (Γ C γ ~ γ') (A T_⊢ α ~ α') }
  ; refC   = λ { (γ ,Σ α) → refC Γ γ ,p refT A α }
  ; symC   = λ { (p ,p r) → symC Γ p ,p symT A r }
  ; transC = λ { (p ,p r)(p' ,p r') → transC Γ p p' ,p transT A r r' }
  }

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
A [ σ ]T = record
  { ∣_∣T_   = λ γ → ∣ A ∣T (∣ σ ∣s γ)
  ; _T_⊢_~_ = λ p α α' → A T ~s σ p ⊢ α ~ α'
  ; refT    = refT A
  ; symT    = symT A
  ; transT  = transT A
  ; coeT    = λ p → coeT A (~s σ p)
  ; cohT    = λ p → cohT A (~s σ p)
  }

id : ∀{Γ} → Tms Γ Γ
id = record
  { ∣_∣s = λ γ → γ
  ; ~s   = λ p → p
  }

_∘_ : ∀{Γ Θ Δ} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
σ ∘ ν = record
  { ∣_∣s = λ γ → ∣ σ ∣s (∣ ν ∣s γ)
  ; ~s   = λ p → ~s σ (~s ν p)
  }

ε : ∀{Γ} → Tms Γ •
ε = record
  { ∣_∣s = λ _ → lift tt
  }

_,_ : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
σ , t = record
  { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ
  ; ~s   = λ p → ~s σ p ,p ~t t p
  }

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▷ A) →  Tms Γ Δ
π₁ σ = record
  { ∣_∣s = λ γ → proj₁ (∣ σ ∣s γ)
  ; ~s   = λ p → proj₁p (~s σ p)
  }

_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = record
  { ∣_∣t = λ γ → ∣ t ∣t (∣ σ ∣s γ)
  ; ~t   = λ p → ~t t (~s σ p)
  }

π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ σ = record
  { ∣_∣t = λ γ → proj₂ (∣ σ ∣s γ)
  ; ~t   = λ p → proj₂p (~s σ p)
  }

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)
idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ
idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ
ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ} → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)} → ((_,_ δ {A} a) ∘ σ) ≡ (_,_ (δ ∘ σ) {A} (a [ σ ]t))
π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → (π₁ {A = A}(_,_ δ {A} a)) ≡ δ
πη    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ ▷ A)} → (_,_ (π₁ {A = A} δ) {A} (π₂ {A = A} δ)) ≡ δ
εη    : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε
π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₂ {A = A}(_,_ δ {A} a) ≡ a

[id]T = refl
[][]T = refl
idl   = refl
idr   = refl
ass   = refl
,∘    = refl
π₁β   = refl
πη    = refl
εη    = refl
π₂β   = refl

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▷ A) Γ
wk {A = A} = π₁ {A = A} id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▷ A) (A [ wk {A = A} ]T)
vz {A = A} = π₂ {A = A} id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ ▷ B) (A [ wk {A = B} ]T) 
vs {B = B} x = x [ wk {A = B} ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▷ A)
<_> {A = A} = _,_ id {A}

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▷ A [ σ ]T) (Δ ▷ A)
σ ^ A = _,_ (σ ∘ wk {A = A [ σ ]T}) {A} (vz {A = A [ σ ]T})

infixl 5 _^_

open import TT.Core

c : Core d
c = record { c1 = record
  { • = •
  ; _,_ = _▷_
  ; _[_]T = _[_]T
  ; id = id
  ; _∘_ = _∘_
  ; ε = ε
  ; _,s_ = _,_
  ; π₁ = λ {Γ}{Δ}{A} → π₁ {Γ}{Δ}{A}
  ; _[_]t = _[_]t
  ; π₂ = λ {Γ}{Δ}{A} → π₂ {Γ}{Δ}{A}
  } ; c2 = record
  { [id]T = [id]T
  ; [][]T = λ {Γ}{Δ}{Σ}{A}{σ}{δ} → [][]T {Γ}{Δ}{Σ}{A}{σ}{δ}
  ; idl = idl
  ; idr = idr
  ; ass = λ {Γ}{Δ}{Σ}{Ω}{σ}{δ}{ν} → ass {Γ}{Δ}{Σ}{Ω}{σ}{δ}{ν}
  ; ,∘ = λ {Γ}{Δ}{Σ}{δ}{σ}{A}{a} → ,∘ {Γ}{Δ}{Σ}{δ}{σ}{A}{a}
  ; π₁β = λ {Γ}{Δ}{A}{δ}{a} → π₁β {Γ}{Δ}{A}{δ}{a}
  ; πη = λ {Γ}{Δ}{A}{δ} → πη {Γ}{Δ}{A}{δ}
  ; εη = εη
  ; π₂β = λ {Γ}{Δ}{A}{δ}{a} → π₂β {Γ}{Δ}{A}{δ}{a}
  } }
