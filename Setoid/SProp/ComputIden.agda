{-# OPTIONS --without-K --prop --type-in-type #-}

module Setoid.SProp.ComputIden {ℓ} where

open import lib
open import Setoid.SProp.lib

open import Setoid.SProp.Decl {ℓ}
open import Setoid.SProp.Core {ℓ}
open import Setoid.SProp.Props

_~C : (Γ : Con){Ω : Con}(ρ₀ ρ₁ : Tms Ω Γ) → Tm Ω Props
(Γ ~C) ρ₀ ρ₁ = record {
  ∣_∣t = λ γ → Γ C ∣ ρ₀ ∣s γ ~ ∣ ρ₁ ∣s γ ;
  ~t = λ γ~ → liftP ((λ ρ₀₁ → transC3 Γ (symC Γ (~s ρ₀ γ~)) ρ₀₁ (~s ρ₁ γ~)) ,p
                      λ ρ₀₁ → transC3 Γ (~s ρ₀ γ~) ρ₀₁ (symC Γ (~s ρ₁ γ~))) }

RC : (Γ : Con){Ω : Con}(ρ : Tms Ω Γ) → Tm Ω (El ((Γ ~C) ρ ρ))
RC Γ ρ = record {
  ∣_∣t = λ γ → lift (liftp (refC Γ _)) ;
  ~t = _ }

SC : (Γ : Con){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁))) → Tm Ω (El ((Γ ~C) ρ₁ ρ₀))
SC Γ ρ₀₁ = record {
  ∣_∣t = λ γ → lift (liftp (symC Γ (unliftp (unlift (∣ ρ₀₁ ∣t γ))))) ;
  ~t = _ }

TC : (Γ : Con){Ω : Con}{ρ₀ ρ₁ ρ₂ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(ρ₁₂ : Tm Ω (El ((Γ ~C) ρ₁ ρ₂))) →
  Tm Ω (El ((Γ ~C) ρ₀ ρ₂))
TC Γ ρ₀₁ ρ₁₂ = record {
  ∣_∣t = λ γ → lift (liftp (transC Γ (unliftp (unlift (∣ ρ₀₁ ∣t γ))) (unliftp (unlift (∣ ρ₁₂ ∣t γ))))) ;
  ~t = _ }

_~T : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))
  (t₀ : Tm Ω (A [ ρ₀ ]T))(t₁ : Tm Ω (A [ ρ₁ ]T)) → Tm Ω Props
(A ~T) ρ₀₁ t₀ t₁ = record {
  ∣_∣t = λ γ → A T unliftp (unlift (∣ ρ₀₁ ∣t γ)) ⊢ ∣ t₀ ∣t γ ~ ∣ t₁ ∣t γ ;
  ~t = λ γ~ → liftP ((λ t₀₁ → transT3 A (symT A (~t t₀ γ~)) t₀₁ (~t t₁ γ~)) ,p
                     (λ t₀₁ → transT3 A (~t t₀ γ~) t₀₁ (symT A (~t t₁ γ~)))) }
  
{-
RT : {Γ : Con}(A : Ty Γ){Ω : Con}(ρ : Tms Ω Γ)(t : Tm Ω (A [ ρ ]T)) → Tm Ω (El ((A ~T) (RC Γ ρ) t t))
ST : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))
  {t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}(t₀₁ : Tm Ω (El ((A ~T) ρ₀₁ t₀ t₁))) →
  Tm Ω (El ((A ~T) (SC Γ ρ₀₁) t₁ t₀))
TT : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ ρ₂ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(ρ₁₂ : Tm Ω (El ((Γ ~C) ρ₁ ρ₂)))
  {t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}{t₂ : Tm Ω (A [ ρ₂ ]T)}
  (t₀₁ : Tm Ω (El ((A ~T) ρ₀₁ t₀ t₁)))(t₁₂ : Tm Ω (El ((A ~T) ρ₁₂ t₁ t₂))) →
  Tm Ω (El ((A ~T) (TC Γ ρ₀₁ ρ₁₂) t₀ t₂))
coeT : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(t₀ : Tm Ω (A [ ρ₀ ]T)) →
  Tm Ω (A [ ρ₁ ]T)
cohT : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(t₀ : Tm Ω (A [ ρ₀ ]T)) →
  Tm Ω (El ((A ~T) ρ₀₁ t₀ (coeT A ρ₀₁ t₀)))

_~s : {Γ Δ : Con}(δ : Tms Γ Δ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁))) →
  Tm Ω (El ((Δ ~C) (δ ∘ ρ₀) (δ ∘ ρ₁)))

_~t : {Γ : Con}{A : Ty Γ}(t : Tm Γ A){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁))) →
  Tm Ω (El ((A ~T) ρ₀₁ (t [ ρ₀ ]t) (t [ ρ₁ ]t)))

_~[]C : (Γ : Con){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{Ψ : Con}{ν : Tms Ψ Ω} →
  ((Γ ~C) ρ₀ ρ₁) [ ν ]t ≡[ TmΓ= U[] ]≡ (Γ ~C) (ρ₀ ∘ ν) (ρ₁ ∘ ν)
_~[]T : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}{Ψ : Con}{ν : Tms Ψ Ω} →
  ((A ~T) ρ₀₁ t₀ t₁) [ ν ]t ≡[ TmΓ= U[] ]≡
  (A ~T) {ρ₀ = ρ₀ ∘ ν} {ρ₁ = ρ₁ ∘ ν} (coe (TmΓ= (El[] ◾ ap El (Γ ~[]C))) (ρ₀₁ [ ν ]t)) (coe (TmΓ= [][]T) (t₀ [ ν ]t)) (coe (TmΓ= [][]T) (t₁ [ ν ]t))
coe[]T : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(t₀ : Tm Ω (A [ ρ₀ ]T)){Ψ : Con}{ν : Tms Ψ Ω} →
  (coeT A ρ₀₁ t₀) [ ν ]t ≡[ TmΓ= [][]T ]≡
  coeT A {ρ₀ = ρ₀ ∘ ν} {ρ₁ = ρ₁ ∘ ν} (coe (TmΓ= (El[] ◾ ap El (Γ ~[]C))) (ρ₀₁ [ ν ]t)) (coe (TmΓ= [][]T) (t₀ [ ν ]t))

•~ : {Ω : Con} → (• ~C) ε ε ≡ ⊤' {Ω}
,~ : {Γ : Con}{A : Ty Γ}{Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)} →
  ((Γ , A) ~C) (ρ₀ ,s t₀) (ρ₁ ,s t₁) ≡
  Σ' ((Γ ~C) ρ₀ ρ₁) ((A ~T) (coe (TmΓ= (El[] ◾ ap El (Γ ~[]C))) vz) (coe (TmΓ= [][]T) (t₀ [ wk ]t)) (coe (TmΓ= [][]T) (t₁ [ wk ]t)))
  
[]T~ : {Δ : Con}{A : Ty Δ}{Γ : Con}{δ : Tms Γ Δ}{Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))
  (t₀ : Tm Ω (A [ δ ]T [ ρ₀ ]T))(t₁ : Tm Ω (A [ δ ]T [ ρ₁ ]T)) →
  ((A [ δ ]T) ~T) ρ₀₁ t₀ t₁ ≡ (A ~T) ((δ ~s) ρ₀₁) (coe (TmΓ= [][]T) t₀) (coe (TmΓ= [][]T) t₁)
coeT[]T : {Δ : Con}{A : Ty Δ}{Γ : Con}{δ : Tms Γ Δ}{Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))
  (t₀ : Tm Ω (A [ δ ]T [ ρ₀ ]T)) →
  coeT (A [ δ ]T) ρ₀₁ t₀ ≡[ TmΓ= [][]T ]≡ coeT A ((δ ~s) ρ₀₁) (coe (TmΓ= [][]T) t₀)

U~ : {Γ : Con}{Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(a₀ : Tm Ω (U [ ρ₀ ]T))(a₁ : Tm Ω (U [ ρ₁ ]T)) →
  (U ~T) ρ₀₁ a₀ a₁ ≡ (El (coe (TmΓ= U[]) a₀) ⇒ coe (TmΓ= U[]) a₁) ×' (El (coe (TmΓ= U[]) a₁) ⇒ coe (TmΓ= U[]) a₀)
coeTU : {Γ : Con}{Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(a₀ : Tm Ω (U [ ρ₀ ]T)) →
  coeT U ρ₀₁ a₀ ≡ coe (TmΓ= (U[] ◾ U[] ⁻¹)) a₀

El~ : {Γ : Con}(a : Tm Γ U){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))
  (t₀ : Tm Ω (El a [ ρ₀ ]T))(t₁ : Tm Ω (El a [ ρ₁ ]T)) →
  ((El a) ~T) ρ₀₁ t₀ t₁ ≡ ⊤'
-}
