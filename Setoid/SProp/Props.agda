{-# OPTIONS --without-K --prop --type-in-type #-}

-- we only need type-in-type to do Π and because Props should be a
-- large type. this should work in the predicative version

module Setoid.SProp.Props where

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SProp.Decl {lsuc lzero}
open import Setoid.SProp.Core {lsuc lzero}

-- a universe of propositions

-- TODO: substitution laws

Props : {Γ : Con} → Ty Γ
Props {Γ} = record
  { ∣_∣T_ = λ γ → Prop
  ; _T_⊢_~_ = λ _ a b → LiftP ((a → b) ×p (b → a))
  ; refT = λ _ → liftP ((λ x → x) ,p (λ x → x))
  ; symT = λ { (liftP (f ,p g)) → liftP (g ,p f) }
  ; transT = λ { (liftP (f ,p g)) (liftP (f' ,p g')) → liftP ((λ x → f' (f x)) ,p (λ y → g (g' y))) }
  ; coeT = λ _ a → a
  ; cohT = λ _ _ → liftP ((λ x → x) ,p (λ x → x))
  }

El : {Γ : Con} → Tm Γ Props → Ty Γ
El {Γ} t = record
  { ∣_∣T_ = λ γ → Lift (Liftp (∣ t ∣t γ))
  ; _T_⊢_~_ = λ _ _ _ → LiftP ⊤p
  ; coeT = λ { γ~ (lift (liftp α)) → lift (liftp (proj₁p (unliftP (~t t γ~)) α)) }
  }

-- closed under unit

⊤P : {Γ : Con} → Tm Γ Props
⊤P {Γ} = record { ∣_∣t = λ γ → ⊤p }

ttP : {Γ : Con} → Tm Γ (El ⊤P)
ttP {Γ} = record {}

-- closed under empty

⊥P : {Γ : Con} → Tm Γ Props
⊥P {Γ} = record { ∣_∣t = λ γ → ⊥p ; ~t = λ _ → liftP ((λ x → x) ,p (λ x → x)) }

abortP : {Γ : Con}{A : Ty Γ} → Tm Γ (El ⊥P) → Tm Γ A
abortP {Γ}{A} t = record
  { ∣_∣t = λ γ → abort⊥p (unliftp (unlift (∣ t ∣t γ)))
  ; ~t = λ {γ} _ → abort⊥pp (unliftp (unlift (∣ t ∣t γ)))
  }

-- closed under Pi

ΠP : {Γ : Con}(A : Ty Γ)(b : Tm (Γ ▷ A) Props) → Tm Γ Props
ΠP {Γ} A b = record
  { ∣_∣t = λ γ → (α : ∣ A ∣T γ) → ∣ b ∣t (γ ,Σ α)
  ; ~t = λ γ~ → liftP ((
    λ f α' → proj₂p (unliftP (~t b (symC Γ γ~ ,p cohT A (symC Γ γ~) α'))) (f (coeT A (symC Γ γ~) α'))) ,p
    λ f' α → proj₂p (unliftP (~t b (γ~ ,p cohT A γ~ α))) (f' (coeT A γ~ α)))
  }

lamP : {Γ : Con}{A : Ty Γ}{b : Tm (Γ ▷ A) Props}(t : Tm (Γ ▷ A) (El b)) → Tm Γ (El (ΠP A b))
lamP {Γ}{A}{b} t = record
  { ∣_∣t = λ γ → lift (liftp λ α → unliftp (unlift (∣ t ∣t (γ ,Σ α))))
  ; ~t = λ _ → liftP ttp
  }

appP : {Γ : Con}{A : Ty Γ}{b : Tm (Γ ▷ A) Props}(t : Tm Γ (El (ΠP A b))) → Tm (Γ ▷ A) (El b)
appP {Γ}{A}{b} t = record
  { ∣_∣t = λ { (γ ,Σ α) → lift (liftp (unliftp (unlift (∣ t ∣t γ)) α)) }
  ; ~t = λ _ → liftP ttp
  }

Πβ : {Γ : Con}{A : Ty Γ}{b : Tm (Γ ▷ A) Props}{t : Tm (Γ ▷ A) (El b)} → appP {A = A}{b}(lamP {A = A}{b} t) ≡ t
Πβ = refl

Πη : {Γ : Con}{A : Ty Γ}{b : Tm (Γ ▷ A) Props}{t : Tm Γ (El (ΠP A b))} → lamP {A = A}{b}(appP {A = A}{b} t) ≡ t
Πη = refl

-- closed under Sigma

ΣP : {Γ : Con}(a : Tm Γ Props)(b : Tm (Γ ▷ El a) Props) → Tm Γ Props
ΣP {Γ} a b = record
  { ∣_∣t = λ γ → Σp (∣ a ∣t γ) λ α → ∣ b ∣t (γ ,Σ lift (liftp α))
  ; ~t = λ {γ}{γ'} γ~ → liftP ((λ { (α ,p β) → proj₁p (unliftP (~t a γ~)) α ,p proj₁p (unliftP (~t b (γ~ ,p _))) β }) ,p
                               (λ { (α ,p β) → proj₂p (unliftP (~t a γ~)) α ,p proj₂p (unliftP (~t b (γ~ ,p _))) β }))
  }

_,P_ : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}(t : Tm Γ (El a))(u : Tm Γ (El b [ < t > ]T)) →
  Tm Γ (El (ΣP a b))
t ,P u = record { ∣_∣t = λ γ → lift (liftp (unliftp (unlift (∣ t ∣t γ)) ,p unliftp (unlift (∣ u ∣t γ)))) }

proj₁P : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props} → Tm Γ (El (ΣP a b)) → Tm Γ (El a)
proj₁P w = record { ∣_∣t = λ γ → lift (liftp (proj₁p (unliftp (unlift (∣ w ∣t γ))))) }

proj₂P : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}(w : Tm Γ (El (ΣP a b))) →
  Tm Γ (El b [ < proj₁P {a = a}{b} w > ]T)
proj₂P w = record { ∣_∣t = λ γ → lift (liftp (proj₂p (unliftp (unlift (∣ w ∣t γ))))) }

ΣPβ₁ : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}{t : Tm Γ (El a)}{u : Tm Γ (El b [ < t > ]T)}
  → proj₁P {a = a}{b} (_,P_ {a = a}{b} t u) ≡ t
ΣPβ₁ = refl

ΣPβ₂ : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}{t : Tm Γ (El a)}{u : Tm Γ (El b [ < t > ]T)}
  → proj₂P {a = a}{b} (_,P_ {a = a}{b} t u) ≡ u
ΣPβ₂ = refl

-- propositional extensionality

open import Setoid.SProp.Iden {lsuc lzero}

ext : {Γ : Con}{a b : Tm Γ Props} →
  Tm (Γ ▷ El a) (El b [ wk {A = El a} ]T) → 
  Tm (Γ ▷ El b) (El a [ wk {A = El b} ]T) →
  Tm Γ (Id a b)
ext {Γ}{a}{b} f g = record { ∣_∣t = λ γ → liftp (liftP ((λ α → unliftp (unlift (∣ f ∣t (γ ,Σ lift (liftp α))))) ,p
                                                         λ β → unliftp (unlift (∣ g ∣t (γ ,Σ lift (liftp β)))))) }
