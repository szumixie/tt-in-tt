{-# OPTIONS --without-K --prop #-}

module Setoid.SProp.Iden {ℓ} where

open import lib
open import Setoid.SProp.lib

open import Setoid.SProp.Decl {ℓ}
open import Setoid.SProp.Core {ℓ}

Id : {Γ : Con}{A : Ty Γ} → Tm Γ A → Tm Γ A → Ty Γ
Id {Γ}{A} t u = record
  { ∣_∣T_ = λ γ → Liftp (A T refC Γ γ ⊢ ∣ t ∣t γ ~ ∣ u ∣t γ)
  ; _T_⊢_~_ = λ _ _ _ → LiftP ⊤p
  ; coeT = λ { {γ}{γ'} γ~ (liftp α~) → liftp (transT A (symT A (~t t γ~)) (transT A
                                                       α~
                                                       (~t u γ~))) }
  }

Id[] : ∀{Γ Θ A}{u v : Tm Γ A}{σ : Tms Θ Γ}
     → Id u v [ σ ]T ≡ Id (u [ σ ]t) (v [ σ ]t)
Id[] = refl

ref : ∀{Γ A}(u : Tm Γ A) → Tm Γ (Id u u)
ref {Γ}{A} u = record { ∣_∣t = λ γ → liftp (~t u (refC Γ γ)) }

ref[] : ∀{Γ Θ A}{u : Tm Γ A}{σ : Tms Θ Γ}
      → ref u [ σ ]t ≡ ref (u [ σ ]t)
ref[] = refl

transp : ∀{Γ}{A : Ty Γ}(P : Ty (Γ ▷ A)){t u : Tm Γ A}(e : Tm Γ (Id t u)) →
  Tm Γ (P [ < t > ]T) → Tm Γ (P [ < u > ]T)
transp {Γ}{A} P {t}{u} e w = record
  { ∣_∣t = λ γ → coeT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ )
  ; ~t = λ {γ}{γ'} γ~ → transT P (symT P (cohT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ))) (transT P
                                 (~t w γ~)
                                 (cohT P (refC Γ γ' ,p unliftp (∣ e ∣t γ')) (∣ w ∣t γ')))
  }

transp[] : ∀{Γ}{A : Ty Γ}{P : Ty (Γ ▷ A)}{t u : Tm Γ A}{e : Tm Γ (Id t u)}
  {w : Tm Γ (P [ < t > ]T)}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = P [ <_> {A = A} u ]T} (transp P {t}{u} e w) σ ≡
  transp (P [ σ ^ A ]T) {t [ σ ]t}{u [ σ ]t}(_[_]t e σ) (_[_]t {A = P [ < t > ]T} w σ)
transp[] = refl

-- weak β rule

transpβ : ∀{Γ}{A : Ty Γ}{P : Ty (Γ ▷ A)}{t : Tm Γ A}{w : Tm Γ (P [ < t > ]T)} →
  Tm Γ (Id (transp P {t}{t} (ref t) w) w)
transpβ {Γ}{A}{P}{t}{w} = record
  { ∣_∣t = λ γ → liftp (symT P (cohT P (refC Γ γ ,p ~t t (refC Γ γ)) (∣ w ∣t γ))) }
