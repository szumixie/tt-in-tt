module Setoid.SProp.PSh where

open import Agda.Primitive

open import lib
open import Setoid.SProp.lib

open import Setoid.SProp.Decl {lzero}
open import Setoid.SProp.Core
open import Setoid.SProp.Func
open import Setoid.SProp.Sigma
open import Setoid.SProp.Props
open import Setoid.SProp.ComputIden

_,_∶_ : ∀{Γ Δ}(σ : Tms Γ Δ)(A : Ty Δ) → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
σ , A ∶ t = _,_ σ {A} t

-- a category
postulate
  ℂ    : Ty •
  Hom  : Ty (• ▷ ℂ ▷ ℂ [ ε ]T)
  idℂ  : Tm (• ▷ ℂ) (Hom [ id , ℂ [ ε ]T ∶ vz ]T)
{-
  _∘ℂ_ : Tm (• ▷ ℂ ▷ ℂ [ ε ]T ▷ Hom ▷ ℂ [ ε ]T ▷ Hom [ (ε , ℂ [ ε ]T ∶ vs (vs vz)) , ℂ [ ε ]T ∶ vz ]T)
            (Hom [ {!!} ]T)
-}

PCon : Ty •
PCon = Σ' {!!}
          {!!}

PTy : Ty (• ▷ PCon)

PTms : Ty (• ▷ PCon ▷ PCon [ ε ]T)

PTm : Ty (• ▷ PCon ▷ PTy)

Pid : Tm (• ▷ PCon) (PTms [ (ε , PCon ∶ vz) , PCon [ ε ]T ∶ vz ]T)
