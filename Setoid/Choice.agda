module Setoid.Choice where

open import lib
open import Setoid.Decl
open import Setoid.Core
open import Setoid.Func

open import TT.Decl
open import TT.Core
open import TT.Func

-- embedding of types into setoids

toSetoid : Set → Setoid
toSetoid A = record
  { ∣_∣   = A
  ; _∶_~_ = _≡_
  ; prop  = λ { {a}{.a} refl refl → refl }
  ; ref   = λ _ → refl
  ; sym   = λ { {a}{.a} refl → refl }
  ; trans = λ { {a}{.a}{.a} refl refl → refl }
  }

-- a closed type

toFamSetoid : Setoid → FamSetoid ⊤S
toFamSetoid A = ?

-- propositional truncation of a setoid

∥_∥ : Setoid → Setoid
∥ A ∥ = record
  { ∣_∣   = ∣ A ∣
  ; _∶_~_ = λ _ _ → ⊤
  ; prop  = λ { tt tt → refl }
  ; ref   = λ _ → tt
  ; sym   = λ _ → tt
  ; trans = λ _ _ → tt
  }


-- axiom of choice: (x:A)→ ∥ B x ∥ → ∥ (x:A) → B x ∥

-- we only justify this for As which come from toSetoid C for some set
-- C (a generalisation of countable choice)

open Decl d
open Core c
open Func f

choice
  : {Γ : Setoid}(A : Setoid Γ)(B : FamSetoid A) → Π A ∥ B ∥ → ∥ Π A B ∥
choice = ?
