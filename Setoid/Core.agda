module Setoid.Core where

open import lib
open import JM

open import Setoid.Decl
open import TT.Core
open import TT.Decl.Congr.NoJM d

-- fixities

infixl 5 _,S_
infixl 7 _[_]TS

infixl 5 _,sS_∶_
infix  6 _∘S_
infixl 8 _[_]tS

-- trivial setoid

⊤S : Setoid
⊤S = record
  { ∣_∣   = ⊤
  ; _∶_~_ = λ _ _ → ⊤
  ; prop  = λ { tt tt → refl }
  ; ref   = λ _ → tt
  ; sym   = λ _ → tt
  ; trans = λ _ _ → tt
  }

-- setoid comprehension

_,S_ : (Γ : Setoid) → FamSetoid Γ → Setoid
Γ ,S A = record
  { ∣_∣   = Σ ∣ Γ ∣ ∣ A ∣F_
  ; _∶_~_ = λ { (γ₀ ,Σ a₀) (γ₁ ,Σ a₁) → Σ (Γ ∶ γ₀ ~ γ₁) λ γ₂ → A ∶ γ₂ F a₀ ~ a₁ }
  ; prop  = λ { {γ ,Σ a}{γ' ,Σ a'}(p ,Σ r)(q ,Σ s) → ,Σ= (prop Γ p q) (propF A r s) }
  ; ref   = λ { (γ ,Σ a) → (ref Γ γ) ,Σ refF A a }
  ; sym   = λ { {γ ,Σ a}{γ' ,Σ a'}(p ,Σ r) → (sym Γ p) ,Σ symF A r }
  ; trans = λ { {γ ,Σ a}{γ' ,Σ a'}{γ'' ,Σ a''}(p ,Σ r)(q ,Σ s) → trans Γ p q ,Σ transF A r s }
  }

,S=
  : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
    {A₀ : FamSetoid Γ₀}{A₁ : FamSetoid Γ₁}(A₂ : A₀ ≡[ FamSetoid= Γ₂ ]≡ A₁)
  → Γ₀ ,S A₀ ≡ Γ₁ ,S A₁
,S= refl refl = refl

-- substitution of families of setoids

_[_]TS : {Γ : Setoid}{Θ : Setoid}(A : FamSetoid Γ) → Θ →S Γ → FamSetoid Θ
_[_]TS {Γ}{Θ} A σ = record
  { ∣_∣F_   = λ γ → ∣ A ∣F map σ γ
  ; _∶_F_~_ = λ {γ}{δ} p a b → A ∶ resp σ p F a ~ b
  ; propF   = λ {γ}{δ}{p}{q}{a}{b} r s
              → coe (ap (λ z → r ≡[ z ]≡ s)
                        {(ap (λ z → A ∶ z F a ~ b) (prop Γ (resp σ p) (resp σ q)))}
                        {ap (λ z → A ∶ resp σ z F a ~ b) (prop Θ p q)}
                        (UIP _ _))
                    (propF A r s)
  ; coeF    = λ {γ}{δ} p → coeF A (resp σ p)
  ; cohF    = λ {γ}{δ} p → cohF A (resp σ p)
  ; refF    = λ {γ} a → coe (ap (λ z → A ∶ z F a ~ a) (prop Γ (ref Γ (map σ γ))(resp σ (ref Θ γ)))) (refF A a)
  ; symF    = λ {γ}{δ}{p}{a}{b} r → coe (ap (λ z → A ∶ z F b ~ a) (prop Γ (sym Γ (resp σ p))(resp σ (sym Θ p))))
                                      (symF A r)
  ; transF  = λ {γ}{γ'}{γ'}{p}{q}{a}{a'}{a''} r s → coe (ap (λ z → A ∶ z F a ~ a'') (prop Γ (trans Γ (resp σ p) (resp σ q)) (resp σ (trans Θ p q))))
                                                       (transF A r s)
  }

-- identity setoid morphism

idS : {Γ : Setoid} → Γ →S Γ
idS = record
  { map  = λ γ → γ
  ; resp = λ p → p
  }

-- setoid morphism composition

_∘S_ : {Γ Δ Σ : Setoid} → Δ →S Σ → Γ →S Δ → Γ →S Σ
_∘S_ {Γ}{Δ}{Σ} σ ν = record
  { map  = λ γ → map σ (map ν γ)
  ; resp = λ γ₂ → resp σ (resp ν γ₂)
  }

--

εS : {Γ : Setoid} → Γ →S ⊤S
εS {Γ} = record { map = λ _ → tt ; resp = λ _ → tt }

-- setoid morphism extension

_,sS_∶_ : {Γ Δ : Setoid}(σ : Γ →S Δ)(A : FamSetoid Δ)
      → Γ →F (A [ σ ]TS) → Γ →S (Δ ,S A)
σ ,sS A ∶ t = record
  { map  = λ γ → (map σ γ) ,Σ (mapF t γ)
  ; resp = λ γ₂ → (resp σ γ₂) ,Σ respF t γ₂
  }

-- first projection

π₁S : {Γ Δ : Setoid}(A : FamSetoid Δ)(σ : Γ →S (Δ ,S A))
    → Γ →S Δ
π₁S A σ = record
  { map  = λ γ → proj₁ (map σ γ)
  ; resp = λ γ₂ → proj₁ (resp σ γ₂)
  }
-- substitution of sections

_[_]tS : {Γ Δ : Setoid}{A : FamSetoid Δ}
         (t : Δ →F A)(σ : Γ →S Δ) → Γ →F (A [ σ ]TS)
_[_]tS {Γ}{Δ}{A} t σ = record
  { mapF  = λ γ → mapF t (map σ γ)
  ; respF = λ γ₂ → respF t (resp σ γ₂)
  }

-- second projection

π₂S : {Γ Δ : Setoid}(A : FamSetoid Δ)(σ : Γ →S (Δ ,S A))
    → Γ →F (A [ π₁S A σ ]TS)
π₂S A σ = record
  { mapF  = λ γ → proj₂ (map σ γ)
  ; respF = λ γ₂ → proj₂ (resp σ γ₂)
  }

-- first part of Core

c1 : Core1 d
c1 = record
  { •     = ⊤S
  ; _,_   = _,S_
  ; _[_]T = _[_]TS
  ; id    = idS
  ; _∘_   = _∘S_
  ; ε     = εS
  ; _,s_  = λ σ {A} t → σ ,sS A ∶ t
  ; π₁    = λ {_}{_}{A} σ → π₁S A σ
  ; _[_]t = _[_]tS
  ; π₂    = λ {_}{_}{A} σ → π₂S A σ
  }

open Core1 c1

-- equalities

abstract
  [id]TS : {Γ : Setoid}{A : FamSetoid Γ} → A [ idS ]TS ≡ A
  [id]TS {Γ}{A} = mkFamSetoid= refl refl refl refl

abstract
  [][]TS : {Γ Δ Σ : Setoid}{A : FamSetoid Σ}{σ : Γ →S Δ}{δ : Δ →S Σ}
         → A [ δ ]TS [ σ ]TS ≡ A [ δ ∘S σ ]TS
  [][]TS = mkFamSetoid= refl refl refl refl

abstract
  idlS : {Γ Δ : Setoid}{δ : Γ →S Δ} → idS ∘S δ ≡ δ
  idlS = mk→S= refl refl

abstract
  idrS : {Γ Δ : Setoid}{δ : Γ →S Δ} → δ ∘S idS ≡ δ
  idrS = mk→S= refl refl

abstract
  assS
    : {Δ Γ Σ Ω : Setoid}{σ : Σ →S Ω}{δ : Γ →S Σ}{ν : Δ →S Γ}
    → (σ ∘S δ) ∘S ν ≡ σ ∘S (δ ∘S ν)
  assS = mk→S= refl refl

coerespT=
  : {Γ Δ : Setoid}
    {map₀ map₁ : ∣ Γ ∣ → ∣ Δ ∣}(map₂ : map₀ ≡ map₁)
    {resp' : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₀ γ) ~ (map₀ δ)}
    {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)
  → coe (respT= {Γ}{Δ} map₂) resp' p
  ≡ coe (∶~= {Δ} refl (ap (λ z → z γ) map₂) (ap (λ z → z δ) map₂)) (resp' p)
coerespT= refl p = refl

module ,∘S
  {Γ Δ Σ : Setoid}{ν : Γ →S Δ}{σ : Σ →S Γ}{A : FamSetoid Δ}{a : Γ →F (A [ ν ]TS)}
  where

    abstract
      map₂ : (λ γ → map (ν ,sS A ∶ a) (map σ γ))
           ≡ (λ γ → map (ν ∘S σ) γ ,Σ mapF (coe (TmΓ= ([][]TS {A = A}{σ}{ν})) (a [ σ ]tS)) γ)
      map₂ = funext λ γ → ,Σ= refl ( from≃ (uncoe (∣∣F= ([][]TS {A = A}{σ}{ν}) refl))
                                   ◾ mapFcoe ([][]TS {A = A}{σ}{ν}) ⁻¹)

    abstract
      resp₂
        : (λ {_}{_} γ₂ → resp (ν ,sS A ∶ a) (resp {Σ}{Γ} σ γ₂))
        ≡[ respT= {Σ}{Δ ,S A} map₂ ]≡
          (λ {_}{_} γ₂ → resp (ν ∘S σ) γ₂ ,Σ respF (coe (TmΓ= ([][]TS {A = A}{σ}{ν})) (a [ σ ]tS)) γ₂)
      resp₂ = funexti λ γ → funexti λ δ → funext λ p →
              coerespT= {Σ}{Δ ,S A} map₂ {λ {_}{_} γ₂ → resp (ν ,sS A ∶ a) (resp σ γ₂)} p
            ◾ from≃ ( uncoe (∶~= refl (ap (λ z → z γ) map₂) (ap (λ z → z δ) map₂)) ⁻¹̃
                    ◾̃ ,Σ≃≃ (to≃ (funext λ γ₂ → ∶F~= {Δ}{A} refl refl refl {γ₂} refl
                                                    {mapF a (map σ γ)}
                                                    {mapF (coe (TmΓ= ([][]TS {A = A}{σ}{ν})) (a [ σ ]tS)) γ}
                                                    (from≃ (uncoe (∣∣F= ([][]TS {A = A}{σ}{ν}) refl)) ◾ mapFcoe ([][]TS {A = A}{σ}{ν}) ⁻¹)
                                                    {mapF a (map σ δ)}
                                                    {mapF (coe (TmΓ= ([][]TS {A = A}{σ}{ν})) (a [ σ ]tS)) δ}
                                                    (from≃ (uncoe (∣∣F= ([][]TS {A = A}{σ}{ν}) refl)) ◾ mapFcoe ([][]TS {A = A}{σ}{ν}) ⁻¹)))
                           r̃
                           (respFcoe' ([][]TS {A = A}{σ}{ν}){a [ σ ]tS} p ⁻¹̃))

    ret : (ν ,sS A ∶ a) ∘S σ ≡ (ν ∘S σ) ,sS A ∶ coe (TmΓ= ([][]TS {A = A}{σ}{ν})) (a [ σ ]tS)
    ret = mk→S= map₂ resp₂

abstract
  π₁βS
    : {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S Δ}{a : Γ →F (A [ δ ]TS)}
    → π₁S A (δ ,sS A ∶ a) ≡ δ
  π₁βS = mk→S= refl refl

abstract
  πηS
    : {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S (Δ ,S A)}
    → π₁S A δ ,sS A ∶ π₂S A δ ≡ δ
  πηS = mk→S= refl refl

abstract
  εηS : {Γ : Setoid}{σ : Γ →S ⊤S} → σ ≡ εS
  εηS = mk→S= refl refl

abstract
  π₂βS : {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S Δ}{a : Γ →F (A [ δ ]TS)}
       → π₂S A (δ ,sS A ∶ a)
       ≡[ TmΓ= ([]T=′ d c1 refl refl {A} refl (π₁βS {Γ}{Δ}{A}{δ}{a})) ]≡
         a
  π₂βS = mk→F= refl refl

c2 : Core2 d c1
c2 = record
  { [id]T = [id]TS
  ; [][]T = λ {_}{_}{_}{A}{σ}{δ} → [][]TS {A = A}{σ}{δ}
  ; idl   = idlS
  ; idr   = idrS
  ; ass   = λ {_}{_}{_}{_}{σ}{δ}{ν} → assS {_}{_}{_}{_}{σ}{δ}{ν}
  ; ,∘    = λ {_}{_}{_}{δ}{σ}{A}{a} → ,∘S.ret {_}{_}{_}{δ}{σ}{A}{a}
  ; π₁β   = λ {_}{_}{A}{δ}{a} → π₁βS {_}{_}{A}{δ}{a}
  ; πη    = λ {_}{_}{A}{δ} → πηS {_}{_}{A}{δ}
  ; εη    = εηS
  ; π₂β   = π₂βS
  }

c : Core d
c = record { c1 = c1 ; c2 = c2 }
