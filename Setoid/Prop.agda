{-# OPTIONS --type-in-type --rewriting #-}

module Setoid.Prop where

open import lib
open import JM

open import Setoid.Decl
open import Setoid.Core

-- strict propositions

Prop : Setoid
Prop = record
  { ∣_∣   = Σ Set λ A → (a a' : A) → a ≡ a'
  ; _∶_~_ = λ { (A ,Σ _)(A' ,Σ _) → (A → A') × (A' → A) }
  ; prop  = λ { {A ,Σ propA}{A' ,Σ propA'}(f ,Σ g)(f' ,Σ g') → ,×= (funext λ x → propA' _ _) (funext λ x → propA _ _) }
  ; ref   = λ { (A ,Σ _) → (λ x → x) ,Σ (λ x → x) }
  ; sym   = λ { (f ,Σ g) → g ,Σ f }
  ; trans = λ { (f ,Σ g)(f' ,Σ g') → (λ x → f' (f x)) ,Σ (λ y → g (g' y)) }
  }

El : FamSetoid Prop
El = record
  { ∣_∣F_   = proj₁
  ; _∶_F_~_ = λ { (f ,Σ g) a a' → ⊤ }
    -- this could be (f a ≡ a') or (a ≡ g a'), but both are contractible

  ; propF = λ _ _ → refl
  ; coeF = λ { (f ,Σ g) a → f a }
  ; cohF = λ _ _ → tt
  ; refF = λ _ → tt
  ; symF = λ _ → tt
  ; transF = λ _ _ → tt
  }

-- hpropositions

hProp : Setoid
hProp = record
  { ∣_∣   = Σ Setoid λ A → (a a' : ∣ A ∣) → A ∶ a ~ a'
  ; _∶_~_ = λ { (A ,Σ _) (A' ,Σ _) → ∥ (A →S A') × (A' →S A) ∥ }
  ; prop  = λ _ _ → eq∥ _ _
  ; ref   = λ _ → in∥ (idS ,Σ idS)
  ; sym   = elim∥ eq∥ (λ { (f ,Σ g) → in∥ (g ,Σ f) })
  ; trans = elim∥ (λ _ _ → funext λ _ → eq∥ _ _) (λ { (f ,Σ g) → elim∥ eq∥ λ { (h ,Σ i) → in∥ (h ∘S f ,Σ g ∘S i) } })
  }

hEl : FamSetoid hProp
hEl = record
  { ∣_∣F_   = λ A → ∣ proj₁ A ∣
  ; _∶_F_~_ = λ { {A ,Σ propA}{A' ,Σ propA'} fg∥ a a' → ⊤ }
    -- here we are not able to define this as (f a ≡ a') as before,
    -- because we can't get out f from fg∥ because ∣ A' ∣ is not a
    -- metatheoretic proposition. instead of ⊤ we can go as far as:
    -- 
    -- A' ∶ elim∥ {!propA'!} (λ { (f ,Σ _) → map f a }) fg∥ ~ a'

  ; propF = λ _ _ → refl
  ; coeF = λ { {A ,Σ propA}{A' ,Σ propA'} fg∥ a → elim∥ {!propA'!} (λ { (f ,Σ _) → map f a }) fg∥ }
    -- the same problem here: ∣ A' ∣ is not a proposition, we only have
    -- 
    -- prop A' : {a'₁ a'₂ : ∣ A' ∣}(p q : A' ∶ a'₁ ~ a'₂) → p ≡ q
    --
    -- and
    --
    -- propA' : (a'₁ a'₂ : ∣ A' ∣) → A' ∶ a'₁ ~ a'₂
    --
    -- but none of these imply a'₁ ≡ a'₂
 
  ; cohF = λ _ _ → tt
  ; refF = λ _ → tt
  ; symF = λ _ → tt
  ; transF = λ _ _ → tt
  }
