module Setoid.Readme where

------------------------------------------------------------------------------
-- Setoid model where the relation goes into hProp
------------------------------------------------------------------------------

import Setoid.Decl
import Setoid.Core
import Setoid.Func
-- import Setoid.Prop
-- import Setoid.Choice

------------------------------------------------------------------------------
-- Using Agda's definitionally proof-irrelevant Prop universe for the relation
------------------------------------------------------------------------------

-- usual setoid model

import Setoid.SProp.Decl
import Setoid.SProp.Core
import Setoid.SProp.Iden
import Setoid.SProp.Func
-- import Setoid.SProp.Props -- Props can't be closed under Π because types are not predicative

-- predicative setoid model

import Setoid.SPropPredicative.Decl
import Setoid.SPropPredicative.Core
import Setoid.SPropPredicative.Iden
import Setoid.SPropPredicative.Func
import Setoid.SPropPredicative.Props
import Setoid.SPropPredicative.Trunc
-- import Setoid.SPropPredicative.SetsFailed
-- import Setoid.SPropPredicative.SetsFailed2

-- predicative setoid model with an extra field which makes transport β rule computational

import Setoid.SPropRefl.Decl
import Setoid.SPropRefl.Core
import Setoid.SPropRefl.Iden
import Setoid.SPropRefl.Func
import Setoid.SPropRefl.Props
import Setoid.SPropRefl.Trunc
