{-# OPTIONS --without-K #-}

module Setoid.SPropRefl.Props where

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropRefl.Decl
open import Setoid.SPropRefl.Core

-- a universe of propositions

Props : {Γ : Con} → Ty Γ false
Props {Γ} = record
  { ∣_∣T_ = λ γ → Prop
  ; _T_⊢_~_ = λ _ a b → LiftP ((a → b) ×p (b → a))
  ; refT = λ _ → liftP ((λ x → x) ,p (λ x → x))
  ; symT = λ { (liftP (f ,p g)) → liftP (g ,p f) }
  ; transT = λ { (liftP (f ,p g)) (liftP (f' ,p g')) → liftP ((λ x → f' (f x)) ,p (λ y → g (g' y))) }
  ; coeT = λ _ a → a
  ; cohT = λ _ _ → liftP ((λ x → x) ,p (λ x → x))
  ; coeTRef = refl
  }

El : {Γ : Con} → Tm Γ Props → Ty Γ true
El {Γ} t = record
  { ∣_∣T_ = λ γ → Liftp (∣ t ∣t γ)
  ; _T_⊢_~_ = λ _ _ _ → LiftP ⊤p
  ; coeT = λ { γ~ (liftp α) → liftp (proj₁p (unliftP (~t t γ~)) α) }
  ; coeTRef = refl
  }

Props[] : {Γ Δ : Con}{σ : Tms Γ Δ} → Props [ σ ]T ≡ Props
Props[] = refl
El[] : {Γ Δ : Con}{σ : Tms Γ Δ}{t : Tm Δ Props} → El t [ σ ]T ≡ El (t [ σ ]t)
El[] = refl

-- closed under unit

⊤P : {Γ : Con} → Tm Γ Props
⊤P {Γ} = record { ∣_∣t = λ γ → ⊤p }

ttP : {Γ : Con} → Tm Γ (El ⊤P)
ttP {Γ} = record {}

⊤P[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ⊤P [ σ ]t ≡ ⊤P
⊤P[] = refl
ttP[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ttP [ σ ]t ≡ ttP
ttP[] = refl

-- closed under empty

⊥P : {Γ : Con} → Tm Γ Props
⊥P {Γ} = record { ∣_∣t = λ γ → ⊥p ; ~t = λ _ → liftP ((λ x → x) ,p (λ x → x)) }

abortP : {Γ : Con}{b : Bool}{A : Ty Γ b} → Tm Γ (El ⊥P) → Tm Γ A
abortP {Γ}{A} t = record
  { ∣_∣t = λ γ → abort⊥p (unliftp (∣ t ∣t γ))
  ; ~t = λ {γ} _ → abort⊥pp (unliftp (∣ t ∣t γ))
  }

⊥P[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ⊥P [ σ ]t ≡ ⊥P
⊥P[] = refl
abortP[] : {Γ Δ : Con}{σ : Tms Γ Δ}{b : Bool}{A : Ty Δ b}{t : Tm Δ (El ⊥P)} → abortP {A = A} t [ σ ]t ≡ abortP (t [ σ ]t)
abortP[] = refl

-- closed under Pi

ΠP : {Γ : Con}(A : Ty Γ true)(b : Tm (Γ ▷ A) Props) → Tm Γ Props
ΠP {Γ} A b = record
  { ∣_∣t = λ γ → (α : ∣ A ∣T γ) → ∣ b ∣t (γ ,Σ α)
    ; ~t = λ γ~ → liftP ((λ f α' → proj₁p (unliftP (~t b (γ~ ,p symT A (cohT A (symC Γ γ~) α')))) (f (coeT A (symC Γ γ~) α'))) ,p
                          λ f' α → proj₂p (unliftP (~t b (γ~ ,p cohT A γ~ α))) (f' (coeT A γ~ α)))
  }

lam : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props} → Tm (Γ ▷ A) (El b) → Tm Γ (El (ΠP A b))
lam {Γ}{A}{b} t = record { ∣_∣t = λ γ → liftp λ α → unliftp (∣ t ∣t (γ ,Σ α)) }

app : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props} → Tm Γ (El (ΠP A b)) → Tm (Γ ▷ A) (El b)
app {Γ}{A}{b} t = record { ∣_∣t = λ { (γ ,Σ α) → liftp (unliftp (∣ t ∣t γ) α) } }

ΠPβ : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props}{t : Tm (Γ ▷ A) (El b)} → app {A = A}{b} (lam {A = A}{b} t) ≡ t
ΠPβ = refl

ΠPη : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props}{t : Tm Γ (El (ΠP A b))} → lam {A = A}{b} (app {A = A}{b} t) ≡ t
ΠPη = refl

ΠP[] : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props}{Θ : Con}{σ : Tms Θ Γ} → ΠP A b [ σ ]t ≡ ΠP (A [ σ ]T) (b [ σ ^ A ]t)
ΠP[] = refl

lam[] : {Γ : Con}{A : Ty Γ true}{b : Tm (Γ ▷ A) Props}{t : Tm (Γ ▷ A) (El b)}{Θ : Con}{σ : Tms Θ Γ} →
  lam {A = A}{b} t [ σ ]t ≡ lam {A = A [ σ ]T}{b [ σ ^ A ]t}(t [ σ ^ A ]t)
lam[] = refl

-- closed under Sigma

ΣP : {Γ : Con}(a : Tm Γ Props)(b : Tm (Γ ▷ El a) Props) → Tm Γ Props
ΣP {Γ} a b = record
  { ∣_∣t = λ γ → Σp (∣ a ∣t γ) λ α → ∣ b ∣t (γ ,Σ liftp α)
  ; ~t = λ {γ}{γ'} γ~ → liftP ((λ { (α ,p β) → proj₁p (unliftP (~t a γ~)) α ,p proj₁p (unliftP (~t b (γ~ ,p _))) β }) ,p
                               (λ { (α ,p β) → proj₂p (unliftP (~t a γ~)) α ,p proj₂p (unliftP (~t b (γ~ ,p _))) β }))
  }

_,P_ : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}(t : Tm Γ (El a))(u : Tm Γ (El b [ < t > ]T)) →
  Tm Γ (El (ΣP a b))
t ,P u = record { ∣_∣t = λ γ → liftp (unliftp (∣ t ∣t γ) ,p unliftp (∣ u ∣t γ)) }

proj₁P : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props} → Tm Γ (El (ΣP a b)) → Tm Γ (El a)
proj₁P w = record { ∣_∣t = λ γ → liftp (proj₁p (unliftp (∣ w ∣t γ))) }

proj₂P : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}(w : Tm Γ (El (ΣP a b))) →
  Tm Γ (El b [ < proj₁P {a = a}{b} w > ]T)
proj₂P w = record { ∣_∣t = λ γ → liftp (proj₂p (unliftp (∣ w ∣t γ))) }

ΣPβ₁ : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}{t : Tm Γ (El a)}{u : Tm Γ (El b [ < t > ]T)} →
  proj₁P {a = a}{b} (_,P_ {a = a}{b} t u) ≡ t
ΣPβ₁ = refl

ΣPβ₂ : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}{t : Tm Γ (El a)}{u : Tm Γ (El b [ < t > ]T)}
  → proj₂P {a = a}{b} (_,P_ {a = a}{b} t u) ≡ u
ΣPβ₂ = refl

ΣP[] : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}{Θ : Con}{σ : Tms Θ Γ} →
  ΣP a b [ σ ]t ≡ ΣP (a [ σ ]t) (b [ σ ^ El a ]t)
ΣP[] = refl

,P[] : {Γ : Con}{a : Tm Γ Props}{b : Tm (Γ ▷ El a) Props}{t : Tm Γ (El a)}{u : Tm Γ (El b [ < t > ]T)}{Θ : Con}{σ : Tms Θ Γ} →
  (_,P_ {a = a}{b} t u) [ σ ]t ≡ _,P_ {a = a [ σ ]t}{b = b [ σ ^ El a ]t} (t [ σ ]t) (u [ σ ]t)
,P[] = refl

-- propositional extensionality

open import Setoid.SPropRefl.Iden

ext : {Γ : Con}{a b : Tm Γ Props} →
  Tm (Γ ▷ El a) (El b [ wk {A = El a} ]T) → 
  Tm (Γ ▷ El b) (El a [ wk {A = El b} ]T) →
  Tm Γ (Id a b)
ext f g = record { ∣_∣t = λ γ → liftp (liftP ((λ α → unliftp (∣ f ∣t (γ ,Σ liftp α))) ,p
                                               λ β → unliftp (∣ g ∣t (γ ,Σ liftp β)))) }
