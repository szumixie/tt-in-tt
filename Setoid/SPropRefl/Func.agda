{-# OPTIONS --prop #-}

module Setoid.SPropRefl.Func where

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropRefl.Decl
open import Setoid.SPropRefl.Core

{-
∣Π∣ : {Γ : Con}{b : Bool}(A : Ty Γ b)(B : Ty (Γ ▷ A) b)(γ : ∣ Γ ∣C) → Set ⌜ b ⌝
∣Π∣ {Γ}{true}  A B γ = Σsp ((α : ∣ A ∣T γ) → ∣ B ∣T (γ ,Σ α)) λ ∣_∣Π → {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ,p α~ ⊢ ∣_∣Π α ~ ∣_∣Π α'
∣Π∣ {Γ}{false} A B γ = Σsp ((α : ∣ A ∣T γ) → ∣ B ∣T (γ ,Σ α)) λ ∣_∣Π → {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ,p α~ ⊢ ∣_∣Π α ~ ∣_∣Π α'

infix 4 ∣_∣Π

∣_∣Π : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Prop ℓ'} → Σsp A B → A
∣_∣Π = proj₁sp

~Π : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Prop ℓ'}(w : Σsp A B) → B (proj₁sp w)
~Π = proj₂sp
-}

module test
  {Γ : Con}(A B : Ty Γ true)
  where
    ∣Π∣ : ∣ Γ ∣C → Set
    ∣Π∣ γ = Σsp (∣ A ∣T γ → ∣ B ∣T γ) λ f →
                {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ⊢ f α ~ f α'

    Fun : Ty Γ true
    Fun = mkTy
      ∣Π∣
      (λ {γ}{γ'} γ~ f f' → {α : ∣ A ∣T γ}{α' : ∣ A ∣T γ'}(α~ : A T γ~ ⊢ α ~ α') → B T γ~ ⊢ proj₁sp f α ~ proj₁sp f' α')
      proj₂sp
      (λ f~ α~ → symT B (f~ (symT A α~)))
      (λ {_}{_}{_}{γ~} f~ f~' {α}{α'} α~ → transT B (f~ (cohT A γ~ α)) (f~' (transT A (symT A (cohT A γ~ α)) α~)))
      (λ γ~ f → (λ α' → coeT B γ~ (proj₁sp f (coeT A (symC Γ γ~) α'))) ,sp
                λ {α}{α'} α~ → transT3 B
                                       (symT B (cohT B γ~ (proj₁sp f (coeT* A γ~ α))))
                                       (proj₂sp f (transT A (cohT* A γ~ α) (transT A α~ (cohT A (symC Γ γ~) α'))))
                                       (cohT B γ~ (proj₁sp f (coeT* A γ~ α'))))
      {!!}
      ( ap {A = {γ : ∣ Γ ∣C} → ∣ B ∣T γ → ∣ B ∣T γ}
           {B = {γ : ∣ Γ ∣C} → ∣Π∣ γ → ∣Π∣ γ}
           (λ z → λ {γ} f → (λ α' → z (proj₁sp f (coeT A (refC Γ γ) α'))) ,sp {!!})
           (coeTRef B)
      ◾ ap {A = {γ : ∣ Γ ∣C} → Σsp (∣ A ∣T γ → ∣ A ∣T γ) λ g →
                                   {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → A T refC Γ γ ⊢ g α ~ g α'}
           {B = {γ : ∣ Γ ∣C} → ∣Π∣ γ → ∣Π∣ γ}
           (λ z → λ {γ} f → (λ α' → proj₁sp f (proj₁sp z α')) ,sp λ {α}{α'} α~ → proj₂sp f (proj₂sp z α~))
           {!coeTRef A!})

module _
  {Γ : Con}(A : Ty Γ true)(B : Ty (Γ ▷ A) true)
  where
    Π₁ : (γ : ∣ Γ ∣C) → Set l0
    Π₁ γ = (α : ∣ A ∣T γ) → ∣ B ∣T (γ ,Σ α)
    Π₂ : {γ : ∣ Γ ∣C}(f : Π₁ γ) → Prop l0
    Π₂ {γ} f = {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ,p α~ ⊢ f α ~ f α'
    ∣Π∣ : (γ : ∣ Γ ∣C) → Set l0
    ∣Π∣ γ = Σsp (Π₁ γ) Π₂ 
    Π~ : {γ γ' : ∣ Γ ∣C}(γ~ : Γ C γ ~ γ')(f : ∣Π∣ γ)(f' : ∣Π∣ γ') → Prop l0
    Π~ {γ}{γ'} γ~ f f' = {α : ∣ A ∣T γ}{α' : ∣ A ∣T γ'}(α~ : A T γ~ ⊢ α ~ α') → B T γ~ ,p α~ ⊢ proj₁sp f α ~ proj₁sp f' α'
    refΠ : ∀{γ} f → Π~ (refC Γ γ) f f
    refΠ = proj₂sp
    symΠ : {γ γ' : ∣ Γ ∣C}{γ~ : Γ C γ ~ γ'}{f : ∣Π∣ γ}{f' : ∣Π∣ γ'} → Π~ γ~ f f' → Π~ (symC Γ γ~) f' f
    symΠ f~ α~ = symT B (f~ (symT A α~))
    transΠ : {γ γ' γ'' : ∣ Γ ∣C}{γ~ : Γ C γ ~ γ'}{γ~' : Γ C γ' ~ γ''}{f : ∣Π∣ γ}{f' : ∣Π∣ γ'}{f'' : ∣Π∣ γ''}(f~ : Π~ γ~ f f')(f~' : Π~ γ~' f' f'') →
      Π~ (transC Γ γ~ γ~') f f''
    transΠ {_}{_}{_}{γ~} f~ f~' {α}{α'} α~ = transT B (f~ (cohT A γ~ α)) (f~' (transT A (symT A (cohT A γ~ α)) α~))

    module _
      {γ γ' : ∣ Γ ∣C}(γ~ : Γ C γ ~ γ')(f : ∣Π∣ γ)
      where
      
      coeΠ₁ : Π₁ γ'
      coeΠ₁ α' = coeT B (γ~ ,p cohT* A γ~ α') (proj₁sp f (coeT* A γ~ α'))

      coeΠ₂ : Π₂ coeΠ₁
      coeΠ₂ {α}{α'} α~ =
        transT3 B
          (symT B (cohT B (γ~ ,p cohT* A γ~ α) (proj₁sp f (coeT* A γ~ α))))
          (proj₂sp f (transT A (cohT* A γ~ α) (transT A α~ (cohT A (symC Γ γ~) α'))))
          (cohT B (γ~ ,p cohT* A γ~ α') (proj₁sp f (coeT* A γ~ α')))

    coeΠ : {γ γ' : ∣ Γ ∣C}(γ~ : Γ C γ ~ γ')(f : ∣Π∣ γ) → ∣Π∣ γ'
    coeΠ γ~ f = coeΠ₁ γ~ f ,sp coeΠ₂ γ~ f

    coeΠ₁Ref : (λ {γ} → coeΠ₁ (refC Γ γ)) ≡ proj₁sp
    coeΠ₁Ref = {!apd (λ z → λ {γ : ∣ Γ ∣C}(f : _)(α' : ∣ A ∣T γ) → proj₁sp f z) (coeTRef A)!}

{-

    coeΠ₁' : {γ : ∣ Γ ∣C}(f : ∣Π∣ γ)(coeA : ∣ A ∣T γ → ∣ A ∣T γ) → Π₁ γ
    coeΠ₁' {γ} f coeA = λ α → coeT B (refC Γ γ ,p {! (cohT* A (refC Γ γ) α)!}) (proj₁sp f (coeA α))

    funeq : {γ : ∣ Γ ∣C}(f : ∣Π∣ γ)(γ= : Γ C γ ~ γ)(p : coeT A γ= ≡ (λ α → α)) →
      (λ (f : ∣Π∣ γ) α' → coeT B (γ= ,p cohT* A γ= α') (proj₁sp f (coeT* A γ= α'))) ≡ proj₁sp
    funeq {γ} f γ= p = {!!}
    -- ^ ezt kell altalanositani, hogy p-re refl-el mintat lehessen illeszteni, stb.

    funeq' : {γ : ∣ Γ ∣C}(f : ∣Π∣ γ)(γ= : Γ C γ ~ γ)(coeA : ∣ A ∣T γ → ∣ A ∣T γ)(coeA= : coeA ≡ (λ α → α))
      (cohA : (α : ∣ A ∣T γ) → A T γ= ⊢ α ~ coeA α)
      (coeB : {α α' : ∣ A ∣T γ}(α~ : A T γ= ⊢ α ~ α') → ∣ B ∣T (γ ,Σ α) → ∣ B ∣T (γ ,Σ α'))
      (coeB= : {α : ∣ A ∣T γ}(α= : A T γ= ⊢ α ~ α) → coeB α= ≡ (λ β → β))
      →
      (λ (f : ∣Π∣ γ) α' → coeB (symT A (cohA α')) (proj₁sp f (coeA α'))) ≡ proj₁sp
    funeq' {γ} f γ= .(λ α → α) refl cohA coeB coeB= = {!coeB= (symT A (cohA α'))!}

    coeΠ₁Ref : {γ : ∣ Γ ∣C} → coeΠ₁ (refC Γ γ) ≡ proj₁sp
    coeΠ₁Ref {γ} = {!(coeTRef A {γ})!}

    coeΠRef : {γ : ∣ Γ ∣C} → coeΠ (refC Γ γ) ≡ λ f → f
    coeΠRef {γ} = {! (coeTRef A {γ})!}
-}
