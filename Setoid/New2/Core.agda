{-# OPTIONS --without-K #-}

module Setoid.New2.Core where

open import lib
open import Setoid.New2.lib
open import Setoid.New2.Decl

∙ = record
  { ∣_∣C = ⊤
  ; _=C = ⊤
  ; Pr0C = λ _ → tt
  ; Pr1C = λ _ → tt
  ; RC = λ _ → tt
  ; nat0C = λ _ → refl
  ; nat1C = λ _ → refl
  }

_▷_ : (Γ : Con) → Ty Γ → Con
Γ ▷ A = record
  { ∣_∣C = Σ ∣ Γ ∣C ∣ A ∣T 
  ; _=C = Σ (Γ =C) λ γ= → Σ (∣ A ∣T (Pr0C Γ γ=)) λ α0 → Σ (∣ A ∣T (Pr1C Γ γ=)) λ α1 → El ((A =T) γ= α0 α1)
  ; Pr0C = λ { (γ= ,Σ (α0 ,Σ (α1 ,Σ α=))) → Pr0C Γ γ= ,Σ α0 }
  ; Pr1C = λ { (γ= ,Σ (α0 ,Σ (α1 ,Σ α=))) → Pr1C Γ γ= ,Σ α1 }
  ; RC = λ { (γ ,Σ α) → RC Γ γ ,Σ ((transport ∣ A ∣T (nat0C Γ γ ⁻¹) α) ,Σ ((transport ∣ A ∣T (nat1C Γ γ ⁻¹) α) ,Σ RT A α)) }
  ; nat0C = λ { (γ ,Σ α) → ,Σ= (nat0C Γ γ) (trtr' ∣ A ∣T (nat0C Γ γ)) }
  ; nat1C = λ { (γ ,Σ α) → ,Σ= (nat1C Γ γ) (trtr' ∣ A ∣T (nat1C Γ γ)) }
  }

_[_]T : {Γ Δ : Con}(A : Ty Δ) → Tms Γ Δ → Ty Γ
_[_]T {Γ}{Δ} A σ = record
  { ∣_∣T = λ γ → ∣ A ∣T (∣ σ ∣s γ)
  ; _=T = λ γ= α0 α1 → (A =T) ((σ =s) γ=) (transport ∣ A ∣T (Pr0s σ γ= ⁻¹) α0) (transport ∣ A ∣T (Pr1s σ γ= ⁻¹) α1)
  ; RT = λ {γ} α → coe {!!} (RT A {∣ σ ∣s γ} α)
  }

id : {Γ : Con} → Tms Γ Γ
id {Γ} = record
  { ∣_∣s = λ γ → γ
  ; _=s = λ γ= → γ=
  ; Pr0s = λ γ= → refl
  ; Pr1s = λ γ= → refl
  ; Rs = λ γ → refl
  }

_∘_ : {Γ Θ Δ : Con} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
σ ∘ δ = record
  { ∣_∣s = λ γ → ∣ σ ∣s (∣ δ ∣s γ)
  ; _=s = λ γ= → (σ =s) ((δ =s) γ=)
  ; Pr0s = λ γ= → Pr0s σ ((δ =s) γ=) ◾ ap ∣ σ ∣s (Pr0s δ γ=)
  ; Pr1s = λ γ= → Pr1s σ ((δ =s) γ=) ◾ ap ∣ σ ∣s (Pr1s δ γ=)
  ; Rs = λ γ → Rs σ (∣ δ ∣s γ) ◾ ap (σ =s) (Rs δ γ)
  }

ε : {Γ : Con} → Tms Γ ∙
ε {Γ} = record
  { ∣_∣s = λ γ → tt
  ; _=s = λ γ= → tt
  ; Pr0s = λ γ= → refl
  ; Pr1s = λ γ= → refl
  ; Rs = λ γ → refl
  }

_,_ : {Γ Δ : Con}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
_,_ {Γ}{Σ} σ {A} t = record
  { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ
  ; _=s = λ γ= → (σ =s) γ= ,Σ (
                 transport ∣ A ∣T (Pr0s σ γ= ⁻¹) (∣ t ∣t (Pr0C Γ γ=)) ,Σ (
                 transport ∣ A ∣T (Pr1s σ γ= ⁻¹) (∣ t ∣t (Pr1C Γ γ=)) ,Σ
                 (t =t) γ=))
  ; Pr0s = λ γ= → ,Σ= (Pr0s σ γ=) (trtr' ∣ A ∣T (Pr0s σ γ=))
  ; Pr1s = λ γ= → ,Σ= (Pr1s σ γ=) (trtr' ∣ A ∣T (Pr1s σ γ=))
  ; Rs = λ γ → ,Σ= (Rs σ γ) {!Rt t {γ}!}
  }

idl : {Γ Δ : Con}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ
idl = {!!}

idr : {Γ Δ : Con}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ
idr = {!!}

