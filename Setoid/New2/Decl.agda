{-# OPTIONS --without-K #-}

module Setoid.New2.Decl where

open import lib
open import Setoid.New2.lib

-- Decl

record Con : Set₁ where
  field
    ∣_∣C  : Set
    _=C   : Set
    Pr0C  : _=C → ∣_∣C
    Pr1C  : _=C → ∣_∣C
    RC    : ∣_∣C → _=C
    nat0C : (γ : ∣_∣C) → Pr0C (RC γ) ≡ γ
    nat1C : (γ : ∣_∣C) → Pr1C (RC γ) ≡ γ
open Con public

record Tms (Γ Δ : Con) : Set where
  field
    ∣_∣s  : ∣ Γ ∣C → ∣ Δ ∣C
    _=s   : Γ =C → Δ =C
    Pr0s  : (γ= : Γ =C) → Pr0C Δ (_=s γ=) ≡ ∣_∣s (Pr0C Γ γ=)
    Pr1s  : (γ= : Γ =C) → Pr1C Δ (_=s γ=) ≡ ∣_∣s (Pr1C Γ γ=)
    Rs    : (γ : ∣ Γ ∣C) → RC Δ (∣_∣s γ) ≡ _=s (RC Γ γ)
    -- maybe we need nats0 and nats1 here
open Tms public

record Ty (Γ : Con) : Set₁ where
  field
    ∣_∣T  : ∣ Γ ∣C → Set
    _=T   : (γ= : Γ =C) → ∣_∣T (Pr0C Γ γ=) → ∣_∣T (Pr1C Γ γ=) → Props
    RT    : {γ : ∣ Γ ∣C}(α : ∣_∣T γ) →
      El (_=T (RC Γ γ)
              (transport ∣_∣T (nat0C Γ γ ⁻¹) α)
              (transport ∣_∣T (nat1C Γ γ ⁻¹) α))
open Ty public

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    ∣_∣t : (γ : ∣ Γ ∣C) → ∣ A ∣T γ
    _=t  : (γ= : Γ =C) → El ((A =T) γ= (∣_∣t (Pr0C Γ γ=)) (∣_∣t (Pr1C Γ γ=)))
    Rt   : {γ : ∣ Γ ∣C} → coe (ap2 (λ z z' → El ((A =T) (RC Γ γ) z z'))
                                   (trself ∣ A ∣T ∣_∣t (nat0C Γ γ))
                                   (trself ∣ A ∣T ∣_∣t (nat1C Γ γ)))
                              (RT A (∣_∣t γ)) ≡
                          (_=t (RC Γ γ))
open Tm public
