module Setoid.New2.lib where

open import lib

data Props : Set
El : Props → Set

data Props where
  ⊤' : Props
  ⊥' : Props
  Π' : (a : Props)(b : El a → Props) → Props
  Σ' : (a : Props)(b : El a → Props) → Props

El ⊤' = ⊤
El ⊥' = ⊥
El (Π' a b) = (x : El a) → El (b x)
El (Σ' a b) = Σ (El a) λ x → El (b x)
