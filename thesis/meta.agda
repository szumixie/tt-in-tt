module thesis.meta where

-- formalisation corresponding to the metatheory chapter of (chapter
-- 2) of the PhD thesis: Type theory in a type theory with quotient
-- inductive types (Ambrus Kaposi)

------------------------------------------------------------------------------
-- section 2.1 Definitions, universes, Π and Σ
------------------------------------------------------------------------------

open import Agda.Primitive

data ⊥ : Set where

data Bool : Set where
  true false : Bool

record ⊤ : Set where
  constructor tt

record Σ {i j}(A : Set i)(B : A → Set j) : Set (i ⊔ j) where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁

open Σ

_×_ : (A B : Set) → Set
A × B = Σ A λ _ → B

data _⊎_ (A B : Set) : Set where
  inj₁ : A → A ⊎ B
  inj₂ : B → A ⊎ B
infixr 1 _⊎_

[_,_] : {C : Set}{A B : Set} → (A → C) → (B → C) → (A ⊎ B) → C
[ f , g ] (inj₁ x) = f x
[ f , g ] (inj₂ x) = g x

ind⊎ : {A B : Set}(P : A ⊎ B → Set) → ((a : A) → P (inj₁ a)) → ((b : B) → P (inj₂ b))
     → (w : A ⊎ B) → P w
ind⊎ P ca cb (inj₁ a) = ca a
ind⊎ P ca cb (inj₂ b) = cb b

-- moved forward from later sections for dependencies

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

Recℕ : (ℕᴹ : Set)
       (zeroᴹ : ℕᴹ)(sucᴹ : ℕᴹ → ℕᴹ)
       → ℕ → ℕᴹ
Recℕ ℕᴹ zeroᴹ sucᴹ zero    = zeroᴹ
Recℕ ℕᴹ zeroᴹ sucᴹ (suc n) = sucᴹ (Recℕ ℕᴹ zeroᴹ sucᴹ n)

data _≡_ {i}{A : Set i}(a : A) : A → Set i where
  refl : a ≡ a

infix 3 _≡_

-- end of moved forward

module Mutual where

  isEven isOdd : ℕ → Bool

  isEven zero = true
  isEven (suc n) = isOdd n

  isOdd zero = false
  isOdd (suc n) = isEven n

  isEvenOdd : ℕ → Bool × Bool
  isEvenOdd zero = true , false
  isEvenOdd (suc n) = let (e , o) = isEvenOdd n
                      in  (o , e)

  -- we can't even write this with the recursor (and this version
  -- doesn't pass the termination checker)
  -- isEven' isOdd' : ℕ → Bool
  -- isEven' = Elimℕ (λ _ → Bool) true (λ n _ → isOdd' n)
  -- isOdd' = Elimℕ (λ _ → Bool) false (λ n _ → isEven' n)

  isEvenOdd' : ℕ → Bool × Bool
  isEvenOdd' = Recℕ (Bool × Bool) (true , false) (λ p → (proj₂ p , proj₁ p))

  -- unit tests

  five six : ℕ
  five = suc (suc (suc (suc (suc zero))))
  six = suc five

  u1 : isEven five ≡ false
  u2 : isOdd  five ≡ true
  u3 : isEven six  ≡ true
  u4 : isOdd  six  ≡ false
  u5 : isEvenOdd five ≡ (false , true)
  u6 : isEvenOdd six ≡ (true , false)
  -- u1' : isEven' five ≡ false
  -- u2' : isOdd'  five ≡ true
  -- u3' : isEven' six  ≡ true
  -- u4' : isOdd'  six  ≡ false
  u5' : isEvenOdd' five ≡ (false , true)
  u6' : isEvenOdd' six ≡ (true , false)

  u1 = refl
  u2 = refl
  u3 = refl
  u4 = refl
  u5 = refl
  u6 = refl
  -- u1' = refl
  -- u2' = refl
  -- u3' = refl
  -- u4' = refl
  u5' = refl
  u6' = refl

------------------------------------------------------------------------------
-- section 2.2 Inductive types
------------------------------------------------------------------------------

Elimℕ : (ℕᴹ : ℕ → Set)
        (zeroᴹ : ℕᴹ zero)(sucᴹ : (n : ℕ) → ℕᴹ n → ℕᴹ (suc n))
        (n : ℕ) → ℕᴹ n
Elimℕ ℕᴹ zeroᴹ sucᴹ zero    = zeroᴹ
Elimℕ ℕᴹ zeroᴹ sucᴹ (suc n) = sucᴹ n (Elimℕ ℕᴹ zeroᴹ sucᴹ n)

_+_ : ℕ → ℕ → ℕ
zero  + n = n
suc m + n = suc (m + n)

_+'_ : ℕ → ℕ → ℕ
_+'_ = Recℕ (ℕ → ℕ) (λ n → n) (λ f n → suc (f n))

data W (S : Set)(P : S → Set) : Set where
  sup : (s : S) → (P s → W S P) → W S P

ElimW : {S : Set}{P : S → Set}
        (Wᴹ : W S P → Set)
        (supᴹ : (s : S)(f : P s → W S P)(fᴹ : (p : P s) → Wᴹ (f p)) → Wᴹ (sup s f))
        (w : W S P) → Wᴹ w
ElimW Wᴹ supᴹ (sup s f) = supᴹ s f (λ p → ElimW Wᴹ supᴹ (f p))

Sℕ : Set
Sℕ = Bool
Pℕ : Sℕ → Set
Pℕ false = ⊥
Pℕ true = ⊤

ℕ' : Set
ℕ' = W Sℕ Pℕ

zero' : ℕ'
zero' = sup false (λ ())

suc' : ℕ' → ℕ'
suc' n = sup true (λ _ → n)

{-
Elimℕ' : (ℕᴹ : ℕ' → Set)
        (zeroᴹ : ℕᴹ zero')(sucᴹ : (n : ℕ') → ℕᴹ n → ℕᴹ (suc' n))
        (n : ℕ') → ℕᴹ n
Elimℕ' ℕᴹ zeroᴹ sucᴹ = ElimW  ℕᴹ (λ { false → λ _ _ → {!!} ; true → {!!} })
-}

------------------------------------------------------------------------------
-- section 2.2.1 Inductive Families
------------------------------------------------------------------------------

data WI {I : Set}(S : I → Set)(P : (i : I) → S i → I → Set) : I → Set where
  supI : {i : I}(s : S i) → ((j : I) → P i s j → WI S P j) → WI S P i

data WI' {I : Set}(S : I → Set)(P : {i : I} → S i → Set)(f : {i : I}{s : S i} → P s → I) : I → Set where
  supI' : {i : I}(s : S i) → ((p : P s) → WI' S P f (f p)) → WI' S P f i

record CWI (I : Set) : Set₁ where
  field
    S : Set
    P : S → Set
    out : S → I
    inp : (s : S) → P s → I

open CWI

⟦_⟧Icon : {I : Set} → CWI I → (I → Set) → Set
⟦ c ⟧Icon X = Σ (S c) λ s → ((p : P c s) → X (inp c _ p))

Index : {I : Set}{c : CWI I}{X : I → Set} → ⟦ c ⟧Icon X → I
Index {I}{c} (s , _) = out c s

⟦_⟧Icon' : {I : Set} → CWI I → (I → Set) → Σ Set λ X → X → I
⟦ c ⟧Icon' X = (Σ (S c) λ s → ((p : P c s) → X (inp c _ p)))
             , (λ { (s , _) → out c s })

data WI'' {I : Set}(c : CWI I) : I → Set where
  con : (w : ⟦ c ⟧Icon (WI'' c)) → WI'' c (Index {I}{c}{WI'' c} w)

data WI''' {I : Set}(c : CWI I) : I → Set where
  con : (w : proj₁ (⟦ c ⟧Icon' (WI''' c))) → WI''' c (proj₂ (⟦ c ⟧Icon' (WI''' c)) w)

Vec : (A : Set) → ℕ → Set
Vec A = WI'' {ℕ} record
  { S = Σ Bool (λ { true → ⊤ ; false → A × ℕ })
  ; P = λ { (true , _) → ⊥ ; (false , _) → ⊤ }
  ; out = λ { (true , _) → zero ; (false , (_ , n)) → suc n }
  ; inp = λ { (true , _) () ; (false , (_ , n)) _ → n  }
  }

------------------------------------------------------------------------------
-- section 2.2.2 Induction recursion
------------------------------------------------------------------------------

module IR where

  data IR {i}(D : Set i) : Set (lsuc i) where
    nil : D → IR D
    nonind : (A : Set) → (A → IR D) → IR D
    ind : (A : Set) → ((A → D) → IR D) → IR D

  ⟦_⟧con : {D : Set} → IR D → (U : Set) → (U → D) → Set
  ⟦ nil d ⟧con U T = ⊤
  ⟦ nonind A f ⟧con U T = Σ A λ a → ⟦ f a ⟧con U T
  ⟦ ind A F ⟧con U T = Σ (A → U) λ g → ⟦ F (λ x → T (g x)) ⟧con U T

  ⟦_⟧fun : {D : Set}(γ : IR D)(U : Set)(T : U → D) → ⟦ γ ⟧con U T → D
  ⟦ nil d ⟧fun U T tt = d
  ⟦ nonind A f ⟧fun U T (a , x) = ⟦ f a ⟧fun U T x
  ⟦ ind A F ⟧fun U T (g , x) = ⟦ F (λ x → T (g x)) ⟧fun U T x

  data WIR {D : Set}(γ : IR D) : Set

  {-# TERMINATING #-}
  T : {D : Set}(γ : IR D) → WIR γ → D

  data WIR {D} γ where
    con : ⟦ γ ⟧con (WIR γ) (T γ) → WIR γ

  T γ (con w) = ⟦ γ ⟧fun (WIR γ) (T γ) w

  cUni : IR Set
  cUni = nonind Bool (λ { true → nil ⊥ ; false → ind ⊤ (λ a → ind (a tt) (λ b → nil ((x : a tt) → b x))) })

------------------------------------------------------------------------------
-- section 2.2.3 The identity type
------------------------------------------------------------------------------

ap : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
ap f refl = refl

transport : ∀{i j}{A : Set i}(P : A → Set j){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
            → P a₀ → P a₁
transport P refl u = u

coe : ∀{i}{A B : Set i} → A ≡ B → A → B
coe refl a = a

+≡+' : ∀(m : ℕ){n : ℕ} → (m + n) ≡ (m +' n)
+≡+' zero = refl
+≡+' (suc m) = ap suc (+≡+' m)

postulate
  funext : ∀{i j}{A : Set i}{B : A → Set j}{f g : (x : A) → B x}
         → ((x : A) → f x ≡ g x) → _≡_ f g

Id : (A : Set) → A → A → Set
Id A a b = WI'' {A × A} record { S = A ; P = λ _ → ⊥ ; out = λ a → a , a ; inp = λ _ () } (a , b)

------------------------------------------------------------------------------
-- section 2.3 Induction induction
------------------------------------------------------------------------------

module IITs where

  postulate
    _≤_ : ℕ → ℕ → Set

  data Con : Set
  data Ty : Con → Set

  data Con where
    · : Con
    _,_ : (Γ : Con) → Ty Γ → Con

  data Ty where
    U : {Γ : Con} → Ty Γ
    Π : {Γ : Con}(A : Ty Γ) → Ty (Γ , A) → Ty Γ

  data SortedList : Set
  data _≤SL_ : ℕ → SortedList → Set

  data SortedList where
    nil : SortedList
    cons : (x : ℕ)(xs : SortedList) → x ≤SL xs → SortedList

  data _≤SL_ where
    nil≤ : {x : ℕ} → x ≤SL nil
    cons≤ : {x y : ℕ} → y ≤ x → {xs : SortedList} → (p : x ≤SL xs) → y ≤SL cons x xs p

  record MRecConTy : Set₁ where
    field
      Conᴹ : Set
      Tyᴹ : Conᴹ → Set
      ·ᴹ : Conᴹ
      _,ᴹ_ : (Γᴹ : Conᴹ) → Tyᴹ Γᴹ → Conᴹ
      Uᴹ : {Γᴹ : Conᴹ} → Tyᴹ Γᴹ
      Πᴹ : {Γᴹ : Conᴹ}(Aᴹ : Tyᴹ Γᴹ) → Tyᴹ (Γᴹ ,ᴹ Aᴹ) → Tyᴹ Γᴹ

  module RecConTy (m : MRecConTy) where
    open MRecConTy m
    RecCon : Con → Conᴹ
    RecTy : {Γ : Con} → Ty Γ → Tyᴹ (RecCon Γ)
    RecCon · = ·ᴹ
    RecCon (Γ , A) = RecCon Γ ,ᴹ RecTy A
    RecTy U = Uᴹ
    RecTy (Π A B) = Πᴹ (RecTy A) (RecTy B)

  record MElimConTy : Set₁ where
    field
      Conᴹ : Con → Set
      Tyᴹ : {Γ : Con} → Conᴹ Γ → Ty Γ → Set
      ·ᴹ : Conᴹ ·
      _,ᴹ_ : {Γ : Con}(Γᴹ : Conᴹ Γ){A : Ty Γ} → Tyᴹ Γᴹ A → Conᴹ (Γ , A)
      Uᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ} → Tyᴹ Γᴹ (U {Γ})
      Πᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}{A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A){B : Ty (Γ , A)} → Tyᴹ (Γᴹ ,ᴹ Aᴹ) B  → Tyᴹ Γᴹ (Π A B)

  module ElimConTy (m : MElimConTy) where
    open MElimConTy m
    RecCon : (Γ : Con) → Conᴹ Γ
    RecTy : {Γ : Con}(A : Ty Γ) → Tyᴹ (RecCon Γ) A
    RecCon · = ·ᴹ
    RecCon (Γ , A) = RecCon Γ ,ᴹ RecTy A
    RecTy U = Uᴹ
    RecTy (Π A B) = Πᴹ (RecTy A) (RecTy B)

  record MRecSortedList : Set₁ where
    field
      SortedListᴹ : Set
      _≤SLᴹ_ : ℕ → SortedListᴹ → Set
      nilᴹ : SortedListᴹ
      consᴹ : (x : ℕ)(xsᴹ : SortedListᴹ) → x ≤SLᴹ xsᴹ → SortedListᴹ
      nil≤ᴹ : {x : ℕ} → x ≤SLᴹ nilᴹ
      cons≤ᴹ : {x y : ℕ} → y ≤ x → {xsᴹ : SortedListᴹ} → (pᴹ : x ≤SLᴹ xsᴹ) → y ≤SLᴹ consᴹ x xsᴹ pᴹ

  module RecSortedList (m : MRecSortedList) where
    open MRecSortedList m
    RecSortedList : SortedList → SortedListᴹ
    Rec≤SL : {x : ℕ}{xs : SortedList} → x ≤SL xs → x ≤SLᴹ (RecSortedList xs)
    RecSortedList nil = nilᴹ
    RecSortedList (cons x xs p) = consᴹ x (RecSortedList xs) (Rec≤SL p)
    Rec≤SL nil≤ = nil≤ᴹ
    Rec≤SL (cons≤ w p) = cons≤ᴹ w (Rec≤SL p)

  -- codes for induction induction

  data CA' (Aref : Set) : Set₁ where
    nil : CA' Aref
    nonind : (K : Set) → (K → CA' Aref) → CA' Aref
    indA : (K : Set) → CA' (Aref ⊎ K) → CA' Aref
    indB : (K : Set)(hind : K → Aref) → CA' Aref → CA' Aref

  c, : CA' ⊥
  c, = indA ⊤ (indB ⊤ (λ _ → inj₂ tt) nil)

  c· : CA' ⊥
  c· = nil

  ⟦_⟧A' : {Aref : Set}(γ : CA' Aref)(A : Set)(B : A → Set)(repA : Aref → A) → Set
  ⟦ nil ⟧A' A B repA = ⊤
  ⟦ nonind K γ ⟧A' A B repA = Σ K λ k → ⟦ γ k ⟧A' A B repA
  ⟦ indA K γ ⟧A' A B repA = Σ (K → A) λ j → ⟦ γ ⟧A' A B [ repA , j ]
  ⟦ indB K hind γ ⟧A' A B repA = ((k : K) → B (repA (hind k))) × ⟦ γ ⟧A' A B repA

  CA : Set₁
  CA = CA' ⊥

  ⟦_⟧A : (γ : CA)(A : Set)(B : A → Set) → Set
  ⟦ γ ⟧A A B = ⟦ γ ⟧A' A B (λ ())

  c,= : {Con : Set}{Ty : Con → Set} → ⟦ c, ⟧A Con Ty ≡ Σ (⊤ → Con) λ Γ → ((_ : ⊤) → Ty (Γ tt)) × ⊤
  c,= = refl

  c·= : {Con : Set}{Ty : Con → Set} → ⟦ c· ⟧A Con Ty ≡ ⊤
  c·= = refl

  data TA (Aref Bref : Set)(γ : CA) : Set
  TB : {Aref Bref : Set}{γ : CA} → TA Aref Bref γ → Set

  data TA Aref Bref γ where
    aref : (a : Aref) → TA Aref Bref γ
    bref : (b : Bref) → TA Aref Bref γ
    arg  : ⟦ γ ⟧A (TA Aref Bref γ) TB → TA Aref Bref γ

  TB (aref a) = ⊥
  TB (bref b) = ⊤
  TB (arg x) = ⊥

  ⟦⟧A'func
    : {A A' : Set}{B : A → Set}{B' : A' → Set}
      {Aref : Set}{repA : Aref → A}{repA' : Aref → A'}
      {γ : CA' Aref}
      (f : A → A')(g : (a : A) → B a → B' (f a))
      (p : (a : Aref) → B' (f (repA a)) → B' (repA' a))
    → ⟦ γ ⟧A' A B repA → ⟦ γ ⟧A' A' B' repA'
  ⟦⟧A'func {γ = nil} f g p tt = tt
  ⟦⟧A'func {γ = nonind K γ} f g p (k , w)
    = k , ⟦⟧A'func {γ = γ k} f g p w
  ⟦⟧A'func {γ = indA K γ} f g p (j , w)
    = (λ k → f (j k)) , ⟦⟧A'func {γ = γ} f g (ind⊎ _ p (λ k b → b)) w
  ⟦⟧A'func {repA = repA}{repA'}{indB K hind γ} f g p (j , w)
    = (λ k → p (hind k) (g (repA (hind k)) (j k))) , (⟦⟧A'func {γ = γ} f g p w)

  ⟦⟧Afunc
    : {A A' : Set}{B : A → Set}{B' : A' → Set}
      {γ : CA}
      (f : A → A')(g : (a : A) → B a → B' (f a))
    → ⟦ γ ⟧A A B → ⟦ γ ⟧A A' B'
  ⟦⟧Afunc {γ = γ} f g w = ⟦⟧A'func {Aref = ⊥}{λ ()}{λ ()}{γ} f g (λ ()) w

  {-# TERMINATING #-}

  ⟦_⟧TA : {Aref Bref : Set}{γ : CA}
        → TA Aref Bref γ
        → (A : Set)(B : A → Set)
          (repA : Aref → A)(repind : Bref → A)(repB : (b : Bref) → B (repind b))
          (conA : ⟦ γ ⟧A A B → A)
        → A

  ⟦_⟧TB : {Aref Bref : Set}{γ : CA}{a : TA Aref Bref γ}
        → TB a
        → (A : Set)(B : A → Set)
          (repA : Aref → A)(repind : Bref → A)(repB : (b : Bref) → B (repind b))
          (conA : ⟦ γ ⟧A A B → A)
        → B (⟦ a ⟧TA A B repA repind repB conA)

  ⟦ aref a ⟧TA A B repA repind repB conA = repA a
  ⟦ bref b ⟧TA A B repA repind repB conA = repind b
  ⟦_⟧TA {Aref}{Bref}{γ} (arg x) A B repA repind repB conA
    = conA (⟦⟧Afunc {TA Aref Bref γ}{A}{TB}{B}{γ}
           (λ a → ⟦ a ⟧TA A B repA repind repB conA)
           (λ a b → ⟦_⟧TB {Aref}{Bref}{γ}{a} b A B repA repind repB conA) x )

  ⟦_⟧TB {a = aref a} () A B repA repind repB conA
  ⟦_⟧TB {a = bref b} tt A B repA repind repB conA = repB b
  ⟦_⟧TB {a = arg x} () A B repA repind repB conA

  data CB' (Aref Bref : Set)(γ : CA) : Set₁ where
    nil : TA Aref Bref γ → CB' Aref Bref γ
    nonind : (K : Set) → (K → CB' Aref Bref γ) → CB' Aref Bref γ
    indA : (K : Set) → CB' (Aref ⊎ K) Bref γ → CB' Aref Bref γ
    indB : (K : Set)(hind : K → TA Aref Bref γ) → CB' Aref (Bref ⊎ K) γ → CB' Aref Bref γ

  ⟦_⟧B' : {Aref Bref : Set}{γ : CA}
        → CB' Aref Bref γ
        → (A : Set)(B : A → Set)
          (repA : Aref → A)(repind : Bref → A)(repB : (b : Bref) → B (repind b))
          (conA : ⟦ γ ⟧A A B → A)
        → Set
  ⟦ nil _ ⟧B' A B repA repind repB conA = ⊤
  ⟦ nonind K δ ⟧B' A B repA repind repB conA = Σ K λ k → ⟦ δ k ⟧B' A B repA repind repB conA
  ⟦_⟧B' {Aref = Aref} (indA K δ) A B repA repind repB conA
    = Σ (K → A) λ j → ⟦_⟧B' {Aref = Aref ⊎ K} δ A B [ repA , j ] repind repB conA
  ⟦_⟧B' {Bref = Bref} (indB K hind δ) A B repA repind repB conA
    = Σ ((k : K) → B (⟦ hind k ⟧TA A B repA repind repB conA)) λ j
      → ⟦_⟧B' {Bref = Bref ⊎ K} δ A B repA
              ([_,_] {A}{Bref}{K} repind (λ k → ⟦ hind k ⟧TA A B repA repind repB conA))
              (ind⊎ {Bref}{K} _ repB j)
              conA

  index' : {Aref Bref : Set}{γ : CA}
          (δ : CB' Aref Bref γ)
        → {A : Set}{B : A → Set}
          {repA : Aref → A}{repind : Bref → A}{repB : (b : Bref) → B (repind b)}
          {conA : ⟦ γ ⟧A A B → A}
        → ⟦ δ ⟧B' A B repA repind repB conA → A
  index' (nil a) {A = A}{B}{repA}{repind}{repB}{conA} tt = ⟦ a ⟧TA A B repA repind repB conA
  index' (nonind K δ) (k , y) = index' (δ k) y
  index' (indA K δ) (j , y) = index' δ y
  index' (indB K hind δ) (j , y) = index' δ y

  CB : CA → Set₁
  CB = CB' ⊥ ⊥

  ⟦_⟧B : {γ : CA} → CB γ → (A : Set)(B : A → Set)(conA : ⟦ γ ⟧A A B → A) → Set
  ⟦_⟧B {γ} δ A B conA = ⟦_⟧B' δ A B (λ ())(λ ())(λ ()) conA

  index : {γ : CA}{δ : CB γ}{A : Set}{B : A → Set}{conA : ⟦ γ ⟧A A B → A} → ⟦ δ ⟧B A B conA → A
  index {γ}{δ}{A}{B}{conA} b = index' δ {repA = λ ()}{λ ()}{λ ()}{conA} b

  cU : CB c,
  cU = indA ⊤ (nil (aref (inj₂ tt)))

  cU= : {Con : Set}{Ty : Con → Set}{con, : ⟦ c, ⟧A Con Ty → Con}
      → ⟦ cU ⟧B Con Ty con,
      ≡ Σ (⊤ → Con) λ Γ → ⊤
  cU= = refl

  iU= : {Con : Set}{Ty : Con → Set}{con, : ⟦ c, ⟧A Con Ty → Con}{conU : ⟦ cU ⟧B Con Ty con,}
      → index {c,}{cU}{Con}{Ty}{con,} conU ≡ (proj₁ conU tt)
  iU= = refl

  cΠ : CB c,
  cΠ = indA ⊤ (indB ⊤ (λ _ → aref (inj₂ tt)) (indB ⊤ (λ _ → arg ((λ _ → bref (inj₂ tt)) , ((λ _ → tt) , tt))) (nil (aref (inj₂ tt)))))

  cΠ= : {Con : Set}{Ty : Con → Set}{con, : ⟦ c, ⟧A Con Ty → Con}
      → ⟦ cΠ ⟧B Con Ty con,
      ≡ Σ (⊤ → Con) λ Γ →
        Σ ((_ : ⊤) → Ty (Γ tt)) λ A →
        Σ ((_ : ⊤) → Ty (con, ((λ _ → Γ tt) , ((λ _ → A tt) , tt)))) λ B →
          ⊤
  cΠ= = refl

  iΠ= : {Con : Set}{Ty : Con → Set}{con, : ⟦ c, ⟧A Con Ty → Con}{conΠ : ⟦ cΠ ⟧B Con Ty con,}
      → index {c,}{cΠ}{Con}{Ty}{con,} conΠ ≡ proj₁ conΠ tt
  iΠ= = refl

------------------------------------------------------------------------------
-- section 2.4 Quotient inductive types
------------------------------------------------------------------------------

module monoid where

  postulate
    X : Set

  -- as a QIT

  data A : Set where
    emb : X → A
    id  : A
    _∘_ : A → A → A
  infixl 6 _∘_
  postulate
    idl : ∀{a} → id ∘ a ≡ a
    idr : ∀{a} → a ∘ id ≡ a
    ass : ∀{a a' a''} → (a ∘ a') ∘ a'' ≡ a ∘ (a' ∘ a'')

  record MA : Set₁ where
    field
      Aᴹ : Set
      embᴹ : X → Aᴹ
      idᴹ : Aᴹ
      _∘ᴹ_ : Aᴹ → Aᴹ → Aᴹ
      idlᴹ : ∀{aᴹ} → idᴹ ∘ᴹ aᴹ ≡ aᴹ
      idrᴹ : ∀{aᴹ} → aᴹ ∘ᴹ idᴹ ≡ aᴹ
      assᴹ : ∀{aᴹ a'ᴹ a''ᴹ} → (aᴹ ∘ᴹ a'ᴹ) ∘ᴹ a''ᴹ ≡ aᴹ ∘ᴹ (a'ᴹ ∘ᴹ a''ᴹ)

  module RecA (m : MA) where
    open MA m
    rec : A → Aᴹ
    rec (emb x)  = embᴹ x
    rec id       = idᴹ
    rec (a ∘ a') = rec a ∘ᴹ rec a'

  -- as a QT

  data B : Set where
    emb : X → B
    id  : B
    _∘_ : B → B → B

  record MB : Set₁ where
    field
      Bᴹ   : Set
      embᴹ : X → Bᴹ
      idᴹ  : Bᴹ
      _∘ᴹ_ : Bᴹ → Bᴹ → Bᴹ

  module RecB (m : MB) where
    open MB m
    rec : B → Bᴹ
    rec (emb x)  = embᴹ x
    rec id       = idᴹ
    rec (a ∘ a') = rec a ∘ᴹ rec a'

  data _~_ : B → B → Set where
    idl~   : ∀{b} → id ∘ b ~ b
    idr~   : ∀{b} → b ∘ id ~ b
    ass~   : ∀{b b' b''} → (b ∘ b') ∘ b'' ~ b ∘ (b' ∘ b'')
    cong~∘ : ∀{b b' b''} → b' ~ b'' → b ∘ b' ~ b ∘ b''
    cong~∘' : ∀{b b' b''} → b' ~ b'' → b' ∘ b ~ b'' ∘ b
  infix 3 _~_

  record M~ : Set₁ where
    field
      _~ᴹ_  : B → B → Set
      idl~ᴹ : ∀{b} → id ∘ b ~ᴹ b
      idr~ᴹ : ∀{b} → b ∘ id ~ᴹ b
      ass~ᴹ : ∀{b b' b''} → (b ∘ b') ∘ b'' ~ᴹ b ∘ (b' ∘ b'')
      cong~∘ᴹ : ∀{b b' b''} → b' ~ᴹ b'' → b ∘ b' ~ᴹ b ∘ b''
      cong~∘'ᴹ : ∀{b b' b''} → b' ~ᴹ b'' → b' ∘ b ~ᴹ b'' ∘ b
    infix 3 _~ᴹ_

  module Rec~ (m : M~) where
    open M~ m
    rec : ∀{b b'} → b ~ b' → b ~ᴹ b'
    rec idl~ = idl~ᴹ
    rec idr~ = idr~ᴹ
    rec ass~ = ass~ᴹ
    rec (cong~∘ p) = cong~∘ᴹ (rec p)
    rec (cong~∘' p) = cong~∘'ᴹ (rec p)

  data C : Set where
    [_] : B → C
  postulate
    [_]≡ : ∀{b b'} → b ~ b' → [ b ] ≡ [ b' ]

  record MC : Set₁ where
    field
      Cᴹ    : Set
      [_]ᴹ  : B → Cᴹ
      [_]≡ᴹ : ∀{b b'} → b ~ b' → [ b ]ᴹ ≡ [ b' ]ᴹ

  module RecC (m : MC) where
    open MC m
    rec : C → Cᴹ
    rec [ b ] = [ b ]ᴹ

  -- now we would like to reconstruct the constructors of the QIT

  embC : X → C
  embC x = [ emb x ]

  idC : C
  idC = [ id ]

  postulate
    cheat : ∀{A : Set} → A

  _∘C_ : C → C → C
  _∘C_ = RecC.rec (record
                     { Cᴹ    = C → C
                     ; [_]ᴹ  = λ b → RecC.rec (record
                                                 { Cᴹ    = C
                                                 ; [_]ᴹ  = λ b' → [ b ∘ b' ]
                                                 ; [_]≡ᴹ = λ {b'}{b''} p → [ cong~∘ p ]≡ })
                     ; [_]≡ᴹ = λ {b b'} p → cheat })
                       -- this can be filled in

module binary where

  -- binary branching trees where order of branches do not matter
  -- using quotient types
  
  data T₀ : Set where
    leaf : T₀
    node : T₀ → T₀ → T₀

  data _~_ : T₀ → T₀ → Set where
    leaf~ : leaf ~ leaf
    node~ : ∀{t t'}{t'' t'''} → t ~ t' → t'' ~ t''' → node t t'' ~ node t' t'''
    perm~ : ∀{t t'} → node t t' ~ node t' t

  refl~ : ∀ t → t ~ t
  refl~ leaf = leaf~
  refl~ (node t t') = node~ (refl~ t) (refl~ t')

  data T₁ : Set where
    [_]  : T₀ → T₁
  postulate
    [_]≡T : ∀ t t' → t ~ t' → _≡_ {A = T₁} [ t ] [ t' ]

  record MT₁ {i : Level} : Set (lsuc i) where
    field
      T₁ᴹ   : Set i
      [_]ᴹ  : T₀ → T₁ᴹ
      [_]≡ᴹ : ∀ t t' → t ~ t' → _≡_ {A = T₁ᴹ} [ t ]ᴹ [ t' ]ᴹ

  module RecT₁ {i}(m : MT₁ {i}) where
    open MT₁ m
    rec : T₁ → T₁ᴹ
    rec [ t₀ ] = [ t₀ ]ᴹ

  leaf₁ : T₁
  leaf₁ = [ leaf ]

  rec= : ∀{i}{T₁ᴹ : Set i}
         {[_]ᴹ₀ [_]ᴹ₁ : T₀ → T₁ᴹ}([_]ᴹ₂ : [_]ᴹ₀ ≡ [_]ᴹ₁)
         {[_]≡ᴹ₀ : ∀ t t' → t ~ t' → [ t ]ᴹ₀ ≡ [ t' ]ᴹ₀}
         {[_]≡ᴹ₁ : ∀ t t' → t ~ t' → [ t ]ᴹ₁ ≡ [ t' ]ᴹ₁}
         ([_]≡ᴹ₂ : _≡_ (transport
                         (λ z → ∀ t t' → t ~ t' → z t ≡ z t')
                         [_]ᴹ₂
                         [_]≡ᴹ₀
                       )
                       [_]≡ᴹ₁)
       → _≡_
           (RecT₁.rec (record { T₁ᴹ = T₁ᴹ ; [_]ᴹ = [_]ᴹ₀ ; [_]≡ᴹ = [_]≡ᴹ₀ }))
           (RecT₁.rec (record { T₁ᴹ = T₁ᴹ ; [_]ᴹ = [_]ᴹ₁ ; [_]≡ᴹ = [_]≡ᴹ₁ }))
  rec= refl refl = refl

  funext'' : ∀{i j k}{A : Set i}{B : A → A → Set j}{C : Set k}
             {D₀ D₁ : A → C}(D₂ : D₀ ≡ D₁)
             {f₀ : (x y : A) → B x y → D₀ x ≡ D₀ y}{f₁ : (x y : A) → B x y → D₁ x ≡ D₁ y}
             (f₂ : (x y : A)(z : B x y) → transport (λ D → D x ≡ D y) D₂ (f₀ x y z) ≡ f₁ x y z)
           → transport (λ D → (x y : A) → B x y → D x ≡ D y) D₂ f₀ ≡ f₁
  funext'' refl f₂ = funext λ x → funext λ y → funext λ z → f₂ x y z

  UIP : ∀{i}{A : Set i}{a a' : A}{p q : a ≡ a'} → p ≡ q
  UIP {p = refl} {q = refl} = refl

  node₁ : T₁ → T₁ → T₁
  node₁ = RecT₁.rec (record
    { T₁ᴹ = T₁ → T₁
    ; [_]ᴹ = λ t₀
             → RecT₁.rec
                 (record
                    { T₁ᴹ = T₁
                    ; [_]ᴹ = λ t₀' → [ node t₀ t₀' ]
                    ; [_]≡ᴹ = λ t₀' t₀'' p
                            → [_]≡T _ _ (node~ (refl~ t₀) p) 
                    })
    ; [_]≡ᴹ = λ t₀ t₀' p → rec= (funext (λ t₀'' → [_]≡T _ _ (node~ p (refl~ t₀''))))
                                (funext'' {A = T₀}{_~_}{T₁}
                                          {λ t₀'' → [ node t₀ t₀'' ]}{λ t₀'' → [ node t₀' t₀'' ]}
                                          (funext (λ t₀'' → [ node t₀ t₀'' ]≡T (node t₀' t₀'') (node~ p (refl~ t₀''))))
                                          {λ t₀'' t₀''' p₁ → [ node t₀ t₀'' ]≡T (node t₀ t₀''') (node~ (refl~ t₀) p₁)}
                                          {λ t₀'' t₀''' p₁ → [ node t₀' t₀'' ]≡T (node t₀' t₀''') (node~ (refl~ t₀') p₁)}
                                          (λ t₀' t₀'' p' → UIP))
    })

module tree_example where

  -- the same for infinitely branching trees

  data T₀ : Set where
    leaf : T₀
    node : (ℕ → T₀) → T₀

  postulate
    isIso : (ℕ → ℕ) → Set

  data _~T_ : T₀ → T₀ → Set where
    leaf : leaf ~T leaf
    node : ∀{f g} → (∀ n → f n ~T g n) → node f ~T node g
    perm : ∀{f g} → isIso g → node f ~T node (λ z → f (g z))

  data T₁ : Set where
    [_]  : T₀ → T₁
  postulate
    [_]≡T : ∀{t t'} → t ~T t' → _≡_ {A = T₁} [ t ] [ t' ]

  record MT₁ : Set₁ where
    field
      T₁ᴹ   : Set
      [_]ᴹ  : T₀ → T₁ᴹ
      [_]≡ᴹ : ∀{t t'} → t ~T t' → _≡_ {A = T₁ᴹ} [ t ]ᴹ [ t' ]ᴹ

  module RecT₁ (m : MT₁) where
    open MT₁ m
    rec : T₁ → T₁ᴹ
    rec [ t ] = [ t ]ᴹ

  leaf₁ : T₁
  leaf₁ = [ leaf ]

  postulate
    cheat : ∀{A : Set} → A

  node₁ : (ℕ → T₁) → T₁
  node₁ f₁ = [ node f₀ ]
    where
      f₀ : ℕ → T₀
      f₀ n = RecT₁.rec (record
                          { T₁ᴹ = T₀
                          ; [_]ᴹ = λ z → z
                          ; [_]≡ᴹ = λ {t t'} p → cheat
                          })
                       (f₁ n)

module funextFromInterval where

  data I : Set where
    zero one : I
  postulate
    seg : zero ≡ one

  RecI : {Iᴹ : Set}(zeroᴹ oneᴹ : Iᴹ) → zeroᴹ ≡ oneᴹ → I → Iᴹ
  RecI zeroᴹ oneᴹ q zero = zeroᴹ
  RecI zeroᴹ oneᴹ q one = oneᴹ

  -- we can turn an equality in A into a function I→A
  ≡2p : {A : Set}{x y : A} → x ≡ y → (I → A)
  ≡2p {A}{x}{y} p i = RecI {A} x y p i

  -- we can turn a function I→A into an equality
  p2≡ : {A : Set}{x y : A} → (p : I → A) → p zero ≡ p one
  p2≡ {A}{x}{y} p = ap p seg

  -- we can compose these two things to prove funext
  funext' : {X Y : Set}{f g : X → Y} → ((x : X) → f x ≡ g x) → f ≡ g
  funext' {X}{Y}{f}{g} φ = p2≡ {X → Y}{f}{g} H
    where
      H : I → (X → Y)
      H i x = ≡2p (φ x) i
