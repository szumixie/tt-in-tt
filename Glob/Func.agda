{-# OPTIONS --without-K --rewriting #-}

module Glob.Func where

open import lib

open import Glob.Decl
open import Glob.Core

-- internal indexed hom set

module excercise where
  _∶_⇒_ : (I : Set)(Γ Δ : I → Con) → Con
  ∣ I ∶ Γ ⇒ Δ ∣C = (i : I) → ∣ Γ i ∣C → ∣ Δ i ∣C 
  homC (I ∶ Γ ⇒ Δ) f f' =
    (Σ I λ i → ∣ Γ i ∣C × ∣ Γ i ∣C) ∶
               (λ { (i ,Σ (γ ,Σ γ')) → homC (Γ i) γ γ' }) ⇒ 
               (λ { (i ,Σ (γ ,Σ γ')) → homC (Δ i) (f i γ) (f' i γ) })

-- indexed Π

Πi : {Γ : Con}(I : Set)(A : I → Ty Γ)(B : (i : I) → Ty (Γ ▷ A i)) → Ty Γ
∣ Πi {Γ} I A B ∣T γ = (i : I) → (α : ∣ A i ∣T γ) → ∣ B i ∣T (γ ,Σ α)
homT (Πi {Γ} I A B) {γ}{γ'} f f' = Πi (Σ I λ i → ∣ A i ∣T γ × ∣ A i ∣T γ')
                                      (λ { (i ,Σ (α ,Σ α')) → homT (A i) α α' })
                                      (λ { (i ,Σ (α ,Σ α')) → homT (B i) (f i α) (f' i α') })

lami : {Γ : Con}{I : Set}{A : I → Ty Γ}{B : (i : I) → Ty (Γ ▷ A i)}
  (t : (i : I) → Tm (Γ ▷ A i) (B i)) → Tm Γ (Πi I A B)
∣ lami {Γ}{I}{A}{B} t ∣t γ i α = ∣ t i ∣t (γ ,Σ α)
homt (lami {Γ}{I}{A}{B} t) γ γ' = lami
  {homC Γ γ γ'}
  {Σ I (λ i → ∣ A i ∣T γ × ∣ A i ∣T γ')}
  {λ { (i ,Σ (α ,Σ α')) → homT (A i) α α' }}
  {λ { (i ,Σ (α ,Σ α')) → homT (B i) (∣ t i ∣t (γ ,Σ α)) (∣ t i ∣t (γ' ,Σ α')) }}
  (λ { (i ,Σ (α ,Σ α')) → homt (t i) (γ ,Σ α) (γ' ,Σ α') })

appi : {Γ : Con}{I : Set}{A : I → Ty Γ}{B : (i : I) → Ty (Γ ▷ A i)}
  (t : Tm Γ (Πi I A B)) → (i : I) → Tm (Γ ▷ A i) (B i)
∣ appi {Γ}{I}{A}{B} t i ∣t (γ ,Σ α) = ∣ t ∣t γ i α
homt (appi {Γ}{I}{A}{B} t i) (γ ,Σ a) (γ' ,Σ a') = appi {homC Γ γ γ'}{Σ I (λ i → ∣ A i ∣T γ × ∣ A i ∣T γ')}
                                                        {λ {(i ,Σ (a ,Σ a')) → homT (A i) a a'}}
                                                        {λ {(i ,Σ (a ,Σ a')) → homT (B i){γ ,Σ a}{γ' ,Σ a'} (∣ t ∣t γ i a) (∣ t ∣t γ' i a')}}
                                                        (homt t γ γ') (i ,Σ (a ,Σ a'))

Πβi' : {Γ : Con}{I : Set}{A : I → Ty Γ}{B : (i : I) → Ty (Γ ▷ A i)}
  (t : (i : I) → Tm (Γ ▷ A i) (B i)) → (i : I) → Tm= (appi (lami t) i) (t i)
∣ Πβi' {Γ} {I} {A} {B} t i ∣t= = refl
homt= (Πβi' {Γ} {I} {A} {B} t i) {γ ,Σ a}{γ' ,Σ a'} = Πβi' {homC Γ γ γ'}{Σ I (λ i → ∣ A i ∣T γ × ∣ A i ∣T γ')}
                                                           {λ {(i ,Σ (a ,Σ a')) → homT (A i) a a'}}
                                                           {λ {(i ,Σ (a ,Σ a')) → homT (B i){γ ,Σ a}{γ' ,Σ a'} (∣ t i ∣t (γ ,Σ a)) (∣ t i ∣t (γ' ,Σ a'))}}
                                                           (λ {(i ,Σ (a ,Σ a')) → homt (t i) (γ ,Σ a) (γ' ,Σ a')}) (i ,Σ (a ,Σ a'))

Πβi : {Γ : Con}{I : Set}{A : I → Ty Γ}{B : (i : I) → Ty (Γ ▷ A i)}
  (t : (i : I) → Tm (Γ ▷ A i) (B i)) → (i : I) → (appi (lami t) i) ≡ t i
Πβi {Γ}{I}{A}{B} t = λ i → Tm=ext (Πβi' t i)

-- real Π

Π : {Γ : Con}(A : Ty Γ)(B : Ty (Γ ▷ A)) → Ty Γ
Π {Γ} A B = Πi ⊤ (λ _ → A) λ _ → B

lam : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)} → Tm (Γ ▷ A) B → Tm Γ (Π A B)
lam {Γ}{A}{B} t = lami {I = ⊤} λ _ → t

app : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)} → Tm Γ (Π A B) → Tm (Γ ▷ A) B
app {Γ}{A}{B} t = appi {I = ⊤} t tt

Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{t : Tm (Γ ▷ A) B}
      → (app (lam t)) ≡ t
Πβ {Γ}{A}{B}{t} = Πβi {Γ}{⊤}{λ _ → A}{λ _ → B}(λ _ → t)tt


{-
Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
    → ((Π A B) [ σ ]T) ≡ (Π (A [ σ ]T) (B [ σ ^ A ]T))
    
lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B}                       
      → ((lam t) [ δ ]t) ≡[ TmΓ= Π[] ]≡ (lam (t [ δ ^ A ]t))

Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
      → (lam (app t)) ≡ t
-}
