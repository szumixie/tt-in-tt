{-# OPTIONS --cubical #-}

module Cubical.ToSet where

open import Cubical.Data.Unit
open import lib using (Lift; unlift; lift)
open import Cubical.Syntax
open import Cubical.Transp

{-# TERMINATING #-}
⟦_⟧I : Ix → Set₁
⟦_⟧  : {i : Ix} → ⟪ i ⟫ → ⟦ i ⟧I

⟦ Conᵢ     ⟧I = Set
⟦ Tyᵢ  Γ   ⟧I = ⟦ Γ ⟧ → Set
⟦ Tmsᵢ Γ Δ ⟧I = Lift (⟦ Γ ⟧ → ⟦ Δ ⟧)
⟦ Tmᵢ  Γ A ⟧I = Lift ((γ : ⟦ Γ ⟧) → ⟦ A ⟧ γ)

⟦ ∙        ⟧   = Unit
⟦ Γ ▷ A    ⟧   = Σ ⟦ Γ ⟧ ⟦ A ⟧
⟦ A [ σ ]T ⟧ γ = ⟦ A ⟧ (⟦ σ ⟧ .unlift γ)

⟦ id    ⟧ .unlift γ = γ
⟦ σ ∘ δ ⟧ .unlift γ = ⟦ σ ⟧ .unlift (⟦ δ ⟧ .unlift γ)
⟦ ε     ⟧ .unlift _ = tt
⟦ σ , t ⟧ .unlift γ = ⟦ σ ⟧ .unlift γ ,Σ ⟦ t ⟧ .unlift γ

⟦ π₁ σ     ⟧ .unlift γ = fst (⟦ σ ⟧ .unlift γ)
⟦ π₂ σ     ⟧ .unlift γ = snd (⟦ σ ⟧ .unlift γ)
⟦ t [ σ ]t ⟧ .unlift γ = ⟦ t ⟧ .unlift (⟦ σ ⟧ .unlift γ)

⟦ [id]T {A = A} i ⟧ = ⟦ A ⟧
⟦ [][]T {A = A}{σ = σ}{δ = δ} i ⟧ = ⟦ A [ σ ∘ δ ]T ⟧

⟦ ass {σ = σ}{δ = δ}{ν = ν} i ⟧ = ⟦ σ ∘ (δ ∘ ν) ⟧
⟦ idl {σ = σ} i ⟧ = ⟦ σ ⟧
⟦ idr {σ = σ} i ⟧ = ⟦ σ ⟧
⟦ ∙η  {σ = σ} i ⟧ = ⟦ σ ⟧

⟦ ▷β₁ {σ = σ} i ⟧ = ⟦ σ ⟧
⟦ ▷β₂ {Γ}{Δ}{A}{σ}{t} i ⟧ = p i where
--  p : ⟦ transp (λ i₁ → ⟪ Tmᵢ Γ (A [ ▷β₁ i₁ ]T) ⟫) i0 (π₂ (σ , t)) ⟧ ≡ ⟦ t ⟧
  p : ⟦ transp (λ i → ⟪ Tmᵢ Γ (A [ ▷β₁ {Γ}{Δ}{A}{σ}{t} i ]T) ⟫) i0 (π₂ {Γ}{Δ}{A}(σ , t)) ⟧ ≡ ⟦ t ⟧
  p = {!transpπ₂!}
⟦ ▷η  {σ = σ} i ⟧ = ⟦ σ ⟧
⟦ ,∘  i ⟧ = {!!}

⟦ Π A x ⟧ = {!!}
⟦ lam x ⟧ = {!!}
⟦ app x ⟧ = {!!}

⟦ Πβ  i ⟧ = {!!}
⟦ Πη  i ⟧ = {!!}
⟦ Π[] i ⟧ = {!!}

⟦ ⊤      ⟧ = {!!}
⟦ tt     ⟧ = {!!}
⟦ ⊤[] i  ⟧ = {!!}
⟦ tt[] i ⟧ = {!!}

⟦ ⊥           ⟧ = {!!}
⟦ exfalso   x ⟧ = {!!}
⟦ ⊥[]       i ⟧ = {!!}
⟦ exfalso[] i ⟧ = {!!}

⟦ U      ⟧ = {!!}
⟦ El   x ⟧ = {!!}
⟦ U[]  i ⟧ = {!!}
⟦ El[] i ⟧ = {!!}

⟦ setTy p q i i₁  ⟧ = {!!}
⟦ setTms p q i i₁ ⟧ = {!!}
⟦ setTm p q i i₁  ⟧ = {!!}

-- 1. define eval to Set (and leave some holes)
-- 2. define eval to hProp
-- 2a. prove isSet Con (simple)

-- open import Cubical.Foundations.HAEquiv using (isSetHProp)

-- 3. define eval to Bool
-- 4. INTERESTING: define eval to ℕ
-- 5. VERY INTERESTING: change the syntax so that we don't have to set-truncate,
--    but groupoid-truncate (we call this coherent syntax)
-- 6. then, you can define eval to hSet
-- 7. we can prove that Con,Ty,Tms,Tm is an hSet

module _
  {Γ Δ : Con}{A : Ty Δ}{σ : Tms Γ Δ}{a : Tm Γ (A [ σ ]T)}
  where

  p'   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{a : Tm Γ (A [ σ ]T)}
        → transp (λ i → ⟪ Tmᵢ Γ (A [ ▷β₁ {Γ}{Δ}{A}{σ}{a} i ]T) ⟫) i0 (π₂ {Γ}{Δ}{A}(σ , a)) ≡ a
  p' {Γ}{Δ}{A}{σ}{a} = ▷β₂

  p : Path (Lift ((γ : ⟦ Γ ⟧) → ⟦ A ⟧ (⟦ σ ⟧ .unlift γ)))
          ⟦ transp (λ i → ⟪ Tmᵢ Γ (A [ ▷β₁ {Γ}{Δ}{A}{σ}{a} i ]T) ⟫) i0 (π₂ {Γ}{Δ}{A}(σ , a)) ⟧
          (lift (transp (λ (i : I) → ((γ : ⟦ Γ ⟧) → ⟦ A [ ▷β₁ {Γ}{Δ}{A}{σ}{a} i ]T ⟧ γ)) i0 (⟦ π₂ {Γ}{Δ}{A}(σ , a) ⟧ .unlift)))
  p = {!transpπ₂!} ● {!!}
