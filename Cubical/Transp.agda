{-# OPTIONS --cubical #-}

module Cubical.Transp where

open import Cubical.Syntax

transp[]T : ∀{Γ₀ Γ₁}{Γ : Path Con Γ₀ Γ₁}{Δ : Con}{A : Ty Δ}
             {σ₀ σ₁}(σ : PathP (λ i → Tms (Γ i) Δ) σ₀ σ₁)
          → transport (λ i → Ty (Γ i)) (A [ σ₀ ]T) ≡ A [ σ₁ ]T
transp[]T {Γ₀}{Γ₁}{Γ}{Δ}{A}{σ₀}{σ₁} σ =
  J (λ Γ₁ Γ → ∀{σ₀ σ₁}(σ : PathP (λ i → Tms (Γ i) Δ) σ₀ σ₁)
            → transport (λ i → Ty (Γ i)) (A [ σ₀ ]T) ≡ A [ σ₁ ]T)
    (λ {σ₀}{σ₁} σ →
      J (λ σ₁ _ → transport (λ i → Ty Γ₀) (A [ σ₀ ]T) ≡ A [ σ₁ ]T)
        (transportRefl _) σ)
    Γ σ

transpid : ∀{Γ₀ Γ₁}{Γ : Path Con Γ₀ Γ₁} → transport (λ i → Tms (Γ i) (Γ i)) id ≡ id
transpid  = J (λ Γ₁ Γ → transport (λ i → Tms (Γ i) (Γ i)) id ≡ id) (transportRefl _) _

transp∘ : ∀{Γ₀ Γ₁}{Γ : Path Con Γ₀ Γ₁}{Δ₀ Δ₁}{Δ : Path Con Δ₀ Δ₁}{Θ : Con}
           {σ₀ σ₁}(σ : PathP (λ i → Tms Θ (Δ i)) σ₀ σ₁)
           {δ₀ δ₁}(δ : PathP (λ i → Tms (Γ i) Θ) δ₀ δ₁)
        → transport (λ i → Tms (Γ i) (Δ i)) (σ₀ ∘ δ₀) ≡ σ₁ ∘ δ₁
transp∘ {Γ₀}{Γ₁}{Γ}{Δ₀}{Δ₁}{Δ}{Θ}{σ₀}{σ₁} σ {δ₀}{δ₁} δ =
  J (λ Γ₁ Γ → ∀{δ₀ δ₁}(δ : PathP (λ i → Tms (Γ i) Θ) δ₀ δ₁)
            → transport (λ i → Tms (Γ i) (Δ i)) (σ₀ ∘ δ₀) ≡ σ₁ ∘ δ₁)
    (λ {δ₀}{δ₁} δ →
      J (λ Δ₁ Δ → ∀{σ₀ σ₁}(σ : PathP (λ i → Tms Θ (Δ i)) σ₀ σ₁)
                → transport (λ i → Tms Γ₀ (Δ i)) (σ₀ ∘ δ₀) ≡ σ₁ ∘ δ₁)
        (λ {σ₀}{σ₁} σ →
          J (λ σ₁ _ → transport (λ i → Tms Γ₀ Δ₀) (σ₀ ∘ δ₀) ≡ σ₁ ∘ δ₁)
            (J (λ δ₁ _ → transport (λ i → Tms Γ₀ Δ₀) (σ₀ ∘ δ₀) ≡ σ₀ ∘ δ₁)
               (transportRefl _) δ)
            σ)
        Δ σ)
    Γ δ

transpε : ∀{Γ₀ Γ₁}{Γ : Path Con Γ₀ Γ₁} → transport (λ i → Tms (Γ i) ∙) ε ≡ ε
transpε = J (λ Γ₁ Γ → transport (λ i → Tms (Γ i) ∙) ε ≡ ε) (transportRefl _) _

transp, : ∀{Γ₀ Γ₁}{Γ : Path Con Γ₀ Γ₁}{Δ₀ Δ₁}{Δ : Path Con Δ₀ Δ₁}
           {σ₀ σ₁}(σ : PathP (λ i → Tms (Γ i) (Δ i)) σ₀ σ₁)
           {A₀ A₁}{A : PathP (λ i → Ty (Δ i)) A₀ A₁}
           {t₀ t₁}(t : PathP (λ i → Tm (Γ i) (A i [ σ i ]T)) t₀ t₁)
        → transport (λ i → Tms (Γ i) (Δ i ▷ A i)) (σ₀ , t₀) ≡ σ₁ , t₁
transp, {Γ₀}{Γ₁}{Γ}{Δ₀}{Δ₁}{Δ}{σ₀}{σ₁} σ {A₀}{A₁}{A}{t₀}{t₁} t =
  J (λ Γ₁ Γ → ∀{σ₀ σ₁}(σ : PathP (λ i → Tms (Γ i) (Δ i)) σ₀ σ₁)
               {t₀ t₁}(t : PathP (λ i → Tm (Γ i) (A i [ σ i ]T)) t₀ t₁)
            → transport (λ i → Tms (Γ i) (Δ i ▷ A i)) (σ₀ , t₀) ≡ σ₁ , t₁)
    (λ {σ₀}{σ₁} σ {t₀}{t₁} t →
      J (λ Δ₁ Δ → ∀{σ₀ σ₁}(σ : PathP (λ i → Tms Γ₀ (Δ i)) σ₀ σ₁)
                   {A₀ A₁}(A : PathP (λ i → Ty (Δ i)) A₀ A₁)
                   {t₀ t₁}(t : PathP (λ i → Tm Γ₀ (A i [ σ i ]T)) t₀ t₁)
                → transport (λ i → Tms Γ₀ (Δ i ▷ A i)) (σ₀ , t₀) ≡ σ₁ , t₁)
        (λ {σ₀}{σ₁} σ {A₀}{A₁} A {t₀}{t₁} t →
          J (λ σ₁ σ → ∀{t₀ t₁}(t : PathP (λ i → Tm Γ₀ (A i [ σ i ]T)) t₀ t₁)
                    → transport (λ i → Tms Γ₀ (Δ₀ ▷ A i)) (σ₀ , t₀) ≡ σ₁ , t₁)
            (λ {t₀}{t₁} t →
              J (λ A₁ A → ∀{t₀ t₁}(t : PathP (λ i → Tm (Γ i0) (A i [ σ₀ ]T)) t₀ t₁)
                        → transport (λ i → Tms Γ₀ (Δ₀ ▷ A i)) (σ₀ , t₀) ≡ σ₀ , t₁)
                (λ {t₀}{t₁} t →
                  J (λ t₁ t → transport (λ i → Tms Γ₀ (Δ₀ ▷ A₀)) (σ₀ , t₀) ≡ σ₀ , t₁)
                    (transportRefl _) t)
                A t)
            σ t)
        Δ σ A t)
    Γ σ t

transpπ₁ : ∀{Γ₀ Γ₁}{Γ : Path Con Γ₀ Γ₁}{Δ₀ Δ₁}{Δ : Path Con Δ₀ Δ₁}
            {A₀ A₁}{A : PathP (λ i → Ty (Δ i)) A₀ A₁}
            {σ₀ σ₁}(σ : PathP (λ i → Tms (Γ i) (Δ i ▷ A i)) σ₀ σ₁)
         → transport (λ i → Tms (Γ i) (Δ i)) (π₁ σ₀) ≡ π₁ σ₁
transpπ₁ {Γ₀}{Γ₁}{Γ}{Δ₀}{Δ₁}{Δ}{A₀}{A₁}{A}{σ₀}{σ₁} σ = 
  J (λ Γ₁ Γ → ∀{σ₀ σ₁}(σ : PathP (λ i → Tms (Γ i) (Δ i ▷ A i)) σ₀ σ₁)
            → transport (λ i → Tms (Γ i) (Δ i)) (π₁ σ₀) ≡ π₁ σ₁)
    (λ {σ₀}{σ₁} σ →
      J (λ Δ₁ Δ → ∀{A₀ A₁}(A : PathP (λ i → Ty (Δ i)) A₀ A₁)
                   {σ₀ σ₁}(σ : PathP (λ i → Tms Γ₀ (Δ i ▷ A i)) σ₀ σ₁)
                → transport (λ i → Tms Γ₀ (Δ i)) (π₁ σ₀) ≡ π₁ σ₁)
        (λ {A₀}{A₁} A {σ₀}{σ₁} σ →
          J (λ A₁ A → ∀{σ₀ σ₁}(σ : PathP (λ i → Tms Γ₀ (Δ₀ ▷ A i)) σ₀ σ₁)
                    → transport (λ i → Tms Γ₀ Δ₀) (π₁ σ₀) ≡ π₁ σ₁)
            (λ {σ₀}{σ₁} σ →
              J (λ σ₁ σ → transport (λ i → Tms Γ₀ Δ₀) (π₁ σ₀) ≡ π₁ σ₁)
                (transportRefl _) σ)
            A σ)
        Δ A σ)
    Γ σ

transpπ₂ : ∀{Γ₀ Γ₁}{Γ : Path Con Γ₀ Γ₁}{Δ : Con}{A₀ A₁}{A : Path (Ty Δ) A₀ A₁}
            {σ₀ σ₁}(σ : PathP (λ i → Tms (Γ i) (Δ ▷ A i)) σ₀ σ₁)
         → transport (λ i → Tm (Γ i) (A i [ π₁ (σ i) ]T)) (π₂ σ₀) ≡ π₂ σ₁
transpπ₂ {Γ₀}{Γ₁}{Γ}{Δ}{A₀}{A₁}{A}{σ₀}{σ₁} σ = 
  J (λ Γ₁ Γ → ∀{σ₀ σ₁}(σ : PathP (λ i → Tms (Γ i) (Δ ▷ A i)) σ₀ σ₁)
            → transport (λ i → Tm (Γ i) (A i [ π₁ (σ i) ]T)) (π₂ σ₀) ≡ π₂ σ₁)
    (λ {σ₀}{σ₁} σ →
      J (λ A₁ A → ∀{σ₀ σ₁}(σ : PathP (λ i → Tms Γ₀ (Δ ▷ A i)) σ₀ σ₁)
                 → transport (λ i → Tm Γ₀ (A i [ π₁ (σ i) ]T)) (π₂ σ₀) ≡ π₂ σ₁)
        (λ {σ₀}{σ₁} σ →
          J (λ σ₁ σ → transport (λ i → Tm Γ₀ (A₀ [ π₁ (σ i) ]T)) (π₂ σ₀) ≡ π₂ σ₁)
            (transportRefl _) σ)
        A σ)
    Γ σ

transp[]t : ∀{Γ₀ Γ₁}{Γ : Path Con Γ₀ Γ₁}{Δ : Con}{A₀ A₁}{A : Path (Ty Δ) A₀ A₁}
             {t₀ t₁}(t : PathP (λ i → Tm Δ (A i)) t₀ t₁)
             {σ₀ σ₁}(σ : PathP (λ i → Tms (Γ i) Δ) σ₀ σ₁)
          → transport (λ i → Tm (Γ i) (A i [ σ i ]T)) (t₀ [ σ₀ ]t) ≡ t₁ [ σ₁ ]t
transp[]t {Γ₀}{Γ₁}{Γ}{Δ}{A₀}{A₁}{A}{t₀}{t₁} t {σ₀}{σ₁} σ = 
  J (λ Γ₁ Γ → ∀{σ₀ σ₁}(σ : PathP (λ i → Tms (Γ i) Δ) σ₀ σ₁)
            → transport (λ i → Tm (Γ i) (A i [ σ i ]T)) (t₀ [ σ₀ ]t) ≡ t₁ [ σ₁ ]t)
    (λ {σ₀}{σ₁} σ →
      J (λ A₁ A → ∀{t₀ t₁}(t : PathP (λ i → Tm Δ (A i)) t₀ t₁)
                → transport (λ i → Tm Γ₀ (A i [ σ i ]T)) (t₀ [ σ₀ ]t) ≡ t₁ [ σ₁ ]t)
        (λ {t₀}{t₁} t →
          J (λ t₁ t → transport (λ i → Tm Γ₀ (A₀ [ σ i ]T)) (t₀ [ σ₀ ]t) ≡ t₁ [ σ₁ ]t)
            (J (λ σ₁ σ → transport (λ i → Tm Γ₀ (A₀ [ σ i ]T)) (t₀ [ σ₀ ]t) ≡ t₀ [ σ₁ ]t)
               (transportRefl _) σ)
            t)
        A t)
    Γ σ

{-
  transp▷ : {Γ : Con}{A : Ty Γ} → transp (λ i → ⟪ Conᵢ ⟫) i0 (Γ ▷ A) ≡ Γ ▷ A
  transp▷ {Γ}{A} = transportRefl _

  transpid : {Γ : I → Con} → transp (λ i → ⟪ Tmsᵢ (Γ i) (Γ i) ⟫) i0 id ≡ id
  transpid {Γ}  = J (λ y e → transp (λ i → ⟪ Tmsᵢ (e i) (e i) ⟫) i0 id ≡ id) (transportRefl _) (λ i → Γ i)

  transpε : {Γ : I → Con} → transp (λ i → ⟪ Tmsᵢ (Γ i) ∙ ⟫) i0 ε ≡ ε
  transpε {Γ}  = J (λ y e → transp (λ i → ⟪ Tmsᵢ (e i) ∙ ⟫) i0 ε ≡ ε) (transportRefl _) (λ i → Γ i)

  transp∘ : {Γ Δ : I → Con}{Θ : Con}{σ : (i : I) → Tms Θ (Δ i)}{δ : (i : I) → Tms (Γ i) Θ} →
    transp (λ i → ⟪ Tmsᵢ (Γ i) (Δ i) ⟫) i0 (σ i0 ∘ δ i0) ≡ (σ i1 ∘ δ i1)
  transp∘ {Γ}{Δ}{Θ}{σ}{δ}  = {!!}

  transp[]T : {Γ : I → Con}{Δ : Con}{A : Ty Δ}(σ : (i : I) → Tms (Γ i) Δ) →
    transp (λ i → ⟪ Tyᵢ (Γ i) ⟫) i0 (A [ σ i0 ]T) ≡ A [ σ i1 ]T
  transp[]T {Γ}{Δ}{A} σ =
     J (λ y e → {σ₀ : Tms (e i0) Δ}{σ₁ : Tms (e i1) Δ}(σ₀₁ : PathP (λ i → Tms (e i) Δ) σ₀ σ₁) → transp (λ i → ⟪ Tyᵢ (e i) ⟫) i0 (A [ σ₀ ]T) ≡ A [ σ₁ ]T)
       (λ {σ₀}{σ₁} σ₀₁ → J {x = σ₀} (λ σ₁ _ → transp (λ i → ⟪ Tyᵢ (refl i) ⟫) i0 (A [ σ₀ ]T) ≡ A [ σ₁ ]T) (transportRefl _) σ₀₁)
       (λ i → Γ i)
       (λ i → σ i)
-}

-- (σ : (i : I) → Tms (Γ i) Δ)
-- σ i1 : Tms (Γ i1) Δ
-- 
    
--      (λ σ' → {!!})
--      (λ i → Γ i)
--      (σ i1) -- should be provable using J first on Γ then σ
{-
  q : {Γ Δ : I → Con}{A : (i : I) → Ty (Δ i)}(σ : (i : I) → Tms (Γ i) (Δ i ▷ A i)) →
    transp (λ i → ⟪ Tmᵢ (Γ i) ((A i) [ π₁ (σ i) ]T) ⟫) i0 (π₂ {Γ i0}{Δ i0}{A i0} (σ i0)) ≡ π₂ {Γ i1}{Δ i1}{A i1}(σ i1)
  q {Γ}{Δ}{A} σ = {!!}
-}
-- transp (λ i → ⟪ Tmᵢ (Γ i) (A i [ π₁ (σ i) ]T) ⟫) i0 (π₂ (σ i0)) : ⟪ Tmᵢ (Γ i1) (A i1 [ π₁ (σ i1) ]T) ⟫
-- π₂ (σ i1) : ⟪ Tmᵢ (Γ i1) (A i1 [ π₁ (σ i1) ]T) ⟫

  --  f (transp (λ i → ⟪ Tmᵢ Γ (A i) ⟫) i0 t) ≡ transp (λ i → f ⟪ Tmᵢ Γ A ⟫) i0 (f t)

--           (transp (λ i → Lift ((γ : ⟦ Γ ⟧) → ⟦ A [ ▷β₁ {Γ}{Δ}{A}{σ}{a} i ]T ⟧ γ)) i0 ⟦ π₂ {Γ}{Δ}{A}(σ , a) ⟧)

