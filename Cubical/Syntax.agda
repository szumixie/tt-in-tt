{-# OPTIONS --cubical #-}

module Cubical.Syntax where

open import Cubical.Foundations.Prelude renaming (_,_ to _,Σ_; _∙_ to _●_) public

infixl 8 _[_]t
infixl 7 _[_]T
infix  6 _∘_
infixl 5 _▷_ _,_

data Ix : Set
data ⟪_⟫ : Ix → Set

Con : Set
Ty  : Con → Set
Tms : Con → Con → Set
Tm  : (Γ : Con) → Ty Γ → Set

data Ix where
  Conᵢ : Ix
  Tyᵢ  : Con → Ix
  Tmsᵢ : Con → Con → Ix
  Tmᵢ  : (Γ : Con) → Ty Γ → Ix

Con     = ⟪ Conᵢ ⟫
Ty  Γ   = ⟪ Tyᵢ Γ ⟫
Tms Γ Δ = ⟪ Tmsᵢ Γ Δ ⟫
Tm  Γ Δ = ⟪ Tmᵢ Γ Δ ⟫

data ⟪_⟫ where
  ∙     : Con
  _▷_   : (Γ : Con) → Ty Γ → Con
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Θ} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
  ε     : ∀{Γ} → Tms Γ ∙
  _,_   : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)

  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▷ A) → Tms Γ Δ
  π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ σ ]T)
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [][]T : ∀{Γ Δ Θ}{A : Ty Θ}{σ : Tms Δ Θ}{δ : Tms Γ Δ}
        → A [ σ ]T [ δ ]T ≡ A [ σ ∘ δ ]T

  ass   : ∀{Γ Δ Θ Ω}{σ : Tms Θ Ω}{δ : Tms Δ Θ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
  idl   : ∀{Γ Δ}{σ : Tms Γ Δ} → id ∘ σ ≡ σ
  idr   : ∀{Γ Δ}{σ : Tms Γ Δ} → σ ∘ id ≡ σ
  ∙η    : ∀{Γ}{σ : Tms Γ ∙} → σ ≡ ε

  ▷β₁   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → π₁ (σ , t) ≡ σ
  ▷β₂   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → subst (Tm _) (cong (A [_]T) ▷β₁) (π₂ (σ , t)) ≡ t
  ▷η    : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ ▷ A)}
        → π₁ σ , π₂ σ ≡ σ
  ,∘    : ∀{Γ Δ Θ}{σ : Tms Γ Δ}{δ : Tms Θ Γ}{A : Ty Δ}{t : Tm Γ (A [ σ ]T)}
        → (σ , t) ∘ δ ≡ σ ∘ δ , subst (Tm _) [][]T (t [ δ ]t)

  Π     : ∀{Γ}(A : Ty Γ) → Ty (Γ ▷ A) → Ty Γ
  lam   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)} → Tm (Γ ▷ A) B → Tm Γ (Π A B)
  app   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)} → Tm Γ (Π A B) → Tm (Γ ▷ A) B

  Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{t : Tm (Γ ▷ A) B}
        → app (lam t) ≡ t
  Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{t : Tm Γ (Π A B)}
        → lam (app t) ≡ t
  Π[]   : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▷ A)}
        → Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ∘ π₁ id , subst (Tm _) [][]T (π₂ id) ]T)
  --lam[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▷ A)}{t : Tm (Δ ▷ A) B}
  --      → subst (Tm _) Π[] (lam t [ σ ]t) ≡ lam (t [ σ ∘ π₁ id , subst (Tm _) [][]T (π₂ id) ]t)

  ⊤     : ∀{Γ} → Ty Γ
  tt    : ∀{Γ} → Tm Γ ⊤
  ⊤[]   : ∀{Γ Δ}{σ : Tms Γ Δ} → ⊤ [ σ ]T ≡ ⊤
  tt[]  : ∀{Γ Δ}{σ : Tms Γ Δ} → subst (Tm _) ⊤[] (tt [ σ ]t) ≡ tt

  ⊥     : ∀{Γ} → Ty Γ
  exfalso   : ∀{Γ}{C : Ty Γ} → Tm Γ ⊥ → Tm Γ C
  ⊥[]   : ∀{Γ Δ}{σ : Tms Γ Δ} → ⊥ [ σ ]T ≡ ⊥
  exfalso[] : ∀{Γ Δ}{σ : Tms Γ Δ}{C : Ty Δ}{t : Tm Δ ⊥}
            → subst (Tm _) ⊥[] (exfalso t [ σ ]t) ≡ exfalso (subst (Tm _) ⊥[] (t [ σ ]t))

  U     : ∀{Γ} → Ty Γ
  El    : ∀{Γ} → Tm Γ U → Ty Γ
  U[]   : ∀{Γ Δ}{σ : Tms Γ Δ} → U [ σ ]T ≡ U
  El[]  : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
          → El a [ σ ]T ≡ El (subst (Tm _) U[] (a [ σ ]t))

  -- truncation
  -- without these 3 constructors it is called wild syntax
  setTy  : ∀{Γ}{A₀ A₁ : Ty Γ}(p q : A₀ ≡ A₁) → p ≡ q
  setTms : ∀{Γ Δ}{σ₀ σ₁ : Tms Γ Δ}(p q : σ₀ ≡ σ₁) → p ≡ q
  setTm  : ∀{Γ A}{t₀ t₁ : Tm Γ A}(p q : t₀ ≡ t₁) → p ≡ q

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▷ A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▷ A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ ▷ B) (A [ wk ]T)
vs = _[ wk ]t

infix 4 ⟨_⟩

⟨_⟩ : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▷ A)
⟨ t ⟩ = id , subst (Tm _) (sym [id]T) t

infixl 5 _↑_

_↑_ : ∀{Γ Δ}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▷ (A [ σ ]T)) (Δ ▷ A)
σ ↑ _ = σ ∘ wk , subst (Tm _) [][]T vz
