{-# OPTIONS --without-K #-}

open import Agda.Primitive
open import PShNew.Cats

module PShNew.TwoLevels.Core (C : Cat {lzero}{lzero}) where

open Cat C

open import lib

open import PShNew.TwoLevels.Decl C

• : ∀{ℓ} → Con {ℓ}
• = record
  { ∣_∣C    = λ Ψ → Lift ⊤
  ; isSetC  = λ { refl refl → refl }
  ; _∶_⟦_⟧C = λ _ _ → lift tt
  ; idC     = refl
  ; ∘C      = refl
  }

_,_ : ∀{ℓ ℓ'}(Γ : Con {ℓ})(A : Ty {ℓ}{ℓ'} Γ) → Con {ℓ ⊔ ℓ'}
Γ , A = record
  { ∣_∣C    = λ Ψ → Σ (∣ Γ ∣C Ψ) (∣ A ∣T {Ψ})
  ; isSetC  = ΣisSet (isSetC Γ) (isSetT A)
  ; _∶_⟦_⟧C = λ { (α ,Σ a) β → Γ ∶ α ⟦ β ⟧C ,Σ A ∶ a ⟦ β ⟧T }
  ; idC     = ,Σ= (idC Γ) (idT A)
  ; ∘C      = ,Σ= (∘C Γ) (∘T A)
  }

infixl 5 _,_

_[_]T : ∀{ℓ ℓ' ℓ''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}(A : Ty {ℓ'}{ℓ''} Δ)(σ : Tms Γ Δ) → Ty Γ
_[_]T {Γ = Γ}{Δ} A σ = record
  { ∣_∣T    = λ α → ∣ A ∣T (∣ σ ∣s α)
  ; isSetT  = isSetT A
  ; _∶_⟦_⟧T = λ a β → coe (ap ∣ A ∣T (nats σ)) (A ∶ a ⟦ β ⟧T)
  ; idT     = λ {Ψ}{α}{a}
              → coe= (apap {f = ∣ σ ∣s}{∣ A ∣T}(idC Γ))
                ◾ coecoeap (nats σ)(ap ∣ σ ∣s (idC Γ))
                ◾ coe= (ap (ap ∣ A ∣T) (isSetC Δ (nats σ ◾ ap ∣ σ ∣s (idC Γ))(idC Δ)))
                ◾ idT A {Ψ}{∣ σ ∣s α}{a}
  ; ∘T      = λ {Ψ}{α}{a}{Ω}{β}{Ξ}{γ}
              → coe= (apap {f = ∣ σ ∣s}{∣ A ∣T} (∘C Γ))
              ◾ coecoeap (nats σ)(ap ∣ σ ∣s (∘C Γ))
              ◾ coe= (ap (ap ∣ A ∣T) (isSetC Δ (nats σ ◾ ap ∣ σ ∣s (∘C Γ))(∘C Δ ◾ (ap (λ z → Δ ∶ z ⟦ γ ⟧C) (nats σ) ◾ nats σ))))
              ◾ coecoeap (∘C Δ)(ap (λ z → Δ ∶ z ⟦ γ ⟧C) (nats σ) ◾ nats σ) ⁻¹
              ◾ ap (coe (ap ∣ A ∣T (ap (λ z → Δ ∶ z ⟦ γ ⟧C) (nats σ) ◾ nats σ))) (∘T A)
              ◾ coecoeap {B = ∣ A ∣T}(ap (λ z → Δ ∶ z ⟦ γ ⟧C) (nats σ))(nats σ){A ∶ A ∶ a ⟦ β ⟧T ⟦ γ ⟧T} ⁻¹
              ◾ ap (coe (ap ∣ A ∣T (nats σ))) (coe⟦⟧ A (nats σ){A ∶ a ⟦ β ⟧T}{_}{γ}) ⁻¹
  }

infixl 7 _[_]T

id : ∀{ℓ}{Γ : Con {ℓ}} → Tms Γ Γ
id = record
  { ∣_∣s = λ α → α
  ; nats = refl
  }

_∘_ : ∀{ℓ ℓ' ℓ''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{Σ : Con {ℓ''}}(σ : Tms Δ Σ)(ν : Tms Γ Δ)→ Tms Γ Σ
σ ∘ ν = record
  { ∣_∣s = λ α → ∣ σ ∣s (∣ ν ∣s α)
  ; nats = nats σ ◾ ap ∣ σ ∣s (nats ν)
  }

infix  6 _∘_

ε : ∀{ℓ ℓ'}{Γ : Con {ℓ}} → Tms {ℓ}{ℓ'} Γ •
ε = record
  { ∣_∣s = λ _ → lift tt
  ; nats = refl
  }

_,s_ : ∀{ℓ ℓ' ℓ''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}(σ : Tms Γ Δ){A : Ty {ℓ'}{ℓ''} Δ}(t : Tm Γ (A [ σ ]T)) → Tms Γ (Δ , A)
σ ,s t = record
  { ∣_∣s = λ α → ∣ σ ∣s α ,Σ ∣ t ∣t α
  ; nats = ,Σ= (nats σ) (natt t)
  }

infixl 5 _,s_

π₁ : ∀{ℓ ℓ' ℓ''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{A : Ty {ℓ'}{ℓ''} Δ}(σ : Tms Γ (Δ , A)) → Tms Γ Δ
π₁ σ = record
  { ∣_∣s = λ α → proj₁ (∣ σ ∣s α)
  ; nats = ap proj₁ (nats σ)
  }

_[_]t : ∀{ℓ ℓ' ℓ''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{A : Ty {ℓ'}{ℓ''} Δ}(t : Tm Δ A)(σ : Tms Γ Δ)
      → Tm Γ (A [ σ ]T)
_[_]t {Γ = Γ}{Δ}{A} t σ = record
  { ∣_∣t = λ α → ∣ t ∣t (∣ σ ∣s α)
  ; natt = ap (coe (ap ∣ A ∣T (nats σ))) (natt t) ◾ apd ∣ t ∣t (nats σ)
  }

π₂ : ∀{ℓ ℓ' ℓ''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{A : Ty {ℓ'}{ℓ''} Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ {A = A} σ = record
  { ∣_∣t = λ α → proj₂ (∣ σ ∣s α)
  ; natt = coe= (apap {f = proj₁}{∣ A ∣T}(nats σ) ⁻¹) ◾ apd proj₂ (nats σ)
  }

infixl 8 _[_]t

[id]T : ∀{ℓ ℓ'}{Γ : Con {ℓ}}{A : Ty {ℓ}{ℓ'} Γ} → A [ id ]T ≡ A
[id]T = =Ty' (λ _ → refl) (λ { refl _ → refl })

module _
  {ℓ ℓ' ℓ'' ℓ'''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{Σ : Con {ℓ''}}{A : Ty {ℓ''}{ℓ'''} Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
  where

  [][]Ta : {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ∣ A [ δ ]T [ σ ]T ∣T α ≡ ∣ A [ δ ∘ σ ]T ∣T α
  [][]Ta _ = refl

  [][]Tb : {Ψ : Obj}{α : ∣ Γ ∣C Ψ}
           {a₀ : ∣ A ∣T (∣ δ ∣s (∣ σ ∣s α))}{a₁ : ∣ A ∣T (∣ δ ∘ σ ∣s α)}(a₂ : a₀ ≡[ [][]Ta α ]≡ a₁)
           {Ω : Obj}(β : Ω ⇒ Ψ)
         → coe (ap ∣ A [ δ ]T ∣T (nats σ)) (A [ δ ]T ∶ a₀ ⟦ β ⟧T)
         ≡[ [][]Ta (Γ ∶ α ⟦ β ⟧C) ]≡
           coe (ap ∣ A ∣T (nats (δ ∘ σ))) (A ∶ a₁ ⟦ β ⟧T)
  [][]Tb {Ψ}{α}{a} refl {Ω} β
    = ap (λ z → coe z (coe (ap ∣ A ∣T (nats δ)) (A ∶ a ⟦ β ⟧T)))
         (apap {f = ∣ δ ∣s}{∣ A ∣T} (nats σ))
    ◾ coecoeap (nats δ)(ap ∣ δ ∣s (nats σ))
  
  [][]T : A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T
  [][]T = =Ty' [][]Ta [][]Tb

idl : ∀{ℓ ℓ'}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{δ : Tms Γ Δ} → id ∘ δ ≡ δ
idl = =Tms' λ _ → refl

idr : ∀{ℓ ℓ'}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{δ : Tms Γ Δ} → δ ∘ id ≡ δ
idr = =Tms' λ _ → refl

ass : ∀{ℓ ℓ' ℓ'' ℓ'''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{Σ : Con {ℓ''}}{Ω : Con {ℓ'''}}
      {σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = =Tms' λ _ → refl

coe∣∣t : ∀{ℓ ℓ'}{Γ : Con}{A₀ A₁ : Ty {ℓ}{ℓ'} Γ}(A₂ : A₀ ≡ A₁){t : Tm Γ A₀}{Ψ : Obj}{α : ∣ Γ ∣C Ψ}
       → ∣ coe (ap (Tm _) A₂) t ∣t α ≡ coe (ap (λ z → ∣ z ∣T α) A₂) (∣ t ∣t α)
coe∣∣t refl = refl

,∘ : ∀{ℓ ℓ' ℓ'' ℓ'''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{Σ : Con {ℓ''}}
     {δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty {ℓ'}{ℓ'''} Δ}{a : Tm Γ (A [ δ ]T)}
   → (_,s_ {Γ = Γ}{Δ} δ {A} a) ∘ σ ≡ _,s_ (δ ∘ σ) {A} (coe (ap (Tm _) ([][]T {A = A}{σ}{δ})) (a [ σ ]t))
,∘ {Γ = Γ}{Δ}{Σ}{δ}{σ}{A}{a}
  = =Tms' (λ {Ψ} α → ,Σ= refl
                         ( coe= (∣∣=Ty' ([][]Ta {A = A}{σ}{δ}) ([][]Tb {A = A}{σ}{δ}) α) ⁻¹
                         ◾ coe∣∣t ([][]T {A = A}{σ}{δ}) ⁻¹))

π₁β : ∀{ℓ ℓ' ℓ''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{A : Ty {ℓ'}{ℓ''} Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
    → π₁ {A = A}(_,s_ δ {A} a) ≡ δ
π₁β = =Tms' λ _ → refl

πη : ∀{ℓ ℓ' ℓ''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{A : Ty {ℓ'}{ℓ''} Δ}{δ : Tms Γ (Δ , A)} → _,s_ (π₁ {A = A} δ) {A} (π₂ {A = A} δ) ≡ δ
πη = =Tms' λ _ → refl

εη : ∀{ℓ ℓ'}{Γ : Con {ℓ}}{σ : Tms {ℓ}{ℓ'} Γ •} → σ ≡ ε
εη = =Tms' λ _ → refl

∣∣[]T : ∀{ℓ ℓ' ℓ''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{A : Ty {ℓ'}{ℓ''} Δ}
        {δ₀ : Tms Γ Δ}{δ₁ : Tms Γ Δ}(δ₂ : δ₀ ≡ δ₁){Ψ : Obj}{α : ∣ Γ ∣C Ψ}
      → ap (λ z → ∣ z ∣T α) (ap (λ z → A [ z ]T) δ₂) ≡ ap (λ z → ∣ A ∣T (∣ z ∣s α)) δ₂
∣∣[]T refl = refl

π₂β : ∀{ℓ ℓ' ℓ''}{Γ : Con {ℓ}}{Δ : Con {ℓ'}}{A : Ty {ℓ'}{ℓ''} Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
    → π₂ {A = A}(_,s_ δ {A} a)
    ≡[ ap (Tm _) (ap (λ z → A [ z ]T) (π₁β {Γ = Γ}{Δ}{A}{δ}{a})) ]≡
      a
π₂β {Γ = Γ}{Δ}{A}{δ}{a}
  = =Tm' λ {Ψ} α → coe∣∣t (ap (λ z → A [ z ]T) (π₁β {Γ = Γ}{Δ}{A}{δ}{a}))
                 ◾ ap (λ z → coe z (∣ a ∣t α))
                       (∣∣[]T {A = A} (π₁β {Γ = Γ}{Δ}{A}{δ}{a}){Ψ}{α})
                 ◾ ap (λ z → coe z (∣ a ∣t α)) (apap {f = λ z → ∣ z ∣s α}{∣ A ∣T}(π₁β {Γ = Γ}{Δ}{A}{δ}{a}))
                 ◾ ap (λ z → coe (ap ∣ A ∣T z) (∣ a ∣t α)) (∣∣=Tms' (λ _ → refl) α)

open import TT.TwoLevels.Decl
open import TT.TwoLevels.Core

c : Core d
c = record
  { •      = •
  ; _,₀_   = _,_
  ; _,₁_   = _,_
  ; _[_]T₀ = _[_]T
  ; _[_]T₁ = _[_]T
  ; id     = id
  ; _∘_    = _∘_
  ; ε      = ε
  ; _,s₀_  = _,s_
  ; _,s₁_  = _,s_
  ; π₁₀    = λ {Γ}{Δ}{A} → π₁ {Γ = Γ}{Δ}{A}
  ; π₁₁    = λ {Γ}{Δ}{A} → π₁ {Γ = Γ}{Δ}{A}
  ; _[_]t₀ = _[_]t
  ; _[_]t₁ = _[_]t
  ; π₂₀    = λ {Γ}{Δ}{A} → π₂ {Γ = Γ}{Δ}{A}
  ; π₂₁    = λ {Γ}{Δ}{A} → π₂ {Γ = Γ}{Δ}{A}
  ; [id]T₀ = [id]T
  ; [id]T₁ = [id]T
  ; [][]T₀ = λ {Γ}{Δ}{Σ}{A}{σ}{δ} → [][]T {Γ = Γ}{Δ}{Σ}{A}{σ}{δ}
  ; [][]T₁ = λ {Γ}{Δ}{Σ}{A}{σ}{δ} → [][]T {Γ = Γ}{Δ}{Σ}{A}{σ}{δ}
  ; idl    = idl
  ; idr    = idr
  ; ass    = λ {Γ}{Δ}{Σ}{Ω}{σ}{δ}{ν} → ass {Γ = Γ}{Δ}{Σ}{Ω}{σ}{δ}{ν}
  ; ,∘₀    = λ {Γ}{Δ}{Σ}{δ}{σ}{A}{a} → ,∘ {Γ = Γ}{Δ}{Σ}{δ}{σ}{A}{a}
  ; ,∘₁    = λ {Γ}{Δ}{Σ}{δ}{σ}{A}{a} → ,∘ {Γ = Γ}{Δ}{Σ}{δ}{σ}{A}{a}
  ; π₁β₀   = λ {Γ}{Δ}{A}{δ}{a} → π₁β {Γ = Γ}{Δ}{A}{δ}{a}
  ; π₁β₁   = λ {Γ}{Δ}{A}{δ}{a} → π₁β {Γ = Γ}{Δ}{A}{δ}{a}
  ; πη₀    = λ {Γ}{Δ}{A}{δ} → πη {Γ = Γ}{Δ}{A}{δ}
  ; πη₁    = λ {Γ}{Δ}{A}{δ} → πη {Γ = Γ}{Δ}{A}{δ}
  ; εη     = λ {Γ}{σ} → εη {Γ = Γ}{σ}
  ; π₂β₀   = λ {Γ}{Δ}{A}{δ}{a} → π₂β {Γ = Γ}{Δ}{A}{δ}{a}
  ; π₂β₁   = λ {Γ}{Δ}{A}{δ}{a} → π₂β {Γ = Γ}{Δ}{A}{δ}{a}
  }
