{-# OPTIONS --without-K #-}

open import Agda.Primitive
open import PShNew.Cats

module PShNew.Func {ℓ : Level}(C : Cat {ℓ}{ℓ}) where

open Cat C

open import lib

open import PShNew.Decl {ℓ}{ℓ}{ℓ} C
open import PShNew.Core {ℓ}{ℓ}{ℓ} C

open import TT.Decl
open import TT.Decl.Congr.NoJM d
open import TT.Core
open import TT.Func

Π : {Γ : Con}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
Π {Γ} A B = record
  { ∣_∣T    = λ {Ψ} α → Σ (∀{Ω}(β : Ω ⇒ Ψ)(x : ∣ A ∣T (Γ ∶ α ⟦ β ⟧C)) → ∣ B ∣T (Γ ∶ α ⟦ β ⟧C ,Σ x))
                          (λ f → ∀{Ω}{β : Ω ⇒ Ψ}{Ξ}{γ : Ξ ⇒ Ω}{x : ∣ A ∣T (Γ ∶ α ⟦ β ⟧C)}
                               → B ∶ f β x ⟦ γ ⟧T
                               ≡[ ap ∣ B ∣T (,Σ= (∘C Γ ⁻¹) refl) ]≡
                                 f (β ∘c γ) (coe (ap (λ z → ∣ A ∣T z) (∘C Γ ⁻¹)) (A ∶ x ⟦ γ ⟧T)))
  ; isSetT  = {!!}
  ; _∶_⟦_⟧T = {!!}
  ; idT     = {!!}
  ; ∘T      = {!!}
  }

f : Func c
f = record
  { Π     = Π
  ; Π[]   = {!!}
  ; lam   = {!!}
  ; app   = {!!}
  ; lam[] = {!!}
  ; Πβ    = {!!}
  ; Πη    = {!!}
  }
