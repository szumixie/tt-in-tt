{-# OPTIONS --without-K --no-eta #-}

module PShNew.Cats where

open import Agda.Primitive
open import lib

record Cat {ℓ}{ℓ'} : Set (lsuc (ℓ ⊔ ℓ')) where
  constructor conCat
  field
    Obj    : Set ℓ
    _⇒_    : Obj → Obj → Set ℓ'
    isSet⇒ : {I J : Obj}{f g : I ⇒ J}(p q : f ≡ g) → p ≡ q
    idc    : ∀{I} → I ⇒ I
    _∘c_   : ∀{I J K} → J ⇒ K → I ⇒ J → I ⇒ K
    idlc   : ∀{I J}{f : I ⇒ J} → idc ∘c f ≡ f
    idrc   : ∀{I J}{f : I ⇒ J} → f ∘c idc ≡ f
    assc   : ∀{I J K L}{f : I ⇒ J}{g : J ⇒ K}{h : K ⇒ L}
           → (h ∘c g) ∘c f ≡ h ∘c (g ∘c f)
         
  infix 6 _∘c_
  infix 4 _⇒_
