{-# OPTIONS --without-K #-}

open import Agda.Primitive
open import PShNew.Cats

module PShNew.Decl {ℓ ℓ' ℓ'' : Level}(C : Cat {ℓ'}{ℓ''}) where

open Cat C

open import lib

open import TT.Decl

------------------------------------------------------------------------------

record Con : Set (lsuc ℓ ⊔ ℓ' ⊔ ℓ'') where
  field
    ∣_∣C    : (Ψ : Obj) → Set ℓ
    isSetC  : {Ψ : Obj}{α₀ α₁ : ∣_∣C Ψ}(p q : α₀ ≡ α₁) → p ≡ q
    _∶_⟦_⟧C : {Ψ : Obj}(α : ∣_∣C Ψ){Ω : Obj}(β : Ω ⇒ Ψ) → ∣_∣C Ω
    idC     : {Ψ : Obj}{α : ∣_∣C Ψ} → _∶_⟦_⟧C α idc ≡ α
    ∘C      : {Ψ : Obj}{α : ∣_∣C Ψ}{Ω : Obj}{β : Ω ⇒ Ψ}{Ξ : Obj}{γ : Ξ ⇒ Ω}
            → _∶_⟦_⟧C α (β ∘c γ) ≡ _∶_⟦_⟧C (_∶_⟦_⟧C α β) γ

  infix 6 ∣_∣C _∶_⟦_⟧C

open Con public

=Con
  : {Γ₀ Γ₁ : Con}
    (=∣∣ : _≡_ {A = (Ψ : Obj) → Set ℓ} (∣_∣C Γ₀) (∣_∣C Γ₁))
  → (=⟦⟧ : (λ {Ψ} → _∶_⟦_⟧C Γ₀ {Ψ})
         ≡[ ap (λ z → {Ψ : Obj} → z Ψ → {Ω : Obj} → Ω ⇒ Ψ → z Ω) =∣∣ ]≡
           (λ {Ψ} → _∶_⟦_⟧C Γ₁ {Ψ}))
  → Γ₀ ≡ Γ₁
=Con
  {record { ∣_∣C =  ∣_∣C ; isSetC = isSetC₀ ; _∶_⟦_⟧C =  _∶_⟦_⟧C ; idC = idC₀ ; ∘C = ∘C₀ }}
  {record { ∣_∣C = .∣_∣C ; isSetC = isSetC₁ ; _∶_⟦_⟧C = ._∶_⟦_⟧C ; idC = idC₁ ; ∘C = ∘C₁ }}
  refl refl
  = ap3 {A = {Ψ : Obj}{α₀ α₁ : ∣_∣C Ψ}(p q : α₀ ≡ α₁) → p ≡ q}
        {B = {Ψ : Obj}{α : ∣_∣C Ψ} → _∶_⟦_⟧C α idc ≡ α}
        {C = {Ψ : Obj}{α : ∣_∣C Ψ}{Ω : Obj}{β : Ω ⇒ Ψ}{Ξ : Obj}{γ : Ξ ⇒ Ω} → _∶_⟦_⟧C α (β ∘c γ) ≡ _∶_⟦_⟧C (_∶_⟦_⟧C α β) γ}
        {D = Con}
        (λ isSetC idC ∘C → record { ∣_∣C = ∣_∣C ; isSetC = isSetC ; _∶_⟦_⟧C = _∶_⟦_⟧C ; idC = idC ; ∘C = ∘C })
        (funexti λ Ψ → funexti λ α₀ → funexti λ α₁ → funext λ p → funext λ q → set2h1 isSetC₀ _ _)
        (funexti λ Ψ → funexti λ α → isSetC₀ _ _)
        (funexti λ Ψ → funexti λ α → funexti λ Ω → funexti λ β → funexti λ Ξ → funexti λ γ → isSetC₀ _ _)

=Con'
  : {Γ₀ Γ₁ : Con}
    (=∣∣ : (Ψ : Obj) → ∣ Γ₀ ∣C Ψ ≡ ∣ Γ₁ ∣C Ψ)
  → (=⟦⟧ : {Ψ : Obj}
           {α₀ : ∣ Γ₀ ∣C Ψ}{α₁ : ∣ Γ₁ ∣C Ψ}(α₂ : α₀ ≡[ =∣∣ Ψ ]≡ α₁)
           {Ω : Obj}(β : Ω ⇒ Ψ)
         → Γ₀ ∶ α₀ ⟦ β ⟧C ≡[ =∣∣ Ω ]≡ Γ₁ ∶ α₁ ⟦ β ⟧C)
  → Γ₀ ≡ Γ₁
=Con' {Γ₀}{Γ₁} =∣∣ =⟦⟧
  = =Con
      (funext =∣∣)
      (funexti
         λ Ψ → funext λ α → funexti λ Ω → funext λ β
         → w ∣ Γ₀ ∣C ∣ Γ₁ ∣C (_∶_⟦_⟧C Γ₀) {Ψ}{α}{Ω}{β}(funext =∣∣)
         ◾ coe= (ap (λ z → z Ω) (fun-β =∣∣))
         ◾ ap (coe (=∣∣ Ω)) (ap (λ z → Γ₀ ∶ coe (z ⁻¹) α ⟦ β ⟧C) (ap (λ z → z Ψ) (fun-β =∣∣)))
         ◾ =⟦⟧ (coecoe (=∣∣ Ψ ⁻¹)(=∣∣ Ψ) ◾ coe= (≡inv' (=∣∣ Ψ))) β)
  where
    w : ∀{ℓ}(∣Γ₀∣ : Obj → Set ℓ)(∣Γ₁∣ : Obj → Set ℓ)(⟦⟧ : {Ψ : Obj} → ∣Γ₀∣ Ψ → {Ω : Obj} → Ω ⇒ Ψ → ∣Γ₀∣ Ω)
        {Ψ : Obj}{α₁ : ∣Γ₁∣ Ψ}{Ω : Obj}{β : Ω ⇒ Ψ}
        (q : ∣Γ₀∣ ≡ ∣Γ₁∣)
      → coe (ap (λ z → {Ψ : Obj} → z Ψ → {Ω : Obj} → Ω ⇒ Ψ → z Ω) q) ⟦⟧ α₁ β
      ≡ coe (ap (λ z → z Ω) q) (⟦⟧ (coe (ap (λ z → z Ψ) q ⁻¹) α₁) β)
    w _ _ _ refl = refl

------------------------------------------------------------------------------

record Ty (Γ : Con) : Set (lsuc ℓ ⊔ ℓ' ⊔ ℓ'') where
  field
    ∣_∣T    : {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → Set ℓ
    isSetT  : {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{a₀ a₁ : ∣_∣T α}(p q : a₀ ≡ a₁) → p ≡ q
    _∶_⟦_⟧T : {Ψ : Obj}{α : ∣ Γ ∣C Ψ}(a : ∣_∣T α){Ω : Obj}(β : Ω ⇒ Ψ) → ∣_∣T (Γ ∶ α ⟦ β ⟧C)
    idT     : {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{a : ∣_∣T α}
            → _∶_⟦_⟧T a idc ≡[ ap ∣_∣T (idC Γ) ]≡ a
    ∘T      : {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{a : ∣_∣T α}{Ω : Obj}{β : Ω ⇒ Ψ}{Ξ : Obj}{γ : Ξ ⇒ Ω}
            → _∶_⟦_⟧T a (β ∘c γ) ≡[ ap ∣_∣T (∘C Γ) ]≡ _∶_⟦_⟧T (_∶_⟦_⟧T a β) γ

  infix 6 ∣_∣T _∶_⟦_⟧T

  coe⟦⟧
    : {Ψ : Obj}
      {α₀ α₁ : ∣ Γ ∣C Ψ}(α₂ : α₀ ≡ α₁)
      {a : ∣_∣T α₀}{Ω : Obj}{β : Ω ⇒ Ψ}
    → _∶_⟦_⟧T (coe (ap ∣_∣T α₂) a) β ≡ coe (ap ∣_∣T (ap (λ z → Γ ∶ z ⟦ β ⟧C) α₂)) (_∶_⟦_⟧T a β)
  coe⟦⟧ refl = refl

open Ty public

=TyΣ
  : {Γ : Con}{A₀ A₁ : Ty Γ}
    (=∣∣ : _≡_ {A = {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → Set ℓ} (λ {Ψ} → ∣ A₀ ∣T {Ψ}) (λ {Ψ} → ∣ A₁ ∣T {Ψ}))
    (=⟦⟧ : (λ {Ψ}{α} → _∶_⟦_⟧T A₀ {Ψ}{α})
                     ≡[ ap (λ z → {Ψ : Obj}{α : ∣ Γ ∣C Ψ}(a : z α){Ω : Obj}(β : Ω ⇒ Ψ) → z (Γ ∶ α ⟦ β ⟧C)) =∣∣ ]≡
                       (λ {Ψ}{α} → _∶_⟦_⟧T A₁ {Ψ}{α}))
  → Σ (A₀ ≡ A₁) λ A₂ →
      {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ap (λ z → ∣ z ∣T α) A₂ ≡ fun-elim (fun-elimi =∣∣ Ψ) α
=TyΣ
  {Γ}
  {record { ∣_∣T =  ∣_∣T' ; isSetT = isSetT₀ ; _∶_⟦_⟧T =  _∶_⟦_⟧T' ; idT = idT₀ ; ∘T = ∘T₀ }}
  {record { ∣_∣T = .∣_∣T' ; isSetT = isSetT₁ ; _∶_⟦_⟧T = ._∶_⟦_⟧T' ; idT = idT₁ ; ∘T = ∘T₁ }}
  refl refl
  =  ap3 {A = A}{B}{C'}{Ty Γ} f isSetT' idT' ∘T'
  ,Σ λ {Ψ} α → apap3 {A = A}{B}{C'}{Ty Γ} f isSetT' idT' ∘T' (λ z → ∣ z ∣T α)
             ◾ ap3const isSetT' idT' ∘T'
  where
    A = {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{a₀ a₁ : ∣_∣T' α}(p q : a₀ ≡ a₁) → p ≡ q
    B = {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{a : ∣_∣T' α} → _∶_⟦_⟧T' a idc ≡[ ap ∣_∣T' (idC Γ) ]≡ a
    C' = {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{a : ∣_∣T' α}{Ω : Obj}{β : Ω ⇒ Ψ}{Ξ : Obj}{γ : Ξ ⇒ Ω}
       → _∶_⟦_⟧T' a (β ∘c γ) ≡[ ap ∣_∣T' (∘C Γ) ]≡ _∶_⟦_⟧T' (_∶_⟦_⟧T' a β) γ
    f : A → B → C' → Ty Γ
    f = λ isSetT' idT' ∘T' → record { ∣_∣T = ∣_∣T' ; isSetT = isSetT' ; _∶_⟦_⟧T = _∶_⟦_⟧T' ; idT = idT' ; ∘T = ∘T' }
    isSetT' = funexti λ Ψ → funexti λ α → funexti λ a₀ → funexti λ a₁ → funext λ p → funext λ q → set2h1 isSetT₀ _ _
    idT' = funexti λ Ψ → funexti λ α → funexti λ a → isSetT₀ _ _
    ∘T' = funexti λ Ψ → funexti λ α → funexti λ a → funexti λ Ω → funexti λ β → funexti λ Ξ → funexti λ γ → isSetT₀ _ _

=TyΣ'
  : {Γ : Con}{A₀ A₁ : Ty Γ}
    (=∣∣' : {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ∣ A₀ ∣T α ≡ ∣ A₁ ∣T α)
    (=⟦⟧' : {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{a₀ : ∣ A₀ ∣T α}{a₁ : ∣ A₁ ∣T α}(a₂ : a₀ ≡[ =∣∣' α ]≡ a₁){Ω : Obj}(β : Ω ⇒ Ψ)
         → A₀ ∶ a₀ ⟦ β ⟧T ≡[ =∣∣' (Γ ∶ α ⟦ β ⟧C) ]≡ A₁ ∶ a₁ ⟦ β ⟧T)
  → Σ (A₀ ≡ A₁) λ A₂ →
      {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ap (λ z → ∣ z ∣T α) A₂ ≡ =∣∣' α
=TyΣ' {Γ}{A₀}{A₁} =∣∣' =⟦⟧'
  =  proj₁ (=TyΣ =∣∣ =⟦⟧)
  ,Σ λ {Ψ} α → proj₂ (=TyΣ =∣∣ =⟦⟧) α
             ◾ ap (λ z → fun-elim z α) (ap (λ z → z Ψ) (fun-βi (λ Ψ → funext λ α → =∣∣' {Ψ} α)))
             ◾ ap (λ z → z α) (fun-β =∣∣')
  where
    p : (λ {Ψ₁} α₁ → fun-elim (fun-elimi (funexti (λ Ψ₂ → funext (=∣∣' {Ψ₂}))) Ψ₁) α₁) ≡ =∣∣'
    p = funexti λ Ψ₁ → funext λ α₁ →
          ap (λ z → fun-elim (z Ψ₁) α₁) (fun-βi (λ Ψ₂ → funext (=∣∣' {Ψ₂}))) ◾ ap (λ z → z α₁) (fun-β (=∣∣' {Ψ₁}))
    w : ∀{ℓ}(∣A₀∣ ∣A₁∣ : {Ψ : Obj} → ∣ Γ ∣C Ψ → Set ℓ)
        (⟦⟧T : {Ψ : Obj}{α : ∣ Γ ∣C Ψ}(a : ∣A₀∣ α){Ω : Obj}(β : Ω ⇒ Ψ) → ∣A₀∣ (Γ ∶ α ⟦ β ⟧C))
        {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{a : ∣A₁∣ α}{Ω : Obj}{β : Ω ⇒ Ψ}
        (q : (λ {Ψ} → ∣A₀∣ {Ψ}) ≡ ∣A₁∣)
      → coe (ap (λ z → {Ψ : Obj}{α : ∣ Γ ∣C Ψ}(a : z α){Ω : Obj}(β : Ω ⇒ Ψ) → z (Γ ∶ α ⟦ β ⟧C)) q) ⟦⟧T a β
      ≡ coe (fun-elim (fun-elimi q Ω) (Γ ∶ α ⟦ β ⟧C)) (⟦⟧T (coe (fun-elim (fun-elimi q Ψ) α ⁻¹) a) β)
    w _ _ _ refl = refl
    =∣∣ = funexti λ Ψ → funext λ α → =∣∣' {Ψ} α
    =⟦⟧ = funexti λ Ψ → funexti λ α → funext λ a₁ → funexti λ Ω → funext λ β
                  → w ∣ A₀ ∣T ∣ A₁ ∣T (_∶_⟦_⟧T A₀) (funexti (λ Ψ₁ → funext (λ α₁ → =∣∣' α₁)))
                  ◾ ap (λ z → coe (z (Γ ∶ α ⟦ β ⟧C)) (A₀ ∶ coe (z α ⁻¹) a₁ ⟦ β ⟧T)) p
                  ◾ =⟦⟧' {Ψ}{α}{coe (=∣∣' α ⁻¹) a₁}{a₁}
                         (coecoe (=∣∣' α ⁻¹)(=∣∣' α) ◾ coe= {p₀ = =∣∣' α ⁻¹ ◾ =∣∣' α}{refl} (≡inv' (=∣∣' α)))
                         {Ω} β

=Ty'
  : {Γ : Con}{A₀ A₁ : Ty Γ}
    (=∣∣' : {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ∣ A₀ ∣T α ≡ ∣ A₁ ∣T α)
    (=⟦⟧' : {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{a₀ : ∣ A₀ ∣T α}{a₁ : ∣ A₁ ∣T α}(a₂ : a₀ ≡[ =∣∣' α ]≡ a₁){Ω : Obj}(β : Ω ⇒ Ψ)
         → A₀ ∶ a₀ ⟦ β ⟧T ≡[ =∣∣' (Γ ∶ α ⟦ β ⟧C) ]≡ A₁ ∶ a₁ ⟦ β ⟧T)
  → A₀ ≡ A₁
=Ty' {Γ}{A₀}{A₁} =∣∣' =⟦⟧' = proj₁ (=TyΣ' {Γ}{A₀}{A₁} =∣∣' =⟦⟧')

∣∣=Ty'
  : {Γ : Con}{A₀ A₁ : Ty Γ}
    (=∣∣' : {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ∣ A₀ ∣T α ≡ ∣ A₁ ∣T α)
    (=⟦⟧' : {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{a₀ : ∣ A₀ ∣T α}{a₁ : ∣ A₁ ∣T α}(a₂ : a₀ ≡[ =∣∣' α ]≡ a₁){Ω : Obj}(β : Ω ⇒ Ψ)
         → A₀ ∶ a₀ ⟦ β ⟧T ≡[ =∣∣' (Γ ∶ α ⟦ β ⟧C) ]≡ A₁ ∶ a₁ ⟦ β ⟧T)
  → {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ap (λ z → ∣ z ∣T α) (=Ty' {Γ}{A₀}{A₁} =∣∣' =⟦⟧') ≡ =∣∣' α
∣∣=Ty' {Γ}{A₀}{A₁} =∣∣' =⟦⟧' = proj₂ (=TyΣ' {Γ}{A₀}{A₁} =∣∣' =⟦⟧')
  
------------------------------------------------------------------------------

record Tms (Γ Δ : Con) : Set (ℓ ⊔ ℓ' ⊔ ℓ'') where
  field
    ∣_∣s : {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ∣ Δ ∣C Ψ
    nats : {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{Ω : Obj}{β : Ω ⇒ Ψ}
         → Δ ∶ (∣_∣s α) ⟦ β ⟧C ≡ ∣_∣s (Γ ∶ α ⟦ β ⟧C)

open Tms public

=TmsΣ : {Γ Δ : Con}{σ₀ σ₁ : Tms Γ Δ}(=∣∣ : (λ {Ψ} → ∣ σ₀ ∣s {Ψ}) ≡ ∣ σ₁ ∣s)
      → Σ (σ₀ ≡ σ₁) λ σ₂ →
          {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ap (λ z → ∣ z ∣s α) σ₂ ≡ fun-elim (fun-elimi =∣∣ Ψ) α
=TmsΣ {Γ}{Δ}
     {record { ∣_∣s =  ∣_∣s' ; nats = nats₀ }}
     {record { ∣_∣s = .∣_∣s' ; nats = nats₁ }}
     refl
  =  ap {A = {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{Ω : Obj}{β : Ω ⇒ Ψ} → Δ ∶ (∣_∣s' α) ⟦ β ⟧C ≡ ∣_∣s' (Γ ∶ α ⟦ β ⟧C)}
        {B = Tms Γ Δ}
        (λ nats' → record { ∣_∣s = ∣_∣s' ; nats = nats' })
        nats'
  ,Σ λ {Ψ} α → apap nats' ⁻¹ ◾ apconst nats'
    where
      nats' = funexti λ Ψ → funexti λ α → funexti λ Ω → funexti λ β → isSetC Δ _ _

=TmsΣ' : {Γ Δ : Con}{σ₀ σ₁ : Tms Γ Δ}(=∣∣ : {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ∣ σ₀ ∣s α ≡ ∣ σ₁ ∣s α)
       → Σ (σ₀ ≡ σ₁) λ σ₂ →
           {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ap (λ z → ∣ z ∣s α) σ₂ ≡ =∣∣ α
=TmsΣ' {Γ}{Δ}{σ₀}{σ₁} =∣∣ =  proj₁ (=TmsΣ w)
                          ,Σ λ {Ψ} α → proj₂ (=TmsΣ w) α
                                     ◾ ap (λ z → fun-elim z α) (ap (λ z → z Ψ) (fun-βi (λ Ψ → funext λ α → =∣∣ α)))
                                     ◾ ap (λ z → z α) (fun-β =∣∣)
  where
    w = funexti λ Ψ → funext λ α → =∣∣ α

=Tms' : {Γ Δ : Con}{σ₀ σ₁ : Tms Γ Δ}(=∣∣ : {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ∣ σ₀ ∣s α ≡ ∣ σ₁ ∣s α)
      → σ₀ ≡ σ₁
=Tms' {Γ}{Δ}{σ₀}{σ₁} =∣∣ = proj₁ (=TmsΣ' {Γ}{Δ}{σ₀}{σ₁} =∣∣)

∣∣=Tms' : {Γ Δ : Con}{σ₀ σ₁ : Tms Γ Δ}(=∣∣ : {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ∣ σ₀ ∣s α ≡ ∣ σ₁ ∣s α)
          {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ap (λ z → ∣ z ∣s α) (=Tms' =∣∣) ≡ =∣∣ α
∣∣=Tms' {Γ}{Δ}{σ₀}{σ₁} =∣∣ = proj₂ (=TmsΣ' {Γ}{Δ}{σ₀}{σ₁} =∣∣)

------------------------------------------------------------------------------

record Tm (Γ : Con)(A : Ty Γ) : Set (ℓ ⊔ ℓ' ⊔ ℓ'') where
  field
    ∣_∣t : {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ∣ A ∣T α
    natt : {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{Ω : Obj}{β : Ω ⇒ Ψ}
         → A ∶ (∣_∣t α) ⟦ β ⟧T ≡ ∣_∣t (Γ ∶ α ⟦ β ⟧C)

open Tm public

=Tm : {Γ : Con}{A : Ty Γ}{t₀ t₁ : Tm Γ A}(=∣∣ : (λ {Ψ} → ∣ t₀ ∣t {Ψ}) ≡ ∣ t₁ ∣t) → t₀ ≡ t₁
=Tm {Γ}{A}
    {record { ∣_∣t =  ∣_∣t ; natt = natt₀ }}
    {record { ∣_∣t = .∣_∣t ; natt = natt₁ }}
    refl
  = ap {A = {Ψ : Obj}{α : ∣ Γ ∣C Ψ}{Ω : Obj}{β : Ω ⇒ Ψ} → A ∶ (∣_∣t α) ⟦ β ⟧T ≡ ∣_∣t (Γ ∶ α ⟦ β ⟧C)}
       {B = Tm Γ A}
       (λ natt → record { ∣_∣t = ∣_∣t ; natt = natt })
       (funexti λ Ψ → funexti λ α → funexti λ Ω → funexti λ β → isSetT A _ _)

=Tm' : {Γ : Con}{A : Ty Γ}{t₀ t₁ : Tm Γ A}(=∣∣ : {Ψ : Obj}(α : ∣ Γ ∣C Ψ) → ∣ t₀ ∣t α ≡ ∣ t₁ ∣t α) → t₀ ≡ t₁
=Tm' {Γ}{A}{t₀}{t₁} =∣∣ = =Tm (funexti λ Ψ → funext λ α → =∣∣ α)

------------------------------------------------------------------------------

d : Decl
d = record
  { Con = Con
  ; Ty  = Ty
  ; Tms = Tms
  ; Tm  = Tm
  }

------------------------------------------------------------------------------

open import TT.Decl.Congr.NoJM d

coe∣∣t : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t : Tm Γ A₀}{Ψ : Obj}{α : ∣ Γ ∣C Ψ}
       → ∣ coe (TmΓ= A₂) t ∣t α ≡ coe (ap (λ z → ∣ z ∣T α) A₂) (∣ t ∣t α)
coe∣∣t refl = refl
