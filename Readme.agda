module Readme where

-- This repository contains formalisations accompanying the following
-- papers:
-- 
--  * Type Theory in Type Theory using Quotient Inductive Types (POPL
--    2016)
--
--  * Normalisation by Evaluation for Dependent Types (FSCD 2016)
--
--  * Normalisation by Evaluation for Type Theory, in Type Theory
--    (LMCS)
--
-- by Thorsten Altenkirch and Ambrus Kaposi,
--
-- and the PhD thesis of Ambrus Kaposi titled
--
--  * Type theory in a type theory with quotient inductive types
--

------------------------------------------------------------------------------
-- Usage
------------------------------------------------------------------------------

-- Agda 2.5.1 or later version of Agda is required for typechecking
-- these files.
--
-- To start browsing the code for these formal developments, use the
-- following commands:
-- 
-- $ git clone git@bitbucket.org:akaposi/tt-in-tt.git
-- $ cd tt-in-tt
-- $ emacs Readme.agda
--
-- Typechecking can be started by selecting the option "Agda/Load"
-- from the menu bar.
-- 
-- Typechecking everything takes around half an hour. If Agda eats all
-- the memory, just select the option "Agda/Kill and restart Agda"
-- from the menu bar and restart typechecking (it only typechecks
-- every file once).

------------------------------------------------------------------------------
-- Contact
------------------------------------------------------------------------------

-- If you have any questions or comments, please don't hesitate to
-- contact Ambrus: kaposi.ambrus@gmail.com.

------------------------------------------------------------------------------
-- Local standard library
------------------------------------------------------------------------------

import lib                           -- our standard library
import JM                            -- heterogeneous equality

------------------------------------------------------------------------------
-- Type Theory in Type Theory using Quotient Inductive Types
------------------------------------------------------------------------------

-- Section 1 - Introduction

import popl.STL                      -- example of simply typed
                                     -- λ-calculus

-- Section 2 - The Type Theory we live in

import popl.IndIndEx                 -- induction-induction example
import popl.QITEx                    -- QIT example

-- Section 3 - Representing the syntax of Type Theory

import TT.Syntax                     -- the syntax
import TT.Rec                        -- recursor for the syntax
import TT.Elim                       -- eliminator for the syntax

import TT.Congr                      -- congruence rules
import TT.Laws                       -- derived laws

-- Section 4 - The standard model

import StandardModel.StandardModel   -- the standard model
import StandardModel.IR.StandardModel -- the standard model with an
                                     -- inductive-recursive universe

-- Section 5 - The logical predicate interpretation

-- import LogPred.LogPred               -- the logical predicate
                                     -- interpretation

-- Section 6 - Homotopy Type Theory

import TT.IsSet                       -- the truncation part of the syntax

import StandardModel.IR.U0            -- an inductive-recursive
                                      -- universe with ⊤,Π,Σ and
                                      -- proofs that it is a set

import StandardModel.IR.StandardModel -- the standard interpretation
                                      -- using the previously defined
                                      -- universe

------------------------------------------------------------------------------
-- Normalisation by Evaluation for Dependent Types
------------------------------------------------------------------------------

-- The formalisation for this paper is not finished, in some places we
-- used cheating, but all the occurrences of cheat can be filled. We
-- use cheat in NBE.LogPred and NBE.Quote.

import NBE.Cheat

-- Section 3 - Object theory

import TT.Syntax                     -- the syntax
import TT.Elim                       -- eliminator for the syntax

-- Section 4 - The category of renamings

import NBE.Renamings

-- Section 5 - The logical predicate interpretation

import Cats                          -- the categorical definitions
import NBE.TM                        -- the presheaf structure of the
                                     -- syntax
import NBE.LogPred.LogPred           -- the logical predicate
                                     -- interpretation

-- Section 6 - Normal forms

import NBE.Nf                        -- neutral terms and normal forms

-- Section 7 - Quote and unquote

import NBE.Quote.Quote               -- definition of quote and
                                     -- unquote

-- Section 8

import NBE.Norm                      -- normalisation and completeness
import NBE.Stab                      -- stability

------------------------------------------------------------------------------
-- Normalisation by Evaluation for Type Theory, in Type Theory
------------------------------------------------------------------------------

-- The formalisation for this paper is not finished, in some places we
-- used cheating, but all the occurrences of cheat can be filled. We
-- use cheat in NBE.LogPred and NBE.Quote.

import NBE.Cheat

-- Section 3 - Object theory

import TT.Syntax                     -- the syntax
import TT.Elim                       -- eliminator for the syntax

-- Section 4 - Injectivity of context and type formers

import TT.ConElim                    -- injectivity of substitution
                                     -- extension
import NormTy.NTy                    -- definition of normal types
import NormTy.NormTy                 -- normalisation of types
import NormTy.Inj                    -- injectivity of Π and El

-- Section 5 - The category of renamings

import NBE.Renamings

-- Section 6 - The logical predicate interpretation

import NBE.TM                        -- the presheaf structure of the
                                     -- syntax
import NBE.LogPred.LogPred           -- the logical predicate
                                     -- interpretation

-- Section 7 - Normal forms

import NBE.Nf                        -- neutral terms and normal forms
import Nf.Nf                         -- an equivalent definition of
                                     -- variables, neutral terms and
                                     -- normal forms
import Nf.Dec                        -- decidability of equality for
                                     -- this equivalent definition

-- Section 8 - Quote and unquote

import NBE.Quote.Quote               -- definition of quote and
                                     -- unquote

-- Section 9

import NBE.Norm                      -- normalisation and completeness
import NBE.Stab                      -- stability
--import NoJM.Cons                     -- consistency

-- Appendix A

import Cats                          -- the categorical definitions

------------------------------------------------------------------------------
-- Ambrus Kaposi's PhD thesis (Type theory in a type theory with
-- quotient inductive types)
------------------------------------------------------------------------------

-- Chapter 2 - Metatheory

import thesis.meta                   -- 

-- Chapter 3 - Syntax and standard semantics of type theory

import TT.Syntax                     -- the syntax
import JM                            -- reasoning in the syntax: 
                                     -- heterogeneous equality
import TT.Examples                   -- reasoning in the syntax:
                                     -- examples
import TT.Rec                        -- recursor for the syntax
--import NoJM.Disj                     -- disjointness of context and
                                     -- type constructors
import TT.Elim                       -- eliminator for the syntax
import NormTy.NTy                    -- definition of normal types
import NormTy.NormTy                 -- normalisation of types
import NormTy.Stab                   -- stability of normalisation of
                                     -- types
import NormTy.Inj                    -- injectivity of Pi
import StandardModel.StandardModel   -- the standard model

-- Chapter 4 - Parametricity

-- import LogPred.LogPred               -- the logical predicate
                                     -- interpretation

-- Chapter 5 - Normalisation by evaluation

import PSh.PSh                       -- the presheaf model
import NBE.Renamings                 -- the category of renamings
import Cats                          -- the categorical definitions
import NBE.TM                        -- the Yoneda embedding of the syntax
import NBE.LogPred.LogPred           -- the presheaf logical predicate
                                     -- interpretation
import NBE.Nf                        -- normal forms
import NBE.Quote.Quote               -- quote and unquote
import NBE.Norm                      -- normalisation and completeness
import NBE.Stab                      -- stability
--import NoJM.Cons                     -- consistency

------------------------------------------------------------------------------
-- Everything put together
------------------------------------------------------------------------------

-- syntax

import TT.Readme                     -- definition of type theory

-- models

import StandardModel.Readme          -- standard (set-theoretic) model
-- import PSh                        -- this will be deprecated
-- import PShNew                     -- this will replace PSh
-- import LogPred                       -- logical predicate translation
import Setoid.Decl
import Setoid.Core
import Setoid.Func

--import NoJM.Readme                   -- definition of type theory
                                     -- without JM equality

-- import NBE.Examples                  -- examples of normalisation

import Typecheck

import asai

-- relationship of extrinsic and intrinsic syntax

import Extrinsic.Extrinsic             -- extrinsic syntax for TT.Core
import Extrinsic.propext               -- propositional extensionality
                                       -- axiom
import Extrinsic.STT                   -- isomorphism of extrinsic and
                                       -- intrinsic definitions of STT
import Extrinsic.TTsmallIso            -- isomorphism of extrinsic and
                                       -- intrinsic definitions of a
                                       -- Con-Ty theory
import Extrinsic.TTsmallRec            -- deriving the intrinsic
                                       -- recursor from the extrinsic
                                       -- one for the Con-Ty theory

-- simple type theory

import STT.Syntax
