{-# OPTIONS --without-K --no-eta --rewriting #-}

module Elim.EndsWith where

open import lib
open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.FuncSP

-- we decide whether an iterated SP Π type ends with U or not

d : Decl
d = record { Con = ⊤ ; Ty = λ _ → Bool ; Tms = λ _ _ → ⊤ ; Tm = λ _ _ → ⊤ }

c : Core d
c = record { c1 = record
      { • = tt
      ; _,_ = λ _ _ → tt
      ; _[_]T = λ b _ → b
      ; id = tt
      ; _∘_ = λ _ _ → tt
      ; ε = tt
      ; _,s_ = λ _ _ → tt
      ; π₁ = λ _ → tt
      ; _[_]t = λ _ _ → tt
      ; π₂ = λ _ → tt
      } ; c2 = record
      { [id]T = refl
      ; [][]T = refl
      ; idl = refl
      ; idr = refl
      ; ass = refl
      ; ,∘ = refl
      ; π₁β = refl
      ; πη = refl
      ; εη = refl
      ; π₂β = refl
      } }

b : Base c
b = record { U = true ; U[] = refl ; El = λ _ → false ; El[] = refl }

f : FuncSP b
f = record
      { Π = λ _ b → b
      ; Π[] = refl
      ; lam = λ _ → tt
      ; app = λ _ → tt
      ; lam[] = refl
      ; Πβ = refl
      ; Πη = refl
      }

open import TT.Decl.Syntax
open import TT.Base.Syntax
open import TT.FuncSP.Syntax
open import TT.Decl.Congr.NoJM syntaxDecl
open import TT.FuncSP.Rec

endsWithU : {Γ : Con} → Ty Γ → Bool
endsWithU A = RecTy f A

test1 : ∀{Γ} → endsWithU (Π {Γ , U}
                            (coe (TmΓ= U[]) vz)
                            U)
             ≡ true
test1 = refl

test2 : ∀{Γ} → endsWithU (Π {Γ , U}
                            (coe (TmΓ= U[]) vz)
                            (El (coe (TmΓ= ([][]T ◾ U[])) (vs vz))))
             ≡ false
test2 = refl

test3 : ∀{Γ} → endsWithU (U {Γ}) ≡ true
test3 = refl

test4 : ∀{Γ} → endsWithU (Π {Γ , U}
                            (coe (TmΓ= U[]) vz)
                            (Π (coe (TmΓ= ([][]T ◾ U[])) (vs vz))
                               U))
             ≡ true
test4 = refl

test5 : ∀{Γ} → endsWithU (Π {Γ , U}
                            (coe (TmΓ= U[]) vz)
                            (Π (coe (TmΓ= ([][]T ◾ U[])) (vs vz))
                               (El (coe (TmΓ= ([][]T ◾ [][]T ◾ U[])) (vs (vs vz))))))
             ≡ false
test5 = refl
