{-# OPTIONS --rewriting #-}

module Elim.R where

open import Agda.Primitive

open import lib
open import JM

open import StandardModel.FuncSP
open import StandardModel.FuncPar

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Base.Syntax
open import TT.FuncSP.Syntax
open import TT.FuncPar.Syntax

open import TT.Decl.Congr.NoJM syntaxDecl
open import TT.Core.Congr.NoJM syntaxCore

open import TT.RecFuncSPFuncPar fsp fp

-- standard model

_ᶜC : Con → Set₁
_ᶜT : ∀{Γ} → Ty Γ →  Γ ᶜC → Set₁
_ᶜs : ∀{Γ Δ} → Tms Γ Δ →  Γ ᶜC →  Δ ᶜC
_ᶜt : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → (γ :  Γ ᶜC) → (A ᶜT) γ

_ᶜC = RecCon
_ᶜT = RecTy
_ᶜs = RecTms
_ᶜt = RecTm

coeᶜt : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t : Tm Γ A₀}
       →  (coe (TmΓ= A₂) t) ᶜt ≡ coe (ap (λ z → (γ :  Γ ᶜC) → z γ) (ap _ᶜT A₂))  (t ᶜt)
coeᶜt refl = refl

open import TT.Decl.DepModel
open import TT.Core.DepModel
open import TT.Base.DepModel
open import TT.FuncSP.DepModel
open import TT.FuncPar.DepModel

open import NBE.Cheat

-- binary logical relation operation

private
  dᴹ : Declᴹ
  dᴹ = record
    { Conᴹ = λ Δ → Δ ᶜC → Δ ᶜC → Set₁
    ; Tyᴹ  = λ {Δ} Δᴿ A → {γ₀ γ₁ : Δ ᶜC} → Δᴿ γ₀ γ₁ → (A ᶜT) γ₀ → (A ᶜT) γ₁ → Set₁
    ; Tmsᴹ = λ {Δ}{Θ} Δᴿ Θᴿ σ → {γ₀ γ₁ : Δ ᶜC} → Δᴿ γ₀ γ₁ → Θᴿ ((σ ᶜs) γ₀) ((σ ᶜs) γ₁)
    ; Tmᴹ  = λ {Δ} Δᴿ {A} Aᴿ t → {γ₀ γ₁ : Δ ᶜC}(γᴿ : Δᴿ γ₀ γ₁) → Aᴿ γᴿ ((t ᶜt) γ₀) ((t ᶜt) γ₁)
    }

  open Declᴹ dᴹ

  coeTmsΓΔᴹ=
    : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
      {δ₀ δ₁ : Tms Γ Δ}(δ₂ : δ₀ ≡ δ₁)
      {δᴿ : Tmsᴹ Γᴹ Δᴹ δ₀}
    → _≡_ {A = Tmsᴹ Γᴹ Δᴹ δ₁}
          (coe (TmsΓΔᴹ= {Γ}{Γᴹ}{Δ}{Δᴹ} δ₂) δᴿ)
          (λ {γ₀}{γ₁} γᴿ → coe (ap2 Δᴹ (ap (λ z → (z ᶜs) γ₀) δ₂) (ap (λ z → (z ᶜs) γ₁) δ₂)) (δᴿ γᴿ))
  coeTmsΓΔᴹ= refl = refl

  Aᴹ= -- TODO: do we need this?
    : {Γ : Con}{Γᴹ : Conᴹ Γ}
      {A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
      {Aᴹ₀ : Tyᴹ Γᴹ A₀}{Aᴹ₁ : Tyᴹ Γᴹ A₁}(Aᴹ₂ : (λ {γ₀}{γ₁} → Aᴹ₀) ≡[ TyΓᴹ= A₂ ]≡ Aᴹ₁)
      {t : Tm Γ A₀}
      {γ₀ γ₁ : Γ ᶜC}{γᴹ : Γᴹ γ₀ γ₁}
    → Aᴹ₀ γᴹ ((t ᶜt) γ₀) ((t ᶜt) γ₁)
    ≡ Aᴹ₁ γᴹ ((coe (TmΓ= A₂) t ᶜt) γ₀) ((coe (TmΓ= A₂) t ᶜt) γ₁)
  Aᴹ= refl refl = refl

  coeTmΓᴹ= -- TODO: do we need this?
    : {Γ : Con}{Γᴹ : Conᴹ Γ}
      {A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
      {Aᴹ₀ : Tyᴹ Γᴹ A₀}{Aᴹ₁ : Tyᴹ Γᴹ A₁}(Aᴹ₂ : (λ {γ₀}{γ₁} → Aᴹ₀) ≡[ TyΓᴹ= A₂ ]≡ Aᴹ₁)
      {t : Tm Γ A₀}
      {tᴹ : Tmᴹ Γᴹ Aᴹ₀ t}
      {γ₀ γ₁ : Γ ᶜC}{γᴹ : Γᴹ γ₀ γ₁}
    → coe (TmΓᴹ= A₂ Aᴹ₂ {t} refl) tᴹ γᴹ
    ≡ coe (Aᴹ= A₂ Aᴹ₂) (tᴹ γᴹ)
  coeTmΓᴹ= refl refl = refl

  c1ᴹ : Core1ᴹ dᴹ
  c1ᴹ = record
    { •ᴹ     = λ _ _ → Lift ⊤
    ; _,Cᴹ_  = λ {Γ} Γᴿ {A} Aᴿ γ₀ γ₁ → Σ (Γᴿ (proj₁ γ₀) (proj₁ γ₁)) (λ γᴿ → Aᴿ γᴿ (proj₂ γ₀) (proj₂ γ₁))
    ; _[_]Tᴹ = λ {Γ} {Γᴿ} {Δ} {Δᴿ} {A} Aᴿ {σ} σᴿ {γ₀}{γ₁} γᴿ Aσ₀ Aσ₁ → Aᴿ (σᴿ γᴿ) Aσ₀ Aσ₁ 
    ; εᴹ     = λ {Γ}{Γᴿ} {γ₀}{γ₁} γᴿ → lift tt
    ; _,sᴹ_  = λ {Γ}{Γᴿ}{Δ}{Δᴿ}{σ} σᴿ {A}{Aᴿ} {t} tᴿ {γ₀}{γ₁} γᴿ → σᴿ γᴿ ,Σ tᴿ γᴿ
    ; idᴹ    = λ {Γ}{Γᴿ} {γ₀}{γ₁} γᴿ → γᴿ 
    ; _∘ᴹ_   = λ {Γ}{Γᴿ}{Δ}{Δᴿ}{Σ}{Σᴿ}{σ} σᴿ {δ} δᴿ {γ₀}{γ₁} γᴿ → σᴿ (δᴿ γᴿ)
    ; π₁ᴹ    = λ {Γ}{Γᴿ}{Δ}{Δᴿ} {A}{Aᴿ}{σ} σᴿ {γ₀}{γ₁} γᴿ → proj₁ (σᴿ γᴿ)
    ; _[_]tᴹ = λ {Γ}{Γᴿ}{Δ}{Δᴿ} {A}{Aᴿ} {t} tᴿ {σ} σᴿ {γ₀}{γ₁} γᴿ → tᴿ (σᴿ γᴿ)
    ; π₂ᴹ    = λ {Γ}{Γᴿ}{Δ}{Δᴿ} {A}{Aᴿ}{σ} σᴿ {γ₀}{γ₁} γᴿ → proj₂ (σᴿ γᴿ)
    }

  open Core1ᴹ c1ᴹ
  
  c2ᴹ : Core2ᴹ dᴹ c1ᴹ
  c2ᴹ = record
    { [id]Tᴹ = coe= {p₀ = TyΓᴹ= [id]T}{refl}(UIP _ _) 
    ; [][]Tᴹ = coe= {p₀ = TyΓᴹ= [][]T}{refl}(UIP _ _)
    ; idlᴹ   = coe= {p₀ = TmsΓΔᴹ= idl}{refl}(UIP _ _)
    ; idrᴹ   = coe= {p₀ = TmsΓΔᴹ= idr}{refl}(UIP _ _)
    ; assᴹ   = coe= {p₀ = TmsΓΔᴹ= ass}{refl}(UIP _ _)
    ; π₁βᴹ   = coe= {p₀ = TmsΓΔᴹ= π₁β}{refl}(UIP _ _)
    ; πηᴹ    = coe= {p₀ = TmsΓΔᴹ= πη}{refl}(UIP _ _)
    ; εηᴹ    = coe= {p₀ = TmsΓΔᴹ= εη}{refl}(UIP _ _)
    ; ,∘ᴹ    = λ {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ}{δ}{δᴹ}{σ}{σᴹ}{A}{Aᴹ}{a}{aᴹ}
               → coeTmsΓΔᴹ= ,∘
               ◾ funexti λ γ₀ → funexti λ γ₁ → funext λ γᴿ
               → {!!}
               
    ; π₂βᴹ   = coe= {p₀ = TmΓᴹ= (A[]T= π₁β) ([]Tᴹ=′ dᴹ c1ᴹ refl refl refl refl refl refl π₁β (coe= (UIP (TmsΓΔᴹ= π₁β) refl))) π₂β}{refl}(UIP _ _)
    }
{-
  open Core2ᴹ c2ᴹ

  cᴹ : Coreᴹ dᴹ
  cᴹ = record
    { c1ᴹ = c1ᴹ
    ; c2ᴹ = c2ᴹ
    }

  bᴹ : Baseᴹ cᴹ
  bᴹ = record
    { Uᴹ    = λ {Γ}{Γᴿ} {γ₀}{γ₁} γᴿ α₀ α₁ → Lift (α₀ → α₁)
    ; Elᴹ   = λ {Γ}{Γᴿ}{a} aᴿ {γ₀}{γ₁} γᴿ aᶜ₀ aᶜ₁ → Lift (aᴿ γᴿ .unlift (aᶜ₀ .unlift) ≡ aᶜ₁ .unlift)
    ; U[]ᴹ  = coe= {p₀ = TyΓᴹ= U[]}{refl}(UIP _ _)
    ; El[]ᴹ = λ {Γ}{Γᴿ}{Δ}{Δᴿ}{σ}{σᴿ}{a}{aᴿ} → cheat
    }

  open Baseᴹ bᴹ

  spᴹ : FuncSPᴹ bᴹ
  spᴹ = record
    { Πᴹ     = λ {Γ}{Γᴿ}{a} aᴿ {B} Bᴿ {γ₀}{γ₁} γᴿ f₀ f₁ → (α₀ : (a ᶜt) γ₀) → Bᴿ (γᴿ ,Σ (lift refl)) (f₀ α₀) (f₁ (aᴿ γᴿ .unlift α₀))
    ; appᴹ   = λ {Γ}{Γᴿ}{a}{aᴿ}{B}{Bᴿ}{t} tᴿ {γ₀}{γ₁} γᴿ → {!!}
    ; lamᴹ   = λ {Γ}{Γᴿ}{a}{aᴿ}{B}{Bᴿ} {t} tᴿ {γ₀}{γ₁} γᴿ α₀ → tᴿ (γᴿ ,Σ {!!})
    ; Π[]ᴹ   = {!!}
    ; lam[]ᴹ = {!!}
    ; Πβᴹ    = {!!}
    ; Πηᴹ    = {!!}
    }

{-

  spᴹ : FuncSPᴹ bᴹ
  spᴹ = record
    { Πᴹ     = λ {Γ}{Γᴾ}{a} aᴾ {B} Bᴾ γ γᴾ f → (α : Lift (⌜ a ⌝t γ))(αᴾ : aᴾ γ γᴾ α) → Bᴾ (γ ,Σ α) (γᴾ ,Σ lift αᴾ) (f (unlift α))
    ; appᴹ   = λ {Γ}{Γᴾ}{a}{aᴾ}{B}{Bᴾ}{t} tᴾ γ γᴾ → tᴾ (proj₁ γ)(proj₁ γᴾ)(proj₂ γ)(unlift (proj₂ γᴾ))
    ; lamᴹ   = λ {Γ}{Γᴾ}{a}{aᴾ}{B}{Bᴾ}{t} tᴾ γ γᴾ → λ α αᴾ → tᴾ (γ ,Σ α)(γᴾ ,Σ lift αᴾ)
    ; Π[]ᴹ   = cheat
    ; lam[]ᴹ = cheat
    ; Πβᴹ    = coe= {p₀ = TmΓAᴹ= TT.FuncSP.Syntax.Πβ}{refl}(UIP _ _)
    ; Πηᴹ    = coe= {p₀ = TmΓAᴹ= TT.FuncSP.Syntax.Πη}{refl}(UIP _ _)
    }

  parᴹ : FuncParᴹ {k = lsuc lzero} cᴹ
  parᴹ = record
    { Πᴹ     = λ {Γ}{Γᴾ} T {B} Bᴾ γ γᴾ f → (α : T) → (Bᴾ α) γ γᴾ (f α)
    ; appᴹ   = λ {Γ}{Γᴾ}{T}{B}{Bᴾ}{t} tᴾ α γ γᴾ → tᴾ γ γᴾ α
    ; lamᴹ   = λ {Γ}{Γᴾ}{T}{B}{Bᴾ}{t} tᴾ γ γᴾ α → tᴾ α γ γᴾ
    ; Π[]ᴹ   = coe= {p₀ = TyΓᴹ= TT.FuncPar.Syntax.Π[]}{refl}(UIP _ _)
    ; lam[]ᴹ = coe= {p₀ = TmΓᴹ= TT.FuncPar.Syntax.Π[] (coe= (UIP (TyΓᴹ= TT.FuncPar.Syntax.Π[]) refl)) TT.FuncPar.Syntax.lam[]}{refl}(UIP _ _)
    ; Πβᴹ    = λ {Γ}{Γᴹ}{A}{B}{Bᴹ} → coe= {p₀ = ap (λ z → (a : A) → Tmᴹ Γᴹ (Bᴹ a) (z a)) TT.FuncPar.Syntax.Πβ}{refl}(UIP _ _)
    ; Πηᴹ    = coe= {p₀ = TmΓAᴹ= TT.FuncPar.Syntax.Πη}{refl}(UIP _ _)
    }
  

open import TT.Core.Elim cᴹ

_ᴾC : (Δ : Con) → ⌜ Δ ⌝C → Set₁
_ᴾT : ∀{Δ}(A : Ty Δ) → (γ : ⌜ Δ ⌝C) → (Δ ᴾC) γ → ⌜ A ⌝T γ → Set₁
_ᴾs : ∀{Δ}{Θ}(σ : Tms Δ Θ) → (γ : ⌜ Δ ⌝C) → (Δ ᴾC) γ → (Θ ᴾC) (⌜ σ ⌝s γ)
_ᴾt : ∀{Δ}{A}(t : Tm Δ A) → (γ : ⌜ Δ ⌝C) → (γᴾ : (Δ ᴾC) γ) → (A ᴾT) γ γᴾ (⌜ t ⌝t γ)


_ᴾC = ElimCon
_ᴾT = ElimTy
_ᴾs = ElimTms
_ᴾt = ElimTm
-}
-}
