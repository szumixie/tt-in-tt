{-# OPTIONS --rewriting #-}

module Elim.QIIT where

open import lib hiding (ℕ; zero; suc)

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Base.Syntax
open import TT.FuncSP.Syntax
open import TT.FuncPar.Syntax

open import TT.FuncSP

private module SP = FuncSP syntaxFuncSP

open import Elim.P
open import Elim.E

-- constructor and eliminator of a quotient inductive inductive type

module _ (Δ : Con) where

  postulate
    con : ⌜ Δ ⌝C

  postulate
    elim : (m : (Δ ᴾC) con) → (Δ ᴱC) con m

-- example

open import TT.Decl.Congr syntaxDecl

NatAlg : Con
NatAlg = • , U , El (coe (TmΓ= U[]) vz) , SP.Π (coe (TmΓ= ([][]T ◾ U[])) (vs vz)) (El (coe (TmΓ= ([][]T ◾ [][]T ◾ U[])) (vs (vs vz))))

Nat : Set₁
Nat = Lift (proj₂ (proj₁ (proj₁ (con NatAlg))))

zero : Nat
zero = coe (ap (λ z → Lift (z (proj₁ (proj₁ (con NatAlg))))) (⌜coe⌝t U[] {vz})) (proj₂ (proj₁ (con NatAlg))) 
