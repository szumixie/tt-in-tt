{-# OPTIONS --rewriting #-}

module Elim.P where

open import Agda.Primitive

open import lib
open import JM

open import StandardModel.FuncSP
open import StandardModel.FuncPar

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Base.Syntax
open import TT.FuncSP.Syntax
open import TT.FuncPar.Syntax

open import TT.Decl.Congr.NoJM syntaxDecl
open import TT.Core.Congr.NoJM syntaxCore

open import TT.RecFuncSPFuncPar fsp fp

⌜_⌝C : Con → Set₁
⌜_⌝T : ∀{Γ} → Ty Γ → ⌜ Γ ⌝C → Set₁
⌜_⌝s : ∀{Γ Δ} → Tms Γ Δ → ⌜ Γ ⌝C → ⌜ Δ ⌝C
⌜_⌝t : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → (γ : ⌜ Γ ⌝C) → ⌜ A ⌝T γ

⌜_⌝C = RecCon
⌜_⌝T = RecTy
⌜_⌝s = RecTms
⌜_⌝t = RecTm

⌜coe⌝t : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t : Tm Γ A₀}
       → ⌜ coe (TmΓ= A₂) t ⌝t ≡ coe (ap (λ z → (γ : ⌜ Γ ⌝C) → z γ) (ap ⌜_⌝T A₂)) ⌜ t ⌝t
⌜coe⌝t refl = refl

open import TT.Decl.DepModel
open import TT.Core.DepModel
open import TT.Base.DepModel
open import TT.FuncSP.DepModel
open import TT.FuncPar.DepModel

open import NBE.Cheat

private
  dᴹ : Declᴹ
  dᴹ = record
    { Conᴹ = λ Δ → ⌜ Δ ⌝C → Set₁
    ; Tyᴹ  = λ {Δ} Δᴾ A → (γ : ⌜ Δ ⌝C) → Δᴾ γ → ⌜ A ⌝T γ → Set₁
    ; Tmsᴹ = λ {Δ}{Θ} Δᴾ Θᴾ σ → (γ : ⌜ Δ ⌝C) → Δᴾ γ → Θᴾ (⌜ σ ⌝s γ)
    ; Tmᴹ  = λ {Δ} Δᴾ {A} Aᴾ t → (γ : ⌜ Δ ⌝C)(γᴾ : Δᴾ γ) → Aᴾ γ γᴾ (⌜ t ⌝t γ)
    }

  open Declᴹ dᴹ
{-
  coeTmΓᴹ=
    : {Γ : Con}{Γᴹ : Conᴹ Γ}
      {A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
      {Aᴹ₀ : Tyᴹ Γᴹ A₀}{Aᴹ₁ : Tyᴹ Γᴹ A₁}(Aᴹ₂ : Aᴹ₀ ≡[ TyΓᴹ= A₂ ]≡ Aᴹ₁)
      {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}(t₂ : t₀ ≡[ TmΓ= A₂ ]≡ t₁)
      (tᴹ : Tmᴹ Γᴹ Aᴹ₀ t₀)
      {γ : ⌜ Γ ⌝C}{γᴾ : Γᴹ γ}
    → (coe (TmΓᴹ= A₂ Aᴹ₂ t₂) tᴹ) γ γᴾ ≡ coe {!!} (tᴹ γ γᴾ)
  coeTmΓᴹ= = {!!}
-}
  c1ᴹ : Core1ᴹ dᴹ
  c1ᴹ = record
    { •ᴹ = λ _ → Lift ⊤
    ; _,Cᴹ_ = λ {Δ} Δᴾ {A} Aᴾ γ → Σ (Δᴾ (proj₁ γ)) λ γᴾ → Aᴾ (proj₁ γ) γᴾ (proj₂ γ)
    ; _[_]Tᴹ = λ {Δ}{Δᴾ}{Θ}{Θᴾ}{A} Aᴾ {δ} δᴾ γ γᴾ α → Aᴾ (⌜ δ ⌝s γ) (δᴾ γ γᴾ) α
    ; εᴹ = λ {Δ}{Δᴾ} γ γᴾ → lift tt
    ; _,sᴹ_ = λ {Δ}{Δᴾ}{Θ}{Θᴾ}{δ} δᴾ {A}{Aᴾ}{t} tᴾ γ γᴾ → δᴾ γ γᴾ ,Σ tᴾ γ γᴾ
    ; idᴹ = λ {Δ}{Δᴾ} γ γᴾ → γᴾ
    ; _∘ᴹ_ = λ {Γ}{Γᴾ}{Δ}{Δᴾ}{Θ}{Θᴾ}{σ} σᴾ {δ} δᴾ γ γᴾ → σᴾ (⌜ δ ⌝s γ) (δᴾ γ γᴾ)
    ; π₁ᴹ = λ {Δ}{Δᴾ}{Θ}{Θᴾ}{A}{Aᴾ}{δ} δᴾ γ γᴾ → proj₁ (δᴾ γ γᴾ)
    ; _[_]tᴹ = λ {Δ}{Δᴾ}{Θ}{Θᴾ}{A}{Aᴾ}{t} tᴾ {δ} δᴾ γ γᴾ → tᴾ (⌜ δ ⌝s γ) (δᴾ γ γᴾ)
    ; π₂ᴹ = λ {Δ}{Δᴾ}{Θ}{Θᴾ}{A}{Aᴾ}{δ} δᴾ γ γᴾ → proj₂ (δᴾ γ γᴾ)
    }

  open Core1ᴹ c1ᴹ

  [][]Tᴾ
    : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
      {Σ : Con}{Σᴹ : Conᴹ Σ}{A : Ty Σ}{Aᴹ : Tyᴹ Σᴹ A}
      {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}{δ : Tms Δ Σ}{δᴹ : Tmsᴹ Δᴹ Σᴹ δ}
    →  _[_]Tᴹ {A = A [ δ ]T} (_[_]Tᴹ {A = A} Aᴹ {δ} δᴹ) {σ} σᴹ
    ≡[ TyΓᴹ= {Γ}{Γᴹ}{A [ δ ]T [ σ ]T}{A [ δ ∘ σ ]T} [][]T ]≡
       _[_]Tᴹ {A = A} Aᴹ {δ ∘ σ}(_∘ᴹ_ {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ} {σ = δ} δᴹ  {σ} σᴹ)
  [][]Tᴾ = coe= {p₀ = TyΓᴹ= [][]T}{refl} (UIP _ _)

  ,∘ᴾ
    : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
      {Θ : Con}{Θᴹ : Conᴹ Θ}{σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
      {δ : Tms Θ Γ}{δᴹ : Tmsᴹ Θᴹ Γᴹ δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
      {t : Tm Γ (A [ σ ]T)}{tᴹ : Tmᴹ Γᴹ (_[_]Tᴹ {A = A} Aᴹ {σ} σᴹ) t}
    → _∘ᴹ_ {Θ}{Θᴹ}{Γ}{Γᴹ}{Δ , A}{_,Cᴹ_ {Δ} Δᴹ {A} Aᴹ}{σ = σ ,s t} (_,sᴹ_ {δ = σ} σᴹ {A}{Aᴹ}{t} tᴹ) {δ} δᴹ
    ≡[ TmsΓΔᴹ= {Θ}{Θᴹ}{Δ , A}{_,Cᴹ_ {Δ} Δᴹ {A} Aᴹ} ,∘ ]≡
      _,sᴹ_ {δ = σ ∘ δ}(_∘ᴹ_ {Θ}{Θᴹ}{Γ}{Γᴹ}{Δ}{Δᴹ}{σ = σ} σᴹ {δ} δᴹ){A}{Aᴹ}
            {coe (TmΓ= [][]T) (t [ δ ]t)}
            (coe (TmΓᴹ= [][]T ([][]Tᴾ {A = A}{Aᴹ}{δ}{δᴹ}{σ}{σᴹ}) (refl {x = coe (TmΓ= [][]T) (t [ δ ]t)})) (_[_]tᴹ {A = A [ σ ]T}{_[_]Tᴹ {A = A} Aᴹ {σ} σᴹ}{t = t} tᴹ {δ} δᴹ))
  ,∘ᴾ {Γ}{Γᴹ}{Δ}{Δᴹ}{Θ}{Θᴹ}{σ}{σᴹ}{δ}{δᴹ}{A}{Aᴹ}{t}{tᴹ}
    = cheat

  c2ᴹ : Core2ᴹ dᴹ c1ᴹ
  c2ᴹ = record
    { [id]Tᴹ = coe= {p₀ = TyΓᴹ= [id]T}{refl}(UIP _ _)
    ; [][]Tᴹ = λ {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ}{A}{Aᴹ}{σ}{σᴹ}{δ}{δᴹ} → [][]Tᴾ {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ}{A}{Aᴹ}{σ}{σᴹ}{δ}{δᴹ}
    ; idlᴹ   = coe= {p₀ = TmsΓΔᴹ= idl}{refl}(UIP _ _)
    ; idrᴹ   = coe= {p₀ = TmsΓΔᴹ= idr}{refl}(UIP _ _)
    ; assᴹ   = coe= {p₀ = TmsΓΔᴹ= ass}{refl}(UIP _ _)
    ; π₁βᴹ   = coe= {p₀ = TmsΓΔᴹ= π₁β}{refl}(UIP _ _)
    ; πηᴹ    = coe= {p₀ = TmsΓΔᴹ= πη}{refl}(UIP _ _)
    ; εηᴹ    = coe= {p₀ = TmsΓΔᴹ= εη}{refl}(UIP _ _)
    ; ,∘ᴹ    = λ {Γ}{Γᴾ}{Δ}{Δᴾ}{Θ}{Θᴾ}{σ}{σᴾ}{δ}{δᴾ}{A}{Aᴾ}{t}{tᴾ} → ,∘ᴾ {Γ}{Γᴾ}{Δ}{Δᴾ}{Θ}{Θᴾ}{σ}{σᴾ}{δ}{δᴾ}{A}{Aᴾ}{t}{tᴾ}
    ; π₂βᴹ   = coe= {p₀ = TmΓᴹ= (A[]T= π₁β) ([]Tᴹ=′ dᴹ c1ᴹ refl refl refl refl refl refl π₁β (coe= (UIP (TmsΓΔᴹ= π₁β) refl))) π₂β}
                    {refl}(UIP _ _)
    }

  cᴹ : Coreᴹ dᴹ
  cᴹ = record
    { c1ᴹ = c1ᴹ
    ; c2ᴹ = c2ᴹ
    }

  bᴹ : Baseᴹ cᴹ
  bᴹ = record
    { Uᴹ    = λ {Γ}{Γᴾ} γ γᴾ α → Lift α → Set
    ; Elᴹ   = λ {Γ}{Γᴾ}{a} aᴾ γ γᴾ α → Lift (aᴾ γ γᴾ α)
    ; U[]ᴹ  = coe= {p₀ = TyΓᴹ= U[]}{refl}(UIP _ _)
    ; El[]ᴹ = cheat
    }

  spᴹ : FuncSPᴹ bᴹ
  spᴹ = record
    { Πᴹ     = λ {Γ}{Γᴾ}{a} aᴾ {B} Bᴾ γ γᴾ f → (α : Lift (⌜ a ⌝t γ))(αᴾ : aᴾ γ γᴾ α) → Bᴾ (γ ,Σ α) (γᴾ ,Σ lift αᴾ) (f (unlift α))
    ; appᴹ   = λ {Γ}{Γᴾ}{a}{aᴾ}{B}{Bᴾ}{t} tᴾ γ γᴾ → tᴾ (proj₁ γ)(proj₁ γᴾ)(proj₂ γ)(unlift (proj₂ γᴾ))
    ; lamᴹ   = λ {Γ}{Γᴾ}{a}{aᴾ}{B}{Bᴾ}{t} tᴾ γ γᴾ → λ α αᴾ → tᴾ (γ ,Σ α)(γᴾ ,Σ lift αᴾ)
    ; Π[]ᴹ   = cheat
    ; lam[]ᴹ = cheat
    ; Πβᴹ    = coe= {p₀ = TmΓAᴹ= TT.FuncSP.Syntax.Πβ}{refl}(UIP _ _)
    ; Πηᴹ    = coe= {p₀ = TmΓAᴹ= TT.FuncSP.Syntax.Πη}{refl}(UIP _ _)
    }

  parᴹ : FuncParᴹ {k = lsuc lzero} cᴹ
  parᴹ = record
    { Πᴹ     = λ {Γ}{Γᴾ} T {B} Bᴾ γ γᴾ f → (α : T) → (Bᴾ α) γ γᴾ (f α)
    ; appᴹ   = λ {Γ}{Γᴾ}{T}{B}{Bᴾ}{t} tᴾ α γ γᴾ → tᴾ γ γᴾ α
    ; lamᴹ   = λ {Γ}{Γᴾ}{T}{B}{Bᴾ}{t} tᴾ γ γᴾ α → tᴾ α γ γᴾ
    ; Π[]ᴹ   = coe= {p₀ = TyΓᴹ= TT.FuncPar.Syntax.Π[]}{refl}(UIP _ _)
    ; lam[]ᴹ = coe= {p₀ = TmΓᴹ= TT.FuncPar.Syntax.Π[] (coe= (UIP (TyΓᴹ= TT.FuncPar.Syntax.Π[]) refl)) TT.FuncPar.Syntax.lam[]}{refl}(UIP _ _)
    ; Πβᴹ    = λ {Γ}{Γᴹ}{A}{B}{Bᴹ} → coe= {p₀ = ap (λ z → (a : A) → Tmᴹ Γᴹ (Bᴹ a) (z a)) TT.FuncPar.Syntax.Πβ}{refl}(UIP _ _)
    ; Πηᴹ    = coe= {p₀ = TmΓAᴹ= TT.FuncPar.Syntax.Πη}{refl}(UIP _ _)
    }
  

open import TT.Core.Elim cᴹ

_ᴾC : (Δ : Con) → ⌜ Δ ⌝C → Set₁
_ᴾT : ∀{Δ}(A : Ty Δ) → (γ : ⌜ Δ ⌝C) → (Δ ᴾC) γ → ⌜ A ⌝T γ → Set₁
_ᴾs : ∀{Δ}{Θ}(σ : Tms Δ Θ) → (γ : ⌜ Δ ⌝C) → (Δ ᴾC) γ → (Θ ᴾC) (⌜ σ ⌝s γ)
_ᴾt : ∀{Δ}{A}(t : Tm Δ A) → (γ : ⌜ Δ ⌝C) → (γᴾ : (Δ ᴾC) γ) → (A ᴾT) γ γᴾ (⌜ t ⌝t γ)


_ᴾC = ElimCon
_ᴾT = ElimTy
_ᴾs = ElimTms
_ᴾt = ElimTm
