{-# OPTIONS --without-K #-}

module test.IWalt where

open import Agda.Primitive
open import lib

data W (S : Set)(P : S → Set) : Set where
  sup : (s : S) → (P s → W S P) → W S P

module _
  {I : Set}
  (S : I → Set)
  (P : ∀{i} → S i → Set)
  (out : ∀{i}{s : S i} → P s → I)
  where

  data IW  : I → Set where
    sup : ∀{i}(s : S i) → ((p : P s) → IW (out p)) → IW i

  module _
    {ℓ : Level}
    (IWᴾ : {i : I} → IW i → Set ℓ)
    (supᴾ : ∀{i}{s : S i}{f : (p : P s) → IW (out p)}
      (fᴾ : (p : P s) → IWᴾ (f p)) → IWᴾ (sup s f))
    where

    elimIW : {i : I}(w : IW i) → IWᴾ w
    elimIW (sup s f) = supᴾ λ p → elimIW (f p)

    elimIWβ : ∀{i}{s : S i}{f : (p : P s) → IW (out p)}
            → elimIW (sup s f) ≡ supᴾ λ p → elimIW (f p)
    elimIWβ = refl

module VecC (A : Set) where
  S : ℕ → Set
  S zero    = ⊤
  S (suc i) = A
  P : ∀{i} → S i → Set
  P {zero} tt = ⊥
  P {suc i} s = ⊤
  out : ∀{i}{s : S i} → P s → ℕ
  out {zero}{tt} ()
  out {suc i}{s} tt = i

Vec : ℕ → Set → Set
Vec n A = IW S P (λ{i}{s} → out {i}{s}) n
  where open VecC A

[] : ∀{A} → Vec 0 A
[] = sup tt λ ()

infixr 5 _∷_
_∷_ : ∀{n A} → A → Vec n A → Vec (suc n) A
x ∷ xs = sup x λ _ → xs

t : Vec 4 ℕ
t = 1 ∷ 2 ∷ 3 ∷ 4 ∷ []

-- I, S, P, out ---> S', P', ind : W S' P' → I, good : W S' P' → Set s.t.   (λ i → Σ (W S' P') λ w → good w × ind w ≡ i) ≃ IW S P out

module VecC' (A : Set) where
  open VecC A

  S' : Set
  S' = Σ ℕ S

  P' : S' → Set
  P' (n ,Σ s) = P s

  Pre : Set
  Pre = W S' P'

  ix : Pre → ℕ
  ix (sup (n ,Σ s) f) = n

  good : Pre → Set
  good (sup (n ,Σ s) f) = (p : P s) → (ix (f p) ≡ out {n}{s} p) × good (f p)

Vec' : ℕ → Set → Set
Vec' n A = Σ Pre λ w → (ix w ≡ n) × good w
  where open VecC' A

[]' : ∀{A} → Vec' 0 A
[]' = sup (0 ,Σ tt) (λ ()) ,Σ (refl ,Σ λ ())

infixr 5 _∷'_
_∷'_ : ∀{n A} → A → Vec' n A → Vec' (suc n) A
_∷'_ {n} {A} x (xs ,Σ p) = sup (suc n ,Σ x) (λ _ → xs) ,Σ (refl ,Σ λ _ → p)

t' : Vec' 4 ℕ
t' = 1 ∷' 2 ∷' 3 ∷' 4 ∷' []'

module Red {I : Set}(S : I → Set)(P : ∀{i} → S i → Set)
  (out : ∀{i}{s : S i} → P s → I) where

  S' : Set
  S' = Σ I S

  P' : S' → Set
  P' (i ,Σ s) = P s

  Pre : Set
  Pre = W S' P'

  ix : Pre → I
  ix (sup (i ,Σ s) f) = i

  good : Pre → Set
  good (sup (i ,Σ s) f) = (p : P s) → (ix (f p) ≡ out p) × good (f p)

  IW' : I → Set
  IW' i = Σ Pre λ w → (ix w ≡ i) × good w

  sup' : ∀{i}(s : S i) → ((p : P s) → IW' (out p)) → IW' i
  sup' {i} s f = sup (i ,Σ s) (λ p → f p .proj₁) ,Σ (refl ,Σ λ p → f p .proj₂)

  module _
    {ℓ : Level}
    (IWᴾ : {i : I} → IW' i → Set ℓ)
    (supᴾ : ∀{i}{s : S i}{f : (p : P s) → IW' (out p)}
      (fᴾ : (p : P s) → IWᴾ (f p)) → IWᴾ (sup' s f))
    where

    elimIW' : {i : I}(w : IW' i) → IWᴾ w
    elimIW' (sup s f ,Σ (refl ,Σ g)) = supᴾ λ p → elimIW' (f p ,Σ g p)

    elimIWβ' : ∀{i}{s : S i}{f : (p : P s) → IW' (out p)}
             → elimIW' (sup' s f) ≡ supᴾ λ p → elimIW' (f p)
    elimIWβ' = refl
