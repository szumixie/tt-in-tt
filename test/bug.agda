module test.bug where

module Simple where

  data A : Set
  data B : A → Set

  data A where
    a : A
    f : (x : A) → B x → A

  data B where
    b : B a

  module _
    (A' : A → Set)(B' : {x : A}(x' : A' x) → B x → Set)
    (a' : A' a)(f' : {x : A}(x' : A' x){y : B x} → B' x' y → A' (f x y))
    (b' : B' a' b)
    where
    elimA : (x : A) → A' x
    elimB : {x : A}(y : B x) → B' (elimA x) y

    elimA a = a'
    elimA (f x y) = f' (elimA x) (elimB y)
    elimB b = b'

-- 1. implement elimA, elimB
-- 2. do your trick
-- 3. define the eliminator for the tricky encoded A-B
-- 4. and see if you can still implement the original eliminator for A-B

module Trick where

  data I : Set
  data P : I → Set

  data I where
    A : I
    B : P A → I

  data P where
    a : P A
    f : (x : P A) → P (B x) → P A
    b : P (B a)

  module _
    (I' : I → Set)(P' : {x : I}(x' : I' x) → P x → Set)
    (A' : I' A)(B' : {y : P A} → P' A' y → I' (B y))
    (a' : P' A' a)(f' : {x : P A}(x' : P' A' x){y : P (B x)} → P' (B' x') y →  P' A' (f x y))
    (b' : P' (B' a') b)
    where

    elimI : (x : I) → I' x
    elimP : {x : I}(y : P x) → P' (elimI x) y

    elimI A = A'
    elimI (B x) = B' {x} (elimP x)
    elimP a = a'
    elimP (f x y) = f' (elimP x) (elimP y)
    elimP b = b'

module Simpler where
  
  data A : Set
  data B : A → Set

  data A where
    a : A
    f : B a → A

  data B where

  module rec
    (A' : Set)(B' : A' → Set)
    (a' : A')(f' : B' a' → A')
    where

    elimA : (x : A) → A'
    elimB : {x : A}(y : B x) → B' (elimA x)

    elimA a = a'
    elimA (f x) = f' (elimB x)
    elimB ()


  module elim
    (A' : A → Set)(B' : {x : A}(x' : A' x) → B x → Set)
    (a' : A' a)(f' : {y : B a} → B' a' y → A' (f y))
    where

    elimA : (x : A) → A' x
    elimB : {x : A}(y : B x) → B' (elimA x) y

    elimA a = a'
    elimA (f x) = f' (elimB x)
    elimB ()
