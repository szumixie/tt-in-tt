{-# OPTIONS --cubical #-}

module test.cubical where

open import Cubical.Core.Prelude renaming (_,_ to _,Σ_) public

mutual
  data Ix : Set where
    Conᵢ : Ix
    Tyᵢ  : Con → Ix
    Tmsᵢ : Con → Con → Ix
    Tmᵢ  : (Γ : Con) → Ty Γ → Ix

  Con : Set -- objects
  Ty  : Con → Set
  Tms : Con → Con → Set -- morphisms
  Tm  : (Γ : Con) → Ty Γ → Set
  Con     = ⟦ Conᵢ ⟧
  Ty  Γ   = ⟦ Tyᵢ Γ ⟧
  Tms Γ Δ = ⟦ Tmsᵢ Γ Δ ⟧
  Tm  Γ Δ = ⟦ Tmᵢ Γ Δ ⟧

  infixl 5 _▷_
  infixl 7 _[_]T
  infixl 5 _,_
  infix  6 _∘_
  infixl 8 _[_]t

  data ⟦_⟧ : Ix → Set where
    ∙     : Con
    _▷_   : (Γ : Con) → Ty Γ → Con

    _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
    
    id    : ∀{Γ} → Tms Γ Γ -- identity morphism
    _∘_   : ∀{Γ Δ Θ} → Tms Δ Θ → Tms Γ Δ → Tms Γ Θ -- compose morphisms
    ε     : ∀{Γ} → Tms Γ ∙
    _,_   : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
    π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▷ A) → Tms Γ Δ

    π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ σ ]T)
    _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)

    [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
    [][]T : ∀{Γ Δ Θ}{A : Ty Θ}{σ : Tms Δ Θ}{δ : Tms Γ Δ}
          → A [ σ ]T [ δ ]T ≡ A [ σ ∘ δ ]T

    ass   : ∀{Γ Δ Θ Ω}{σ : Tms Θ Ω}{δ : Tms Δ Θ}{ν : Tms Γ Δ}
            → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν) -- composition is associative
    idl   : ∀{Γ Δ}{σ : Tms Γ Δ} → id ∘ σ ≡ σ -- left identity
    idr   : ∀{Γ Δ}{σ : Tms Γ Δ} → σ ∘ id ≡ σ -- right identity

    ∙η    : ∀{Γ}{σ : Tms Γ ∙} → σ ≡ ε
    ▷β₁   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{a : Tm Γ (A [ σ ]T)}
          → π₁ (σ , a) ≡ σ
    ▷β₂   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{a : Tm Γ (A [ σ ]T)}
          → transport (cong (Tm _) (cong (A [_]T) ▷β₁)) (π₂ (σ , a)) ≡ a
    ▷η    : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ ▷ A)}
          → π₁ σ , π₂ σ ≡ σ
    ,∘    : ∀{Γ Δ Θ}{σ : Tms Γ Δ}{δ : Tms Θ Γ}{A : Ty Δ}{a : Tm Γ (A [ σ ]T)}
          → (σ , a) ∘ δ ≡ σ ∘ δ , transport (cong (Tm _) [][]T) (a [ δ ]t)

    U     : ∀{Γ} → Ty Γ
    El    : ∀{Γ} → Tm Γ U → Ty Γ

    U[]   : ∀{Γ Δ}{σ : Tms Γ Δ} → U [ σ ]T ≡ U
    El[]  : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
          → El a [ σ ]T ≡ El (transport (cong (Tm _) U[]) (a [ σ ]t))

    -- ⊤
    -- ⊥

    Π     : ∀{Γ}(A : Ty Γ) → Ty (Γ ▷ A) → Ty Γ
    lam   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)} → Tm (Γ ▷ A) B → Tm Γ (Π A B)
    app   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)} → Tm Γ (Π A B) → Tm (Γ ▷ A) B

    Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{t : Tm (Γ ▷ A) B}
          → app (lam t) ≡ t
    Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{t : Tm Γ (Π A B)}
          → lam (app t) ≡ t
    Π[]   : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▷ A)}
          → Π A B [ σ ]T ≡ Π (A [ σ ]T)
              (B [ σ ∘ π₁ id , transport (cong (Tm _) [][]T) (π₂ id) ]T)
    {-
    lam[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▷ A)}{t : Tm (Δ ▷ A) B}
          → transport ((cong (Tm _) Π[])) (lam t [ σ ]t)
              ≡ lam {Γ}{A [ σ ]T} (t [ σ ∘ π₁ id , transp (λ i → Tm (Γ ▷ A [ σ ]T) ((([][]T {A = A}{σ = π₁ id}{δ = σ}) i))) i0
                                                          (π₂ {Γ ▷ A [ σ ]T} id) ]t)
    -}

    -- truncation
    setTy  : ∀{Γ}{A₀ A₁ : Ty Γ}(p q : A₀ ≡ A₁) → p ≡ q
    setTms : ∀{Γ Δ}{σ₀ σ₁ : Tms Γ Δ}(p q : σ₀ ≡ σ₁) → p ≡ q
    setTm  : ∀{Γ A}{t₀ t₁ : Tm Γ A}(p q : t₀ ≡ t₁) → p ≡ q
    -- without these 3 constructors it is called wild syntax

    -- 5. grpTy  : ∀{Γ}{A₀ A₁ : Ty Γ}{p q : A₀ ≡ A₁}(r s : p ≡ q) → r ≡ s

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▷ A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▷ A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ ▷ B) (A [ wk ]T)
vs = _[ wk ]t

infix 4 ⟨_⟩

⟨_⟩ : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▷ A)
⟨ a ⟩ = id , transport (cong (Tm _) (sym [id]T)) a

infixl 5 _↑_

_↑_ : ∀{Γ Δ}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▷ (A [ σ ]T)) (Δ ▷ A)
σ ↑ _ = σ ∘ wk , transport (cong (Tm _) [][]T) vz

module twoDifferentPaths where

  w : U[] {∙}{∙}{id} ≡ [id]T
  w = setTy _ _

module test where
  open import Cubical.Data.Nat

  countΠTy : ∀{Γ} → Ty Γ → ℕ
  countΠTy (A [ σ ]T) = countΠTy A
  countΠTy ([id]T {A = A} _) = countΠTy A
  countΠTy ([][]T {A = A} _) = countΠTy A
  countΠTy U        = 0
  countΠTy (El x)   = 0
  countΠTy (U[] j)  = 0 -- (λ j → 0) : countΠTy (U [ σ ]T) ≡ countΠTy U
  -- cong countΠTy U[] = refl {0}
  countΠTy (El[] _) = 0
  countΠTy (Π A B)  = 1 + countΠTy A + countΠTy B
  countΠTy (Π[] {A = A} {B = B} _) = 1 + countΠTy A + countΠTy B
  countΠTy (setTy {Γ}{A₀}{A₁} p q i j) = isSetℕ (countΠTy A₀)(countΠTy A₁)(λ k → countΠTy (p k))(λ k → countΠTy (q k)) i j
  -- p : A₀ ≡ A₁
  -- q : A₀ ≡ A₁
  -- countΠTy (p i) ≡ countΠTy (q i)

  countΠ : Con → ℕ
  countΠ ∙       = 0
  countΠ (Γ ▷ A) = countΠ Γ + countΠTy A

  t1 : countΠ ∙ ≡ 0
  t1 = refl

  t3 : countΠ (∙ ▷ (Π (Π U U) (Π U (transport (cong Ty (cong (∙ ▷_) (U[] {∙}{∙}{ε}))) (U {∙ ▷ U [ ε ]T})) [ wk ]T))) ≡ 3
  t3 = {!!}


  A : Ty (∙ ▷ U)
  A = transp (λ i → Ty (∙ ▷ U[] {∙}{∙}{ε} i)) i0 U

  e : PathP _ (Ty (∙ ▷ U [ ε ]T)) (Ty (∙ ▷ U))
  e i = ⟦ Tyᵢ (∙ ▷ U[] {∙}{∙}{ε} i) ⟧

module interpreter where

  open import Cubical.Data.Unit
  open import Cubical.Data.Sum

  

  ieval : Ix → Set₁
  eval  : {i : Ix} → ⟦ i ⟧ → ieval i

  ieval Conᵢ = Set
  ieval (Tyᵢ Γ) = Set → eval Γ
  ieval (Tmsᵢ x x₁) = {!!}
  ieval (Tmᵢ Γ x) = {!!}

  eval a = {!!}
{-

  -- this should be called eval:
  ⟦_⟧C : Con → Set
  ⟦_⟧T : {Γ : Con} → Ty Γ → ⟦ Γ ⟧C → Set
  ⟦_⟧s : {Γ Δ : Con} → Tms Γ Δ → ⟦ Γ ⟧C → ⟦ Δ ⟧C 
  ⟦_⟧t : {Γ : Con}{A : Ty Γ} → Tm Γ A → (γ : ⟦ Γ ⟧C) → ⟦ A ⟧T γ

  ⟦ ∙ ⟧C = Unit
  ⟦ Γ ▷ A ⟧C = Σ ⟦ Γ ⟧C ⟦ A ⟧T
  ⟦ A [ σ ]T ⟧T γ = ⟦ A ⟧T (⟦ σ ⟧s γ)
  ⟦ [id]T {A = A} i ⟧T γ = ⟦ A ⟧T γ
  ⟦ [][]T i ⟧T = {!!}
  ⟦ U ⟧T = {!!}
  ⟦ El x ⟧T = {!!}
  ⟦ U[] i ⟧T = {!!}
  ⟦ El[] i ⟧T = {!!}
  ⟦ Π A B ⟧T = {!!}
  ⟦ Π[] i ⟧T = {!!}
  ⟦ setTy p q i j ⟧T = {!!}
  ⟦ id ⟧s γ = γ
  ⟦ x ∘ x₁ ⟧s = {!!}
  ⟦ ε ⟧s = {!!}
  ⟦ σ , x ⟧s = {!!}
  ⟦ π₁ x ⟧s = {!!}
  ⟦ ass i ⟧s = {!!}
  ⟦ idl i ⟧s = {!!}
  ⟦ idr i ⟧s = {!!}
  ⟦ ∙η i ⟧s = {!!}
  ⟦ ▷β₁ i ⟧s = {!!}
  ⟦ ▷η i ⟧s = {!!}
  ⟦ ,∘ i ⟧s = {!!}
  ⟦ setTms p q i i₁ ⟧s = {!!}
  ⟦_⟧t = {!!}
-}

-- 1. define eval to Set (and leave some holes)
-- 2. define eval to hProp
-- 2a. prove isSet Con (simple)

-- open import Cubical.Foundations.HAEquiv using (isSetHProp)

-- 3. define eval to Bool
-- 4. INTERESTING: define eval to ℕ
-- 5. VERY INTERESTING: change the syntax so that we don't have to set-truncate, but groupoid-truncate (we call this coherent syntax)
-- 6. then, you can define eval to hSet
-- 7. we can prove that Con,Ty,Tms,Tm is an hSet
