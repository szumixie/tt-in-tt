{-# OPTIONS --rewriting #-}

module test.countpi where

open import lib
open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.Func
open import TT.Rec
open import TT.Syntax
open import TT.Examples

infixl 6 _+_

_+_ : ℕ → ℕ → ℕ
zero  + m = m
suc n + m = suc (n + m)

{-# BUILTIN NATPLUS _+_ #-}

module _ where
  open Decl
  open Core
  open Core1
  open Core2
  open Base
  open Func

  declΠ : Decl
  declΠ .Con     = ℕ
  declΠ .Ty  _   = ℕ
  declΠ .Tms _ _ = ⊤
  declΠ .Tm  _ _ = ⊤

  coreΠ : Core declΠ
  coreΠ .c1 .•           = 0
  coreΠ .c1 ._,_         = _+_
  coreΠ .c1 ._[_]T  n tt = n
  coreΠ .c1 .id          = tt
  coreΠ .c1 ._∘_   tt tt = tt
  coreΠ .c1 .ε           = tt
  coreΠ .c1 ._,s_  tt tt = tt
  coreΠ .c1 .π₁       tt = tt
  coreΠ .c1 ._[_]t tt tt = tt
  coreΠ .c1 .π₂       tt = tt
  coreΠ .c2 .[id]T = refl
  coreΠ .c2 .[][]T = refl
  coreΠ .c2 .idl   = refl
  coreΠ .c2 .idr   = refl
  coreΠ .c2 .ass   = refl
  coreΠ .c2 .,∘    = refl
  coreΠ .c2 .π₁β   = refl
  coreΠ .c2 .πη    = refl
  coreΠ .c2 .εη    = refl
  coreΠ .c2 .π₂β   = refl

  baseΠ : Base coreΠ
  baseΠ .U     = 0
  baseΠ .U[]   = refl
  baseΠ .El tt = 0
  baseΠ .El[]  = refl

  funcΠ : Func coreΠ
  funcΠ .Π  A B = 1 + A + B
  funcΠ .Π[]    = refl
  funcΠ .lam tt = tt
  funcΠ .app tt = tt
  funcΠ .lam[]  = refl
  funcΠ .Πβ     = refl
  funcΠ .Πη     = refl

countΠ : Con → ℕ
countΠ = RecCon baseΠ funcΠ

t1 : countΠ • ≡ 0
t1 = refl

t2 : countΠ Γ ≡ 1
t2 = refl

t3 : countΠ (• , ((U ⇒ U) ⇒ U ⇒ U)) ≡ 3
t3 = refl
