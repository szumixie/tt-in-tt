{-# OPTIONS --safe --without-K --postfix-projections #-}

module test.FinTypes where

open import Relation.Binary.PropositionalEquality
open import Relation.Nullary
open import Function
open import Data.Nat as ℕ
  using (ℕ; zero; suc)
import Data.Nat.Properties as ℕ
open import Data.Fin
open import Data.Fin.Properties

-- Π_{i=0}^n f(i)
Πᶠ : ∀ n → (Fin n → ℕ) → ℕ
Πᶠ zero    f = 1
Πᶠ (suc n) f = f zero ℕ.+ Πᶠ n (f ∘ suc)

lamᶠ : ∀{n}{f : Fin n → ℕ} → ((i : Fin n) → Fin (f i)) → Fin (Πᶠ n f)
lamᶠ = {!!}

appᶠ : ∀{n}{f : Fin n → ℕ} → Fin (Πᶠ n f) → (j : Fin n) → Fin (f j)
appᶠ = {!!}

-- Σ_{i=0}^n f(i)
Σᶠ : ∀ n → (Fin n → ℕ) → ℕ
Σᶠ zero    f = 0
Σᶠ (suc n) f = f zero ℕ.+ Σᶠ n (f ∘ suc)

-- j + Σ_{k=0}^i f(k)
_,ᶠ_ : ∀{n}{f : Fin n → ℕ}(i : Fin n) → Fin (f i) → Fin (Σᶠ n f)
_,ᶠ_ {suc n}{f} zero j = subst Fin
  (ℕ.+-comm (Σᶠ n (f ∘ suc)) (f zero))
  (raise (Σᶠ n (f ∘ suc)) j)
_,ᶠ_ {suc n}{f} (suc i) j = subst
  (Fin ∘ (ℕ._+ Σᶠ n (f ∘ suc)))
  (toℕ-fromℕ (f zero))
  (fromℕ (f zero) + _,ᶠ_ {n}{f ∘ suc} i j)

proj₁ᶠ : ∀{n}{f : Fin n → ℕ} → Fin (Σᶠ n f) → Fin n
proj₁ᶠ {suc n} {f} i with toℕ i ℕ.≤? f zero
proj₁ᶠ {suc n} {f} i | yes p = zero
proj₁ᶠ {suc n} {f} i | no ¬p = suc (proj₁ᶠ {n}{f ∘ suc}
  {!fromℕ (f zero)!})

⊤ᶠ : ℕ
⊤ᶠ = 1

ttᶠ : Fin ⊤ᶠ
ttᶠ = zero

⊤ηᶠ : ∀{i} → i ≡ ttᶠ
⊤ηᶠ {zero} = refl

⊥ᶠ : ℕ
⊥ᶠ = 0

exfalsoᶠ : ∀{n} → Fin ⊥ᶠ → Fin n
exfalsoᶠ ()

_⊎_ : ℕ → ℕ → ℕ
_⊎_ = ℕ._+_

inj₁ᶠ : ∀{n m} → Fin n → Fin (n ⊎ m)
inj₁ᶠ {n}{m} i = subst Fin (ℕ.+-comm m n) (raise m i)

inj₂ᶠ : ∀{n m} → Fin m → Fin (n ⊎ m)
inj₂ᶠ {n}{m} i = subst (Fin ∘ _⊎ m) (toℕ-fromℕ n) (fromℕ n + i)

caseᶠ : ∀{n m}(f : Fin (n ⊎ m) → ℕ)
      → ((i : Fin n) → Fin (f (inj₁ᶠ i)))
      → ((j : Fin m) → Fin (f (inj₂ᶠ j)))
      → (k : Fin (n ⊎ m)) → Fin (f k)
caseᶠ f g h k = {!!}
