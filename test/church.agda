{-# OPTIONS --rewriting #-}

module test.church where

open import lib hiding (ℕ)
open import TT.Syntax
open import TT.Examples using (_⇒_; _$⇒_; ⇒[])

vzU : ∀{Γ} → Ty (Γ , U)
vzU = El (coe (ap (Tm _) U[]) vz)

𝔽 : ∀{Γ} → Ty Γ
𝔽 = Π U vzU

𝕋 : ∀{Γ} → Ty Γ
𝕋 = Π U (vzU ⇒ vzU)

Id : ∀{Γ} → Tm Γ 𝕋
Id = lam (lam vz)

𝔹 : ∀{Γ} → Ty Γ
𝔹 = Π U (vzU ⇒ vzU ⇒ vzU)

True : ∀{Γ} → Tm Γ 𝔹
True = lam (lam (coe (ap (Tm _) (⇒[] ⁻¹)) (lam (vs vz))))

False : ∀{Γ} → Tm Γ 𝔹
False = lam (lam (vs (lam vz)))
