{-# OPTIONS --without-K #-}

module test.BoolTypes where

open import lib

[_] : Bool → Set
[ true ]  = ⊤
[ false ] = ⊥

-- ⋀ ~ Π
⋀ : (A : Bool) → ([ A ] → Bool) → Bool
⋀ true  B = B tt
⋀ false B = true

⋀-lam : ∀{A B} → ((u : [ A ]) → [ B u ]) → [ ⋀ A B ]
⋀-lam {true}  f = f tt
⋀-lam {false} f = tt

⋀-app : ∀{A B} → [ ⋀ A B ] → (u : [ A ]) → [ B u ]
⋀-app {true} t u = t

⋀β : ∀{A B t} → ⋀-app {A}{B} (⋀-lam t) ≡ t
⋀β {true}  = refl
⋀β {false} = funext λ ()

⋀η : ∀{A B t} → ⋀-lam {A}{B} (⋀-app t) ≡ t
⋀η {true}  = refl
⋀η {false} = refl

-- ⋁ ~ Σ
⋁ : (A : Bool) → ([ A ] → Bool) → Bool
⋁ true  B = B tt
⋁ false B = false

_,⋁_ : ∀{A B}(u : [ A ]) → [ B u ] → [ ⋁ A B ]
_,⋁_ {true}  u v = v
_,⋁_ {false} u v = u

⋁-proj₁ : ∀{A B} → [ ⋁ A B ] → [ A ]
⋁-proj₁ {true}  t = tt
⋁-proj₁ {false} t = t

⋁-proj₂ : ∀{A B}(t : [ ⋁ A B ]) → [ B (⋁-proj₁ t) ]
⋁-proj₂ {true} t = t

⋁β₁ : ∀{A B u v} → ⋁-proj₁ {A}{B} (u ,⋁ v) ≡ u
⋁β₁ {true}  = refl
⋁β₁ {false} = refl

⋁β₂ : ∀{A B u v} → ⋁-proj₂ {A}{B} (u ,⋁ v) ≡[ ap (λ t → [ B t ]) (⋁β₁ {A}{B}{u}{v}) ]≡ v
⋁β₂ {true} = refl

⋁η : ∀{A B t} → ⋁-proj₁ {A}{B} t ,⋁ ⋁-proj₂ {A}{B} t ≡ t
⋁η {true}  = refl
⋁η {false} = refl

-- ∨ ~ +
_∨_ : Bool → Bool → Bool
true  ∨ B = true
false ∨ B = B

∨-inj₁ : ∀{A B} → [ A ] → [ A ∨ B ]
∨-inj₁ {true} u = tt

∨-inj₂ : ∀{A B} → [ B ] → [ A ∨ B ]
∨-inj₂ {true}  v = tt
∨-inj₂ {false} v = v

∨-case : ∀{A B}(P : [ A ∨ B ] → Bool)
       → ((u : [ A ]) → [ P (∨-inj₁ u) ])
       → ((v : [ B ]) → [ P (∨-inj₂ v) ])
       → (t : [ A ∨ B ]) → [ P t ]
∨-case {true}  P f g t = f tt
∨-case {false} P f g t = g t

∨β₁ : ∀{A B P f g u} → ∨-case {A}{B} P f g (∨-inj₁ u) ≡ f u
∨β₁ {true} = refl

∨β₂ : ∀{A B P f g v} → ∨-case {A}{B} P f g (∨-inj₂ v) ≡ g v
∨β₂ {true}{P = P} with P tt
∨β₂ {true}{P = P}    | true  = refl
∨β₂ {true}{P = P}{f} | false = ⊥-elim (f tt)
∨β₂ {false} = refl
