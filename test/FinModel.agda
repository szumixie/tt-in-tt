{-# OPTIONS --without-K --postfix-projections #-}

module test.FinModel where

open import lib
open import TT.Decl
open import TT.Core

infixl 6 _+_

_+_ : ℕ → ℕ → ℕ
zero  + m = m
suc n + m = suc (n + m)
{-# BUILTIN NATPLUS _+_ #-}

-- Σ_{i=0}^n f(i)
Σᶠ : ∀ n → (Fin n → ℕ) → ℕ
Σᶠ zero    f = 0
Σᶠ (suc n) f = f zero + Σᶠ n λ i → f (suc i)

-- j + Σ_{k=0}^i f(k)
_,ᶠ_ : ∀{n}{f : Fin n → ℕ}(i : Fin n) → Fin (f i) → Fin (Σᶠ n f)
_,ᶠ_ {suc n} {f} zero j = {!!}
_,ᶠ_ {suc n} {f} (suc i) j = {!!}

module _ where
  open Decl

  d : Decl
  d .Con     = ℕ
  d .Ty  Γ   = Fin Γ → ℕ
  d .Tms Γ Δ = Fin Γ → Fin Δ
  d .Tm  Γ A = (γ : Fin Γ) → Fin (A γ)

open Decl d

module _ where
  open Core
  open Core1

  c : Core d
  c .c1 .• = 1
  c .c1 ._,_ = Σᶠ
  c .c1 ._[_]T A σ γ = A (σ γ)
  c .c1 .id γ = γ
  c .c1 ._∘_ σ τ γ = σ (τ γ)
  c .c1 .ε γ = zero
  c .c1 ._,s_ σ t γ = σ γ ,ᶠ t γ
  c .c1 .π₁ = {!!}
  c .c1 ._[_]t = {!!}
  c .c1 .π₂ = {!!}
  c .c2 = {!!}
