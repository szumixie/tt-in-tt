{-# OPTIONS --without-K --prop --postfix-projections #-}

module test.IxRed.ConTyProp where

open import lib
open import Setoid.SProp.lib

module Pre where
  infixr 5 _▷_

  data Con : Set
  data Ty  : Set

  data Con where
    ∙   : Con
    _▷_ : Con → Ty → Con
  data Ty where
    U : Con → Ty
    Π : Con → Ty → Ty → Ty

  record Motive : Set₁ where
    infixr 5 _▷ᴾ_
    field
      Conᴾ : Con → Set
      Tyᴾ  : Ty → Set
      ∙ᴾ   : Conᴾ ∙
      _▷ᴾ_ : ∀{Γ} → Conᴾ Γ → ∀{A} → Tyᴾ A → Conᴾ (Γ ▷ A)
      Uᴾ   : ∀{Γ} → Conᴾ Γ → Tyᴾ (U Γ)
      Πᴾ   : ∀{Γ} → Conᴾ Γ → ∀{A} → Tyᴾ A → ∀{B} → Tyᴾ B → Tyᴾ (Π Γ A B)

  module Elim (P : Motive) where
    open Motive P
    Conᴱ : ∀ Γ → Conᴾ Γ
    Tyᴱ  : ∀ A → Tyᴾ A

    Conᴱ ∙       = ∙ᴾ
    Conᴱ (Γ ▷ A) = Conᴱ Γ ▷ᴾ Tyᴱ A
    Tyᴱ (U Γ)     = Uᴾ (Conᴱ Γ)
    Tyᴱ (Π Γ A B) = Πᴾ (Conᴱ Γ) (Tyᴱ A) (Tyᴱ B)

  open Motive public

record Alg : Set₁ where
  infixr 5 _▷_
  field
    Con : Set
    Ty  : Con → Set
    ∙   : Con
    _▷_ : (Γ : Con) → Ty Γ → Con
    U   : ∀{Γ} → Ty Γ
    Π   : ∀{Γ}(A : Ty Γ) → Ty (Γ ▷ A) → Ty Γ

record Motive (T : Alg) : Set₁ where
  open Alg T
  infixr 5 _▷ᴾ_
  field
    Conᴾ : Con → Set
    Tyᴾ  : ∀ Γ → Ty Γ → Set
    ∙ᴾ   : Conᴾ ∙
    _▷ᴾ_ : ∀{Γ} → Conᴾ Γ → ∀{A} → Tyᴾ Γ A → Conᴾ (Γ ▷ A)
    Uᴾ   : ∀{Γ} → Conᴾ Γ → Tyᴾ Γ U
    Πᴾ   : ∀{Γ} → Conᴾ Γ → ∀{A} → Tyᴾ Γ A → ∀{B} → Tyᴾ (Γ ▷ A) B → Tyᴾ Γ (Π A B)

record Elim {T : Alg}(P : Motive T) : Set where
  open Alg T
  open Motive P
  field
    Conᴱ : ∀ Γ → Conᴾ Γ
    Tyᴱ  : ∀ Γ A → Tyᴾ Γ A
    ∙ᴱ   : Conᴱ ∙ ≡ ∙ᴾ
    ▷ᴱ   : ∀{Γ A} → Conᴱ (Γ ▷ A) ≡ Conᴱ Γ ▷ᴾ Tyᴱ Γ A
    Uᴱ   : ∀{Γ} → Tyᴱ Γ U ≡ Uᴾ (Conᴱ Γ)
    Πᴱ   : ∀{Γ A B} → Tyᴱ Γ (Π A B) ≡ Πᴾ (Conᴱ Γ) (Tyᴱ Γ A) (Tyᴱ (Γ ▷ A) B)

elim : Alg → Set₁
elim T = (P : Motive T) → Elim P

module _ where
  open Pre
  _≡ᶜ_ : Con → Con → Prop
  _≡ᵗ_ : Ty → Ty → Prop

  ∙ ≡ᶜ ∙ = ⊤p
  (Γ ▷ A) ≡ᶜ (Γ′ ▷ A′) = Γ ≡ᶜ Γ′ ×p A ≡ᵗ A′
  _ ≡ᶜ _ = ⊥p

  U Γ ≡ᵗ U Γ′ = Γ ≡ᶜ Γ′
  Π Γ A B ≡ᵗ Π Γ′ A′ B′ = Γ ≡ᶜ Γ′ ×p A ≡ᵗ A′ ×p B ≡ᵗ B′
  _ ≡ᵗ _ = ⊥p

  reflᶜ : ∀{Γ} → Γ ≡ᶜ Γ
  reflᵗ : ∀{A} → A ≡ᵗ A

  reflᶜ {∙}     = ttp
  reflᶜ {Γ ▷ A} = reflᶜ ,p reflᵗ {A}
  reflᵗ {U Γ}     = reflᶜ
  reflᵗ {Π Γ A B} = reflᶜ ,p reflᵗ {A} ,p reflᵗ {B}

  transpᶜ : (P : Con → Set) {Γ₀ Γ₁ : Con} → Γ₀ ≡ᶜ Γ₁ → P Γ₀ → P Γ₁
  transpᵗ : (P : Ty  → Set) {A₀ A₁ : Ty}  → A₀ ≡ᵗ A₁ → P A₀ → P A₁

  transpᶜ P {∙} {∙} Γ₀₁ p = p
  transpᶜ P {Γ₀ ▷ A₀} {Γ₁ ▷ A₁} (Γ₀₁ ,p A₀₁) p
    = transpᶜ (λ Γ → P (Γ ▷ A₁)) Γ₀₁ (transpᵗ (λ A → P (Γ₀ ▷ A)) A₀₁ p)
  transpᵗ P {U Γ₀} {U Γ₁} Γ₀₁ p = transpᶜ (λ Γ → P (U Γ)) Γ₀₁ p
  transpᵗ P {Π Γ₀ A₀ B₀} {Π Γ₁ A₁ B₁} (Γ₀₁ ,p A₀₁ ,p B₀₁) p
    = transpᶜ (λ Γ → P (Π Γ A₁ B₁)) Γ₀₁
      (transpᵗ (λ A → P (Π Γ₀ A B₁)) A₀₁
        (transpᵗ (λ B → P (Π Γ₀ A₀ B)) B₀₁ p))

  goodCon : Con → Prop
  goodTy  : Con → Ty → Prop

  goodCon ∙ = ⊤p
  goodCon (Γ ▷ A) = goodCon Γ ×p goodTy Γ A
  goodTy Γ′ (U Γ) = goodCon Γ ×p (Γ′ ≡ᶜ Γ)
  goodTy Γ′ (Π Γ A B) = goodCon Γ ×p goodTy Γ A ×p goodTy (Γ ▷ A) B ×p (Γ′ ≡ᶜ Γ)

module _ where
  open Alg

  T : Alg
  T .Con = Σsp Pre.Con goodCon
  T .Ty (Γ ,sp _) = Σsp Pre.Ty (goodTy Γ)
  T .∙ = Pre.∙ ,sp ttp
  T ._▷_ (Γ ,sp gΓ) (A ,sp gA) = (Γ Pre.▷ A) ,sp (gΓ ,p gA)
  T .U {Γ ,sp gΓ} = Pre.U Γ ,sp (gΓ ,p reflᶜ)
  T .Π {Γ ,sp gΓ} (A ,sp gA) (B ,sp gB) = Pre.Π Γ A B ,sp (gΓ ,p gA ,p gB ,p reflᶜ)

open Alg T

T-elim : elim T
T-elim P = E where
  open Motive P
  open Elim

  P′ : Pre.Motive
  P′ .Pre.Conᴾ Γ = ∀ gΓ → Conᴾ (Γ ,sp gΓ)
  P′ .Pre.Tyᴾ  A = ∀ Γ gΓ gA → Tyᴾ (Γ ,sp gΓ) (A ,sp gA)
  P′ .Pre.∙ᴾ _ = ∙ᴾ
  P′ .Pre._▷ᴾ_ Γᴾ Aᴾ (gΓ ,p gA) = Γᴾ gΓ ▷ᴾ Aᴾ _ _ gA
  P′ .Pre.Uᴾ {Γ} Γᴾ Γ₀ gΓ₀ (gΓ ,p Γ₌) = transpᶜ
    (λ Γ → (gΓ : goodCon Γ)(Γ₌ : Γ₀ ≡ᶜ Γ)
         → Conᴾ (Γ ,sp gΓ) → Tyᴾ (Γ₀ ,sp gΓ₀) (Pre.U Γ ,sp (gΓ ,p Γ₌)))
    Γ₌ (λ _ _ Γᴾ → Uᴾ Γᴾ) gΓ Γ₌ (Γᴾ gΓ)
  P′ .Pre.Πᴾ {Γ} Γᴾ {A} Aᴾ {B} Bᴾ Γ₀ gΓ₀ (gΓ ,p gA ,p gB ,p Γ₌) = transpᶜ
    (λ Γ → (gΓ : goodCon Γ)(gA : goodTy Γ A)(gB : goodTy (Γ Pre.▷ A) B)(Γ₌ : Γ₀ ≡ᶜ Γ)
         → Conᴾ (Γ ,sp gΓ) → Tyᴾ (Γ ,sp gΓ) (A ,sp gA)
         → Tyᴾ ((Γ Pre.▷ A) ,sp (gΓ ,p gA)) (B ,sp gB)
         → Tyᴾ (Γ₀ ,sp gΓ₀) (Pre.Π Γ A B ,sp (gΓ ,p gA ,p gB ,p Γ₌)))
    Γ₌ (λ _ _ _ _ Γᴾ Aᴾ Bᴾ → Πᴾ Γᴾ Aᴾ Bᴾ) gΓ gA gB Γ₌ (Γᴾ gΓ) (Aᴾ _ _ gA) (Bᴾ _ _ gB)

  module PreElim = Pre.Elim P′
  E : Elim P
  E .Conᴱ (Γ ,sp gΓ) = PreElim.Conᴱ Γ gΓ
  E .Tyᴱ  (Γ ,sp gΓ) (A ,sp gA) = PreElim.Tyᴱ A Γ gΓ gA
  E .∙ᴱ = refl
  E .▷ᴱ = refl
  E .Uᴱ = {!!}
  E .Πᴱ = {!!}
