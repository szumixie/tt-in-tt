{-# OPTIONS --safe --cubical --postfix-projections #-}

module test.IxRed.SizedBagAlt where

open import Cubical.Foundations.Prelude
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Prod

module _ (A : Set) where
  record Alg : Set₁ where
    infixr 5 _∷_
    field
      SizedBag : ℕ → Set
      []       : SizedBag 0
      _∷_      : A → ∀{n} → SizedBag n → SizedBag (suc n)
      comm     : ∀ x y {n}(xs : SizedBag n) → x ∷ y ∷ xs ≡ y ∷ x ∷ xs

  record Motive (T : Alg) : Set₁ where
    open Alg T
    infixr 5 _∷ᴾ_
    field
      SizedBagᴾ : ∀{n} → SizedBag n → Set
      []ᴾ       : SizedBagᴾ []
      _∷ᴾ_      : ∀ x {n}{xs : SizedBag n} → SizedBagᴾ xs → SizedBagᴾ (x ∷ xs)
      commᴾ     : ∀ x y {n}{xs : SizedBag n}(xsᴾ : SizedBagᴾ xs)
                → PathP (λ i → SizedBagᴾ (comm x y xs i))
                        (x ∷ᴾ y ∷ᴾ xsᴾ) (y ∷ᴾ x ∷ᴾ xsᴾ)

  record Elim {T : Alg}(P : Motive T) : Set where
    open Alg T
    open Motive P
    infixr 5 _∷ᴱ_
    field
      SizedBagᴱ : ∀{n}(xs : SizedBag n) → SizedBagᴾ xs
      []ᴱ       : SizedBagᴱ [] ≡ []ᴾ
      _∷ᴱ_      : ∀ x {n}(xs : SizedBag n) → SizedBagᴱ (x ∷ xs) ≡ x ∷ᴾ SizedBagᴱ xs
      {-commᴱ     : ∀ x y {n}(xs : SizedBag n)
                → PathP (λ i → SizedBagᴱ (comm x y xs i) ≡ commᴾ x y (SizedBagᴱ xs) i)
                        ((x ∷ᴱ (y ∷ xs)) ∙ cong (x ∷ᴾ_) (y ∷ᴱ xs))
                        ((y ∷ᴱ (x ∷ xs)) ∙ cong (y ∷ᴾ_) (x ∷ᴱ xs))-}

  elim : Alg → Set₁
  elim T = (P : Motive T) → Elim P

  module Native where
    infixr 5 _∷_
    data SizedBag : ℕ → Set where
      []   : SizedBag 0
      _∷_  : A → ∀{n} → SizedBag n → SizedBag (suc n)
      comm : ∀ x y {n}(xs : SizedBag n) → x ∷ y ∷ xs ≡ y ∷ x ∷ xs

    T : Alg
    T .Alg.SizedBag = SizedBag
    T .Alg.[]       = []
    T .Alg._∷_      = _∷_
    T .Alg.comm     = comm

    T-elim : elim T
    T-elim P = go where
      open Motive P
      open Elim
      rec : ∀{n}(xs : SizedBag n) → SizedBagᴾ xs
      rec []              = []ᴾ
      rec (x ∷ xs)        = x ∷ᴾ rec xs
      rec (comm x y xs i) = commᴾ x y (rec xs) i
      go : Elim P
      go .SizedBagᴱ = rec
      go .[]ᴱ       = refl
      go ._∷ᴱ_ x xs = refl

  module Pre where
    data SizedBag : Set where
      []   : SizedBag
      cons : A → ℕ → SizedBag → SizedBag
      comm : ∀ x y m n xs
           → cons x m (cons y n xs) ≡ cons y m (cons x n xs)

  module _ where
    open Pre
    good : ℕ → SizedBag → Set
    good n′ [] = 0 ≡ n′
    good n′ (cons x n xs) = (suc n ≡ n′) × good n xs
    good n′ (comm x y m n xs i) = (suc m ≡ n′) × (suc n ≡ m) × good n xs

  module _ where
    open Alg
    T : Alg
    T .SizedBag n = Σ Pre.SizedBag (good n)
    T .[] = Pre.[] , refl
    T ._∷_ x {n} (xs , p) = Pre.cons x n xs , refl , p
    T .comm x y {n} (xs , p) i = Pre.comm x y (suc n) n xs i , refl , refl , p

  open Alg T

  T-elim : elim T
  T-elim P = go where
    open Motive P
    open Elim

    rec : ∀{n}(xs : SizedBag n) → SizedBagᴾ xs
    rec (Pre.[] , p) = J (λ _ p → SizedBagᴾ (Pre.[] , p)) []ᴾ p
    rec (Pre.cons x n xs , p , q) =
      J (λ _ p → SizedBagᴾ (Pre.cons x n xs , p , q)) (x ∷ᴾ rec (xs , q)) p
    rec (Pre.comm x y m n xs i , p , q , r) =
      J (λ _ p → SizedBagᴾ (Pre.comm x y m n xs i , p , q , r))
        {!J (λ m q → SizedBagᴾ (Pre.comm x y m n xs i , (λ _ → suc m) , q , r))
          ? q!} p

    go : Elim P
    go .SizedBagᴱ = rec
    go .[]ᴱ  = {!!}
    go ._∷ᴱ_ = {!!}
