{-# OPTIONS --safe --cubical --postfix-projections #-}

module test.IxRed.Fin where

open import Cubical.Foundations.Prelude
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Prod

record Alg : Set₁ where
  field
    Fin   : ℕ → Set
    fzero : ∀{n} → Fin (suc n)
    fsuc  : ∀{n} → Fin n → Fin (suc n)

record Motive {ℓ}(T : Alg) : Set (ℓ-suc ℓ) where
  open Alg T
  field
    Finᴾ   : ∀{n} → Fin n → Set ℓ
    fzeroᴾ : ∀{n} → Finᴾ (fzero {n})
    fsucᴾ  : ∀{n m} → Finᴾ m → Finᴾ (fsuc {n} m)

record Elim {ℓ}{T : Alg}(P : Motive {ℓ} T) : Set ℓ where
  open Alg T
  open Motive P
  field
    Finᴱ   : ∀{n}(m : Fin n) → Finᴾ m
    fzeroᴱ : ∀{n} → Finᴱ fzero ≡ fzeroᴾ {n}
    fsucᴱ  : ∀{n}{m : Fin n} → Finᴱ (fsuc m) ≡ fsucᴾ (Finᴱ m)

elim : ∀{ℓ} → Alg → Set (ℓ-suc ℓ)
elim T = (P : Motive T) → Elim P

module Ix where
  data Fin : ℕ → Set where
    fzero : ∀{n} → Fin (suc n)
    fsuc  : ∀{n} → Fin n → Fin (suc n)

  t : Fin 4
  t = fsuc (fsuc fzero)

  t' : Fin 4
  t' = fsuc (transport refl (fsuc fzero))

  toℕ : ∀{n} → Fin n → ℕ
  toℕ fzero    = 0
  toℕ (fsuc m) = suc (toℕ m)

module Pre where
  data Fin : Set where
    fzero : ℕ → Fin
    fsuc  : ℕ → Fin → Fin

ix : Pre.Fin → ℕ
ix (Pre.fzero n)  = suc n
ix (Pre.fsuc n m) = suc n

good : Pre.Fin → Set
good (Pre.fzero n)  = Unit
good (Pre.fsuc n m) = (ix m ≡ n) × good m

module _ where
  open Alg

  T : Alg
  T .Fin n = Σ[ m ∈ Pre.Fin ] (ix m ≡ n) × good m
  T .fzero {n} = Pre.fzero n , refl , tt
  T .fsuc {n} (m , p) = Pre.fsuc n m , refl , p

open Alg T

T-elim : ∀{ℓ} → elim {ℓ} T
T-elim P = go where
  open Motive P
  open Elim
  rec : ∀{n}(m : Fin n) → Finᴾ m
  rec (Pre.fzero n , p , tt) =
    J (λ y p → Finᴾ (Pre.fzero n , p , tt)) fzeroᴾ p
  rec (Pre.fsuc n m , p , q) =
    J (λ y p → Finᴾ (Pre.fsuc n m , p , q)) (fsucᴾ (rec (m , q))) p
  go : Elim P
  go .Finᴱ   = rec
  go .fzeroᴱ = transportRefl _
  go .fsucᴱ  = transportRefl _

open Motive
open Elim

t : Fin 4
t = fsuc (fsuc fzero)

t' : Fin 4
t' = fsuc (transport refl (fsuc fzero))

toℕ : ∀{n} → Fin n → ℕ
toℕ m = T-elim P .Finᴱ m where
  P : Motive T
  P .Finᴾ m  = ℕ
  P .fzeroᴾ  = 0
  P .fsucᴾ n = suc n
