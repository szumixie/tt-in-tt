{-# OPTIONS --without-K --postfix-projection #-}

module test.IxRed.ConTyParam where

open import lib

record Alg : Set₁ where
  infixr 5 _▷_
  field
    Con : Set
    Ty  : Con → Set
    ∙   : Con
    _▷_ : ∀ Γ  → Ty Γ → Con
    U   : ∀{Γ} → Ty Γ
    El  : ∀ Γ  → Ty (Γ ▷ U)
    Π   : ∀{Γ}(A : Ty Γ) → Ty (Γ ▷ A) → Ty Γ

record Motive (T : Alg) : Set₁ where
  open Alg T
  infixr 5 _▷ᴾ_
  field
    Conᴾ : Con → Set
    Tyᴾ  : ∀{Γ} → Conᴾ Γ → Ty Γ → Set
    ∙ᴾ   : Conᴾ ∙
    _▷ᴾ_ : ∀{Γ}(Γᴾ : Conᴾ Γ){A} → Tyᴾ Γᴾ A → Conᴾ (Γ ▷ A)
    Uᴾ   : ∀{Γ}(Γᴾ : Conᴾ Γ) → Tyᴾ Γᴾ U
    Elᴾ  : ∀{Γ}(Γᴾ : Conᴾ Γ) → Tyᴾ (Γᴾ ▷ᴾ Uᴾ Γᴾ) (El Γ)
    Πᴾ   : ∀{Γ}(Γᴾ : Conᴾ Γ){A}(Aᴾ : Tyᴾ Γᴾ A){B} → Tyᴾ (Γᴾ ▷ᴾ Aᴾ) B → Tyᴾ Γᴾ (Π A B)

record Elim {T : Alg}(P : Motive T) : Set where
  open Alg T
  open Motive P
  field
    Conᴱ : ∀ Γ → Conᴾ Γ
    Tyᴱ  : ∀ Γ A → Tyᴾ (Conᴱ Γ) A
    ∙ᴱ   : Conᴱ ∙ ≡ ∙ᴾ
    ▷ᴱ   : ∀{Γ A} → Conᴱ (Γ ▷ A) ≡ Conᴱ Γ ▷ᴾ Tyᴱ Γ A
    Uᴱ   : ∀{Γ} → Tyᴱ Γ U ≡ Uᴾ (Conᴱ Γ)
    Elᴱ  : ∀{Γ} → Tyᴱ (Γ ▷ U) (El Γ)
                  ≡[ ap (λ Γᴾ → Tyᴾ Γᴾ (El Γ)) (▷ᴱ ◾ ap (Conᴱ Γ ▷ᴾ_) Uᴱ) ]≡
                  Elᴾ (Conᴱ Γ)
    Πᴱ   : ∀{Γ A B} → Tyᴱ Γ (Π A B) ≡ Πᴾ (Conᴱ Γ) (Tyᴱ Γ A)
                                         (coe (ap (λ Γᴾ → Tyᴾ Γᴾ B) ▷ᴱ) (Tyᴱ (Γ ▷ A) B))

elim : Alg → Set₁
elim T = (P : Motive T) → Elim P

module Good where
  infixr 5 _▷_

  data Con : Set
  data Ty (Γ : Con) : Set

  data Con where
    ∙   : Con
    _▷_ : ∀ Γ → Ty Γ → Con

  data Ty Γ where
    U  : Ty Γ
    El : ∀ Δ → Γ ≡ Δ ▷ U → Ty Γ
    Π  : ∀ A → Ty (Γ ▷ A) → Ty Γ

module _ where
  open Alg

  T : Alg
  T .Con  = Good.Con
  T .Ty   = Good.Ty
  T .∙    = Good.∙
  T ._▷_  = Good._▷_
  T .U    = Good.U
  T .El Γ = Good.El Γ refl
  T .Π    = Good.Π

open Alg T

{-# TERMINATING #-}
T-elim : elim T
T-elim P = go where
  open Motive P
  open Elim

  elimCon : (Γ : Con) → Conᴾ Γ
  elimTy  : ∀ Γ (A : Ty Γ) → Tyᴾ (elimCon Γ) A

  elimCon Good.∙ = ∙ᴾ
  elimCon (Γ Good.▷ A) = elimCon Γ ▷ᴾ elimTy Γ A

  elimTy Γ Good.U = Uᴾ (elimCon Γ)
  elimTy (Γ Good.▷ Good.U) (Good.El Γ refl) = Elᴾ (elimCon Γ)
  elimTy Γ (Good.Π A B) = Πᴾ (elimCon Γ) (elimTy Γ A) (elimTy (Γ ▷ A) B)

  go : Elim P
  go .Conᴱ = elimCon
  go .Tyᴱ  = elimTy
  go .∙ᴱ   = refl
  go .▷ᴱ   = refl
  go .Uᴱ   = refl
  go .Elᴱ  = refl
  go .Πᴱ   = refl
