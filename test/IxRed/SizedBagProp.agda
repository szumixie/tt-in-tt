{-# OPTIONS --safe --cubical --prop --postfix-projections #-}

module test.IxRed.SizedBagProp where

open import Cubical.Foundations.Prelude
open import Cubical.Data.Nat

data ⊥ : Prop where

record ⊤ : Prop where
  instance constructor ★

infixr 2 _×_
record _×_ (A B : Prop) : Prop where
  constructor _,_
  field
    fst : A
    snd : B
open _×_

record Σᴾ (A : Set)(B : A → Prop) : Set where
  constructor _,_
  field
    fst : A
    snd : B fst
open Σᴾ

infix 4 _ℕ≡_
_ℕ≡_ : ℕ → ℕ → Prop
zero  ℕ≡ zero  = ⊤
suc x ℕ≡ suc y = x ℕ≡ y
_     ℕ≡ _     = ⊥

ℕrefl : ∀ n → n ℕ≡ n
ℕrefl zero    = ★
ℕrefl (suc n) = ℕrefl n

module _ (A : Set) where
  record Alg : Set₁ where
    infixr 5 _∷_
    field
      SizedBag : ℕ → Set
      []       : SizedBag 0
      _∷_      : A → ∀{n} → SizedBag n → SizedBag (suc n)
      comm     : ∀ x y {n}(xs : SizedBag n) → x ∷ y ∷ xs ≡ y ∷ x ∷ xs

  record Motive (T : Alg) : Set₁ where
    open Alg T
    infixr 5 _∷ᴾ_
    field
      SizedBagᴾ : ∀{n} → SizedBag n → Set
      []ᴾ       : SizedBagᴾ []
      _∷ᴾ_      : ∀ x {n}{xs : SizedBag n} → SizedBagᴾ xs → SizedBagᴾ (x ∷ xs)
      commᴾ     : ∀ x y {n}{xs : SizedBag n}(xsᴾ : SizedBagᴾ xs)
                → PathP (λ i → SizedBagᴾ (comm x y xs i))
                        (x ∷ᴾ y ∷ᴾ xsᴾ) (y ∷ᴾ x ∷ᴾ xsᴾ)

  record Elim {T : Alg}(P : Motive T) : Set where
    open Alg T
    open Motive P
    infixr 5 _∷ᴱ_
    field
      SizedBagᴱ : ∀{n}(xs : SizedBag n) → SizedBagᴾ xs
      []ᴱ       : SizedBagᴱ [] ≡ []ᴾ
      _∷ᴱ_      : ∀ x {n}(xs : SizedBag n) → SizedBagᴱ (x ∷ xs) ≡ x ∷ᴾ SizedBagᴱ xs
      {-commᴱ     : ∀ x y {n}(xs : SizedBag n)
                → PathP (λ i → SizedBagᴱ (comm x y xs i) ≡ commᴾ x y (SizedBagᴱ xs) i)
                        ((x ∷ᴱ (y ∷ xs)) ∙ cong (x ∷ᴾ_) (y ∷ᴱ xs))
                        ((y ∷ᴱ (x ∷ xs)) ∙ cong (y ∷ᴾ_) (x ∷ᴱ xs))-}

  elim : Alg → Set₁
  elim T = (P : Motive T) → Elim P

  module Native where
    infixr 5 _∷_
    data SizedBag : ℕ → Set where
      []   : SizedBag 0
      _∷_  : A → ∀{n} → SizedBag n → SizedBag (suc n)
      comm : ∀ x y {n}(xs : SizedBag n) → x ∷ y ∷ xs ≡ y ∷ x ∷ xs

    T : Alg
    T .Alg.SizedBag = SizedBag
    T .Alg.[]       = []
    T .Alg._∷_      = _∷_
    T .Alg.comm     = comm

    T-elim : elim T
    T-elim P = go where
      open Motive P
      open Elim
      rec : ∀{n}(xs : SizedBag n) → SizedBagᴾ xs
      rec []              = []ᴾ
      rec (x ∷ xs)        = x ∷ᴾ rec xs
      rec (comm x y xs i) = commᴾ x y (rec xs) i
      go : Elim P
      go .SizedBagᴱ = rec
      go .[]ᴱ       = refl
      go ._∷ᴱ_ x xs = refl

  module Pre where
    data SizedBag : Set where
      []   : SizedBag
      cons : A → ℕ → SizedBag → SizedBag
      comm : ∀ x y n xs → cons x (suc n) (cons y n xs)
                        ≡ cons y (suc n) (cons x n xs)

  module _ where
    open Pre
    good : ℕ → SizedBag → Prop
    good n′ []                = 0 ℕ≡ n′
    good n′ (cons x n xs)     = suc n ℕ≡ n′ × good n xs
    good n′ (comm x y n xs i) = suc (suc n) ℕ≡ n′ × suc n ℕ≡ suc n × good n xs

  module _ where
    open Alg
    T : Alg
    T .SizedBag n = Σᴾ Pre.SizedBag (good n)
    T .[] = Pre.[] , ★
    T ._∷_ x {n} (xs , p) = Pre.cons x n xs , ℕrefl n , p
    T .comm x y {n} (xs , p) i = Pre.comm x y n xs i , ℕrefl n , ℕrefl n , p

  open Alg T

  T-elim : elim T
  T-elim P = go where
    open Motive P
    open Elim

    rec : ∀{n}(xs : SizedBag n) → SizedBagᴾ xs
    rec (xs , p) = {!!}

    go : Elim P
    go .SizedBagᴱ = rec
    go .[]ᴱ = {!!}
    go ._∷ᴱ_ = {!!}
