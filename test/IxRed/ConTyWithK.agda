{-# OPTIONS --safe --with-K --postfix-projections #-}

module test.IxRed.ConTyWithK where

open import Relation.Binary.PropositionalEquality
open import Data.Product

record Alg : Set₁ where
  infixr 5 _▷_
  field
    Con : Set
    Ty  : Con → Set
    ∙   : Con
    _▷_ : (Γ : Con) → Ty Γ → Con
    U   : ∀{Γ} → Ty Γ
    Π   : ∀{Γ}(A : Ty Γ) → Ty (Γ ▷ A) → Ty Γ

record Motive (T : Alg) : Set₁ where
  open Alg T
  infixr 5 _▷ᴾ_
  field
    Conᴾ : Con → Set
    Tyᴾ  : ∀{Γ} → Conᴾ Γ → Ty Γ → Set
    ∙ᴾ   : Conᴾ ∙
    _▷ᴾ_ : ∀{Γ}(Γᴾ : Conᴾ Γ){A} → Tyᴾ Γᴾ A → Conᴾ (Γ ▷ A)
    Uᴾ   : ∀{Γ}(Γᴾ : Conᴾ Γ) → Tyᴾ Γᴾ U
    Πᴾ   : ∀{Γ}(Γᴾ : Conᴾ Γ){A}(Aᴾ : Tyᴾ Γᴾ A) → ∀{B} → Tyᴾ (Γᴾ ▷ᴾ Aᴾ) B → Tyᴾ Γᴾ (Π A B)

record Elim {T : Alg}(P : Motive T) : Set where
  open Alg T
  open Motive P
  field
    Conᴱ : ∀ Γ → Conᴾ Γ
    Tyᴱ  : ∀ Γ A → Tyᴾ (Conᴱ Γ) A
    ∙ᴱ   : Conᴱ ∙ ≡ ∙ᴾ
    ▷ᴱ   : ∀{Γ A} → Conᴱ (Γ ▷ A) ≡ Conᴱ Γ ▷ᴾ Tyᴱ Γ A
    Uᴱ   : ∀{Γ} → Tyᴱ Γ U ≡ Uᴾ (Conᴱ Γ)
    Πᴱ   : ∀{Γ A B} → Tyᴱ Γ (Π A B) ≡ Πᴾ (Conᴱ Γ) (Tyᴱ Γ A)
           (subst (λ Γᴾ → Tyᴾ Γᴾ B) ▷ᴱ (Tyᴱ (Γ ▷ A) B))

elim : Alg → Set₁
elim T = (P : Motive T) → Elim P

module Pre where
  infixr 5 _▷_

  data Con : Set
  data Ty  : Set

  data Con where
    ∙   : Con
    _▷_ : Con → Ty → Con
  data Ty where
    U : Con → Ty
    Π : Con → Ty → Ty → Ty

module Good where
  infixr 5 _▷_

  data Con : Pre.Con → Set
  data Ty  : Pre.Con → Pre.Ty → Set

  data Con where
    ∙   : Con Pre.∙
    _▷_ : ∀{Γ} → Con Γ → ∀{A} → Ty Γ A → Con (Γ Pre.▷ A)
  data Ty where
    U : ∀{Γ} → Con Γ → Ty Γ (Pre.U Γ)
    Π : ∀{Γ} → Con Γ → ∀{A} → Ty Γ A → ∀{B} → Ty (Γ Pre.▷ A) B → Ty Γ (Pre.Π Γ A B)

module _ where
  open Alg

  T : Alg
  T .Con = Σ Pre.Con Good.Con
  T .Ty (Γ , _) = Σ Pre.Ty (Good.Ty Γ)
  T .∙ = -, Good.∙
  T ._▷_ (_ , Γ) (_ , A) = -, Γ Good.▷ A
  T .U {_ , Γ} = -, Good.U Γ
  T .Π {_ , Γ} (_ , A) (_ , B) = -, Good.Π Γ A B

open Alg T

uGoodCon : ∀{Γ}(gΓ gΓ′ : Good.Con Γ) → gΓ ≡ gΓ′
uGoodTy  : ∀{Γ A}(gA gA′ : Good.Ty Γ A) → gA ≡ gA′

uGoodCon Good.∙ Good.∙ = refl
uGoodCon (Γ Good.▷ A) (Γ′ Good.▷ A′) rewrite uGoodCon Γ Γ′ | uGoodTy A A′ = refl
uGoodTy (Good.U Γ) (Good.U Γ′) rewrite uGoodCon Γ Γ′ = refl
uGoodTy (Good.Π Γ A B) (Good.Π Γ′ A′ B′)
  rewrite uGoodCon Γ Γ′ | uGoodTy A A′ | uGoodTy B B′ = refl

T-elim : elim T
T-elim P = go where
  open Motive P
  open Elim

  elimCon : (Γ : Con) → Conᴾ Γ
  elimTy  : ∀ Γ (A : Ty Γ) → Tyᴾ (elimCon Γ) A

  elimCon (Pre.∙ , Good.∙) = ∙ᴾ
  elimCon (pΓ Pre.▷ pA , gΓ Good.▷ gA) = elimCon (pΓ , gΓ) ▷ᴾ elimTy _ (pA , gA)
  elimTy (pΓ , gΓ) (Pre.U pΓ , Good.U gΓ′) rewrite uGoodCon gΓ gΓ′ = Uᴾ (elimCon (pΓ , gΓ′))
  elimTy (pΓ , gΓ) (Pre.Π pΓ pA pB , Good.Π gΓ′ gA gB) rewrite uGoodCon gΓ gΓ′
    = Πᴾ (elimCon (pΓ , gΓ′)) (elimTy _ (pA , gA)) (elimTy _ (pB , gB))

  go : Elim P
  go .Conᴱ = elimCon
  go .Tyᴱ  = elimTy
  go .∙ᴱ = refl
  go .▷ᴱ = refl
  go .Uᴱ {_ , Γ} rewrite uGoodCon Γ Γ = refl
  go .Πᴱ {_ , Γ} rewrite uGoodCon Γ Γ = refl
