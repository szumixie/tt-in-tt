{-# OPTIONS --without-K --postfix-projections #-}

module test.BoolModel where

open import lib
open import TT.Decl
open import TT.Core
open import TT.Func

i : Bool → Set
i true  = ⊤
i false = ⊥

-- ⋀ ~ Π
⋀ : (A : Bool) → (i A → Bool) → Bool
⋀ true  B = B tt
⋀ false B = true

⋀-lam : {A : Bool}{B : i A → Bool} → ((a : i A) → i (B a)) → i (⋀ A B)
⋀-lam {true}  f = f tt
⋀-lam {false} f = tt

⋀-app : {A : Bool}{B : i A → Bool} → i (⋀ A B) → (a : i A) → i (B a)
⋀-app {true} f a = f

-- ⋁ ~ Σ
⋁ : (A : Bool) → (i A → Bool) → Bool
⋁ true  B = B tt
⋁ false B = false

_,⋁_ : {A : Bool}{B : i A → Bool}(a : i A) → i (B a) → i (⋁ A B)
_,⋁_ {true}  a b = b
_,⋁_ {false} a b = a

⋁-proj₁ : {A : Bool}{B : i A → Bool} → i (⋁ A B) → i A
⋁-proj₁ {true}  p = tt
⋁-proj₁ {false} p = p

⋁-proj₂ : {A : Bool}{B : i A → Bool}(p : i (⋁ A B)) → i (B (⋁-proj₁ p))
⋁-proj₂ {true} p = p

module _ where
  open Decl

  d : Decl
  d .Con     = Bool
  d .Ty  Γ   = i Γ → Bool
  d .Tms Γ Δ = i Γ → i Δ
  d .Tm  Γ A = (γ : i Γ) → i (A γ)

open Decl d

module _ where
  open Core
  open Core1
  open Core2

  c : Core d
  c .c1 .• = true
  c .c1 ._,_ = ⋁

  c .c1 ._[_]T A σ γ = A (σ γ)

  c .c1 .id γ = γ
  c .c1 ._∘_ σ ν γ = σ (ν γ)
  c .c1 .ε γ = tt
  c .c1 ._,s_ σ t γ = σ γ ,⋁ t γ
  c .c1 .π₁ σ γ = ⋁-proj₁ (σ γ)

  c .c1 ._[_]t t σ γ = t (σ γ)
  c .c1 .π₂ {Δ = Δ} σ γ = ⋁-proj₂ {Δ} (σ γ)

  c .c2 .[id]T = refl
  c .c2 .[][]T = refl

  c .c2 .idl = refl
  c .c2 .idr = refl
  c .c2 .ass = refl
  c .c2 .,∘  = refl
  c .c2 .π₁β {Δ = true}  = refl
  c .c2 .π₁β {Δ = false} = refl
  c .c2 .πη {Δ = true}  = refl
  c .c2 .πη {Δ = false} = refl
  c .c2 .εη = refl

  c .c2 .π₂β {Δ = true} = refl
  c .c2 .π₂β {true} {false}{δ = δ} = ⊥-elim (δ tt)
  c .c2 .π₂β {false}{false} = funext λ ()

open Core c

module _ where
  open Func

  f : Func c
  f .Π A B γ = ⋀ (A γ) λ x → B (γ ,⋁ x)
  f .Π[] {true}  = refl
  f .Π[] {false} = funext λ ()

  f .lam t γ = ⋀-lam λ a → t (γ ,⋁ a)
  f .app {true} t γ = ⋀-app (t (⋁-proj₁ γ)) (⋁-proj₂ γ)
  f .lam[] {true}  = refl
  f .lam[] {false} = funext λ ()

  f .Πβ {true}{A}{B}{t} = if_then_else_
    { C = λ b → (B : i b → Bool)(t : (γ : i b) → i (B γ))
              → (λ γ → ⋀-app (⋀-lam t) γ) ≡ t
    } (A tt) (λ B t → refl) (λ B t → funext λ ()) B t
  f .Πβ {false} = funext λ ()
  f .Πη {true}{A}{B}{t} = if_then_else_
    { C = λ b → (B : i b → Bool)(t : ⊤ → i (⋀ b (λ x → B x)))
              → (λ γ → ⋀-lam {b} (λ a → ⋀-app (t tt) a)) ≡ t
    } (A tt) (λ B t → refl) (λ B t → refl) B t
  f .Πη {false} = funext λ ()

{-
_∧_ : Con → Con → Con
true ∧ b = b
false ∧ b = false

_×'_ : {Γ : Con}(A B : Ty Γ) → Ty Γ
(A ×' B) γ = A γ ∧ B γ

_,'_ : {Γ : Con}{A B : Ty Γ} → Tm Γ A → Tm Γ B → Tm Γ (A ×' B)
(_,'_ {Γ}{A}{B} u v) γ = {!!} -- if_then_else_ {C = λ b → i b → i (if b then B γ else false)} (A γ) (λ _ → v γ) (λ ()) (u γ)

proj₁' : {Γ : Con}{A B : Ty Γ} → Tm Γ (A ×' B) → Tm Γ A
proj₁' t γ = {!t γ!}

-- can this have a universe?
-}
