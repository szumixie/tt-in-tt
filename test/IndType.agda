module test.IndType where

open import lib

⟦_⟧ : (Σ Set λ S → S → Set) → Set → Set
⟦ S ,Σ P ⟧ X = Σ S λ s → P s → X

data W S P : Set where
  con : ⟦ S ,Σ P ⟧ (W S P) → W S P

ℕ′ : Set
ℕ′ = W Bool λ b → if b then ⊤ else ⊥

zero′ : ℕ′
zero′ = con (false ,Σ ⊥-elim)

suc′ : ℕ′ → ℕ′
suc′ n = con (true ,Σ λ tt → n)

ℕ′-elim : ∀{ℓ}(P : ℕ′ → Set ℓ) → P zero′ → (∀ n → P n → P (suc′ n)) → ∀ n → P n
ℕ′-elim P z s (con (false ,Σ p)) =
  coe (ap (λ x → P (con (false ,Σ x))) (funext (λ x → ⊥-elim x))) z
ℕ′-elim P z s (con (true ,Σ p)) = s (p tt) (ℕ′-elim P z s (p tt))
