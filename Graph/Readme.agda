module Graph.Readme where

-- the graph model of type theory: a context is modelled as a set and
-- a homogeneous binary relation over it

import Graph.Decl            -- declaration of the model
import Graph.Core            -- substitution calculus
import Graph.Func            -- function space

-- the graph model of type theory given with a display map variant: a
-- context is modelled as two sets and two functions from one to the
-- other

import Graph.DisplayMap.Decl -- declaration of the model
import Graph.DisplayMap.Core -- substitution calculus
import Graph.DisplayMap.Func -- function space
