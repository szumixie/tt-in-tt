{-# OPTIONS --without-K #-}

module Graph.Decl where

open import lib

record Con : Set₁ where
  field
    ⌜_⌝C : Set
    _⁼C  : ⌜_⌝C → ⌜_⌝C → Set
  infix 7 ⌜_⌝C
open Con public

record Ty (Γ : Con) : Set₁ where
  field
    ⌜_⌝T : ⌜ Γ ⌝C → Set
    _⁼T  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀)(α₁ : ⌜_⌝T γ₁) → Set
  infix 7 ⌜_⌝T
open Ty public

record Tms (Γ Δ : Con) : Set where
  field
    ⌜_⌝s : ⌜ Γ ⌝C → ⌜ Δ ⌝C
    _⁼s  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s γ₀) (⌜_⌝s γ₁)
  infix 7 ⌜_⌝s
open Tms public

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    ⌜_⌝t : (γ : ⌜ Γ ⌝C) → ⌜ A ⌝T γ
    _⁼t  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (A ⁼T) γ₌ (⌜_⌝t γ₀) (⌜_⌝t γ₁)
  infix 7 ⌜_⌝t
open Tm public

open import TT.Decl

d : Decl
d = decl Con Ty Tms Tm
