{-# OPTIONS --without-K --allow-unsolved-metas #-}

module Graph.DisplayMap.Core where

open import lib

open import Graph.DisplayMap.Decl

• : Con
• = record
  { ⌜_⌝C = ⊤
  ; _⁼C  = ⊤
  }

_,_ : (Γ : Con) → Ty Γ → Con
Γ , A = record
  { ⌜_⌝C = Σ ⌜ Γ ⌝C ⌜ A ⌝T
  ; _⁼C  = Σ (Γ ⁼C) λ γ₌ → Σ (⌜ A ⌝T (p₀ Γ γ₌)) λ α₀ → Σ (⌜ A ⌝T (p₁ Γ γ₌)) λ α₁ → (A ⁼T) γ₌ α₀ α₁
  ; p₀ = λ { (γ₌ ,Σ (α₀ ,Σ (α₁ ,Σ α₌))) → p₀ Γ γ₌ ,Σ α₀ }
  ; p₁ = λ { (γ₌ ,Σ (α₀ ,Σ (α₁ ,Σ α₌))) → p₁ Γ γ₌ ,Σ α₁ }
  }

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
_[_]T {Γ}{Δ} A σ = record
  { ⌜_⌝T = λ γ → ⌜ A ⌝T (⌜ σ ⌝s γ)
  ; _⁼T = λ γ₌ α₀ α₁ → (A ⁼T) ((σ ⁼s) γ₌)
                              (transport ⌜ A ⌝T (ap (λ z → z γ₌) (p₀s σ)) α₀)
                              (transport ⌜ A ⌝T (ap (λ z → z γ₌) (p₁s σ)) α₁)
  }

id : ∀{Γ} → Tms Γ Γ
id {Γ} = record
  { ⌜_⌝s = λ γ → γ
  ; _⁼s  = λ γ₌ → γ₌
  ; p₀s = refl
  ; p₁s = refl
  }

_∘_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
_∘_ {Γ}{Δ}{Σ} σ ν = record
  { ⌜_⌝s = λ γ → ⌜ σ ⌝s (⌜ ν ⌝s γ)
  ; _⁼s  = λ γ₌ → (σ ⁼s) ((ν ⁼s) γ₌)
  ; p₀s = ap (λ z → (λ γ₌ → ⌜ σ ⌝s (z γ₌))) (p₀s ν) ◾ ap (λ z → (λ γ₌ → z ((ν ⁼s) γ₌))) (p₀s σ)
  ; p₁s = ap (λ z → (λ γ₌ → ⌜ σ ⌝s (z γ₌))) (p₁s ν) ◾ ap (λ z → (λ γ₌ → z ((ν ⁼s) γ₌))) (p₁s σ)
  }

ε : ∀{Γ} → Tms Γ •
ε {Γ} =  record
  { ⌜_⌝s = λ γ → tt
  ; _⁼s  = λ γ₌ → tt
  ; p₀s = refl
  ; p₁s = refl
  }

_,s_ : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
_,s_ {Γ}{Δ} σ {A} t =  record
  { ⌜_⌝s = λ γ → ⌜ σ ⌝s γ ,Σ ⌜ t ⌝t γ
  ; _⁼s  = λ γ₌ → (σ ⁼s) γ₌ ,Σ (
                  transport ⌜ A ⌝T (ap (λ z → z γ₌) (p₀s σ)) (⌜ t ⌝t (p₀ Γ γ₌)) ,Σ (
                  transport ⌜ A ⌝T (ap (λ z → z γ₌) (p₁s σ)) (⌜ t ⌝t (p₁ Γ γ₌)) ,Σ
                  (t ⁼t) γ₌))
  ; p₀s = {!!}
  -- ap (λ z → λ γ₌ → z γ₌ ,Σ coe (ap ⌜ A ⌝T (ap (λ z → z γ₌) (p₀s σ))) (⌜ t ⌝t (p₀ Γ γ₌))) (p₀s σ)
  -- ap (λ z → λ γ₌ → z γ₌ ,Σ ⌜ t ⌝t (p₀ Γ γ₌)) (p₀s σ)
  ; p₁s = {!!}
  }

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) → Tms Γ Δ
π₁ σ =  record
  { ⌜_⌝s = λ γ → proj₁ (⌜ σ ⌝s γ)
  ; _⁼s  = λ γ₌ → proj₁ ((σ ⁼s) γ₌)
  ; p₀s = ap (λ z → λ γ₌ → proj₁ (z γ₌)) (p₀s σ)
  ; p₁s = ap (λ z → λ γ₌ → proj₁ (z γ₌)) (p₁s σ)
  }

_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
_[_]t {Γ}{Δ}{A} t σ = record
  { ⌜_⌝t = λ γ → ⌜ t ⌝t (⌜ σ ⌝s γ)
  ; _⁼t  = λ γ₌ → coe (ap2 ((A ⁼T) ((σ ⁼s) γ₌))
                           (apd (λ z → ⌜ t ⌝t (z γ₌)) (p₀s σ) ⁻¹ ◾ ap (λ z → coe z (⌜ t ⌝t (⌜ σ ⌝s (p₀ Γ γ₌)))) (apap {f = λ z → z γ₌}{⌜ A ⌝T} (p₀s σ)))
                           (apd (λ z → ⌜ t ⌝t (z γ₌)) (p₁s σ) ⁻¹ ◾ ap (λ z → coe z (⌜ t ⌝t (⌜ σ ⌝s (p₁ Γ γ₌)))) (apap {f = λ z → z γ₌}{⌜ A ⌝T} (p₁s σ))))
                      ((t ⁼t) ((σ ⁼s) γ₌))
  }

π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ {Γ}{Δ}{A} σ = record
  { ⌜_⌝t = λ γ → proj₂ (⌜ σ ⌝s γ)
  ; _⁼t  = λ γ₌ → coe {!!} (proj₂ (proj₂ (proj₂ ((σ ⁼s) γ₌))))
  }

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T = refl

[][]T : {Γ Θ Δ : Con}{A : Ty Δ}{δ : Tms Γ Θ}{σ : Tms Θ Δ}
        → A [ σ ]T [ δ ]T ≡ A [ _∘_ {Γ}{Θ}{Δ} σ δ ]T
[][]T = {!!}

idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Δ}{Δ} id δ) ≡ δ
idl = {!!}

idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Γ}{Δ} δ id) ≡ δ
idr = {!!}

ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
      → _∘_ {Γ}{Δ}{Ω}(_∘_ {Δ}{Σ}{Ω} σ δ) ν ≡ _∘_ {Γ}{Σ}{Ω} σ (_∘_ {Γ}{Δ}{Σ} δ ν)
ass = {!!}

π₁β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → (π₁ {Γ}{Δ}{A}(_,s_ {Γ}{Δ} δ {A} a)) ≡ δ
π₁β = {!!}

πη : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
      → _,s_ {Γ}{Δ}(π₁ {Γ}{Δ}{A} δ){A}(π₂ {Γ}{Δ}{A} δ) ≡ δ
πη = {!!}

εη : ∀{Γ}{σ : Tms Γ •}
      → σ ≡ ε
εη = {!!}
{-
,∘ : {Γ Δ Σ : Con}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)} →
  ((_,s_  δ {A} a) ∘ σ) ≡ (_,s_ (δ ∘ σ) {A} (a [ σ ]t))
,∘ {Γ}{Δ}{Σ}{δ}{σ}{A}{a} = refl

π₂β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → π₂ {Γ}{Δ}{A}(_,s_ {Γ}{Δ} δ {A} a) ≡ a
π₂β = refl
-}
open import TT.Core

c1 : Core1 d
c1 = record
  { • = •
  ; _,_ = _,_
  ; _[_]T = _[_]T
  ; id = id
  ; _∘_ = _∘_
  ; ε = ε
  ; _,s_ = _,s_
  ; π₁ = π₁
  ; _[_]t = _[_]t
  ; π₂ = π₂
  }

c2 : Core2 d c1
c2 = record
  { [id]T = [id]T
  ; [][]T = λ {Γ}{Δ}{Σ}{A}{σ}{δ} → [][]T {Γ}{Δ}{Σ}{A}{σ}{δ}
  ; idl = idl
  ; idr = idr
  ; ass = λ {Γ}{Δ}{Σ}{Ω}{σ}{δ}{ν} → ass {Γ}{Δ}{Σ}{Ω}{σ}{δ}{ν}
  ; ,∘ = λ {Γ}{Δ}{Σ}{δ}{σ}{A}{a} → {!!} -- ,∘ {Γ}{Δ}{Σ}{δ}{σ}{A}{a}
  ; π₁β = λ {Γ}{Δ}{A}{δ}{a} → π₁β {Γ}{Δ}{A}{δ}{a}
  ; πη = πη
  ; εη = εη
  ; π₂β = λ {Γ}{Δ}{A}{δ}{a} → {!!} -- π₂β {Γ}{Δ}{A}{δ}{a}
  }

c : Core d
c = record { c1 = c1 ; c2 = c2 }

