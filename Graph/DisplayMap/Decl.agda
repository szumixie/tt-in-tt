{-# OPTIONS --without-K #-}

module Graph.DisplayMap.Decl where

open import lib

record Con : Set₁ where
  field
    ⌜_⌝C : Set
    _⁼C  : Set
    p₀ p₁ : _⁼C → ⌜_⌝C
  infix 7 ⌜_⌝C
open Con public

record Ty (Γ : Con) : Set₁ where
  field
    ⌜_⌝T : ⌜ Γ ⌝C → Set
    _⁼T  : (γ₌ : Γ ⁼C)(α₀ : ⌜_⌝T (p₀ Γ γ₌))(α₁ : ⌜_⌝T (p₁ Γ γ₌)) → Set
  infix 7 ⌜_⌝T
open Ty public

record Tms (Γ Δ : Con) : Set where
  field
    ⌜_⌝s : ⌜ Γ ⌝C → ⌜ Δ ⌝C
    _⁼s  : Γ ⁼C → Δ ⁼C
    p₀s  : (λ γ₌ → ⌜_⌝s (p₀ Γ γ₌)) ≡ (λ γ₌ → p₀ Δ (_⁼s γ₌))
    p₁s  : (λ γ₌ → ⌜_⌝s (p₁ Γ γ₌)) ≡ (λ γ₌ → p₁ Δ (_⁼s γ₌))
  infix 7 ⌜_⌝s
open Tms public

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    ⌜_⌝t : (γ : ⌜ Γ ⌝C) → ⌜ A ⌝T γ
    _⁼t  : (γ₌ : Γ ⁼C) → (A ⁼T) γ₌ (⌜_⌝t (p₀ Γ γ₌)) (⌜_⌝t (p₁ Γ γ₌))
  infix 7 ⌜_⌝t
open Tm public

open import TT.Decl

d : Decl
d = decl Con Ty Tms Tm
