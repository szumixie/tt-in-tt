{-# OPTIONS --no-eta #-}

module JM where

open import Agda.Primitive
open import lib

record _≃_ {ℓ}{A B : Set ℓ}(a : A)(b : B) : Set (lsuc ℓ) where
  constructor _,≃_
  field
    projT : A ≡ B
    projt : a ≡[ projT ]≡ b

infixl 5 _,≃_
infix 4 _≃_

open _≃_

_◾̃_ : ∀{ℓ}{A B C : Set ℓ}{a : A}{b : B}{c : C}
    → a ≃ b → b ≃ c → a ≃ c
(refl ,≃ refl) ◾̃ (refl ,≃ refl) = refl ,≃ refl

infixl 4 _◾̃_

_⁻¹̃ : ∀{ℓ}{A B : Set ℓ}{a : A}{b : B}
    → a ≃ b → b ≃ a
(refl ,≃ refl) ⁻¹̃ = refl ,≃ refl

infix 5 _⁻¹̃

r̃ : ∀{ℓ}{A : Set ℓ}{a : A} → a ≃ a
r̃ = refl ,≃ refl

_≃⟨_⟩_ : ∀{ℓ}{A B C : Set ℓ}(x : A){y : B}{z : C} → x ≃ y → y ≃ z → x ≃ z
x ≃⟨ x≃y ⟩ y≃z = x≃y ◾̃ y≃z

infixr 2 _≃⟨_⟩_

_∎̃ : ∀{ℓ}{A : Set ℓ}(x : A) → x ≃ x
x ∎̃ = r̃

infix  3 _∎̃

uncoe : ∀{ℓ}{A B : Set ℓ}{a : A}(p : A ≡ B) → a ≃ coe p a
uncoe p = p ,≃ refl

to≃ : ∀{ℓ}{A : Set ℓ}{a a' : A} → a ≡ a' → a ≃ a'
to≃ p = refl ,≃ p

from≃ : ∀{ℓ}{A : Set ℓ}{a a' : A}
      → a ≃ a' → a ≡ a'
from≃ (refl ,≃ refl) = refl

from≡ : ∀{ℓ}{A B : Set ℓ}(p : A ≡ B){a : A}{b : B}
      → a ≡[ p ]≡ b → a ≃ b
from≡ refl refl = refl ,≃ refl

to≡ : ∀{ℓ}{A B : Set ℓ}{a : A}{b : B}
      (p : a ≃ b) → a ≡[ projT p ]≡ b
to≡ (refl ,≃ refl) = refl

ap≃ : ∀{ℓ ℓ' ℓ''}{A : Set ℓ}{B : A → Set ℓ'}{C : A → Set ℓ''}
      (f : {x : A} → B x → C x)
      {a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
      {b₀ : B a₀}{b₁ : B a₁}(b₂ : b₀ ≃ b₁)
    → f b₀ ≃ f b₁
ap≃ f refl (refl ,≃ refl) = refl ,≃ refl

ap≃' : ∀{ℓ ℓ' ℓ''}{A : Set ℓ}{B : A → Set ℓ'}{C : {x : A} → B x → Set ℓ''}
       (f : {x : A}(y : B x) → C y)
       {a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
       {b₀ : B a₀}{b₁ : B a₁}(b₂ : b₀ ≃ b₁)
     → f b₀ ≃ f b₁
ap≃' f refl (refl ,≃ refl) = refl ,≃ refl

apd≃ : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}(f : (x : A) → B x)
      → {a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
      → f a₀ ≃ f a₁
apd≃ f refl = refl ,≃ refl

apd≃' : ∀{ℓ ℓ'}{A₀ A₁ : Set ℓ}
        {B₀ : A₀ → Set ℓ'}{B₁ : A₁ → Set ℓ'}(B₂ : B₀ ≃ B₁)
        {f₀ : (x : A₀) → B₀ x}{f₁ : (x : A₁) → B₁ x}
        (f₂ : f₀ ≃ f₁)
        {a₀ : A₀}{a₁ : A₁}(a₂ : a₀ ≃ a₁)
      → f₀ a₀ ≃ f₁ a₁
apd≃' (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

ap≃'' : ∀{ℓ ℓ' ℓ''}{A : Set ℓ}{B : A → Set ℓ'}
        {C₀ C₁ : (x : A) → B x → Set ℓ''}(C₂ : C₀ ≡ C₁)
        {f₀ : {x : A}(y : B x) → C₀ x y}{f₁ : {x : A}(y : B x) → C₁ x y}
        (f₂ : (λ {x} → f₀ {x}) ≃ (λ {x} → f₁ {x}))
        {a : A}{b : B a}
      → f₀ b ≃ f₁ b
ap≃'' refl (refl ,≃ refl) = refl ,≃ refl

UIP : ∀{ℓ}{A : Set ℓ}{x y : A}(p q : x ≡ y) → p ≡ q
UIP refl refl = refl

loopcoe : ∀{ℓ}{A : Set ℓ}(p : A ≡ A){a : A} → coe p a ≡ a
loopcoe refl = refl

funext≃' :  ∀{ℓ ℓ'}{A : Set ℓ}{B₀ B₁ : A → Set ℓ'}
            {f₀ : (x : A) → B₀ x}{f₁ : (x : A) → B₁ x}
          → ((x : A) → f₀ x ≃ f₁ x)
          → f₀ ≃ f₁
funext≃' {ℓ}{ℓ'}{A}{B₀}{B₁}{f₀}{f₁} p

  = ap (λ z → (x : A) → z x) (funext (λ x → projT (p x)))
  ,≃ funext (λ x → from≃ ( ap≃ {A = A → Set ℓ'}
                               {λ B → (x : A) → B x}
                               {λ B → B x}
                               (λ f → f x)
                               {B₁}
                               {B₀}
                               (funext (λ x → projT (p x) ⁻¹))
                               (uncoe (ap (λ z → (x : A) → z x)
                                 (funext (λ x → projT (p x)))) ⁻¹̃)
                        ◾̃ p x))

funexti≃' : ∀{ℓ ℓ'}{A : Set ℓ}{B₀ B₁ : A → Set ℓ'}
            {f₀ : {x : A} → B₀ x}{f₁ : {x : A} → B₁ x}
          → ((x : A) → f₀ {x} ≃ f₁ {x})
          → (λ {x} →  f₀ {x}) ≃ (λ {x} → f₁ {x})
funexti≃' {ℓ}{ℓ'}{A}{B₀}{B₁}{f₀}{f₁} p

  = ap (λ z → {x : A} → z x) (funext (λ x → projT (p x)))
  ,≃ funexti (λ x → from≃ ( ap≃ {A = A → Set ℓ'}
                               {λ B → {x : A} → B x}
                               {λ B → B x}
                               (λ f → f {x})
                               {B₁}
                               {B₀}
                               (funext (λ x → projT (p x) ⁻¹))
                               (uncoe (ap (λ z → {x : A} → z x)
                                 (funext (λ x → projT (p x)))) ⁻¹̃)
                        ◾̃ p x))

funext≃   : ∀{ℓ ℓ'}{A₀ A₁ : Set ℓ}(A₂ : A₀ ≡ A₁)
            {B₀ : A₀ → Set ℓ'}{B₁ : A₁ → Set ℓ'}
            {f₀ : (x : A₀) → B₀ x}{f₁ : (x : A₁) → B₁ x}
          → ({x₀ : A₀}{x₁ : A₁}(x₂ : x₀ ≃ x₁) → f₀ x₀ ≃ f₁ x₁)
          → f₀ ≃ f₁
funext≃ {ℓ}{ℓ'}{A} refl {B₀}{B₁}{f₀}{f₁} p

  = ap (λ z → (x : A) → z x) (funext (λ x → projT (p {x} r̃)))
  ,≃ funext (λ x → from≃ ( ap≃ {A = A → Set ℓ'}
                              {λ B → (x : A) → B x}
                              {λ B → B x}
                              (λ f → f x)
                              {B₁}
                              {B₀}
                              (funext (λ x → projT (p {x} r̃) ⁻¹))
                              (uncoe (ap (λ z → (x : A) → z x)
                                (funext (λ x₁ → projT (p r̃)))) ⁻¹̃)
                        ◾̃ p {x} r̃))

funexti≃  : ∀{ℓ ℓ'}{A₀ A₁ : Set ℓ}(A₂ : A₀ ≡ A₁)
            {B₀ : A₀ → Set ℓ'}{B₁ : A₁ → Set ℓ'}
            {f₀ : {x : A₀} → B₀ x}{f₁ : {x : A₁} → B₁ x}
          → ({x₀ : A₀}{x₁ : A₁}(x₂ : x₀ ≃ x₁) → f₀ {x₀} ≃ f₁ {x₁})
          → (λ {x} →  f₀ {x}) ≃ (λ {x} → f₁ {x})
funexti≃ {ℓ}{ℓ'}{A} refl {B₀}{B₁}{f₀}{f₁} p

  = (ap (λ z → {x : A} → z x) ((funext (λ x → projT (p {x} r̃)))))
  ,≃ funexti (λ x → from≃ ( ap≃ {A = A → Set ℓ'}
                               {λ B → {x : A} → B x}
                               {λ B → B x}
                               (λ f → f {x})
                               {B₁}
                               {B₀}
                               (funext (λ x → projT (p {x} r̃) ⁻¹))
                               (uncoe (ap (λ z → {x : A} → z x)
                                 (funext (λ x₁ → projT (p r̃)))) ⁻¹̃)
                         ◾̃ p {x} r̃))

→≃ : ∀{ℓ ℓ'}{A₀ A₁ : Set ℓ}(A₂ : A₀ ≡ A₁)
     {B₀ : A₀ → Set ℓ'}{B₁ : A₁ → Set ℓ'}(B₂ : B₀ ≃ B₁)
   → ((x : A₀) → B₀ x) ≡ ((x : A₁) → B₁ x)
→≃ refl (refl ,≃ refl) = refl

→i≃ : ∀{ℓ ℓ'}{A₀ A₁ : Set ℓ}(A₂ : A₀ ≡ A₁)
      {B₀ : A₀ → Set ℓ'}{B₁ : A₁ → Set ℓ'}(B₂ : B₀ ≃ B₁)
    → ({x : A₀} → B₀ x) ≡ ({x : A₁} → B₁ x)
→i≃ refl (refl ,≃ refl) = refl

Σ≃ : ∀{ℓ ℓ'}{A₀ A₁ : Set ℓ}(A₂ : A₀ ≡ A₁)
     {B₀ : A₀ → Set ℓ'}{B₁ : A₁ → Set ℓ'}(B₂ : B₀ ≃ B₁)
   → Σ A₀ B₀ ≡ Σ A₁ B₁
Σ≃ refl (refl ,≃ refl) = refl

proj₁≃ : ∀{ℓ ℓ'}{A₀ A₁ : Set ℓ}(A₂ : A₀ ≡ A₁)
         {B₀ : A₀ → Set ℓ'}{B₁ : A₁ → Set ℓ'}(B₂ : B₀ ≃ B₁)
         {w₀ : Σ A₀ B₀}{w₁ : Σ A₁ B₁}(w₂ : w₀ ≃ w₁)
       → proj₁ w₀ ≃ proj₁ w₁
proj₁≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

≡≃ : ∀{ℓ}{A₀ A₁ : Set ℓ}(A₂ : A₀ ≡ A₁)
     {a₀₀ a₀₁ : A₀}{a₁₀ a₁₁ : A₁}
     (a₂₀ : a₀₀ ≃ a₁₀)(a₂₁ : a₀₁ ≃ a₁₁)
   → (a₀₀ ≡ a₀₁) ≃ (a₁₀ ≡ a₁₁)
≡≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

,Σ≃≃ : ∀{ℓ ℓ'}{A₀ A₁ : Set ℓ}
     {B₀ : A₀ → Set ℓ'}{B₁ : A₁ → Set ℓ'}(B₂ : B₀ ≃ B₁)
     {a₀ : A₀}{a₁ : A₁}(a₂ : a₀ ≃ a₁)
     {b₀ : B₀ a₀}{b₁ : B₁ a₁}(b₂ : b₀ ≃ b₁)
   → _≃_
     {A = Σ A₀ B₀}
     {B = Σ A₁ B₁}
     (a₀ ,Σ b₀)
     (a₁ ,Σ b₁)
,Σ≃≃ (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

,Σ≃' : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}
     {a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
     {b₀ : B a₀}{b₁ : B a₁}(b₂ : b₀ ≃ b₁)
   → _≡_ {A = Σ A B} (a₀ ,Σ b₀) (a₁ ,Σ b₁)
,Σ≃' refl (refl ,≃ refl) = refl

proj₁coe : {A₀ A₁ : Set}(A₂ : A₀ ≡ A₁)
           {B₀ : A₀ → Set}{B₁ : A₁ → Set}
           (B₂ : B₀ ≡[ ap (λ z → z → Set) A₂ ]≡ B₁)
           (p : Σ A₀ B₀ ≡ Σ A₁ B₁)
           {a₀ : A₀}{b₀ : B₀ a₀}
         → a₀ ≃ proj₁ (coe p (a₀ ,Σ b₀))
proj₁coe refl refl refl = refl ,≃ refl

UIP' : ∀{ℓ}{A B : Set ℓ}{a : A}{b : B}
       (p q : A ≡ B) → a ≡[ p ]≡ b → a ≡[ q ]≡ b
UIP' refl refl refl = refl

UIP'' : ∀{ℓ}{A : Set ℓ}{a b : A}(p : A ≡ A) → a ≡[ p ]≡ b → a ≡ b
UIP'' refl refl = refl

APP≃ : ∀{ℓ ℓ'}{A : Set ℓ}{B₀ B₁ : A → Set ℓ'}(B₂ : B₀ ≡ B₁)
     {f₀ : (x : A) → B₀ x}{f₁ : (x : A) → B₁ x}(f₂ : f₀ ≃ f₁)
   → {a : A} → f₀ a ≃ f₁ a
APP≃ refl (refl ,≃ refl) = refl ,≃ refl

APPi≃
  : ∀{ℓ ℓ'}{A : Set ℓ}{B₀ B₁ : A → Set ℓ'}(B₂ : B₀ ≡ B₁)
    {f₀ : {x : A} → B₀ x}{f₁ : {x : A} → B₁ x}
    (f₂ : (λ {x} → f₀ {x}) ≃ (λ {x} → f₁ {x}))
  → {a₀ a₁ : A}(a₂ : a₀ ≡ a₁) → f₀ {a₀} ≃ f₁ {a₁}
APPi≃ refl (refl ,≃ refl) refl = refl ,≃ refl

module ConorJM where

  -- John Major equality by Conor McBride

  data JM1 {A : Set}(a : A) : {B : Set} → B → Set where
    ref : JM1 a a

  toJM1 : {A B : Set}{a : A}{b : B} → a ≃ b → JM1 a b
  toJM1 (refl ,≃ refl) = ref

  fromJM1 : {A B : Set}{a : A}{b : B} → JM1 a b → a ≃ b
  fromJM1 ref = refl ,≃ refl

  isoJM : {A B : Set}{a : A}{b : B}(p : a ≃ b) → fromJM1 (toJM1 p) ≡ p
  isoJM (refl ,≃ refl) = refl

  isoJM1 : {A B : Set}{a : A}{b : B}(p : JM1 a b) → toJM1 (fromJM1 p) ≡ p
  isoJM1 ref = refl

APP≃'
  : ∀{ℓ ℓ'}{A : Set ℓ}{B₀ B₁ : A → Set ℓ'}(B₂ : B₀ ≡ B₁)
    {f₀ : (x : A) → B₀ x}{f₁ : (x : A) → B₁ x}(f₂ : f₀ ≃ f₁)
  → {a₀ a₁ : A}(a₂ : a₀ ≡ a₁) → f₀ a₀ ≃ f₁ a₁
APP≃' refl (refl ,≃ refl) refl = refl ,≃ refl

APP≃''
  : ∀{ℓ ℓ'}{A₀ A₁ : Set ℓ}
    {B₀ : A₀ → Set ℓ'}{B₁ : A₁ → Set ℓ'}(B₂ : B₀ ≃ B₁)
    {f₀ : (x : A₀) → B₀ x}{f₁ : (x : A₁) → B₁ x}(f₂ : f₀ ≃ f₁)
  → {a₀ : A₀}{a₁ : A₁}(a₂ : a₀ ≃ a₁) → f₀ a₀ ≃ f₁ a₁
APP≃'' (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

proj₂≃ : ∀{ℓ ℓ'}{A₀ A₁ : Set ℓ}(A₂ : A₀ ≡ A₁)
         {B₀ : A₀ → Set ℓ'}{B₁ : A₁ → Set ℓ'}(B₂ : B₀ ≃ B₁)
         {w₀ : Σ A₀ B₀}{w₁ : Σ A₁ B₁}(w₂ : w₀ ≃ w₁)
       → proj₂ w₀ ≃ proj₂ w₁
proj₂≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

proj₂coe : {A₀ A₁ : Set}(A₂ : A₀ ≡ A₁)
           {B₀ : A₀ → Set}{B₁ : A₁ → Set}(B₂ : B₀ ≡[ ap (λ z → z → Set) A₂ ]≡ B₁)
           (p : Σ A₀ B₀ ≡ Σ A₁ B₁)
           {a₀ : A₀}{b₀ : B₀ a₀}
         → b₀ ≃ proj₂ (coe p (a₀ ,Σ b₀))
proj₂coe refl refl refl = refl ,≃ refl
