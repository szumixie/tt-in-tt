{-# OPTIONS --no-eta #-}

-- normal types indexed by contexts and including terms

module NormTy.NTy where

open import Agda.Primitive
open import lib
open import JM

open import TT.Syntax
open import TT.Congr

data NTy : Con → Set

⌜_⌝T : ∀{Γ} → NTy Γ → Ty Γ

data NTy where
  NΠ  : {Γ : Con}(A : NTy Γ) → NTy (Γ , ⌜ A ⌝T) → NTy Γ
  NU  : {Γ : Con} → NTy Γ
  NEl : {Γ : Con}(Â : Tm Γ U) → NTy Γ

⌜ NΠ A B ⌝T = Π ⌜ A ⌝T ⌜ B ⌝T
⌜ NU ⌝T = U
⌜ NEl Â ⌝T = El Â

NTy= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
     → NTy Γ₀ ≡ NTy Γ₁
NTy= refl = refl

⌜⌝T= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}(A₂ : A₀ ≡[ NTy= Γ₂ ]≡ A₁)
     → ⌜ A₀ ⌝T ≡[ Ty= Γ₂ ]≡ ⌜ A₁ ⌝T
⌜⌝T= refl refl = refl

⌜⌝T≃ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}(A₂ : A₀ ≃ A₁)
     → ⌜ A₀ ⌝T ≃ ⌜ A₁ ⌝T
⌜⌝T≃ refl (refl ,≃ refl) = refl ,≃ refl

NΠ= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}(A₂ : A₀ ≡[ NTy= Γ₂ ]≡ A₁)
      {B₀ : NTy (Γ₀ , ⌜ A₀ ⌝T)}{B₁ : NTy (Γ₁ , ⌜ A₁ ⌝T)}(B₂ : B₀ ≡[ NTy= (,C= Γ₂ (⌜⌝T= Γ₂ A₂)) ]≡ B₁)
    → (NΠ A₀ B₀) ≡[ NTy= Γ₂ ]≡ (NΠ A₁ B₁)
NΠ= refl refl refl = refl

NEl= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
       {Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Â₀ ≡[ Tm= Γ₂ (U= Γ₂) ]≡ Â₁)
     → (NEl Â₀) ≡[ NTy= Γ₂ ]≡ (NEl Â₁)
NEl= refl refl = refl

record MNTy {i} : Set (lsuc i) where
  field
    NTyᴹ : ∀ Γ → NTy Γ → Set i
    NΠᴹ  : {Γ : Con}{A : NTy Γ}(Aᴹ : NTyᴹ Γ A)
           {B : NTy (Γ , ⌜ A ⌝T)}(Bᴹ : NTyᴹ (Γ , ⌜ A ⌝T) B) → NTyᴹ Γ (NΠ A B)
    NUᴹ  : {Γ : Con} → NTyᴹ Γ NU
    NElᴹ : {Γ : Con}(Â : Tm Γ U) → NTyᴹ Γ (NEl Â)

module _ {i}(M : MNTy {i}) where
  open MNTy M
  
  elimNTy : ∀{Γ}(A : NTy Γ) → NTyᴹ Γ A
  elimNTy (NΠ A B) = NΠᴹ (elimNTy A) (elimNTy B)
  elimNTy NU = NUᴹ
  elimNTy (NEl Â) = NElᴹ Â
