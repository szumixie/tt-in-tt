{-# OPTIONS --prop #-}

module FiniteModels.PropModel where

open import lib
open import Setoid.SProp.lib

open import TT.Decl
open import TT.Core
open import TT.Func
open import TT.Sigma

d : Decl
d = decl
  Prop
  (λ Γ → Γ → Prop)
  (λ Γ Δ → Liftp (Γ → Δ))
  λ Γ A → Liftp ((γ : Γ) → A γ)

c : Core d
c = record {c1 = record
  { • = ⊤p
  ; _,_ = Σp
  ; _[_]T = λ A σ γ → A (unliftp σ γ)
  ; id = liftp λ γ → γ
  ; _∘_ = λ σ δ → liftp λ γ → unliftp σ (unliftp δ γ)
  ; ε = liftp λ γ → _
  ; _,s_ = λ σ t → liftp λ γ → unliftp σ γ ,p unliftp t γ
  ; π₁ = λ σ → liftp λ γ → proj₁p (unliftp σ γ)
  ; _[_]t = λ t σ → liftp λ γ → unliftp t (unliftp σ γ)
  ; π₂ = λ σ → liftp λ γ → proj₂p (unliftp σ γ)
  } ; c2 = record
  { [id]T = refl
  ; [][]T = refl
  ; idl = refl
  ; idr = refl
  ; ass = refl
  ; ,∘ = refl
  ; π₁β = refl
  ; πη = refl
  ; εη = refl
  ; π₂β = refl
  } }

f : Func c
f = record
  { Π = λ A B γ → (α : A γ) → B (γ ,p α)
  ; Π[] = refl
  ; lam = λ t → liftp λ γ α → unliftp t (γ ,p α)
  ; app = λ t → liftp λ γ → unliftp t (proj₁p γ) (proj₂p γ)
  ; lam[] = refl
  ; Πβ = refl
  ; Πη = refl
  }

s : Sigma c
s = record
  { Σ' = λ A B γ → Σp (A γ) λ α → B (γ ,p α)
  ; Σ[] = refl
  ; _,Σ'_ = λ u v → liftp λ γ → unliftp u γ ,p unliftp v γ
  ; proj₁' = λ t → liftp λ γ → proj₁p (unliftp t γ)
  ; proj₂' = λ t → liftp λ γ → proj₂p (unliftp t γ)
  ; Σβ₁ = refl
  ; Σβ₂ = refl
  ; Ση = refl
  ; ,Σ[] = refl
  }
