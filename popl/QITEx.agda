module popl.QITEx where

open import lib

_∘_ : {A : Set}{B : A → Set}{C : {x : A} → B x → Set}
      (g : {x : A}(y : B x) → C y)(f : (x : A) → B x)
    → (x : A) → C (f x)
(g ∘ f) x = g (f x)

postulate isIso : (ℕ → ℕ) → Set

data T₀ : Set where
  leaf : T₀
  node : (ℕ → T₀) → T₀

data _~_ : T₀ → T₀ → Set where
  leaf : leaf ~ leaf
  node : {f g : ℕ → T₀} → (∀ {n} → f n ~ g n) → node f ~ node g
  perm : (g : ℕ → T₀)(f : ℕ → ℕ) → isIso f → node g ~ node (g ∘ f)

T : Set
T = T₀ / _~_

-- nodeT : (ℕ → T) → T
-- nodeT = {!!}

-- nodeTok : ∀ {f} → [ node f ] ≡ nodeT (λ i → [ f i ])
-- nodeTok = {!!}

data T' : Set where
  leaf' : T'
  node' : (ℕ → T') → T'

postulate
  perm' : (g : ℕ → T')(f : ℕ → ℕ) → isIso f → node' g ≡ node' (g ∘ f) 
  isSet  : {u v : T'}{e0 e1 : u ≡ v} → e0 ≡ e1

T'rec : {M : Set}(l : M)(n : (ℕ → M) → M)(p : (g : ℕ → M)(f : ℕ → ℕ) → isIso f → n g ≡ n (g ∘ f))
      → T' → M
T'rec l n p leaf' = l
T'rec l n p (node' f) = n (λ i → T'rec l n p (f i))

module ElimT
  (Tᴹ : T' → Set)
  (leafᴹ : Tᴹ leaf')
  (nodeᴹ : {f : ℕ → T'}(fᴹ : (n : ℕ) → Tᴹ (f n)) → Tᴹ (node' f))
  (permᴹ : {g : ℕ → T'}(gᴹ : (n : ℕ) → Tᴹ (g n))
           (f : ℕ → ℕ)(p : isIso f)
         → nodeᴹ gᴹ ≡[ ap Tᴹ (perm' g f p) ]≡ nodeᴹ (gᴹ ∘ f))
  where

    Elim : (t : T') → Tᴹ t
    Elim leaf' = leafᴹ
    Elim (node' f) = nodeᴹ (λ n → Elim (f n))
