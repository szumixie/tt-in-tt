{-# OPTIONS --no-eta --rewriting #-}

module NBE.Norm where

open import lib
open import JM
open import Cats
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Quote.Motives
open import NBE.Quote.Quote
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l
open import NBE.Glue

-- embedding renamings into neutral substitutions

⌜_⌝Vne : ∀{Ψ Ω} → Vars Ω Ψ → Nes Ω Ψ
⌜Vne⌝ : ∀{Ψ Ω}(β : Vars Ω Ψ) → ⌜ β ⌝V ≡ ⌜ ⌜ β ⌝Vne ⌝nes

⌜ εV ⌝Vne = εnes
⌜ β ,V x ⌝Vne = ⌜ β ⌝Vne ,nes var (coe (VarΓ= (ap (_[_]T _) (⌜Vne⌝ β))) x)

abstract
  ⌜Vne⌝ εV = refl
  ⌜Vne⌝ (β ,V x) = ,s≃' (⌜Vne⌝ β) (⌜coe⌝v (ap (_[_]T _) (⌜Vne⌝ β)) ⁻¹̃)

-- the identity semantic element

ID : (Γ : Con) → ⟦ Γ ⟧C $F id
ID Γ = coe ($F= ⟦ Γ ⟧C (⌜Vne⌝ idV ⁻¹ ◾ ⌜idV⌝)) (uC (QC Γ) $S ⌜ idV ⌝Vne)

-- normalisation for substitutions

NORMs : ∀{Γ Δ}(σ : Tms Γ Δ) → ≡NFC Δ $F σ
NORMs {Γ}{Δ} σ = qC (QC Δ) $S (σ ,Σ coe ($F= ⟦ Δ ⟧C idr) (⟦ σ ⟧s $S (id ,Σ ID Γ)))

norms : ∀{Γ Δ} → Tms Γ Δ → Nfs Γ Δ
norms σ = proj₁ (NORMs σ)

abstract
  compls : ∀{Γ Δ}(σ : Tms Γ Δ) → σ ≡ ⌜ norms σ ⌝nfs
  compls σ = proj₂ (NORMs σ)

-- normalisation for terms

NORMt : ∀{Γ A}(t : Tm Γ A) → ≡NFT A $F (id ,Σ coe (TmΓ= ([id]T ⁻¹)) t)
NORMt {Γ}{A} t = qT (QT A) $S ( id
                              ,Σ coe (TmΓ= ([id]T ⁻¹)) t
                              ,Σ ID Γ
                              ,Σ coe ($F= ⟦ A ⟧T pcoe) (⟦ t ⟧t $S (id ,Σ ID Γ)))
  where
    
    abstract
      pcoe : _≡_ {A = (TMC Γ ,P TMT A ,P ⟦ Γ ⟧C [ wkn ]F) $P Γ}
                 ((TMt t ^S ⟦ Γ ⟧C) $n (id ,Σ ID Γ))
                 (id ,Σ coe (TmΓ= ([id]T ⁻¹)) t ,Σ ID Γ)
      pcoe = Tbase= ⟦ Γ ⟧C refl ([id]t' ◾̃ uncoe (TmΓ= ([id]T ⁻¹))) r̃

normt : ∀{Γ A} → Tm Γ A → Nf Γ A
normt t = coe (NfΓ= [id]T) (proj₁ (NORMt t))

abstract
  complt : ∀{Γ A}(t : Tm Γ A) → t ≡ ⌜ normt t ⌝nf
  complt t = from≃ (uncoe (TmΓ= ([id]T ⁻¹)) ◾̃ to≃ (proj₂ (NORMt t)) ◾̃ ⌜coe⌝nf [id]T ⁻¹̃)

-- reasoning using normal forms

reas : (P : ∀{Γ A} → Tm Γ A → Set)(p : ∀{Γ A}(n : Nf Γ A) → P ⌜ n ⌝nf)
     → ∀{Γ A}(t : Tm Γ A) → P t
reas P p {Γ}{A} t = coe (ap P (complt t ⁻¹)) (p (normt t))
