{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import NBE.Nf

module NBE.experiments.Stab
  (norms  : ∀{Γ Δ} → Tms Γ Δ → Nfs Γ Δ)
  (compls : ∀{Γ Δ}(σ : Tms Γ Δ) → σ ≡ ⌜ norms σ ⌝nfs)
  (normt  : ∀{Γ A} → Tm Γ A → Nf Γ A)
  (complt : ∀{Γ A}(t : Tm Γ A) → t ≡ ⌜ normt t ⌝nf)
  (stabs  : ∀{Γ Δ}(τ : Nfs Γ Δ) → norms ⌜ τ ⌝nfs ≡ τ)
  (stabt  : ∀{Γ A}(n : Nf Γ A) → normt ⌜ n ⌝nf ≡ n)
  where

-- stability for contexts and types

open import NBE.experiments.NormalModel norms compls normt complt stabs stabt

open import NBE.experiments.NConTy

open import JM

open import TT.Congr


stabC : (Γ : NCon) → normC ⌜ Γ ⌝C ≡ Γ
stabT : {Γ : NCon}(A : NTy Γ) → normT ⌜ A ⌝T ≡[ NTy= (stabC Γ) ]≡ A

stabC •N = refl
stabC (Γ ,N A)

  = ,N= (stabC Γ)
        (stabT A)

stabT (ΠN {Γ} A B) = from≃ ( uncoe (NTy= (stabC Γ)) ⁻¹̃
                           ◾̃ ΠN≃ (stabC Γ)
                                 (from≡ (NTy= (stabC Γ)) (stabT A))
                                 (from≡ (NTy= (,N= (stabC Γ) (stabT A))) (stabT B)))

stabT (UN {Γ}) = from≃ ( uncoe (NTy= (stabC Γ)) ⁻¹̃
                       ◾̃ UN≃ (stabC Γ))

stabT (ElN {Γ} Â) = from≃ ( uncoe (NTy= (stabC Γ)) ⁻¹̃
                          ◾̃ ElN≃ (stabC Γ)
                                 (norm-elim {t = ⌜ Â ⌝nf} ⁻¹̃ ◾̃ to≃ (stabt Â)))
                                 
  where
  
    abstract
        inj⌜⌝nf'' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
                    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
                    {v₀ : Nf Γ₀ A₀}{v₁ : Nf Γ₁ A₁}
                  → ⌜ v₀ ⌝nf ≃ ⌜ v₁ ⌝nf → v₀ ≃ v₁
        inj⌜⌝nf'' refl (refl , refl) {v₀}{v₁} (refl , p)
        
          = to≃ (stabt v₀ ⁻¹ ◾ ap normt p ◾ stabt v₁)
          
    abstract
        norm-elim : ∀{Γ A}{t : Tm Γ A} → normt t ≃ normt' t
        norm-elim {Γ}{A}{t}
        
          = inj⌜⌝nf'' (complC Γ)
                      (from≡ (Ty= (complC Γ)) (complT A))
                      (to≃ (complt t ⁻¹) ◾̃ from≡ (Tm= (complC Γ) (complT A)) (complt' t))

