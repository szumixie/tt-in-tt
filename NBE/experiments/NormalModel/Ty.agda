{-# OPTIONS --no-eta #-}

open import lib hiding (_,_)
open import TT.Syntax
open import NBE.Nf

module NBE.experiments.NormalModel.Ty
  (norms  : ∀{Γ Δ} → Tms Γ Δ → Nfs Γ Δ)
  (compls : ∀{Γ Δ}(σ : Tms Γ Δ) → σ ≡ ⌜ norms σ ⌝nfs)
  (normt  : ∀{Γ A} → Tm Γ A → Nf Γ A)
  (complt : ∀{Γ A}(t : Tm Γ A) → t ≡ ⌜ normt t ⌝nf)
  (stabs  : ∀{Γ Δ}(τ : Nfs Γ Δ) → norms ⌜ τ ⌝nfs ≡ τ)
  (stabt  : ∀{Γ A}(n : Nf Γ A) → normt ⌜ n ⌝nf ≡ n)
  where

open import TT.Congr
open import TT.Laws
open import TT.Elim
open import NBE.experiments.NConTy
open import NBE.experiments.NormalModel.Motives
open import NBE.experiments.NormalModel.Cxt

-- substituting normal types by normal substitutions

_[_]nT : ∀{Γ Θ}(A : NTy Θ)(σ : Nfs ⌜ Γ ⌝C ⌜ Θ ⌝C) → NTy Γ
⌜[]nT⌝ : ∀{Γ Θ}{A : NTy Θ}{σ : Nfs ⌜ Γ ⌝C ⌜ Θ ⌝C}
       → ⌜ A ⌝T [ ⌜ σ ⌝nfs ]T ≡ ⌜ _[_]nT {Γ}{Θ} A σ ⌝T

_^nT_ : ∀{Γ Δ}(σ : Nfs ⌜ Γ ⌝C ⌜ Δ ⌝C)(A : NTy Δ) → Nfs ⌜ Γ ,N (A [ σ ]nT) ⌝C ⌜ Δ ,N A ⌝C
σ ^nT A = norms (coe (Tms= (,C= refl ⌜[]nT⌝) refl) (⌜ σ ⌝nfs ^ ⌜ A ⌝T))

abstract
  ⌜^nT⌝ : ∀{Γ Δ}{σ : Nfs ⌜ Γ ⌝C ⌜ Δ ⌝C}{A : NTy Δ}
        → ⌜ σ ⌝nfs ^ ⌜ A ⌝T
        ≡[ Tms= (,C= refl ⌜[]nT⌝) refl ]≡
          ⌜ _^nT_ {Γ}{Δ} σ A ⌝nfs
  ⌜^nT⌝ {σ = σ}{A} = compls (coe (Tms= (,C= refl ⌜[]nT⌝) refl) (⌜ σ ⌝nfs ^ ⌜ A ⌝T))

ΠN A B [ σ ]nT = ΠN (A [ σ ]nT) (B [ σ ^nT A ]nT)
UN [ σ ]nT = UN
ElN Â [ σ ]nT = ElN (normt (coe (TmΓ= U[]) (⌜ Â ⌝nf [ ⌜ σ ⌝nfs ]t)))
infixl 7 _[_]nT

abstract
  ⌜[]nT⌝ {Γ}{Δ}{A = ΠN A B}{σ}
    = Π[] ◾ Π= (refl {x = ⌜ Γ ⌝C})
               (⌜[]nT⌝ {A = A}{σ})
               ([]T= (,C= refl ⌜[]nT⌝) refl refl ⌜^nT⌝ ◾ ⌜[]nT⌝ {A = B}{σ ^nT A})

  ⌜[]nT⌝ {A = UN} = U[]
  
  ⌜[]nT⌝ {A = ElN Â}{σ}
    = El[] ◾ ap El (complt (coe (TmΓ= U[]) (⌜ Â ⌝nf [ ⌜ σ ⌝nfs ]t)))

-- methods

mTy : MethodsTy M mCon
mTy = record
  { _[_]Tᴹ = λ { {_}{Γ' Σ., pΓ}{_}{Δ' Σ., pΔ}(A' Σ., pA)(σ' Σ., pσ)
               → A' [ σ' ]nT Σ., ([]T= pΓ pΔ pA pσ ◾ ⌜[]nT⌝ {A = A'})
               }
  ; Uᴹ     = λ { {Γ}{Γ' Σ., pΓ}
               → UN Σ., U= pΓ
               }
  ; Elᴹ    = λ { {Γ}{Γ' Σ., pΓ}{Â}(Â' Σ., pÂ)
              → ElN Â' Σ., El= pΓ pÂ
               }
  ; Πᴹ     = λ { {Γ}{Γ' Σ., pΓ}{A}(A' Σ., pA){B}(B' Σ., pB)
               → ΠN A' B' Σ., Π= pΓ pA pB
               }
  }
