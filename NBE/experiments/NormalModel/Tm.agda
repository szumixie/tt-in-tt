{-# OPTIONS --no-eta #-}

open import lib hiding (_,_)
open import TT.Syntax
open import NBE.Nf

module NBE.experiments.NormalModel.Tm
  (norms  : ∀{Γ Δ} → Tms Γ Δ → Nfs Γ Δ)
  (compls : ∀{Γ Δ}(σ : Tms Γ Δ) → σ ≡ ⌜ norms σ ⌝nfs)
  (normt  : ∀{Γ A} → Tm Γ A → Nf Γ A)
  (complt : ∀{Γ A}(t : Tm Γ A) → t ≡ ⌜ normt t ⌝nf)
  (stabs  : ∀{Γ Δ}(τ : Nfs Γ Δ) → norms ⌜ τ ⌝nfs ≡ τ)
  (stabt  : ∀{Γ A}(n : Nf Γ A) → normt ⌜ n ⌝nf ≡ n)
  where

open import TT.Congr
open import TT.Laws
open import TT.Elim
open import NBE.experiments.NConTy
open import NBE.experiments.NormalModel.Motives
open import NBE.experiments.NormalModel.Cxt
open import NBE.experiments.NormalModel.Ty norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.Tms norms compls normt complt stabs stabt

mTm : MethodsTm M mCon mTy mTms
mTm = record
  { _[_]tᴹ = λ { {Γ}{Γ' Σ., pΓ}{Δ}{Δ' Σ., pΔ}{A}{A' Σ., pA}{t}(t' Σ., pt){σ}(σ' Σ., pσ)
               →   normt  (coe (Tm= pΓ ([]T= pΓ pΔ pA pσ ◾ ⌜[]nT⌝ {A = A'})) (t [ σ ]t))
               Σ., complt (coe (Tm= pΓ ([]T= pΓ pΔ pA pσ ◾ ⌜[]nT⌝ {A = A'})) (t [ σ ]t))
               }
  ; π₂ᴹ    = λ { {Γ}{Γ' Σ., pΓ}{Δ}{Δ' Σ., pΔ}{A}{A' Σ., pA}{σ}(σ' Σ., pσ)
               →   normt  (coe (Tm= pΓ ([]T= pΓ pΔ pA (compls (coe (Tms= pΓ pΔ) (π₁ σ))) ◾ ⌜[]nT⌝ {A = A'})) (π₂ σ))
               Σ., complt (coe (Tm= pΓ ([]T= pΓ pΔ pA (compls (coe (Tms= pΓ pΔ) (π₁ σ))) ◾ ⌜[]nT⌝ {A = A'})) (π₂ σ))
               }
  ; appᴹ   = λ { {Γ}{Γ' Σ., pΓ}{A}{A' Σ., pA}{B}{B' Σ., pB}{t}(t' Σ., pt)
               →   normt  (coe (Tm= (,C= pΓ pA) pB) (app t))
               Σ., complt (coe (Tm= (,C= pΓ pA) pB) (app t))
               }
  ; lamᴹ   = λ { {Γ}{Γ' Σ., pΓ}{A}{A' Σ., pA}{B}{B' Σ., pB}{t}(t' Σ., pt)
               →   normt  (coe (Tm= pΓ (Π= pΓ pA pB)) (lam t))
               Σ., complt (coe (Tm= pΓ (Π= pΓ pA pB)) (lam t))
               }
  }
