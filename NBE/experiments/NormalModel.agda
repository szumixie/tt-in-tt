{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import NBE.Nf

module NBE.experiments.NormalModel
  (norms  : ∀{Γ Δ} → Tms Γ Δ → Nfs Γ Δ)
  (compls : ∀{Γ Δ}(σ : Tms Γ Δ) → σ ≡ ⌜ norms σ ⌝nfs)
  (normt  : ∀{Γ A} → Tm Γ A → Nf Γ A)
  (complt : ∀{Γ A}(t : Tm Γ A) → t ≡ ⌜ normt t ⌝nf)
  (stabs  : ∀{Γ Δ}(τ : Nfs Γ Δ) → norms ⌜ τ ⌝nfs ≡ τ)
  (stabt  : ∀{Γ A}(n : Nf Γ A) → normt ⌜ n ⌝nf ≡ n)
  where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.Elim
open import NBE.experiments.NConTy
open import NBE.experiments.NormalModel.Motives
open import NBE.experiments.NormalModel.Cxt
open import NBE.experiments.NormalModel.Ty  norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.Tms norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.Tm norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.HTy norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.HTms norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.HTm norms compls normt complt stabs stabt

open elim M mCon mTy mTms mTm mHTy mHTms mHTm

normC : Con → NCon
normC Γ = proj₁ (Con-elim Γ)

abstract
  complC : (Γ : Con) → Γ ≡ ⌜ normC Γ ⌝C
  complC Γ = proj₂ (Con-elim Γ)

normT : ∀{Γ} → Ty Γ → NTy (normC Γ)
normT A = proj₁ (Ty-elim A)

abstract
  complT : ∀{Γ}(A : Ty Γ) → A ≡[ Ty= (complC Γ) ]≡ ⌜ normT A ⌝T
  complT A = proj₂ (Ty-elim A)

normt' : ∀{Γ A} → Tm Γ A → Nf ⌜ normC Γ ⌝C ⌜ normT A ⌝T
normt' t = proj₁ (Tm-elim t)

abstract
  complt' : ∀{Γ A}(t : Tm Γ A) → t ≡[ Tm= (complC Γ) (complT A) ]≡ ⌜ normt' t ⌝nf
  complt' t = proj₂ (Tm-elim t)
