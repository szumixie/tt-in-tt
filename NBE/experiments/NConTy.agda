{-# OPTIONS --no-eta #-}

module NBE.experiments.NConTy where

open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.RecOld
open import NBE.Nf

data NCon : Set
data NTy  : NCon → Set

data NCon where
  •N   : NCon
  _,N_ : (Γ : NCon) → NTy Γ → NCon

⌜_⌝C : NCon → Con
⌜_⌝T : ∀{Γ} → NTy Γ → Ty ⌜ Γ ⌝C

⌜ •N ⌝C = •
⌜ Γ ,N A ⌝C = ⌜ Γ ⌝C , ⌜ A ⌝T

data NTy where
  ΠN  : {Γ : NCon}(A : NTy Γ) → NTy (Γ ,N A) → NTy Γ
  UN  : {Γ : NCon} → NTy Γ
  ElN : {Γ : NCon}(Â : Nf ⌜ Γ ⌝C U) → NTy Γ

⌜ ΠN A B ⌝T = Π ⌜ A ⌝T ⌜ B ⌝T
⌜ UN ⌝T = U
⌜ ElN Â ⌝T = El ⌜ Â ⌝nf

-- congruence rules

NTy= : {Γ₀ Γ₁ : NCon}(Γ₂ : Γ₀ ≡ Γ₁) → NTy Γ₀ ≡ NTy Γ₁
NTy= = ap NTy

⌜⌝C= : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁) → ⌜ Γ₀ ⌝C ≡ ⌜ Γ₁ ⌝C
⌜⌝C= refl = refl

⌜⌝T= : {Γ₀ Γ₁ : NCon}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : NTy Γ₀}{A₁ : NTy Γ₁}(A₂ : A₀ ≡[ NTy= Γ₂ ]≡ A₁)
      → ⌜ A₀ ⌝T ≡[ Ty= (⌜⌝C= Γ₂) ]≡ ⌜ A₁ ⌝T
⌜⌝T= refl refl = refl

⌜⌝T≃ : {Γ₀ Γ₁ : NCon}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : NTy Γ₀}{A₁ : NTy Γ₁}(A₂ : A₀ ≃ A₁)
      → ⌜ A₀ ⌝T ≃ ⌜ A₁ ⌝T
⌜⌝T≃ refl (refl ,≃ refl) = refl ,≃ refl

,N= : {Γ₀ Γ₁ : NCon}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}(A₂ : A₀ ≡[ NTy= Γ₂ ]≡ A₁)
    → Γ₀ ,N A₀ ≡ Γ₁ ,N A₁
,N= refl refl = refl

,N≃ : {Γ₀ Γ₁ : NCon}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}(A₂ : A₀ ≃ A₁)
    → Γ₀ ,N A₀ ≡ Γ₁ ,N A₁
,N≃ refl (refl ,≃ refl) = refl

ΠN= : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}(A₂ : A₀ ≡[ NTy= Γ₂ ]≡ A₁)
      {B₀ : NTy (Γ₀ ,N A₀)}{B₁ : NTy (Γ₁ ,N A₁)}(B₂ : B₀ ≡[ NTy= (,N= Γ₂ A₂) ]≡ B₁)
    → ΠN A₀ B₀ ≡[ NTy= Γ₂ ]≡ ΠN A₁ B₁
ΠN= refl refl refl = refl

ΠN=' : ∀{Γ}{A₀ A₁ : NTy Γ}(A₂ : A₀ ≡ A₁)
       {B₀ : NTy (Γ ,N A₀)}{B₁ : NTy (Γ ,N A₁)}(B₂ : B₀ ≃ B₁)
     → ΠN A₀ B₀ ≡ ΠN A₁ B₁
ΠN=' refl (refl ,≃ refl) = refl

ΠN≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}(A₂ : A₀ ≃ A₁)
      {B₀ : NTy (Γ₀ ,N A₀)}{B₁ : NTy (Γ₁ ,N A₁)}(B₂ : B₀ ≃ B₁)
    → ΠN A₀ B₀ ≃ ΠN A₁ B₁
ΠN≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

UN= : {Γ₀ Γ₁ : NCon}(Γ₂ : Γ₀ ≡ Γ₁) → UN {Γ₀} ≡[ NTy= Γ₂ ]≡ UN {Γ₁}
UN= refl = refl

UN≃ : {Γ₀ Γ₁ : NCon}(Γ₂ : Γ₀ ≡ Γ₁) → UN {Γ₀} ≃ UN {Γ₁}
UN≃ refl = refl ,≃ refl

ElN= : {Γ₀ Γ₁ : NCon}(Γ₂ : Γ₀ ≡ Γ₁)
       (U₂ : UN {Γ₀} ≡[ NTy= Γ₂ ]≡ UN {Γ₁})
       {Â₀ : Nf ⌜ Γ₀ ⌝C U}{Â₁ : Nf ⌜ Γ₁ ⌝C U}(Â₂ : Â₀ ≡[ Nf= (⌜⌝C= Γ₂) (⌜⌝T= Γ₂ U₂) ]≡ Â₁)
     → ElN Â₀ ≡[ NTy= Γ₂ ]≡ ElN Â₁
ElN= refl refl refl = refl

ElN≃ : {Γ₀ Γ₁ : NCon}(Γ₂ : Γ₀ ≡ Γ₁){Â₀ : Nf ⌜ Γ₀ ⌝C U}{Â₁ : Nf ⌜ Γ₁ ⌝C U}(Â₂ : Â₀ ≃ Â₁)
     → ElN {Γ₀} Â₀ ≃ ElN {Γ₁} Â₁
ElN≃ refl (refl ,≃ refl) = refl ,≃ refl
