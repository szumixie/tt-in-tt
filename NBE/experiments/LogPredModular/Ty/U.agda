{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Ty.U
  (U̅ : FamPSh TMU)
  where

open import lib
open import JM.JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt

open Motives M
open MethodsCon mCon

module mUᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  where

    p$F : ∀{Ψ} → (TMC Γ ,P TMT U ,P ⟦Γ⟧ [ wkn ]F) $P Ψ → Set
    p$F (ρ , s , α) = U̅ $F coe (TmΓ= U[]) s

    abstract
      coe$F$ : ∀{Ψ Ω}{f : Vars Ω Ψ}{ρ : TMC Γ $P Ψ}{u : TMT U $F ρ}
             → coe (TmΓ= U[]) (coe (TmΓ= U[]) u [ ⌜ f ⌝V ]t)
             ≡ coe (TmΓ= U[]) (coe (TmΓ= [][]T) (u [ ⌜ f ⌝V ]t))
      coe$F$ = from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                     ◾̃ coe[]t' U[]
                     ◾̃ uncoe (TmΓ= [][]T)
                     ◾̃ uncoe (TmΓ= U[]))

    p$F$ : ∀{Ψ Ω}(f : Vars Ω Ψ)
           {α : (TMC Γ ,P TMT U ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}
         → p$F α
         → p$F ((TMC Γ ,P TMT U ,P ⟦Γ⟧ [ wkn ]F) $P f $ α)
    p$F$ f a = coe ($F= U̅ coe$F$) (U̅ $F f $ a)

    abstract
      pidF : ∀{Ψ : Con}{α : (TMC Γ ,P TMT U ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}
             {a : p$F α}
           → p$F$ idV {α} a ≡[ ap p$F (idP (TMC Γ ,P TMT U ,P ⟦Γ⟧ [ wkn ]F) {α = α}) ]≡ a
      pidF {Ψ}{α}{a}
      
        = from≃ ( uncoe (ap p$F (idP (TMC Γ ,P TMT U ,P ⟦Γ⟧ [ wkn ]F))) ⁻¹̃
                ◾̃ uncoe ($F= U̅ coe$F$) ⁻¹̃
                ◾̃ idF' U̅)

    abstract
      pcompF : ∀{Ψ Ω Ξ}{f : Vars Ω Ψ}{g : Vars Ξ Ω}
               {α : (TMC Γ ,P TMT U ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}{a : p$F α}
             → p$F$ (f ∘V g) {α} a
             ≡[ ap p$F (compP (TMC Γ ,P TMT U ,P ⟦Γ⟧ [ wkn ]F) {α = α}) ]≡
               p$F$ g {(TMC Γ ,P TMT U ,P ⟦Γ⟧ [ wkn ]F) $P f $ α} (p$F$ f {α} a)
      pcompF {Ψ}{Ω}{Ξ}{f}{g}{α}{a}
      
        = from≃ ( uncoe (ap p$F (compP (TMC Γ ,P TMT U ,P ⟦Γ⟧ [ wkn ]F))) ⁻¹̃
                ◾̃ uncoe ($F= U̅ coe$F$) ⁻¹̃
                ◾̃ compF' U̅
                ◾̃ $Ff$≃ U̅ (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                                 ◾̃ coe[]t' U[]
                                 ◾̃ uncoe (TmΓ= [][]T)
                                 ◾̃ uncoe (TmΓ= U[])))
                          (uncoe ($F= U̅ coe$F$))
                ◾̃ uncoe ($F= U̅ coe$F$))
      
    Uᴹ : Tyᴹ ⟦Γ⟧ U
    Uᴹ = record
      { _$F_   = p$F
      ; _$F_$_ = p$F$
      ; idF    = pidF
      ; compF  = pcompF
      }
