{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Ty.El
  (U̅ : FamPSh TMU)
  (E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F))
  where

open import lib
open import JM.JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty.U U̅

open Motives M
open MethodsCon mCon

Elbase= : ∀{Ψ}{ρ₀ ρ₁ : TMU $P Ψ}(ρ₂ : ρ₀ ≡ ρ₁)
          {t₀ : TMEl $F ρ₀}{t₁ : TMEl $F ρ₁}(t₂ : t₀ ≃ t₁)
          {v₀ : U̅ $F ρ₀}{v₁ : U̅ $F ρ₁}(v₂ : v₀ ≃ v₁)
        → _≡_ {A = (TMU ,P TMEl ,P U̅ [ wkn ]F) $P Ψ} (ρ₀ , t₀ , v₀) (ρ₁ , t₁ , v₁)
Elbase= refl (refl , refl) (refl , refl) = refl


module mElᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {Â : Tm Γ U}(⟦Â⟧ : Tmᴹ ⟦Γ⟧ mUᴹ.Uᴹ Â)  
  where

    p$F : ∀{Ψ} → (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ → Set
    p$F (ρ , s , α) = E̅l $F ( coe (TmΓ= U[]) (Â [ ρ ]t)
                            , coe (TmΓ= El[]) s
                            , ⟦Â⟧ $S (ρ , α))

    abstract
      coe$F$ : ∀{Ψ Ω}{f : Vars Ω Ψ}
               {ρ : TMC Γ $P Ψ}{u : TMT (El Â) $F ρ}{α : ⟦Γ⟧ $F ρ}
             → _≡_ {A = (TMU ,P TMEl ,P U̅ [ wkn ]F) $P Ω}
                   ( coe (TmΓ= U[]) (coe (TmΓ= U[]) (Â [ ρ ]t) [ ⌜ f ⌝V ]t)
                   , coe (TmΓ= El[]) (coe (TmΓ= El[]) u [ ⌜ f ⌝V ]t)
                   , U̅ $F f $ (⟦Â⟧ $S (ρ , α)))
                   ( coe (TmΓ= U[]) (Â [ ρ ∘ ⌜ f ⌝V ]t)
                   , coe (TmΓ= El[]) (coe (TmΓ= [][]T) (u [ ⌜ f ⌝V ]t))
                   , ⟦Â⟧ $S (ρ ∘ ⌜ f ⌝V , ⟦Γ⟧ $F f $ α))
      coe$F$ {Ψ}{Ω}{f}{ρ}{u}{α}
      
        = Elbase= (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                         ◾̃ coe[]t' U[]
                         ◾̃ [][]t'
                         ◾̃ uncoe (TmΓ= U[])))
                  ( uncoe (TmΓ= El[]) ⁻¹̃
                  ◾̃ coe[]t' El[]
                  ◾̃ uncoe (TmΓ= [][]T)
                  ◾̃ uncoe (TmΓ= El[]))
                  ( uncoe ($F= U̅ mUᴹ.coe$F$)
                  ◾̃ uncoe (ap (_$F_ (mUᴹ.Uᴹ {Γ}{⟦Γ⟧})) (natn (TMt Â ^S ⟦Γ⟧)))
                  ◾̃ to≃ (natS ⟦Â⟧))

    p$F$ : ∀{Ψ Ω}(f : Vars Ω Ψ){α : (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}
         → p$F α
         → p$F ((TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F) $P f $ α)
    p$F$ f {ρ , u , α} a = coe ($F= E̅l coe$F$) (E̅l $F f $ a)

    abstract
      pidF : ∀{Ψ}{α : (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}
             {a : p$F α}
           → p$F$ idV a ≡[ ap p$F (idP (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F)) ]≡ a
      pidF = from≃ ( uncoe (ap p$F (idP (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F))) ⁻¹̃
                   ◾̃ uncoe ($F= E̅l coe$F$) ⁻¹̃
                   ◾̃ idF' E̅l)

    abstract
      pcompF : ∀{Ψ Ω Ξ}{f : Vars Ω Ψ}{g : Vars Ξ Ω}
               {α : (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}{a : p$F α}
             → p$F$ (f ∘V g) a
             ≡[ ap p$F (compP (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F)) ]≡
               p$F$ g (p$F$ f a)
      pcompF = from≃ ( uncoe (ap p$F (compP (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F))) ⁻¹̃
                     ◾̃ uncoe ($F= E̅l coe$F$) ⁻¹̃
                     ◾̃ compF' E̅l
                     ◾̃ $Ff$≃ E̅l (Elbase= (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                                                ◾̃ coe[]t' U[]
                                                ◾̃ [][]t'
                                                ◾̃ uncoe (TmΓ= U[])))
                                         ( uncoe (TmΓ= El[]) ⁻¹̃
                                         ◾̃ coe[]t' El[]
                                         ◾̃ uncoe (TmΓ= [][]T)
                                         ◾̃ uncoe (TmΓ= El[]))
                                         ( uncoe ($F= U̅ mUᴹ.coe$F$)
                                         ◾̃ uncoe (ap (_$F_ (mUᴹ.Uᴹ {Γ}{⟦Γ⟧})) (natn (TMt Â ^S ⟦Γ⟧)))
                                         ◾̃ to≃ (natS ⟦Â⟧)))
                                (uncoe ($F= E̅l coe$F$))
                     ◾̃ uncoe ($F= E̅l coe$F$))
      
    Elᴹ : Tyᴹ ⟦Γ⟧ (El Â)
    Elᴹ = record
      { _$F_   = p$F
      ; _$F_$_ = p$F$
      ; idF    = pidF
      ; compF  = pcompF
      }
