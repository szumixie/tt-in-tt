module NBE.LogPredModular.TM where

open import lib
open import Cats
open import TT.Syntax renaming (_$_ to _$$_)

-- TM is a weak morphism in the category of models from the term model
-- to the presheaf model

record TM (C : Cat) : Set₁ where
  open Cat C
  field
    -- the maps
    TMC : Con → PSh C
    TMT : ∀{Γ} → Ty Γ → FamPSh (TMC Γ)
    TMs : ∀{Γ Δ} → Tms Γ Δ → TMC Γ →n TMC Δ
    TMt : ∀{Γ A} → Tm Γ A → TMC Γ →S TMT A

    ------------------------------------------------------------------
    -- substitution calculus
    ------------------------------------------------------------------

    -- Con
    
    -- we don't have
    -- 
    --   TMC (Γ , A) ≡ TMC Γ ,P TMT A
    -- 
    -- but we have an isomorphism of presheaves
    π₁TM : ∀{Γ A} → TMC (Γ , A) →n TMC Γ
    π₂TM : ∀{Γ A} → TMC (Γ , A) →S (TMT A [ π₁TM ]F)
    _,TM_ : ∀{Γ A} → (TMC Γ ,P TMT A) →n TMC (Γ , A)

    -- Ty

    TM[]T : ∀{Γ Θ}{A : Ty Θ}{σ : Tms Γ Θ}
          → TMT (A [ σ ]T) ≡ TMT A [ TMs σ ]F

    -- Tms

    TM∘ : ∀{Γ Θ Δ}{σ : Tms Θ Δ}{ν : Tms Γ Θ}
        → TMs (σ ∘ ν) ≡ TMs σ ∘n TMs ν
    TMid : ∀{Γ} → TMs {Γ} id ≡ idn

    -- Tm

    TM[]t : ∀{Γ Θ}{A : Ty Θ}{t : Tm Θ A}{σ : Tms Γ Θ}
          → TMt (t [ σ ]t)
          ≡[ ap (_→S_ (TMC Γ)) TM[]T ]≡
            TMt t [ TMs σ ]S

    ------------------------------------------------------------------
    -- base type and base family
    ------------------------------------------------------------------

    ------------------------------------------------------------------
    -- function space
    ------------------------------------------------------------------
