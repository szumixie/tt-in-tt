module NBE.LogPredModular.Yoneda where

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import Cats

YC : Con → PSh (REN ᵒᵖ)
YC Γ = record
  { _$P_   = λ Δ → Tms Δ Γ
  ; _$P_$_ = λ β ρ → ρ ∘ ⌜ β ⌝V
  ; idP    = ap (_∘_ _) ⌜idV⌝ ◾ idr
  ; compP  = ap (_∘_ _) ⌜∘V⌝ ⁻¹ ◾ ass ⁻¹
  }

abstract
  YTidF : ∀{Γ Δ}{A : Ty Γ}{α : YC Γ $P Δ}{t : Tm Δ (A [ α ]T)}
         → coe (TmΓ= [][]T) (t [ ⌜ idV ⌝V ]t)
         ≡[ ap (λ z → Tm Δ (A [ z ]T)) (idP (YC Γ)) ]≡
           t
  YTidF {A = A} = from≃ ( uncoe (ap (λ z → Tm _ (A [ z ]T)) (idP (YC _))) ⁻¹̃
                         ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                         ◾̃ []t≃ refl (to≃ ⌜idV⌝)
                         ◾̃ from≡ (TmΓ= [id]T) [id]t)

abstract
  YTcompF : ∀{Γ}{A : Ty Γ}{Δ Θ Ω : Con}{β : Vars Θ Δ}{γ : Vars Ω Θ}
             {α : YC Γ $P Δ} {t : Tm Δ (A [ α ]T)}
           → coe (TmΓ= [][]T) (t [ ⌜ β ∘V γ ⌝V ]t)
           ≡[ ap (λ z → Tm Ω (A [ z ]T)) (compP (YC Γ)) ]≡
             coe (TmΓ= [][]T) (coe (TmΓ= [][]T) (t [ ⌜ β ⌝V ]t) [ ⌜ γ ⌝V ]t)
  YTcompF {A = A} = from≃ ( uncoe (ap (λ z → Tm _ (A [ z ]T)) (compP (YC _))) ⁻¹̃
                           ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                           ◾̃ []t≃ refl (to≃ (⌜∘V⌝ ⁻¹))
                           ◾̃ [][]t' ⁻¹̃
                           ◾̃ coe[]t' [][]T ⁻¹̃
                           ◾̃ uncoe (TmΓ= [][]T))

YT : ∀{Γ} → Ty Γ → FamPSh (YC Γ)
YT A = record
  { _$F_   = λ {Δ} ρ → Tm Δ (A [ ρ ]T)
  ; _$F_$_ = λ {Δ}{Θ} β {ρ} t → coe (TmΓ= [][]T) (t [ ⌜ β ⌝V ]t)
  ; idF    = YTidF
  ; compF  = YTcompF
  }

Ys : ∀{Γ Δ} → Tms Δ Γ → YC Δ →n YC Γ
Ys ρ = record
  { _$n_ = _∘_ ρ
  ; natn = ass
  }

Yt : ∀{Γ A} → Tm Γ A → YC Γ →S YT A
Yt t = record
  { _$S_ = _[_]t t
  ; natS = [][]t
  }

abstract
  YUidP : {I : Con} {α : Tm I U} → coe (TmΓ= U[]) (α [ ⌜ idV ⌝V ]t) ≡ α
  YUidP = from≃ (uncoe (TmΓ= U[]) ⁻¹̃ ◾̃ [⌜idV⌝]t)

abstract
  YUcompP : {I : Con} {J : Con} {K : Con} {f : Vars J I}
      {g : Vars K J} {α : Tm I U} →
      coe (TmΓ= U[]) (α [ ⌜ f ∘V g ⌝V ]t) ≡
      coe (TmΓ= U[]) (coe (TmΓ= U[]) (α [ ⌜ f ⌝V ]t) [ ⌜ g ⌝V ]t)
  YUcompP = from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                   ◾̃ [⌜∘V⌝]t ⁻¹̃
                   ◾̃ coe[]t' U[] ⁻¹̃
                   ◾̃ uncoe (TmΓ= U[]))

YU : PSh (REN ᵒᵖ)
YU =  record
  { _$P_   = λ Γ → Tm Γ U
  ; _$P_$_ = λ β t → coe (TmΓ= U[]) (t [ ⌜ β ⌝V ]t)
  ; idP    = YUidP
  ; compP  = YUcompP
  }

abstract
  YElidF : {I : Con} {α : YU $P I} {x : Tm I (El α)} →
      coe (TmΓ= El[]) (x [ ⌜ idV ⌝V ]t) ≡[
      ap (λ Â → Tm I (El Â)) (idP YU) ]≡ x
  YElidF = from≃ ( uncoe (ap (λ Â → Tm _ (El Â)) (idP YU)) ⁻¹̃
                  ◾̃ uncoe (TmΓ= El[]) ⁻¹̃
                  ◾̃ [⌜idV⌝]t)

abstract
  YElcompF : {I : Con} {J : Con} {K : Con} {f : Vars J I}
      {g : Vars K J} {α : YU $P I} {x : Tm I (El α)} →
      coe (TmΓ= El[]) (x [ ⌜ f ∘V g ⌝V ]t) ≡[
      ap (λ Â → Tm K (El Â)) (compP YU) ]≡
      coe (TmΓ= El[]) (coe (TmΓ= El[]) (x [ ⌜ f ⌝V ]t) [ ⌜ g ⌝V ]t)
  YElcompF = from≃ ( uncoe (ap (λ Â → Tm _ (El Â)) (compP YU)) ⁻¹̃
                    ◾̃ uncoe (TmΓ= El[]) ⁻¹̃
                    ◾̃ [⌜∘V⌝]t ⁻¹̃
                    ◾̃ coe[]t' El[] ⁻¹̃
                    ◾̃ uncoe (TmΓ= El[]))

YEl : FamPSh YU
YEl = record
  { _$F_   = λ {Γ} Â → Tm Γ (El Â)
  ; _$F_$_ = λ β t → coe (TmΓ= El[]) (t [ ⌜ β ⌝V ]t)
  ; idF    = YElidF
  ; compF  = YElcompF
  }

open import NBE.LogPredModular.TM

tm : TM (REN ᵒᵖ)
tm = record
  { TMC = YC
  ; TMT = YT
  ; TMs = Ys
  ; TMt = Yt
  ; π₁TM = {!!}
  ; π₂TM = {!!}
  ; _,TM_ = {!!}
  ; TM[]T = {!!}
  ; TM∘ = {!!}
  ; TMid = {!!}
  ; TM[]t = {!!}
  }
