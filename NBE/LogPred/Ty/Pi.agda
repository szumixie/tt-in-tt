{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Ty.Pi where

open import lib hiding (lift)
open import JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty.Subst

open Motives M
open MethodsCon mCon

module mΠᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {A : Ty Γ}(⟦A⟧ : Tyᴹ ⟦Γ⟧ A)
  {B : Ty (Γ , A)}(⟦B⟧ : Tyᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) B)
  where

    open import NBE.LogPred.Ty.Pi.Exp {Γ}{⟦Γ⟧}{A}{⟦A⟧}{B} ⟦B⟧
    open import NBE.LogPred.Ty.Pi.LiftExp {Γ}{⟦Γ⟧}{A}{⟦A⟧}{B}{⟦B⟧}
    open import NBE.LogPred.Ty.Pi.LiftId {Γ}{⟦Γ⟧}{A}{⟦A⟧}{B}{⟦B⟧}
    open import NBE.LogPred.Ty.Pi.LiftComp {Γ}{⟦Γ⟧}{A}{⟦A⟧}{B}{⟦B⟧}

    Πᴹ : Tyᴹ ⟦Γ⟧ (Π A B)
    Πᴹ = record
      { _$F_   = E
      ; _$F_$_ = lift
      ; idF    = liftId
      ; compF  = liftComp
      }
