{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

open import lib hiding (lift)
open import JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty.Subst
open import NBE.LogPred.Ty.Pi.Exp

open Motives M
open MethodsCon mCon

open import NBE.Cheat

module NBE.LogPred.Ty.Pi.LiftId
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {A : Ty Γ}{⟦A⟧ : Tyᴹ ⟦Γ⟧ A}
  {B : Ty (Γ , A)}{⟦B⟧ : Tyᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) B}
  {Ψ : Con}
  {α' : (TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}  
  {e : E ⟦B⟧ α'}
  where

open import NBE.LogPred.Ty.Pi.LiftExp {⟦B⟧ = ⟦B⟧}{Ψ} idV {α'}

liftId : lift e ≡[ ap (E ⟦B⟧) (idP (TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F)) ]≡ e
liftId = cheat
