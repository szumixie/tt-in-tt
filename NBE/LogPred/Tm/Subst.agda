{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Tm.Subst
  (U̅ : FamPSh TMU) -- U^--
  (E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F)) -- E^--l
  where

open import lib
open import JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty U̅ E̅l
open import NBE.LogPred.Tms U̅ E̅l
open import NBE.LogPred.Ty.Subst

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms

module m[]tᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {Δ : Con}{⟦Δ⟧ : Conᴹ Δ}
  {A : Ty Δ}{⟦A⟧ : Tyᴹ ⟦Δ⟧ A}
  {t : Tm Δ A}(⟦t⟧ : Tmᴹ ⟦Δ⟧ ⟦A⟧ t)
  {σ : Tms Γ Δ}(⟦σ⟧ : Tmsᴹ ⟦Γ⟧ ⟦Δ⟧ σ)
  where

    abstract
      pcoe$S : ∀{Ψ}{ρ : TMC Γ $P Ψ}{α : ⟦Γ⟧ $F ρ}
             → _≡_ {A = (TMC Δ ,P TMT A ,P ⟦Δ⟧ [ wkn ]F) $P Ψ}
                   ((TMt t ^S ⟦Δ⟧) $n (σ ∘ ρ ,Σ ⟦σ⟧ $S (ρ ,Σ α)))
                   ( σ ∘ proj₁ (proj₁ ((TMt (t [ σ ]t) ^S ⟦Γ⟧) $n (ρ ,Σ α)))
                   ,Σ coe (TmΓ= [][]T) (proj₂ (proj₁ ((TMt (t [ σ ]t) ^S ⟦Γ⟧) $n (ρ ,Σ α))))
                   ,Σ ⟦σ⟧ $S (proj₁ (proj₁ ((TMt (t [ σ ]t) ^S ⟦Γ⟧) $n (ρ ,Σ α))) ,Σ proj₂ ((TMt (t [ σ ]t) ^S ⟦Γ⟧) $n (ρ ,Σ α))))
      pcoe$S {Ψ}{ρ}{α} = Tbase= ⟦Δ⟧ refl ([][]t' ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T)) r̃

    p$S : ∀{Ψ}(α : (TMC Γ ,P ⟦Γ⟧) $P Ψ)
        → ⟦A⟧ [ ⟦σ⟧ ]Tᴹ [ TMt (t [ σ ]t) ^S ⟦Γ⟧ ]F $F α
    p$S {Ψ}(ρ ,Σ α) = coe ($F= ⟦A⟧ pcoe$S) (⟦t⟧ $S (σ ∘ ρ ,Σ ⟦σ⟧ $S (ρ ,Σ α)))

    abstract
      pnatS : ∀{Ψ Ω}{f : Vars Ω Ψ}{α : (TMC Γ ,P ⟦Γ⟧) $P Ψ}
            → ⟦A⟧ [ ⟦σ⟧ ]Tᴹ [ TMt (t [ σ ]t) ^S ⟦Γ⟧ ]F $F f $ p$S α
            ≡ p$S ((TMC Γ ,P ⟦Γ⟧) $P f $ α)
      pnatS = from≃ ( uncoe (ap (_$F_ (⟦A⟧ [ ⟦σ⟧ ]Tᴹ)) (natn (TMt (t [ σ ]t) ^S ⟦Γ⟧))) ⁻¹̃
                    ◾̃ uncoe ($F= ⟦A⟧ (m[]Tᴹ.$F$coe ⟦A⟧ ⟦σ⟧)) ⁻¹̃
                    ◾̃ $Ff$≃ ⟦A⟧
                            (pcoe$S ⁻¹)
                            (uncoe ($F= ⟦A⟧ pcoe$S) ⁻¹̃)
                    ◾̃ from≡ ($F= ⟦A⟧ (,Σ≃' (,Σ≃' refl (refl ,≃ [][]t)) (refl ,≃ refl))) (natS ⟦t⟧)
                    ◾̃ t$S≃ {t = ⟦t⟧}
                           (,Σ≃' ass
                                (from≡ (ap (_$F_ ⟦Δ⟧) ass) (natS ⟦σ⟧)))
                    ◾̃ uncoe ($F= ⟦A⟧ pcoe$S))

    _[_]tᴹ : Tmᴹ ⟦Γ⟧ (⟦A⟧ [ ⟦σ⟧ ]Tᴹ) (t [ σ ]t)
    _[_]tᴹ = record
      { _$S_ = p$S
      ; natS = pnatS
      }
