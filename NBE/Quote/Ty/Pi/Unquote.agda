{-# OPTIONS --no-eta --rewriting #-}

module NBE.Quote.Ty.Pi.Unquote where

open import lib
open import JM
open import Cats
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Quote.Motives
open import NBE.Quote.Cxt
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l
import NBE.LogPred.Ty.Pi

open Motives M
open MethodsCon mCon

open import NBE.Cheat

open import NBE.Glue

module uT$S
  {Γ : Con}{Γᴹ : Conᴹ Γ}
  {A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A)
  {B : Ty (Γ , A)}(Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B)
  {Ψ : Con}
  (δ : TMC Γ $P Ψ)
  (n : NET (Π A B) $F δ)
  (γ : ⟦ Γ ⟧C $F δ)
  where

  module map
    {Ω : Con}
    (β : Vars Ω Ψ)
    {u : TMT A $F (δ ∘ ⌜ β ⌝V)}
    (v : ⟦ A ⟧T $F (δ ∘ ⌜ β ⌝V ,Σ u ,Σ ⟦ Γ ⟧C $F β $ γ))
    where

      open import NBE.LogPred.Ty.Pi.Exp ⟦ B ⟧T (δ ,Σ ⌜ n ⌝ne ,Σ γ)

      p : ≡NFT A $F (TMC Γ $P β $ δ ,Σ u)
      p = qT Aᴹ $S (TMC Γ $P β $ δ ,Σ u ,Σ ⟦ Γ ⟧C $F β $ γ ,Σ v)

      abstract
        lcoe1 : B [ δ ∘ ⌜ β ⌝V ^ A ]T [ < ⌜ proj₁ p ⌝nf > ]T
              ≡ B [ TMC Γ $P β $ δ ,s u ]T
        lcoe1 = [][]T ◾ ap (_[_]T B) (^∘<> refl ◾ ,s≃' refl (to≃ (proj₂ p ⁻¹)))

      abstract
        lcoe2 : _≡_ {A = (TMC Γ ,P TMT A ,P ⟦ Γ ⟧C [ wkn ]F) $P Ω}
                    (δ ∘ ⌜ β ⌝V ,Σ u ,Σ ⟦ Γ ⟧C $F β $ γ)
                    ( π₁ (TMC Γ $P β $ δ ,s u)
                    ,Σ π₂ (TMC Γ $P β $ δ ,s u)
                    ,Σ coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ γ))
        lcoe2 = Tbase= ⟦ Γ ⟧C (π₁β ⁻¹) (π₂β' ⁻¹̃) (uncoe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)))

      b : ⟦ B ⟧T $F ( (TMC Γ $P β $ δ ,s u)
                    ,Σ ⌜ coe (NeΓ= lcoe1) (appNe (coe (NeΓ= ([][]T ◾ Π[])) (n [ β ]ne)) (proj₁ p)) ⌝ne
                    ,Σ ((coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ γ)) ,Σ coe ($F= ⟦ A ⟧T lcoe2) v))
      b = uT Bᴹ $S ( (TMC Γ $P β $ δ ,s u)
                   ,Σ coe (NeΓ= lcoe1)
                         (appNe (coe (NeΓ= ([][]T ◾ Π[])) (n [ β ]ne)) (proj₁ p))
                   ,Σ ((coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ γ)) ,Σ coe ($F= ⟦ A ⟧T lcoe2) v))

      abstract
        lcoe3 : _≡_ {A = (TMC (Γ , A) ,P TMT B ,P ⟦ Γ , A ⟧C [ wkn ]F) $P Ω}
                    ( (TMC Γ $P β $ δ ,s u)
                    ,Σ ⌜ coe (NeΓ= lcoe1) (appNe (coe (NeΓ= ([][]T ◾ Π[])) (n [ β ]ne)) (proj₁ p)) ⌝ne
                    ,Σ (coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ γ) ,Σ coe ($F= ⟦ A ⟧T lcoe2) v))
                    ( (δ ∘ ⌜ β ⌝V ,s u)
                    ,Σ coe (TmΓ= pcoe1) (coe (TmΓ= pcoe2) (⌜ n ⌝ne [ ⌜ β ⌝V ]t) $$ u)
                    ,Σ (coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ γ) ,Σ coe ($F= ⟦ A ⟧T projTbase) v))
        lcoe3 = Tbase= ⟦ Γ , A ⟧C
                       refl
                       ( ⌜coe⌝ne lcoe1
                       ◾̃ $≃ (from≃ (⌜coe⌝ne ([][]T ◾ Π[]) ◾̃ to≃ (⌜[]ne⌝ {n = n} ⁻¹) ◾̃ uncoe (TmΓ= pcoe2))) (proj₂ p ⁻¹)
                       ◾̃ uncoe (TmΓ= pcoe1))
                       (,Σ≃≃ r̃
                           r̃
                           (uncoe ($F= ⟦ A ⟧T lcoe2) ⁻¹̃ ◾̃ uncoe ($F= ⟦ A ⟧T projTbase)))

      ret : ⟦ B ⟧T $F ( (δ ∘ ⌜ β ⌝V ,s u)
                      ,Σ coe (TmΓ= pcoe1)
                            (coe (TmΓ= pcoe2)
                                 (⌜ n ⌝ne [ ⌜ β ⌝V ]t) $$ u)
                      ,Σ ( coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹))
                              (⟦ Γ ⟧C $F β $ γ)
                        ,Σ coe ($F= ⟦ A ⟧T projTbase) v))
      ret = coe ($F= ⟦ B ⟧T lcoe3) b

  ret : ⟦ Π A B ⟧T $F (δ ,Σ ⌜ n ⌝ne ,Σ γ)
  ret = record
    { map = map.ret
    ; nat = cheat
    }
