{-# OPTIONS --no-eta --rewriting #-}

module NBE.Quote.Ty.Pi where

open import lib
open import JM
open import Cats
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Quote.Motives
open import NBE.Quote.Cxt
open import NBE.Quote.Ty.Pi.Quote
open import NBE.Quote.Ty.Pi.Unquote
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l
import NBE.LogPred.Ty.Pi

open Motives M
open MethodsCon mCon

open import NBE.Cheat

open import NBE.Glue

Πᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}
     {A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A)
     {B : Ty (Γ , A)}(Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B)
   → Tyᴹ Γᴹ (Π A B)
Πᴹ {Γ}{Γᴹ}{A} Aᴹ {B} Bᴹ = record
  { qT = record
      { _$S_ = λ { (δ ,Σ s' ,Σ γ ,Σ a) → qT$S.ret Aᴹ Bᴹ δ s' γ a }
      ; natS = cheat
      }
  ; uT = record
      { _$S_ = λ { (δ ,Σ n ,Σ γ) → uT$S.ret Aᴹ Bᴹ δ n γ }
      ; natS = cheat
      }
  }
