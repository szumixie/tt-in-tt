{-# OPTIONS --no-eta --rewriting #-}

module NBE.Quote.Ty.Base where

open import lib
open import JM
open import Cats
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Quote.Motives
open import NBE.Quote.Cxt
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l

open Motives M
open MethodsCon mCon

open import NBE.Cheat

open import NBE.Glue

module mUᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ}
  where

    module qT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (s : TMT U $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      (a : ⟦ U ⟧T $F (ρ ,Σ s ,Σ α))
      where

        abstract
          pcoe : Σ (NFT U $F id) (λ n → coe (TmΓ= ([id]T ⁻¹)) (coe (TmΓ= U[]) s) ≡ ⌜ n ⌝nf)
               ≡ Σ (NFT U $F ρ)  (λ n → s ≡ ⌜ n ⌝nf)
          pcoe = Σ≃ (NfΓ= ([id]T ◾ U[] ⁻¹))
                    (funext≃ (NfΓ= ([id]T ◾ U[] ⁻¹))
                             (λ x₂ → ≡≃ (TmΓ= ([id]T ◾ U[] ⁻¹))
                                        (uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= U[]) ⁻¹̃)
                                        (ap≃ {B = Nf _} ⌜_⌝nf ([id]T ◾ U[] ⁻¹) x₂)))

    module uT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (n : NET U $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      where

        abstract
          eq : coe (TmΓ= ([id]T ⁻¹)) (coe (TmΓ= U[]) ⌜ n ⌝ne)
             ≡  ⌜ coe (NfΓ= ([id]T ⁻¹)) (neuU (coe (NeΓ= U[]) n)) ⌝nf
          eq = from≃ ( uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                     ◾̃ uncoe (TmΓ= U[]) ⁻¹̃
                     ◾̃ ⌜coe⌝ne U[] ⁻¹̃
                     ◾̃ ⌜coe⌝nf ([id]T ⁻¹) ⁻¹̃)

    Uᴹ : Tyᴹ Γᴹ U
    Uᴹ = record
      { qT = record
          { _$S_ = λ { (ρ ,Σ s ,Σ α ,Σ a) → coe (qT$S.pcoe ρ s α a) a }
          ; natS = cheat
          }
      ; uT = record
          { _$S_ = λ { (ρ ,Σ n ,Σ α) → coe (NfΓ= ([id]T ⁻¹)) (neuU (coe (NeΓ= U[]) n)) ,Σ uT$S.eq ρ n α }
          ; natS = cheat
          }
      }

module mElᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ}
  {Â : Tm Γ U}(Âᴹ : Tmᴹ Γᴹ mUᴹ.Uᴹ Â)
  where

    module qT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (s : TMT (El Â) $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      (a : ⟦ El Â ⟧T $F (ρ ,Σ s ,Σ α))
      where

        abstract
          pcoe : Σ (NFT (El (coe (TmΓ= U[]) (Â [ ρ ]t))) $F id) (λ n → coe (TmΓ= ([id]T ⁻¹)) (coe (TmΓ= El[]) s) ≡ ⌜ n ⌝nf)
               ≡ Σ (NFT (El Â) $F ρ) (λ n → s ≡ ⌜ n ⌝nf)
          pcoe = Σ≃ (NfΓ= ([id]T ◾ El[] ⁻¹))
                    (funext≃ (NfΓ= ([id]T ◾ El[] ⁻¹))
                             (λ x₂ → ≡≃ (TmΓ= ([id]T ◾ El[] ⁻¹))
                                        (uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= El[]) ⁻¹̃)
                                        (ap≃ {B = Nf _} ⌜_⌝nf ([id]T ◾ El[] ⁻¹) x₂)))
                    
        ret : ≡NFT (El Â) $F (ρ ,Σ s)
        ret = coe pcoe a
      
    module uT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (n : NET (El Â) $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      where

        abstract
          eq : coe (TmΓ= ([id]T ⁻¹)) (coe (TmΓ= El[]) ⌜ n ⌝ne)
             ≡ ⌜ coe (NfΓ= ([id]T ⁻¹)) (neuEl (coe (NeΓ= El[]) n)) ⌝nf
          eq = from≃ ( uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                     ◾̃ uncoe (TmΓ= El[]) ⁻¹̃
                     ◾̃ ⌜coe⌝ne El[] ⁻¹̃
                     ◾̃ ⌜coe⌝nf ([id]T ⁻¹) ⁻¹̃)

    Elᴹ : Tyᴹ Γᴹ (El Â)
    Elᴹ = record
      { qT = record
          { _$S_ = λ { (ρ ,Σ s ,Σ α ,Σ a) → qT$S.ret ρ s α a }
          ; natS = cheat
          }
      ; uT = record
          { _$S_ = λ { (ρ ,Σ n ,Σ α) → (coe (NfΓ= ([id]T ⁻¹)) (neuEl (coe (NeΓ= El[]) n))) ,Σ uT$S.eq ρ n α }
          ; natS = cheat
          }
      }
