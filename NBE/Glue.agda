{-# OPTIONS --rewriting #-}

module NBE.Glue where

open import lib
open import JM
open import Cats
open import TT.Syntax
open import TT.Congr
open import NBE.TM
open import NBE.Nf
open import NBE.Renamings

open import NBE.Cheat

≡NFC : (Δ : Con) → FamPSh (TMC Δ)
≡NFC Δ = record
  { _$F_   = λ {Ψ} ρ → Σ (NFC Δ $P Ψ) λ τ → ρ ≡ ⌜ τ ⌝nfs
  ; _$F_$_ = λ { β (ρ ,Σ refl) → ρ [ β ]nfs ,Σ ⌜[]nfs⌝ {ρ = ρ}{β} }
  ; idF    = cheat
  ; compF  = cheat
  }

≡NFT : {Γ : Con}(A : Ty Γ) → FamPSh (TMC Γ ,P TMT A)
≡NFT A = record
  { _$F_   = λ { {Ψ} (ρ ,Σ t) → Σ (NFT A $F ρ) λ n → t ≡ ⌜ n ⌝nf }
  ; _$F_$_ = λ { {Ψ}{Ω} β {ρ ,Σ .(⌜ v ⌝nf)} (v ,Σ refl)
               → coe (NfΓ= [][]T) (v [ β ]nf) ,Σ from≃ (uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃  to≃ (⌜[]nf⌝ {v = v}{β = β}) ◾̃ ⌜coe⌝nf [][]T ⁻¹̃)
               }
  ; idF    = cheat
  ; compF  = cheat
  }
