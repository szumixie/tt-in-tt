{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.Comp where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim

open import NBE.Renamings.Var
open import NBE.Renamings.Vars
open import NBE.Renamings.Proj

-- renaming of variables

_[_]v : ∀{Γ Δ A}(x : Var Δ A)(β : Vars Γ Δ) → Var Γ (A [ ⌜ β ⌝V ]T)
vze [ β ]v   = coe (VarΓ= (ap (_[_]T _) (⌜π₁v⌝ β ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹)) (π₂v β)
vsu x [ β ]v = coe (VarΓ= (ap (_[_]T _) (⌜π₁v⌝ β ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹)) (x [ π₁v β ]v)

infixl 8 _[_]v

abstract
  ⌜[]v⌝ : ∀{Γ Δ A}{x : Var Δ A}{β : Vars Γ Δ} → ⌜ x ⌝v [ ⌜ β ⌝V ]t ≡ ⌜ x [ β ]v ⌝v
  ⌜[]v⌝ {x = vze}{β}
    = from≃ ( π₂id[]'
            ◾̃ ⌜π₂v⌝' β ⁻¹̃
            ◾̃ ⌜coe⌝v (ap (_[_]T _) (⌜π₁v⌝ β ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹) ⁻¹̃)
  ⌜[]v⌝ {x = vsu x}{β}
    = from≃ ( [][]t'
            ◾̃ []t≃ refl (to≃ (π₁id∘ ◾ ⌜π₁v⌝ β ⁻¹))
            ◾̃ to≃ (⌜[]v⌝ {x = x}{β = π₁v β})
            ◾̃ ⌜coe⌝v (ap (_[_]T _) (⌜π₁v⌝ β ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹) ⁻¹̃)

-- congruence

[]v≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
       {Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
       {x₀ : Var Δ₀ A₀}{x₁ : Var Δ₁ A₁}(x₂ : x₀ ≃ x₁)
       {β₀ : Vars Γ₀ Δ₀}{β₁ : Vars Γ₁ Δ₁}(β₂ : β₀ ≃ β₁)
     → x₀ [ β₀ ]v ≃ x₁ [ β₁ ]v
[]v≃ refl refl (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

coe[]v' : ∀{Γ Δ}{β : Vars Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){x : Var Δ A₀}
        → coe (VarΓ= A₂) x [ β ]v ≃ x [ β ]v
coe[]v' refl = refl ,≃ refl

-- composition

_∘V_ : ∀{Γ Δ Θ} → Vars Γ Δ → Vars Θ Γ → Vars Θ Δ
infix  6 _∘V_

⌜∘V⌝  : ∀{Γ Δ Θ}(β : Vars Γ Δ){γ : Vars Θ Γ} → ⌜ β ⌝V ∘ ⌜ γ ⌝V ≡ ⌜ β ∘V γ ⌝V

εV ∘V γ = εV
(β ,V x) ∘V γ = β ∘V γ ,V coe (VarΓ= ([][]T ◾ ap (_[_]T _) (⌜∘V⌝ β))) (x [ γ ]v)

abstract
  ⌜∘V⌝ εV = εη
  ⌜∘V⌝ (β ,V x){γ}
    = ,∘
    ◾ ,s≃' (⌜∘V⌝ β)
           (uncoe (TmΓ= [][]T) ⁻¹̃
           ◾̃ to≃ (⌜[]v⌝ {x = x}{γ})
           ◾̃ ⌜coe⌝v ([][]T ◾ ap (_[_]T _) (⌜∘V⌝ β)) ⁻¹̃)

-- congruence

∘V≃' : ∀{Γ Δ}{β : Vars Γ Δ}
       {Θ₀ Θ₁ : Con}(Θ₂ : Θ₀ ≡ Θ₁)
       {γ₀ : Vars Θ₀ Γ}{γ₁ : Vars Θ₁ Γ}(γ₂ : γ₀ ≃ γ₁)
     → (β ∘V γ₀) ≃ (β ∘V γ₁)
∘V≃' refl (refl ,≃ refl) = refl ,≃ refl

∘V≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {β₀ : Vars Γ₀ Δ₀}{β₁ : Vars Γ₁ Δ₁}(β₂ : β₀ ≃ β₁)
      {Θ₀ Θ₁ : Con}(Θ₂ : Θ₀ ≡ Θ₁)
      {γ₀ : Vars Θ₀ Γ₀}{γ₁ : Vars Θ₁ Γ₁}(γ₂ : γ₀ ≃ γ₁)
    → (β₀ ∘V γ₀) ≃ (β₁ ∘V γ₁)
∘V≃ refl refl (refl ,≃ refl) refl (refl ,≃ refl) = refl ,≃ refl

-- functor laws

abstract
  [⌜∘V⌝]T : ∀{Γ Δ Θ A}{β : Vars Δ Θ}{γ : Vars Γ Δ} → A [ ⌜ β ⌝V ]T [ ⌜ γ ⌝V ]T ≡ A [ ⌜ β ∘V γ ⌝V ]T
  [⌜∘V⌝]T {β = β} = [][]T ◾ ap (_[_]T _) (⌜∘V⌝ β)

abstract
  [⌜∘V⌝]t : ∀{Γ Δ Θ A}{β : Vars Δ Θ}{γ : Vars Γ Δ}{t : Tm Θ A} → t [ ⌜ β ⌝V ]t [ ⌜ γ ⌝V ]t ≃ t [ ⌜ β ∘V γ ⌝V ]t
  [⌜∘V⌝]t {β = β} = [][]t' ◾̃ []t≃ refl (to≃ (⌜∘V⌝ β))
