{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.Ass where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim

open import NBE.Renamings.Var
open import NBE.Renamings.Vars
open import NBE.Renamings.Proj
open import NBE.Renamings.Comp
open import NBE.Renamings.Weaken
open import NBE.Renamings.Id
open import NBE.Renamings.Idl
open import NBE.Renamings.VsuComp
open import NBE.Renamings.IdComp
open import NBE.Renamings.Idr
open import NBE.Renamings.CompComp

abstract
  assV : ∀{Γ Δ Θ Σ}{β : Vars Γ Δ}{γ : Vars Δ Θ}{ι : Vars Θ Σ}
       → (ι ∘V γ) ∘V β ≡ ι ∘V (γ ∘V β)
  assV {ι = εV} = refl
  assV {ι = ι ,V x} = ,V≃' assV
                          ( uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) (⌜∘V⌝ _))) ⁻¹̃
                          ◾̃ coe[]v' ([][]T ◾ ap (_[_]T _) (⌜∘V⌝ _))
                          ◾̃ [][]v {x = x}
                          ◾̃ uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) (⌜∘V⌝ _))))
