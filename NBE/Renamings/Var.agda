{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.Var where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr

-- variables

data Var  : (Γ : Con)(A : Ty Γ) → Set
⌜_⌝v : ∀{Γ A} → Var Γ A → Tm Γ A

data Var where
  vze : ∀ {Γ}{A : Ty Γ} → Var (Γ , A) (A [ wk ]T)
  vsu : ∀ {Γ}{A B : Ty Γ} → Var Γ A → Var (Γ , B) (A [ wk ]T)

⌜ vze ⌝v = vz
⌜ vsu v ⌝v = vs ⌜ v ⌝v

infix 4 ⌜_⌝v

-- the eliminator

record MVar {i} : Set (lsuc i) where
  field
    Varᴹ : ∀ Γ A → Var Γ A → Set i
    vzeᴹ : ∀{Γ A} → Varᴹ (Γ , A) (A [ wk ]T) vze
    vsuᴹ : ∀{Γ}{A B : Ty Γ}{x : Var Γ A}(xᴹ : Varᴹ Γ A x) → Varᴹ (Γ , B) (A [ wk ]T) (vsu x)

module _ {i}(M : MVar {i}) where
  open MVar M
  
  Var-elim : ∀{Γ A}(x : Var Γ A) → Varᴹ Γ A x
  Var-elim vze = vzeᴹ
  Var-elim (vsu x) = vsuᴹ (Var-elim x)

-- congruence rules

Var= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
     → Var Γ₀ A₀ ≡ Var Γ₁ A₁
Var= refl refl = refl

VarΓ= : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁) → Var Γ A₀ ≡ Var Γ A₁
VarΓ= {Γ} = ap (Var Γ)

⌜coe⌝v : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁){x : Var Γ A₀}
       → ⌜ coe (VarΓ= A₂) x ⌝v ≃ ⌜ x ⌝v
⌜coe⌝v refl = refl ,≃ refl

vsu≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
       {x₀ : Var Γ₀ A₀}{x₁ : Var Γ₁ A₁}(x₂ : x₀ ≃ x₁)
       {B₀ : Ty Γ₀}{B₁ : Ty Γ₁}(B₂ : B₀ ≃ B₁)
     → vsu {B = B₀} x₀ ≃ vsu {B = B₁} x₁
vsu≃ refl (refl ,≃ refl)(refl ,≃ refl)(refl ,≃ refl) = refl ,≃ refl

vsu≃' : ∀{Γ A₀ A₁ B}(A₂ : A₀ ≡ A₁)
        {x₀ : Var Γ A₀}{x₁ : Var Γ A₁}
      → x₀ ≃ x₁ → vsu {B = B} x₀ ≃ vsu {B = B} x₁
vsu≃' refl (refl ,≃ refl) = refl ,≃ refl

vze≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
     → vze {Γ₀}{A₀} ≃ vze {Γ₁}{A₁}
vze≃ refl (refl ,≃ refl) = refl ,≃ refl

vze= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
       (ΓA₂ : Γ₀ , A₀ ≡ Γ₁ , A₁)
       (A[wk]₂ : A₀ [ wk ]T ≡[ Ty= ΓA₂ ]≡ A₁ [ wk ]T)
     → vze {Γ₀}{A₀} ≡[ Var= ΓA₂ A[wk]₂ ]≡ vze {Γ₁}{A₁}
vze= refl refl refl refl = refl

⌜⌝v≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
       {x₀ : Var Γ₀ A₀}{x₁ : Var Γ₁ A₁}(x₂ : x₀ ≃ x₁)
     → ⌜ x₀ ⌝v ≃ ⌜ x₁ ⌝v
⌜⌝v≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl
