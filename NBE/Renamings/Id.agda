{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.Id where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim

open import NBE.Renamings.Var
open import NBE.Renamings.Vars
open import NBE.Renamings.Proj
open import NBE.Renamings.Weaken

-- the identity renaming

abstract
  pidV, : {Γ : Con}(idVΓ : Vars Γ Γ){A : Ty Γ} → ⌜ idVΓ ⌝V ≡ id
        → ⌜ wkV {A = A} idVΓ ⌝V ≡ wk
  pidV, {Γ} idVΓ {A} pΓ
    = ⌜wkV⌝ idVΓ ⁻¹ ◾ ∘= refl refl refl {id}{⌜ idVΓ ⌝V} (pΓ ⁻¹) refl ⁻¹ ◾ idl

abstract
  pidV,' : {Γ : Con}(idVΓ : Vars Γ Γ){A : Ty Γ}(pΓ : ⌜ idVΓ ⌝V ≡ id)
         → ⌜ wkV idVΓ ,V coe (VarΓ= (ap (_[_]T A) (pidV, idVΓ pΓ ⁻¹))) vze ⌝V ≡ id
  pidV,' {Γ} idVΓ {A} pΓ
    = ,s≃' ( ⌜wkV⌝ idVΓ ⁻¹
           ◾ ∘= refl refl refl {id}{⌜ idVΓ ⌝V} (pΓ ⁻¹) refl ⁻¹
           ◾ idl)
           (⌜coe⌝v (ap (_[_]T A) (pidV, idVΓ pΓ ⁻¹)))
    ◾ πη

idV' : (Γ : Con) → Σ (Vars Γ Γ) λ idVΓ → ⌜ idVΓ ⌝V ≡ id
idV'
  = ElimCon'
      (λ Γ → Σ (Vars Γ Γ) λ idVΓ → ⌜ idVΓ ⌝V ≡ id)
      (εV ,Σ (εη ⁻¹))
      (λ { {Γ}{A}(idVΓ ,Σ pΓ)
       →   wkV idVΓ ,V coe (VarΓ= (ap (_[_]T _) (pidV, idVΓ pΓ ⁻¹))) vze
       ,Σ pidV,' idVΓ pΓ })

idV : ∀{Γ} → Vars Γ Γ
idV {Γ} = proj₁ (idV' Γ)

abstract
  ⌜idV⌝ : ∀{Γ} → ⌜ idV {Γ} ⌝V ≡ id
  ⌜idV⌝ {Γ} = proj₂ (idV' Γ)

-- a law

abstract
  ⌜wkid⌝ : ∀{Γ A} → wk {Γ}{A} ≡ ⌜ wkV idV ⌝V
  ⌜wkid⌝ = idl ⁻¹ ◾ ap (λ z → z ∘ wk) (⌜idV⌝ ⁻¹) ◾ ⌜wkV⌝ idV

-- functor laws

abstract
  [⌜idV⌝]T : ∀{Γ}{A : Ty Γ} → A [ ⌜ idV ⌝V ]T ≡ A
  [⌜idV⌝]T = ap (_[_]T _) ⌜idV⌝ ◾ [id]T

abstract
  [⌜idV⌝]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ ⌜ idV ⌝V ]t ≃ t
  [⌜idV⌝]t = []t≃ refl (to≃ ⌜idV⌝) ◾̃ [id]t'
