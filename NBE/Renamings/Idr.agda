{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.Idr where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim

open import NBE.Renamings.Var
open import NBE.Renamings.Vars
open import NBE.Renamings.Proj
open import NBE.Renamings.Comp
open import NBE.Renamings.Weaken
open import NBE.Renamings.Id
open import NBE.Renamings.IdComp

abstract
  idrV : ∀{Γ Δ}{β : Vars Γ Δ} →  β ∘V idV ≡ β
  idrV {β = εV} = refl
  idrV {β = β ,V x} = ,V≃' idrV
                          ( uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) (⌜∘V⌝ _))) ⁻¹̃
                          ◾̃ [id]v ⁻¹̃)
