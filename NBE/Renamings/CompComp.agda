{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.CompComp where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim

open import NBE.Renamings.Var
open import NBE.Renamings.Vars
open import NBE.Renamings.Proj
open import NBE.Renamings.Comp
open import NBE.Renamings.Weaken
open import NBE.Renamings.Id
open import NBE.Renamings.Idl
open import NBE.Renamings.VsuComp
open import NBE.Renamings.IdComp
open import NBE.Renamings.Idr

abstract
  p[][]v : ∀{Γ Δ Θ A}(β : Vars Γ Δ)(γ : Vars Δ (Θ , A))
         → π₂v γ [ β ]v ≃ π₂v (γ ∘V β)
  p[][]v {Γ}{Δ}{Θ}{A} β γ
    =  Vars-elim
        (record
         { Varsᴹ = λ Δ' ΘA' γ' → (pΔ : Δ' ≡ Δ)(pΘA : ΘA' ≡ (Θ , A)) → γ' ≡[ Vars= pΔ pΘA ]≡ γ
                   → π₂v γ [ β ]v ≃ π₂v (γ ∘V β)
         ; εVᴹ   = λ _ p _ → ⊥-elim (disj•, p)
         ; _,Vᴹ_ = λ {Δ'}{Θ'}{γ'} _ {A'} x pΔ pΘA pγ
                   → []v≃ refl (pΔ ⁻¹)
                          ([]T≃' (pΔ ⁻¹)
                                 (inj,₀ pΘA ⁻¹)
                                 (from≡ (Ty= (inj,₀ pΘA)) (inj,₁ pΘA) ⁻¹̃)
                                 (⌜⌝V≃ (pΔ ⁻¹)
                                       (inj,₀ pΘA ⁻¹)
                                       {π₁v γ}{π₁v (γ' ,V x)}
                                       (π₁v≃ (pΔ ⁻¹)
                                             (inj,₀ pΘA ⁻¹)
                                             (from≡ (Ty= (inj,₀ pΘA)) (inj,₁ pΘA) ⁻¹̃)
                                             {γ}{γ' ,V x} (from≡ (Vars= pΔ pΘA) pγ ⁻¹̃))))
                          (π₂v≃ (pΔ ⁻¹)
                                (inj,₀ pΘA ⁻¹)
                                (from≡ (Ty= (inj,₀ pΘA)) (inj,₁ pΘA) ⁻¹̃)

                                {γ}{γ' ,V x}(from≡ (Vars= pΔ pΘA) pγ ⁻¹̃))
                          {β}{coe (Vars= refl (pΔ ⁻¹)) β}
                          (uncoe (Vars= refl (pΔ ⁻¹)))
                   ◾̃ []v≃ {Γ}refl {Δ'}refl
                          ([]T≃ refl
                                (⌜⌝V≃ refl refl {π₁v (γ' ,V x)}{γ'}(to≃ (π₁vβ {β = γ'}{x}))))
                          π₂vβ'
                          {coe (Vars= refl (pΔ ⁻¹)) β} r̃
                   ◾̃ uncoe (VarΓ= ([][]T ◾ ap (_[_]T A') (⌜∘V⌝ γ')))
                   ◾̃ π₂vβ' ⁻¹̃
                   ◾̃ π₂v≃ refl (inj,₀ pΘA) (from≡ (Ty= (inj,₀ pΘA)) (inj,₁ pΘA))
                          (∘V≃ pΔ pΘA {γ' ,V x}{γ} (from≡ (Vars= pΔ pΘA) pγ) refl {coe (Vars= refl (pΔ ⁻¹)) β}{β} (uncoe (Vars= refl (pΔ ⁻¹)) ⁻¹̃))
         }) γ refl refl refl

abstract
  p[][]v' : ∀{Γ Δ Θ A B}(x : Var Θ A)(β : Vars Γ Δ)(γ : Vars Δ (Θ , B))
          → (∀{Γ' Δ'}{β' : Vars Γ' Δ'}{γ' : Vars Δ' Θ} → x [ γ' ]v [ β' ]v ≃ x [ γ' ∘V β' ]v)
          → x [ π₁v γ ]v [ β ]v ≃ x [ π₁v (γ ∘V β) ]v
  p[][]v' {Γ}{Δ}{Θ}{A}{B} x β γ rec
    = Vars-elim
        (record
         { Varsᴹ = λ Δ' ΘB' γ' → (pΔ : Δ' ≡ Δ)(pΘB : ΘB' ≡ (Θ , B)) → γ' ≡[ Vars= pΔ pΘB ]≡ γ
                   → x [ π₁v γ ]v [ β ]v ≃ x [ π₁v (γ ∘V β) ]v
         ; εVᴹ   = λ _ p _ → ⊥-elim (disj•, p)
         ; _,Vᴹ_ = λ {Δ'}{Θ'}{γ'} _ {B'} x' pΔ pΘB pγ
                   → rec {β' = β}{π₁v γ}
                   ◾̃ []v≃ refl refl r̃ {x} r̃ {π₁v γ ∘V β}{π₁v (γ ∘V β)}
                          ( ∘V≃ (pΔ ⁻¹) (inj,₀ pΘB ⁻¹) {π₁v γ}{π₁v (γ' ,V x')}
                                (π₁v≃ (pΔ ⁻¹) (inj,₀ pΘB ⁻¹) (from≡ (Ty= (inj,₀ pΘB)) (inj,₁ pΘB) ⁻¹̃) {γ}{γ' ,V x'} (from≡ (Vars= pΔ pΘB) pγ ⁻¹̃))
                                refl
                                {β}{coe (Vars= refl (pΔ ⁻¹)) β} (uncoe (Vars= refl (pΔ ⁻¹)))
                          ◾̃ ∘V≃ refl refl {π₁v (γ' ,V x')}{γ'} (to≃ (π₁vβ {β = γ'}{x'})) refl r̃
                          ◾̃ to≃ (π₁vβ {β = γ' ∘V coe (Vars= refl (pΔ ⁻¹)) β}{coe (VarΓ= ([][]T ◾ ap (_[_]T B') (⌜∘V⌝ γ'))) (x' [ coe (Vars= refl (pΔ ⁻¹)) β ]v)} ⁻¹)
                          ◾̃ π₁v≃ refl (inj,₀ pΘB) (from≡ (Ty= (inj,₀ pΘB)) (inj,₁ pΘB))
                                 (∘V≃ pΔ pΘB {γ' ,V x'}{γ} (from≡ (Vars= pΔ pΘB) pγ) refl {coe (Vars= refl (pΔ ⁻¹)) β}{β} (uncoe (Vars= refl (pΔ ⁻¹)) ⁻¹̃)))
         }) γ refl refl refl

abstract
  [][]v : ∀{Γ Δ Θ A}{x : Var Θ A}{β : Vars Γ Δ}{γ : Vars Δ Θ}
        → x [ γ ]v [ β ]v ≃ x [ γ ∘V β ]v
        
  [][]v {x = vze {Θ}{A}}{β}{γ}
    = coe[]v' (ap (_[_]T _) (⌜π₁v⌝ γ ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹)
    ◾̃ (p[][]v β γ)
    ◾̃ uncoe (VarΓ= (ap (_[_]T _) (⌜π₁v⌝ (γ ∘V β) ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹))
    
  [][]v {x = vsu x}{β}{γ}
    = coe[]v' (ap (_[_]T _) (⌜π₁v⌝ γ ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹)
    ◾̃ p[][]v' x β γ ([][]v {x = x}) -- note how we pass the recursive arguments
    ◾̃ uncoe (VarΓ= (ap (_[_]T _) (⌜π₁v⌝ (γ ∘V β) ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹))
