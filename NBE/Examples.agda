module NBE.Examples where

open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import NBE.Renamings
open import NBE.Nf

v : Nf (• , U , Π U U) U
v = neuU (coe (NeΓ= U[])
              (appNe (coe (NeΓ= (Π[] ◾ Π≃' U[] (to≃ U[] ◾̃ U≃ (,C≃ refl (to≃ U[])))))
                          (var vze))
                     (neuU (coe (NeΓ= ([][]T ◾ U[]))
                                (var (vsu vze))))))
