{-# OPTIONS --no-eta #-}

module NBE.Nf.Rename where

open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.Nf.Nf

-- renaming neutral terms, normal forms and their relation to terms

_[_]ne : ∀{Γ Δ A}(n : Ne Δ A)(β : Vars Γ Δ) → Ne Γ (A [ ⌜ β ⌝V ]T)
_[_]nf : ∀{Γ Δ A}(v : Nf Δ A)(β : Vars Γ Δ) → Nf Γ (A [ ⌜ β ⌝V ]T)

infixl 8 _[_]ne _[_]nf

⌜[]ne⌝ : ∀{Γ Δ A}{n : Ne Δ A}{β : Vars Γ Δ}
       → ⌜ n ⌝ne [ ⌜ β ⌝V ]t ≡ ⌜ n [ β ]ne ⌝ne

⌜[]nf⌝ : ∀{Γ Δ A}{v : Nf Δ A}{β : Vars Γ Δ}
       → ⌜ v ⌝nf [ ⌜ β ⌝V ]t ≡ ⌜ v [ β ]nf ⌝nf

var x [ β ]ne = var (x [ β ]v)
appNe {A = A} n v [ β ]ne = coe (NeΓ= ([][]T ◾ ap (_[_]T _) ^∘<⌜⌝> ◾ [][]T ⁻¹))
                                (appNe (coe (NeΓ= Π[]) (n [ β ]ne))
                                     (v [ β ]nf))
  where
    abstract
      ^∘<⌜⌝> : (⌜ β ⌝V ^ A) ∘ < ⌜ v [ β ]nf ⌝nf > ≡ < ⌜ v ⌝nf > ∘ ⌜ β ⌝V
      ^∘<⌜⌝> = ,∘
             ◾ ,s≃' (ass ◾ ap (_∘_ ⌜ β ⌝V) π₁idβ ◾ idr ◾ idl ⁻¹)
                    ( uncoe (TmΓ= [][]T) ⁻¹̃
                    ◾̃ coe[]t' [][]T
                    ◾̃ π₂idβ
                    ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                    ◾̃ to≃ (⌜[]nf⌝ {v = v}{β = β} ⁻¹)
                    ◾̃ coe[]t' ([id]T ⁻¹) ⁻¹̃
                    ◾̃ uncoe (TmΓ= [][]T))
             ◾ ,∘ ⁻¹
    
neuU n [ β ]nf = coe (NfΓ= (U[] ⁻¹)) (neuU (coe (NeΓ= U[]) (n [ β ]ne)))
neuEl n [ β ]nf = coe (NfΓ= (El[] ⁻¹)) (neuEl (coe (NeΓ= El[]) (n [ β ]ne))) 
lamNf v [ β ]nf = coe (NfΓ= (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹))
                      (lamNf (v [ β ^V _ ]nf))


abstract
  ⌜[]ne⌝ {n = var x} = ⌜[]v⌝ {x = x}
  ⌜[]ne⌝ {n = appNe n v} = from≃ ( $[]
                               ◾̃ $≃ (from≃ ( uncoe (TmΓ= Π[]) ⁻¹̃
                                           ◾̃ to≃ (⌜[]ne⌝ {n = n})
                                           ◾̃ ⌜coe⌝ne Π[] ⁻¹̃))
                                    (⌜[]nf⌝ {v = v})
                               ◾̃ ⌜coe⌝ne ([][]T ◾ ap (_[_]T _) _ ◾ [][]T ⁻¹) ⁻¹̃)
abstract
  ⌜[]nf⌝ {v = neuU n}  = ⌜[]ne⌝ {n = n}
                       ◾ from≃ (⌜coe⌝ne U[] ⁻¹̃ ◾̃ ⌜coe⌝nf (U[] ⁻¹) ⁻¹̃)
  ⌜[]nf⌝ {v = neuEl n} = ⌜[]ne⌝ {n = n}
                       ◾ from≃ (⌜coe⌝ne El[] ⁻¹̃ ◾̃ ⌜coe⌝nf (El[] ⁻¹) ⁻¹̃)
  ⌜[]nf⌝ {v = lamNf v}

    = from≃ ( from≡ (TmΓ= Π[]) lam[]
            ◾̃ lam≃ refl
                   r̃
                   (to≃ (ap (_[_]T _) (⌜^⌝ ⁻¹)))
                   ( []t≃ refl (to≃ (⌜^⌝ ⁻¹))
                   ◾̃ to≃ (⌜[]nf⌝ {v = v}))
            ◾̃ ⌜coe⌝nf (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹) ⁻¹̃)

-- congruence and conversion rules

coe[]ne' : ∀{Γ Δ}{β : Vars Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){n : Ne Δ A₀}
        → coe (NeΓ= A₂) n [ β ]ne ≃ n [ β ]ne
coe[]ne' refl = refl ,≃ refl

[]nf≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ A}{v : Nf Δ A}
        {β₀ : Vars Γ₀ Δ}{β₁ : Vars Γ₁ Δ}(β₂ : β₀ ≃ β₁)
      → v [ β₀ ]nf ≃ v [ β₁ ]nf
[]nf≃ refl (refl ,≃ refl) = refl ,≃ refl

coe[]nf' : ∀{Γ Δ}{β : Vars Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){v : Nf Δ A₀}
        → coe (NfΓ= A₂) v [ β ]nf ≃ v [ β ]nf
coe[]nf' refl = refl ,≃ refl
