module NBE.Cheat where

postulate
  cheat : ∀{ℓ}{A : Set ℓ} → A
