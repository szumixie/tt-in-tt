{-# OPTIONS --without-K #-}

module ReflGraph.Layered.Decl where

open import lib

open import Graph.Decl

Conᴿ : Con → Set
Conᴿ Γ = (γ : ⌜ Γ ⌝C) → (Γ ⁼C) γ γ

Tyᴿ : {Γ : Con} → Conᴿ Γ → Ty Γ → Set
Tyᴿ {Γ} Γᴿ A = {γ : ⌜ Γ ⌝C}(α : ⌜ A ⌝T γ) → (A ⁼T) (Γᴿ γ) α α

Tmsᴿ : ∀{Γ Δ} → Conᴿ Γ → Conᴿ Δ → Tms Γ Δ → Set
Tmsᴿ {Γ}{Δ} Γᴿ Δᴿ σ = (γ : ⌜ Γ ⌝C) → Δᴿ (⌜ σ ⌝s γ) ≡ (σ ⁼s) (Γᴿ γ)

Tmᴿ  : ∀{Γ}(Γᴿ : Conᴿ Γ) → {A : Ty Γ} → Tyᴿ Γᴿ A → Tm Γ A → Set
Tmᴿ {Γ} Γᴿ {A} Aᴿ t = (γ : ⌜ Γ ⌝C) → Aᴿ (⌜ t ⌝t γ) ≡ (t ⁼t) (Γᴿ γ)

open import TT.Decl.DModel

dᴿ : Declᴹ d
dᴿ = record { Conᴹ = Conᴿ ; Tyᴹ = Tyᴿ ; Tmsᴹ = Tmsᴿ ; Tmᴹ = Tmᴿ }
