{-# OPTIONS --without-K #-}

module ReflGraph.Func where

open import lib

open import ReflGraph.Decl
open import ReflGraph.Core

open import TT.Func

Π : {Γ : Con} (A : Ty Γ) → Ty (Γ , A) → Ty Γ
Π {Γ} A B = record
  { ⌜_⌝T = λ γ → Σ ((α : ⌜ A ⌝T γ) → ⌜ B ⌝T (γ ,Σ α)) λ ⌜f⌝ → Σ
                   ({α₀ α₁ : ⌜ A ⌝T γ}(α⁼ : (A ⁼T) (RC Γ γ) α₀ α₁) → (B ⁼T) (RC Γ γ ,Σ α⁼) (⌜f⌝ α₀) (⌜f⌝ α₁)) λ f⁼ →
                   (α : ⌜ A ⌝T γ) → RT B (⌜f⌝ α) ≡ f⁼ (RT A α)
  ; _⁼T = λ { {γ₀}{γ₁} γ⁼ (⌜f₀⌝ ,Σ (f₀⁼ ,Σ f₀R)) (⌜f₁⌝ ,Σ (f₁⁼ ,Σ f₁R)) →
      {α₀ : ⌜ A ⌝T γ₀}{α₁ : ⌜ A ⌝T γ₁}(α⁼ : (A ⁼T) γ⁼ α₀ α₁) → (B ⁼T) (γ⁼ ,Σ α⁼) (⌜f₀⌝ α₀) (⌜f₁⌝ α₁) }
  ; RT = λ { (⌜f⌝ ,Σ (f⁼ ,Σ fR)) → f⁼ }
  }



f : Func c
f = record
  { Π = Π
  ; Π[] = λ {Γ}{Δ}{σ}{A}{B} → {!!}
  ; lam = λ {Γ}{A}{B} t → record {
      ⌜_⌝t = λ γ → (λ α → ⌜ t ⌝t (γ ,Σ α)) ,Σ ((λ α~ → (t ⁼t) (RC Γ γ ,Σ α~)) ,Σ λ α → Rt t (γ ,Σ α))
      ; _⁼t = λ γ~ α~ → (t ⁼t) (γ~ ,Σ α~)
      ; Rt = λ γ → refl }
  ; app = λ {Γ}{A}{B} t → record {
      ⌜_⌝t = λ { (γ ,Σ α) → proj₁ (⌜ t ⌝t γ) α } ;
      _⁼t = λ { (γ~ ,Σ α~) → (t ⁼t) γ~ α~ } ;
      Rt = λ { (γ ,Σ α) → proj₂ (proj₂ (⌜ t ⌝t γ)) α ◾ ap (λ z → z (RT A α)) (Rt t γ) } }
  ; lam[] = λ {Γ}{Δ}{σ}{A}{B}{t} → Tm= {!!} {!!} {!!}
  ; Πβ = λ {Γ}{A}{B}{t} → Tm= refl refl (funext λ { (γ ,Σ α) → ◾rid _ })
  ; Πη = λ {Γ}{A}{B}{t} → Tm=
      (funext λ γ → ,Σ= refl (,Σ= (Rt t γ ⁻¹)
         (J (λ z → transport (λ f⁼ → (α : ⌜ A ⌝T γ) → RT B (proj₁ (⌜ t ⌝t γ) α) ≡ f⁼ (RT A α)) (z ⁻¹) (λ α → proj₂ (proj₂ (⌜ t ⌝t γ)) α ◾ ap (λ z → z (RT A α)) z) ≡ proj₂ (proj₂ (⌜ t ⌝t γ))) (funext λ α → ◾rid _) (Rt t γ))))
      (funexti λ γ → funexti λ γ' → funext λ γ~ → {!!})
      {!!}
  }
