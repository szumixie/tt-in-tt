{-# OPTIONS --without-K --enable-prop --rewriting #-}

module ReflGraph.SProp.Decl where

open import lib

-- open import Setoid.SProp.lib

data _~_ {ℓ}{A : Set ℓ}(a : A) : A → Prop ℓ where
  refl~ : a ~ a

infix 4 _~_

ap~ : ∀{ℓ ℓ'}{A : Set ℓ}{B : Set ℓ'}(f : A → B){a₀ a₁ : A}(a₂ : a₀ ~ a₁)
    → f a₀ ~ f a₁
ap~ f refl~ = refl~

postulate
  transport~ : ∀{ℓ ℓ'}{A : Set ℓ}(P : A → Set ℓ'){x y : A}(p : x ~ y) → P x → P y
  transport~β : ∀{ℓ ℓ'}{A : Set ℓ}{P : A → Set ℓ'}{x : A}{u : P x} → transport~ P refl~ u ≡ u

{-# REWRITE transport~β #-}

infixl 4 _◾~_
infix 5 _⁻¹~

_⁻¹~ : ∀{ℓ}{A : Set ℓ}{a b : A} → a ~ b → b ~ a
_⁻¹~ refl~ = refl~

_◾~_ : ∀{ℓ}{A : Set ℓ}{a b c : A} → a ~ b → b ~ c → a ~ c
_◾~_ refl~ a~ = a~

-- 

record Con : Set₁ where
  field
    ⌜_⌝C : Set
    _⁼C  : ⌜_⌝C → ⌜_⌝C → Set
    RC   : (γ : ⌜_⌝C) → _⁼C γ γ
  infix 7 ⌜_⌝C
open Con public

record Ty (Γ : Con) : Set₁ where
  field
    ⌜_⌝T : ⌜ Γ ⌝C → Set
    _⁼T  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀)(α₁ : ⌜_⌝T γ₁) → Set
    RT   : {γ : ⌜ Γ ⌝C}(α : ⌜_⌝T γ) → _⁼T (RC Γ γ) α α
  infix 7 ⌜_⌝T
open Ty public

record Tms (Γ Δ : Con) : Set where
  field
    ⌜_⌝s : ⌜ Γ ⌝C → ⌜ Δ ⌝C
    _⁼s  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s γ₀) (⌜_⌝s γ₁)
    Rs   : (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s γ) ~ _⁼s (RC Γ γ)
  infix 7 ⌜_⌝s
open Tms public

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    ⌜_⌝t : (γ : ⌜ Γ ⌝C) → ⌜ A ⌝T γ
    _⁼t  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (A ⁼T) γ₌ (⌜_⌝t γ₀) (⌜_⌝t γ₁)
    Rt   : (γ : ⌜ Γ ⌝C) → RT A (⌜_⌝t γ) ~ _⁼t (RC Γ γ)
  infix 7 ⌜_⌝t
open Tm public
