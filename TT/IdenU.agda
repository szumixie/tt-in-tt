{-# OPTIONS --no-eta #-}

module TT.IdenU where

open import Agda.Primitive
open import lib
open import JM

open import TT.Decl
open import TT.Core
open import TT.Base
import TT.Decl.Congr
import TT.Core.Congr
import TT.Core.Laws
import TT.Base.Congr

-- Identity type

module _ {i j}{d : Decl}{c : Core {i}{j} d}(b : Base c) where
  open Decl d
  open Core c
  open Base b
  open TT.Decl.Congr d
  open TT.Core.Congr c
  open TT.Core.Laws c
  open TT.Base.Congr b

  record IdenU1 : Set (i ⊔ j) where
    field
      Id : {Γ : Con}{a : Tm Γ U} → Tm Γ (El a) → Tm Γ (El a) → Tm Γ U

      Id[] : ∀{Γ Θ a}{u v : Tm Γ (El a)}{σ : Tms Θ Γ}
           → Id u v [ σ ]t ≡[ TmΓ= U[] ]≡ Id (coe (TmΓ= El[]) (u [ σ ]t)) (coe (TmΓ= El[]) (v [ σ ]t))

      ref : ∀{Γ a}(u : Tm Γ (El a)) → Tm Γ (El (Id u u))

      ref[] : ∀{Γ Θ a}{u : Tm Γ (El a)}{σ : Tms Θ Γ}
            → ref u [ σ ]t ≡[ TmΓ= (El[] ◾ El= refl Id[]) ]≡ ref (coe (TmΓ= El[]) (u [ σ ]t))

  module _ (i1 : IdenU1) where
    open IdenU1 i1

    IdU=
      : {Γ : Con}{a₀ a₁ : Tm Γ U}(a₂ : a₀ ≡ a₁)
        {t₀ : Tm Γ (El a₀)}{t₁ : Tm Γ (El a₁)}(t₂ : t₀ ≡[ TmΓ= (El= refl a₂) ]≡ t₁)
        {u₀ : Tm Γ (El a₀)}{u₁ : Tm Γ (El a₁)}(u₂ : u₀ ≡[ TmΓ= (El= refl a₂) ]≡ u₁)
      → Id t₀ u₀ ≡ Id t₁ u₁
    IdU= refl refl refl = refl

    abstract
      lineUT
        : {Γ : Con}{a : Tm Γ U}{t u : Tm Γ (El a)}(p : Tm Γ (El (Id t u)))
        → El (Id t u) ≡ El (Id (coe (TmΓ= El[]) (t [ wk ]t)) (coe (TmΓ= El[]) vz)) [ < u > ]T
      lineUT {Γ}{a}{t}{u} p
        = El= refl ( IdU= =a =t =u
                   ◾ ap (λ z → coe (TmΓ= z) (Id (coe (TmΓ= El[]) (coe (TmΓ= El[]) (t [ wk ]t) [ < u > ]t)) (coe (TmΓ= El[]) (coe (TmΓ= El[]) vz [ < u > ]t)))) (≡inv' U[] ⁻¹)
                   ◾ coecoeTmΓ (U[] ⁻¹) U[] ⁻¹
                   ◾ ap (coe (TmΓ= U[])) (coe2rTmΓ U[] Id[] ⁻¹))
        ◾ El[] ⁻¹
        where
          =a : a ≡ coe (TmΓ= U[]) (coe (TmΓ= U[]) (a [ wk ]t) [ < u > ]t)
          =a = coe= {p₀ = refl}{(TmΓ= ([id]T ⁻¹ ◾ (A[]T= ((π₁id∘ ◾ π₁β) ⁻¹) ◾ ([][]T ⁻¹ ◾ ([σ]T= U[] ◾ U[])))))}(UIP _ _)
             ◾ coecoeTmΓ ([id]T ⁻¹)(A[]T= ((π₁id∘ ◾ π₁β) ⁻¹) ◾ ([][]T ⁻¹ ◾ ([σ]T= U[] ◾ U[]))) ⁻¹
             ◾ ap (coe (TmΓ= (A[]T= ((π₁id∘ ◾ π₁β) ⁻¹) ◾ ([][]T ⁻¹ ◾ ([σ]T= U[] ◾ U[])))))  (coe2rTmΓ [id]T [id]t ⁻¹)
             ◾ coecoeTmΓ (A[]T= ((π₁id∘ ◾ π₁β) ⁻¹))([][]T ⁻¹ ◾ ([σ]T= U[] ◾ U[])) ⁻¹
             ◾ ap (coe (TmΓ= ([][]T ⁻¹ ◾ ([σ]T= U[] ◾ U[])))) ([]t=' refl refl refl refl (wk∘<> ⁻¹))
             ◾ coecoeTmΓ ([][]T ⁻¹)([σ]T= U[] ◾ U[]) ⁻¹
             ◾ ap (coe (TmΓ= ([σ]T= U[] ◾ U[])))(coe2rTmΓ [][]T [][]t ⁻¹)
             ◾ coecoeTmΓ ([σ]T= U[]) U[] ⁻¹
             ◾ ap (coe (TmΓ= U[])) (coe[]t'' U[]) ⁻¹
          =t : t ≡[ TmΓ= (El= refl =a) ]≡ coe (TmΓ= El[]) (coe (TmΓ= El[]) (t [ wk ]t) [ < u > ]t)
          =t = coe= {p₀ = TmΓ= (El= refl =a)}{TmΓ= ([id]T ⁻¹ ◾ ([]T= refl refl refl ((π₁id∘ ◾ π₁β) ⁻¹) ◾ ([][]T ⁻¹ ◾ ([σ]T= El[] ◾ El[]))))}(UIP _ _)
             ◾ coecoeTmΓ ([id]T ⁻¹)([]T= refl refl refl ((π₁id∘ ◾ π₁β) ⁻¹) ◾ ([][]T ⁻¹ ◾ ([σ]T= El[] ◾ El[]))) ⁻¹
             ◾ ap (coe (TmΓ= ([]T= refl refl refl ((π₁id∘ ◾ π₁β) ⁻¹) ◾ ([][]T ⁻¹ ◾ ([σ]T= El[] ◾ El[]))))) (coe2rTmΓ [id]T [id]t ⁻¹)
             ◾ coecoeTmΓ ([]T= refl refl refl ((π₁id∘ ◾ π₁β) ⁻¹))([][]T ⁻¹ ◾ ([σ]T= El[] ◾ El[])) ⁻¹
             ◾ ap (coe (TmΓ= ([][]T ⁻¹ ◾ ([σ]T= El[] ◾ El[])))) ([]t=' refl refl refl refl (wk∘<> ⁻¹))
             ◾ coecoeTmΓ ([][]T ⁻¹)([σ]T= El[] ◾ El[]) ⁻¹
             ◾ ap (coe (TmΓ= ([σ]T= El[] ◾ El[]))) (coe2rTmΓ [][]T [][]t ⁻¹)
             ◾ coecoeTmΓ ([σ]T= El[]) El[] ⁻¹
             ◾ ap (coe (TmΓ= El[])) (coe[]t'' El[] ⁻¹)
          =u : u ≡[ TmΓ= (El= refl =a) ]≡ coe (TmΓ= El[]) (coe (TmΓ= El[]) vz [ < u > ]t)
          =u = coe= {p₀ = TmΓ= (El= refl =a)}{TmΓ= (([][]T ◾ A[]T= wk∘<> ◾ [id]T) ⁻¹ ◾ ([σ]T= El[] ◾ El[]))}(UIP _ _)
             ◾ coecoeTmΓ (([][]T ◾ A[]T= wk∘<> ◾ [id]T) ⁻¹)([σ]T= El[] ◾ El[]) ⁻¹
             ◾ ap (coe (TmΓ= ([σ]T= El[] ◾ El[]))) (coe2rTmΓ ([][]T ◾ A[]T= wk∘<> ◾ [id]T) vz[<>] ⁻¹)
             ◾ coecoeTmΓ ([σ]T= El[]) El[] ⁻¹
             ◾ ap (coe (TmΓ= El[])) (coe[]t'' El[] ⁻¹)

    lineU
      : {Γ : Con}{a : Tm Γ U}{t u : Tm Γ (El a)}(p : Tm Γ (El (Id t u)))
      → Tms Γ (Γ , El a , (El (Id (coe (TmΓ= El[])(t [ wk ]t)) (coe (TmΓ= El[]) vz))))
    lineU {Γ}{a}{t}{u} p = < u > ,s coe (TmΓ= (lineUT p)) p

    record Iden2 : Set (i ⊔ j) where
      field
        IdElim : ∀{Γ a}(t : Tm Γ (El a))(P : Ty (Γ , El a , El (Id (coe (TmΓ= El[])(t [ wk ]t)) (coe (TmΓ= El[]) vz))))
                 (pref : Tm Γ (P [ lineU (ref t) ]T))
                 {u : Tm Γ (El a)}(p : Tm Γ (El (Id t u)))
               → Tm Γ (P [ lineU p ]T)

        IdElim[]
          : ∀{Γ a}(t : Tm Γ (El a)){P : Ty (Γ , El a , El (Id (coe (TmΓ= El[])(t [ wk ]t)) (coe (TmΓ= El[]) vz)))}
            {pref : Tm Γ (P [ lineU (ref t) ]T)}
            {u : Tm Γ (El a)}{p : Tm Γ (El (Id t u))}
            {Θ : Con}{σ : Tms Θ Γ}
          → IdElim t P pref p [ σ ]t
          ≡[ TmΓ= {!!} ]≡
            IdElim
              {Θ}
              {coe (TmΓ= U[])  (a [ σ ]t)}
              (coe (TmΓ= El[]) (t [ σ ]t))
              (coe (Ty= (,C= (Γ,C= El[]) {!!}))
                   (P [ σ ^ El a ^ El (Id (coe (TmΓ= El[])(t [ wk ]t)) (coe (TmΓ= El[]) vz)) ]T))
              {!!}
              {coe (TmΓ= El[]) (u [ σ ]t)}
              (coe (TmΓ= (El[] ◾ El= refl Id[])) (p [ σ ]t))

        IdElimβ
          : {Γ : Con}{a : Tm Γ U}{t : Tm Γ (El a)}{P : Ty (Γ , El a , El (Id (coe (TmΓ= El[])(t [ wk ]t)) (coe (TmΓ= El[]) vz)))}
            {pref : Tm Γ (P [ lineU (ref t) ]T)}
          → IdElim t P pref (ref t) ≡ pref



{-
    IdElim[] : ∀{Γ A}{a : Tm Γ A}{P : Ty (Γ , A , Id (a [ wk ]t) vz)}
               {pref : Tm Γ (P [ line Id Id[] (ref a) ]T)}
               {u : Tm Γ A}{p : Tm Γ (Id a u)}
               {Θ : Con}{σ : Tms Θ Γ}
             → IdElim a P pref p [ σ ]t
             ≡[ TmΓ= (line[] Id Id[]) ]≡
               IdElim
                 {Θ}{A [ σ ]T}(a [ σ ]t)
                 (coe (Ty= (Γ,C= (Id[] ◾ IdT= Id [wk][^]T [wk][^]t vz[^])))
                      (P [ σ ^ A ^ Id (a [ wk ]t) vz ]T))
                 (coe (TmΓ= (line[] Id Id[] ◾ []T=' refl refl (ap (line Id Id[]) ref[]))) (pref [ σ ]t))
                 {u [ σ ]t}
                 (coe (TmΓ= Id[]) (p [ σ ]t))

-}

