{-# OPTIONS --without-K --no-eta #-}

module TT.FuncPar where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
import TT.Decl.Congr.NoJM

-- Function space with a metatheoretic domain

record FuncPar {i j k}{d : Decl}(c : Core {i}{j} d) : Set (i ⊔ j ⊔ (lsuc k)) where
  open Decl d
  open TT.Decl.Congr.NoJM d
  open Core c

  field
    Π : (A : Set k){Γ : Con}(B : A → Ty Γ) → Ty Γ

    Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Set k}{B : A → Ty Δ}
        → ((Π A B) [ σ ]T) ≡ (Π A (λ a → B a [ σ ]T))

    lam : {A : Set k}{Γ : Con}{B : A → Ty Γ} → ((a : A) → Tm Γ (B a)) → Tm Γ (Π A B)
    app : {A : Set k}{Γ : Con}{B : A → Ty Γ} → Tm Γ (Π A B)           → ((a : A) → Tm Γ (B a))

    lam[] : {A : Set k}{Γ Δ : Con}{B : A → Ty Δ}{δ : Tms Γ Δ}{t : (a : A) → Tm Δ (B a)}
          → ((lam t) [ δ ]t) ≡[ TmΓ= Π[] ]≡ (lam (λ a → t a [ δ ]t))

    Πβ    : {A : Set k}{Γ : Con}{B : A → Ty Γ}{t : (a : A) → Tm Γ (B a)}
          → (app (lam t)) ≡ t

    Πη    :  {A : Set k}{Γ : Con}{B : A → Ty Γ}{t : Tm Γ (Π A B)}
          → (lam (app t)) ≡ t
