{-# OPTIONS #-}

module TT.Iden where

open import Agda.Primitive
open import lib
open import JM

open import TT.Decl
open import TT.Core
import TT.Decl.Congr 
import TT.Core.Congr
import TT.Core.Laws

-- Identity type

module _ {i j}{d : Decl}(c : Core {i}{j} d) where
  open Decl d
  open TT.Decl.Congr d
  open TT.Core.Congr c
  open TT.Core.Laws c
  open Core c

  record Iden1 : Set (i ⊔ j) where
    field
      Id : {Γ : Con}{A : Ty Γ} → Tm Γ A → Tm Γ A → Ty Γ

      Id[] : ∀{Γ Θ A}{u v : Tm Γ A}{σ : Tms Θ Γ}
           → Id u v [ σ ]T ≡ Id (u [ σ ]t) (v [ σ ]t)

      ref : ∀{Γ A}(u : Tm Γ A) → Tm Γ (Id u u)

      ref[] : ∀{Γ Θ A}{u : Tm Γ A}{σ : Tms Θ Γ}
            → ref u [ σ ]t ≡[ TmΓ= Id[] ]≡ ref (u [ σ ]t)

  module _ (i1 : Iden1) where
    open Iden1 i1

    Id=
      : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
        {a₀ : Tm Γ A₀}{a₁ : Tm Γ A₁}(a₂ : a₀ ≡[ TmΓ= A₂ ]≡ a₁)
        {b₀ : Tm Γ A₀}{b₁ : Tm Γ A₁}(b₂ : b₀ ≡[ TmΓ= A₂ ]≡ b₁)
      → Id a₀ b₀ ≡ Id a₁ b₁
    Id= refl refl refl = refl

    line : {Γ : Con}{A : Ty Γ}{a u : Tm Γ A}(p : Tm Γ (Id a u))
         → Tms Γ (Γ , A , Id (a [ wk ]t) vz)
    line {Γ}{A}{a}{u} p
      = < u > ,s coe (TmΓ= (Id= [wk][id,] [wk][id,]t (vs[<>] (Id (u [ wk ]t) vz)) ◾ Id[] ⁻¹)) p

    line[]
      : {Γ : Con}{A : Ty Γ}{a : Tm Γ A}{P : Ty (Γ , A , Id (a [ wk ]t) vz)}
        {u : Tm Γ A}{p : Tm Γ (Id a u)}
        {Θ : Con}{σ : Tms Θ Γ}
      → P [ line p ]T [ σ ]T
      ≡ coe (Ty= (Γ,C= (Id[] ◾ Id= [wk][^]T [wk][^]t vz[^]))) (P [ σ ^ A ^ Id (a [ wk ]t) vz ]T) [ line (coe (TmΓ= Id[]) (p [ σ ]t)) ]T
    line[] {Γ}{A}{a}{P}{u}{p}{Θ}{σ}
      = [][]T
      ◾ []T='
          refl refl
          ( <>,∘
          ◾ ∘='
              refl refl
              (from≃
                 ( ,s≃
                     refl refl r̃ (to≃ (Id[] ◾ Id= [wk][^]T [wk][^]t vz[^]))
                     ( uncoe (TmΓ= ([][]T ◾ []T= refl refl refl <>∘ ◾ [][]T ⁻¹)) ⁻¹̃
                     ◾̃ coe[]t' (Id= [wk][id,] [wk][id,]t (vs[<>] (Id (u [ wk ]t) vz)) ◾ Id[] ⁻¹)
                     ◾̃ uncoe (TmΓ= Id[])
                     ◾̃ uncoe (TmΓ= (Id= [wk][id,] [wk][id,]t (vs[<>] (Id (u [ σ ]t [ wk ]t) vz)) ◾ Id[] ⁻¹)))
                 ◾̃ uncoe (TmsΓ-= (Γ,C= (Id[] ◾ Id= [wk][^]T [wk][^]t vz[^]) ⁻¹)))))
      ◾ [][]T ⁻¹
      ◾ coe[]T (Γ,C= (Id[] ◾ Id= [wk][^]T [wk][^]t vz[^])) ⁻¹


    record Iden2 : Set (i ⊔ j) where
      field
        IdElim : ∀{Γ A}(a : Tm Γ A)(P : Ty (Γ , A , Id (a [ wk ]t) vz))
                 (pref : Tm Γ (P [ line (ref a) ]T))
                 {u : Tm Γ A}(p : Tm Γ (Id a u))
               → Tm Γ (P [ line p ]T)

        IdElim[] : ∀{Γ A}{a : Tm Γ A}{P : Ty (Γ , A , Id (a [ wk ]t) vz)}
                   {pref : Tm Γ (P [ line (ref a) ]T)}
                   {u : Tm Γ A}{p : Tm Γ (Id a u)}
                   {Θ : Con}{σ : Tms Θ Γ}
                 → IdElim a P pref p [ σ ]t
                 ≡[ TmΓ= line[] ]≡
                   IdElim
                     {Θ}{A [ σ ]T}(a [ σ ]t)
                     (coe (Ty= (Γ,C= (Id[] ◾ Id= [wk][^]T [wk][^]t vz[^])))
                          (P [ σ ^ A ^ Id (a [ wk ]t) vz ]T))
                     (coe (TmΓ= (line[] ◾ A[]T= (ap line ref[]))) (pref [ σ ]t))
                     {u [ σ ]t}
                     (coe (TmΓ= Id[]) (p [ σ ]t))

        IdElimβ
          : {Γ : Con}{A : Ty Γ}{a : Tm Γ A}{P : Ty (Γ , A , Id (a [ wk ]t) vz)}
            {pref : Tm Γ (P [ line (ref a) ]T)}
          → IdElim a P pref (ref a) ≡ pref

    record Iden2-alt : Set (i ⊔ j) where
      field
        j-fill : ∀{Γ Δ A}(a : Tm Γ A)(M : Ty Δ)
                 (δ : Tms (Γ , A , Id (a [ wk ]t) vz) Δ)
                 (m : Tm Γ (M [ δ ∘ line (ref a) ]T))
                 → Tm (Γ , A , Id (a [ wk ]t) vz) (M [ δ ]T)

        -- j-uni : ∀{Γ Δ Θ A}(γ : Tms Θ Γ)(a : Tm Γ A)(M : Ty Δ)
        --          (δ : Tms (Γ , A , Id (a [ wk ]t) vz) Δ)
        --          (m : Tm Γ (M [ δ ∘ line (ref a) ]T))
        --          (j-fill a M δ m)[ γ ]t ≡ jfill (a [ γ ]t) ? ? ? 

        j-beta : ∀{Γ Δ A}(a : Tm Γ A)(M : Ty Δ)
                 (δ : Tms (Γ , A , Id (a [ wk ]t) vz) Δ)
                 (m : Tm Γ (M [ δ ∘ line (ref a) ]T))
                 → (j-fill a M δ m)[ line (ref a) ]t ≡[ TmΓ= [][]T ]≡ m

  record Iden : Set (i ⊔ j) where
    field
      i1 : Iden1
      i2 : Iden2 i1

    open Iden1 i1 public
    open Iden2 i2 public
