{-# OPTIONS --without-K --no-eta #-}

module TT.IsSet.Syntax where

open import TT.Decl.Syntax public
open import TT.IsSet

postulate
  syntaxIsSet : IsSet syntaxDecl
  
open IsSet syntaxIsSet public
