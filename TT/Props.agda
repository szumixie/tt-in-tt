{-# OPTIONS --without-K #-}

module TT.Props where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
open import TT.Base
import TT.Decl.Congr.NoJM
import TT.Core.Congr.NoJM
import TT.Base.Congr.NoJM

-- we use the universe U defined in TT.Base to define Prop

record Props {i j}{d : Decl}{c : Core {i}{j} d}(b : Base c) : Set (i ⊔ j) where
  open Decl d
  open Core c
  open Base b
  open TT.Decl.Congr.NoJM d
  open TT.Core.Congr.NoJM c
  open TT.Base.Congr.NoJM b

  field
    irr : {Γ : Con}{P : Tm Γ U}(u v : Tm Γ (El P)) → u ≡ v

    Π   : {Γ : Con}(A : Ty Γ)(b : Tm (Γ , A) U) → Tm Γ U
    lam : {Γ : Con}{A : Ty Γ}{b : Tm (Γ , A) U} → Tm (Γ , A) (El b) → Tm Γ (El (Π A b))
    app : {Γ : Con}{A : Ty Γ}{b : Tm (Γ , A) U} → Tm Γ (El (Π A b)) → Tm (Γ , A) (El b)
    Πβ  : {Γ : Con}{A : Ty Γ}{b : Tm (Γ , A) U}{t : Tm (Γ , A) (El b)} → app (lam t) ≡ t
    Πη  : {Γ : Con}{A : Ty Γ}{b : Tm (Γ , A) U}{t : Tm Γ (El (Π A b))} → lam (app t) ≡ t

    Π[] : {Γ : Con}{A : Ty Γ}{b : Tm (Γ , A) U}{Θ : Con}{σ : Tms Θ Γ} →
      (Π A b) [ σ ]t ≡[ TmΓ= U[] ]≡ Π (A [ σ ]T) (coe (TmΓ= U[]) (b [ σ ^ A ]t))
    lam[] : {Γ : Con}{A : Ty Γ}{b : Tm (Γ , A) U}{t : Tm (Γ , A) (El b)}{Θ : Con}{σ : Tms Θ Γ} →
      (lam t) [ σ ]t ≡[ TmΓ= (El[] ◾ El= refl Π[]) ]≡ lam (coe (TmΓ= El[]) (t [ σ ^ A ]t))
    -- substitution law for app is not needed because it can be
    -- proven, see e.g. app[] in TT.Func.Laws.NoJM

    Σ'     : {Γ : Con}(a : Tm Γ U)(b : Tm (Γ , El a) U) → Tm Γ U
    _,Σ'_  : {Γ : Con}{a : Tm Γ U}{b : Tm (Γ , El a) U}(u : Tm Γ (El a))(v : Tm Γ (El b [ < u > ]T)) → Tm Γ (El (Σ' a b))
    proj₁' : {Γ : Con}{a : Tm Γ U}{b : Tm (Γ , El a) U}(t : Tm Γ (El (Σ' a b))) → Tm Γ (El a)
    proj₂' : {Γ : Con}{a : Tm Γ U}{b : Tm (Γ , El a) U}(t : Tm Γ (El (Σ' a b))) → Tm Γ (El b [ < proj₁' t > ]T)
    Σβ₁'   : {Γ : Con}{a : Tm Γ U}{b : Tm (Γ , El a) U}{u : Tm Γ (El a)}{v : Tm Γ (El b [ < u > ]T)} → proj₁' (u ,Σ' v) ≡ u
    Σβ₂'   : {Γ : Con}{a : Tm Γ U}{b : Tm (Γ , El a) U}{u : Tm Γ (El a)}{v : Tm Γ (El b [ < u > ]T)} → proj₂' (u ,Σ' v) ≡[ TmΓ= (A[]T= (<>= Σβ₁')) ]≡ v
    Σ'[]   : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{b : Tm (Δ , El a) U}
           → ((Σ' a b) [ σ ]t) ≡[ TmΓ= U[] ]≡
                  (Σ' (coe (TmΓ= U[]) (a [ σ ]t)) (coe (Tm= (Γ,C= El[]) (U[]' (Γ,C= El[]))) (b [ σ ^ El a ]t)))
    {-
    TODO
    ,Σ[]   : {Γ : Con}{a : Tm Γ U}{b : Tm (Γ , El a) U}{u : Tm Γ (El a)}{v : Tm Γ (El b [ < u > ]T)}{Θ : Con}{σ : Tms Θ Γ}
           → (u ,Σ' v) [ σ ]t ≡[ TmΓ= (El[] ◾ El= refl Σ'[]) ]≡
             (coe (TmΓ= El[]) (u [ σ ]t) ,Σ'
              coe (TmΓ= ([][]T ◾ (El[] ◾ El= refl {!!} ◾ El[] ⁻¹))) (v [ σ ]t))
    -}
    
    ⊤'    : {Γ : Con} → Tm Γ U
    ⊤'[]  : {Γ : Con}{Θ : Con}{σ : Tms Θ Γ} → ⊤' [ σ ]t ≡[ TmΓ= U[] ]≡ ⊤'
    tt'   : {Γ : Con} → Tm Γ (El ⊤')
    tt'[] : {Γ : Con }{Θ : Con}{σ : Tms Θ Γ} → tt' [ σ ]t ≡[ TmΓ= (El[] ◾ El= refl ⊤'[]) ]≡ tt'

    ⊥'       : {Γ : Con} → Tm Γ U
    ⊥'[]     : {Γ : Con}{Θ : Con}{σ : Tms Θ Γ} → ⊥' [ σ ]t ≡[ TmΓ= U[] ]≡ ⊥'
    abort'   : {Γ : Con}{C : Ty Γ}(t : Tm Γ (El ⊥')) → Tm Γ C
    abort'[] : {Γ : Con}{C : Ty Γ}{t : Tm Γ (El ⊥')}{Θ : Con}{σ : Tms Θ Γ} → abort' {Γ}{C} t [ σ ]t ≡ abort' (coe (TmΓ= (El[] ◾ El= refl ⊥'[])) (t [ σ ]t))

  -- abbreviations

  _$_ : {Γ : Con}{A : Ty Γ}{b : Tm (Γ , A) U}(t : Tm Γ (El (Π A b)))(u : Tm Γ A) → Tm Γ (El b [ < u > ]T)
  t $ u = (app t) [ < u > ]t
  infixl 5 _$_

  _⇒_ : {Γ : Con}(A : Ty Γ)(b : Tm Γ U) → Tm Γ U
  A ⇒ b = Π A (coe (TmΓ= U[]) (b [ wk ]t))
  
  _×'_ : {Γ : Con}(a b : Tm Γ U) → Tm Γ U
  a ×' b = Σ' a (coe (TmΓ= U[]) (b [ wk ]t))
  infixl 5 _×'_
