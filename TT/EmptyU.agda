{-# OPTIONS --without-K --no-eta #-}

module TT.EmptyU where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
open import TT.Base
import TT.Decl.Congr.NoJM
import TT.Base.Congr.NoJM

record EmptyU {i j}{d : Decl}{c : Core {i}{j} d}(b : Base c) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr.NoJM d
  open Core c
  open Base b
  open TT.Base.Congr.NoJM b

  field
    Void   : ∀{Γ} → Tm Γ U
    Void[] : ∀{Γ Θ}{σ : Tms Γ Θ} → Void [ σ ]t ≡[ TmΓ= U[] ]≡ Void
    abort   : ∀{Γ}{C : Tm Γ U} → Tm Γ (El Void) → Tm Γ (El C)
    abort[] : ∀{Γ Θ}{σ : Tms Γ Θ}{C : Tm Θ U}{t : Tm Θ (El Void)}
            → abort t [ σ ]t ≡[ TmΓ= (El[]'' Void[]) ]≡ abort (coe (TmΓ= (El[]'' Void[])) (t [ σ ]t))
