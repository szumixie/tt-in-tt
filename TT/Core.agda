{-# OPTIONS --without-K #-}

module TT.Core where

open import Agda.Primitive
open import lib
open import TT.Decl
import TT.Decl.Congr.NoJM

-- Core substitution calculus

module _ {i j}(d : Decl {i}{j}) where
  open Decl d
  open TT.Decl.Congr.NoJM d

  record Core1 : Set (i ⊔ j) where
    field
      •     : Con  -- \bub
      _,_   : (Γ : Con) → Ty Γ → Con
      
      _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

      id    : ∀{Γ} → Tms Γ Γ
      _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
      ε     : ∀{Γ} → Tms Γ •
      _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
      π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ

      _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) 
      π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)
      
    infixl 5 _,_
    infixl 7 _[_]T
    infixl 5 _,s_
    infix  6 _∘_
    infixl 8 _[_]t

  module _ (c1 : Core1) where
    open Core1 c1

    []T=′ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
           {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
           {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ σ₁)
         → A₀ [ σ₀ ]T ≡[ Ty= Γ₂ ]≡ A₁ [ σ₁ ]T
    []T=′ refl refl refl refl = refl

    record Core2 : Set (i ⊔ j) where
      field
        [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
        [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
                → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)
                
        idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ 
        idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ 
        ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
              → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
        ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
              → ((δ ,s a) ∘ σ) ≡ ((δ ∘ σ) ,s coe (TmΓ= [][]T) (a [ σ ]t))
        π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
              → (π₁ (δ ,s a)) ≡ δ
        πη    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
              → (π₁ δ ,s π₂ δ) ≡ δ
        εη    : ∀{Γ}{σ : Tms Γ •}
              → σ ≡ ε
        
        π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
              → π₂ (δ ,s a) ≡[ TmΓ= ([]T=′ refl refl refl π₁β) ]≡ a

      -- abbreviations

      wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
      wk = π₁ id

      vz : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
      vz = π₂ id

      vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T) 
      vs x = x [ wk ]t

      <_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
      < t > = id ,s coe (TmΓ= ([id]T ⁻¹)) t

      infix 4 <_>

      _^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ σ ]T) (Δ , A)
      σ ^ A = (σ ∘ wk) ,s coe (TmΓ= [][]T) vz

      infixl 5 _^_

      [_,_] : {A : ∀{Γ} → Ty Γ}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]T ≡ A)(t : ∀{Γ} → Tm Γ A → Tm Γ A)
              {Γ : Con} → Tms (Γ , A) (Γ , A)
      [ A[] , t ] = wk ,s coe (TmΓ= (A[] ⁻¹)) (t (coe (TmΓ= A[]) vz))

  record Core : Set (i ⊔ j) where
    field
      c1 : Core1
      c2 : Core2 c1

    open Core1 c1 public
    open Core2 c2 public
