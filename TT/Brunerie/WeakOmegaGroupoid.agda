{-# OPTIONS --without-K --no-eta #-}

module TT.Brunerie.WeakOmegaGroupoid {i} where

open import Agda.Primitive
open import lib

open import TT.Decl.Morphism
open import TT.Core.Morphism
open import TT.Brunerie
open import TT.Brunerie.Syntax public
open import StandardModel.Decl
open import StandardModel.Core


-- Brunerie weak ω-groupoids


weak-ωGroupoid = Σ (Declᴱ syntaxDecl (d {i}))  (λ dᴱ → Coreᴱ syntaxCore (c {i}) dᴱ)


