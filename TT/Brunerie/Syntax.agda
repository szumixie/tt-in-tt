module TT.Brunerie.Syntax where

open import TT.Decl.Syntax public
open import TT.Core.Syntax public
open import TT.Brunerie
open import Agda.Primitive

postulate
  syntaxBrunerie : Brunerie {k = lzero} syntaxCore

open Brunerie syntaxBrunerie public
