{-# OPTIONS --no-eta #-}

module TT.BooleanU where

open import Agda.Primitive
open import lib
open import JM

open import TT.Decl
open import TT.Core
open import TT.Base
import TT.Decl.Congr
import TT.Core.Congr
import TT.Core.Laws
import TT.Base.Congr

record BooleanU {i j}{d : Decl}{c : Core {i}{j} d}(b : Base c) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr d
  open Core c
  open TT.Core.Congr c
  open TT.Core.Laws c
  open Base b
  open TT.Base.Congr b

  field
    Bool'          : ∀{Γ} → Tm Γ U
    Bool[]         : ∀{Γ Θ}{σ : Tms Γ Θ} → Bool' [ σ ]t ≡[ TmΓ= U[] ]≡ Bool'
    true' false'   : ∀{Γ} → Tm Γ (El Bool')
    true[]         : ∀{Γ Θ}{σ : Tms Γ Θ} → true' [ σ ]t ≡[ TmΓ= (El[]'' Bool[]) ]≡ true'
    false[]        : ∀{Γ Θ}{σ : Tms Γ Θ} → false' [ σ ]t ≡[ TmΓ= (El[]'' Bool[]) ]≡ false'
    if'_then_else_  : ∀{Γ}{C : Tm (Γ , El Bool') U}(b : Tm Γ (El Bool'))
                   → Tm Γ (El C [ < true' > ]T) → Tm Γ (El C [ < false' > ]T)
                   → Tm Γ (El C [ < b > ]T)
    ifthenelse[]   : ∀{Γ Θ}{σ : Tms Γ Θ}{C : Tm (Θ , El Bool') U}{b : Tm Θ (El Bool')}
                     {c : Tm Θ (El C [ < true' > ]T)}{d : Tm Θ (El C [ < false' > ]T)}
                   → (if' b then c else d) [ σ ]t
                   ≡[ TmΓ= (El[<>][]' Bool[] b) ]≡
                     if'_then_else_
                       {C = coe (TmΓ= U[]) (C [ coe (Tms-Γ= (Γ,C= (El[]'' Bool[]))) (σ ^ El Bool') ]t)}
                       (coe (TmΓ= (El[]'' Bool[])) (b [ σ ]t))
                       (coe (TmΓ= (El[<>][] Bool[] true[]))  (c [ σ ]t))
                       (coe (TmΓ= (El[<>][] Bool[] false[])) (d [ σ ]t))
    trueβ          : ∀{Γ Θ}{σ : Tms Γ Θ}{C : Tm (Θ , El Bool') U}
                     {c : Tm Θ (El C [ < true' > ]T)}{d : Tm Θ (El C [ < false' > ]T)}
                   → if' true' then c else d ≡ c
    falseβ         : ∀{Γ Θ}{σ : Tms Γ Θ}{C : Tm (Θ , El Bool') U}
                     {c : Tm Θ (El C [ < true' > ]T)}{d : Tm Θ (El C [ < false' > ]T)}
                   → if' false' then c else d ≡ d


record LargeElim {i j}{d : Decl}{c : Core {i}{j} d}{b : Base c}(bo : BooleanU b) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr d
  open Core c
  open TT.Core.Congr c
  open TT.Core.Laws c
  open Base b
  open TT.Base.Congr b
  open BooleanU bo

  field
    If_then_else_ : ∀{Γ}(b : Tm Γ (El Bool')) → Tm Γ U → Tm Γ U → Tm Γ U
    Ifthenelse[]  : ∀{Γ Θ}{σ : Tms Γ Θ}{b : Tm Θ (El Bool')}{c d : Tm Θ U}
                  → (If b then c else d) [ σ ]t
                  ≡[ TmΓ= U[] ]≡
                    If   coe (TmΓ= (El[]'' Bool[])) (b [ σ ]t)
                    then coe (TmΓ= U[]) (c [ σ ]t)
                    else coe (TmΓ= U[]) (d [ σ ]t)
    Trueβ         : ∀{Γ Θ}{σ : Tms Γ Θ}{c d : Tm Θ U} → If true'  then c else d ≡ c
    Falseβ        : ∀{Γ Θ}{σ : Tms Γ Θ}{c d : Tm Θ U} → If false' then c else d ≡ d
