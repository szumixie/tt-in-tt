{-# OPTIONS --without-K --no-eta #-}

open import TT.Decl

module TT.Decl.Morphism {i j i' j'}(d1 : Decl {i}{j}) (d2 : Decl {i'}{j'}) where

open import Agda.Primitive

private module d1 = Decl d1
private module d2 = Decl d2

record Declᴱ : Set (lsuc (i ⊔ j ⊔ i' ⊔ j')) where
  field
    Conᴱ : d1.Con → d2.Con
    Tyᴱ  : {Γ : d1.Con} → d1.Ty Γ → d2.Ty (Conᴱ Γ)
    Tmsᴱ : {Γ Δ : d1.Con} → d1.Tms Γ Δ → d2.Tms (Conᴱ Γ) (Conᴱ Δ)
    Tmᴱ  : {Γ : d1.Con}{A : d1.Ty Γ} → d1.Tm Γ A → d2.Tm (Conᴱ Γ) (Tyᴱ A)
