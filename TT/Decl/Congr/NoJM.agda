{-# OPTIONS --without-K --no-eta #-}

open import Agda.Primitive
open import TT.Decl

module TT.Decl.Congr.NoJM {i}{j}(d : Decl {i}{j}) where

open import lib

open Decl d

Ty= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → Ty Γ₀ ≡ Ty Γ₁
Ty= refl = refl

Tms= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁) → Tms Γ₀ Δ₀ ≡ Tms Γ₁ Δ₁
Tms= refl refl = refl

Tm= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
    → Tm Γ₀ A₀ ≡ Tm Γ₁ A₁
Tm= refl refl = refl

TmΓ= : {Γ : Con}{A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
     → Tm Γ A₀ ≡ Tm Γ A₁
TmΓ= A₂ = Tm= refl A₂

Tms-Γ= : ∀{Δ Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁) → Tms Γ₀ Δ ≡ Tms Γ₁ Δ
Tms-Γ= Γ₂ = Tms= Γ₂ refl

TmsΓ-= : ∀{Δ Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁) → Tms Δ Γ₀ ≡ Tms Δ Γ₁
TmsΓ-= Γ₂ = Tms= refl Γ₂

⁻¹Ty : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
     → A₁ ≡[ Ty= (Γ₂ ⁻¹) ]≡ A₀
⁻¹Ty refl refl = refl

coeTmΓ=
  : {Γ₀ : Con}{Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}(C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    {D₀ : Ty Γ₀}{D₁ : Ty Γ₁}(D₂ : D₀ ≡[ Ty= Γ₂ ]≡ D₁)
    {q₀ : C₀ ≡ D₀}{q₁ : C₁ ≡ D₁}(q₂ : q₀ ≡[ ≡= (Ty= Γ₂) C₂ D₂ ]≡ q₁)
    {t₀ : Tm Γ₀ C₀}{t₁ : Tm Γ₁ C₁}(t₂ : t₀ ≡[ Tm= Γ₂ C₂ ]≡ t₁)
  → coe (TmΓ= q₀) t₀ ≡[ Tm= Γ₂ D₂ ]≡ coe (TmΓ= q₁) t₁
coeTmΓ= refl refl refl refl refl = refl

