{-# OPTIONS --no-eta #-}

open import TT.Decl

module TT.Decl.Congr.JM {i}{j}(d : Decl {i}{j}) where

open import lib
open import JM

open Decl d

Tm≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
    → Tm Γ₀ A₀ ≡ Tm Γ₁ A₁
Tm≃ refl (refl ,≃ refl) = refl
