{-# OPTIONS --without-K #-}

module TT.Decl.Syntax where

open import Agda.Primitive
open import TT.Decl

postulate
  syntaxDecl : Decl {lzero}{lzero}
  
open Decl syntaxDecl public
