{-# OPTIONS --without-K --rewriting #-}

open import lib
open import TT.Syntax
open import TT.Congr
open import TT.Decl.DepModel

module TT.Decl.Elim {i j}(d : Declᴹ {i}{j}) where

open Declᴹ d

postulate
  ElimCon : ∀ Γ → Conᴹ Γ
  ElimTy  : ∀{Γ}(A : Ty Γ) → Tyᴹ (ElimCon Γ) A
  ElimTms : ∀{Γ Δ}(δ : Tms Γ Δ) → Tmsᴹ (ElimCon Γ) (ElimCon Δ) δ
  ElimTm  : ∀{Γ}{A : Ty Γ}(t : Tm Γ A) → Tmᴹ (ElimCon Γ) (ElimTy A) t
