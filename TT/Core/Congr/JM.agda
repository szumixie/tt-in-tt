{-# OPTIONS --no-eta #-}

open import Agda.Primitive
open import TT.Decl
open import TT.Core

module TT.Core.Congr.JM {i}{j}{d : Decl {i}{j}}(c : Core d) where

open import lib
open import JM

open Decl d
open Core c
open import TT.Decl.Congr.JM d
open import TT.Decl.Congr.NoJM d
open import TT.Core.Congr.NoJM c

-- Con

,C≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
      → _≡_ {A = Con} (Γ₀ , A₀) (Γ₁ , A₁)
,C≃ refl (refl ,≃ refl) = refl

-- Ty

[]T≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}{A : Ty Δ}
         {ρ₀ : Tms Γ₀ Δ}{ρ₁ : Tms Γ₁ Δ}(ρ₂ : ρ₀ ≃ ρ₁)
       → A [ ρ₀ ]T ≃ A [ ρ₁ ]T
[]T≃ refl (refl ,≃ refl) = refl ,≃ refl

[]T≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
        {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
       → A₀ [ ρ₀ ]T ≃ A₁ [ ρ₁ ]T
[]T≃' refl refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

-- Tms

id≃ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → id {Γ₀} ≃ id {Γ₁}
id≃ refl = refl ,≃ refl

,s≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
      {t₀ : Tm Γ₀ (A₀ [ ρ₀ ]T)}{t₁ : Tm Γ₁ (A₁ [ ρ₁ ]T)}(t₂ : t₀ ≃ t₁)
    → _≃_ {A = Tms Γ₀ (Δ₀ , A₀)}{Tms Γ₁ (Δ₁ , A₁)} (ρ₀ ,s t₀) (ρ₁ ,s t₁)
,s≃ refl refl (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

,s≃' : ∀{Γ}{Δ}
       {ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
       {A : Ty Δ}
       {t₀ : Tm Γ (A [ ρ₀ ]T)}{t₁ : Tm Γ (A [ ρ₁ ]T)}(t₂ : t₀ ≃ t₁)
     → _≡_ {A = Tms Γ (Δ , A)} (ρ₀ ,s t₀) (ρ₁ ,s t₁)
,s≃' refl (refl ,≃ refl) = refl

∘≃ : ∀{Γ Δ}{ρ : Tms Γ Δ}
     {Θ₀ Θ₁ : Con}(Θ₂ : Θ₀ ≡ Θ₁)
     {σ₀ : Tms Θ₀ Γ}{σ₁ : Tms Θ₁ Γ}(σ₂ : σ₀ ≃ σ₁)
   → (ρ ∘ σ₀) ≃ (ρ ∘ σ₁)
∘≃ refl (refl ,≃ refl) = refl ,≃ refl

∘≃' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
      {Θ₀ Θ₁ : Con}(Θ₂ : Θ₀ ≡ Θ₁)
      {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
      {σ₀ : Tms Θ₀ Γ₀}{σ₁ : Tms Θ₁ Γ₁}(σ₂ : σ₀ ≃ σ₁)
    → (ρ₀ ∘ σ₀) ≃ (ρ₁ ∘ σ₁)
∘≃' refl refl refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

π₁≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
      {δ₀ : Tms Γ₀ (Δ₀ , A₀)}{δ₁ : Tms Γ₁ (Δ₁ , A₁)}(δ₂ : δ₀ ≃ δ₁)
    → π₁ δ₀ ≃ π₁ δ₁
π₁≃ refl refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

π₁id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
        {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
      → π₁ {Γ₀ , A₀} id ≃ π₁ {Γ₁ , A₁} id
π₁id≃ refl (refl ,≃ refl) = refl ,≃ refl

π₁π₁id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
          {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
        → π₁ {(Γ₀ , A₀) , B₀} (π₁ id) ≃ π₁ {(Γ₁ , A₁) , B₁} (π₁ id)
π₁π₁id≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

π₁id∘coe''' : ∀{Δ Γ₀ Γ₁}{A : Ty Δ}(Γ₂ : Γ₀ ≡ Γ₁){σ : Tms Γ₀ (Δ , A)}
         → π₁ id ∘ coe (Tms-Γ= Γ₂) σ
         ≃ π₁ id ∘ σ
π₁id∘coe''' refl = r̃

id,t≃ : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t : Tm Γ (A₀ [ id ]T)}
      → _≃_ {A = Tms Γ (Γ , A₀)}
            {Tms Γ (Γ , A₁)}
            (id ,s t)
            (id ,s coe (TmΓ= (ap (λ z → z [ id ]T) A₂)) t)
id,t≃ refl = refl ,≃ refl

id,t= : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t : Tm Γ (A₀ [ id ]T)}
      → _≡_ {A = Tms Γ (Γ , A₁)}
            (coe (TmsΓ-= (,C≃ refl (to≃ A₂))) (id ,s t))
            (id ,s coe (TmΓ= (ap (λ z → z [ id ]T) A₂)) t)
id,t= refl = refl

ε≃ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
   → ε {Γ₀} ≃ ε {Γ₁}
ε≃ refl = refl ,≃ refl

coe∘'' : ∀{Θ Δ Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){σ : Tms (Γ , A₀) Δ}{δ : Tms Θ Γ}
      → coe (Tms-Γ= (,C≃ refl (to≃ A₂))) σ ∘ (δ ^ A₁)
      ≃ σ ∘ (δ ^ A₀)
coe∘'' refl = refl ,≃ refl

^≃ : ∀{Γ Δ}{σ₀ σ₁ : Tms Γ Δ}(σ₂ : σ₀ ≡ σ₁){A : Ty Δ}
   → (σ₀ ^ A) ≃ (σ₁ ^ A)
^≃ refl = refl ,≃ refl

^≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}
      {σ₀ : Tms Γ₀ Δ}{σ₁ : Tms Γ₁ Δ}(σ₂ : σ₀ ≃ σ₁)
      {A : Ty Δ}
    → (σ₀ ^ A) ≃ (σ₁ ^ A)
^≃' refl (refl ,≃ refl) = refl ,≃ refl

^≃'' : ∀{Γ Δ}{σ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁)
     → (σ ^ A₀) ≃ (σ ^ A₁)
^≃'' refl = refl ,≃ refl

^≃''' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
        {Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
        {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≃ σ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
      → (σ₀ ^ A₀) ≃ (σ₁ ^ A₁)
^≃''' refl refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

<>≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
      {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≃ t₁)
    → < t₀ > ≃ < t₁ >
<>≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

-- Tm

[]t≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}{A : Ty Δ}{t : Tm Δ A}
         {ρ₀ : Tms Γ₀ Δ}{ρ₁ : Tms Γ₁ Δ}(ρ₂ : ρ₀ ≃ ρ₁)
       → t [ ρ₀ ]t ≃ t [ ρ₁ ]t
[]t≃ refl (refl ,≃ refl) = refl ,≃ refl

[]t≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
        {t₀ : Tm Δ₀ A₀}{t₁ : Tm Δ₁ A₁}(t₂ : t₀ ≃ t₁)
        {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
      → t₀ [ ρ₀ ]t ≃ t₁ [ ρ₁ ]t
[]t≃' refl refl (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

[]t≃'' : ∀{Γ Δ}{A : Ty Δ}{t : Tm Δ A}{ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
       → t [ ρ₀ ]t ≃ t [ ρ₁ ]t
[]t≃'' refl = refl ,≃ refl

coe[]t' : ∀{Γ Δ}{ρ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){u : Tm Δ A₀}
        → coe (TmΓ= A₂) u [ ρ ]t ≃ u [ ρ ]t
coe[]t' refl = refl ,≃ refl

coecoe[]t : ∀{Γ Δ}{ρ : Tms Γ Δ}{A B : Ty Δ}{u : Tm Δ A}(q : A ≡ B)
            {C : Ty Γ}(r : B [ ρ ]T ≡ C)
          → coe (TmΓ= r) (coe (TmΓ= q) u [ ρ ]t) ≃ u [ ρ ]t
coecoe[]t refl refl = refl ,≃ refl

π₂≃ : ∀{Γ Δ}{A : Ty Δ}{ρ₀ ρ₁ : Tms Γ (Δ , A)}(ρ₂ : ρ₀ ≡ ρ₁)
    → π₂ ρ₀ ≃ π₂ ρ₁
π₂≃ refl = refl ,≃ refl

π₂≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
       {ρ₀ : Tms Γ₀ (Δ₀ , A₀)}{ρ₁ : Tms Γ₁ (Δ₁ , A₁)}(ρ₂ : ρ₀ ≃ ρ₁)
     → π₂ ρ₀ ≃ π₂ ρ₁
π₂≃' refl refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

π₂id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
        {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
       → π₂ {Γ₀ , A₀} id ≃ π₂ {Γ₁ , A₁} id
π₂id≃ refl (refl ,≃ refl) = refl ,≃ refl

π₂π₁id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
          {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
        → π₂ {(Γ₀ , A₀) , B₀} (π₁ id) ≃ π₂ {(Γ₁ , A₁) , B₁} (π₁ id)
π₂π₁id≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

vz≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
    → vz {Γ₀}{A₀} ≃ vz {Γ₁}{A₁}
vz≃ refl (refl ,≃ refl) = refl ,≃ refl

suc'≃ : {A : ∀{Γ} → Ty Γ}{s : ∀{Γ} → Tm Γ A → Tm Γ A}
     {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
     {t₀ : Tm Γ₀ A}{t₁ : Tm Γ₁ A}(t₂ : t₀ ≃ t₁)
   → s t₀ ≃ s t₁
suc'≃ refl (refl ,≃ refl) = refl ,≃ refl

coe7TmΓ
  : {Γ : Con}{A1 A2 A3 A4 A5 A6 A7 A8 : Ty Γ}
    (p1 : A1 ≡ A2)(p2 : A2 ≡ A3)(p3 : A3 ≡ A4)(p4 : A4 ≡ A5)(p5 : A5 ≡ A6)(p6 : A6 ≡ A7)(p7 : A7 ≡ A8)
    (q : A1 ≡ A8){t : Tm Γ A1}
  → coe (TmΓ= p7) (coe (TmΓ= p6) (coe (TmΓ= p5) (coe (TmΓ= p4) (coe (TmΓ= p3) (coe (TmΓ= p2) (coe (TmΓ= p1) t))))))
  ≡ coe (TmΓ= q) t
coe7TmΓ refl refl refl refl refl refl refl refl = refl

coe6TmΓ
  : {Γ : Con}{A1 A2 A3 A4 A5 A6 A7 : Ty Γ}
    (p1 : A1 ≡ A2)(p2 : A2 ≡ A3)(p3 : A3 ≡ A4)(p4 : A4 ≡ A5)(p5 : A5 ≡ A6)(p6 : A6 ≡ A7)
    (q : A1 ≡ A7){t : Tm Γ A1}
  → coe (TmΓ= p6) (coe (TmΓ= p5) (coe (TmΓ= p4) (coe (TmΓ= p3) (coe (TmΓ= p2) (coe (TmΓ= p1) t)))))
  ≡ coe (TmΓ= q) t
coe6TmΓ refl refl refl refl refl refl refl = refl

coe5TmΓ
  : {Γ : Con}{A1 A2 A3 A4 A5 A6 : Ty Γ}
    (p1 : A1 ≡ A2)(p2 : A2 ≡ A3)(p3 : A3 ≡ A4)(p4 : A4 ≡ A5)(p5 : A5 ≡ A6)
    (q : A1 ≡ A6){t : Tm Γ A1}
  → coe (TmΓ= p5) (coe (TmΓ= p4) (coe (TmΓ= p3) (coe (TmΓ= p2) (coe (TmΓ= p1) t))))
  ≡ coe (TmΓ= q) t
coe5TmΓ refl refl refl refl refl refl = refl

coe4TmΓ
  : {Γ : Con}{A1 A2 A3 A4 A5 : Ty Γ}
    (p1 : A1 ≡ A2)(p2 : A2 ≡ A3)(p3 : A3 ≡ A4)(p4 : A4 ≡ A5)
    (q : A1 ≡ A5){t : Tm Γ A1}
  → coe (TmΓ= p4) (coe (TmΓ= p3) (coe (TmΓ= p2) (coe (TmΓ= p1) t)))
  ≡ coe (TmΓ= q) t
coe4TmΓ refl refl refl refl refl = refl

coe3TmΓ
  : {Γ : Con}{A1 A2 A3 A4 : Ty Γ}
    (p1 : A1 ≡ A2)(p2 : A2 ≡ A3)(p3 : A3 ≡ A4)
    (q : A1 ≡ A4){t : Tm Γ A1}
  → coe (TmΓ= p3) (coe (TmΓ= p2) (coe (TmΓ= p1) t))
  ≡ coe (TmΓ= q) t
coe3TmΓ refl refl refl refl = refl

coe2TmΓ
  : {Γ : Con}{A1 A2 A3 : Ty Γ}
    (p1 : A1 ≡ A2)(p2 : A2 ≡ A3)
    (q : A1 ≡ A3){t : Tm Γ A1}
  → coe (TmΓ= p2) (coe (TmΓ= p1) t)
  ≡ coe (TmΓ= q) t
coe2TmΓ refl refl refl = refl
