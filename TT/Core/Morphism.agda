{-# OPTIONS --without-K --no-eta #-}

open import TT.Decl
open import TT.Core

module TT.Core.Morphism {i j i' j'}{d1 : Decl {i}{j}}{d2 : Decl {i'}{j'}}(c1 : Core d1)(c2 : Core d2) where

open import Agda.Primitive

open import lib

open import TT.Decl.Morphism

private module d1 = Decl d1
private module d2 = Decl d2
private module c1 = Core c1
private module c2 = Core c2

open import TT.Decl.Congr d2

record Coreᴱ (dᴱ : Declᴱ d1 d2) : Set (lsuc (i ⊔ j ⊔ i' ⊔ j')) where
  open Declᴱ dᴱ
  field
    •ᴱ : Conᴱ c1.• ≡ c2.•
    ,ᴱ : ∀{Γ A} → (Conᴱ (Γ c1., A)) ≡ ((Conᴱ Γ) c2., (Tyᴱ A))
    []Tᴱ   : ∀{Γ Δ}{A : d1.Ty Δ}{σ : d1.Tms Γ Δ}
           → (Tyᴱ (A c1.[ σ ]T)) ≡ (Tyᴱ A c2.[ Tmsᴱ σ ]T)
    idᴱ : ∀{Γ} → (Tmsᴱ (c1.id {Γ})) ≡ c2.id
    ∘ᴱ  : ∀{Γ Δ Σ}{σ : d1.Tms Δ Σ}{ν : d1.Tms Γ Δ}
        → (Tmsᴱ (σ c1.∘ ν)) ≡ (Tmsᴱ σ c2.∘ Tmsᴱ ν)
    εᴱ  : ∀{Γ} → (Tmsᴱ (c1.ε {Γ})) ≡[ TmsΓ-= •ᴱ ]≡ c2.ε
    ,sᴱ : ∀{Γ Δ}{σ : d1.Tms Γ Δ}{A : d1.Ty Δ}{t : d1.Tm Γ (A c1.[ σ ]T)}
        → (Tmsᴱ (σ c1.,s t)) ≡[ TmsΓ-= ,ᴱ ]≡ (Tmsᴱ σ c2.,s coe (TmΓ= []Tᴱ) (Tmᴱ t))
    π₁ᴱ : ∀{Γ Δ}{A : d1.Ty Δ}{σ : d1.Tms Γ (Δ c1., A)}
        → (Tmsᴱ (c1.π₁ σ)) ≡ (c2.π₁ (coe (TmsΓ-= ,ᴱ) (Tmsᴱ σ)))
    []tᴱ : ∀{Γ Δ}{A : d1.Ty Δ}{t : d1.Tm Δ A}{σ : d1.Tms Γ Δ}
         → (Tmᴱ (t c1.[ σ ]t)) ≡[ TmΓ= []Tᴱ ]≡ (Tmᴱ t c2.[ Tmsᴱ σ ]t)
    π₂ᴱ  : ∀{Γ Δ}{A : d1.Ty Δ}{σ : d1.Tms Γ (Δ c1., A)}
         → (Tmᴱ (c1.π₂ σ)) ≡[ TmΓ= ([]Tᴱ ◾ ap (c2._[_]T (Tyᴱ A)) π₁ᴱ) ]≡ (c2.π₂ (coe (TmsΓ-= ,ᴱ) (Tmsᴱ σ)))
