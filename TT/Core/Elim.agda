{-# OPTIONS --without-K --rewriting #-}

open import lib
open import TT.Syntax
open import TT.Congr
open import TT.DepModel

module TT.Core.Elim {i j}{d : Declᴹ}(c : Coreᴹ {i}{j} d) where

open Declᴹ d
open Coreᴹ c

postulate
  ElimCon : ∀ Γ → Conᴹ Γ
  ElimTy  : ∀{Γ}(A : Ty Γ) → Tyᴹ (ElimCon Γ) A
  ElimTms : ∀{Γ Δ}(δ : Tms Γ Δ) → Tmsᴹ (ElimCon Γ) (ElimCon Δ) δ
  ElimTm  : ∀{Γ}{A : Ty Γ}(t : Tm Γ A) → Tmᴹ (ElimCon Γ) (ElimTy A) t

postulate
  β• : ElimCon • ≡ •ᴹ
  β, : ∀{Γ A} → ElimCon (Γ , A) ≡ (ElimCon Γ ,Cᴹ ElimTy A)
  
{-# REWRITE β• #-}
{-# REWRITE β, #-}

postulate
  β[]T : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ} → ElimTy (A [ δ ]T) ≡ ElimTy A [ ElimTms δ ]Tᴹ

{-# REWRITE β[]T #-}

postulate
  βε  : ∀{Γ} → ElimTms (ε {Γ}) ≡ εᴹ
  β,s : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ (A [ σ ]T)}
      → (ElimTms (σ ,s t)) ≡ (ElimTms σ ,sᴹ ElimTm t)
  βid : ∀{Γ} → ElimTms (id {Γ}) ≡ idᴹ
  β∘  : ∀{Γ Δ Σ}{σ : Tms Δ Σ}{ν : Tms Γ Δ}
      → ElimTms (σ ∘ ν) ≡ ElimTms σ ∘ᴹ ElimTms ν
  βπ₁ : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}
      → ElimTms (π₁ σ) ≡ π₁ᴹ (ElimTms σ)

{-# REWRITE βid #-}
{-# REWRITE β∘  #-}
{-# REWRITE βε  #-}
{-# REWRITE β,s #-}
{-# REWRITE βπ₁ #-}

postulate
  β[]t : ∀{Γ Δ}{A : Ty Δ}{t : Tm Δ A}{σ : Tms Γ Δ}
       → ElimTm (t [ σ ]t) ≡ ElimTm t [ ElimTms σ ]tᴹ
  βπ₂  : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}
       → ElimTm (π₂ σ)     ≡ π₂ᴹ (ElimTms σ)

{-# REWRITE β[]t #-}
{-# REWRITE βπ₂  #-}
