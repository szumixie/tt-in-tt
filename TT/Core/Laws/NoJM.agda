{-# OPTIONS --without-K --no-eta #-}

open import Agda.Primitive
open import TT.Decl
open import TT.Core

module TT.Core.Laws.NoJM {i}{j}{d : Decl {i}{j}}(c : Core d) where

open import lib

open Decl d
open Core c
open import TT.Decl.Congr.NoJM d
open import TT.Core.Congr.NoJM c

------------------------------------------------------------------------------
-- Con
------------------------------------------------------------------------------

,[],
  : {A : ∀{Γ} → Ty Γ}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]T ≡ A)
    {Γ Θ : Con}{σ : Tms Γ Θ}{P : Ty (Θ , A)}
  → Γ , A [ σ ]T , P [ σ ^ A ]T
  ≡ Γ , A , P [ coe (Tms-Γ= (,C= refl A[])) (σ ^ A) ]T
,[], A[] = ,C= (,C= refl A[]) ([]T=' (,C= refl A[]) refl refl)

------------------------------------------------------------------------------
-- Tms (1)
------------------------------------------------------------------------------

π₁∘ : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
     → π₁ δ ∘ ρ ≡ π₁ (δ ∘ ρ)
π₁∘ {Γ}{Δ}{Θ}{A}{δ}{ρ}
  = π₁β ⁻¹ ◾ π₁=' (,∘ ⁻¹) ◾ π₁=' (ap (λ z → (z ∘ ρ)) πη)

π₁idβ : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ (A [ ρ ]T)}
      → π₁ id ∘ (ρ ,s t) ≡ ρ
π₁idβ = π₁∘ ◾ π₁=' idl ◾ π₁β

abstract
  ∘π₁id : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}
        → ρ ∘ π₁ {A = A [ ρ ]T} id ≡ π₁ id ∘ (ρ ^ A)
  ∘π₁id {Γ}{Δ}{ρ}{A} = π₁β ⁻¹ ◾ ap π₁ idl ⁻¹ ◾ π₁∘ ⁻¹

π₁id∘ : ∀{Γ Δ}{A : Ty Δ}{ρ : Tms Γ (Δ , A)}
      → π₁ id ∘ ρ ≡ π₁ ρ
π₁id∘ = π₁∘ ◾ π₁=' idl

abstract
  π₁id∘coe : ∀{Δ Γ₀ Γ₁}{A : Ty Δ}(Γ₂ : Γ₀ ≡ Γ₁){σ : Tms Γ₀ (Δ , A)}
           → π₁ id ∘ coe (Tms-Γ= Γ₂) σ
           ≡ coe (Tms-Γ= Γ₂) (π₁ σ)
  π₁id∘coe refl = π₁∘ ◾ ap π₁ idl

π₂[] : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
     → π₂ δ [ ρ ]t ≡[ TmΓ= ([][]T ◾ []T= refl refl refl π₁∘) ]≡ π₂ (δ ∘ ρ)
π₂[] {A = A}{δ}{ρ}
  = ap (λ z → coe (TmΓ= ([][]T ◾ A[]T= z)) (π₂ δ [ ρ ]t)) (◾ass (π₁β ⁻¹)(π₁=' (,∘ ⁻¹))(π₁=' (ap (λ z → z ∘ ρ) πη)) ⁻¹)
  ◾ ap (λ z → coe (TmΓ= ([][]T ◾ z)) (π₂ δ [ ρ ]t)) (A[◾]T= (π₁β ⁻¹)(π₁=' (,∘ ⁻¹) ◾ π₁=' (ap (λ z → z ∘ ρ) πη)))
  ◾ ap (λ z → coe (TmΓ= ([][]T ◾ (z ◾ A[]T= (π₁=' (,∘ ⁻¹) ◾ π₁=' (ap (λ z → z ∘ ρ) πη))))) (π₂ δ [ ρ ]t)) (A[⁻¹]T= π₁β) 
  ◾ ap (λ z → coe (TmΓ= ([][]T ◾ (A[]T= π₁β ⁻¹ ◾ z))) (π₂ δ [ ρ ]t)) (A[◾]T= (π₁=' (,∘ ⁻¹))(π₁=' (ap (λ z → z ∘ ρ) πη)))
  ◾ coecoeTmΓ [][]T (A[]T= π₁β ⁻¹ ◾ (A[]T= (π₁=' (,∘ ⁻¹)) ◾ A[]T= (π₁=' (ap (λ z → z ∘ ρ) πη)))) ⁻¹
  ◾ coecoeTmΓ (A[]T= π₁β ⁻¹) (A[]T= (π₁=' (,∘ ⁻¹)) ◾ A[]T= (π₁=' (ap (λ z → z ∘ ρ) πη)))  ⁻¹
  ◾ ap (coe (TmΓ= (A[]T= (π₁=' (,∘ ⁻¹)) ◾ A[]T= (π₁=' (ap (λ z → z ∘ ρ) πη)))))
       (coe2rTmΓ (A[]T= π₁β) π₂β ⁻¹)
  ◾ coecoeTmΓ (A[]T= (π₁=' (,∘ ⁻¹))) (A[]T= (π₁=' (ap (λ z → z ∘ ρ) πη))) ⁻¹
  ◾ ap (coe (TmΓ= (A[]T= (π₁=' (ap (λ z → z ∘ ρ) πη))))) (π₂= refl refl refl (,∘ ⁻¹))
  ◾ π₂= refl refl refl (ap (λ z → z ∘ ρ) (πη {δ = δ}))
{-
<>∘' : ∀{Γ Δ A}{u : Tm Δ A}{δ : Tms Γ Δ}
    → < u > ∘ δ ≡ δ ,s u [ δ ]t
<>∘' {Γ}{Δ}{A}{u}{δ}
  = ,∘
  ◾ ,s= refl refl {id ∘ δ}{δ} idl refl
        ( ap (λ z → coe (TmΓ= (A[]T= idl)) (coe (TmΓ= [][]T) z)) (coe[]t'' ([id]T ⁻¹))
        ◾ coecoeTmΓ [][]T (A[]T= idl)
        ◾ coecoeTmΓ ([σ]T= ([id]T ⁻¹))([][]T ◾ A[]T= idl)
        ◾ {!!})

<>∘ : ∀{Γ Δ A}{u : Tm Δ A}{δ : Tms Γ Δ}
    → < u > ∘ δ ≡ (δ ^ A) ∘ < u [ δ ]t >
<>∘ {Γ}{Δ}{A}{u}{δ}
  = ,∘
  ◾ {!coe[]t''!}
{-
    ,s= refl refl
        {id ∘ δ}{(δ ∘ π₁ id) ∘ < u [ δ ]t >}(idl ◾ idr ⁻¹ ◾ σ∘= (π₁idβ ⁻¹) ◾ ass ⁻¹)
        refl
        {coe (TmΓ= [][]T) (coe (TmΓ= ([id]T ⁻¹)) u [ δ ]t)}
        {coe (TmΓ= [][]T) (coe (TmΓ= [][]T) (π₂ id) [ < u [ δ ]t > ]t)}
        {!!}-}
{-        
        ( coecoe (TmΓ= [][]T) (TmΓ= (A[]T= (idl ◾ idr ⁻¹ ◾ ap (_∘_ δ) (π₁idβ ⁻¹) ◾ ass ⁻¹)))
        ◾ ap (coe (TmΓ= [][]T ◾ TmΓ= (A[]T= (idl ◾ idr ⁻¹ ◾ ap (_∘_ δ) (π₁idβ ⁻¹) ◾ ass ⁻¹)))) (coe[]t ([id]T ⁻¹))
        ◾ coecoe (TmΓ= (ap (λ z → z [ δ ]T) ([id]T ⁻¹))) (TmΓ= [][]T ◾ TmΓ= (A[]T= (idl ◾ idr ⁻¹ ◾ ap (_∘_ δ) (π₁idβ ⁻¹) ◾ ass ⁻¹)))
        ◾ {!!}
        ◾ ap (λ z → coe (TmΓ= (ap (λ z → z [ < u [ δ ]t > ]T) [][]T) ◾ TmΓ= [][]T) (coe (TmΓ= (([][]T ◾ A[]T= π₁∘) ⁻¹)) (coe (TmΓ= (A[]T= (π₁=' idl) ⁻¹)) z))) (coe2rTmΓ (A[]T= π₁β) π₂β ⁻¹)
        ◾ ap (λ z → coe (TmΓ= (ap (λ z → z [ < u [ δ ]t > ]T) [][]T) ◾ TmΓ= [][]T) (coe (TmΓ= (([][]T ◾ A[]T= π₁∘) ⁻¹)) z)) (coe2rTmΓ (A[]T= (π₁=' idl)) (π₂= refl refl refl idl) ⁻¹)
        ◾ ap (coe (TmΓ= (ap (λ z → z [ < u [ δ ]t > ]T) [][]T) ◾ TmΓ= [][]T)) (coe2rTmΓ ([][]T ◾ A[]T= π₁∘) π₂[] ⁻¹)
        ◾ coecoe (TmΓ= (ap (λ z → z [ < u [ δ ]t > ]T) [][]T)) (TmΓ= [][]T) ⁻¹
        ◾ ap (coe (TmΓ= [][]T)) (coe[]t [][]T ⁻¹))
-}
  ◾ (,∘ ⁻¹)
-}

wk∘<> : ∀{Γ A}{u : Tm Γ A} → wk ∘ < u > ≡ id
wk∘<> = π₁id∘ ◾ π₁β

------------------------------------------------------------------------------
-- Ty
------------------------------------------------------------------------------

[wk][id,]
  : {Γ : Con}{A B : Ty Γ}{u : Tm Γ (B [ id ]T)}
  → A ≡ A [ wk ]T [ id ,s u ]T
[wk][id,] {Γ}{A}{B}{u}
  = [id]T ⁻¹
  ◾ ap
      (_[_]T A)
      ( π₁β ⁻¹
      ◾ π₁id∘ ⁻¹)
  ◾ [][]T ⁻¹

[wk][,]
  : {Γ Δ : Con}{σ : Tms Γ Δ}{A B : Ty Δ}{u : Tm Γ (B [ σ ]T)}
  → A [ wk ]T [ σ ,s u ]T ≡ A [ σ ]T
[wk][,] {Γ}{Δ}{σ}{A}{B}{u}
  = [][]T
  ◾ A[]T= (π₁id∘ ◾ π₁β)

abstract
  [wk][wk][id,,]
    : {Γ : Con}{A B : Ty Γ}{C : Ty (Γ , B)}{u : Tm Γ (B [ id ]T)}{v : Tm Γ (C [ id ,s u ]T)}
    → A ≡ A [ wk ]T [ wk ]T [ id ,s u ,s v ]T
  [wk][wk][id,,] {Γ}{A}{B}{C}{u}{v}
    = [id]T ⁻¹
    ◾ ap
        (_[_]T A)
        ( π₁β ⁻¹
        ◾ ap π₁ (π₁β ⁻¹ ◾ π₁id∘ ⁻¹)
        ◾ π₁id∘ ⁻¹)
    ◾ [][]T ⁻¹
    ◾ [][]T ⁻¹

[wk][^]T
  : {Γ Θ : Con}{A : Ty Γ}{σ : Tms Θ Γ}{B : Ty Γ}
  → B [ wk ]T [ σ ^ A ]T ≡ B [ σ ]T [ wk ]T
[wk][^]T = [][]T ◾ []T=' refl refl π₁idβ ◾ [][]T ⁻¹

[]T∘ : {Γ Δ Θ : Con}{ν : Tms Γ Δ}{σ : Tms Δ Θ}{A : Ty Δ}{B : Ty Θ}
      (p : A ≡ B [ σ ]T)
    → A [ ν ]T ≡ B [ σ ∘ ν ]T
[]T∘ p = [σ]T= p ◾ [][]T

[][]T∘ : {Γ Δ Δ' Θ : Con}{σ : Tms Γ Δ}{ν : Tms Δ Θ}{σ' : Tms Γ Δ'}{ν' : Tms Δ' Θ}
      → ν ∘ σ ≡ ν' ∘ σ' → {A : Ty Θ} → A [ ν ]T [ σ ]T ≡ A [ ν' ]T [ σ' ]T
[][]T∘ p = [][]T ◾ (A[]T= p ◾ [][]T ⁻¹)
