{-# OPTIONS --without-K --rewriting #-}

module TT.Case where

open import lib
open import TT.Syntax

record Cases : Set₁ where
  field
    Conᴹ   : Con → Set
    Tyᴹ    : {Γ : Con} → Ty Γ → Set
    Tmsᴹ   : {Γ Δ : Con} → Tms Γ Δ → Set
    Tmᴹ    : {Γ : Con}{A : Ty Γ} → Tm Γ A → Set
    •ᴹ     : Conᴹ •
    _,ᴹ_   : (Γ : Con)(A : Ty Γ) → Conᴹ (Γ , A)
    _[_]Tᴹ : {Γ Δ : Con}(A : Ty Δ)(δ : Tms Γ Δ) → Tyᴹ (A [ δ ]T)
    εᴹ     : {Γ : Con} → Tmsᴹ (ε {Γ})
    _,sᴹ_  : {Γ Δ : Con}(δ : Tms Γ Δ){A : Ty Δ}(t : Tm Γ (A [ δ ]T))
           → Tmsᴹ (δ ,s t)
    idᴹ    : {Γ : Con} → Tmsᴹ (id {Γ})
    _∘ᴹ_   : {Γ Δ Σ : Con}(σ : Tms Δ Σ)(δ : Tms Γ Δ)
           → Tmsᴹ (σ ∘ δ)
    π₁ᴹ    : {Γ Δ : Con}{A : Ty Δ}(δ : Tms Γ (Δ , A))
           → Tmsᴹ (π₁ δ)
    _[_]tᴹ : {Γ Δ : Con}{A : Ty Δ}(t : Tm Δ A)(δ : Tms Γ Δ)
           → Tmᴹ (t [ δ ]t)
    π₂ᴹ    : {Γ Δ : Con}{A : Ty Δ}(δ : Tms Γ (Δ , A))
           → Tmᴹ (π₂ δ)
    Uᴹ     : {Γ : Con} → Tyᴹ (U {Γ})
    Elᴹ    : {Γ : Con}(a : Tm Γ U) → Tyᴹ (El a)
    Πᴹ     : {Γ : Con}(A : Ty Γ)(B : Ty (Γ , A)) → Tyᴹ (Π A B)
    appᴹ   : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm Γ (Π A B))
           → Tmᴹ (app t)
    lamᴹ   : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm (Γ , A) B)
           → Tmᴹ (lam t)

  -- there is no way to express the equations

  infixl 4 _,ᴹ_
  infixl 7 _[_]Tᴹ
  infixl 4 _,sᴹ_
  infix  6 _∘ᴹ_
  infixl 8 _[_]tᴹ

module _ (M : Cases) where
  open Cases M
  
  postulate
    CaseCon : (Γ : Con) → Conᴹ Γ
    CaseTy  : {Γ : Con}(A : Ty Γ) → Tyᴹ A
    CaseTms : {Γ Δ : Con}(σ : Tms Γ Δ) → Tmsᴹ σ
    CaseTm  : {Γ : Con}{A : Ty Γ}(t : Tm Γ A) → Tmᴹ t
    Case•   : CaseCon • ≡ •ᴹ
    Case,   : {Γ : Con}{A : Ty Γ} → CaseCon (Γ , A) ≡ (Γ ,ᴹ A)
    Case[]T : {Γ Δ : Con}{A : Ty Δ}{δ : Tms Γ Δ}
            → CaseTy (A [ δ ]T) ≡ A [ δ ]Tᴹ
    Caseε   : {Γ : Con} → CaseTms (ε {Γ}) ≡ εᴹ
    Case,s  : {Γ Δ : Con}{δ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ (A [ δ ]T)}
            → CaseTms (δ ,s t) ≡ (δ ,sᴹ t)
    Caseid  : {Γ : Con} → CaseTms (id {Γ}) ≡ idᴹ
    Case∘   : {Γ Δ Σ : Con}{σ : Tms Δ Σ}{δ : Tms Γ Δ}
            → CaseTms (σ ∘ δ) ≡ σ ∘ᴹ δ
    Caseπ₁  : {Γ Δ : Con}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
            → CaseTms (π₁ δ) ≡ π₁ᴹ δ
    Case[]t : {Γ Δ : Con}{A : Ty Δ}{t : Tm Δ A}{δ : Tms Γ Δ}
            → CaseTm (t [ δ ]t) ≡ t [ δ ]tᴹ
    Caseπ₂  : {Γ Δ : Con}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
            → CaseTm (π₂ δ) ≡ π₂ᴹ δ
    CaseU   : {Γ : Con} → CaseTy (U {Γ}) ≡ Uᴹ
    CaseEl  : {Γ : Con}{a : Tm Γ U} → CaseTy (El a) ≡ Elᴹ a
    CaseΠ   : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)} → CaseTy (Π A B) ≡ Πᴹ A B
    Caseapp : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
           → CaseTm (app t) ≡ appᴹ t
    Caselam : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
           → CaseTm (lam t) ≡ lamᴹ t

  {-# REWRITE Case•   #-}
  {-# REWRITE Case,   #-}
  {-# REWRITE Case[]T #-}
  {-# REWRITE Caseε   #-}
  {-# REWRITE Case,s  #-}
  {-# REWRITE Caseid  #-}
  {-# REWRITE Case∘   #-}
  {-# REWRITE Caseπ₁  #-}
  {-# REWRITE Case[]t #-}
  {-# REWRITE Caseπ₂  #-}
  {-# REWRITE CaseU   #-}
  {-# REWRITE CaseEl  #-}
  {-# REWRITE CaseΠ   #-}
  {-# REWRITE Caseapp #-}
  {-# REWRITE Caselam #-}
