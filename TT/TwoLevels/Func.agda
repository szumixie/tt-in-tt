{-# OPTIONS --without-K --no-eta #-}

module TT.TwoLevels.Func where

open import Agda.Primitive
open import lib

open import TT.TwoLevels.Decl
open import TT.TwoLevels.Core

-- Function space

record Func₀ {d : Decl}(c : Core d) : Set₂ where
  open Decl d
  open Core c

  field
    Π : ∀{Γ}(A : Ty₀ Γ)(B : Ty₀ (Γ ,₀ A)) → Ty₀ Γ
    
    Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty₀ Δ}{B : Ty₀ (Δ ,₀ A)}
        → ((Π A B) [ σ ]T₀) ≡ (Π (A [ σ ]T₀) (B [ σ ^₀ A ]T₀))

    lam : ∀{Γ}{A : Ty₀ Γ}{B : Ty₀ (Γ ,₀ A)} → Tm₀ (Γ ,₀ A) B → Tm₀ Γ (Π A B)

    app : ∀{Γ}{A : Ty₀ Γ}{B : Ty₀ (Γ ,₀ A)} → Tm₀ Γ (Π A B) → Tm₀ (Γ ,₀ A) B

    lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty₀ Δ}{B : Ty₀ (Δ ,₀ A)}{t : Tm₀ (Δ ,₀ A) B}                       
          → ((lam t) [ δ ]t₀) ≡[ ap (Tm₀ _) Π[] ]≡ (lam (t [ δ ^₀ A ]t₀))
    Πβ    : ∀{Γ}{A : Ty₀ Γ}{B : Ty₀ (Γ ,₀ A)}{t : Tm₀ (Γ ,₀ A) B}
          → (app (lam t)) ≡ t
    Πη    : ∀{Γ}{A : Ty₀ Γ}{B : Ty₀ (Γ ,₀ A)}{t : Tm₀ Γ (Π A B)}
          → (lam (app t)) ≡ t

  -- abbreviations

  _$_ : ∀ {Γ}{A : Ty₀ Γ}{B : Ty₀ (Γ ,₀ A)}(t : Tm₀ Γ (Π A B))(u : Tm₀ Γ A) → Tm₀ Γ (B [ < u >₀ ]T₀)
  t $ u = (app t) [ < u >₀ ]t₀

  infixl 5 _$_

record Func₁ {d : Decl}(c : Core d) : Set₂ where
  open Decl d
  open Core c

  field
    Π : ∀{Γ}(A : Ty₁ Γ)(B : Ty₁ (Γ ,₁ A)) → Ty₁ Γ
    
    Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty₁ Δ}{B : Ty₁ (Δ ,₁ A)}
        → ((Π A B) [ σ ]T₁) ≡ (Π (A [ σ ]T₁) (B [ σ ^₁ A ]T₁))

    lam : ∀{Γ}{A : Ty₁ Γ}{B : Ty₁ (Γ ,₁ A)} → Tm₁ (Γ ,₁ A) B → Tm₁ Γ (Π A B)

    app : ∀{Γ}{A : Ty₁ Γ}{B : Ty₁ (Γ ,₁ A)} → Tm₁ Γ (Π A B) → Tm₁ (Γ ,₁ A) B

    lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty₁ Δ}{B : Ty₁ (Δ ,₁ A)}{t : Tm₁ (Δ ,₁ A) B}                       
          → ((lam t) [ δ ]t₁) ≡[ ap (Tm₁ _) Π[] ]≡ (lam (t [ δ ^₁ A ]t₁))
    Πβ    : ∀{Γ}{A : Ty₁ Γ}{B : Ty₁ (Γ ,₁ A)}{t : Tm₁ (Γ ,₁ A) B}
          → (app (lam t)) ≡ t
    Πη    : ∀{Γ}{A : Ty₁ Γ}{B : Ty₁ (Γ ,₁ A)}{t : Tm₁ Γ (Π A B)}
          → (lam (app t)) ≡ t

  -- abbreviations

  _$_ : ∀ {Γ}{A : Ty₁ Γ}{B : Ty₁ (Γ ,₁ A)}(t : Tm₁ Γ (Π A B))(u : Tm₁ Γ A) → Tm₁ Γ (B [ < u >₁ ]T₁)
  t $ u = (app t) [ < u >₁ ]t₁

  infixl 5 _$_
