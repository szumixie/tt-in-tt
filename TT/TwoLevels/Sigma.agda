{-# OPTIONS --no-eta #-}

module TT.TwoLevels.Sigma where

open import Agda.Primitive
open import lib

open import TT.TwoLevels.Decl
open import TT.TwoLevels.Core

record Sigma {d : Decl}(c : Core d) : Set₂ where
  open Decl d
  open Core c

  field
    Σ' : ∀{Γ}(A : Ty₀ Γ)(B : Ty₀ (Γ ,₀ A)) → Ty₀ Γ
    
    Σ[]    : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty₀ Δ}{B : Ty₀ (Δ ,₀ A)}
           → ((Σ' A B) [ σ ]T₀) ≡ (Σ' (A [ σ ]T₀) (B [ σ ^₀ A ]T₀))

    _,Σ'_  : ∀{Γ}{A : Ty₀ Γ}{B : Ty₀ (Γ ,₀ A)}(a : Tm₀ Γ A) → Tm₀ Γ (B [ < a >₀ ]T₀) → Tm₀ Γ (Σ' A B)
    proj₁' : ∀{Γ}{A : Ty₀ Γ}{B : Ty₀ (Γ ,₀ A)}(w : Tm₀ Γ (Σ' A B)) → Tm₀ Γ A
    proj₂' : ∀{Γ}{A : Ty₀ Γ}{B : Ty₀ (Γ ,₀ A)}(w : Tm₀ Γ (Σ' A B)) → Tm₀ Γ (B [ < proj₁' w >₀ ]T₀)
    Σβ₁    : ∀{Γ}{A : Ty₀ Γ}{B : Ty₀ (Γ ,₀ A)}{a : Tm₀ Γ A}{b : Tm₀ Γ (B [ < a >₀ ]T₀)}
           → proj₁' (a ,Σ' b) ≡ a
    Σβ₂    : ∀{Γ}{A : Ty₀ Γ}{B : Ty₀ (Γ ,₀ A)}{a : Tm₀ Γ A}{b : Tm₀ Γ (B [ < a >₀ ]T₀)}
           → proj₂' (a ,Σ' b) ≡[ ap (Tm₀ _) (ap (λ z → B [ < z >₀ ]T₀) Σβ₁) ]≡ b
    ,Σ[]   : ∀{Γ}{A : Ty₀ Γ}{B : Ty₀ (Γ ,₀ A)}{a : Tm₀ Γ A}{b : Tm₀ Γ (B [ < a >₀ ]T₀)}
             {Θ}{σ : Tms Θ Γ}
           → (a ,Σ' b) [ σ ]t₀
           ≡[ ap (Tm₀ _) Σ[] ]≡
             (   (a [ σ ]t₀)
             ,Σ' coe (ap (Tm₀ _) ([][]T₀ ◾ ap (_[_]T₀ B) {!!} ◾ [][]T₀ ⁻¹)) (b [ σ ]t₀))
