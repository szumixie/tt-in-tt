{-# OPTIONS --without-K #-}

module TT.TwoLevels.Univ where

open import Agda.Primitive
open import lib
open import TT.TwoLevels.Decl
open import TT.TwoLevels.Core

-- Base type and family

record Univ {d : Decl}(c : Core d) : Set₃ where
  open Decl d
  open Core c

  field
    U : ∀{Γ} → Ty₁ Γ
    U[] : ∀{Γ Δ}{σ : Tms Γ Δ} → (U [ σ ]T₁) ≡ U
    El : ∀{Γ}(Â : Tm₁ Γ U) → Ty₀ Γ
    El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{Â : Tm₁ Δ U}
         → (El Â [ σ ]T₀) ≡ (El (coe (ap (Tm₁ _) U[]) (Â [ σ ]t₁)))
