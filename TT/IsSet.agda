{-# OPTIONS --without-K --no-eta #-}

module TT.IsSet where

open import Agda.Primitive
open import lib
open import TT.Decl

record IsSet {ℓ ℓ'}(d : Decl {ℓ}{ℓ'}) : Set (ℓ ⊔ ℓ') where
  open Decl d
  field
    setC : {Γ Δ : Con}{α β : Γ ≡ Δ} → α ≡ β
    setT : {Γ : Con}{A B : Ty Γ}{α β : A ≡ B} → α ≡ β
    sets : {Γ Δ : Con}{σ ν : Tms Γ Δ}{α β : σ ≡ ν} → α ≡ β
    sett : {Γ : Con}{A : Ty Γ}{t u : Tm Γ A}{α β : t ≡ u} → α ≡ β
