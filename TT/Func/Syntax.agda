{-# OPTIONS --without-K #-}

module TT.Func.Syntax where

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Func

postulate
  syntaxFunc : Func syntaxCore
  
open Func syntaxFunc public
