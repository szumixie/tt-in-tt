module TT.Func.DepModel where

open import Agda.Primitive

open import lib

open import TT.Core.Syntax
open import TT.Func.Syntax

open import TT.Decl.DepModel
open import TT.Core.DepModel

record Funcᴹ {i j}{d : Declᴹ {i}{j}}(c : Coreᴹ d) : Set (i ⊔ j) where
  open Declᴹ d
  open Coreᴹ c
  field
    Πᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A){B : Ty (Γ , A)}
             (Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B) → Tyᴹ Γᴹ (Π A B)

    appᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm Γ (Π A B)} → Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) t → Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ (app t)
    lamᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Γ , A) B} → Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ t → Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) (lam t)

    Π[]ᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}{A : Ty Δ}
             {Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
           → Πᴹ Aᴹ Bᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ= Π[] ]≡ Πᴹ (Aᴹ [ δᴹ ]Tᴹ) (Bᴹ [ δᴹ ^ᴹ Aᴹ ]Tᴹ)

    lam[]ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Δ , A) B}{tᴹ : Tmᴹ (Δᴹ ,Cᴹ Aᴹ) Bᴹ t}
           → (lamᴹ tᴹ) [ δᴹ ]tᴹ ≡[ TmΓᴹ= Π[] Π[]ᴹ lam[] ]≡ lamᴹ (tᴹ [ δᴹ ^ᴹ Aᴹ ]tᴹ)
    Πβᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
             {B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Γ , A) B}{tᴹ : Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ t}
           → appᴹ (lamᴹ tᴹ) ≡[ TmΓAᴹ= Πβ ]≡ tᴹ
    Πηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
             {B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm Γ (Π A B)}{tᴹ : Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) t}
           → lamᴹ (appᴹ tᴹ) ≡[ TmΓAᴹ= Πη ]≡ tᴹ
