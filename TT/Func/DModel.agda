module TT.Func.DModel where

open import Agda.Primitive

open import lib

import TT.Decl.Congr.NoJM
import TT.Core.Congr.NoJM

open import TT.Decl
open import TT.Decl.DModel
open import TT.Core
open import TT.Core.DModel
open import TT.Func

record Funcᴹ {i j i' j'}{d : Decl {i'}{j'}}{dᴹ : Declᴹ {i}{j} d}{c : Core d}(cᴹ : Coreᴹ dᴹ c)(f : Func c) : Set (i ⊔ j ⊔ i' ⊔ j') where
  open Decl d
  open Declᴹ dᴹ
  open Core c
  open Coreᴹ cᴹ
  open Func f
  open TT.Decl.Congr.NoJM d
  open TT.Core.Congr.NoJM c
  field
    Πᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A){B : Ty (Γ , A)}
             (Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B) → Tyᴹ Γᴹ (Π A B)

    appᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm Γ (Π A B)} → Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) t → Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ (app t)
    lamᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Γ , A) B} → Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ t → Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) (lam t)

    Π[]ᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}{A : Ty Δ}
             {Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
           → Πᴹ Aᴹ Bᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ= Π[] ]≡ Πᴹ (Aᴹ [ δᴹ ]Tᴹ) (Bᴹ [ δᴹ ^ᴹ Aᴹ ]Tᴹ)

    lam[]ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Δ , A) B}{tᴹ : Tmᴹ (Δᴹ ,Cᴹ Aᴹ) Bᴹ t}
           → (lamᴹ tᴹ) [ δᴹ ]tᴹ ≡[ TmΓᴹ= Π[] Π[]ᴹ lam[] ]≡ lamᴹ (tᴹ [ δᴹ ^ᴹ Aᴹ ]tᴹ)
    Πβᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
             {B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Γ , A) B}{tᴹ : Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ t}
           → appᴹ (lamᴹ tᴹ) ≡[ TmΓAᴹ= Πβ ]≡ tᴹ
    Πηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
             {B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm Γ (Π A B)}{tᴹ : Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) t}
           → lamᴹ (appᴹ tᴹ) ≡[ TmΓAᴹ= Πη ]≡ tᴹ
