{-# OPTIONS --no-eta #-}

open import TT.Decl
open import TT.Core
open import TT.Func

module TT.Func.Laws.JM {i}{j}{d : Decl {i}{j}}{c : Core d}(f : Func c) where

open import lib
open import JM

open Decl d
open Core c
open Func f
open import TT.Decl.Congr.NoJM d
open import TT.Decl.Congr.JM d
open import TT.Core.Congr.NoJM c
open import TT.Core.Congr.JM c
open import TT.Func.Congr.NoJM f
open import TT.Func.Congr.JM f
open import TT.Core.Laws.JM c
open import TT.Func.Laws.NoJM f

------------------------------------------------------------------------------
-- Tm
------------------------------------------------------------------------------

-- other naturality for Π

abstract
  $[] : ∀{Γ Δ A B}{t : Tm Δ (Π A B)}{u : Tm Δ A}{δ : Tms Γ Δ}
      → (t $ u) [ δ ]t ≃ coe (TmΓ= Π[]) (t [ δ ]t) $ (u [ δ ]t)
  $[] {Γ}{Δ}{A}{B}{t}{u}{δ}

    = [][]t'
    ◾̃ []t≃ refl (to≃ <>∘)
    ◾̃ [][]t' ⁻¹̃
    ◾̃ to≃ (ap (λ z → z [  < u [ δ ]t > ]t) (app[] {δ = δ}{t = t}) ⁻¹)

abstract
  app[,] : ∀{Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm Δ (Π A B)}
           {ρ : Tms Γ Δ}{u : Tm Γ (A [ ρ ]T)}
         → app t [ ρ ,s u ]t ≃ app (coe (TmΓ= Π[]) (t [ ρ ]t)) [ < u > ]t
  app[,] = []t≃'' ,→^
         ◾̃ [][]t' ⁻¹̃
         ◾̃ to≃ ((ap (λ z → z [ < _ > ]t)) (app[] ⁻¹))
