{-# OPTIONS --without-K --no-eta #-}

open import Agda.Primitive
open import TT.Decl
open import TT.Core
open import TT.Func

module TT.Func.Laws.NoJM {i}{j}{d : Decl {i}{j}}{c : Core d}(f : Func c) where

open import lib

open Decl d
open Core c
open Func f
open import TT.Decl.Congr.NoJM d
open import TT.Core.Congr.NoJM c
open import TT.Func.Congr.NoJM f
open import TT.Core.Laws.NoJM c

------------------------------------------------------------------------------
-- Tm
------------------------------------------------------------------------------

-- other naturality for Π

abstract
  app[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm Δ (Π A B)}
        → app (coe (TmΓ= Π[]) (t [ δ ]t)) ≡ (app t) [ δ ^ A ]t
  app[] {Γ}{Δ}{δ}{A}{B}{t}

    = ap (λ z → app (coe (TmΓ= Π[]) (z [ δ ]t))) (Πη ⁻¹)
    ◾ app= lam[]
    ◾ Πβ
