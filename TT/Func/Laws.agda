open import TT.Decl
open import TT.Core
open import TT.Func

module TT.Func.Laws {i}{j}{d : Decl {i}{j}}{c : Core d}(f : Func c) where

open import TT.Func.Laws.NoJM f public
open import TT.Func.Laws.JM f public
