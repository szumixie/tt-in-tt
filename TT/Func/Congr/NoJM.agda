{-# OPTIONS --without-K --no-eta #-}

open import TT.Decl
open import TT.Core
open import TT.Func

module TT.Func.Congr.NoJM {i}{j}{d : Decl {i}{j}}{c : Core d}(f : Func c) where

open import lib

open Decl d
open Core c
open Func f
open import TT.Decl.Congr.NoJM d
open import TT.Core.Congr.NoJM c

-- Ty

Π= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
     {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
     {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≡[ Ty= (,C= Γ₂ A₂) ]≡ B₁)
   → Π A₀ B₀ ≡[ Ty= Γ₂ ]≡ Π A₁ B₁
Π= refl refl refl = refl

Π=' : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}
     (B₂ : B₀ ≡[ Ty= (Γ,C= A₂) ]≡ B₁)
   → Π A₀ B₀ ≡ Π A₁ B₁
Π=' A₂ B₂ = Π= refl A₂ B₂

-- Tm

app= : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t₀ t₁ : Tm Γ (Π A B)} → t₀ ≡ t₁ → app t₀ ≡ app t₁
app= refl = refl
