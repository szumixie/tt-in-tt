open import TT.Decl
open import TT.Core
open import TT.Func

module TT.Func.Congr {i}{j}{d : Decl {i}{j}}{c : Core d}(f : Func c) where

open import TT.Func.Congr.NoJM f public
open import TT.Func.Congr.JM f public
