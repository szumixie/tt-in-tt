module TT.BaseType.Syntax where

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Iden.Syntax
open import TT.BaseType

postulate
  syntaxBaseType : BaseType syntaxDecl syntaxCore

open BaseType syntaxBaseType public
