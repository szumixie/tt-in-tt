{-# OPTIONS #-}

module TT.ExtIden where

open import Agda.Primitive
open import lib
open import JM

open import TT.Decl
open import TT.Core
import TT.Decl.Congr
import TT.Core.Congr
import TT.Core.Laws

-- Extensional identity type

module _ {i j}{d : Decl}(c : Core {i}{j} d) where
  open Decl d
  open TT.Decl.Congr d
  open TT.Core.Congr c
  open TT.Core.Laws c
  open Core c

  record ExtIden : Set (i ⊔ j) where
    field
      Id : {Γ : Con}{A : Ty Γ} → Tm Γ A → Tm Γ A → Ty Γ

      Id[] : ∀{Γ Θ A}{u v : Tm Γ A}{σ : Tms Θ Γ}
           → Id u v [ σ ]T ≡ Id (u [ σ ]t) (v [ σ ]t)

      ref : ∀{Γ A}(u : Tm Γ A) → Tm Γ (Id u u)

      ref[] : ∀{Γ Θ A}{u : Tm Γ A}{σ : Tms Θ Γ}
            → ref u [ σ ]t ≡[ TmΓ= Id[] ]≡ ref (u [ σ ]t)

      reflect : ∀{Γ A}{u v : Tm Γ A} → Tm Γ (Id u v) → u ≡ v

    transp : ∀{Γ A}(P : Ty (Γ , A)){u v : Tm Γ A}(w : Tm Γ (Id u v)) →
      Tm Γ (P [ < u > ]T) → Tm Γ (P [ < v > ]T)
    transp {Γ} P w = transport (λ z → Tm Γ (P [ < z > ]T)) (reflect w)

    cong : ∀{Γ A B}(f : Tm (Γ , A) (B [ wk ]T)){u v : Tm Γ A}(e : Tm Γ (Id u v))
       → Tm Γ (Id (transport (Tm Γ) ([][]T ◾ ap (B [_]T) (π₁id∘ ◾ π₁β) ◾ [id]T) (f [ < u > ]t))
                  (transport (Tm Γ) ([][]T ◾ ap (B [_]T) (π₁id∘ ◾ π₁β) ◾ [id]T) (f [ < v > ]t)))
    cong {Γ}{A}{B} f {u}{v} e = transport (λ v → Tm Γ (Id (transport (Tm Γ) ([][]T ◾ ap (_[_]T B) (π₁id∘ ◾ π₁β) ◾ [id]T) (f [ < u > ]t)) (transport (Tm Γ) ([][]T ◾ ap (_[_]T B) (π₁id∘ ◾ π₁β) ◾ [id]T) (f [ < v > ]t))))
                                          (reflect e)
                                          (ref _)

    congd : ∀{Γ A B}(f : Tm (Γ , A) B){u v : Tm Γ A}(e : Tm Γ (Id u v)) →
      Tm Γ (Id {Γ}{B [ < v > ]T}
               (transp B e (f [ < u > ]t))
               (f [ < v > ]t))
    congd {Γ}{A}{B} f {u}{v} e = J (λ {v} reflecte → Tm Γ (Id (coe (ap (λ z → Tm Γ (B [ id ,s coe (Tm= refl ([id]T ⁻¹)) z ]T)) reflecte) (f [ id ,s coe (Tm= refl ([id]T ⁻¹)) u ]t)) (f [ id ,s coe (Tm= refl ([id]T ⁻¹)) v ]t)))
                                   (ref _)
                                   (reflect e)

    reflect⁻¹ : ∀{Γ A}{u v : Tm Γ A} → u ≡ v → Tm Γ (Id u v)
    reflect⁻¹ refl = ref _

    -- we can prove this if the syntax is a set (or we have UIP in the metatheory):
    -- reflectβ : ∀{Γ A}{u v : Tm Γ A}{p : u ≡ v} → reflect (reflect⁻¹ p) ≡ p
    -- reflectβ {p = refl} = {!!}

    -- we can prove this if we have UIP for Id (which we don't assume
    -- above). reflect and J imply UIP but reflect and transport probably don't
    -- reflectη : ∀{Γ A}{u v : Tm Γ A}{t : Tm Γ (Id u v)} → reflect⁻¹ (reflect t) ≡ t
    -- reflectη {t = t} = {!!}
