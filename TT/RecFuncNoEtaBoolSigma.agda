{-# OPTIONS --rewriting #-}

open import TT.Decl
open import TT.Decl.Congr.NoJM
open import TT.Core
open import TT.Core.Congr.NoJM
open import TT.FuncNoEta
open import TT.Boolean
open import TT.Sigma

module TT.RecFuncNoEtaBoolSigma
  {i j}
  {d : Decl}
  {c : Core {i}{j} d}
  (f : FuncNoEta c)
  (b : Boolean c)
  (s : Sigma c)
  where

open import lib

open import TT.Decl.Syntax as SD
open import TT.Core.Syntax as SC
open import TT.FuncNoEta.Syntax as SF
open import TT.Boolean.Syntax as SB
open import TT.Sigma.Syntax as SS

private module D = Decl d
private module C = Core c
private module F = FuncNoEta f
private module B = Boolean b
private module S = Sigma s

postulate
  RecCon : SD.Con → D.Con
  RecTy  : ∀{Γ}(A : SD.Ty Γ) → D.Ty (RecCon Γ)
  RecTms : ∀{Γ Δ}(δ : SD.Tms Γ Δ) → D.Tms (RecCon Γ) (RecCon Δ)
  RecTm  : ∀{Γ}{A : SD.Ty Γ}(t : SD.Tm Γ A) → D.Tm (RecCon Γ) (RecTy A)

-- Core

postulate
  β• : RecCon SC.• ≡ C.•
  β, : ∀{Γ A} → (RecCon (Γ SC., A)) ≡ ((RecCon Γ) C., (RecTy A))

{-# REWRITE β• #-}
{-# REWRITE β, #-}

postulate
  β[]T   : ∀{Γ Δ}{A : SD.Ty Δ}{σ : SD.Tms Γ Δ}
         → (RecTy (A SC.[ σ ]T)) ≡ (RecTy A C.[ RecTms σ ]T)

{-# REWRITE β[]T #-}

postulate
  βid : ∀{Γ} → (RecTms (SC.id {Γ})) ≡ C.id
  β∘  : ∀{Γ Δ Σ}{σ : SD.Tms Δ Σ}{ν : SC.Tms Γ Δ}
      → (RecTms (σ SC.∘ ν)) ≡ (RecTms σ C.∘ RecTms ν)
  βε  : ∀{Γ} → (RecTms (SC.ε {Γ})) ≡ C.ε
  β,s : ∀{Γ Δ}{σ : SD.Tms Γ Δ}{A : SD.Ty Δ}{t : SD.Tm Γ (A SC.[ σ ]T)}
      → (RecTms (σ SC.,s t)) ≡ (RecTms σ C.,s RecTm t)
  βπ₁ : ∀{Γ Δ}{A : SD.Ty Δ}{σ : SD.Tms Γ (Δ SC., A)}
      → (RecTms (SC.π₁ σ)) ≡ (C.π₁ (RecTms σ))

{-# REWRITE βid #-}
{-# REWRITE β∘  #-}
{-# REWRITE βε  #-}
{-# REWRITE β,s #-}
{-# REWRITE βπ₁ #-}

postulate
  β[]t : ∀{Γ Δ}{A : SD.Ty Δ}{t : SD.Tm Δ A}{σ : SD.Tms Γ Δ}
       → (RecTm (t SC.[ σ ]t)) ≡ (RecTm t C.[ RecTms σ ]t)
  βπ₂  : ∀{Γ Δ}{A : SD.Ty Δ}{σ : SD.Tms Γ (Δ SC., A)}
       → (RecTm (SC.π₂ σ)) ≡ (C.π₂ (RecTms σ))

{-# REWRITE β[]t #-}
{-# REWRITE βπ₂  #-}

postulate
  β[][]T
    : ∀{Γ Δ Σ}{A : SD.Ty Σ}{σ : SD.Tms Γ Δ}{δ : SD.Tms Δ Σ}
    → ap RecTy (SC.[][]T {Γ}{Δ}{Σ}{A}{σ}{δ}) ≡ C.[][]T

  β[id]T : ∀{Γ}{A : Ty Γ} → ap RecTy (SC.[id]T {Γ}{A}) ≡ C.[id]T


{-# REWRITE β[][]T #-}
{-# REWRITE β[id]T #-}

-- FuncNoEta

postulate
  βΠ : ∀{Γ}{A : SD.Ty Γ}{B : SD.Ty (Γ SC., A)}
     → (RecTy (SF.Π A B)) ≡ (F.Π (RecTy A) (RecTy B))

{-# REWRITE βΠ #-}

postulate
  βlam : ∀{Γ}{A : SD.Ty Γ}{B : SD.Ty (Γ SC., A)}{t : SD.Tm (Γ SC., A) B}
       → (RecTm (SF.lam t)) ≡ (F.lam (RecTm t))
  βapp : ∀{Γ}{A : SD.Ty Γ}{B : SD.Ty (Γ SC., A)}{t : SD.Tm Γ (SF.Π A B)}
       → (RecTm (SF.app t)) ≡ (F.app (RecTm t))

{-# REWRITE βlam #-}
{-# REWRITE βapp #-}

-- Boolean

postulate
  βBool : {Γ : SD.Con} → RecTy (SB.Bool' {Γ}) ≡ B.Bool' {RecCon Γ}

{-# REWRITE βBool #-}

postulate
  βtrue  : {Γ : SD.Con} → RecTm (SB.true' {Γ}) ≡ B.true'
  βfalse : {Γ : SD.Con} → RecTm (SB.false' {Γ}) ≡ B.false'

{-# REWRITE βtrue #-}
{-# REWRITE βfalse #-}

RecTmcoe
  : {Γ : SD.Con}{A₀ A₁ : SD.Ty Γ}(A₂ : A₀ ≡ A₁){t : SD.Tm Γ A₁}
  → RecTm (coe (TmΓ= syntaxDecl (A₂ ⁻¹)) t) ≡ coe (TmΓ= d (ap RecTy A₂ ⁻¹)) (RecTm t)
RecTmcoe refl = refl

postulate
  βifthenelse
    : {Γ : SD.Con}{C : SD.Ty (Γ SC., SB.Bool')}
      {t : SD.Tm Γ SB.Bool'}
      {u : SD.Tm Γ (C SC.[ SC.< SB.true' > ]T)}
      {v : SD.Tm Γ (C SC.[ SC.< SB.false' > ]T)}
    → RecTm (SB.if' t then u else v)
      ≡ coe (TmΓ= d (ap (λ z → RecTy C C.[ C.id C.,s z ]T) (RecTmcoe [id]T {t} ⁻¹)))
          (B.if'_then_else_ {C = RecTy C}
             (RecTm t)
             (coe (TmΓ= d (ap (λ z → RecTy C C.[ C.id C.,s z ]T) (RecTmcoe [id]T {true'  {Γ}}))) (RecTm u))
             (coe (TmΓ= d (ap (λ z → RecTy C C.[ C.id C.,s z ]T) (RecTmcoe [id]T {false' {Γ}}))) (RecTm v)))

{-# REWRITE βifthenelse #-}

-- Sigma

postulate
  βΣ : ∀{Γ}{A : SD.Ty Γ}{B : SD.Ty (Γ SC., A)}
     → (RecTy (SS.Σ' A B)) ≡ (S.Σ' (RecTy A) (RecTy B))

{-# REWRITE βΣ #-}

postulate
  β,Σ'
    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{u : Tm Γ A}{v : Tm Γ (B [ < u > ]T)}
    → RecTm (u SS.,Σ' v) ≡ RecTm u S.,Σ' coe (TmΓ= d (ap (λ z → RecTy B C.[ C.id C.,s z ]T) (RecTmcoe [id]T {u}))) (RecTm v)
  βproj₁'
    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}(w : Tm Γ (Σ' A B))
    → RecTm (SS.proj₁' w) ≡ S.proj₁' (RecTm w)

{-# REWRITE β,Σ' #-}
{-# REWRITE βproj₁' #-}

postulate
  βproj₂'
    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}(w : Tm Γ (Σ' A B))
    → RecTm (SS.proj₂' w) ≡  coe (TmΓ= d (ap (λ z → RecTy B C.[ C.id C.,s z ]T) (RecTmcoe [id]T {proj₁' w} ⁻¹))) (S.proj₂' (RecTm w))
