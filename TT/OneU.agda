{-# OPTIONS --without-K --no-eta #-}

module TT.OneU where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
open import TT.Base
import TT.Decl.Congr.NoJM
import TT.Base.Congr.NoJM

record OneU {i j}{d : Decl}{c : Core {i}{j} d}(b : Base c) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr.NoJM d
  open Core c
  open Base b
  open TT.Base.Congr.NoJM b

  field
    Unit   : ∀{Γ} → Tm Γ U
    Unit[] : ∀{Γ Θ}{σ : Tms Γ Θ} → Unit [ σ ]t ≡[ TmΓ= U[] ]≡ Unit
    *      : ∀{Γ} → Tm Γ (El Unit)
    *[]    : ∀{Γ Θ}{σ : Tms Γ Θ} → * [ σ ]t ≡[ TmΓ= (El[]'' Unit[]) ]≡ *
