module TT.Restriction where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
import TT.Decl.Congr.NoJM
import TT.Core.Congr.NoJM

record Restriction {i j}{d : Decl}(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr.NoJM d
  open Core c
  open TT.Core.Congr.NoJM c

  field
    Restr : {Γ : Con}(A : Ty Γ){B : Ty (Γ , A)}(u v : Tm (Γ , A) B) → Ty Γ
    mkRestr : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{u v : Tm (Γ , A) B}(t : Tm Γ A)
      (e : u [ < t > ]t ≡ v [ < t > ]t) → Tm Γ (Restr A u v)
    unRestr : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{u v : Tm (Γ , A) B}(w : Tm Γ (Restr A u v)) →
      Tm Γ A
    eqRestr : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{u v : Tm (Γ , A) B}(w : Tm Γ (Restr A u v)) →
      u [ < unRestr w > ]t ≡ v [ < unRestr w > ]t
    Restrβun : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{u v : Tm (Γ , A) B}
      {t : Tm Γ A}{e : u [ < t > ]t ≡ v [ < t > ]t} →
      unRestr (mkRestr t e) ≡ t
    Restrβeq : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{u v : Tm (Γ , A) B}
      {t : Tm Γ A}{e : u [ < t > ]t ≡ v [ < t > ]t} →
      eqRestr (mkRestr t e) ≡[ ap (λ z → u [ < z > ]t ≡ v [ < z > ]t) Restrβun ]≡ e
    Restrη : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{u v : Tm (Γ , A) B}{w : Tm Γ (Restr A u v)} →
      mkRestr (unRestr w) (eqRestr w) ≡ w

    Restr[] : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{u v : Tm (Γ , A) B}{Θ : Con}{σ : Tms Θ Γ} →
      Restr A u v [ σ ]T ≡ Restr (A [ σ ]T) (u [ σ ^ A ]t) (v [ σ ^ A ]t)
--    mkRestr[] : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{u v : Tm (Γ , A) B}{t : Tm Γ A}
--      {e : u [ < t > ]t ≡ v [ < t > ]t}{Θ : Con}{σ : Tms Θ Γ} →
--      mkRestr t e [ σ ]t ≡[ ap (Tm Θ) Restr[] ]≡ mkRestr (t [ σ ]t) {!ap _[ σ ]t e!}
