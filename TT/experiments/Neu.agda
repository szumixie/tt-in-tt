{-# OPTIONS --rewriting --without-K #-}

module TT.experiments.Neu where

open import lib

infixl 5 _▷_
infixl 5 _,_

data Con : Set
data Ty : Con → Set
data Var : (Γ : Con) → Ty Γ → Set
data Neu : (Γ : Con) → Ty Γ → Set

data Con where
  •   : Con
  _▷_ : (Γ : Con)(A : Ty Γ) → Con

data Ty where
  U  : {Γ : Con} → Ty Γ
  El : {Γ : Con}(a : Neu Γ U) → Ty Γ
  Π  : {Γ : Con}(a : Neu Γ U)(B : Ty (Γ ▷ El a)) → Ty Γ

wkT : {Γ : Con} → Ty Γ → {B : Ty Γ} → Ty (Γ ▷ B)
wkV : {Γ : Con}{A : Ty Γ} → Var Γ A → {C : Ty Γ} → Var (Γ ▷ C) (wkT A)
wkN : {Γ : Con}{A : Ty Γ} → Neu Γ A → {C : Ty Γ} → Neu (Γ ▷ C) (wkT A)

wkT U = U
wkT (El a) = El (wkN a)
wkT (Π a B) = Π (wkN a) {!B!}

data Var where
  vz : {Γ : Con}{A : Ty Γ} → Var (Γ ▷ A) (wkT A)
  vs : {Γ : Con}{A B : Ty Γ} → Var Γ A → Var (Γ ▷ B) (wkT A)

wkV vz = vs vz
wkV (vs x) = vs (wkV x)

data Neu where
  var : {Γ : Con}{A : Ty Γ} → Var Γ A → Neu Γ A

wkN (var x) = var (wkV x)


data Neus : Con → Con → Set
_[_]T : {Δ : Con}(A : Ty Δ){Γ : Con}(σ : Neus Γ Δ) → Ty Γ
_[_]V : {Δ : Con}{A : Ty Δ}(x : Var Δ A){Γ : Con}(σ : Neus Γ Δ) → Neu Γ (A [ σ ]T)
_[_]N : {Δ : Con}{A : Ty Δ}(n : Neu Δ A){Γ : Con}(σ : Neus Γ Δ) → Neu Γ (A [ σ ]T)
_^_ : {Γ Δ : Con}(σ : Neus Γ Δ)(A : Ty Δ) → Neus (Γ ▷ A [ σ ]T) (Δ ▷ A)

data Neus where
  ε : {Γ : Con} → Neus Γ •
  _,_ : {Γ Δ : Con}(σ : Neus Γ Δ){A : Ty Δ}(n : Neu Γ (A [ σ ]T)) → Neus Γ (Δ ▷ A)

id : {Γ : Con} → Neus Γ Γ
id {•} = ε
id {Γ ▷ A} = {!id {Γ}!}

wkNs : {Γ Δ : Con}(σ : Neus Γ Δ){A : Ty Γ} → Neus (Γ ▷ A) Δ
wkNs ε = ε
wkNs (σ , n) = wkNs σ , {!!}

U [ σ ]T = U
El a [ σ ]T = El (a [ σ ]N)
Π a A [ σ ]T = Π (a [ σ ]N) {!!}

vz [ σ , n ]V = {!n!}
vs x [ σ , n ]V = {!x [ σ ]V!}

var x [ σ ]N = x [ σ ]V

ε ^ A = ε , {!vz!}
(σ , x) ^ A = {!!}
