{-# OPTIONS --without-K --no-eta #-}

module TT.experiments.Core1.Syntax where

open import TT.Decl.Syntax public
open import TT.experiments.Core1

postulate
  syntaxCore1 : Core1 syntaxDecl
  
open Core1 syntaxCore1 public
