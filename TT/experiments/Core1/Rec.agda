{-# OPTIONS --without-K --rewriting #-}

open import TT.Decl
open import TT.experiments.Core1

module TT.experiments.Core1.Rec {i j}{d : Decl}(c : Core {i}{j} d) where

open import lib

open import TT.experiments.Core1.Syntax as S

private module D = Decl d
private module C = Core c

postulate
  RecCon : S.Con → D.Con
  RecTy  : ∀{Γ}(A : S.Ty Γ) → D.Ty (RecCon Γ)
  RecTms : ∀{Γ Δ}(δ : S.Tms Γ Δ) → D.Tms (RecCon Γ) (RecCon Δ)
  RecTm  : ∀{Γ}{A : S.Ty Γ}(t : S.Tm Γ A) → D.Tm (RecCon Γ) (RecTy A)

postulate
  β• : RecCon S.• ≡ Core.• c
  β, : ∀{Γ A} → (RecCon (Γ S., A)) ≡ ((RecCon Γ) C., (RecTy A))

{-# REWRITE β• #-}
{-# REWRITE β, #-}

postulate
  β[]T   : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ Δ}
         → (RecTy (A S.[ σ ]T)) ≡ (RecTy A C.[ RecTms σ ]T)

{-# REWRITE β[]T #-}

postulate
  βε  : ∀{Γ} → (RecTms (S.ε {Γ})) ≡ C.ε
  β,s : ∀{Γ Δ}{σ : S.Tms Γ Δ}{A : S.Ty Δ}{t : S.Tm Γ (A S.[ σ ]T)}
      → (RecTms (σ S.,s t)) ≡ (RecTms σ C.,s RecTm t)
  βπ₁ : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ (Δ S., A)}
      → (RecTms (S.π₁ σ)) ≡ (C.π₁ (RecTms σ))

{-# REWRITE βε  #-}
{-# REWRITE β,s #-}
{-# REWRITE βπ₁ #-}

postulate
  β[]t : ∀{Γ Δ}{A : S.Ty Δ}{t : S.Tm Δ A}{σ : S.Tms Γ Δ}
       → (RecTm (t S.[ σ ]t)) ≡ (RecTm t C.[ RecTms σ ]t)
  βπ₂  : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ (Δ S., A)}
       → (RecTm (S.π₂ σ)) ≡ (C.π₂ (RecTms σ))

{-# REWRITE β[]t #-}
{-# REWRITE βπ₂  #-}
