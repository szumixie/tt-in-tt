module TT.experiments.IR1 where

open import lib

data Con : Set
data Ty : Con → Set
data Tms : Con → Con → Set
data Tm : (Γ : Con) → Ty Γ → Set

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con) → Ty Γ → Con

data Ty where
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ


data Tm where
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) 
  π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infixl 8 _[_]t


id : ∀{Γ} → Tms Γ Γ
wks : {Γ Δ : Con}{B : Ty Γ} → Tms Γ Δ → Tms (Γ , B) Δ
wkt : {Γ : Con}{A : Ty Γ}{B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wks id ]T)

wks {Γ} {.•} {B} ε = ε
wks {Γ} {.(_ , _)} {B} (σ ,s t) = wks σ ,s {!wkt t!}
wks {Γ} {Δ} {B} (π₁ σ) = π₁ (wks σ)

wkt = {!!}

id {•} = ε
id {Γ , A} = wks (id {Γ}) ,s {!!}

{-

id {•} = ε
id {Γ , A} = π₁ (id {Γ , A}) ,s π₂ (id {Γ , A})

_∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
_∘_ = {!!}

infix  6 _∘_
-}
