{-# OPTIONS --rewriting --without-K #-}

module TT.experiments.Neu6 where

-- only variables and it works

open import lib

infixl 5 _▷_

data Con : Set
data Ty : Con → Set
data Var : (Γ : Con) → Ty Γ → Set
data Neu : (Γ : Con) → Ty Γ → Set

data Con where
  •   : Con
  _▷_ : (Γ : Con)(A : Ty Γ) → Con

data Ty where
  U  : {Γ : Con} → Ty Γ
  El : {Γ : Con}(a : Neu Γ U) → Ty Γ

wkT : {Γ : Con} → Ty Γ → {B : Ty Γ} → Ty (Γ ▷ B)
wkV : {Γ : Con}{A : Ty Γ} → Var Γ A → {C : Ty Γ} → Var (Γ ▷ C) (wkT A)
wkN : {Γ : Con}{A : Ty Γ} → Neu Γ A → {C : Ty Γ} → Neu (Γ ▷ C) (wkT A)

wkT U = U
wkT (El a) = El (wkN a)

data Var where
  vz : {Γ : Con}{A : Ty Γ} → Var (Γ ▷ A) (wkT A)
  vs : {Γ : Con}{A B : Ty Γ} → Var Γ A → Var (Γ ▷ B) (wkT A)

wkV vz = vs vz
wkV (vs x) = vs (wkV x)

data Neu where
  var : {Γ : Con}{A : Ty Γ} → Var Γ A → Neu Γ A

wkN (var x) = var (wkV x)
