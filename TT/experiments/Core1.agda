{-# OPTIONS --without-K --no-eta #-}

module TT.experiments.Core1 where

open import Agda.Primitive
open import lib
open import TT.Decl
import TT.Decl.Congr.NoJM

-- Core substitution calculus

module _ {i j}(d : Decl {i}{j}) where
  open Decl d
  open TT.Decl.Congr.NoJM d

  record Core1 : Set (i ⊔ j) where
    field
      •     : Con  -- \bub
      _,_   : (Γ : Con) → Ty Γ → Con
      
      _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

      ε     : ∀{Γ} → Tms Γ •
      _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
      π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ

      _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) 
      π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)
      
    infixl 5 _,_
    infixl 7 _[_]T
    infixl 5 _,s_
    infixl 8 _[_]t

  module _ (c1 : Core1) where
    open Core1 c1

    []T=′ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
           {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
           {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ σ₁)
         → A₀ [ σ₀ ]T ≡[ Ty= Γ₂ ]≡ A₁ [ σ₁ ]T
    []T=′ refl refl refl refl = refl

    record Core2 : Set (i ⊔ j) where
      field
        π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
              → (π₁ (δ ,s a)) ≡ δ
        πη    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
              → (π₁ δ ,s π₂ δ) ≡ δ
        εη    : ∀{Γ}{σ : Tms Γ •}
              → σ ≡ ε
        
        π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
              → π₂ (δ ,s a) ≡[ TmΓ= ([]T=′ refl refl refl π₁β) ]≡ a

  record Core : Set (i ⊔ j) where
    field
      c1 : Core1
      c2 : Core2 c1

    open Core1 c1 public
    open Core2 c2 public
