{-# OPTIONS --rewriting --without-K #-}

module TT.experiments.Neu4 where

open import lib

infixl 5 _▷_
infix  6 _++_

data Con : Set
data Ty : Con → Set

data Con where
  •   : Con
  _▷_ : (Γ : Con)(A : Ty Γ) → Con

data Var : (Γ : Con) → Ty Γ → Set
data Neu : (Γ : Con) → Ty Γ → Set

data Ty where
  U  : {Γ : Con} → Ty Γ
  El : {Γ : Con}(a : Neu Γ U) → Ty Γ
  Π  : {Γ : Con}(a : Neu Γ U)(B : Ty (Γ ▷ El a)) → Ty Γ

data Tel : Con → Set
_++_ : (Γ : Con)(Ω : Tel Γ) → Con

data Tel where
  •   : {Γ : Con} → Tel Γ
  _▷_ : {Γ : Con}(Ω : Tel Γ)(A : Ty (Γ ++ Ω)) → Tel Γ

Γ ++ • = Γ
Γ ++ (Ω ▷ A) = (Γ ++ Ω) ▷ A

wkTel : {Γ : Con}(Ω : Tel Γ){C : Ty Γ} → Tel (Γ ▷ C)
insT : {Γ : Con}(Ω : Tel Γ)(A : Ty (Γ ++ Ω)){C : Ty Γ} → Ty ((Γ ▷ C) ++ wkTel Ω)
insV : {Γ : Con}(Ω : Tel Γ){A : Ty (Γ ++ Ω)}(n : Var (Γ ++ Ω) A){C : Ty Γ} → Var ((Γ ▷ C) ++ wkTel Ω) (insT Ω A)
insN : {Γ : Con}(Ω : Tel Γ){A : Ty (Γ ++ Ω)}(n : Neu (Γ ++ Ω) A){C : Ty Γ} → Neu ((Γ ▷ C) ++ wkTel Ω) (insT Ω A)

wkTel • = •
wkTel (Ω ▷ A){C} = wkTel Ω ▷ insT Ω A

insT Ω U = U
insT Ω (El a) = El (insN Ω a)
insT Ω (Π a B) = Π (insN Ω a) (insT (Ω ▷ El a) B)

data Var where
  vz : {Γ : Con}{A : Ty Γ}(Ω : Tel (Γ ▷ A)) → Var ((Γ ▷ A) ++ Ω) {!insT Ω A!}

insV = {!!}

data Neu where
  var : {Γ : Con}{A : Ty Γ} → Var Γ A → Neu Γ A
--  app : {Γ : Con}{a : Tm Γ U}{B : Ty (Γ ▷ El a)}(t : Neu Γ (Π a B))(u : Neu Γ (El a)) → Neu Γ ()

insN Ω (var x) = var (insV Ω x)

{-
wkT : {Γ : Con} → Ty Γ → {B : Ty Γ} → Ty (Γ ▷ B)
wkV : {Γ : Con}{A : Ty Γ} → Var Γ A → {C : Ty Γ} → Var (Γ ▷ C) (wkT A)
wkN : {Γ : Con}{A : Ty Γ} → Neu Γ A → {C : Ty Γ} → Neu (Γ ▷ C) (wkT A)

wkT U = U
wkT (El a) = El (wkN a)
wkT (Π a B) = Π (wkN a) {!B!}


wkV vz = vs vz
wkV (vs x) = vs (wkV x)

wkN (var x) = var (wkV x)


data Neus : Con → Con → Set
_[_]T : {Δ : Con}(A : Ty Δ){Γ : Con}(σ : Neus Γ Δ) → Ty Γ
_[_]V : {Δ : Con}{A : Ty Δ}(x : Var Δ A){Γ : Con}(σ : Neus Γ Δ) → Neu Γ (A [ σ ]T)
_[_]N : {Δ : Con}{A : Ty Δ}(n : Neu Δ A){Γ : Con}(σ : Neus Γ Δ) → Neu Γ (A [ σ ]T)
_^_ : {Γ Δ : Con}(σ : Neus Γ Δ)(A : Ty Δ) → Neus (Γ ▷ A [ σ ]T) (Δ ▷ A)

data Neus where
  ε : {Γ : Con} → Neus Γ •
  _,_ : {Γ Δ : Con}(σ : Neus Γ Δ){A : Ty Δ}(n : Neu Γ (A [ σ ]T)) → Neus Γ (Δ ▷ A)

id : {Γ : Con} → Neus Γ Γ
id {•} = ε
id {Γ ▷ A} = {!id {Γ}!}

wkNs : {Γ Δ : Con}(σ : Neus Γ Δ){A : Ty Γ} → Neus (Γ ▷ A) Δ
wkNs ε = ε
wkNs (σ , n) = wkNs σ , {!!}

U [ σ ]T = U
El a [ σ ]T = El (a [ σ ]N)
Π a A [ σ ]T = Π (a [ σ ]N) {!!}

vz [ σ , n ]V = {!n!}
vs x [ σ , n ]V = {!x [ σ ]V!}

var x [ σ ]N = x [ σ ]V

ε ^ A = ε , {!vz!}
(σ , x) ^ A = {!!}
-}
