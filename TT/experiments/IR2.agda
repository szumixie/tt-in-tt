module TT.experiments.IR2 where

open import lib

data Con : Set
data Ty : Con → Set
data Tm : (Γ : Con) → Ty Γ → Set
data Var : (Γ : Con) → Ty Γ → Set

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con) → Ty Γ → Con

data Ty where
  wkT   : ∀{Γ B} → Ty Γ → Ty (Γ , B)
  U     : ∀{Γ} → Ty Γ
  El    : ∀{Γ} → Tm Γ U → Ty Γ

data Var where
  zero  : ∀{Γ A} → Var (Γ , A) (wkT A)
  suc   : ∀{Γ A B} → Var Γ A → Var (Γ , B) (wkT A)

data Tm where
  var   : ∀{Γ A} → Var Γ A → Tm Γ A

postulate
  wkU : ∀{Γ A} → wkT {Γ}{A} U ≡ U


wkt : ∀{Γ A B} → Tm Γ A → Tm (Γ , B) (wkT A)
wkt (var x) = var (suc x)


data Tms : Con → Con → Set
_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
_[_]v : ∀{Γ Δ}{A : Ty Δ} → Var Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)

data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)

wkT A [ σ ,s t ]T = A [ σ ]T
U [ σ ]T = U
El t [ σ ]T = El (t [ σ ]t)

var x [ σ ]t = x [ σ ]v

zero [ σ ]v = {!!}
suc x [ σ ]v = {!!}

{-
data Tms : Con → Con → Set
data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infixl 8 _[_]t
-}

--  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) 

--  
--  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ
--   π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)

{-

id : ∀{Γ} → Tms Γ Γ
wks : {Γ Δ : Con}{B : Ty Γ} → Tms Γ Δ → Tms (Γ , B) Δ
wkt : {Γ : Con}{A : Ty Γ}{B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wks id ]T)

wks {Γ} {.•} {B} ε = ε
wks {Γ} {.(_ , _)} {B} (σ ,s t) = wks σ ,s {!wkt t!}
wks {Γ} {Δ} {B} (π₁ σ) = π₁ (wks σ)

wkt = {!!}

id {•} = ε
id {Γ , A} = wks (id {Γ}) ,s {!!}

{-

id {•} = ε
id {Γ , A} = π₁ (id {Γ , A}) ,s π₂ (id {Γ , A})

_∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
_∘_ = {!!}

infix  6 _∘_
-}
-}
