{-# OPTIONS --without-K --type-in-type #-}

module TTEscardoStyle where

open import library

data Con : Set
data Ty  : Con → Set
data Tm  : (Γ : Con) → Ty Γ → Set
data _⇒_ : Con → Con → Set
infixr 3 _⇒_

data Con where
  ∅   : Con
  _,_ : (Γ : Con)(A : Ty Γ) → Con
infixl 6 _,_

data Ty where
  _[_]T : {Δ Γ : Con}(A : Ty Γ)(σ : Δ ⇒ Γ) → Ty Δ
  Π     : {Γ : Con}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
  U     : {Γ : Con} → Ty Γ
  El    : {Γ : Con} → Tm Γ U → Ty Γ

data _⇒_ where
  ε   : {Γ : Con} → Γ ⇒ ∅
  id  : {Γ : Con} → Γ ⇒ Γ
  _∘_ : {Γ Δ Θ : Con}(σ : Δ ⇒ Θ)(ρ : Γ ⇒ Δ) → Γ ⇒ Θ
  _,_ : {Δ Γ : Con}{A : Ty Γ}(σ : Δ ⇒ Γ)(u : Tm Δ (A [ σ ]T)) → Δ ⇒ Γ , A
  p   : {Γ : Con}{A : Ty Γ} → (Γ , A) ⇒ Γ
infixl 6 _∘_

data Tm where
  --     Γ ⊢ A
  -- ----------------
  -- Γ ⊢ A ≡ A [ id ]     Γ ⊢ t : A
  -- ------------------------------
  --        Γ ⊢ t : A [ id ]
  T1 : {Γ : Con}{A : Ty Γ}(t : Tm Γ A) → Tm Γ (A [ id ]T)

  -- Γ ⊢ A     ρ : Δ ⇒ Γ     σ : Θ ⇒ Δ
  -- ---------------------------------
  --  Θ ⊢ A [ ρ ] [ σ ] ≡ A [ ρ ∘ σ ]      Θ ⊢ t : A [ ρ ] [ σ ]
  -- -----------------------------------------------------------
  --                   Θ ⊢ t : A [ ρ ∘ σ ]
  T2 : {Θ Δ Γ : Con}{A : Ty Γ}{ρ : Δ ⇒ Γ}{σ : Θ ⇒ Δ}
       (t : Tm Θ (A [ ρ ]T [ σ ]T)) → Tm Θ (A [ ρ ∘ σ ]T)

  T3 : {Δ Γ : Con}{ρ : Δ ⇒ Γ} → Tm Δ (U [ ρ ]T) → Tm Δ U

  T4 : {Θ Δ Γ : Con}{A : Ty Γ}{ρ : Δ ⇒ Γ}{σ : Θ ⇒ Δ}
       (t : Tm Θ (A [ ρ ∘ σ ]T)) → Tm Θ (A [ ρ ]T [ σ ]T)

  T5 : {Ω Θ Δ Γ : Con}{A : Ty Γ}{ρ : Δ ⇒ Γ}{σ : Θ ⇒ Δ}{ν : Ω ⇒ Θ}
       (t : Tm Ω (A [ ρ ∘ (σ ∘ ν) ]T)) → Tm Ω (A [ (ρ ∘ σ) ∘ ν ]T)

  T6 : {Θ Δ Γ : Con}{A : Ty Γ}{B : Ty Δ}{ρ : Δ ⇒ Γ}{σ : Θ ⇒ Δ}{u : Tm Θ (B [ σ ]T)}
       (t : Tm Θ (A [ ρ ∘ σ ]T)) → Tm Θ (A [ ρ ∘ (p ∘ (σ , u)) ]T)

  T7 : {Δ Γ : Con}{A : Ty Γ}{σ : Δ ⇒ Γ} → Tm Δ (A [ σ ]T) → Tm Δ (A [ σ ∘ id ]T)

  T8 : {Θ Δ Γ : Con}{A : Ty Γ}{ρ : Δ ⇒ Γ}{σ : Θ ⇒ Δ}
       (t : Tm Θ (A [ ρ ]T [ σ ]T)) → Tm Θ (A [ ρ ∘ σ ]T)

  _[_] : {Δ Γ : Con}{A : Ty Γ}(t : Tm Γ A)(σ : Δ ⇒ Γ) → Tm Δ (A [ σ ]T)
  q    : {Γ : Con}{A : Ty Γ} → Tm (Γ , A) (A [ p ]T)
  lam  : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm (Γ , A) B)
       → Tm Γ (Π A B)
  app  : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}(f : Tm Γ (Π A B))(u : Tm Γ A)
       → Tm Γ (B [ id , T1 u ]T)

  Π̂    : {Γ : Con}(Â : Tm Γ U)(B̂ : Tm (Γ , El Â) U) → Tm Γ U
  λ̂    : {Γ : Con}{Â : Tm Γ U}{B̂ : Tm (Γ , El Â) U}
         (t : Tm (Γ , El Â) (El B̂)) → Tm Γ (El (Π̂ Â B̂))
  _$_  : {Γ : Con}{Â : Tm Γ U}{B̂ : Tm (Γ , El Â) U}
         (f : Tm Γ (El (Π̂ Â B̂)))(u : Tm Γ (El Â))
       → Tm Γ (El (T3 (B̂ [ id , T1 u ])))
  Û    : {Γ : Con} → Tm Γ U

{-
⟦_⟧Con : (Γ : Con) → Set
⟦_⟧⇒   : {Δ Γ : Con}(σ : Δ ⇒ Γ)(δ : ⟦ Δ ⟧Con) → ⟦ Γ ⟧Con
-- ⟦_⟧Ty  : {Γ : Con}(A : Ty Γ)(γ : ⟦ Γ ⟧Con) → Set
-- ⟦_⟧Tm  : {Γ : Con}{A : Ty Γ}(u : Tm Γ A)(γ : ⟦ Γ ⟧Con) → ⟦ A ⟧Ty γ

⟦ ∅     ⟧Con = One
⟦ Γ , A ⟧Con = Σ ⟦ Γ ⟧Con ⟦ A ⟧Ty

-- I had to put types before substitutions to make Agda happy
⟦ A [ σ ] ⟧Ty γ = ⟦ A ⟧Ty (⟦ σ ⟧⇒ γ)
⟦ Pi A B  ⟧Ty γ = (x : ⟦ A ⟧Ty γ) → ⟦ B ⟧Ty (γ , x)
⟦ U ⟧Ty γ = {!!}
⟦ El a ⟧Ty γ = {!!}

⟦ ε     ⟧⇒ _ = *
⟦ id    ⟧⇒ γ = γ
⟦ σ ∘ ρ ⟧⇒ γ = ⟦ σ ⟧⇒ (⟦ ρ ⟧⇒ γ)
⟦ p     ⟧⇒ (γ , _) = γ
⟦ σ , u ⟧⇒ γ = ⟦ σ ⟧⇒ γ , ⟦ u ⟧Tm γ

⟦ T1 t ⟧Tm γ = ⟦ t ⟧Tm γ
⟦ T2 t ⟧Tm γ = ⟦ t ⟧Tm γ

⟦ u [ σ ] ⟧Tm γ = ⟦ u ⟧Tm (⟦ σ ⟧⇒ γ)
⟦ q       ⟧Tm (_ , a) = a
⟦ lam t   ⟧Tm γ = λ x → ⟦ t ⟧Tm (γ , x)
⟦ app f u ⟧Tm γ = (⟦ f ⟧Tm γ) (⟦ u ⟧Tm γ)

-- types
e1 : {Γ : Con}{A : Ty Γ} → ⟦ A [ id ] ⟧Ty ≡ ⟦ A ⟧Ty
e1 = refl

e2 : {Θ Δ Γ : Con}{A : Ty Γ}{ρ : Δ ⇒ Γ}{σ : Θ ⇒ Δ} → ⟦ A [ ρ ] [ σ ] ⟧Ty ≡ ⟦ A [ ρ ∘ σ ] ⟧Ty
e2 = refl

e3 : {Δ Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{ρ : Δ ⇒ Γ} → ⟦ (Pi A B) [ ρ ] ⟧Ty ≡ ⟦ Pi (A [ ρ ]) (B [ (ρ ∘ p , T2 (q {Δ}{A [ ρ ]})) ]) ⟧Ty
e3 = refl

-- other direction
f : {Γ : Con}{A : Ty Γ}(t t' : Tm Γ A) → ⟦ t ⟧Tm ≡ ⟦ t' ⟧Tm → t ≡ t'
f = {!!}
-}
