module TT.experiments.IR4 where

open import lib

data Con : Set
data Ty : Con → Set
data Tms : Con → Con → Set
data Tm : (Γ : Con) → Ty Γ → Set

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con)(A : Ty Γ) → Con

data Ty where
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ}(t : Tm Γ (A [ σ ]T)) → Tms Γ (Δ , A)
  p     : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ

data Tm where
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) 
  q     : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ p ]T)

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infixl 8 _[_]t

id : {Γ : Con} → Tms Γ Γ
id {•} = ε
id {Γ , A} = p ,s q

_∘_   : {Γ Δ Σ : Con}(σ : Tms Δ Σ)(ν : Tms Γ Δ) → Tms Γ Σ
ε ∘ ν = ε
(σ ,s t) ∘ ν = (σ ∘ ν) ,s {!t [ ν ]t!}
p ∘ (ν ,s t) = ν
p ∘ p = {!!}
