{-# OPTIONS --without-K #-}

module TT.experiments.Neu3 where

open import lib

infixl 5 _▷_
infixl 7 _[_]T
infixl 8 _[_]V
infixl 8 _[_]N
infix  6 _∘_
infixl 5 _,_
infixl 5 _^_

data Con : Set
data Ty : Con → Set
data Var : (Γ : Con) → Ty Γ → Set
data Neu : (Γ : Con) → Ty Γ → Set
data Nes : Con → Con → Set

data Con where
  •   : Con
  _▷_ : (Γ : Con)(A : Ty Γ) → Con

_[_]T : {Δ : Con}(A : Ty Δ){Γ : Con}(σ : Nes Γ Δ) → Ty Γ
_[_]V : {Δ : Con}{A : Ty Δ}(x : Var Δ A){Γ : Con}(σ : Nes Γ Δ) → Neu Γ (A [ σ ]T)
_[_]N : {Δ : Con}{A : Ty Δ}(n : Neu Δ A){Γ : Con}(σ : Nes Γ Δ) → Neu Γ (A [ σ ]T)
id    : {Γ : Con} → Nes Γ Γ
_∘_   : {Γ Θ Δ : Con}(σ : Nes Θ Δ)(δ : Nes Γ Θ) → Nes Γ Δ
[∘]T  : {Γ Θ Δ : Con}{σ : Nes Θ Δ}{δ : Nes Γ Θ}{A : Ty Δ} → A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T
[∘]N  : {Γ Θ Δ : Con}{σ : Nes Θ Δ}{δ : Nes Γ Θ}{A : Ty Δ}{n : Neu Δ A} → transport (Neu Γ) [∘]T (n [ σ ∘ δ ]N) ≡ n [ σ ]N [ δ ]N
wkNs  : {Γ Δ : Con}(σ : Nes Γ Δ){C : Ty Γ} → Nes (Γ ▷ C) Δ
π₁    : {Γ Δ : Con}{A : Ty Δ}(σ : Nes Γ (Δ ▷ A)) → Nes Γ Δ
π₂    : {Γ Δ : Con}{A : Ty Δ}(σ : Nes Γ (Δ ▷ A)) → Neu Γ (A [ π₁ σ ]T)
wkNs= : {Γ Δ : Con}{σ : Nes Γ Δ}{C : Ty Γ} → wkNs σ {C} ≡ σ ∘ π₁ id

data Ty where
  U  : {Γ : Con} → Ty Γ
  El : {Γ : Con}(a : Neu Γ U) → Ty Γ
  Π  : {Γ : Con}(a : Neu Γ U)(B : Ty (Γ ▷ El a)) → Ty Γ

data Var where
  vz : {Γ : Con}{A : Ty Γ} → Var (Γ ▷ A) (A [ wkNs id ]T)
  vs : {Γ : Con}{A B : Ty Γ} → Var Γ A → Var (Γ ▷ B) (A [ wkNs id ]T)

data Neu where
  var : {Γ : Con}{A : Ty Γ} → Var Γ A → Neu Γ A

data Nes where
  ε : {Γ : Con} → Nes Γ •
  _,_ : {Γ Δ : Con}(σ : Nes Γ Δ){A : Ty Δ}(n : Neu Γ (A [ σ ]T)) → Nes Γ (Δ ▷ A)

π₁ (σ , n) = σ
π₂ (σ , n) = n

wkNs ε = ε
wkNs (σ , n) = wkNs σ , {!tr (Neu _) ? (n [ wkNs id ]N)!}

id {•} = ε
id {Γ ▷ A} = wkNs (id {Γ}) , var vz

wkNs= {σ = ε} = {!!}
wkNs= {σ = σ , n} = {!!}

ε ∘ δ = ε
(σ , n) ∘ δ = (σ ∘ δ) , transport (Neu _) ([∘]T ⁻¹) (n [ δ ]N)

U [ σ ]T = U
El a [ σ ]T = El (a [ σ ]N)
Π a B [ σ ]T = Π (a [ σ ]N) (B [ wkNs σ , {!vz!} ]T)

[∘]T = {!!}

vz [ σ , n ]V = {!n!}
vs x [ σ , n ]V = {!x [ σ ]V!}

var x [ σ ]N = x [ σ ]V

[∘]N {σ = σ} {δ} {A} {var x} = {!!}

_^_ : {Γ Δ : Con}(σ : Nes Γ Δ)(A : Ty Δ) → Nes (Γ ▷ A [ σ ]T) (Δ ▷ A)
σ ^ A = {!!}
