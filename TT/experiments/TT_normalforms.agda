
{-# OPTIONS --without-K --no-termination-check --type-in-type #-}

module TT_normalforms where

open import library

-- type theory with only normal forms

data Con : Set
data Ty  : Con → Set
data Neu : (Γ : Con) → Ty Γ → Set
data Tm  : (Γ : Con) → Ty Γ → Set
data _⇒_ : Con → Con → Set

_[_]T : {Γ : Con}(A : Ty Γ){Δ : Con}(σ : Δ ⇒ Γ) → Ty Δ
_[_]  : {Γ : Con}{A : Ty Γ}(a : Tm Γ A){Δ : Con}(ρ : Δ ⇒ Γ) → Tm Δ (A [ ρ ]T)
id    : {Γ : Con} → Γ ⇒ Γ
[id]T : {Γ : Con}{A : Ty Γ} → A [ id ]T ≡ A

T1 : {Γ : Con}{A : Ty Γ} → Tm Γ (A [ id ]T) → Tm Γ A
T1 {Γ} = transport (Tm Γ) [id]T

T2 : {Γ : Con}{A : Ty Γ} → Tm Γ A → Tm Γ (A [ id ]T)
T2 {Γ} = transport (Tm Γ) ([id]T ⁻¹)

data Con where
  ∅   : Con
  _,_ : (Γ : Con)(A : Ty Γ) → Con
infixl 6 _,_

_⁺ : {Γ : Con}{A : Ty Γ}(B : Ty Γ) → Ty (Γ , A)

data _⇒_ where
  ε   : {Γ : Con} → Γ ⇒ ∅
  _,_ : {Γ : Con}{A : Ty Γ}{Δ : Con}(ρ : Δ ⇒ Γ)(u : Tm Δ (A [ ρ ]T))
      → Δ ⇒ (Γ , A)

data Ty where
  U  : {Γ : Con} → Ty Γ
  El : {Γ : Con} → Tm Γ U → Ty Γ
  Pi : {Γ : Con}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ

data Var : (Γ : Con)(A : Ty Γ) → Set where
  zero : {Γ : Con}{A : Ty Γ} → Var (Γ , A) (A ⁺)
  suc  : {Γ : Con}{A B : Ty Γ} → Var Γ A → Var (Γ , B) (A ⁺)

data Neu where
  var : {Γ : Con}{A : Ty Γ} → Var Γ A → Neu Γ A
  app : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}
        (f : Neu Γ (Pi A B))(u : Tm Γ A)
      → Neu Γ (B [ id , T2 u ]T)

data Tm where
  neu : {Γ : Con}{A : Ty Γ} → Neu Γ A → Tm Γ A
  lam : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm (Γ , A) B)
      → Tm Γ (Pi A B)
  ∣_∣ : {Γ : Con}(A : Ty Γ) → Tm Γ U
  

_⁺ = {!!}

_[_]T = {!!}

_[_] = {!!}

id = {!!}

[id]T = {!!}

{-


data Tm : Con → Ty → Set

data Neu : Con → Ty → Set where
  var : {Γ : Con}{A : Ty} → Var Γ A → Neu Γ A
  app : {Γ : Con}{A B : Ty} → Neu Γ (A ⇒ B) → Tm Γ A → Neu Γ B

data Tm where
  neu : {Γ : Con}{A : Ty} → Neu Γ A → Tm Γ A
  lam : {Γ : Con}{A B : Ty} → Tm (Γ , A) B → Tm Γ (A ⇒ B)

q : {Γ : Con}{A : Ty} → Tm (Γ , A) A
q = neu (var zero)

data Tms : Con → Con → Set where
  ε : {Γ : Con} → Tms Γ ∅
  _,_ : {Γ Δ : Con}{A : Ty} → Tms Γ Δ → Tm Γ A → Tms Γ (Δ , A)

p : {Γ : Con}{A : Ty} → Tms (Γ , A) Γ
p = emb p'

  where

    data Vars : Con → Con → Set where
      ε : {Γ : Con} → Vars Γ ∅
      _,_ : {Γ Δ : Con}{A : Ty} → Vars Γ Δ → Var Γ A → Vars Γ (Δ , A)

    +1 : {Γ Δ : Con}{A : Ty} → Vars Γ Δ → Vars (Γ , A) Δ
    +1 ε = ε
    +1 (xs , x) = +1 xs , suc x

    p' : {Γ : Con}{A : Ty} → Vars (Γ , A) Γ
    p' {∅} = ε
    p' {Γ , A} = +1 (p' {Γ}) , suc zero

    emb : {Γ Δ : Con} → Vars Γ Δ → Tms Γ Δ
    emb ε = ε
    emb (vs , x) = emb vs , neu (var x)

_!!_ : {Γ Δ : Con}{A : Ty} → Tms Δ Γ → Var Γ A → Tm Δ A
ε !! ()
(σ , t) !! zero = t
(σ , t) !! suc x = σ !! x

id : {Γ : Con} → Tms Γ Γ
_∘_ : {Γ Δ Θ : Con} → Tms Δ Θ → Tms Γ Δ → Tms Γ Θ

id {∅} = ε
id {∅ , A} = ε , neu (var zero)
id {(Γ , A) , B} = (id {Γ , A} ∘ p) , neu (var zero)

_[_] : {Γ Δ : Con}{A : Ty} → Tm Γ A → Tms Δ Γ → Tm Δ A

ε ∘ σ = ε
(ρ , t) ∘ σ = ((ρ ∘ σ) , t [ σ ])

App : {Γ : Con}{A B : Ty} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B

neu (var x) [ σ ] = σ !! x
neu (app f u) [ σ ] = App (neu f [ σ ]) (u [ σ ])
lam t [ σ ] = lam (t [ (σ ∘ p) , q ])

App (neu x) u = neu (app x u)
App (lam t) u = t [ id , u ]
-}
