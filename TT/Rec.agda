{-# OPTIONS --without-K --rewriting #-}

open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.Func

module TT.Rec {i j}{d : Decl}{c : Core {i}{j} d}(b : Base c)(f : Func c) where

open import lib

open import TT.Core.Syntax as S
open import TT.Base.Syntax as SB
open import TT.Func.Syntax as SF

private module D = Decl d
private module C = Core c
private module B = Base b
private module F = Func f

postulate
  RecCon : S.Con → D.Con
  RecTy  : ∀{Γ}(A : S.Ty Γ) → D.Ty (RecCon Γ)
  RecTms : ∀{Γ Δ}(δ : S.Tms Γ Δ) → D.Tms (RecCon Γ) (RecCon Δ)
  RecTm  : ∀{Γ}{A : S.Ty Γ}(t : S.Tm Γ A) → D.Tm (RecCon Γ) (RecTy A)

-- Core

postulate
  β• : RecCon S.• ≡ C.•
  β, : ∀{Γ A} → (RecCon (Γ S., A)) ≡ ((RecCon Γ) C., (RecTy A))

{-# REWRITE β• #-}
{-# REWRITE β, #-}

postulate
  β[]T   : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ Δ}
         → (RecTy (A S.[ σ ]T)) ≡ (RecTy A C.[ RecTms σ ]T)

{-# REWRITE β[]T #-}

postulate
  βid : ∀{Γ} → (RecTms (S.id {Γ})) ≡ C.id
  β∘  : ∀{Γ Δ Σ}{σ : S.Tms Δ Σ}{ν : S.Tms Γ Δ}
      → (RecTms (σ S.∘ ν)) ≡ (RecTms σ C.∘ RecTms ν)
  βε  : ∀{Γ} → (RecTms (S.ε {Γ})) ≡ C.ε
  β,s : ∀{Γ Δ}{σ : S.Tms Γ Δ}{A : S.Ty Δ}{t : S.Tm Γ (A S.[ σ ]T)}
      → (RecTms (σ S.,s t)) ≡ (RecTms σ C.,s RecTm t)
  βπ₁ : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ (Δ S., A)}
      → (RecTms (S.π₁ σ)) ≡ (C.π₁ (RecTms σ))

{-# REWRITE βid #-}
{-# REWRITE β∘  #-}
{-# REWRITE βε  #-}
{-# REWRITE β,s #-}
{-# REWRITE βπ₁ #-}

postulate
  β[]t : ∀{Γ Δ}{A : S.Ty Δ}{t : S.Tm Δ A}{σ : S.Tms Γ Δ}
       → (RecTm (t S.[ σ ]t)) ≡ (RecTm t C.[ RecTms σ ]t)
  βπ₂  : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ (Δ S., A)}
       → (RecTm (S.π₂ σ)) ≡ (C.π₂ (RecTms σ))

{-# REWRITE β[]t #-}
{-# REWRITE βπ₂  #-}

postulate
  β[][]T
    : ∀{Γ Δ Σ}{A : S.Ty Σ}{σ : S.Tms Γ Δ}{δ : S.Tms Δ Σ}
    → ap RecTy (S.[][]T {Γ}{Δ}{Σ}{A}{σ}{δ}) ≡ C.[][]T

{-# REWRITE β[][]T #-}

-- Base

postulate
  βU : ∀{Γ} → (RecTy (SB.U {Γ})) ≡ B.U
  
{-# REWRITE βU #-}

postulate
  βU[] : ∀{Γ Δ}{σ : Tms Γ Δ} → ap RecTy (SB.U[] {Γ}{Δ}{σ}) ≡ (B.U[] {RecCon Γ}{RecCon Δ}{RecTms σ})

{-# REWRITE βU[] #-}

postulate
  βEl : ∀{Γ}{Â : S.Tm Γ SB.U}
      → (RecTy (SB.El Â)) ≡ (B.El (RecTm Â))
      
{-# REWRITE βEl #-}

-- Func

postulate
  βΠ : ∀{Γ}{A : S.Ty Γ}{B : S.Ty (Γ S., A)}
     → (RecTy (SF.Π A B)) ≡ (F.Π (RecTy A) (RecTy B))

{-# REWRITE βΠ #-}

postulate
  βlam : ∀{Γ}{A : S.Ty Γ}{B : S.Ty (Γ S., A)}{t : S.Tm (Γ S., A) B}
       → (RecTm (SF.lam t)) ≡ (F.lam (RecTm t))
  βapp : ∀{Γ}{A : S.Ty Γ}{B : S.Ty (Γ S., A)}{t : S.Tm Γ (SF.Π A B)}
       → (RecTm (SF.app t)) ≡ (F.app (RecTm t))

{-# REWRITE βlam #-}
{-# REWRITE βapp #-}
