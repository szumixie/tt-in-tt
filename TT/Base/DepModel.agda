module TT.Base.DepModel where

open import Agda.Primitive

open import lib

open import TT.Decl.Syntax
open import TT.Decl.Congr.NoJM syntaxDecl
open import TT.Base.Syntax
open import TT.Base.Congr.NoJM syntaxBase

open import TT.Decl.DepModel
open import TT.Core.DepModel

record Baseᴹ {i j}{d : Declᴹ {i}{j}}(c : Coreᴹ d) : Set (i ⊔ j) where
  open Declᴹ d
  open Coreᴹ c
  field
    Uᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ} → Tyᴹ Γᴹ U
    Elᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Â : Tm Γ U}(Âᴹ : Tmᴹ Γᴹ Uᴹ Â) → Tyᴹ Γᴹ (El Â)

    U[]ᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
           → Uᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ= U[] ]≡ Uᴹ
    El[]ᴹ  : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {Â : Tm Δ U}{Âᴹ : Tmᴹ Δᴹ Uᴹ Â}
           → Elᴹ Âᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ= El[] ]≡ Elᴹ (coe (TmΓᴹ= U[] U[]ᴹ refl) (Âᴹ [ δᴹ ]tᴹ))

  U[]'ᴹ
    : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Γᴹ₀ : Conᴹ Γ₀}{Γᴹ₁ : Conᴹ Γ₁}(Γᴹ₂ : Γᴹ₀ ≡[ Conᴹ= Γ₂ ]≡ Γᴹ₁)
      {Δ : Con}{Δᴹ : Conᴹ Δ}{σ : Tms Γ₀ Δ}{σᴹ : Tmsᴹ Γᴹ₀ Δᴹ σ}
    → Uᴹ [ σᴹ ]Tᴹ ≡[ Tyᴹ= Γ₂ Γᴹ₂ (U[]' Γ₂) ]≡ Uᴹ
  U[]'ᴹ refl refl = U[]ᴹ

  El[]'ᴹ
    : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Γᴹ₀ : Conᴹ Γ₀}{Γᴹ₁ : Conᴹ Γ₁}(Γᴹ₂ : Γᴹ₀ ≡[ Conᴹ= Γ₂ ]≡ Γᴹ₁)
      {Δ : Con}{Δᴹ : Conᴹ Δ}{σ : Tms Γ₀ Δ}{σᴹ : Tmsᴹ Γᴹ₀ Δᴹ σ}
      {A : Tm Δ U}{Aᴹ : Tmᴹ Δᴹ Uᴹ A}
    → Elᴹ Aᴹ [ σᴹ ]Tᴹ
    ≡[ Tyᴹ= Γ₂ Γᴹ₂ (El[]' Γ₂) ]≡
      Elᴹ (coe (Tmᴹ= Γ₂ Γᴹ₂ (U[]' Γ₂) (U[]'ᴹ Γ₂ Γᴹ₂) refl)
               (Aᴹ [ σᴹ ]tᴹ))
  El[]'ᴹ refl refl = El[]ᴹ

  El[]''ᴹ
    : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
      {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
      {A : Tm Δ U}{Aᴹ : Tmᴹ Δᴹ Uᴹ A}
      {B : Tm Γ U}{Bᴹ : Tmᴹ Γᴹ Uᴹ B}
      (p : A [ σ ]t ≡[ TmΓ= U[] ]≡ B)(pᴹ : Aᴹ [ σᴹ ]tᴹ ≡[ TmΓᴹ= U[] U[]ᴹ p ]≡ Bᴹ)
    → (Elᴹ Aᴹ [ σᴹ ]Tᴹ) ≡[ TyΓᴹ= (El[]'' p) ]≡ Elᴹ Bᴹ
  El[]''ᴹ refl refl = El[]ᴹ

  Uᴹ=
    : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Γᴹ₀ : Conᴹ Γ₀}{Γᴹ₁ : Conᴹ Γ₁}(Γᴹ₂ : Γᴹ₀ ≡[ Conᴹ= Γ₂ ]≡ Γᴹ₁)
    → Uᴹ {Γ₀}{Γᴹ₀} ≡[ Tyᴹ= Γ₂ Γᴹ₂ (U= Γ₂) ]≡ Uᴹ {Γ₁}{Γᴹ₁}
  Uᴹ= refl refl = refl

  Elᴹ=
    : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Γᴹ₀ : Conᴹ Γ₀}{Γᴹ₁ : Conᴹ Γ₁}(Γᴹ₂ : Γᴹ₀ ≡[ Conᴹ= Γ₂ ]≡ Γᴹ₁)
      {A₀ : Tm Γ₀ U}{A₁ : Tm Γ₁ U}(A₂ : A₀ ≡[ Tm= Γ₂ (U= Γ₂) ]≡ A₁)
      {Aᴹ₀ : Tmᴹ Γᴹ₀ Uᴹ A₀}{Aᴹ₁ : Tmᴹ Γᴹ₁ Uᴹ A₁}(Aᴹ₂ : Aᴹ₀ ≡[ Tmᴹ= Γ₂ Γᴹ₂ (U= Γ₂) (Uᴹ= Γ₂ Γᴹ₂) A₂ ]≡ Aᴹ₁)
    → Elᴹ Aᴹ₀ ≡[ Tyᴹ= Γ₂ Γᴹ₂ (El= Γ₂ A₂) ]≡ Elᴹ Aᴹ₁
  Elᴹ= refl refl refl refl = refl
