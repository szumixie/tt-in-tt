{-# OPTIONS --without-K #-}

module TT.Base.Syntax where

open import TT.Decl.Syntax public
open import TT.Core.Syntax public
open import TT.Base

postulate
  syntaxBase : Base syntaxCore
  
open Base syntaxBase public
