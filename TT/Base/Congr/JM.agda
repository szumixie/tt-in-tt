{-# OPTIONS --no-eta #-}

open import TT.Decl
open import TT.Core
open import TT.Base

module TT.Base.Congr.JM {i}{j}{d : Decl {i}{j}}{c : Core d}(b : Base c) where

open import lib
open import JM

open Decl d
open Core c
open Base b
open import TT.Decl.Congr.NoJM d
open import TT.Decl.Congr.JM d
open import TT.Core.Congr.NoJM c
open import TT.Core.Congr.JM c
open import TT.Core.Laws.NoJM c
open import TT.Core.Laws.JM c
open import TT.Base.Congr.NoJM b

El=' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)(U₂ : U {Γ₀} ≡[ Ty= Γ₂ ]≡ U {Γ₁})
       {Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Â₀ ≡[ Tm= Γ₂ U₂ ]≡ Â₁)
     → El Â₀ ≡[ Ty= Γ₂ ]≡ El Â₁
El=' refl refl refl = refl

U≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
   → U {Γ₀} ≃ U {Γ₁}
U≃ refl = refl ,≃ refl

El≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Â₀ ≃ Â₁)
    → El Â₀ ≃ El Â₁
El≃ refl (refl ,≃ refl) = refl ,≃ refl

abstract
  El[<>][]
    : {A : ∀{Γ} → Tm Γ U}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]t ≡[ TmΓ= U[] ]≡ A)
      {t : ∀{Γ} → Tm Γ (El A)}(t[] : ∀{Γ Θ}{σ : Tms Γ Θ} → t [ σ ]t ≡[ TmΓ= (El[]'' A[]) ]≡ t)
      {Γ Θ : Con}{σ : Tms Γ Θ}{C : Tm (Θ , El A) U}
    → El C [ < t > ]T [ σ ]T
    ≡ El (coe (TmΓ= U[]) (C [ coe (Tms-Γ= (Γ,C= (El[]'' A[]))) (σ ^ El A) ]t)) [ < t > ]T
  El[<>][] A[] t[] = [<>][]T (El[]'' A[]) t[] ◾ []T=' refl El[] refl

abstract
  El[<>][]'
    : {A : ∀{Γ} → Tm Γ U}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]t ≡[ TmΓ= U[] ]≡ A)
      {Γ Θ : Con}(t : Tm Θ (El A)){σ : Tms Γ Θ}{C : Tm (Θ , El A) U}
    → El C [ < t > ]T [ σ ]T
    ≡ El (coe (TmΓ= U[]) (C [ coe (Tms-Γ= (Γ,C= (El[]'' A[]))) (σ ^ El A) ]t)) [ < coe (TmΓ= (El[]'' A[])) (t [ σ ]t) > ]T
  El[<>][]' A[] t = [<>][]T' (El[]'' A[]) t ◾ []T=' refl El[] refl
