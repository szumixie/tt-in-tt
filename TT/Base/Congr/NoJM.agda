{-# OPTIONS --without-K --no-eta #-}

open import TT.Decl
open import TT.Core
open import TT.Base

module TT.Base.Congr.NoJM {i}{j}{d : Decl {i}{j}}{c : Core d}(b : Base c) where

open import lib

open Decl d
open Core c
open Base b
open import TT.Decl.Congr.NoJM d
open import TT.Core.Congr.NoJM c
open import TT.Core.Laws.NoJM c

U= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → U {Γ₀} ≡[ Ty= Γ₂ ]≡ U {Γ₁}
U= refl = refl

El= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Â₀ ≡[ Tm= Γ₂ (U= Γ₂) ]≡ Â₁)
    → El Â₀ ≡[ Ty= Γ₂ ]≡ El Â₁
El= refl refl = refl

U[]' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ : Con}{σ : Tms Γ₀ Δ} → U [ σ ]T ≡[ Ty= Γ₂ ]≡ U
U[]' refl = U[]
  
El[]' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ : Con}{σ : Tms Γ₀ Δ}{A : Tm Δ U}
      → El A [ σ ]T ≡[ Ty= Γ₂ ]≡ El (coe (Tm= Γ₂ (U[]' Γ₂)) (A [ σ ]t))
El[]' refl = El[]

El[]''
  : ∀{Γ Δ}{σ : Tms Γ Δ}
    {A : Tm Δ U}{B : Tm Γ U}(p : A [ σ ]t ≡[ TmΓ= U[] ]≡ B)
  → (El A [ σ ]T) ≡ El B
El[]'' refl = El[]
