module TT.FuncSP.Syntax where

open import TT.Decl.Syntax
open import TT.Base.Syntax
open import TT.FuncSP

postulate
  syntaxFuncSP : FuncSP syntaxBase
  
open FuncSP syntaxFuncSP public
