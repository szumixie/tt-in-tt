module TT.Readme where

------------------------------------------------------------------------------
-- parts of the syntax defined as records
------------------------------------------------------------------------------

import TT.Decl              -- declaration of a model of type theory
import TT.Decl.Syntax
import TT.Decl.DepModel
import TT.Decl.Congr

import TT.IsSet
import TT.IsSet.Syntax
import TT.IsSet.DepModel

import TT.Core              -- core substitution calculus
import TT.Core.Syntax
import TT.Core.DepModel
import TT.Core.Congr
import TT.Core.Laws

import TT.Base              -- U, El
import TT.Base.Syntax
import TT.Base.DepModel
import TT.Base.Congr

import TT.Func              -- Π
import TT.Func.Syntax
import TT.Func.DepModel
import TT.Func.Congr
import TT.Func.Laws

import TT.Sigma             -- Σ
import TT.Empty             -- Void
import TT.One               -- Unit
import TT.Boolean           -- Bool
import TT.Natural           -- 𝕟
import TT.Iden              -- Id
import TT.Iden.Syntax

import TT.FuncU             -- U is closed under Π
import TT.FuncU.Syntax
import TT.FuncU.DepModel
import TT.EmptyU            -- U is closed under Void
import TT.OneU              -- U is closed under Unit
import TT.BooleanU          -- U is closed under Bool

import TT.FuncSP            -- strictly positive function space
import TT.FuncSP.Syntax
import TT.FuncSP.DepModel
import TT.FuncSP.Rec

------------------------------------------------------------------------------
-- grouping together the above
------------------------------------------------------------------------------

import TT.Syntax
import TT.Congr
import TT.Laws

import TT.DepModel

import TT.Rec
import TT.RecOld
import TT.Elim
import TT.ElimOld
import TT.ConElim

------------------------------------------------------------------------------
-- examples
------------------------------------------------------------------------------

import TT.Examples

------------------------------------------------------------------------------
-- alternative syntax
------------------------------------------------------------------------------

import TT.TwoLevels.Decl
import TT.TwoLevels.Core
import TT.TwoLevels.Func
-- import TT.TwoLevels.Sigma

import TT.Levels.Decl
import TT.Levels.Core
import TT.Levels.Func
-- import TT.Levels.Sigma
