{-# OPTIONS --without-K #-}

module TT.CwF where

open import Agda.Primitive
open import lib
open import TT.Decl
import TT.Decl.Congr.NoJM

-- Core substitution calculus

module _ {i j}(d : Decl {i}{j}) where
  open Decl d
  open TT.Decl.Congr.NoJM d

  record CwF : Set (i ⊔ j) where
    field
      •     : Con  -- \bub
      _,_   : (Γ : Con) → Ty Γ → Con
      
      _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

      id    : ∀{Γ} → Tms Γ Γ
      _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
      ε     : ∀{Γ} → Tms Γ •
      _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
      p     : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
      q     : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ p ]T)

      _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) 

      [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
      [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
              → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)
              
      [id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≡[ TmΓ= [id]T ]≡  t
      [][]t : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}{t : Tm Σ A}
              → (t [ δ ]t [ σ ]t) ≡[ TmΓ= [][]T ]≡ (t [ δ ∘ σ ]t)
              
      idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ 
      idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ 
      ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
            → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
      ,β₁   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)} → p ∘ (σ ,s t) ≡ σ
      ,β₂   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)} → q [ σ ,s t ]t ≡[ TmΓ= ([][]T ◾ ap (A [_]T) ,β₁) ]≡ t
      ,η    : ∀{Γ}{A : Ty Γ} → (p {Γ}{A} ,s q) ≡ id
      ,∘    : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}{Θ : Con}{ν : Tms Θ Γ} → (σ ,s t) ∘ ν ≡ (σ ∘ ν ,s coe (TmΓ= [][]T) (t [ ν ]t))
      εη    : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε

    infixl 5 _,_
    infixl 7 _[_]T
    infixl 5 _,s_
    infix  6 _∘_
    infixl 8 _[_]t

  open import TT.Core
  open import JM

  module _ (c : Core d) where
    open Core c
    open import TT.Core.Congr c
    open import TT.Core.Laws c

    toCwF : CwF
    toCwF =
      record
        { • = •
        ; _,_ = _,_
        ; _[_]T = _[_]T
        ; id = id
        ; _∘_ = _∘_
        ; ε = ε
        ; _,s_ = _,s_
        ; p = π₁ id
        ; q = π₂ id
        ; _[_]t = _[_]t
        ; [id]T = [id]T
        ; [][]T = [][]T
        ; [id]t = [id]t
        ; [][]t = [][]t
        ; idl = idl
        ; idr = idr
        ; ass = ass
        ; ,β₁ = π₁idβ
        ; ,β₂ = from≃ (uncoe (TmΓ= ([][]T ◾ ap (_[_]T _) π₁idβ)) ⁻¹̃ ◾̃ π₂idβ)
        ; ,η = λ {Γ}{A} → πη {δ = id}
        ; ,∘ = ,∘
        ; εη = εη
        }

  module _ (c : CwF) where
    open CwF c

    ,s= : ∀{Γ Δ}{σ₀ σ₁ : Tms Γ Δ}(σ₀₁ : σ₀ ≡ σ₁){A : Ty Δ}
      {t₀ : Tm Γ (A [ σ₀ ]T)}{t₁ : Tm Γ (A [ σ₁ ]T)}(t₀₁ : t₀ ≡[ ap (λ z → Tm Γ (A [ z ]T)) σ₀₁ ]≡ t₁) →
      σ₀ ,s t₀ ≡ σ₁ ,s t₁
    ,s= refl refl = refl
      

    fromCwF : Core d
    fromCwF = record { c1 = record
      { • = •
      ; _,_ = _,_
      ; _[_]T = _[_]T
      ; id = id
      ; _∘_ = _∘_
      ; ε = ε
      ; _,s_ = _,s_
      ; π₁ = p ∘_
      ; _[_]t = _[_]t
      ; π₂ = λ σ → coe (TmΓ= [][]T) (q [ σ ]t)
      } ; c2 = record
      { [id]T = [id]T
      ; [][]T = [][]T
      ; idl = idl
      ; idr = idr
      ; ass = ass
      ; ,∘ = ,∘
      ; π₁β = ,β₁
      ; πη = ,∘ ⁻¹ ◾ ap (_∘ _) ,η ◾ idl
      ; εη = εη
      ; π₂β = {!,β₂!}
      } }
