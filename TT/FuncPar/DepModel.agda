module TT.FuncPar.DepModel where

open import Agda.Primitive

open import lib

open import TT.Core.Syntax
open import TT.FuncPar.Syntax

open import TT.Decl.DepModel
open import TT.Core.DepModel

record FuncParᴹ {i j k}{d : Declᴹ {i}{j}}(c : Coreᴹ d) : Set (i ⊔ j ⊔ lsuc k) where
  open Declᴹ d
  open Coreᴹ c
  field
    Πᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ}(A : Set k){B : A → Ty Γ}
             (Bᴹ : (a : A) → Tyᴹ Γᴹ (B a)) → Tyᴹ Γᴹ (Π A B)

    appᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Set k}{B : A → Ty Γ}{Bᴹ : (a : A) → Tyᴹ Γᴹ (B a)}
             {t : Tm Γ (Π A B)} → Tmᴹ Γᴹ (Πᴹ A Bᴹ) t → (a : A) → Tmᴹ Γᴹ (Bᴹ a) (app t a)
    lamᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Set k}{B : A → Ty Γ}{Bᴹ : (a : A) → Tyᴹ Γᴹ (B a)}
             {t : (a : A) → Tm Γ (B a)} → ((a : A) → Tmᴹ Γᴹ (Bᴹ a) (t a)) → Tmᴹ Γᴹ (Πᴹ A Bᴹ) (lam t)

    Π[]ᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {A : Set k}{B : A → Ty Δ}{Bᴹ : (a : A) → Tyᴹ Δᴹ (B a)}
           → Πᴹ A Bᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ= Π[] ]≡ Πᴹ A (λ a → (Bᴹ a) [ δᴹ ]Tᴹ)

    lam[]ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {A : Set k}{B : A → Ty Δ}{Bᴹ : (a : A) → Tyᴹ Δᴹ (B a)}
             {t : (a : A) → Tm Δ (B a)}{tᴹ : (a : A) → Tmᴹ Δᴹ (Bᴹ a) (t a)}
           → (lamᴹ tᴹ) [ δᴹ ]tᴹ ≡[ TmΓᴹ= Π[] Π[]ᴹ lam[] ]≡ lamᴹ (λ a → (tᴹ a) [ δᴹ ]tᴹ)

    Πβᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Set k}{B : A → Ty Γ}{Bᴹ : (a : A) → Tyᴹ Γᴹ (B a)}
             {t : (a : A) → Tm Γ (B a)}{tᴹ : (a : A) → Tmᴹ Γᴹ (Bᴹ a) (t a)}
           → appᴹ (lamᴹ tᴹ) ≡[ ap (λ z → ((a : A) → Tmᴹ Γᴹ (Bᴹ a) (z a))) Πβ ]≡ tᴹ

    Πηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Set k}{B : A → Ty Γ}{Bᴹ : (a : A) → Tyᴹ Γᴹ (B a)}
             {t : Tm Γ (Π A B)}{tᴹ : Tmᴹ Γᴹ (Πᴹ A Bᴹ) t}
           → lamᴹ (appᴹ tᴹ) ≡[ TmΓAᴹ= Πη ]≡ tᴹ

