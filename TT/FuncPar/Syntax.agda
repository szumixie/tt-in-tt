module TT.FuncPar.Syntax {k} where

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.FuncPar

postulate
  syntaxFuncPar : FuncPar {k = k} syntaxCore

open FuncPar syntaxFuncPar public
