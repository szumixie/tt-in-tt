{-# OPTIONS --without-K --no-eta #-}

module TT.One where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
import TT.Decl.Congr.NoJM

record One {i j}{d : Decl}(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr.NoJM d
  open Core c

  field
    Unit   : ∀{Γ} → Ty Γ
    Unit[] : ∀{Γ Θ}{σ : Tms Γ Θ} → Unit [ σ ]T ≡ Unit
    *      : ∀{Γ} → Tm Γ Unit
    *[]    : ∀{Γ Θ}{σ : Tms Γ Θ} → * [ σ ]t ≡[ TmΓ= Unit[] ]≡ *
