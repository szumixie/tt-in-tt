{-# OPTIONS --without-K --no-eta #-}

open import TT.Decl
open import TT.Core
open import TT.Sigma

module TT.Sigma.Congr.NoJM {i}{j}{d : Decl {i}{j}}{c : Core d}(s : Sigma c) where

open import lib

open Decl d
open Core c
open Sigma s
open import TT.Decl.Congr.NoJM d
open import TT.Core.Congr.NoJM c

Σ'= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
      {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≡[ Ty= (,C= Γ₂ A₂) ]≡ B₁)
    → Σ' A₀ B₀ ≡[ Ty= Γ₂ ]≡ Σ' A₁ B₁
Σ'= refl refl refl = refl
