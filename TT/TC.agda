{-# OPTIONS --no-eta #-}

module TT.TC where

open import Agda.Primitive
open import lib
open import TT.Decl
open import TT.Core
open import TT.Func

-- type checking calculus

record TC {i j}{d : Decl}{c : Core {i}{j} d}(f : Func {i} c) : Set (i ⊔ j) where
  open Decl d
  open Core c
  open Func f

  field
    coerce   : {Γ : Con}(A B : Ty Γ)(t : Tm Γ A) → Tm Γ B
    coerce[] : {Γ : Con}{A B : Ty Γ}{t : Tm Γ A}{Θ : Con}{σ : Tms Θ Γ} →
      (coerce A B t) [ σ ]t ≡ coerce (A [ σ ]T) (B [ σ ]T) (t [ σ ]t)
    coerceβ  : {Γ : Con}{A : Ty Γ}{t : Tm Γ A} → coerce A A t ≡ t
    Π₁       : {Γ : Con}(C : Ty Γ) → Ty Γ
    Π₂       : {Γ : Con}(C : Ty Γ) → Ty (Γ , Π₁ C)
    Π₁[]     : {Γ : Con}{C : Ty Γ}{Θ : Con}{σ : Tms Θ Γ} → Π₁ C [ σ ]T ≡ Π₁ (C [ σ ]T)
    Π₂[]     : {Γ : Con}{C : Ty Γ}{Θ : Con}{σ : Tms Θ Γ} → Π₂ C [ σ ^ Π₁ C ]T ≡[ ap (λ z → Ty (Θ , z)) Π₁[] ]≡ Π₂ (C [ σ ]T)
    Π₁β      : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)} → Π₁ (Π A B) ≡ A
    Π₂β      : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)} → Π₂ (Π A B) ≡[ ap (λ z → Ty (Γ , z)) Π₁β ]≡ B
