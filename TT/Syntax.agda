{-# OPTIONS --without-K #-}

module TT.Syntax where

open import TT.Decl.Syntax public
open import TT.Core.Syntax public
open import TT.Base.Syntax public
open import TT.Func.Syntax public
