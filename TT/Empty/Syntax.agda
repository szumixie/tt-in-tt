module TT.Empty.Syntax where

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Empty

postulate
  syntaxEmpty : Empty syntaxCore
  
open Empty syntaxEmpty public
