{-# OPTIONS --no-eta #-}

module TT.SigmaU where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
open import TT.Base
import TT.Decl.Congr.NoJM
import TT.Core.Congr.NoJM
import TT.Core.Laws.JM
import TT.Base.Congr.NoJM

-- record SigmaU
module _
  {i j}{d : Decl}{c : Core {i}{j} d}(b : Base {i}{j} c)
--  : Set (i ⊔ j)
  where
  open Decl d
  open TT.Decl.Congr.NoJM d
  open Core c
  open TT.Core.Congr.NoJM c
  open TT.Core.Laws.JM c
  open Base b
  open TT.Base.Congr.NoJM b

--  field
  postulate
    Σ'     : ∀{Γ}(a : Tm Γ U)(b : Tm (Γ , El a) U) → Tm Γ U

    Σ[]    : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{b : Tm (Δ , El a) U}
           → ((Σ' a b) [ σ ]t)
           ≡[ TmΓ= U[] ]≡
             (Σ' (coe (TmΓ= U[]) (a [ σ ]t)) (coe (Tm= (Γ,C= El[]) (U[]' (Γ,C= El[]))) (b [ σ ^ El a ]t)))

    _,Σ'_  : ∀{Γ}{a : Tm Γ U}{b : Tm (Γ , El a) U}(t : Tm Γ (El a)) → Tm Γ (El b [ < t > ]T) → Tm Γ (El (Σ' a b))
    proj₁' : ∀{Γ}{a : Tm Γ U}{b : Tm (Γ , El a) U}(w : Tm Γ (El (Σ' a b))) → Tm Γ (El a)
    proj₂' : ∀{Γ}{a : Tm Γ U}{b : Tm (Γ , El a) U}(w : Tm Γ (El (Σ' a b))) → Tm Γ (El b [ < proj₁' w > ]T)
    Σβ₁    : ∀{Γ}{a : Tm Γ U}{b : Tm (Γ , El a) U}{u : Tm Γ (El a)}{v : Tm Γ (El b [ < u > ]T)}
           → proj₁' (u ,Σ' v) ≡ u
    Σβ₂    : ∀{Γ}{a : Tm Γ U}{b : Tm (Γ , El a) U}{u : Tm Γ (El a)}{v : Tm Γ (El b [ < u > ]T)}
           → proj₂' (u ,Σ' v) ≡[ TmΓ= (A[]T= (<>= Σβ₁)) ]≡ v
    Ση     : ∀{Γ}{a : Tm Γ U}{b : Tm (Γ , El a) U}{w : Tm Γ (El (Σ' a b))}
           → (proj₁' w ,Σ' proj₂' w) ≡ w

    ,Σ[]   : ∀{Γ}{a : Tm Γ U}{b : Tm (Γ , El a) U}{u : Tm Γ (El a)}{v : Tm Γ (El b [ < u > ]T)}
             {Θ}{σ : Tms Θ Γ}
           → (u ,Σ' v) [ σ ]t
           ≡[ TmΓ= (El[] ◾ El= refl Σ[]) ]≡ -- (El[] ◾ El= refl Σ[])
             (   coe (TmΓ= El[]) (u [ σ ]t)
             ,Σ' coe (TmΓ= ([][]T ◾ A[]T= <>∘ ◾ [][]T ⁻¹ ◾ {!El[]!}))
                     (v [ σ ]t))

-- (TmΓ= ([][]T ◾ A[]T= <>∘ ◾ [][]T ⁻¹))
