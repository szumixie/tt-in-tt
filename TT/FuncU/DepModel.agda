module TT.FuncU.DepModel where

open import Agda.Primitive

open import lib

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Base.Syntax
open import TT.FuncU.Syntax

open import TT.Decl.Congr.NoJM syntaxDecl
open import TT.Core.Congr.NoJM syntaxCore
open import TT.Base.Congr.NoJM syntaxBase

open import TT.Decl.DepModel
open import TT.Core.DepModel
open import TT.Base.DepModel

record FuncUᴹ {i j}(d : Declᴹ {i}{j}){c : Coreᴹ d}(b : Baseᴹ c) : Set (i ⊔ j) where
  open Declᴹ d
  open Coreᴹ c
  open Baseᴹ b

  field
    Πᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Tm Γ U}(Aᴹ : Tmᴹ Γᴹ Uᴹ A)
             {B : Tm (Γ , El A) U}(Bᴹ : Tmᴹ (Γᴹ ,Cᴹ Elᴹ Aᴹ) Uᴹ B)
           → Tmᴹ Γᴹ Uᴹ (Π A B)

    appᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Tm Γ U}{Aᴹ : Tmᴹ Γᴹ Uᴹ A}
             {B : Tm (Γ , El A) U}{Bᴹ : Tmᴹ (Γᴹ ,Cᴹ Elᴹ Aᴹ) Uᴹ B}
             {t : Tm Γ (El (Π A B))} → Tmᴹ Γᴹ (Elᴹ (Πᴹ Aᴹ Bᴹ)) t → Tmᴹ (Γᴹ ,Cᴹ Elᴹ Aᴹ) (Elᴹ Bᴹ) (app t)

    lamᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Tm Γ U}{Aᴹ : Tmᴹ Γᴹ Uᴹ A}
             {B : Tm (Γ , El A) U}{Bᴹ : Tmᴹ (Γᴹ ,Cᴹ Elᴹ Aᴹ) Uᴹ B}
             {t : Tm (Γ , El A) (El B)} → Tmᴹ (Γᴹ ,Cᴹ Elᴹ Aᴹ) (Elᴹ Bᴹ) t → Tmᴹ Γᴹ (Elᴹ (Πᴹ Aᴹ Bᴹ)) (lam t)

    Π[]ᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {A : Tm Δ U}{Aᴹ : Tmᴹ Δᴹ Uᴹ A}{B : Tm (Δ , El A) U}{Bᴹ : Tmᴹ (Δᴹ ,Cᴹ Elᴹ Aᴹ) Uᴹ B}
           → Πᴹ Aᴹ Bᴹ [ δᴹ ]tᴹ
           ≡[ TmΓᴹ= U[] U[]ᴹ Π[] ]≡
             Πᴹ (coe (TmΓᴹ= U[] U[]ᴹ refl)
                     (Aᴹ [ δᴹ ]tᴹ))
                (coe (Tmᴹ= (,C= refl El[]) (Γ,Cᴹ= El[] El[]ᴹ) (U[]' (,C= refl El[])) (U[]'ᴹ (,C= refl El[]) (Γ,Cᴹ= El[] El[]ᴹ)) refl)
                     (Bᴹ [ δᴹ ^ᴹ Elᴹ Aᴹ ]tᴹ))

    lam[]ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {A : Tm Δ U}{Aᴹ : Tmᴹ Δᴹ Uᴹ A}{B : Tm (Δ , El A) U}{Bᴹ : Tmᴹ (Δᴹ ,Cᴹ Elᴹ Aᴹ) Uᴹ B}
             {t : Tm (Δ , El A) (El B)}{tᴹ : Tmᴹ (Δᴹ ,Cᴹ Elᴹ Aᴹ) (Elᴹ Bᴹ) t}
           → (lamᴹ tᴹ) [ δᴹ ]tᴹ
           ≡[ TmΓᴹ= (El[]'' Π[]) (El[]''ᴹ Π[] Π[]ᴹ) lam[] ]≡
             lamᴹ (coe (Tmᴹ= (Γ,C= El[]) (Γ,Cᴹ= El[] El[]ᴹ) (El[]' (Γ,C= El[])) (El[]'ᴹ (Γ,C= El[]) (Γ,Cᴹ= El[] El[]ᴹ)) refl)
                       (tᴹ [ δᴹ ^ᴹ Elᴹ Aᴹ ]tᴹ))

    Πβᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Tm Γ U}(Aᴹ : Tmᴹ Γᴹ Uᴹ A)
             {B : Tm (Γ , El A) U}{Bᴹ : Tmᴹ (Γᴹ ,Cᴹ Elᴹ Aᴹ) Uᴹ B}
             {t : Tm (Γ , El A) (El B)}{tᴹ : Tmᴹ (Γᴹ ,Cᴹ Elᴹ Aᴹ) (Elᴹ Bᴹ) t}
           → appᴹ (lamᴹ tᴹ) ≡[ TmΓAᴹ= Πβ ]≡ tᴹ

    Πηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Tm Γ U}(Aᴹ : Tmᴹ Γᴹ Uᴹ A)
             {B : Tm (Γ , El A) U}{Bᴹ : Tmᴹ (Γᴹ ,Cᴹ Elᴹ Aᴹ) Uᴹ B}
             {t : Tm Γ (El (Π A B))}{tᴹ : Tmᴹ Γᴹ (Elᴹ (Πᴹ Aᴹ Bᴹ)) t}
           → lamᴹ (appᴹ tᴹ) ≡[ TmΓAᴹ= Πη ]≡ tᴹ
