module TT.FuncNoEta.Syntax where

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.FuncNoEta

postulate
  syntaxFuncNoEta : FuncNoEta syntaxCore
  
open FuncNoEta syntaxFuncNoEta public
