{-# OPTIONS --rewriting #-}

open import TT.Decl
open import TT.Decl.Congr.NoJM
open import TT.Core
open import TT.Core.Congr.NoJM
open import TT.FuncNoEta

module TT.RecFuncNoEta
  {i j}
  {d : Decl}
  {c : Core {i}{j} d}
  (f : FuncNoEta c)
  where

open import lib

open import TT.Decl.Syntax as SD
open import TT.Core.Syntax as SC
open import TT.FuncNoEta.Syntax as SF

private module D = Decl d
private module C = Core c
private module F = FuncNoEta f

postulate
  RecCon : SD.Con → D.Con
  RecTy  : ∀{Γ}(A : SD.Ty Γ) → D.Ty (RecCon Γ)
  RecTms : ∀{Γ Δ}(δ : SD.Tms Γ Δ) → D.Tms (RecCon Γ) (RecCon Δ)
  RecTm  : ∀{Γ}{A : SD.Ty Γ}(t : SD.Tm Γ A) → D.Tm (RecCon Γ) (RecTy A)

-- Core

postulate
  β• : RecCon SC.• ≡ C.•
  β, : ∀{Γ A} → (RecCon (Γ SC., A)) ≡ ((RecCon Γ) C., (RecTy A))

{-# REWRITE β• #-}
{-# REWRITE β, #-}

postulate
  β[]T   : ∀{Γ Δ}{A : SD.Ty Δ}{σ : SD.Tms Γ Δ}
         → (RecTy (A SC.[ σ ]T)) ≡ (RecTy A C.[ RecTms σ ]T)

{-# REWRITE β[]T #-}

postulate
  βid : ∀{Γ} → (RecTms (SC.id {Γ})) ≡ C.id
  β∘  : ∀{Γ Δ Σ}{σ : SD.Tms Δ Σ}{ν : SC.Tms Γ Δ}
      → (RecTms (σ SC.∘ ν)) ≡ (RecTms σ C.∘ RecTms ν)
  βε  : ∀{Γ} → (RecTms (SC.ε {Γ})) ≡ C.ε
  β,s : ∀{Γ Δ}{σ : SD.Tms Γ Δ}{A : SD.Ty Δ}{t : SD.Tm Γ (A SC.[ σ ]T)}
      → (RecTms (σ SC.,s t)) ≡ (RecTms σ C.,s RecTm t)
  βπ₁ : ∀{Γ Δ}{A : SD.Ty Δ}{σ : SD.Tms Γ (Δ SC., A)}
      → (RecTms (SC.π₁ σ)) ≡ (C.π₁ (RecTms σ))

{-# REWRITE βid #-}
{-# REWRITE β∘  #-}
{-# REWRITE βε  #-}
{-# REWRITE β,s #-}
{-# REWRITE βπ₁ #-}

postulate
  β[]t : ∀{Γ Δ}{A : SD.Ty Δ}{t : SD.Tm Δ A}{σ : SD.Tms Γ Δ}
       → (RecTm (t SC.[ σ ]t)) ≡ (RecTm t C.[ RecTms σ ]t)
  βπ₂  : ∀{Γ Δ}{A : SD.Ty Δ}{σ : SD.Tms Γ (Δ SC., A)}
       → (RecTm (SC.π₂ σ)) ≡ (C.π₂ (RecTms σ))

{-# REWRITE β[]t #-}
{-# REWRITE βπ₂  #-}

postulate
  β[][]T
    : ∀{Γ Δ Σ}{A : SD.Ty Σ}{σ : SD.Tms Γ Δ}{δ : SD.Tms Δ Σ}
    → ap RecTy (SC.[][]T {Γ}{Δ}{Σ}{A}{σ}{δ}) ≡ C.[][]T

  β[id]T : ∀{Γ}{A : Ty Γ} → ap RecTy (SC.[id]T {Γ}{A}) ≡ C.[id]T


{-# REWRITE β[][]T #-}
{-# REWRITE β[id]T #-}

-- FuncNoEta

postulate
  βΠ : ∀{Γ}{A : SD.Ty Γ}{B : SD.Ty (Γ SC., A)}
     → (RecTy (SF.Π A B)) ≡ (F.Π (RecTy A) (RecTy B))

{-# REWRITE βΠ #-}

postulate
  βlam : ∀{Γ}{A : SD.Ty Γ}{B : SD.Ty (Γ SC., A)}{t : SD.Tm (Γ SC., A) B}
       → (RecTm (SF.lam t)) ≡ (F.lam (RecTm t))
  βapp : ∀{Γ}{A : SD.Ty Γ}{B : SD.Ty (Γ SC., A)}{t : SD.Tm Γ (SF.Π A B)}
       → (RecTm (SF.app t)) ≡ (F.app (RecTm t))

{-# REWRITE βlam #-}
{-# REWRITE βapp #-}
