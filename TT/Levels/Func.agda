{-# OPTIONS --without-K --no-eta #-}

module TT.Levels.Func where

open import lib

open import TT.Levels.Decl
open import TT.Levels.Core

-- Function space

record Func {d : Decl}(c : Core d) : Set where
  open Decl d
  open Core c

  field
    Π : ∀{Γ n}(A : Ty Γ n)(B : Ty (Γ , A) n) → Ty Γ n

    Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{n}{A : Ty Δ n}{B : Ty (Δ , A) n}
        → ((Π A B) [ σ ]T) ≡ (Π (A [ σ ]T) (B [ σ ^ A ]T))
        
    lam : ∀{Γ n}{A : Ty Γ n}{B : Ty (Γ , A) n} → Tm (Γ , A) B → Tm Γ (Π A B)

    app : ∀{Γ n}{A : Ty Γ n}{B : Ty (Γ , A) n} → Tm Γ (Π A B) → Tm (Γ , A) B

    lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{n}{A : Ty Δ n}{B : Ty (Δ , A) n}{t : Tm (Δ , A) B}                       
          → ((lam t) [ δ ]t) ≡[ ap (Tm _) Π[] ]≡ (lam (t [ δ ^ A ]t))
    Πβ    : ∀{Γ n}{A : Ty Γ n}{B : Ty (Γ , A) n}{t : Tm (Γ , A) B}
          → (app (lam t)) ≡ t
    Πη    : ∀{Γ n}{A : Ty Γ n}{B : Ty (Γ , A) n}{t : Tm Γ (Π A B)}
          → (lam (app t)) ≡ t

  -- abbreviations

  _$_ : ∀ {Γ n}{A : Ty Γ n}{B : Ty (Γ , A) n}(t : Tm Γ (Π A B))(u : Tm Γ A) → Tm Γ (B [ < u > ]T)
  t $ u = (app t) [ < u > ]t

  infixl 5 _$_
