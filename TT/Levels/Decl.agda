{-# OPTIONS --without-K --no-eta #-}

module TT.Levels.Decl where

open import lib

-- Declaration of a model

record Decl : Set₁ where
  field
    Con : Set
    Ty  : Con → ℕ → Set
    Tms : Con → Con → Set
    Tm  : (Γ : Con){n : ℕ} → Ty Γ n → Set
