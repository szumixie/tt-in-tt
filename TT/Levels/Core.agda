{-# OPTIONS --without-K --no-eta #-}

module TT.Levels.Core where

open import lib
open import TT.Levels.Decl

-- Core substitution calculus

record Core (d : Decl) : Set where
  open Decl d

  field
    •     : Con  -- \bub
    _,_   : (Γ : Con){n : ℕ} → Ty Γ n → Con
    
    _[_]T : ∀{Γ Δ n} → Ty Δ n → Tms Γ Δ → Ty Γ n

    id    : ∀{Γ} → Tms Γ Γ
    _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
    ε     : ∀{Γ} → Tms Γ •
    _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){n : ℕ}{A : Ty Δ n} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
    π₁    : ∀{Γ Δ n}{A : Ty Δ n} → Tms Γ (Δ , A) →  Tms Γ Δ

    _[_]t : ∀{Γ Δ n}{A : Ty Δ n} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) 
    π₂    : ∀{Γ Δ n}{A : Ty Δ n}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)

  infixl 5 _,_
  infixl 7 _[_]T
  infixl 5 _,s_
  infix  6 _∘_
  infixl 8 _[_]t

  field
    [id]T : ∀{Γ n}{A : Ty Γ n} → A [ id ]T ≡ A
    [][]T : ∀{Γ Δ Σ n}{A : Ty Σ n}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
            → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)

    idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ 
    idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ 
    ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
          → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
    ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{n : ℕ}{A : Ty Δ n}{a : Tm Γ (A [ δ ]T)}
          → ((δ ,s a) ∘ σ) ≡ ((δ ∘ σ) ,s coe (ap (Tm _) [][]T) (a [ σ ]t))
    π₁β   : ∀{Γ Δ n}{A : Ty Δ n}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
          → (π₁ (δ ,s a)) ≡ δ
    πη    : ∀{Γ Δ n}{A : Ty Δ n}{δ : Tms Γ (Δ , A)}
          → (π₁ δ ,s π₂ δ) ≡ δ
    εη    : ∀{Γ}{σ : Tms Γ •}
          → σ ≡ ε

    π₂β   : ∀{Γ Δ n}{A : Ty Δ n}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
          → (π₂ (δ ,s a)) ≡[ ap (Tm _) (ap (λ z → A [ z ]T) π₁β) ]≡ a

  -- abbreviations

  wk : ∀{Γ n}{A : Ty Γ n} → Tms (Γ , A) Γ
  wk = π₁ id

  vz : ∀{Γ n}{A : Ty Γ n} → Tm (Γ , A) (A [ wk ]T)
  vz = π₂ id

  vs : ∀{Γ n}{A B : Ty Γ n} → Tm Γ A → Tm (Γ , B) (A [ wk ]T) 
  vs x = x [ wk ]t

  <_> : ∀{Γ n}{A : Ty Γ n} → Tm Γ A → Tms Γ (Γ , A)
  < t > = id ,s coe (ap (Tm _) ([id]T ⁻¹)) t

  infix 4 <_>

  _^_ : {Γ Δ : Con}(σ : Tms Γ Δ){n : ℕ}(A : Ty Δ n) → Tms (Γ , A [ σ ]T) (Δ , A)
  _^_ = λ { σ A → (σ ∘ π₁ id) ,s coe (ap (Tm _) [][]T) (π₂ id) }

  infixl 5 _^_
