{-# OPTIONS --rewriting #-}

open import lib
open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Func.Syntax
open import TT.Base.Syntax
open import TT.TC.Syntax

module TypecheckTC where

-- well-scoped presyntax with annotated terms

data PTy (n : ℕ) : Set
data PTm (n : ℕ) : Set

data PTm n where
  pvar : Fin n → PTm n
  plam : PTy n → PTm (suc n) → PTm n -- Church version
  papp : PTm n → PTm n → PTm n
  pann : PTm n → PTy n → PTm n

data PTy n where
  pU   : PTy n
  pEl  : PTm n → PTy n
  pΠ   : PTy n → PTy (suc n) → PTy n

open import TT.ConElim

-- length of a context (TODO: cheating because the syntax of RecCon' does not use TC)

len : Con → ℕ
len = RecCon' ℕ zero suc

-- lookup a variable from a context

lookup : (Γ : Con) → Fin (len Γ) → (Σ (Ty Γ) λ A → Tm Γ A)
lookup
  = ElimCon'
      (λ Γ → Fin (len Γ) → (Σ (Ty Γ) λ A → Tm Γ A))
      (λ ())
      (λ { {Γ} {A} _ zero    →                       A [ wk ]T ,Σ vz
         ; {Γ} {A} f (suc n) → let (A ,Σ t) = f n in A [ wk ]T ,Σ vs t })

-- inference and checking

inferTm : (Γ : Con)(pt : PTm (len Γ)) → Σ (Ty Γ) λ A → Tm Γ A
checkTm : (Γ : Con)(pt : PTm (len Γ))(A : Ty Γ) → Tm Γ A
checkTy : (Γ : Con)(pA : PTy (len Γ)) → Ty Γ

inferTm Γ (pvar x) = lookup Γ x
inferTm Γ (plam pA pt) with checkTy Γ pA
... | A with inferTm (Γ , A) pt
... | B ,Σ t = Π A B ,Σ lam t
inferTm Γ (papp pt pu) with inferTm Γ pt
... | C ,Σ t with checkTm Γ pu (Π₁ C)
... | u = (Π₂ C [ < u > ]T) ,Σ (coerce C (Π (Π₁ C) (Π₂ C)) t $ u)
inferTm Γ (pann pt pA) with checkTy Γ pA
... | A = A ,Σ checkTm Γ pt A

checkTm Γ pt A with inferTm Γ pt
checkTm Γ pt A | A' ,Σ t = coerce A' A t

checkTy Γ pU = U
checkTy Γ (pEl pa) with checkTm Γ pa U
... | a = El a
checkTy Γ (pΠ pA pB) with checkTy Γ pA
... | A with checkTy (Γ , A) pB
... | B = Π A B

