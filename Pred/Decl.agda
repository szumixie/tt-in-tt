{-# OPTIONS --without-K #-}

module Pred.Decl where

open import lib
open import TT.Decl

record Con : Set₁ where
  field
    ⌜_⌝C : Set
    _ᴾC  : ⌜_⌝C → Set
  infix 7 ⌜_⌝C
open Con public

record Ty (Γ : Con) : Set₁ where
  field
    ⌜_⌝T : ⌜ Γ ⌝C → Set
    _ᴾT  : {γ : ⌜ Γ ⌝C} → (Γ ᴾC) γ → ⌜_⌝T γ → Set
  infix 7 ⌜_⌝T
open Ty public

record Tms (Γ Δ : Con) : Set₁ where
  field
    ⌜_⌝s : ⌜ Γ ⌝C → ⌜ Δ ⌝C
    _ᴾs  : {γ : ⌜ Γ ⌝C} → (Γ ᴾC) γ → (Δ ᴾC) (⌜_⌝s γ)
    -- (Γ ᴾC) is the predicate at Γ
    -- (Δ ᴾC) is the predicate at Δ
    -- for a σ, (σ ᴾs) says that ⌜ σ ⌝s preserves the predicate: if
    -- the predicate is true for the input (at Γ) then it is true for
    -- the output (at Δ)
  infix 7 ⌜_⌝s
open Tms public

record Tm (Γ : Con)(A : Ty Γ) : Set₁ where
  field
    ⌜_⌝t : (γ : ⌜ Γ ⌝C) → ⌜ A ⌝T γ
    _ᴾt  : {γ : ⌜ Γ ⌝C}(γₚ : (Γ ᴾC) γ) → (A ᴾT) γₚ (⌜_⌝t γ)
  infix 7 ⌜_⌝t
open Tm public

pDecl : Decl
pDecl = decl Con Ty Tms Tm

-- Con
-- _,_   \
--        -> determine Ty
-- _[_]T /
-- _,s_   -> determines Tm
-- ...
-- rest of Core
-- Func
-- Sigma
-- ExtIden

-- if you get stuck, look at Standard, Graph
