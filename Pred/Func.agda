{-# OPTIONS --without-K #-}

module Pred.Func where

open import lib
open import TT.Func
open import Pred.Decl
open import Pred.Core

private

  Π : ∀{Γ}(A : Ty Γ) → Ty (Γ , A) → Ty Γ
  ⌜ Π A B ⌝T γ    = (a : ⌜ A ⌝T γ) → ⌜ B ⌝T (γ ,Σ a)
  (Π A B ᴾT) γₚ f = ∀{a}(aₚ : (A ᴾT) γₚ a) → (B ᴾT) (γₚ ,Σ aₚ) (f a)

  lam : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)
  ⌜ lam t ⌝t γ  a  = ⌜ t ⌝t (γ ,Σ a)
  (lam t ᴾt) γₚ aₚ = (t ᴾt) (γₚ ,Σ aₚ)

  app : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B
  ⌜ app t ⌝t (γ  ,Σ a)  = ⌜ t ⌝t γ a
  (app t ᴾt) (γₚ ,Σ aₚ) = (t ᴾt) γₚ aₚ

pFunc : Func pCore
pFunc = record
  { Π     = Π
  ; Π[]   = refl
  ; lam   = lam
  ; app   = app
  ; lam[] = refl
  ; Πβ    = refl
  ; Πη    = refl
  }

open Func pFunc public
