{-# OPTIONS --without-K #-}

module Pred.Core where

open import lib
open import TT.Core
open import Pred.Decl

private

  • : Con
  ⌜ • ⌝C    = ⊤
  (• ᴾC) tt = ⊤

  _,_ : (Γ : Con) → Ty Γ → Con
  ⌜ Γ , A ⌝C            = Σ ⌜ Γ ⌝C ⌜ A ⌝T
  ((Γ , A) ᴾC) (γ ,Σ a) = Σ ((Γ ᴾC) γ) λ γₚ → (A ᴾT) γₚ a

  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
  ⌜ A [ σ ]T ⌝T   γ    = ⌜ A ⌝T (⌜ σ ⌝s γ)
  ((A [ σ ]T) ᴾT) γₚ a = (A ᴾT) ((σ ᴾs) γₚ) a

  id : ∀{Γ} → Tms Γ Γ
  ⌜ id ⌝s γ  = γ
  (id ᴾs) γₚ = γₚ

  _∘_ : ∀{Γ Δ Θ} → Tms Δ Θ → Tms Γ Δ → Tms Γ Θ
  ⌜ σ ∘ τ ⌝s   γ  = ⌜ σ ⌝s (⌜ τ ⌝s γ)
  ((σ ∘ τ) ᴾs) γₚ = (σ ᴾs) ((τ ᴾs) γₚ)

  ε : ∀{Γ} → Tms Γ •
  ⌜ ε ⌝s _ = tt
  (ε ᴾs) _ = tt

  _,s_ : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
  ⌜ σ ,s t ⌝s   γ  = ⌜ σ ⌝s γ ,Σ ⌜ t ⌝t γ
  ((σ ,s t) ᴾs) γₚ = (σ ᴾs) γₚ ,Σ (t ᴾt) γₚ

  π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ
  ⌜ π₁ σ ⌝s γ  = proj₁ (⌜ σ ⌝s γ)
  (π₁ σ ᴾs) γₚ = proj₁ ((σ ᴾs) γₚ)

  π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)
  ⌜ π₂ σ ⌝t γ  = proj₂ (⌜ σ ⌝s γ)
  (π₂ σ ᴾt) γₚ = proj₂ ((σ ᴾs) γₚ)

  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
  ⌜ t [ σ ]t ⌝t   γ  = ⌜ t ⌝t (⌜ σ ⌝s γ)
  ((t [ σ ]t) ᴾt) γₚ = (t ᴾt) ((σ ᴾs) γₚ)

pCore : Core pDecl
pCore = record
  { c1 = record
    { •     = •
    ; _,_   = _,_
    ; _[_]T = _[_]T
    ; id    = id
    ; _∘_   = _∘_
    ; ε     = ε
    ; _,s_  = _,s_
    ; π₁    = π₁
    ; _[_]t = _[_]t
    ; π₂    = π₂
    }
  ; c2 = record
    { [id]T = refl
    ; [][]T = refl
    ; idl   = refl
    ; idr   = refl
    ; ass   = refl
    ; ,∘    = refl
    ; π₁β   = refl
    ; πη    = refl
    ; εη    = refl
    ; π₂β   = refl
    }
  }

open Core pCore public
