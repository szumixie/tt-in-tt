module Pred.ExtIden where

open import lib
open import TT.ExtIden
open import Pred.Decl
open import Pred.Core

private

  Id : {Γ : Con}{A : Ty Γ} → Tm Γ A → Tm Γ A → Ty Γ
  ⌜ Id t u ⌝T γ = ⌜ t ⌝t γ ≡ ⌜ u ⌝t γ
  (Id {A = A} t u ᴾT) γₚ p = (t ᴾt) γₚ ≡[ ap ((A ᴾT) _) p ]≡ (u ᴾt) γₚ

  ref : ∀{Γ A}(t : Tm Γ A) → Tm Γ (Id t t)
  ⌜ ref _ ⌝t _ = refl
  (ref _ ᴾt) _ = refl

  reflect : ∀{Γ A}{t u : Tm Γ A} → Tm Γ (Id t u) → t ≡ u
  reflect t = {!!}

pExtIden : ExtIden pCore
pExtIden = record
  { Id      = Id
  ; Id[]    = refl
  ; ref     = ref
  ; ref[]   = refl
  ; reflect = reflect
  }

open ExtIden pExtIden public
