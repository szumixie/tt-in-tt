module Pred.Sigma where

open import lib
open import TT.Sigma
open import Pred.Decl
open import Pred.Core

private
  Σ' : ∀{Γ}(A : Ty Γ) → Ty (Γ , A) → Ty Γ
  ⌜ Σ' A B ⌝T γ           = Σ (⌜ A ⌝T γ) λ a → ⌜ B ⌝T (γ ,Σ a)
  (Σ' A B ᴾT) γₚ (a ,Σ b) = Σ ((A ᴾT) γₚ a) λ aₚ → (B ᴾT) (γₚ ,Σ aₚ) b

  _,Σ'_ : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm Γ A) → Tm Γ (B [ < t > ]T) → Tm Γ (Σ' A B)
  ⌜ t ,Σ' u ⌝t   γ  = ⌜ t ⌝t γ ,Σ ⌜ u ⌝t γ
  ((t ,Σ' u) ᴾt) γₚ = (t ᴾt) γₚ ,Σ (u ᴾt) γₚ

  proj₁' : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Σ' A B) → Tm Γ A
  ⌜ proj₁' t ⌝t γ  = proj₁ (⌜ t ⌝t γ)
  (proj₁' t ᴾt) γₚ = proj₁ ((t ᴾt) γₚ)

  proj₂' : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm Γ (Σ' A B)) → Tm Γ (B [ < proj₁' t > ]T)
  ⌜ proj₂' t ⌝t γ  = proj₂ (⌜ t ⌝t γ)
  (proj₂' t ᴾt) γₚ = proj₂ ((t ᴾt) γₚ)

pSigma : Sigma pCore
pSigma = record
  { Σ'     = Σ'
  ; Σ[]    = refl
  ; _,Σ'_  = _,Σ'_
  ; proj₁' = proj₁'
  ; proj₂' = proj₂'
  ; Σβ₁    = refl
  ; Σβ₂    = refl
  ; Ση     = refl
  ; ,Σ[]   = refl
  }

open Sigma pSigma public
