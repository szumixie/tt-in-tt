\documentclass[final,hyperref={pdfpagelabels=false}]{beamer}

%include lhs2TeX.fmt
%include agda.fmt
%include lib.fmt

\usepackage[orientation=portrait,size=a1,scale=1.1]{beamerposter}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{booktabs}

\usetheme{Boadilla}
%\usecolortheme{seahorse}

\setbeamertemplate{footline}{}
\setbeamertemplate{navigation symbols}{}
%\setbeamersize{sidebar width left=3em}
%\setbeamersize{sidebar width right=3em}
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize} %to set the body size

%----------------------------------------------------------------------------------------

\begin{document}

\addtobeamertemplate{block end}{}{\vspace*{2ex}}

\begin{frame}[t]

%----------------------------------------------------------------------------------------

\begin{columns}[t]

\begin{column}{.84\textwidth}

\begin{block}{\centering \Huge{Type Theory in Type Theory using Quotient Inductive Types$^*$}}
\begin{center}
{
Thorsten Altenkirch and Ambrus Kaposi \\
School for Computer Science, University of Nottingham, United Kingdom\\
\{txa, auk\}\verb$@$cs.nott.ac.uk}
\end{center}
\end{block}

\end{column}

\begin{column}{.09\textwidth}

\begin{block}{}
\includegraphics[scale=0.229]{aec-badge-popl}    
\end{block}

\end{column}

\end{columns}

%----------------------------------------------------------------------------------------

\begin{columns}[t]

\begin{column}{.958\textwidth}

\begin{block}{Abstract}
  We present an internal formalisation of a type theory with dependent
  types in Type Theory using a special case of higher inductive types
  from Homotopy Type Theory which we call quotient inductive types
  (QITs). Our formalisation of type theory avoids referring to
  preterms or a typability relation but defines directly well typed
  objects by an inductive definition. We use the elimination principle
  to define the set-theoretic and logical predicate
  interpretation. The work has been formalized using the Agda system
  extended with QITs using postulates.
%\vspace{-1em}
\end{block}

\end{column}

\end{columns}

%----------------------------------------------------------------------------------------

\begin{columns}[t]

\begin{column}{.465\textwidth}

\begin{block}{Quotient Inductive Types (QITs)}

QITs are a special case of Higher Inductive Types in a strict type
theory where all higher path spaces are trivial. They allow the
definition of usual constructors and equality constructors \emph{at
  the same time}. We can simulate them in Agda by postulating the
equality constructors and defining the eliminator. An example is the
type of infinitely branching trees where the actual order of subtrees
doesn't matter. The definition below is not the same as quotienting
infinite branching trees because we were not able to lift the |node|
constructor to the quotient.
  
\begin{spec}
data T  :  Set where
  leaf  :  T
  node  :  (ℕ → T) → T
postulate
  perm  :  (g : ℕ → T)(f : ℕ → ℕ) → isIso f
        →  node g ≡ node (g ∘ f)   

module ElimT
  (Tᴹ     :  T → Set)
  (leafᴹ  :  Tᴹ leaf)
  (nodeᴹ  :  {f : ℕ → T}(fᴹ : (n : ℕ) → Tᴹ (f n))
          →  Tᴹ (node f))
  (permᴹ  :  {g : ℕ → T}(gᴹ : (n : ℕ) → Tᴹ (g n))
             (f : ℕ → ℕ)(p : isIso f)
          →  nodeᴹ gᴹ ≡[ ap Tᴹ (perm g f p) ]≡ nodeᴹ (gᴹ ∘ f))
  where

  Elim : (t : T) → Tᴹ t
  Elim leaf = leafᴹ
  Elim (node f) = nodeᴹ (λ n → Elim (f n))
\end{spec}
\end{block}

%----------------------------------------------------------------------------------------

\begin{block}{Results}
  \begin{itemize}
  \item We have for the first time presented a workable internal
    syntax of Type Theory which only features typed objects.
  \item We implemented the following models:
    \begin{itemize}
    \item Standard model (metacircular interpretation)
    \item Logical predicate interpretation (Bernardy-Jansson-Paterson,
      2012)
    \item Presheaf model
    \item In preparation: Normalisation by Evaluation
    \end{itemize}
  \end{itemize}
\end{block}

%----------------------------------------------------------------------------------------

\begin{block}{Template Type Theory}
\begin{itemize}
  \item Internalising type theory opens the possibility of
    \emph{template type theory}. An interpretation of type theory can
    be given as an algebra for the syntax and the interpretation of
    new constants in this algebra. We can then interpret code using
    these new principles by interpreting it in the given algebra. The
    new code can use all the conveniences of the host system such as
    implicit arguments and definable syntactic extensions.
  \item Some possible applications:
    \begin{itemize}
    \item Using presheaf models to justify guarded type theory.
    \item Modelling the local state monad (Haskell's STM monad)
    \item Computational explanation of Homotopy Type Theory by the
      cubical set model
    \item Derivation of parametricity results using the logical
      predicate interpretation
    \item Generic programming
    \end{itemize}
\end{itemize}
%\vspace{-2em}
\end{block}
  
%----------------------------------------------------------------------------------------

\begin{block}{}
∗ Supported by EPSRC grant EP/M016951/1.
\end{block}

\end{column}

%\begin{column}{.01\textwidth}\end{column}
 
\begin{column}{.465\textwidth}

%----------------------------------------------------------------------------------------

\begin{block}{Syntax of Type Theory}

The syntax is presented as a Quotient Inductive Inductive
Type. Signature of the types representing the syntax:
\begin{spec}
data Con  : Set
data Ty   : Con → Set
data Tms  : Con → Con → Set
data Tm   : ∀ Γ → Ty Γ → Set
\end{spec}
Constructors for contexts, types, substitutions and terms:
\begin{spec}
data Con where
  •      : Con
  _,_    : (Γ : Con) → Ty Γ → Con

data Ty where
  _[_]T  : Ty Δ → Tms Γ Δ → Ty Γ
  Π      : (A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
  U      : Ty Γ
  El     : (A : Tm Γ U) → Ty Γ
  
data Tms where
  ε      :  Tms Γ •
  _,_    :  (δ : Tms Γ Δ) → Tm Γ (A [ δ ]T) →  Tms Γ (Δ , A)
  id     :  Tms Γ Γ
  _∘_    :  Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  π₁     :  Tms Γ (Δ , A) →  Tms Γ Δ

data Tm where
  _[_]t  :  Tm Δ A → (δ : Tms Γ Δ) →  Tm Γ (A [ δ ]T) 
  π₂     :  (δ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ δ ]T)
  app    :  Tm Γ (Π A B) → Tm (Γ , A) B
  lam    :  Tm (Γ , A) B → Tm Γ (Π A B)
\end{spec}
Equality constructors for types, substitutions and terms:
\begin{spec}
postulate -- Ty
  [id]T  :  A [ id ]T ≡ A
  [][]T  :  A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T
  U[]    :  U [ δ ]T ≡ U
  El[]   :  El A [ δ ]T ≡ El (coe (TmΓ≡ U[]) (A [ δ ]t))
_ ^ _  :  (δ : Tms Γ Δ)(A : Ty Δ) →  Tms (Γ , A [ δ ]T) (Δ , A)
δ ^ A = (δ ∘ π₁ id) , coe (TmΓ≡ [][]T) (π₂ id)
  Π[]    :  (Π A B) [ δ ]T ≡ Π (A [ δ ]T) (B [ δ ^ A ]T) 
postulate -- Tms
  idl    : id ∘ δ ≡ δ 
  idr    : δ ∘ id ≡ δ 
  ass    : (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
  ,∘     : (δ , t) ∘ σ ≡ (δ ∘ σ) , coe (TmΓ≡ [][]T) (t [ σ ]t)
  π₁β    : π₁ (δ , t) ≡ δ
  πη     : (π₁ δ , π₂ δ) ≡ δ
  εη     : {σ : Tms Γ •} → σ ≡ ε
postulate -- Tm
  [id]t  : t [ id ]t ≡[ TmΓ≡ [id]T ]≡ t
  [][]t  : (t [ δ ]t) [ σ ]t ≡[ TmΓ≡ [][]T ]≡  t [ δ ∘ σ ]t
  π₂β    : π₂ (δ , a) ≡[ TmΓ≡ (ap (_[_]T A) π₁β) ]≡ a
  Πβ     : app (lam t) ≡ t
  Πη     : lam (app t) ≡ t
  lam[]  : (lam t) [ δ ]t ≡[ TmΓ≡ Π[] ]≡ lam (t [ δ ^ A ]t)
\end{spec}
\end{block}

\end{column}

\end{columns}

\end{frame} % End of the enclosing frame

\end{document}
