\documentclass{beamer}
\usetheme{default}
\useoutertheme{infolines}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{tikz}
\usepackage[utf8]{inputenc}
\usepackage{pmboxdraw}
\usepackage{csquotes}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows}

\usepackage{scrextend}
%\changefontsizes{14pt}

\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\setbeamerfont{itemize/enumerate subsubbody}{size=\normalsize}

\DeclareUnicodeCharacter{393}{$\Gamma$}
\DeclareUnicodeCharacter{2192}{$\ra$}
\DeclareUnicodeCharacter{1D39}{${^\M}$}
\DeclareUnicodeCharacter{3B2}{$\beta$}
\DeclareUnicodeCharacter{2261}{$\equiv$}
\DeclareUnicodeCharacter{2081}{$_1$}

\input{abbrevs.tex}

\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}

\begin{document}

%---------------------------------------------------------------------

\begin{frame}[plain]
  \vspace{0.5cm}
  \begin{center}
    \textcolor{blue}{
      {\Large Normalisation by Evaluation for \\ a Type Theory with Large
        Elimination \\\vspace{1em}}
    }
    
    \vspace{0.7cm}

    Ambrus Kaposi \\
    E{\"o}tv{\"o}s Lor{\'a}nd University, Budapest \\

    \vspace{0.7cm}
    
    j.w.w. Thorsten Altenkirch and Andr{\'a}s Kov{\'a}cs \\

    \vspace{0.7cm}

    TYPES 2017, Budapest, \\
    30 May 2017
  \end{center}
\end{frame}



%---------------------------------------------------------------------

\begin{frame}{What are natural numbers? (i)}
  A model of natural numbers is an element of the following record.
  \begin{alignat*}{4}
    & \N && : \Set \\
    & \zero && : \N \\
    & \suc && : \N \ra \N
  \end{alignat*}
  A morphism of two models $M$ and $N$.
  \begin{alignat*}{4}
    & \underline{\N} && : \N_M \ra \N_N \\
    & \underline{\zero} && : \underline{\N}\,\zero_M \equiv \zero_N \\
    & \underline{\suc} && : \underline{\N}\,(\suc_M\,n) \equiv \suc_N\,(\underline{\N}\,n)
  \end{alignat*}
  The inductive type of natural numbers is a model $S$ and for any
  model $M$, a morphism from $S$ to $M$ such that the equalities are
  definitional.
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{What are natural numbers? (ii)}
  A dependent model of natural numbers over a model $M$ is an element
  of the following record.
  \begin{alignat*}{4}
    & \N^\D && : \N_M \ra \Set \\
    & \zero^\D && : \N^\D\,\zero_M \\
    & \suc^\D && : \N^\D\,n \ra \N^\D\,(\suc_M\,n)
  \end{alignat*}
  A dependent morphism from $M$ to $\D$.
  \begin{alignat*}{4}
    & \underline{\N} && : (n : \N_M) \ra \N^\D\,n \\
    & \underline{\zero} && : \underline{\N}\,\zero_M \equiv \zero^\D \\
    & \underline{\suc} && : \underline{\N}\,(\suc_M\,n) \equiv \suc^\D\,(\underline{\N}\,n)
  \end{alignat*}
  The inductive types of natural numbers is a model $S$ and for any
  dependent model $\D$, a dependent morphism from $S$ to $\D$.
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{What is type theory? (in Agda) (i)}
  \vspace{0.5em}
  A model of type theory is an element of the following record (CwF).
  \fontsize{8}{8.2}\selectfont
  \begin{alignat*}{4}
    & \Con && : \Set \\
    & \Ty && : \Con \ra \Set \\
    & \Tms && : \Con \ra \Con \ra \Set \\
    & \Tm && : (\Gamma:\Con)\ra \Ty\,\Gamma\ra \Set \\\hline
    & \cdot && : \Con && 												  [\id] && : A[\id] \equiv A \\
    & \blank,\blank && : (\Gamma : \Con) \ra \Ty \, \Gamma \ra \Con && 						  [][] && : A[\sigma][\nu] \equiv A[\sigma \circ \nu] \\
    & \blank[\blank] && : \Ty \, \Delta \ra \Tms \, \Gamma \, \Delta \ra \Ty \, \Gamma && 		          {\id\circ} && : \id \circ \sigma \equiv \sigma \\
    & \id && : \Tms \, \Gamma \, \Gamma && 							                  {\circ\id} && : \sigma \circ \id \equiv \sigma \\
    & \blank\circ\blank && : \Tms\,\Theta\,\Delta \ra \Tms\,\Gamma\,\Theta \ra \Tms\,\Gamma\,\Delta && 		  {\circ\circ} && : (\sigma \circ \nu) \circ \delta \equiv \sigma \circ (\nu \circ \delta) \\  
    & \epsilon && : \Tms \, \Gamma \, \cdot && 									  \epsilon\eta && : \{\sigma : \Tms\,\Gamma\,\cdot\} \ra \sigma \equiv \epsilon \\
    & \blank,\blank && : (\sigma : \Tms\,\Gamma\,\Delta) \ra \Tm\,\Gamma\,A[\sigma] \ra \Tms\,\Gamma\,(\Delta,A) \hspace{1em} && \pi_1\beta && : \pi_1\,(\sigma, t) \equiv \sigma \\
    & \pi_1 && : \Tms\,\Gamma\,(\Delta,A) \ra \Tms\,\Gamma\,\Delta &&                                               \pi\eta && : (\pi_1\,\sigma, \pi_2\,\sigma) \equiv \sigma \\
    & \blank[\blank] && : \Tm\,\Delta\,A \ra (\sigma : \Tms\,\Gamma\,\Delta) \ra \Tm\,\Gamma\,A[\sigma] &&          {,\circ} && : (\sigma, t) \circ \nu \equiv (\sigma \circ \nu) , (\tr{[][]}{t[\nu]}) \\
    & \pi_2 && : (\sigma : \Tms\,\Gamma\,(\Delta,A)) \ra \Tm\,\Gamma\,A[\pi_1 \, \sigma] &&                         \pi_2\beta && : \pi_2\,(\sigma, t) \equiv^{\pi_1\beta} t \\\hline
    & \Pi && : (A : \Ty \, \Gamma) \ra \Ty \, (\Gamma , A) \ra \Ty \, \Gamma &&                                     \Pi[] && : (\Pi\,A\,B)[\sigma] \equiv \Pi\,A[\sigma]\,B[\sigma\uparrow] \\
    & \lam && : \Tm\,(\Gamma,A)\,B \ra \Tm\,\Gamma\,(\Pi\,A\,B) &&                                                  \Pi\beta && : \app\,(\lam\,t) \equiv t \\
    & \app && : \Tm\,\Gamma\,(\Pi\,A\,B) \ra \Tm\,(\Gamma,A)\,B &&                                                  \Pi\eta && : \lam\,(\app\,t) \equiv t \\
    & && &&                                                                                                         \lam[] && : (\lam\,t)[\sigma] \equiv^{\Pi[]} \lam\,(t[\sigma\uparrow])
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{What is type theory? (ii)}
  \vspace{0.5em} A morphism between models $M$ and $N$ is an element
  of the following record.
  {\fontsize{8}{8.2}\selectfont
    \begin{alignat*}{6}
      & \underline{\Con} && : \Con_M \ra \Con_N \\
      & \underline{\Ty} && : \Ty_M\,\Gamma \ra \Ty_N\,(\underline{\Con}\,\Gamma) \\
      & \underline{\Tms} && : \Tms_M\,\Gamma\,\Delta \ra \Tms_N\,(\underline{\Con}\,\Gamma)\,(\underline{\Con}\,\Delta) \\
      & \underline{\Tm} && : \Tm_M\,\Gamma\,A \ra \Tm_N\,(\underline{\Con}\,\Gamma)\,(\underline{\Ty}\,A) \\\hline
      & \underline{\cdot} && : \underline{\Con}\,\cdot_M \equiv \cdot_N \\
      & \underline{\blank,\blank} && : \underline{\Con}\,(\Gamma,_M A) \equiv (\underline{\Con}\,\Gamma),_N (\underline{\Ty}\,A) \\
      & \underline{\blank[\blank]} && : \underline{\Ty}\,(A[\sigma]_M) \equiv (\underline{\Ty}\,A)[\underline{\Tms}\,\sigma]_N \\
      & \underline{\id} && : \underline{\Tms}\,\id_M \equiv \id_N \\
      & \underline{\circ} && : \underline{\Tms}\,(\sigma\circ_M\,\nu) \equiv (\underline{\Tms}\,\sigma)\circ_N(\underline{\Tms}\,\nu) \\
      & \underline{\Pi} && : \underline{\Ty}\,(\Pi_M\,A\,B) \equiv \Pi_N\,(\underline{\Ty}\,A)\,(\underline{\Ty}\,B) \hspace{20em} \\
      & ...
  \end{alignat*}}
  We write $f_M$ for the field $f$ of $M$.
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{What is type theory? (iii)}
  \vspace{0.5em} Given a model $M$, a dependent model over $M$ is an
  element of the following record.  {\fontsize{8}{8.2}\selectfont
    \begin{alignat*}{4}
      & \Con^\D && : \Con_M\ra\Set \\
      & \Ty^\D && : \Con^\D\,\Gamma \ra \Ty_M\,\Gamma\ra \Set \\
      & \Tms^\D && : \Con^\D\,\Gamma\ra \Con^\D\,\Delta \ra \Tms_M\,\Gamma\,\Delta \ra \Set \\
      & \Tm^\D && : (\Gamma^\D : \Con^\D\,\Gamma) \ra \Ty^\D\,\Gamma^\D\,A \ra \Tm_M\,\Gamma\,A\ra \Set \\\hline
      & \cdot^\D && : \Con^\D\,\cdot_M \\
      & \blank,^\D\blank && : (\Gamma^\D : \Con^\D\,\Gamma) \ra \Ty^\D\,\Gamma^\D\,A \ra \Con^\D\,(\Gamma,_M A) \\
      & \blank[\blank]^\D && : \Ty^\D \, \Delta^\D\,A \ra \Tms^\D \, \Gamma^\D \, \Delta^\D\,\sigma \ra \Ty^\D \, \Gamma^\D\,(A[\sigma]_M) \\
      & \id^\D && : \Tms \, \Gamma^\D \, \Gamma^\D\,\id_M \\
      & \blank\circ^\D\blank && : \Tms^\D\,\Theta^\D\,\Delta^\D\,\sigma \ra \Tms^\D\,\Gamma^\D\,\Theta^\D\,\nu \ra \Tms^\D\,\Gamma^\D\,\Delta^\D\,(\sigma\circ_M \nu) \\
      & \Pi^\D && : (A^\D : \Ty^\D \, \Gamma^\D\,A) \ra \Ty^\D \, (\Gamma^\D ,^\D A^\D\,B) \ra \Ty^\D \, \Gamma^\D\,(\Pi_M\,A\,B) \\
      & ...
  \end{alignat*}}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{What is type theory? (iv)}
  \begin{itemize}
  \item The syntax is:
    \begin{itemize}
    \item a model $S$
    \item together with a recursion principle:
      \begin{itemize}
      \item for any model $M$, there is a a morphism from $S$ to $M$
    \end{itemize}
    \item and an induction principle:
      \begin{itemize}
      \item for any dependent model $D$ over the syntax, there is a
        dependent morphism from $S$ to $D$
      \end{itemize}
    \end{itemize}
  \item This is an intrisic syntax (only well-typed terms)
  \item Also, it is quotiented by conversion
  \item It is an instance of a higher inductive inductive type
    (quotient inductive inductive type)
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Standard model (set model)}
  \begin{alignat*}{4}
    & \Con_{M} && := \Set \\
    & \Ty_{M}\,\ll\Gamma\rr && := \ll\Gamma\rr\ra\Set \\
    & \Tms_{M}\,\ll\Gamma\rr\,\ll\Delta\rr && := \ll\Gamma\rr \ra \ll\Delta\rr \\
    & \Tm_{M}\,\ll\Gamma\rr\,\ll A\rr && := (\alpha:\ll\Gamma\rr)\ra \ll A\rr\,\alpha \\\hline
    & \cdot_{M} && := \top \\
    & \ll\Gamma\rr,_{M}\ll A\rr && := \Sigma(\alpha:\ll\Gamma\rr).\ll A\rr\,\alpha \\
    & \id_{M} && := \lambda \alpha.\alpha \\
    & \Pi_{M}\,A\,B\,\alpha && := (x:\ll A \rr\,\alpha)\ra \ll B\rr\,(\alpha,x) \\
    & ...
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Presheaf model}
  Fix a category $\C$. We define a model $\hat{C}$.
  \begin{alignat*}{4}
    & \Con_{\hat{\C}} && := \C^\op \ra \Set \text{ functor} \\
    & \Ty_{\hat{\C}}\,\hat{\Gamma} && := \text{family of presheaves over } \hat{\Gamma} \\
    & \Tms_{\hat{\C}}\,\hat{\Gamma}\,\hat{\Delta} && := \hat{\Gamma} \nat \hat{\Delta} \\
    & \Tm_{\hat{\C}}\,\hat{\Gamma}\,\hat{A} && := \text{section of presheaves from $\hat{\Gamma}$ to  $\hat{A}$} \\\hline
    & \cdot_{\hat{\C}} && := \text{constant $\top$ presheaf} \\
    & \blank,_{\hat{\C}}\blank && := \text{pointwise dependent product of presheaves} \\
    & \id_{\hat{\C}} && := \text{identity natural transformation} \\
    & \Pi_{\hat{\C}} && := \text{presheaf exponential} \\
    & ...
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Normalisation by Evaluation}
  \begin{center}
  \begin{tikzpicture}
  \node (Syntax) at (2,4.3) {Syntax ($S$)};
  \node (Model) at (8,4.2) {Model};
  \node (NF) at (2,3) {Normal forms};
  \draw (2,2) circle (2cm);
  \draw (2,2) circle (1.2cm);
  \draw (8,2) circle (1.8cm);
  \draw[thick,->] (4,2.6) -- node[above] {recursor} (6.2,2.6);
  \draw[thick,->] (6.2,1.4) -- node[above] {quote} (3.1,1.4);
  \end{tikzpicture}
  \end{center}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Normalisation by Evaluation for simple type theory}
  $\R$ is the category of renamings: objects are contexts, morphisms
  are lists of variables.

  We use the model $\hat{\R}$, then we define a quote function.
  \begin{center}
  \begin{tikzpicture}
  \node (Syntax) at (2,4.3) {$S$};
  \node (Model) at (8,4.2) {$\hat{\R}$};
  \node (NF) at (2,3) {Normal forms};
  \draw (2,2) circle (2cm);
  \draw (2,2) circle (1.2cm);
  \draw (8,2) circle (1.8cm);
  \draw[thick,->] (4,2.6) -- node[above] {recursor} (6.2,2.6);
  \draw[thick,->] (6.2,1.4) -- node[above] {quote} (3.1,1.4);
  \end{tikzpicture}
  \end{center}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Normalisation by Evaluation for dependent type theory}
  First we define a morphism $\Y$ (Yoneda embedding).
  \begin{columns}
  \begin{column}{0.5\textwidth}
      \begin{center}
  \begin{tikzpicture}
  \node (Syntax) at (0,0.8) {$S$};
  \node (Model) at (3,0.8) {$\hat{\R}$};
  \draw (0,0) circle (0.5cm);
  \draw (3,0) circle (0.5cm);
  \draw[thick,->] (0.6,0) -- node[above] {$\Y$} (2.4,0);
  \end{tikzpicture}
       \end{center}
  \end{column}
  \begin{column}{0.5\textwidth}
  \begin{alignat*}{6}
    & \underline{\Con}_\Y\,\Gamma && := \lambda\Psi.\Tms\,\Psi\,\Gamma \\
    & \underline{\Ty}_\Y\,A && := \lambda\Psi\,\rho.\Tm\,\Psi\,A[\rho] \\
%    & \underline{\Tms}_\Y\,\sigma && := \lambda\Psi\,\rho.\sigma\circ\rho \\
    & \underline{\Tm}_\Y\,t && := \lambda\Psi\,\rho.t[\rho]
  \end{alignat*}
  \end{column}
  \end{columns}
\vspace{1em}
  Then we define a dependent model $D$ over $S$:
  \begin{alignat*}{4}
    & \Con^\D\,\Gamma && := \Ty_{\hat{\R}}\,\big(\underline{\Con}_\Y\,\Gamma\big) \\
    & \Ty^\D\,\Gamma^\D\,A && := \Ty_{\hat{\R}}\,\big(\underline{\Con}_\Y\,\Gamma ,_{\hat{\R}} \underline{\Ty}_\Y\,A ,_{\hat{\R}} \Gamma^\D[\wk_{\hat{\R}}]_{\hat{\R}}\big) \\
%    & \Tms^\D\,\Gamma^\D\,\Delta^\D\,\sigma && := \Tm_{\hat{\R}}\,(\underline{\Con}_\Y\,\Gamma,_{\hat{\R}} \Gamma^\D)\,\big(\Delta^\D[\underline{\Tms}_\Y\,\sigma]_{\hat{\R}}[\wk_{\hat{\R}}]_{\hat{\R}}\big) \\
    & \Tm^\D\,\Gamma^\D\,A^\D\,t && := \Tm_{\hat{\R}}\,\big(\underline{\Con}_\Y\,\Gamma,_{\hat{\R}} \Gamma^\D\big)\,\big(A^\D[\underline{\Tm}_\Y\,t\uparrow_{\hat{\R}}]_{\hat{\R}}\big)
  \end{alignat*}
  And finally, we define quote.
  \begin{center}
  \begin{tikzpicture}
  \node (Syntax) at (0,0.8) {$S$};
  \node (Model) at (3,0.8) {$D$};
  \draw (0,0) circle (0.5cm);
  \draw (0,0) circle (0.3cm);
  \draw (3,0) circle (0.5cm);
  \draw[thick,->] (0.6,0.1) -- node[above] {eliminator} (2.4,0.1);
  \draw[thick,->] (2.4,-0.1) -- node[below] {quote} (0.32,-0.1);
  \end{tikzpicture}
  \end{center}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Normalisation by Evaluation for type theory with a universe}
  The dependent model includes quote. Unfolded definitions excluding
  functoriality:
  \begin{alignat*}{6}
    & \Con^\D\,\Gamma := \forall\Psi.(\rho:\Tms\,\Psi\,\Delta)\ra \Set \\
    & \\
    & \Ty^\D\,\Gamma^\D\,A := \forall\Psi.(\rho:\Tms\,\Psi\,\Delta)\ra\Gamma^\D\,\Psi\,\rho.\r\ra(t : \Tm\,\Psi\,A[\rho]) \\
    & \hspace{2em} \ra (\r:\Set)\times(\q : \r \ra \isNf\,\Psi\,A[\rho]\,t)\times(\u:\isNe\,\Psi\,A[\rho]\,t \ra r) \\
    & \\
    & \Tm^\D\,\Gamma^\D\,A^\D\,t := \forall\Psi.(\rho:\Tms\,\Psi\,\Gamma)(q : \Gamma^\D\,\Psi\,\rho.\r) \ra A^\D\,\Psi\,\rho\,q\,(t[\rho]).\r
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Propaganda}
  Use our formalisation! Alternative to Andrej's one.

  We implemented:
  \begin{itemize}
  \item Syntax: $\Pi$, $\Sigma$, $\Bool$, $\N$, $\Id$, $\0$, $\1$,
    universe closed under these
  \item Models: standard (all type formers), presheaf, setoid
  \item Translation: logical predicates for parametericity
  \item Dependent model: presheaf logical predicates, normalisation by
    evaluation
  \item We have an issue system, you can resolve some/create more
  \item Contributors: Thorsten Altenkirch, Simon Boulier, Fredrik
    Nordvall Forsberg, Ambrus Kaposi, Andr{\'a}s Kov{\'a}cs, Gun Pinyo
  \end{itemize}

  \vspace{3em}
  \url{http://bitbucket.org/akaposi/tt-in-tt}
\end{frame}

%---------------------------------------------------------------------

\end{document}
