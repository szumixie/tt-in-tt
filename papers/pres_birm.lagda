\documentclass{beamer}
\usetheme{default}
\useoutertheme{infolines}
\usepackage[greek,english]{babel}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{tikz}

\usepackage{scrextend}
\changefontsizes{14pt}

%include lhs2TeX.fmt
%include agda.fmt
%include lib.fmt

\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\setbeamerfont{itemize/enumerate subsubbody}{size=\normalsize}

\newcommand{\U}{\mathsf{U}}
\newcommand{\El}{\mathsf{El}}
\newcommand{\REN}{\mathsf{REN}}
\newcommand{\op}{\mathsf{op}}
\newcommand{\ra}{\rightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\Set}{\mathsf{Set}}
\newcommand{\PSh}{\mathsf{PSh}}
\newcommand{\FamPSh}{\mathsf{FamPSh}}
\renewcommand{\ll}{\llbracket}
\providecommand{\rr}{\rrbracket}
\newcommand{\Con}{\mathsf{Con}}
\newcommand{\Ty}{\mathsf{Ty}}
\newcommand{\Tm}{\mathsf{Tm}}
\newcommand{\Tms}{\mathsf{Tms}}
\newcommand{\R}{\mathsf{R}}
\newcommand{\TM}{\mathsf{TM}}
\newcommand{\NE}{\mathsf{NE}}
\newcommand{\NF}{\mathsf{NF}}
\newcommand{\q}{\mathsf{q}}
\renewcommand{\u}{\mathsf{u}}
\renewcommand{\ne}{\mathsf{ne}}
\newcommand{\nf}{\mathsf{nf}}
\newcommand{\lQ}{\mathsf{lQ}}
\newcommand{\lU}{\mathsf{lU}}
\renewcommand{\lq}{\mathsf{lq}}
\newcommand{\lu}{\mathsf{lu}}
\newcommand{\cul}{\ulcorner}
\newcommand{\cur}{\urcorner}
\newcommand{\norm}{\mathsf{norm}}
\newcommand{\Nf}{\mathsf{Nf}}
\newcommand{\Ne}{\mathsf{Ne}}
\newcommand{\Nfs}{\mathsf{Nfs}}
\newcommand{\Nes}{\mathsf{Nes}}
\newcommand{\ID}{\mathsf{ID}}
\newcommand{\id}{\mathsf{id}}
\newcommand{\nat}{\,\dot{\rightarrow}\,}
%\newcommand{\nat}{\overset{\mathsf{n}}{\ra}} % this is how we denote it in the formalisation
\newcommand{\Nat}{\overset{\mathsf{N}}{\ra}}
\renewcommand{\S}{\overset{\mathsf{s}}{\ra}} % we have it with uppercase S in the formalisation
\newcommand{\ap}{\mathsf{ap}}
\newcommand{\blank}{\mathord{\hspace{1pt}\text{--}\hspace{1pt}}} %from the book
%\newcommand{\blank}{\!{-}\!}
\newcommand{\lam}{\mathsf{lam}}
\newcommand{\app}{\mathsf{app}}
\newcommand{\circid}{\circ\hspace{-0.2em}\id}
\newcommand{\circcirc}{\circ\hspace{-0.2em}\circ}
\newcommand{\tr}[2]{\ensuremath{{}_{#1 *}\mathopen{}{#2}\mathclose{}}} % from the book
\newcommand{\M}{\mathsf{M}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\data}{\mathsf{data}}
\newcommand{\ind}{\hspace{1em}}
\newcommand{\idP}{\mathsf{idP}}
\newcommand{\compP}{\mathsf{compP}}
\newcommand{\idF}{\mathsf{idF}}
\newcommand{\compF}{\mathsf{compF}}
\newcommand{\proj}{\mathsf{proj}}
\newcommand{\ExpPSh}{\mathsf{ExpPSh}}
\newcommand{\map}{\mathsf{map}}
\newcommand{\Var}{\mathsf{Var}}
\newcommand{\Vars}{\mathsf{Vars}}
\newcommand{\vze}{\mathsf{vze}}
\newcommand{\vsu}{\mathsf{vsu}}
\newcommand{\wk}{\mathsf{wk}}
\newcommand{\neuU}{\mathsf{neuU}}
\newcommand{\neuEl}{\mathsf{neuEl}}
\newcommand{\var}{\mathsf{var}}
\newcommand{\natn}{\mathsf{natn}}
\newcommand{\natS}{\mathsf{natS}}
\newcommand{\LET}{\mathsf{let}}
\newcommand{\IN}{\mathsf{in}}
\newcommand{\refl}{\mathsf{refl}}
\newcommand{\zero}{\mathsf{zero}}
\newcommand{\suc}{\mathsf{suc}}
\newcommand{\trans}{\mathbin{\raisebox{0.5ex}{$\displaystyle\centerdot$}}}

\begin{document}

%---------------------------------------------------------------------

\begin{frame}[plain]
  \vspace{0.5cm}
  \begin{center}
    \textcolor{blue}{
      {\Large Type theory in type theory \\\vspace{0.2em}
      using\\\vspace{0.5em}
      quotient inductive types}
    }
    
    \vspace{0.7cm}

    Ambrus Kaposi \\
    (joint work with Thorsten Altenkirch) \\

    \vspace{0.2cm}

    University of Nottingham \\

    \vspace{0.7cm}

    Theoretical CS Seminar, Birmingham \\
    26 February 2016
    
  \end{center}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Goal}
\begin{itemize}
\item To represent the syntax of type theory inside type theory
\item Why?
  \begin{itemize}
  \item Study the metatheory in a nice language
  \item Template type theory
  \end{itemize}
\end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Structure}
  \tableofcontents
\end{frame}

%=====================================================================

\section{Simple type theory}

%---------------------------------------------------------------------

\begin{frame}{Expressing the judgements of type theory}
  \begin{center}
    \vspace{2em}
    
    |Γ ⊢ t : A|

    \vspace{1em}
    
    will be formalised as
    
    \vspace{1em}

    |t : Tm Γ A|

    \vspace{4em}

    (We have a \textbf{typed} presentation, no preterms)
  \end{center}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Simple type theory with preterms}
  \begin{spec}
    x   ::= zero | suc x
    t   ::= x | lam t | app t t
    A   ::= ι | A ⇒ A
    Γ   ::= • | Γ , A
  \end{spec}
  We define the relations $\vdash_v$ and $\vdash$.
  \begin{equation*}
    \begin{gathered}
      \infer{\Gamma,A \vdash_v \zero : A}{}
    \end{gathered}
    \hspace{2em}
    \begin{gathered}
      \infer{\Gamma,B \vdash_v \suc\,x : A}{\Gamma \vdash_v x : A}
    \end{gathered}
  \end{equation*}
  \begin{equation*}
    \begin{gathered}
      \infer{\Gamma \vdash x : A}{\Gamma \vdash_v x : A}
    \end{gathered}
    \hspace{0.7em}
    \begin{gathered}
      \infer{\Gamma \vdash \lam\,t : A \ra B}{\Gamma,A \vdash t : B}
    \end{gathered}
    \hspace{0.7em}
    \begin{gathered}
      \infer{\Gamma \vdash \app\, t \,u : B}{\Gamma\vdash t : A \ra B & \Gamma\vdash u : A}
    \end{gathered}
  \end{equation*}  
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Simple type theory in idealised Agda (i)}
\begin{spec}
data Ty     : Set where
  ι         : Ty
  _⇒_       : Ty → Ty → Ty
data Con    : Set where
  •         : Con
  _,_       : Con → Ty → Con
data Var    : Con → Ty → Set where
  zero      : Var (Γ , A) A
  suc       : Var Γ A → Var (Γ , B) A
data Tm     : Con → Ty → Set where
  var       : Var Γ A → Tm Γ A
  lam       : Tm (Γ , A) B → Tm Γ (A ⇒ B)
  app       : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
\end{spec}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Simple type theory in Agda (ii)}
  \begin{itemize}
  \item In addition, we need substitutions:
    \begin{spec}
      Tms   : Con → Con → Set
      _[_]  : Tm Γ A → Tms Δ Γ → Tm Δ A
    \end{spec}
  \item Now we can define a conversion relation:
    \begin{spec}
      _~_ : Tm Γ A → Tm Γ A → Set
    \end{spec}
    eg. |app (lam t) u ~ t [ id , u ]|
  \item The intended syntax is a quotient:
    \begin{spec}
      Tm Γ A / ~
    \end{spec}
  \end{itemize}
\end{frame}

%=====================================================================

\section{Dependent type theory}

%---------------------------------------------------------------------

\begin{frame}{The syntax of dependent type theory (i)}
  \begin{itemize}
  \item Types depend on contexts
  \item Substitutions are mentioned in the application rule: \\\vspace{0.3em}
    \begin{spec}
      app  :  Tm Γ (Π A B) → (a : Tm Γ A)
           →  Tm Γ (B [ a ])
    \end{spec}
  \item We need an inductive-inductive definition:
    \begin{spec}
      data Con  : Set
      data Ty   : Con → Set
      data Tms  : Con → Con → Set
      data Tm   : (Γ : Con) → Ty Γ → Set
    \end{spec}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{The syntax of dependent type theory (ii)}
  \begin{itemize}
  \item In addition, there is a coercion rule for terms: \\\vspace{0.1em}
    \begin{equation*}
      \infer{\Gamma \vdash t : B}{\Gamma \vdash A \sim B && \Gamma \vdash t : A}
    \end{equation*}
  \item This forces us to define conversion relations mutually:
    \begin{spec}
      data Con     : Set
      data Ty      : Con → Set
      data Tms     : Con → Con → Set
      data Tm      : (Γ : Con) → Ty Γ → Set
      data _~Con_  : Con → Con → Set
      data _~Ty_   : Ty Γ → Ty Γ → Set
      data _~Tms_  : Tms Δ Γ → Tms Δ Γ → Set
      data _~Tm_   : Tm Γ A → Tm Γ A → Set
    \end{spec}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{}
\fontsize{7}{7.2}\selectfont
(1) Contexts:
\begin{equation*}
  \begin{gathered}
    \infer[\text{C-empty}]{\cdot \vdash}{\vphantom{\Gamma}}
  \end{gathered}
  \hspace{1em}
  \begin{gathered}
    \color{blue}
    \infer[\text{C-ext}]{\Gamma.x:A \vdash}{\Gamma \vdash && \Gamma \vdash A : \mathsf{U}}
  \end{gathered}
\end{equation*}
(2) Terms:
\begin{equation*}
  \begin{gathered}
    \infer[\text{var}]{\Gamma.x:A \vdash x : A}{\Gamma \vdash A : \mathsf{U}}
  \end{gathered}
  \hspace{1em}
  \begin{gathered}
    \infer[\text{wk}]{\Gamma.x:B \vdash t : A}{\Gamma \vdash t : A && \Gamma \vdash B : \mathsf{U}}
  \end{gathered}
  \hspace{1em}
  \begin{gathered}
    \infer[\mathsf{U}\text{-I}]{\Gamma \vdash \mathsf{U} : \mathsf{U}}{\Gamma \vdash}
  \end{gathered}
\end{equation*}
\begin{equation*}
  \begin{gathered}
    \infer[\Pi\text{-F}]{\Gamma \vdash \Pi(x:A).B : \mathsf{U}}{\Gamma \vdash A : \mathsf{U} && \Gamma.x:A \vdash B : \mathsf{U}}
  \end{gathered}
  \hspace{1em}
  \begin{gathered}
    \infer[\Pi\text{-I}]{\Gamma \vdash \lambda x . t : \Pi(x:A).B}{\Gamma.x:A \vdash t : B}
  \end{gathered}
  \hspace{1em}
  \begin{gathered}
    \color{red}
    \infer[\Pi\text{-E}]{\Gamma \vdash f \, a : B[x \mapsto a]}{\Gamma \vdash f : \Pi(x:A).B && \Gamma \vdash a : A}
  \end{gathered}
\end{equation*}
\begin{equation*}
\begin{gathered}
\color{red}\infer[\text{t-coe}]{\Delta \vdash t : B}{\Gamma \sim \Delta \vdash & \Gamma \vdash A \sim B : \mathsf{U} & \Gamma \vdash t : A}
\end{gathered}
\end{equation*}
(3) Conversion for contexts:
\begin{equation*}
\begin{gathered}
\infer[\text{C-eq-refl}]{\Gamma \sim \Gamma \vdash}{\Gamma \vdash}
\end{gathered}
\hspace{1em}
\begin{gathered}
\infer[\text{C-eq-sym}]{\Delta \sim \Gamma \vdash}{\Gamma \sim \Delta \vdash }
\end{gathered}
\hspace{1em}
\begin{gathered}
\infer[\text{C-eq-trans}]{\Gamma \sim \Theta \vdash}{\Gamma \sim \Delta \vdash &
  \Delta \sim \Theta \vdash}
\end{gathered}
\end{equation*}
\begin{equation*}
\begin{gathered}
\infer[\text{C-ext-cong}]{\Gamma.x:A \sim \Delta.x:B \vdash}{\Gamma \sim \Delta \vdash & \Gamma \vdash A \sim B : \mathsf{U}}
\end{gathered}
\end{equation*}
(4) Conversion for terms:
\begin{equation*}
\begin{gathered}
\infer[\text{t-eq-refl}]{\Gamma \vdash t \sim t : A}{\Gamma \vdash t : A}
\end{gathered}
\hspace{1em}
\begin{gathered}
\infer[\text{t-eq-sym}]{\Gamma \vdash v \sim u : A}{\Gamma \vdash u \sim v : A}
\end{gathered}
\hspace{1em}
\begin{gathered}
\infer[\text{t-eq-trans}]{\Gamma \vdash u \sim w : A}{\Gamma \vdash u \sim v : A & \Gamma \vdash v \sim w : A}
\end{gathered}
\end{equation*}
\begin{equation*}
\begin{gathered}
\infer[\text{t-eq-coe}]{\Delta \vdash u \sim v : B}{\Gamma \sim \Delta \vdash & \Gamma \vdash A \sim B : \mathsf{U} & \Gamma \vdash u \sim v : A}
\end{gathered}
\hspace{1em}
\begin{gathered}
\infer[\Pi\text{-F-cong}]{\Gamma \vdash \Pi(x:A).B \sim \Pi(x:A').B' : \mathsf{U}}{\Gamma \vdash A \sim A' : \mathsf{U} && \Gamma.x:A \vdash B \sim B' : \mathsf{U}}
\end{gathered}
\end{equation*}
\begin{equation*}
\begin{gathered}
\infer[\Pi\text{-I-cong}]{\Gamma \vdash \lambda x.t \sim \lambda x.t' : \Pi(x:A).B}{\Gamma.x:A \vdash t \sim t' : B}
\end{gathered}
\hspace{1em}
\begin{gathered}
\infer[\Pi\text{-E-cong}]{\Gamma \vdash f \, a \sim f' \, a' : B[x \mapsto a]}{\Gamma \vdash f \sim f' : \Pi(x:A).B && \Gamma \vdash a \sim a' : A}
\end{gathered}
\end{equation*}
\begin{equation*}
  \begin{gathered}
    \color{red}
\infer[\Pi\text{-}\beta]{\Gamma \vdash (\lambda x . t) \, a \sim t[x \mapsto a] : B[x \mapsto a]}{\Gamma.x:A \vdash t : B}
\end{gathered}
\hspace{1em}
\begin{gathered}
\infer[\Pi\text{-}\eta]{\Gamma \vdash f \sim (\lambda x . f \, x) : \Pi(x:A).B}{\Gamma \vdash f : \Pi(x:A).B}
\end{gathered}
\end{equation*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Lots of boilerplate}
  \begin{itemize}
  \item The |_~X_| relations are equivalence relations
  \item Coercion rules
  \item Congruence rules
  \item We need to work with setoids
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{The identity type |_≡_|}
  \begin{itemize}
  \item Equality (the identity type) is an equivalence relation
  \item We can coerce between equal types
  \item Equality is a congruence
  \item What about the extra equalities (eg. |β|, |η| for |Π|)?
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Higher inductive types}
  \begin{itemize}
  \item An idea from homotopy type theory: \\ constructors for
    equalities.\vspace{-0.5em}
  \item Example:
  \begin{spec}
    data I     : Set where
      zero     : I
      one      : I
      segment  : zero ≡ one
  \end{spec}\pause\vspace{-1.5em}
  \begin{spec}
    RecI  :  (Iᴹ : Set)
             (zeroᴹ     : Iᴹ)
             (oneᴹ      : Iᴹ)
             (segmentᴹ  : zeroᴹ ≡ oneᴹ)
          →  I → Iᴹ
  \end{spec}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Quotient inductive types (QITs)}
  \begin{itemize}
  \item A higher inductive type which is truncated to an h-set.
  \item They are \emph{not} the same as quotient types: equality
    constructors are defined at the same time
  \item QITs can be simulated in Agda
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{The syntax of dependent type theory (iii)}
  \begin{itemize}
  \item We defined the syntax of a basic type theory as a quotient
    inductive inductive type (with |Π| and an uninterpreted family of
    types |U|, |El|)
  \item We don't need to state the equivalence relation, coercion,
    congruence laws anymore
  \item We collect the arguments of the recursor into a record:
    \begin{spec}
      record Model   : Set where
        field  Conᴹ  : Set
               Tyᴹ   : Conᴹ → Set   ...
    \end{spec}
  \item \vspace{-1em} which is the type of algebras for the QIT \\=
    the type of models of type theory, close to CwF. Initiality is
    given by the recursor
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{}
  \fontsize{9}{7.2}\selectfont
  \begin{alignat*}{6}
  & \hspace{-1.5em} \ind \cdot && : \Con && 												  \ind [\id] && : A[\id] \equiv A \\							       
  & \hspace{-1.5em} \ind \blank,\blank && : (\Gamma : \Con) \ra \Ty \, \Gamma \ra \Con && 						  \ind [][] && : A[\sigma][\nu] \equiv A[\sigma \circ \nu] \\
  &                                                                                                                                 && && \ind \U[] && : \U[\sigma] \equiv \U \\						
  & \hspace{-1.5em} \ind \blank[\blank] && : \Ty \, \Delta \ra \Tms \, \Gamma \, \Delta \ra \Ty \, \Gamma && 				  \ind \El[] && : (\El\,\hat{A})[\sigma] \equiv \El\,(\tr{\U[]}{\hat{A}[\sigma]}) \\		       
  & \hspace{-1.5em} \ind \U && : \Ty \, \Gamma && 											  \ind \Pi[] && : (\Pi\,A\,B)[\sigma] \equiv \Pi\,(A[\sigma])\,(B[\sigma^A]) \\		       
  & \hspace{-1.5em} \ind \El && : \Tm \, \Gamma \, \U \ra \Ty \, \Gamma && 						              \\
  & \hspace{-1.5em} \ind \Pi && : (A : \Ty \, \Gamma) \ra \Ty \, (\Gamma , A) \ra \Ty \, \Gamma && 					  \ind \id\circ && :  \id \circ \sigma \equiv \sigma \\					       
  &                                                                                                                                 && && \ind \hspace{-0.2em}\circid && :   \sigma \circ \id \equiv \sigma \\					       
  & \hspace{-1.5em} \ind \id && :  \Tms \, \Gamma \, \Gamma && 										  \ind \hspace{-0.2em}\circcirc && : (\sigma \circ \nu) \circ \delta \equiv \sigma \circ (\nu \circ \delta) \\
  & \hspace{-1.5em} \ind \blank\circ\blank && :  \Tms\,\Theta\,\Delta \ra \Tms\,\Gamma\,\Theta \ra \Tms\,\Gamma\,\Delta && 		  \ind \epsilon\eta && : \{\sigma : \Tms\,\Gamma\,\cdot\} \ra \sigma \equiv \epsilon \\	         
  & \hspace{-1.5em} \ind \epsilon && : \Tms \, \Gamma \, \cdot && 									  \ind \pi_1\beta && : \pi_1\,(\sigma, t) \equiv \sigma \\				       
  & \hspace{-1.5em} \ind \blank,\blank && : (\sigma : \Tms\,\Gamma\,\Delta) \ra \Tm\,\Gamma\,A[\sigma] \ra \Tms\,\Gamma\,(\Delta,A) && 	  \ind \pi\eta && : (\pi_1\,\sigma, \pi_2\,\sigma) \equiv \sigma \\			       
  & \hspace{-1.5em} \ind \pi_1 && : \Tms\,\Gamma\,(\Delta,A) \ra \Tms\,\Gamma\,\Delta && 						  \ind ,\circ && : (\sigma, t) \circ \nu \equiv (\sigma \circ \nu) , (\tr{[][]}{t[\nu]}) \\
  &                                                                                                                                 && && \\
  & \hspace{-1.5em} \ind \blank[\blank] && : \Tm\,\Delta\,A \ra (\sigma : \Tms\,\Gamma\,\Delta) \ra \Tm\,\Gamma\,A[\sigma] && 		  \ind \pi_2\beta && : \pi_2\,(\sigma, t) \equiv^{\pi_1\beta} t \\     
  & \hspace{-1.5em} \ind \pi_2 && : (\sigma : \Tms\,\Gamma\,(\Delta,A)) \ra \Tm\,\Gamma\,A[\pi_1 \, \sigma] && 				  \ind \Pi\beta && : \app\,(\lam\,t) \equiv t \\		        		       
  & \hspace{-1.5em} \ind \lam && : \Tm\,(\Gamma,A)\,B \ra \Tm\,\Gamma\,(\Pi\,A\,B) && 							  \ind \Pi\eta && : \lam\,(\app\,t) \equiv t \\			        			       
  & \hspace{-1.5em} \ind \app && : \Tm\,\Gamma\,(\Pi\,A\,B) \ra \Tm\,(\Gamma,A)\,B &&						          \ind \lam[] && : (\lam\,t)[\sigma] \equiv^{\Pi[]} \lam\,(t[\sigma^A]) 
\end{alignat*}
\end{frame}

%=====================================================================

\section{Standard model}

%---------------------------------------------------------------------

\begin{frame}{Standard model}
  \begin{itemize}
  \item A sanity check
  \item Every syntactic construct is interpreted as the corresponding
    metatheoretic construction.
    \begin{spec}
      Conᴹ              = Set
      Tyᴹ   ⟦Γ⟧         = ⟦Γ⟧ → Set
      
      Πᴹ    ⟦A⟧ ⟦B⟧  γ  = (x : ⟦A⟧ γ) → ⟦B⟧ (γ , x)
      lamᴹ  ⟦t⟧      γ  = λ x → ⟦t⟧ (γ , x)
      ...
    \end{spec}
  \end{itemize}
\end{frame}

%=====================================================================

\section{Logical predicate interpretation}

%---------------------------------------------------------------------

\begin{frame}{Logical predicate interpretation (i)}
  \begin{itemize}
  \item Unary parametricity says that terms respect logical
    predicates. Example:
  \begin{equation*}
    A : \U, x : A \vdash t : A
  \end{equation*}
  \item For any predicate on $A$, if $x$ respects it, so will $t$.
  \item Given a type $B$ and $u : B$, we define $B^M\,x := (x \equiv
    u)$.
  \item $A := B, A^M := B^M, x := u, x^M := \refl$
  \item Now we get $A^M\,t = B^M\,t = (t \equiv u)$.
  \end{itemize}
\end{frame}

\begin{frame}{Logical predicate interpretation (ii)}
  \begin{itemize}
  \item Bernardy-Jansson-Paterson: Parametricity and Dependent Types,
    2012
  \item A type is interpreted as a logical predicate over that type
    \begin{equation*}
      \begin{gathered}
        \infer{|Γ ᴾ| \text{ valid}}{|Γ| \text{ valid}}
      \end{gathered}
      \hspace{2em}
      \begin{gathered}
        \infer{|Γ ᴾ ⊢ A ᴾ : A → Set|}{|Γ ⊢ A : Set|}
      \end{gathered}
    \end{equation*}
  \item A term is interpreted as a proof that it satisfies the
    predicate
    \begin{equation*}
      \infer{|Γ ᴾ ⊢ t ᴾ : (A ᴾ) t|}{|Γ ⊢ t : A|}
    \end{equation*}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Logical predicate interpretation (iii)}
  An interpretation from the syntax into the syntax:
\begin{spec}
  • ᴾ               = •
  (Γ , x : A) ᴾ     = Γ ᴾ , x : A , xᴹ : A ᴾ x
  U ᴾ               = λ A → (A → U)
  ((x : A) → B) ᴾ   = λ f →  ((x : A) (xᴹ : A ᴾ x)
                             → B ᴾ (f x))
  (λ x → t) ᴾ       = λ x xᴹ → t ᴾ
  (t u) ᴾ           = t ᴾ u (u ᴾ)
  x ᴾ               = xᴹ
\end{spec}
These equations are all typed. Template type theory: automated
derivation of free theorems
\end{frame}

%=====================================================================

\section{Presheaf models, normalisation by evaluation}

%---------------------------------------------------------------------


\newcommand\arcfrombottom{
  \begin{tikzpicture}[scale=0.03em]
    \draw (0,0) arc (0:180:0.5);
    \draw (0,0) edge[->] (0,-0.3);
    \draw (-1,0) edge (-1,-0.3);
  \end{tikzpicture}
}
\newcommand\arcfromtop{
  \begin{tikzpicture}[scale=0.03em]
    \draw (0,0) arc (180:360:0.5);
    \draw (0,0) edge[->] (0,0.3);
    \draw (1,0) edge (1,0.3);
  \end{tikzpicture}
}

\begin{frame}{Normalisation}
\begin{equation*}
   \text{completeness }\arcfromtop \hspace{1em} \norm\downarrow\begin{array}{l}\infer={\hspace{1em} \Nf \, \Gamma \, A \hspace{1em} }{\Tm \, \Gamma \, A}\end{array}\uparrow\cul\blank\cur \hspace{1em} \arcfrombottom\text{ stability}
\end{equation*}
\begin{alignat*}{10}
  & \data \, \Ne && : (\Gamma:\Con) \ra \Ty\,\Gamma \ra \Set \\                                                                 
  & \ind \var && : \Var\,\Gamma\,A \ra \Ne\,\Gamma\,A \\
  & \ind \app && : \Ne\,\Gamma\,(\Pi\,A\,B) \ra (v : \Nf\,\Gamma\,A) \ra \Ne\,\Gamma\,(B[\cul v\cur]) \\
  & \data \, \Nf && : (\Gamma:\Con) \ra \Ty\,\Gamma \ra \Set \\
  & \ind \neuU && : \Ne\,\Gamma\,\U \ra \Nf\,\Gamma\,\U \\
  & \ind \neuEl && : \Ne\,\Gamma\,(\El\,\hat{A}) \ra \Nf\,\Gamma\,(\El\,\hat{A}) \\
  & \ind \lam && : \Nf\,(\Gamma,A)\,B \ra \Nf\,\Gamma\,(\Pi\,A\,B)
\end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Presheaf model}
  \begin{itemize}
  \item Proof relevant version of Kripke model: category instead of
    poset
  \item Given a category $\mathcal{C}$
  \item Contexts are presheaves over $\mathcal{C}$: for every object
    of $\mathcal{C}$ we have a set and for morphisms we get maps
    between the sets
  \item Types are families of presheaves, terms are sections
  \item We need to give interpretations to the base type
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{NBE for simple type theory (i)}
  \begin{itemize}
  \item Presheaf model over the category of renamings $\REN^{\op}$
    \begin{itemize}
    \item Objects are contexts
    \item Morphisms are renamings (lists of variables)
    \end{itemize}
  \item The base type |•| at |Γ| is interpreted as |Ne Γ •|
  \item We denote the interpretation $\ll\blank\rr$
  \item We define quote and unquote mutually:
    \begin{equation*}
      \begin{gathered}
        \u_A :  \NE_A \nat \ll A \rr
      \end{gathered}
      \hspace{2em}
      \begin{gathered}
        \q_A : \ll A \rr \nat \NF_A
      \end{gathered}
    \end{equation*}
\begin{equation*}
  \norm_A\,(t:\Tm\,\Gamma\,A) := \q_{A}\,(\ll t\rr\,\id_\Gamma)
\end{equation*}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{NBE for simple type theory (ii)}
  \begin{itemize}
  \item Presheaf model over $\REN^{\op}$, base type |•| is |NE •|
  \item For completeness, we need a logical relation
  \begin{itemize}
  \item metatheoretic
  \item Kripke (base category $\REN^{\op}$)
  \item binary
  \item proof-irrelevant
  \item relation at |•| is equality
  \end{itemize}
   \begin{tikzpicture}
    \node (NE) at (0,0) {$\NE_A$};
    \node (XX) at (4,0) {$\Sigma\,(\TM_A \times \ll A\rr)\,\mathsf{R}_A$};
    \node (NF) at (8,0) {$\NF_A$};
    \node (TM) at (4,-1.5) {$\TM_A$};
    \draw[->] (NE) edge node[above] {$\u_A$} (XX);
    \draw[->] (XX) edge node[above] {$\q_A$} (NF);
    \draw[->] (NE) edge node[below] {$\cul\blank\cur\hspace{1em}$} (TM);
    \draw[->] (NF) edge node[below] {$\hspace{1em}\cul\blank\cur$} (TM);
    \draw[->] (XX) edge node[right] {$\mathsf{proj}$} (TM);
    \end{tikzpicture}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{NBE for type theory}
  \begin{itemize}
  \item No need for presheaf model
  \item Instead we have a logical predicate
  \begin{itemize}
  \item metatheoretic
  \item Kripke (base category $\REN^{\op}$)
  \item unary
  \item proof-relevant
  \item predicate at |•|: |λ t . Σ (n : Ne Γ •). n ≡ t |
  \end{itemize}
   \begin{tikzpicture}
    \node (NE) at (0,0) {$\NE_\Delta$};
    \node (XX) at (4,0) {$\Sigma\,\TM_\Delta \, \Delta^{\mathsf{P}}$};
    \node (NF) at (8,0) {$\NF_\Delta$};
    \node (TM) at (4,-1.5) {$\TM_\Delta$};
    \draw[->] (NE) edge node[above] {$\u_\Delta$} (XX);
    \draw[->] (XX) edge node[above] {$\q_\Delta$} (NF);
    \draw[->] (NE) edge node[below] {$\cul\blank\cur\hspace{1em}$} (TM);
    \draw[->] (NF) edge node[below] {$\hspace{1em}\cul\blank\cur$} (TM);
    \draw[->] (XX) edge node[right] {$\mathsf{proj}$} (TM);
    \end{tikzpicture}
  \end{itemize}
\end{frame}


%=====================================================================

\section{The future}

%---------------------------------------------------------------------

\begin{frame}{Further work}
  \begin{itemize}
  \item We internalized a very basic type theory, this can be extended
    easily with universes and inductive types. How to do it a nice
    categorical way?
  \item We used axioms (quotient inductive types, functional
    extensionality) in our metatheory. This can be solved by cubical
    type theory.
  \item Still lots of boilerplate equality reasoning. Solution:
    informally extensional type theory, formally cubical type theory?
  \item If we work within HoTT, we can only eliminate into
    h-sets. Hence, the standard model doesn't work as described.
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Template type theory}
  \begin{itemize}
  \item Given a model of type theory, together with new constants in
    that model
  \item We can interpret code that uses the new constants inside the
    model
  \item The code can use all the conveniences such as implicit
    arguments, pattern matching etc.
  \item This way we can justify extensions of type theory:
    \begin{itemize}
    \item guarded type theory
    \item local state monad
    \item parametricity
    \item homotopy type theory
    \end{itemize}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\end{document}
