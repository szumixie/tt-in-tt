\documentclass[a4paper]{easychair}

\usepackage{doc}
\usepackage{makeidx}

\usepackage{cite}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{stmaryrd}
\usepackage{tikz}

\newcommand{\U}{\mathsf{U}}
\newcommand{\El}{\mathsf{El}}
\newcommand{\REN}{\mathsf{REN}}
\newcommand{\op}{\mathsf{op}}
\newcommand{\ra}{\rightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\Set}{\mathsf{Set}}
\newcommand{\PSh}{\mathsf{PSh}}
\newcommand{\FamPSh}{\mathsf{FamPSh}}
\renewcommand{\ll}{\llbracket}
\providecommand{\rr}{\rrbracket}
\newcommand{\Con}{\mathsf{Con}}
\newcommand{\Ty}{\mathsf{Ty}}
\newcommand{\Tm}{\mathsf{Tm}}
\newcommand{\Tms}{\mathsf{Tms}}
\newcommand{\R}{\mathsf{R}}
\newcommand{\TM}{\mathsf{TM}}
\newcommand{\NE}{\mathsf{NE}}
\newcommand{\NF}{\mathsf{NF}}
\newcommand{\q}{\mathsf{q}}
\renewcommand{\u}{\mathsf{u}}
\renewcommand{\ne}{\mathsf{ne}}
\newcommand{\nf}{\mathsf{nf}}
\newcommand{\lQ}{\mathsf{lQ}}
\newcommand{\lU}{\mathsf{lU}}
\renewcommand{\lq}{\mathsf{lq}}
\newcommand{\lu}{\mathsf{lu}}
\newcommand{\cul}{\ulcorner}
\newcommand{\cur}{\urcorner}
\newcommand{\norm}{\mathsf{norm}}
\newcommand{\Nf}{\mathsf{Nf}}
\newcommand{\Ne}{\mathsf{Ne}}
\newcommand{\Nfs}{\mathsf{Nfs}}
\newcommand{\Nes}{\mathsf{Nes}}
\newcommand{\ID}{\mathsf{ID}}
\newcommand{\id}{\mathsf{id}}
\newcommand{\nat}{\,\dot{\rightarrow}\,}
%\newcommand{\nat}{\overset{\mathsf{n}}{\ra}} % this is how we denote it in the formalisation
\newcommand{\Nat}{\overset{\mathsf{N}}{\ra}}
\renewcommand{\S}{\overset{\mathsf{s}}{\ra}} % we have it with uppercase S in the formalisation
\newcommand{\ap}{\mathsf{ap}}
\newcommand{\blank}{\mathord{\hspace{1pt}\text{--}\hspace{1pt}}} %from the book
%\newcommand{\blank}{\!{-}\!}
\newcommand{\lam}{\mathsf{lam}}
\newcommand{\app}{\mathsf{app}}
\newcommand{\circid}{\circ\hspace{-0.2em}\id}
\newcommand{\circcirc}{\circ\hspace{-0.2em}\circ}
\newcommand{\tr}[2]{\ensuremath{{}_{#1 *}\mathopen{}{#2}\mathclose{}}} % from the book
\newcommand{\M}{\mathsf{M}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\data}{\mathsf{data}}
\newcommand{\ind}{\hspace{1em}}
\newcommand{\idP}{\mathsf{idP}}
\newcommand{\compP}{\mathsf{compP}}
\newcommand{\idF}{\mathsf{idF}}
\newcommand{\compF}{\mathsf{compF}}
\newcommand{\proj}{\mathsf{proj}}
\newcommand{\ExpPSh}{\mathsf{ExpPSh}}
\newcommand{\map}{\mathsf{map}}
\newcommand{\Var}{\mathsf{Var}}
\newcommand{\Vars}{\mathsf{Vars}}
\newcommand{\vze}{\mathsf{vze}}
\newcommand{\vsu}{\mathsf{vsu}}
\newcommand{\wk}{\mathsf{wk}}
\newcommand{\neuU}{\mathsf{neuU}}
\newcommand{\neuEl}{\mathsf{neuEl}}
\newcommand{\var}{\mathsf{var}}
\newcommand{\natn}{\mathsf{natn}}
\newcommand{\natS}{\mathsf{natS}}
\newcommand{\LET}{\mathsf{let}}
\newcommand{\IN}{\mathsf{in}}
\newcommand{\refl}{\mathsf{refl}}
\newcommand{\trans}{\mathbin{\raisebox{0.5ex}{$\displaystyle\centerdot$}}}

\newcommand\arcfrombottom{
  \begin{tikzpicture}[scale=0.03em]
    \draw (0,0) arc (0:180:0.5);
    \draw (0,0) edge[->] (0,-0.3);
    \draw (-1,0) edge (-1,-0.3);
  \end{tikzpicture}
}
\newcommand\arcfromtop{
  \begin{tikzpicture}[scale=0.03em]
    \draw (0,0) arc (180:360:0.5);
    \draw (0,0) edge[->] (0,0.3);
    \draw (1,0) edge (1,0.3);
  \end{tikzpicture}
}

%% Document
%%
\begin{document}

%% Front Matter
%%
\title{Normalisation by Evaluation for Dependent Types\footnote{Supported by USAF grant FA9550-16-1-0029.}}

\titlerunning{Normalisation by Evaluation for Dependent Types}

\author{
  Thorsten Altenkirch
  \and
  Ambrus Kaposi
}

\institute{
  University of Nottingham, United Kingdom\\
  \email{\{txa,auk\}@cs.nott.ac.uk}
}

\authorrunning{Altenkirch and Kaposi}

\clearpage

\maketitle

%---------------------------------------------------------------------

\begin{abstract}
  We prove normalisation for a basic dependent type theory using the
  technique of normalisation by evaluation (NBE). NBE works by
  evaluating the syntax into a model and then computing normal forms
  by quoting semantic objects into normal terms. As model we use the
  syntax glued together with a proof-relevant logical predicate. The
  syntax is defined internally in type theory as a quotient-inductive
  type. We use a typed presentation of the syntax, we don't mention
  preterms. Parts of the construction are formalised in Agda. A full
  paper will be presented at FSCD 2016.
\end{abstract}

\pagestyle{empty}

%---------------------------------------------------------------------

\subsection*{Specifying normalisation}

We denote the type of well typed terms of type $A$ in context $\Gamma$
by $\Tm \, \Gamma \, A$. This type is defined as a quotient inductive
inductive type (QIIT, see \cite{ttintt}): in addition to normal
constructors for terms such as $\lam$ and $\app$, it also has equality
constructors e.g. expressing the $\beta$ computation rule for
functions. An equality $t \equiv_{\Tm \, \Gamma \, A} t'$ expresses
that $t$ and $t'$ are convertible. Typed normal forms are denoted $\Nf
\, \Gamma \, A$ and are defined mutually with neutral terms $\Ne \,
\Gamma \, A$ and the embedding $\cul \blank \cur : \Nf \, \Gamma \, A
\ra \Tm\,\Gamma\,A$. Normalisation is given by a function $\norm$
which is an isomorphism\footnote{This might by surprising however it
  can be explained by the fact that the conversion relation is part of
  the equality structure on terms.}:
\vspace{-0.7em}
\begin{equation*}
   \text{completeness }\arcfromtop \hspace{2em} \norm\downarrow\begin{array}{l}\infer={\hspace{1em} \Nf \, \Gamma \, A \hspace{1em} }{\Tm \, \Gamma \, A}\end{array}\uparrow\cul\blank\cur \hspace{2em} \arcfrombottom\text{ stability}
\vspace{-0.5em}
\end{equation*}
Completeness says that normalising a term produces a term which is
convertible to it: $t \equiv \cul \norm \, t \cur$. Stability
expresses that there is no redundancy in the type of normal forms: $n
\equiv \norm \, \cul n \cur$. The usual notion of soundness of the
semantics, that is, if $t \equiv t'$ then $\norm \, t \equiv \norm \,
t'$ is given by congruence of equality. The elimination rule for the
QIIT of the syntax ensures that every function defined from the syntax
respects the equality constructors.

%---------------------------------------------------------------------

\subsection*{NBE for simple types}

NBE is one way to implement the above specification. It works by
evaluating the syntax in a model and defining a quote function which
turns semantic objects into normal terms. We follow the categorical
approach to NBE as given by \cite{alti:ctcs95} for simple types. Here
the model is a presheaf model over the category of renamings (objects
are contexts, morphisms are lists of variables) and the interpretation
of the base type is the set of normal terms at the base type.

The structure of the normalisation proof is given by the following
diagram. It summarizes normalisation of a substitution into context
$\Delta$, a similar diagram can be given for terms.
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\NE_\Delta$};
\node (XX) at (3,0) {$\boxed{\Sigma\,(\TM_\Delta \times \ll\Delta\rr)\,\mathsf{R}_\Delta}$};
\node (NF) at (6,0) {$\NF_\Delta$};
\node (TM) at (3,-1.5) {$\TM_\Delta$};
\draw[->] (NE) edge node[above] {$\u_\Delta$} (XX);
\draw[->] (XX) edge node[above] {$\q_\Delta$} (NF);
\draw[->] (NE) edge node[below] {$\cul\blank\cur\hspace{1em}$} (TM);
\draw[->] (NF) edge node[below] {$\hspace{1em}\cul\blank\cur$} (TM);
\draw[->] (XX) edge node[right] {$\mathsf{proj}$} (TM);
\end{tikzpicture}
\end{center}
$\NE_\Delta$, $\TM_\Delta$ and $\NF_\Delta$ denote the Yoneda
embeddings of neutral terms, terms and normal terms,
respectively. These are all presheaves over the category of renamings
where the action on objects returns lists of neutral terms $\Nes$,
substitutions $\Tms$ and lists of normal forms $\Nfs$, respectively.
\begin{alignat*}{7}
  & \NE_\Delta \, \Gamma := \Nes\,\Gamma\,\Delta \hspace{6em} \TM_\Delta \, \Gamma := \Tms\,\Gamma\,\Delta \hspace{6em} && && \NF_\Delta \, \Gamma := \Nfs\,\Gamma\,\Delta
\end{alignat*}
The presheaf interpretation of the context $\Delta$ is denoted
$\ll\Delta\rr$. $\R_\Delta$ denotes a binary logical relation at
context $\Delta$. This is a relation between the syntax $\TM$ and the
presheaf semantics $\ll\blank\rr$ and is equality at the base
type. $\u_\Delta$ denotes the unquote natural transformation and
$\q_\Delta$ is quotation. These are defined mutually by induction on
contexts and types.

Normalisation of a substitution $\sigma$ is given by evaluating it in
the presheaf model $\ll\sigma\rr$ and then using quote. It also needs
the semantic counterpart of the identity substitution which is given
by unquoting the identity substitution $\u_\Gamma\,\id$ and a witness
of the logical relation which is given by the fundamental theorem for
the logical relation $\R_\sigma$.
\begin{equation*}
  \norm_\Delta\,(\sigma:\TM_\Delta\,\Gamma) : \NF_\Delta\,\Gamma := \q_{\Delta}\,\big(\sigma, \ll\sigma\rr\,(\u_\Gamma\,\id), \R_\sigma\,(\u_{\Gamma}\,\id)\big)
\end{equation*}
Completeness is given by commutativity of the right hand
triangle. Stability can be proven by mutual induction on terms and
normal forms.

%---------------------------------------------------------------------

\subsection*{NBE for dependent types}

NBE has been extended to dependent types using untyped realizers
\cite{abel2013normalization} and a typed version has been given by
\cite{danielsson2006formalisation} however without a proof of
soundness. Our goal was to extend the categorical approach summarized
in the previous section to dependent types. The straightforward
generalisation does not work because there seems to be no way of
defining unquote for $\Pi$. Here we need to define a semantic function
which works for arbitrary inputs, not only those which are related to
a term. It seems that we need to restrict the presheaf model to only
contain such functions.

We solve this problem by merging the presheaf model $\ll\blank\rr$ and
the logical relation $\R$ into a proof-relevant logical predicate
$\mathsf{P}$. That is, we replace the boxed part of the above diagram
by $\boxed{\Sigma\,\TM_\Delta\,\mathsf{P}_\Delta}$. In the presheaf
model, the interpretation of the base type were normal forms of the
base type, and the logical relation at the base type was equality of
the term and the normal form (equality was proof irrelevant, hence the
logical relation was proof irrelevant). In our case, the logical
predicate at the base type says that there exists a normal form which
is equal to the term, hence it needs to be proof relevant. It can be
seen as an instance of categorical glueing.

%---------------------------------------------------------------------

\bibliography{local,alti}{}
\bibliographystyle{plain}

\end{document}
