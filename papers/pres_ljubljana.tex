\documentclass{beamer}
\usetheme{default}
\useoutertheme{infolines}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{tikz}
\usepackage[utf8]{inputenc}
\usepackage{pmboxdraw}

\usepackage{scrextend}
\changefontsizes{14pt}

\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\setbeamerfont{itemize/enumerate subsubbody}{size=\normalsize}

\DeclareUnicodeCharacter{393}{$\Gamma$}
\DeclareUnicodeCharacter{2192}{$\ra$}
\DeclareUnicodeCharacter{1D39}{${^\M}$}
\DeclareUnicodeCharacter{3B2}{$\beta$}
\DeclareUnicodeCharacter{2261}{$\equiv$}

\input{abbrevs.tex}

\begin{document}

%---------------------------------------------------------------------

\begin{frame}[plain]
  \vspace{0.5cm}
  \begin{center}
    \textcolor{blue}{
      {\Large Normalisation by evaluation \\\vspace{0.2em}
      for\\\vspace{0.5em}
      an intrinsic syntax of type theory}
    }
    
    \vspace{0.7cm}

    Ambrus Kaposi \\
    E{\"o}tv{\"o}s Lor{\'a}nd University, Budapest \\

    \vspace{0.7cm}
    
    joint work with Thorsten Altenkirch (STSM) \\

    \vspace{0.7cm}

    EUTypes Meeting, Ljubljana \\
    31 January 2017
    
  \end{center}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Extrinsic syntax}
  \begin{alignat*}{5}
    & \Gamma && \in \Con && ::= \cdot \,|\, \Gamma,A \\
    & A && \in \Ty && ::= \iota \,|\, \Pi\,A\,A' \\
    & t && \in \Tm && ::= x \,|\, \lam_A\,t \,|\, \app\,t\,t'
  \end{alignat*}
  \vspace{1em}
  \begin{equation*}
    \begin{gathered}
      \infer{\vdash \Gamma,A}{\vdash \Gamma && \Gamma \vdash A}
    \end{gathered}
    \hspace{0.7em}
    \begin{gathered}
      \infer{\Gamma\vdash\Pi\,A\,B}{\Gamma\vdash A && \Gamma,A \vdash B}
    \end{gathered}
    \hspace{0.7em}
    \begin{gathered}
      \infer{\Gamma \vdash \lam\,t : \Pi\,A\,B}{\Gamma,A \vdash t : B}
    \end{gathered}
  \end{equation*}
  \vspace{0.5em}
  \begin{equation*}
    ...
  \end{equation*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Intrinsic syntax}
  Constructors for the inductive inductive type:
  \begin{alignat*}{5}
    & \Con && : \Set \\
    & \Ty && : \Con \ra \Set \\
    & \Tm && : (\Gamma : \Con) \ra \Ty\,\Gamma \ra \Set \\
    & \blank,\blank && : (\Gamma : \Con) \ra \Ty\,\Gamma \ra \Con \\
    & \Pi && : (A : \Ty\,\Gamma) \ra \Ty\,(\Gamma,A) \ra \Ty\,\Gamma \\
    & \lam && : \Tm\,(\Gamma,A)\,B \ra \Tm\,\Gamma\,(\Pi\,A\,B) \\
    & ... \\
    & \\
    &
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Conversion relation}
  Constructors for the quotient inductive inductive type:
  \begin{alignat*}{5}
    & \Con && : \Set \\
    & \Ty && : \Con \ra \Set \\
    & \Tm && : (\Gamma : \Con) \ra \Ty\,\Gamma \ra \Set \\
    & \blank,\blank && : (\Gamma : \Con) \ra \Ty\,\Gamma \ra \Con \\
    & \Pi && : (A : \Ty\,\Gamma) \ra \Ty\,(\Gamma,A) \ra \Ty\,\Gamma \\
    & \lam && : \Tm\,(\Gamma,A)\,B \ra \Tm\,\Gamma\,(\Pi\,A\,B) \\
    & ... \\
    & \beta && : \app\,(\lam\,t) \equiv t \\
    & \eta && : \lam\,(\app\,t) \equiv t
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Eliminator}
  \begin{itemize}
  \item Non-dependent:
    \begin{alignat*}{5}
      & (\Con^\M : \Set)(\Ty^\M : \Con^\M\ra\Set)\,... \hspace{10em} \\
      & ...\, \big(\beta^\M : \app^\M\,(\lam^\M\,t^M) \equiv t^M\big) \\
      & \Rec_\Con : \Con \ra \Con^\M \\
      & \Rec_\Ty : \Ty\,\Gamma \ra \Ty^\M\,(\Rec_\Con\,\Gamma)
    \end{alignat*}
  \item Dependent:
    \begin{alignat*}{5}
      & (\Con^\M : \Con\ra\Set)(\Ty^\M : \Con^\M\,\Gamma\ra\Ty\,\Gamma\ra\Set)\,... \\
      & \Elim_\Con : (\Gamma:\Con) \ra \Con^\M\,\Gamma \\
      & \Elim_\Ty : (A : \Ty\,\Gamma) \ra \Ty^\M\,(\Elim_\Con\,\Gamma)\,A
    \end{alignat*}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}[fragile]{Quotient inductive types in Agda}
\begin{verbatim}
{-# OPTIONS --rewriting #-}

postulate
  Con : Set
  Ty  : Con → Set
  _,_ : (Γ : Con) → Ty Γ → Con

  RecCon : Con → Conᴹ
  RecTy  : Ty Γ → Tyᴹ (RecCon Γ)

  β, : RecCon (Γ , A) ≡ RecCon Γ ,ᴹ RecTy A

{-# REWRITE β, #-}
\end{verbatim}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Relation to old-style syntax}
  Conjecture:
  \begin{equation*}
    \Con \,\,\cong\,\, \Big(\big(\overline{\Gamma}:\overline{\Con}\big)\,\times\,\vdash\overline{\Gamma}\Big)/{\sim_\Con}
  \end{equation*}
  \begin{alignat*}{5}
    & && \,(\Gamma:\Con)\,\times\,\Ty\,\Gamma \\
    & \cong\,\, && \Big(\big(\overline{\Gamma}:\overline{\Con}\big)\,\times\,{\vdash\overline{\Gamma}}\,\times\,(\overline{A}:\overline{\Ty})\,\times\,{\overline{\Gamma}\vdash\overline{A}}\Big)/{\sim_\Con}/{\sim_\Ty}
  \end{alignat*}
  \vspace{1em}
  \begin{equation*}
    ...
  \end{equation*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Models formalised}
  For a theory with $\Pi$, a base type and a family over the base
  type.
  \begin{itemize}
  \item Non-dependent eliminator:
    \begin{itemize}
    \item standard model
    \item presheaf model
    \item setoid model
    \end{itemize}
  \item Dependent eliminator:
    \begin{itemize}
    \item logical predicate translation of Bernardy
    \item presheaf logical predicate interpretation
    \end{itemize}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Neutral terms and normal forms}
  \begin{alignat*}{5}
    & n && ::= \,\, && x \,|\, n\,v \\
    & v && ::= && n \,|\, \lambda x . v
  \end{alignat*}
  \vspace{0.4em}
  
  The intrinsically typed version:
  \begin{alignat*}{10}
    & \Ne && : (\Gamma:\Con) \ra \Ty\,\Gamma \ra \Set \\
    & \Nf && : (\Gamma:\Con) \ra \Ty\,\Gamma \ra \Set \\
    & \var && : \Var\,\Gamma\,A \ra \Ne\,\Gamma\,A \\
    & \app && : \Ne\,\Gamma\,(\Pi\,A\,B) \ra (v : \Nf\,\Gamma\,A)\ra \Ne\,\Gamma\,\big(B[\lb\cul v\cur\rb]\big) \\
    & \neu && : \Ne\,\Gamma\,\iota \ra \Nf\,\Gamma\,\iota \\
    & \lam && : \Nf\,(\Gamma,A)\,B \ra \Nf\,\Gamma\,(\Pi\,A\,B)
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Normalisation}
  \begin{alignat*}{5}
    & \norm && : (t : \Tm\,\Gamma\,A) \ra (v : \Nf\,\Gamma\,A)\times t \equiv \cul v \cur \\
    & \\
    & \stab && : (v : \Nf\,\Gamma\,A) \ra \norm\,\cul v\cur \equiv v \\
    & \\
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Decidability of equality}
  \[
  \Dec\,A = A + \neg A
  \]
  \[
  X \in \{\Ne, \Nf \}
  \]
  First try:
  \begin{alignat*}{5}
    & \dec_X : (n_0\,n_1 : X\,\Gamma\,A)\ra\Dec\,(n_0 \equiv n_1) \\
    &
  \end{alignat*}
  \pause
  Problem:
  \begin{alignat*}{5}
    & \ind \app\,n_0\,v_0 : \Ne\,\Gamma\,(B_0[\lb\cul v_0\cur\rb]) \\
    & \ind \app\,n_1\,v_1 : \Ne\,\Gamma\,(B_1[\lb\cul v_1\cur\rb])
  \end{alignat*}
  \pause
  \[
  n_0 : \Ne\,\Gamma\,(\Pi\,A_0\,B_0) \hspace{3em} n_1 : \Ne\,\Gamma\,(\Pi\,A_1\,B_1)
  \]
  \vspace{10em}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Decidability of equality}
  \[
  \Dec\,A = A + \neg A
  \]
  \[
  X \in \{\Ne, \Nf \}
  \]
  Second try:
  \begin{alignat*}{5}
    & \dec_X : (n_0 : X\,\Gamma\,A_0)(n_1 : X\,\Gamma\,A_1) \\
    & \ind \ra \Dec\,\big((q : A_0 \equiv A_1) \times (n_0 \equiv^q n_1)\big)
  \end{alignat*}
  \pause
  Problem:
  \begin{alignat*}{5}
    & \ind \lam\,v_0 : \Nf\,\Gamma\,(\Pi\,A_0\,B_0) \\
    & \ind \lam\,v_1 : \Nf\,\Gamma\,(\Pi\,A_1\,B_1)
  \end{alignat*}
  \pause
  \[
  v_0 : \Nf\,(\Gamma,A_0)\,B_0 \hspace{3em} v_1 : \Nf\,(\Gamma,A_1)\,B_1
  \]
  \vspace{10em}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Decidability of equality}
  \[
  \Dec\,A = A + \neg A
  \]
  \[
  X \in \{\Ne, \Nf \}
  \]
  Third try:
  \begin{alignat*}{5}
    & \dec_X : (n_0 : X\,\Gamma_0\,A_0)(n_1 : X\,\Gamma_1\,A_1) \\
    & \ind \ra \Dec\,\big((p : \Gamma_0 \equiv \Gamma_1)\times(q : A_0 \equiv^p A_1)\times(n_0 \equiv^{p,q} n_1)\big)
  \end{alignat*}
  \pause
  Problem: \\
  \hspace{2em} \\
  \hspace{2em} We'd need normal forms indexed by normal types.
  \vspace{10em}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Bidirectional type checking}
  \begin{alignat*}{5}
    & \Ne\text{:} \hspace{1em} && \text{the context determines the type} \hspace{1em} && \text{(type inference)} \\
    & \Nf\text{:} && \text{the type is an input} && \text{(type checking)} \\
  \end{alignat*}
  \pause
  Solution:
  \begin{alignat*}{5}
    & \dec_\Ne : (n_0 : \Ne\,\Gamma\,A_0)(n_1 : \Ne\,\Gamma\,A_1) \\
    & \ind \ra \Dec\,\big((q : A_0 \equiv A_1)\times(n_0 \equiv^q n_1)\big) \\
    & \dec_\Nf : (v_0\,v_1 : \Nf\,\Gamma\,A) \ra \Dec\,(v_0 \equiv v_1)
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Summary}
  \begin{itemize}
  \item Intrinsically typed syntax quotiented by conversion
  \item Formalised in Agda
  \item Normalisation can be proved and equality is decidable
  \item Future work:
    \begin{itemize}
    \item Extend the theory with more type formers
    \item We used K in our metatheory. What if we don't?
    \end{itemize}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{TYPES 2017, 29 May -- 1 June}
  \begin{center}
    \includegraphics[scale=0.33]{budapest.jpg}
  \end{center}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{NBE for dependent types}
  \begin{alignat*}{5}
    & && \hspace{-1.2em} \text{Yoneda embedding} && && \hspace{-1.15em} \text{Presheaf logical predicate} \\
    & \Y_\Gamma && : \REN^\op \ra \Set && \P_\Gamma && : \Sigma_\REN\,\Y_\Gamma \ra \Set \\
    & \Y_{A} && : \Sigma_\REN\,\Y_\Gamma\ra\Set \hspace{3em} && \P_A && : \Sigma_{\REN, \Y_\Gamma, \Y_A}\,\P_\Gamma \ra \Set \\
    & \Y_\sigma && : \Y_\Gamma \nat \Y_\Delta && \P_\sigma && : \Sigma_{\Y_\Gamma}\,\P_\Gamma \S \P_\Delta[\Y_\sigma] \\
    & \Y_t && : \Y_\Gamma \S \Y_A && \P_t && : \Sigma_{\Y_\Gamma}\,\P_\Gamma \S \P_A[\Y_t]
  \end{alignat*}
  At the base type:
  \[
  \P_\iota\,t = (v:\Nf\,\Gamma\,\iota)\times(t\equiv \cul v\cur)
  \]
\end{frame}

%---------------------------------------------------------------------

\end{document}
