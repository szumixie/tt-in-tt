{-# OPTIONS --rewriting #-}

module NegFunext.Metatheoretic {i} where

open import Agda.Primitive

open import lib

open import TT.Decl
open import TT.Core
open import TT.FuncNoEta

open import StandardModel.Decl

open Decl {lsuc i} d

open import StandardModel.Core

open Core {lsuc i} c

f : FuncNoEta {lsuc i} c
f = record
  { Π     = λ A B γ → ((α : A γ) → B (γ ,Σ α)) × Bool
  ; Π[]   = refl
  ; lam   = λ t γ → (λ α → t (γ ,Σ α)) ,Σ true
  ; app   = λ { {Γ}{A}{B} t (γ ,Σ α) → proj₁ {B = λ _ → Bool} (t γ) α }
  ; lam[] = refl
  ; Πβ    = refl
  }

open FuncNoEta f

nofunext : ({Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{f g : Tm Γ (Π A B)}{γ : Γ}
           → ((α : A γ) → proj₁ {B = λ _ → Bool} (f γ) α ≡ proj₁ {B = λ _ → Bool} (g γ) α)
           → f γ ≡ g γ)
           -- we could do this internally
         → true ≡ false
nofunext H = ap (λ z → coe z true) (apconst (H (λ _ → refl)){Bool} ⁻¹)
           ◾ apd proj₂
                 (H {Lift ⊤}{λ _ → Lift ⊤}{λ _ → Lift ⊤}
                    {λ _ → ((λ _ → lift tt) ,Σ true)}
                    {λ _ → ((λ _ → lift tt) ,Σ false)}
                    (λ _ → refl))
