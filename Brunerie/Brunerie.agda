{-# OPTIONS --without-K --no-eta #-}

module Brunerie.Brunerie {k} where


open import TT.Decl.Syntax public
open import TT.Core.Syntax public
open import TT.BaseType.Syntax public
open import TT.Iden.Syntax public


open import TT.Brunerie


open import lib
open import Agda.Primitive

𝔹 : Brunerie {k = k} syntaxCore
𝔹 = record
    { ⋆           = ⋆
    ; _≃_         = Id
    ; ≃[]         = Id[]
    ; isContr     = {!Con → Set k!}
    ; base-contr  = {!!}
    ; rec-contr   = {!!}
    ; coherence   = {!!}
    ; coherence[] = {!!}
    }
